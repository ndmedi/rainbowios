#!/usr/bin/env python
# -*- coding: utf-8 -*-

import optparse
import re
import urllib2
import textwrap
import os


FOSS_LIST_RE = r'\s*(\S+).+<([^>]+).+\[\S+\]\((\S*)\)'
FOSS_LIST_RE2 = r'\s*(\S+).+<([^>]+)'


def parse_args():
    parser = optparse.OptionParser()
    parser.add_option('-i', '--input', dest='filename', action="append", default=["./README.md"],
                      help="Read FOSS list from FILE", metavar="FILE")
    parser.add_option('-o', '--output', dest='dest',
                      help="Write FOSS license to FILE", metavar="FILE")
    opts, args = parser.parse_args()

    # Remove default value if any is given as parameter
    if len(opts.filename) > 1:
        opts.filename.pop(0)

    return opts, args, parser


def parse_foss_list(filename):
    foss=[]
    with open(filename) as f:
        for l in f:
            m = re.match(FOSS_LIST_RE, l)
            if m:
                foss.append((m.group(1), m.group(2), m.group(3)))
                continue
            m = re.match(FOSS_LIST_RE2, l)
            if m:
                foss.append((m.group(1), m.group(2)))

    return foss


def main():
    import sys
    sys.path.insert(0, '.')

    opts, args, parser = parse_args()

    dest = None
    if opts.dest is not None:
        dest = open(opts.dest, "w")
    else:
        dest = sys.stdout

    html_header = open(os.path.dirname("./") + "/Colleagues/Rainbow-iOS/MyInfos/LegalsView/legals_html_header.html").read()
    dest.write(html_header)

    for file_name in opts.filename:

        foss = parse_foss_list(file_name)

        for f in foss:

            if len(f) > 2:
                try:
                    txt = urllib2.urlopen(f[2]).read()
                except IOError:
                    sys.stderr.write("Failed to dowload license file for {} at '{}'.\n".format(f[0], f[2]))
                    exit(1)
            else:
                try:
                    txt = open(os.path.dirname(file_name) + "/licenses/"+f[0]+".md").read()

                except IOError:
                    sys.stderr.write("Warning: No license file for {}.\n".format(f[0]))
                    continue

            dest.write("<div class=\"component\">"+f[0] + "</div>"+"\n")
            dest.write("<div class=\"copyright-holders\">" + f[1] + "</div>"+ "\n")
            dest.write("<pre class=\"license\">")

            # Some license file have spaces at beginning of each line
            txt = txt.replace("\n \n", "\n\n").strip()
            txt = txt.split("\n\n")

            for line in txt:
                dest.write(textwrap.fill(line.strip(), 80) + "\n\n")

            dest.write("</pre>")

            dest.write("\n")

        dest.write("</div>"+"\n"+"</body>"+"\n"+"</html>")

if __name__ == '__main__':
    main()
