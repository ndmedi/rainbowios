<a name="Serviceability"></a>

## Serviceability
---

### Stopping the SDK

At any time, you can stop the connection to Rainbow by calling the API `disconnect`. This will stop all services. The only way to reconnect is to call the API `connect` again.

```objective-c
[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didLogout:) name:kLoginManagerDidLogoutSucceeded object:nil];
[[ServicesManager sharedInstance].loginManager disconnect];
[[ServicesManager sharedInstance].loginManager resetAllCredentials];
```