## Presence
---

### Change presence manually

The SDK for iOS allows to change the presence of the connected user by calling the following api:

```objective-c
[[ServicesManager sharedInstance].contactsManagerService changeMyPresence:[Presence presenceAvailable]];
```

The following Methods are supported:

| Presence constant | value | Meaning |
|------------------ | ----- | ------- |
| **`presenceAvailable`** | "online" | The connected user is seen as **available** |
| **`presenceDoNotDistrub`** | "dnd" | The connected user is seen as **do not disturb** |
| **`presenceAway`** | "away" | The connected user is seen as **away** |
| **`presenceExtendedAway`** | "invisible" | The connected user is connected but **seen as offline** |

