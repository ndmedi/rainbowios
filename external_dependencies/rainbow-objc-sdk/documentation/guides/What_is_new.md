## What's new
---

Welcome to the new release of the Rainbow SDK for iOS. There are a number of significant updates in this version that we hope you will like, some of the key highlights include:

### SDK for iOS 1.52.0 - Feb 2019
---

***Others Changes***
* Some bugs and crashes have been fixed in this version.

### SDK for iOS 1.51.0 - Jan 2019
---

***Others Changes***
* A tutorial to generate certificates for the push notification has been added.
* The SDK does not support anymore the architecture armv7s.
* Some bugs and crashes have been fixed in this version.

### SDK for iOS 1.50.0 - Dec 2018
---

***Others Changes***
* The attachment of a last message of a conversation is now correctly set when opening the application if the message has already been stored in the cache.
* If a message contains an attachment, its body is not set anymore to the filename.
* Some bugs and crashes have been fixed in this version.

### SDK for iOS 1.49.0 - Nov 2018
---

***API Changes***

* The new method `isBusyPresentation` has been added to the object `Contact`.
* The new getter `bodyInMarkdown` on a **Message** object has been added to allow you to manage the display if a message has a markdown content.

***Others Changes***

* Some bugs and crashs have been fixed in this version.

### SDK for iOS 1.48.0 - Nov 2018
---

***3-Release SDK Breaking Changes***

* None

***API Breaking Changes***

* None

***API Changes***

* The new method `notifyGuestUsers` has been added in the module **RoomsService** to send an invitation to a guest to join a room.
* The new method `cancelGuestUsers` has been added in the module **RoomsService** to cancel invitations sent to a guest to join a room.

***Others Changes***

* Some bugs / crashs have been fixed in this version

### SDK for iOS 1.47.0 - Oct 2018
---

***3-Release SDK Breaking Changes***

* Due to the data privacy improvements introduced in Rainbow 1.47 and as mentionned in the SDK iOS v1.0.14 release notes, your application MUST use at least the Rainbow SDK iOS v1.0.14 or a higher version to continue to work. All Rainbow SDK iOS versions prior to v1.0.14 are no more compliant with our Rainbow platform.

***API Breaking Changes***

* None

***API Changes***

* The new method `setAvatar` has been added in the module **MyUser** to change the connected user avatar.
* The new method `beginNewOutgoingCallWithPeer` with the parameter `andSubject` has been added in the module **RTCService** to make a call with a « reason of call ».

***Others Changes***

* Some bugs / crashs have been fixed in this version

### SDK for iOS 1.46.0 - Sept 2018
---

The versions of the Rainbow SDK for iOS are now aligned on the Rainbow iOS application. Therefore, we will release a new version at least one time per month and the naming of the major version is the same as the Rainbow IOS application, that is why we are now at the version 1.46.0 of the Rainbow SDK for iOS.

***3-Release SDK Breaking Changes***

* None

***API Breaking Changes***

* None

***API Changes***

* None

***Others Changes***

* A new [Legals page](/#/documentation/doc/sdk/ios/guides/Legals) has been added which includes disclaimer, software licence, FOSS list and status.
* The WebRTC stack has been updated to the version GoogleWebRTC-1.1.22973.
* Some bugs have been fixed in this version


### SDK for iOS 1.0.15 - Aug 2018
---

***3-Release SDK Breaking Changes***

* None

***API Breaking Changes***

* None

***API Changes***

* None

***Others Changes***

* Some bugs have been fixed in this version

### SDK for iOS 1.0.14 - Aug 2018
---

***3-Release SDK Breaking Changes***

* Due to data privacy improvements, Rainbow platform will introduce breaking changes in the way data associated to users are located around the world in one hand and the way users connect to the platform in other hand. Consequently, any SDK IOS prior to version 1.0.14 are entered deprecation period and will no more work once Rainbow platform 1.47 will be deployed on production (starting Sept, 30th)”. Before Sept’30, your application has to migrate to SDK IOS version 1.0.14 at least in order for your application to continue to work after this date.

***API Breaking Changes***

* None

***API Changes***

* None

***Others Changes***

* Fix: When a privilege (user, moderator, owner) was changed for a room participant, his status was set to unknown and the privilege was not updated accordingly.
* Fixing many issues and crashes

### SDK for iOS 1.0.13 - Jul 2018
---

***API Changes***

* Added `-(void)saveNotificationWithUserInfo:(NSDictionary *) userInfo` in `NotificationsManager`
* Added `-(void) cancelOutgoingCall` in `TelephonyService`
* The `RTCCall` notifications have been renamed, `RTCCallStatusRinging` is now `CallStatusRinging`, `RTCCallStatusConnecting` is now `CallStatusConnecting`, ...
* The `TelephonyService` notifications have been renamed, `kRTCServiceDidAddCallNotification` is now `kTelephonyServiceDidAddCallNotification`, `kRTCServiceDidUpdateCallNotification` is now  `kTelephonyServiceDidUpdateCallNotification`, ...


***Others Changes***

* Load roster cache at init of the SDK
* Fixing many issues and crashes

### SDK for iOS 1.0.12 - May 2018
---

***API Breaking Changes***

* iOS SDK have now a minmal deployement version set to **iOS 10**

***API Changes***

* The method `acceptInvitation` and `declineInvitation` now take in parameters a completion block, that give a feedback on the operation

***Others Changes***

* Fixing many issues and crashes


### SDK for iOS 1.0.11 - May 2018
---

**We are proud to publish our first public release of the Rainbow SDK for iOS.**

Thanks to this `SDK` you will be able to develop amazing applications for chatting or doing live audio and video communications.

**Some available features:**

- Instance messaging

- Contact management

- Presence

- Multimedia calls

Have a look into [Getting started](/#/documentation/doc/sdk/ios/guides/Getting_Started).
