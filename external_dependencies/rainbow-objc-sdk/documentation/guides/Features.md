## Features
---
Here is the list of features supported by the Rainbow-iOS-SDK


### Instant Messaging

> Send and receive One-to-One messages
>
> Message Delivery Receipts (received and read)
>
> Retrieving or creating a conversation from a contact
>
> Listening for a incoming message
>
> Use push notifications

### Contacts

> Get the list of contacts
>
> Get contact individually
>
> Managing contacts updates
>
> Displaying contact full information
> 
> Searching for a contact by name
>
> Adding the contact to the user network
 
> Removing the contact from the user network



### Presence

> Get the presence of contacts
>
> Set the user connected presence


### Calls

> Start audio call 
>
> Start video call

