## Development Kit

---

The Rainbow SDK for iOS contains:

- A native iOS library: This library has to be included in your application.

- Several demos applications to demonstrate how to use the SDK for iOS library.

- An API documentation that describes the available APIs (that you can call from your application) and that contains some tutorials explaining the fundamentals parts.

### iOS SDK library

---

There is no delivery of this file. See the [Getting Started](/#/documentation/doc/sdk/ios/guides/Getting_Started) guide to understand how to load it. 


### Samples

---

In order to simplify the SDK for iOS API development, some project or starter kit are available on Github.

| Content | Description |
| ---------------- | -------- |
| [Rainbow-iOS-SDK-Sample-Contacts](https://github.com/Rainbow-CPaaS/Rainbow-iOS-SDK-Samples/tree/master/Rainbow-iOS-SDK-Sample-Contacts) | demonstration of the Rainbow contact management API
| [Rainbow-iOS-SDK-Sample-IM](https://github.com/Rainbow-CPaaS/Rainbow-iOS-SDK-Samples/tree/master/Rainbow-iOS-SDK-Sample-IM) | demonstration of WebRTC calling API
| [Rainbow-iOS-SDK-Sample-WebRTC](https://github.com/Rainbow-CPaaS/Rainbow-iOS-SDK-Samples/tree/master/Rainbow-iOS-SDK-Sample-WebRTC) | demonstration of the Instant Messaging API
| [Rainbow-iOS-SDK-Sample-Swift](https://github.com/Rainbow-CPaaS/Rainbow-iOS-SDK-Samples/tree/master/Rainbow-iOS-SDK-Sample-Swift) | sample application in Swift language


_Last updated April, 17th 2018_
