#!/bin/bash

mkdir -p docs/
cat Rainbow/Rainbow.h | grep "import <Rainbow" | awk {'print $2'} | sed 's%<Rainbow/%%g' | sed 's%>%%g' > docs/list_import.txt
rm -rf docs/headers/
mkdir -p docs/headers/Rainbow
while read header_name; do 
	echo $header_name; 
	find . -name $header_name | xargs -I {} cp {} docs/headers/Rainbow/; 
done < docs/list_import.txt

# copy webrtc.h
mkdir -p docs/headers/WebRTC
webrtcpath=`find . -name WebRTC.framework -type d`
cp -rvp $webrtcpath/Headers/ docs/headers/WebRTC/

jazzy --hide-documentation-coverage
