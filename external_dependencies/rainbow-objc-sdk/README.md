# Rainbow SDK opensource list

Follow list of all opensource used in Rainbow iOS SDK.


Opensource name     			| Website          | Licence |Modified by ALE       | Version |
------------------------------|------------------|---------|--------------|--------------|
LinqToObjectiveC  				| <https://github.com/ColinEberhardt/LinqToObjectiveC.git> | [MIT](https://raw.githubusercontent.com/ColinEberhardt/LinqToObjectiveC/master/MIT-LICENSE.txt)        | YES (Forked) | 2.1.1
SocketRocket     				| <https://github.com/square/SocketRocket.git>  | [Apache2.0](https://raw.githubusercontent.com/square/SocketRocket/master/LICENSE)    | YES (Forked) | 0.6.0
XMPPFramework          			| <https://github.com/robbiehanson/XMPPFramework.git>    | BSD Style License  | YES (Forked) | 3.7.0
GCDAsyncSocket (included in XMPPFramework)	| <https://github.com/robbiehanson/CocoaAsyncSocket>  | Public Domain | NO | 7.4.1
CocoaLumberJack (included in XMPPFramework) | <https://github.com/CocoaLumberjack/CocoaLumberjack>  | [BSD](https://raw.githubusercontent.com/CocoaLumberjack/CocoaLumberjack/1.9.0/LICENSE.txt) | NO | 1.9.0
KissXML (included in XMPPFramework) 		| <https://github.com/robbiehanson/KissXML>  | [MIT](https://raw.githubusercontent.com/robbiehanson/KissXML/5.0.1/LICENSE.txt) | NO | 5.0
NSURLSession-SynchronousTask	| <https://github.com/floschliep/NSURLSession-SynchronousTask>  | [MIT](https://raw.githubusercontent.com/floschliep/NSURLSession-SynchronousTask/1.1/LICENSE.txt) | NO | 1.1
NSString+Emoji          | <https://github.com/nebulist/NSString-Emoji.git> | [MIT](https://raw.githubusercontent.com/nebulist/NSString-Emoji/master/LICENSE) | YES (Forked) | 0.3.0
NSDate-Extensions           | <https://github.com/khendry/NSDate-Extensions.git> | BSD | YES (Forked) | 1.0
PINCache                    | <https://github.com/pinterest/PINCache.git> | [Apache2.0](https://raw.githubusercontent.com/pinterest/PINCache/master/LICENSE.txt) | NO | 3.0.1-beta6
OrderedDictionary           | <https://github.com/nicklockwood/OrderedDictionary.git> | [MIT](https://raw.githubusercontent.com/nicklockwood/OrderedDictionary/master/LICENCE.md) | NO | 1.4
UIUserNotificationSettings-Extension | <https://github.com/alexruperez/UIUserNotificationSettings-Extension> | [MIT](https://raw.githubusercontent.com/alexruperez/UIUserNotificationSettings-Extension/master/LICENSE) | YES (Forked) | 0.1.3
