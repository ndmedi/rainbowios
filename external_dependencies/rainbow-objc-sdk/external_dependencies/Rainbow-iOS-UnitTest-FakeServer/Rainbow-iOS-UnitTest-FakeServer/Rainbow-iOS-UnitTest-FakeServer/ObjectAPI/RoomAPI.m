/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "RoomAPI.h"
#import "ObjectAPIStore.h"

@implementation RoomAPI

-(instancetype) init {
    if (self = [super init]) {
        self.ID = [self randomString];
        self.users = [NSMutableArray new];
    }
    return self;
}

-(NSDictionary *) addUser:(NSString *) userID privilege:(NSString *) privilege status:(NSString *) status {
    // Find the jid.
    NSString *jid = nil;
    for (ContactAPI *contact in [ObjectAPIStore sharedObject].contacts) {
        if ([contact.ID isEqualToString:userID]) {
            jid = contact.jid;
            break;
        }
    }
    NSMutableDictionary *user = [NSMutableDictionary new];
    [user setObject:userID forKey:@"userId"];
    [user setObject:privilege forKey:@"privilege"];
    [user setObject:@"2016-12-10T10:11:03.000Z" forKey:@"additionDate"];
    [user setObject:jid forKey:@"jid_im"];
    [user setObject:status forKey:@"status"];
    [self.users addObject:user];
    
    return user;
}

-(NSDictionary *) representation {
    return @{
             @"id": self.ID,
             @"jid": self.jid,
             @"name": self.name,
             @"topic": self.topic,
             @"visibility": self.visibility,
             @"creationDate": self.creationDate,
             @"creator": self.creator,
             @"users": self.users
    };
}

@end
