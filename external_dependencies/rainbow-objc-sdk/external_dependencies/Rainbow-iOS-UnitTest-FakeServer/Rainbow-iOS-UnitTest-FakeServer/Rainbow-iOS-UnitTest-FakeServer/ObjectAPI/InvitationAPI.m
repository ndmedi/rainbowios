/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "InvitationAPI.h"

@implementation InvitationAPI

-(instancetype) init {
    if (self = [super init]) {
        self.ID = [self randomString];
    }
    return self;
}

-(NSDictionary *) representation {
    
    NSMutableDictionary *res = [NSMutableDictionary new];
    
    [res addEntriesFromDictionary:@{
                                    @"id": self.ID,
                                    //@"invitedUserId": self.invitedUserId ? self.invitedUserId : @"",
                                    //@"invitedUserEmail": self.invitedUserEmail ? self.invitedUserEmail : @"",
                                    //@"invitingUserId": self.invitingUserId ? self.invitingUserId : @"",
                                    //@"invitingUserEmail": self.invitingUserEmail ? self.invitingUserEmail : @"",
                                    @"invitingDate": self.invitingDate,
                                    @"status": self.status
                                    }];
    
    if ([self.invitedUserId length]) {
        [res setObject:self.invitedUserId forKey:@"invitedUserId"];
    }
    if ([self.invitedUserEmail length]) {
        [res setObject:self.invitedUserEmail forKey:@"invitedUserEmail"];
    }
    if ([self.invitingUserId length]) {
        [res setObject:self.invitingUserId forKey:@"invitingUserId"];
    }
    if ([self.invitingUserEmail length]) {
        [res setObject:self.invitingUserEmail forKey:@"invitingUserEmail"];
    }
    
    return res;
}

@end
