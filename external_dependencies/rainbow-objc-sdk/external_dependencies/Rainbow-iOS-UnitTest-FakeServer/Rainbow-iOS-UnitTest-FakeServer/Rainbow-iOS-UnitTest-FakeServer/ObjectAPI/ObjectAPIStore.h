/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import <Foundation/Foundation.h>
#import "SettingsAPI.h"
#import "ConversationAPI.h"
#import "ContactAPI.h"
#import "InvitationAPI.h"
#import "MessageAPI.h"
#import "RoomAPI.h"

@interface ObjectAPIStore : NSObject

+(instancetype) sharedObject;

-(void) removeAllObjects;

@property (nonatomic, strong) SettingsAPI *settings;

@property (nonatomic, strong) NSMutableArray<ConversationAPI *> *conversations;
-(NSArray<NSDictionary *> *) conversationsDictionary;

@property (nonatomic, strong) NSMutableArray<ContactAPI *> *contacts;
-(NSArray<NSDictionary *> *) contactsDictionary;

@property (nonatomic, strong) NSMutableArray<InvitationAPI *> *invitations;
-(NSArray<NSDictionary *> *) sentInvitationsDictionary;
-(NSArray<NSDictionary *> *) receivedInvitationsDictionary;

@property (nonatomic, strong) NSMutableArray<MessageAPI *> *messages;

@property (nonatomic, strong) NSMutableArray<RoomAPI *> *rooms;
-(NSArray<NSDictionary *> *) roomsDictionary;

@end
