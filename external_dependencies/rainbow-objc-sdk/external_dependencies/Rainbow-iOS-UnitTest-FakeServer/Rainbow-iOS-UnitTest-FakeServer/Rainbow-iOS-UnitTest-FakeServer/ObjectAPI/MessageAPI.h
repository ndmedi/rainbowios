/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "ObjectAPI.h"

typedef NS_ENUM(NSInteger, ObjectAPIMessageType) {
    /**
     *  Type unknown
     */
    ObjectAPIMessageTypeUnknown = 0,
    /**
     *  Type chat
     */
    ObjectAPIMessageTypeChat,
    /**
     *  Type Group chat
     */
    ObjectAPIMessageTypeGroupChat,
    /**
     *  Type WebRTC
     */
    ObjectAPIMessageTypeWebRTC,
    /**
     *  Type WebRTC Start
     */
    ObjectAPIMessageTypeWebRTCStart,
    /**
     *  Type WebRTC Ringing
     */
    ObjectAPIMessageTypeWebRTCRinging,
    /**
     *  Type File Transfer
     */
    ObjectAPIMessageTypeFileTransfer,
    /**
     *  Type Group chat event
     */
    ObjectAPIMessageTypeGroupChatEvent,
    /*
     *  Management message
     */
    ObjectAPIMessageTypeManagement
};

@interface MessageAPI : ObjectAPI

/* Easy to copy/paste :

*/
@property (nonatomic) ObjectAPIMessageType messageType;
@property (nonatomic, strong) NSString *ID;
@property (nonatomic, strong) NSString *from_jid;
@property (nonatomic, strong) NSString *to_jid;
@property (nonatomic, strong) NSString *body;
@property (nonatomic, strong) NSString *date;
@property (nonatomic        ) BOOL received;
@property (nonatomic, strong) NSString *receivedDate;
@property (nonatomic        ) BOOL read;
@property (nonatomic, strong) NSString *readDate;
@property (nonatomic) BOOL isArchived;
@property (nonatomic, strong) NSString *archiveDate;
@property (nonatomic) BOOL needAck;
@property (nonatomic) BOOL isPush;
@property (nonatomic) NSString *delayDate;


@property (nonatomic, strong) NSString *conversationID;
@property (nonatomic, strong) NSString *action;

+(NSString *) stringForMessageType:(ObjectAPIMessageType) messageType;
@end
