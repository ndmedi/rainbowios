/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "ObjectAPIStore.h"


@implementation ObjectAPIStore

+(instancetype) sharedObject {
    static ObjectAPIStore *sharedObj = nil;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        sharedObj = [ObjectAPIStore new];
    });
    return sharedObj;
}

-(instancetype) init {
    if (self = [super init]) {
        // Default settings
        self.settings = [SettingsAPI new];
        self.settings.presence = @"online";
        self.settings.displayNameOrderFirstNameFirst = NO;
        
        self.conversations = [NSMutableArray new];
        self.contacts = [NSMutableArray new];
        self.invitations = [NSMutableArray new];
        self.messages = [NSMutableArray new];
        self.rooms = [NSMutableArray new];
    }
    return self;
}

-(void) removeAllObjects {
    [self.conversations removeAllObjects];
    [self.contacts removeAllObjects];
    [self.invitations removeAllObjects];
    [self.messages removeAllObjects];
    [self.rooms removeAllObjects];
}

-(NSArray<NSDictionary *> *) conversationsDictionary {
    NSMutableArray *array = [NSMutableArray new];
    for (ConversationAPI *conversation in self.conversations) {
        [array addObject:conversation.representation];
    }
    return array;
}

-(NSArray<NSDictionary *> *) contactsDictionary {
    NSMutableArray *array = [NSMutableArray new];
    for (ContactAPI *contact in self.contacts) {
        [array addObject:contact.representation];
    }
    return array;
}

-(NSArray<NSDictionary *> *) sentInvitationsDictionary {
    NSMutableArray *array = [NSMutableArray new];
    for (InvitationAPI *invitation in self.invitations) {
        if (!invitation.received) {
            [array addObject:invitation.representation];
        }
    }
    return array;
}

-(NSArray<NSDictionary *> *) receivedInvitationsDictionary {
    NSMutableArray *array = [NSMutableArray new];
    for (InvitationAPI *invitation in self.invitations) {
        if (invitation.received) {
            [array addObject:invitation.representation];
        }
    }
    return array;
}

-(NSArray<NSDictionary *> *) roomsDictionary {
    NSMutableArray *array = [NSMutableArray new];
    for (RoomAPI *room in self.rooms) {
        [array addObject:room.representation];
    }
    return array;
}

@end
