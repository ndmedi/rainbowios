/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "MessageAPI.h"

@implementation MessageAPI

-(instancetype) init {
    if (self = [super init]) {
        self.ID = [self randomString];
        self.messageType = ObjectAPIMessageTypeChat;
    }
    return self;
}

-(NSDictionary *) representation {
    return @{
             @"id": self.ID,
             @"from_jid": self.from_jid,
             @"to_jid": self.to_jid,
             @"body": self.body,
             @"received": @(self.received),
             @"read": @(self.read)
    };
}

+(NSString *) stringForMessageType:(ObjectAPIMessageType) messageType {
    switch (messageType) {
        case ObjectAPIMessageTypeChat:
            return @"chat";
            break;
        case ObjectAPIMessageTypeGroupChat:
            return @"groupchat";
            break;
        case ObjectAPIMessageTypeWebRTC:
            return @"webrtc";
            break;
        case ObjectAPIMessageTypeManagement:
            return @"management";
            break;
        case ObjectAPIMessageTypeWebRTCStart:
            return @"webrtc-start";
            break;
        case ObjectAPIMessageTypeFileTransfer:
            return @"file-transfer";
            break;
        case ObjectAPIMessageTypeWebRTCRinging:
            return @"webrtc-ringing";
            break;
        case ObjectAPIMessageTypeGroupChatEvent:
            return @"groupchat-event";
            break;
        case ObjectAPIMessageTypeUnknown:
        default:
            return @"unknown";
            break;
    }
}
@end
