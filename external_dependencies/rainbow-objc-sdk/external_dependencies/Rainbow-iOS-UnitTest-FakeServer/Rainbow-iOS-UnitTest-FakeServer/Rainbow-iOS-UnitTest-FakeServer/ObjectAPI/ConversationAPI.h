/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "ObjectAPI.h"

@interface ConversationAPI : ObjectAPI

/* Easy to copy/paste :

conversation.ID = ;
conversation.peerID = ;
conversation.type = ;
conversation.jid = ;
conversation.lastMessage = ;
conversation.lastMessageDate = ;
conversation.unreceived = ;
conversation.unread = ;
conversation.mute = ;

*/

@property (nonatomic, strong) NSString *ID;
@property (nonatomic, strong) NSString *peerID;
@property (nonatomic, strong) NSString *type; // user | room
@property (nonatomic, strong) NSString *jid;
@property (nonatomic, strong) NSString *lastMessage;
@property (nonatomic, strong) NSString *lastMessageDate;
@property (nonatomic, strong) NSNumber *unreceived;
@property (nonatomic, strong) NSNumber *unread;
@property (nonatomic        ) BOOL mute;

@end
