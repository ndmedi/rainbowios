/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "ConversationAPI.h"
#import "ObjectAPIStore.h"


@implementation ConversationAPI

-(instancetype) init {
    if (self = [super init]) {
        self.ID = [self randomString];
    }
    return self;
}

-(NSDictionary *) representation {
    
    NSMutableDictionary *res = [NSMutableDictionary new];
    
    [res addEntriesFromDictionary:@{
                                    @"id": self.ID,
                                    @"peerId": self.peerID,
                                    @"type": self.type,
                                    @"jid_im": self.jid,
                                    @"lastMessageText": self.lastMessage,
                                    @"lastMessageDate": self.lastMessageDate,
                                    @"unreceivedMessageNumber": self.unreceived ? self.unreceived : @(0),
                                    @"unreadMessageNumber": self.unread ? self.unread : @(0),
                                    @"mute" : self.mute ? @YES : @NO
                                    }];
    
    if ([self.type isEqualToString:@"user"]) {
        for (ContactAPI *contact in [ObjectAPIStore sharedObject].contacts) {
            if ([contact.ID isEqualToString:self.peerID]) {
                
                [res addEntriesFromDictionary:@{
                                                @"loginEmail": contact.loginEmail,
                                                @"firstName": contact.firstname,
                                                @"lastName": contact.lastname,
                                                @"displayName": [NSString stringWithFormat:@"%@ %@", contact.firstname, contact.lastname]
                                                }];
                
                break;
            }
        }
    }
    
    if ([self.type isEqualToString:@"room"]) {
        for (RoomAPI *room in [ObjectAPIStore sharedObject].rooms) {
            if ([room.ID isEqualToString:self.peerID]) {
                
                [res addEntriesFromDictionary:@{
                                                @"name": room.name,
                                                @"topic": room.topic
                                                }];
                
                break;
            }
        }
    }
    return res;
}

@end
