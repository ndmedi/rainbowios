/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "LoginManager.h"
#import "XMPPService.h"
#import "MyUser.h"
#import "ApiUrlManagerService.h"
#import "Reachability.h"

/** Notification sent before starting login */
FOUNDATION_EXPORT NSString *const kLoginManagerInternalWillLogin;
/** Internal notification for xmpp disconnetion */
FOUNDATION_EXPORT NSString *const kLoginManagerDidDisconnect;
// Internal notification for lost connection
FOUNDATION_EXPORT NSString *const kLoginManagerInternalDidLostConnection;
// Internal notification for did login
FOUNDATION_EXPORT NSString *const kLoginManagerInternalDidLoginSucceeded;

@interface LoginManager ()
@property (nonatomic, readwrite) BOOL loginDidSucceed;
@property (nonatomic, readwrite) BOOL autoLogin;
@property (nonatomic, strong) Server *server;
@property (nonatomic, readwrite, strong) NSNumber *serverApiVersion;
@property (nonatomic, weak) Reachability *reachability;

-(instancetype) initWithDownloadManager:(DownloadManager *) downloadManager withXmppService:(XMPPService *) xmppService myUser:(MyUser *) myUser apiUrlManagerService:(ApiUrlManagerService *) apiUrlManagerService contactsManagerService:(ContactsManagerService *) contactsManagerService;
-(BOOL) loginToServer:(Server *) server;
-(BOOL) connectToXMPPWithUserName:(NSString *) username password:(NSString *) password;
@end
