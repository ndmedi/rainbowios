/*
 * Rainbow
 *
 * Copyright (c) 2018, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "XMPPService.h"
#import "MessageInternal.h"
#import "XMPPIQ.h"
#import "XMPPJID.h"
#import "XMPPStream.h"
#import "ConversationsSearchHelper+Internal.h"
#import "XMPPServiceProtocol.h"
#import "ServicesManager.h"
#import "Conversation+Internal.h"

NSString *const kSearchDidFoundResultsInConversations = @"didFoundResultsInConversations";
NSString *const kSearchDidFoundResultsInAConversation = @"didFoundResultsInAConversation";
NSString *const kSearchDidFoundMessagesInAConversation = @"didFoundMessagesInAConversation";

@implementation SearchMessageConversationResult
-(instancetype) initWithConversation:(Conversation*)conversation {
    self = [super init];
    if(self){
        self.peer = conversation.peer;
        self.conversationId = conversation.conversationId;
        self.unreadMessagesCount = conversation.unreadMessagesCount;
        self.lastMessage = conversation.lastMessage;
        self.creationDate = conversation.creationDate;
        self.status = conversation.status;
        
        self.occurences = 0;
    }
    return self;
}
-(instancetype) initWithPeer:(Peer*)peer {
    self = [super init];
    if(self){
        self.peer = peer;
        self.conversationId = nil;
        self.unreadMessagesCount = 0;
        self.lastMessage = nil;
        self.creationDate = nil;
        self.status = ConversationStatusActive;
        
        self.occurences = 0;
    }
    return self;
}

- (BOOL)isEqual:(SearchMessageConversationResult*)object {
    if([object.peer isEqual:self.peer])
        return YES;
    return NO;
}

@end

@interface ConversationsSearchHelper() <XMPPServiceConversationsSearchDelegate>
@property (nonatomic, strong) XMPPService *xmppService;
@property (nonatomic, strong) NSMutableArray<SearchMessageConversationResult *> *searchResult;
@property (nonatomic, strong) NSMutableArray<Message *> *messagesResult;
@property (nonatomic, strong) NSMutableArray<Message *> *occurencesResult;
@property (nonatomic        ) int searchResultCount;
@property (nonatomic, strong) NSString *currentRequestUID;
@property (nonatomic, strong) NSString *currentTextToSearch;
@property (nonatomic        ) ConversationSearchHelperQueryType currentRequestType;
@property (nonatomic, strong) Peer *currentRequestedPeer;
@end

@implementation ConversationsSearchHelper

-(instancetype)initWithXMPPService:(XMPPService *) xmppService {
    self = [super init];
    if(self){
        _xmppService = xmppService;
        _xmppService.conversationsSearchDelegate = self;
        
        _searchResult = [NSMutableArray<SearchMessageConversationResult*> array];
        _messagesResult = [NSMutableArray<Message*> array];
        _occurencesResult = [NSMutableArray<Message*> array];
        
        [self clearSearchResults];
    }
    return self;
}

-(void) dealloc {
    [self clearSearchResults];
    
    _xmppService.conversationsSearchDelegate = nil;
    _xmppService = nil;
}

// Clean previous results
-(void) clearSearchResults {
    @synchronized(_searchResult) {
        [_searchResult removeAllObjects];
        [_messagesResult removeAllObjects];
        [_occurencesResult removeAllObjects];
        _searchResultCount = 0;
        _currentRequestUID = nil;
        _currentTextToSearch = nil;
        _currentRequestType = ConversationSearchHelperQueryTypeNone;
        _currentRequestedPeer = nil;
    }
}

/**
Search all conversations that contains a message with at least one textToSearch str occurence
*/
-(void) searchConversationsWithText:(NSString *) textToSearch {
    
    // Clean the previous results
    [self clearSearchResults];
    
    // Create a global UID for this search
    NSString *uniqueID = [XMPPStream generateUUID];
    _currentRequestUID = uniqueID;
    _currentRequestType = ConversationSearchHelperQueryTypeGlobal;
    _currentTextToSearch = [NSString stringWithString:textToSearch];
    
    // Query IQ for Peer to Peer conversations
    XMPPIQ *iq = [[XMPPIQ alloc] initWithType:@"get" elementID:uniqueID child:[self buildSearchQuery: textToSearch peer:nil queryID:uniqueID max:100 before:0 after:0]];
    [iq setXmlns:@"jabber:client"];

    // P2P: send search query for Peer to Peer conversations
    [_xmppService sendSearchMessageQueryIQ:iq];
    
//    [[ServicesManager sharedInstance].conversationsManagerService.conversations enumerateObjectsUsingBlock:^(Conversation * _Nonnull conversation, NSUInteger idx, BOOL * _Nonnull stop) {
//        if(![conversation.peer isKindOfClass:[Room class]]) {
//            XMPPIQ *iq = [[XMPPIQ alloc] initWithType:@"get" elementID:uniqueID child:[self buildSearchQuery: textToSearch peer:conversation.peer queryID:uniqueID max:0 before:0 after:0]];
//            [iq setXmlns:@"jabber:client"];
//            [iq addAttributeWithName:@"to" stringValue:conversation.peer.jid];
//            [_xmppService sendSearchMessageQueryIQ:iq];
//        }
//    }];
    
    // ROOMS: send search query for EACH rooms
    [[ServicesManager sharedInstance].roomsService.roomsLoadedFromCache enumerateObjectsUsingBlock:^(Room * _Nonnull room, NSUInteger idx, BOOL * _Nonnull stop) {
        
        XMPPIQ *iq = [[XMPPIQ alloc] initWithType:@"get" elementID:uniqueID child:[self buildSearchQuery: textToSearch peer:room queryID:uniqueID max:0 before:0 after:0]];
        [iq setXmlns:@"jabber:client"];
        [iq addAttributeWithName:@"to" stringValue:room.jid];
        [_xmppService sendSearchMessageQueryIQ:iq];
        
    }];
    
}

/**
 Search within a conversation messages that contains the textToSearch string
 
 <iq xmlns="jabber:client" type="get" id="310E9D2E-54F6-4F7F-9AF5-89BAB05FC4D4">
    <query xmlns="urn:xmpp:mam:tmp" queryid="310E9D2E-54F6-4F7F-9AF5-89BAB05FC4D4">
        <x xmlns="jabber:x:data" type="form">
            <field var="FORM_TYPE" type="hidden"><value>urn:xmpp:mam:1</value></field>
            <field var="with" type="text-single"><value>bff1d14ea4054e85aae86066696f2f1d@openrainbow.com</value></field>
            <field var="withtext" type="text-single"><value>mercredi</value></field>
        </x>
        <set xmlns="http://jabber.org/protocol/rsm"><max>10</max></set>
    </query>
 </iq>
 */
-(void) searchInConversationWithPeer:(Peer *)peer textToSearch:(NSString *) textToSearch {
    
    // Clean the previous results
    [self clearSearchResults];
    
    // Create a global UID for this search
    NSString *uniqueID = [XMPPStream generateUUID];
    _currentRequestUID = uniqueID;
    _currentRequestType = ConversationSearchHelperQueryTypeForOccurences;
    _currentTextToSearch = [NSString stringWithString:textToSearch];
    _currentRequestedPeer = peer;
    
    // Query IQ for Peer to Peer conversations
    XMPPIQ *iq = [[XMPPIQ alloc] initWithType:@"get" elementID:uniqueID child:[self buildSearchQuery: textToSearch peer:peer queryID:uniqueID max:1000 before:0 after:0]];
    [iq setXmlns:@"jabber:client"];
    [iq addAttributeWithName:@"to" stringValue:peer.jid];
    
    // P2P: send search query in the specified conversation
    [_xmppService sendSearchMessageQueryIQ:iq];
    
}

-(void) searchMessagesWithPeer:(Peer*)peer beforeAndAfter:(NSUInteger)timestamp {
    
    // Clean the previous results
    [self clearSearchResults];
    
    // Create a global UID for this search
    NSString *uniqueID = [XMPPStream generateUUID];
    _currentRequestUID = uniqueID;
    _currentRequestType = ConversationSearchHelperQueryTypeSingle;
    _currentRequestedPeer = peer;
    
    // Query IQ - BEFORE
    XMPPIQ *iq1 = [[XMPPIQ alloc] initWithType:@"set" elementID:uniqueID child:[self buildSearchQuery:nil peer:peer queryID:uniqueID max:10 before: timestamp+1 after:0]];
    [iq1 setXmlns:@"jabber:client"];
    [iq1 addAttributeWithName:@"to" stringValue:peer.jid];
    
    // P2P: send search query in the specified conversation
    [_xmppService sendSearchMessageQueryIQ:iq1];
    
    // Query IQ - AFTER
    XMPPIQ *iq2 = [[XMPPIQ alloc] initWithType:@"set" elementID:uniqueID child:[self buildSearchQuery:nil peer:peer queryID:uniqueID max:10 before:0  after:timestamp]];
    [iq2 setXmlns:@"jabber:client"];
    [iq2 addAttributeWithName:@"to" stringValue:peer.jid];
    
    // P2P: send search query in the specified conversation
    [_xmppService sendSearchMessageQueryIQ:iq2];
}

/*
 <iq to='room_07ef805225004e7f92f3141624226166@muc.openrainbow.com' id='9b3ecd68-c963-4fe3-9506-a4101bc248fb' type='get' xmlns='jabber:client'>
 -----------
     <query queryid='ee6340af-6b25-49de-a66f-6e6c62e23aa1' xmlns='urn:xmpp:mam:tmp'>
         <set xmlns='http://jabber.org/protocol/rsm'>
            <max>5</max>
            <before/>
            <after/>
         </set>
         <x xmlns='jabber:x:data' type='form'>
            <field var='FORM_TYPE' type='hidden'>
                <value>urn:xmpp:mam:1</value>
            </field>
            <field var='with' type='text-single'>
                <value>bff1d14ea4089e85aae86266688f2f04@openrainbow.com</value>
            </field>
             <field var='withtext' type='text-single'>
                <value>itsbousin</value>
             </field>
         </x>
     </query>
 ----------
 </iq>
 */

-(NSXMLElement *) buildSearchQuery:(NSString *)text peer:(Peer *)peer queryID:(NSString *)uniqueID max:(int)max before:(NSUInteger)beforeTimestamp after:(NSUInteger)afterTimestamp {
    
    // START QUERY
    NSXMLElement *query = [NSXMLElement elementWithName:@"query" xmlns:@"urn:xmpp:mam:tmp"];
    [query addAttributeWithName:@"queryid" stringValue:uniqueID];
    
    NSXMLElement *set = [NSXMLElement elementWithName:@"set" xmlns:@"http://jabber.org/protocol/rsm"];
    [set addChild: [NSXMLElement elementWithName:@"max" numberValue:[NSNumber numberWithInt:max]]];
    
    // FORM elements
    NSXMLElement *x = [NSXMLElement elementWithName:@"x" xmlns:@"jabber:x:data"];

    NSXMLElement *field1 = [NSXMLElement elementWithName:@"field"];
    [field1 addAttributeWithName:@"var" stringValue:@"FORM_TYPE"];
    [field1 addAttributeWithName:@"type" stringValue:@"hidden"];
    [field1 addChild: [NSXMLElement elementWithName:@"value" stringValue:@"urn:xmpp:mam:1"]];
    [x addChild:field1];
    
    NSXMLElement *field2 = [NSXMLElement elementWithName:@"field"];
    [field2 addAttributeWithName:@"var" stringValue:@"with"];
    
    if(text) {
        [x addAttributeWithName:@"type" stringValue:@"form"];
        
        if(peer) {
            [field2 addAttributeWithName:@"type" stringValue:@"text-single"];
            if(_currentRequestType == ConversationSearchHelperQueryTypeGlobal ) {
                if([peer isKindOfClass:[Room class]])
                    [field2 addChild: [NSXMLElement elementWithName:@"value" stringValue: [ServicesManager sharedInstance].myUser.contact.jid]];
                else
                    [field2 addChild: [NSXMLElement elementWithName:@"value" stringValue: peer.jid]];
            }
            else if(_currentRequestType == ConversationSearchHelperQueryTypeForOccurences || _currentRequestType == ConversationSearchHelperQueryTypeSingle ) {
                if([peer isKindOfClass:[Room class]])
                    [field2 addChild: [NSXMLElement elementWithName:@"value" stringValue: [ServicesManager sharedInstance].myUser.contact.jid]];
                else
                    [field2 addChild: [NSXMLElement elementWithName:@"value" stringValue: peer.jid]];
            }
            
            [x addChild:field2];
        }
        
        NSXMLElement *field3 = [NSXMLElement elementWithName:@"field"];
        [field3 addAttributeWithName:@"var" stringValue:@"withtext"];
        [field3 addAttributeWithName:@"type" stringValue:@"text-single"];
        [field3 addChild: [NSXMLElement elementWithName:@"value" stringValue:text]];
        [x addChild:field3];
    }
    
    else {
        [query setXmlns:@"urn:xmpp:mam:1"];
        [x addAttributeWithName:@"type" stringValue:@"submit"];
        
        if(beforeTimestamp > 0)
            [set addChild: [NSXMLElement elementWithName:@"before" stringValue: [NSString stringWithFormat:@"%lu", (long)beforeTimestamp]]];
        if(afterTimestamp > 0)
            [set addChild: [NSXMLElement elementWithName:@"after" stringValue: [NSString stringWithFormat:@"%lu", (long)afterTimestamp]]];
        
        if(_currentRequestType == ConversationSearchHelperQueryTypeForOccurences || _currentRequestType == ConversationSearchHelperQueryTypeSingle ) {
            if([peer isKindOfClass:[Room class]])
                [field2 addChild: [NSXMLElement elementWithName:@"value" stringValue:[ServicesManager sharedInstance].myUser.contact.jid]];
            else
                [field2 addChild: [NSXMLElement elementWithName:@"value" stringValue:peer.jid]];

            [x addChild:field2];
        }
    }
    
    [query addChild:x];
    [query addChild:set];
    // END QUERY
    
    return query;
}

-(void) addResultByJid:(NSString *) jid withOccurrences:(int)count {
    if([jid isEqualToString:[ServicesManager sharedInstance].myUser.contact.jid]) {
        NSLog(@"ConversationsSearchHelper::addResultByJid - Peer with myUser jid skipped");
        return;
    }
    
    SearchMessageConversationResult *result;
    
    Conversation *conversation = [[ServicesManager sharedInstance].conversationsManagerService getConversationWithPeerJID:jid];
    
    if(conversation) {
        result = [[SearchMessageConversationResult alloc] initWithConversation:conversation];
    } else {
        Peer *peer;
        
        if([jid containsString:@"room_"])
            peer = (Peer *)[[ServicesManager sharedInstance].roomsService getRoomByJid:jid];
        else
            peer = (Peer *)[[ServicesManager sharedInstance].contactsManagerService getContactWithJid:jid];

        if(peer)
            result = [[SearchMessageConversationResult alloc] initWithPeer:peer];
    }
    
    if(result) {
        @synchronized(_searchResult) {
            if(![_searchResult containsObject:result]) {
                if(count > 0)
                    result.occurences = count;
                else
                    result.occurences = 1;
                
                [_searchResult addObject:result];
            } else {
                NSUInteger resultIndex = [_searchResult indexOfObject:result];
                
                if(count > 0)
                    ([_searchResult objectAtIndex:resultIndex]).occurences = count;
                else
                    ([_searchResult objectAtIndex:resultIndex]).occurences++;
            }
            
            if(count > 0)
                _searchResultCount += count;
            else
                _searchResultCount++;
        }
        
        // Notify for added result object
        [[NSNotificationCenter defaultCenter] postNotificationName:kSearchDidFoundResultsInConversations object:@{@"results":[_searchResult copy], @"count":[NSNumber numberWithInteger:_searchResultCount]} ];
    }
    
    else {
        NSLog(@"ConversationsSearchHelper::addResultByJid - Peer with jid %@ NOT FOUND", jid);
    }
}

-(void) addResultByJid:(NSString *) jid withOccurrences:(int)count firstOccurence:(NSUInteger)first lastOccurence:(NSUInteger)last {
    [[NSNotificationCenter defaultCenter] postNotificationName:kSearchDidFoundResultsInAConversation object:@{@"jid":jid, @"string": _currentTextToSearch, @"count":[NSNumber numberWithInteger:count], @"firstOccurence": [NSNumber numberWithUnsignedInteger:first], @"lastOccurence": [NSNumber numberWithUnsignedInteger:last]} ];
}

-(void) addResultMessage:(Message *) message {
    
    if(_currentRequestType == ConversationSearchHelperQueryTypeSingle) {
        @synchronized(_messagesResult) {
            
            if(![_messagesResult containsObject:message]) {
                [_messagesResult insertObject:message atIndex:0]; // the message timeline is reversed
        
                [[NSNotificationCenter defaultCenter] postNotificationName:kSearchDidFoundMessagesInAConversation object:@{@"jid":_currentRequestedPeer.jid, @"messages":[_messagesResult copy]} ];
            }
        }
    }
    
    else if(_currentRequestType == ConversationSearchHelperQueryTypeForOccurences) {
        @synchronized(_occurencesResult) {
            
            if(![_occurencesResult containsObject:message]) {
                [_occurencesResult insertObject:message atIndex:0]; // the message timeline is reversed
                
                [[NSNotificationCenter defaultCenter] postNotificationName:kSearchDidFoundMessagesInAConversation object:@{@"jid":_currentRequestedPeer.jid, @"occurences":[_occurencesResult copy]} ];
            }
        }
    }
}


#pragma mark - XMPPService delegate

- (void)xmppService:(XMPPService *)service didReceiveResultIQ:(XMPPIQ *)resultIQ {
    if(![resultIQ.elementID isEqualToString:_currentRequestUID])
        return; // Reject all unwanted received messages for the current request
    
    NSXMLElement *query = [resultIQ elementForName:@"query" xmlns:@"urn:xmpp:mam:tmp"];
    NSXMLElement *set = [query elementForName:@"set" xmlns:@"http://jabber.org/protocol/rsm"];
    
    if(set) {
        NSXMLElement *count = [set elementForName:@"count"];
        int countValue = [count stringValueAsInt];
        
        if(countValue > 0) {
            NSString *jid = [XMPPJID jidWithString:resultIQ.fromStr].bare;
            
            NSXMLElement *first = [set elementForName:@"first"];
            NSUInteger firstValue = [first stringValueAsNSUInteger];
            NSXMLElement *last = [set elementForName:@"last"];
            NSUInteger lastValue = [last stringValueAsNSUInteger];
            
            if([jid containsString:@"room_"]) {
                NSLog(@"ConversationsSearchHelper::didReceiveResultIQ - ROOM - jid: %@ count: %d first: %lu last: %lu", jid, countValue, (unsigned long)firstValue, (unsigned long)lastValue);
                
                if(_currentRequestType == ConversationSearchHelperQueryTypeGlobal)
                    [self addResultByJid:jid withOccurrences:countValue];
                else if(_currentRequestType == ConversationSearchHelperQueryTypeForOccurences)
                    [self addResultByJid:jid withOccurrences:countValue firstOccurence:firstValue lastOccurence:lastValue];
                
            } else {
                NSLog(@"ConversationsSearchHelper::didReceiveResultIQ - P2P - jid: %@ count: %d first: %lu last: %lu", jid, countValue, (unsigned long)firstValue, (unsigned long)lastValue);
                
                if(_currentRequestType == ConversationSearchHelperQueryTypeGlobal)
                    [self addResultByJid:jid withOccurrences:countValue];
                else if(_currentRequestType == ConversationSearchHelperQueryTypeForOccurences)
                    [self addResultByJid:jid withOccurrences:countValue firstOccurence:firstValue lastOccurence:lastValue];
            }
        }
        
        else {
            // NO RESULTS - Send a ZERO COUNT notification to UI
            [[NSNotificationCenter defaultCenter] postNotificationName:kSearchDidFoundResultsInAConversation object:@{@"results":@[], @"count":[NSNumber numberWithInteger:0]} ];
        }
    }
}

- (void)xmppService:(XMPPService *)service didReceiveResultMessage:(XMPPMessage *)resultMessage {
    NSXMLElement *result = [resultMessage elementForName:@"result"];
    
    if(result) {
        NSString *queryID = [result attributeStringValueForName:@"queryid"];
        
        if(![queryID isEqualToString:_currentRequestUID])
            return; // Reject all unwanted received messages for the current request

        NSXMLElement *forwarded = [result elementForName:@"forwarded" xmlnsPrefix:@"urn:xmpp:forward:0"];
        
        if(forwarded) {
            NSXMLElement *message = [forwarded elementForName:@"message"];
            if(message) {
                
                NSString *messageType = [message attributeStringValueForName:@"type"];
                NSString *myJIDStr = [ServicesManager sharedInstance].myUser.contact.jid;
                NSString *to = [message attributeStringValueForName:@"to"];
                XMPPJID *toXMPPJID = ([to length] > 0) ? [XMPPJID jidWithString:to] : nil;
                NSString *from = [message attributeStringValueForName:@"from"];
                XMPPJID *fromXMPPJID = ([from length] > 0) ? [XMPPJID jidWithString:from] : nil;
                
                XMPPMessage *xmppMessage = [XMPPMessage messageFromElement:message];
                
                if([messageType isEqualToString:@"chat"]) {
                    NSString *jid = (![fromXMPPJID.bare isEqualToString:myJIDStr]) ? fromXMPPJID.bare : toXMPPJID.bare;

                    NSLog(@"ConversationsSearchHelper::didReceiveResultMessage - P2P - jid: %@", jid);
                    
                    if(_currentRequestType == ConversationSearchHelperQueryTypeGlobal) {
                        [self addResultByJid:jid withOccurrences:0];
                    }
                    
                    else {
                        Message *theMessage = [service messageFromXmppMessage:xmppMessage];

                        if(theMessage && theMessage.groupChatEventType == MessageGroupChatEventNone) {
                            [theMessage setDeliveryState:MessageDeliveryStateRead atTimestamp:theMessage.timestamp];
                            [self addResultMessage:theMessage];
                        }
                    }
                }
                
                else if([messageType isEqualToString:@"groupchat"]) {
//                    NSString *roomJIDStr = fromXMPPJID.bare;
                    NSString *fromJIDStr = [XMPPJID jidWithString:fromXMPPJID.resource].bare;
                    
                    NSLog(@"ConversationsSearchHelper::didReceiveResultMessage - ROOM - jid: %@", fromJIDStr);

                    [xmppMessage addAttributeWithName:@"from" stringValue:fromJIDStr];
                    [xmppMessage addAttributeWithName:@"to" stringValue:myJIDStr];
                    
                    Message *theMessage = [service messageFromXmppMessage:xmppMessage];
                    
                    if(theMessage) {
                        [theMessage setDeliveryState:MessageDeliveryStateReceived atTimestamp:theMessage.timestamp];
                        [self addResultMessage:theMessage];
                    }
                }
            }
        }
    }
}

@end
