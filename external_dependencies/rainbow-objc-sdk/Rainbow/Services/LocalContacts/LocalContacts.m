/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "LocalContacts.h"
#import "Tools.h"
#import "LocalContactsToolkit.h"
#import "ContactInternal.h"
#import "ContactsManagerService+Internal.h"
#import <AddressBook/AddressBook.h>


static BOOL displayFirstNameFirst;
static BOOL sortByFirstName;

@interface LocalContacts ()
@property (nonatomic, readwrite) BOOL accessGranted;
@property (nonatomic, strong) NSOperationQueue *loadContactsOperation;
@property (nonatomic) BOOL isStarted;
@end

@implementation LocalContacts

-(CNAuthorizationStatus) getCurrentAddressBookAuthorization {
    return [CNContactStore authorizationStatusForEntityType:CNEntityTypeContacts];
}

-(instancetype) init {
    self = [super init];
    if(self){
        _loadContactsOperation = [NSOperationQueue new];
        _loadContactsOperation.maxConcurrentOperationCount = 1;
        _isStarted = NO;
#if (TARGET_IPHONE_SIMULATOR || TARGET_OS_IPHONE)
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidFinishLaunching:) name:UIApplicationDidFinishLaunchingNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidFinishLaunching:) name:UIApplicationDidBecomeActiveNotification object:nil];
#endif
    }
    return self;
}

-(void) start {
    _accessGranted = [self getCurrentAddressBookAuthorization] == CNAuthorizationStatusAuthorized;
    _isStarted = YES;
}

-(void) stop {
    _accessGranted = NO;
    _isStarted = NO;
}

-(void) dealloc {
    [_loadContactsOperation cancelAllOperations];
    _loadContactsOperation = nil;
#if (TARGET_IPHONE_SIMULATOR || TARGET_OS_IPHONE)
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidFinishLaunchingNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidBecomeActiveNotification object:nil];
#endif
}
#if (TARGET_IPHONE_SIMULATOR || TARGET_OS_IPHONE)
-(void) applicationDidFinishLaunching:(NSNotificationCenter *) notification {
    [self getSettingsValue];
}
#endif

-(BOOL) displayFirstNameFirst {
    return displayFirstNameFirst;
}

-(BOOL) sortByFirstName {
    return sortByFirstName;
}

-(void) requestAddressBookAccess {
    CNContactStore * contactStore = [[CNContactStore alloc] init];
    [contactStore requestAccessForEntityType:CNEntityTypeContacts completionHandler:^(BOOL granted, NSError * _Nullable error) {
        if (error) {
            NSLog(@"An error occured while asking the user to give AddressBook access to the application.");
        } else if (!granted) {
            NSLog(@"Access denied by user while asking the him to give AddressBook access to the application.");
        } else {
            NSLog(@"Access granted by user while asking the him to give AddressBook access to the application.");
        }
        [self willChangeValueForKey:kLocalContactsAccessGrantedKey];
        _accessGranted = granted;
        [self didChangeValueForKey:kLocalContactsAccessGrantedKey];
    }];
}


#pragma mark - Local AddressBook changes monitoring

/**
 *  Retrieve iphone contacts. This action may request the user authorisation on the first time.
 *  If the user refuse the return value will be nil
 *
 *  @return an array of CNContact
 */
-(NSArray*) retrieveContacts {
    __block NSMutableArray *returnArray = [NSMutableArray array];
    if([CNContactStore class]){
        NSError* contactError;
        CNContactStore* addressBook = [[CNContactStore alloc] init];
        [addressBook containersMatchingPredicate:[CNContainer predicateForContainersWithIdentifiers: @[addressBook.defaultContainerIdentifier]] error:&contactError];
        NSArray * keysToFetch =@[CNContactEmailAddressesKey, CNContactPhoneNumbersKey, CNContactFamilyNameKey, CNContactGivenNameKey, CNContactPostalAddressesKey,
                                 CNContactJobTitleKey, CNContactImageDataKey, CNContactIdentifierKey, CNContactOrganizationNameKey, CNContactUrlAddressesKey,
                                 CNContactImageDataAvailableKey];
        CNContactFetchRequest * request = [[CNContactFetchRequest alloc] initWithKeysToFetch:keysToFetch];
        BOOL success = [addressBook enumerateContactsWithFetchRequest:request error:&contactError usingBlock:^(CNContact * __nonnull contact, BOOL * __nonnull stop){
            [returnArray addObject:contact];
        }];
        if(!success){
            NSLog(@"Localcontacts helper, error while fetching local contacts");
        }
    }
    
    return returnArray;
}

-(void) getSettingsValue {
    CNMutableContact *fake = [CNMutableContact new];
    fake.familyName = @"fake";
    fake.givenName = @"contact";
    CNContactDisplayNameOrder order = [CNContactFormatter nameOrderForContact:fake];
    BOOL newDisplayFirstNameFirst = (order == CNContactDisplayNameOrderGivenNameFirst);
    
    BOOL newSortByFirstName = [[CNContactsUserDefaults sharedDefaults] sortOrder] == CNContactSortOrderGivenName;
    
    if(newSortByFirstName != sortByFirstName){
        [self willChangeValueForKey:kLocalContactsSortByNameKey];
        sortByFirstName = newSortByFirstName;
        [self didChangeValueForKey:kLocalContactsSortByNameKey];
    }
    if(newDisplayFirstNameFirst != displayFirstNameFirst){
        [self willChangeValueForKey:kLocalContactsDisplayFirstNameFirstKey];
        displayFirstNameFirst = newDisplayFirstNameFirst;
        [self didChangeValueForKey:kLocalContactsDisplayFirstNameFirstKey];
    }
}

@end
