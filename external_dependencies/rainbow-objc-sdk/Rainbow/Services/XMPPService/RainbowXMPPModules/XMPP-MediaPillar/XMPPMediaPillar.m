//
//  XMPPMediaPillar.m
//  Rainbow
//
//  Created by Joseph on 23/11/2017.
//  Copyright © 2017 ALE. All rights reserved.
//

#import "XMPPMediaPillar.h"

@implementation XMPPMediaPillar


/**
 <iq xmlns="jabber:client" xml:lang="en" to="mp_c3ecd7169bd14d3bbae971411401a613@aio-pcg-all-in-one-dev-1.opentouch.cloud/mediapillar" from="b554da831c77468c808697a87b7c89d0@aio-pcg-all-in-one-dev-1.opentouch.cloud/web_win_1.42.6_562W0kec" type="set" id="5bc469c6-f6da-4d2c-b617-2259a02f1e46:sendIQ">
    <mediapillar xmlns="urn:xmpp:janus:1">
        <register>
            <jidIm>b554da831c77468c808697a87b7c89d0@aio-pcg-all-in-one-dev-1.opentouch.cloud</jidIm>
            <jidTel>tel_b554da831c77468c808697a87b7c89d0@aio-pcg-all-in-one-dev-1.opentouch.cloud</jidTel>
            <number>10047674726057759</number>
            <displayName>Grey Omni</displayName>
            <secret>10047674726057759</secret>
        </register>
    </mediapillar>
 </iq>
 */

-(void) registerMediaPillar:(NSString *)number displayName:(NSString *)name secret:(NSString *)secret toURLAddress:(NSString *)urlAddress jid:(NSString *)jid jidTel:(NSString *)jidTel{
    
    NSXMLElement *mediaPillarElement = [NSXMLElement elementWithName:@"mediapillar" xmlns:XMLNSMediaPillar];
    
    NSXMLElement *registerElement = [NSXMLElement elementWithName:@"register"];
    
    NSXMLElement *jidImElement = [NSXMLElement elementWithName:@"jidIm" stringValue:jid];
    NSXMLElement *jidTelElement = [NSXMLElement elementWithName:@"jidTel" stringValue:jidTel];
    NSXMLElement *numberElement = [NSXMLElement elementWithName:@"number" stringValue:number];
    NSXMLElement *displayNameElement = [NSXMLElement elementWithName:@"displayName" stringValue:name];
    NSXMLElement *secretElement = [NSXMLElement elementWithName:@"secret" stringValue:secret];
    
    [registerElement addChild:jidImElement];
    [registerElement addChild:jidTelElement];
    [registerElement addChild:numberElement];
    [registerElement addChild:displayNameElement];
    [registerElement addChild:secretElement];
    
    [mediaPillarElement addChild:registerElement];
    
    XMPPJID *mediaPillarJID = [XMPPJID jidWithString:urlAddress];
    
    XMPPIQ *mediaPillarRegisterIQ = [XMPPIQ iqWithType:@"set"
                                                    to:mediaPillarJID
                                             elementID:[XMPPStream generateUUID]
                                                 child:mediaPillarElement];
    
    [mediaPillarRegisterIQ addAttributeWithName:@"from" stringValue:xmppStream.myJID.full];

    NSLog(@"[MEDIAPILLAR] Sending register IQ %@", mediaPillarRegisterIQ);
    
    XMPPElementReceipt *receipt = nil;
    [xmppStream sendElement:mediaPillarRegisterIQ andGetReceipt:&receipt];
    
    NSLog(@"[MEDIAPILLAR] Receipt %@", receipt);
}

/**
<iq from="33b4c28f58f94f17a19bd7e794542047@vberder-all-in-one-dev-1.opentouch.cloud/web_win_1.32.6_He4cKtkp" to="mediapillargateway.vberder-all-in-one-dev-1.opentouch.cloud" type="set" id="db556df0-bc20-4d1e-96f1-bf89d5102bc0:sendIQ" xmlns:stream="http://etherx.jabber.org/streams">
 <mediapillar xmlns="urn:xmpp:janus:1">
    <unregister>
        <number>1056789</number>
    </unregister>
 </mediapillar>
</iq>
 */

-(void) unregisterMediaPillar:(NSString *)number toURLAddress:(NSString *)urlAddress{
    
    NSXMLElement *mediaPillarElement = [NSXMLElement elementWithName:@"mediapillar" xmlns:XMLNSMediaPillar];
    NSXMLElement *unregisterElement = [NSXMLElement elementWithName:@"unregister"];
    
    NSXMLElement *numberElement = [NSXMLElement elementWithName:@"number" stringValue:number];
    
    [unregisterElement addChild:numberElement];
    [mediaPillarElement addChild:unregisterElement];
    
    XMPPJID *mediaPillarJID = [XMPPJID jidWithString:urlAddress];
    
    XMPPIQ *mediaPillarUnRegisterIQ = [XMPPIQ iqWithType:@"set"
                                                    to:mediaPillarJID
                                             elementID:[XMPPStream generateUUID]
                                                 child:mediaPillarElement];
    
    [mediaPillarUnRegisterIQ addAttributeWithName:@"from" stringValue:xmppStream.myJID.full];
    
    NSLog(@"[MEDIAPILLAR] Sending unregister IQ %@", mediaPillarUnRegisterIQ);
    
    XMPPElementReceipt *receipt = nil;
    [xmppStream sendElement:mediaPillarUnRegisterIQ andGetReceipt:&receipt];
    
    NSLog(@"[MEDIAPILLAR] Receipt %@", receipt);

}

@end
