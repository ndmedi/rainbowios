/*
 * Rainbow
 *
 * Copyright (c) 2018, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "XMPPIQ+XEP_0167.h"
#import "NSXMLElement+XMPP.h"

@implementation XMPPIQ (XEP_0167)

+ (XMPPIQ *)iqWithHoldJingleFrom:(NSString *)fromJid to:(NSString *)toJid elementID:(NSString *)eid sessionID:(NSString*) sid
{
    return [XMPPIQ iqWithJingle:[XMPPIQ holdJingleFromJid:fromJid toJid:toJid sessionID:sid] from:fromJid to:toJid elementID:eid];
}

+ (XMPPIQ *)iqWithUnholdJingleFrom:(NSString *)fromJid to:(NSString *)toJid elementID:(NSString *)eid sessionID:(NSString*) sid
{
    return [XMPPIQ iqWithJingle:[XMPPIQ unholdJingleFromJid:fromJid toJid:toJid sessionID:sid] from:fromJid to:toJid elementID:eid];
}

+ (XMPPIQ *)iqWithJingle:(NSXMLElement *)jingle from:(NSString *)fromJid to:(NSString *)toJid elementID:(NSString *)eid
{
    XMPPIQ *iq = [[XMPPIQ alloc] initWithType:@"set" elementID:eid child:jingle];
    [iq addAttributeWithName:@"from" stringValue:fromJid];
    [iq addAttributeWithName:@"to" stringValue:toJid];
    
    return iq;
}

+(NSXMLElement *) holdJingleFromJid:(NSString*) fromJid toJid:(NSString *) toJid sessionID:(NSString*) sid {
    /*
     <iq id='xv39z423' type='set' from='juliet@capulet.lit/balcony' to='romeo@montague.lit/orchard'>
        <jingle xmlns='urn:xmpp:jingle:1' action='session-info' initiator='romeo@montague.lit/orchard' sid='a73sjjvkla37jfea'>
            <hold xmlns='urn:xmpp:jingle:apps:rtp:info:1'/>
        </jingle>
     </iq>
     */
    
    NSXMLElement *jingle = [NSXMLElement elementWithName:@"jingle" xmlns:@"urn:xmpp:jingle:1"];
    [jingle addAttributeWithName:@"action" stringValue:@"session-info"];
    [jingle addAttributeWithName:@"initiator" stringValue:fromJid];
    [jingle addAttributeWithName:@"sid" stringValue:sid];
    
    NSXMLElement *hold = [NSXMLElement elementWithName:@"hold" xmlns:@"urn:xmpp:jingle:apps:rtp:info:1"];
    [jingle addChild:hold];
    
    return jingle;
}

+(NSXMLElement *) unholdJingleFromJid:(NSString*) fromJid toJid:(NSString *) toJid sessionID:(NSString*) sid {
    /*
     <iq id='xv39z423' type='set' from='juliet@capulet.lit/balcony' to='romeo@montague.lit/orchard'>
        <jingle xmlns='urn:xmpp:jingle:1' action='session-info' initiator='romeo@montague.lit/orchard' sid='a73sjjvkla37jfea'>
            <unhold xmlns='urn:xmpp:jingle:apps:rtp:info:1'/>
        </jingle>
     </iq>
     */
    
    NSXMLElement *jingle = [NSXMLElement elementWithName:@"jingle" xmlns:@"urn:xmpp:jingle:1"];
    [jingle addAttributeWithName:@"action" stringValue:@"session-info"];
    [jingle addAttributeWithName:@"initiator" stringValue:fromJid];
    [jingle addAttributeWithName:@"sid" stringValue:sid];
    
    NSXMLElement *unhold = [NSXMLElement elementWithName:@"unhold" xmlns:@"urn:xmpp:jingle:apps:rtp:info:1"];
    [jingle addChild:unhold];
    
    return jingle;
}

-(BOOL) isHoldJingle {
    NSXMLElement *jingleElement = [self elementForName:@"jingle" xmlns:@"urn:xmpp:jingle:1"];
    return (jingleElement && [jingleElement elementForName:@"hold" xmlns:@"urn:xmpp:jingle:apps:rtp:info:1"]) ? YES : NO;
}

-(BOOL) isUnholdJingle {
    NSXMLElement *jingleElement = [self elementForName:@"jingle" xmlns:@"urn:xmpp:jingle:1"];
    return (jingleElement && [jingleElement elementForName:@"unhold" xmlns:@"urn:xmpp:jingle:apps:rtp:info:1"]) ? YES : NO;
}

-(NSString *) sessionIDFromJingle {
    NSXMLElement *jingleElement = [self elementForName:@"jingle" xmlns:@"urn:xmpp:jingle:1"];
    if(jingleElement) {
        return [jingleElement attributeStringValueForName:@"sid"];
    }
    return nil;
}

@end
