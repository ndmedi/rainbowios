/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "XMPPPush.h"
#import "defines.h"
#import "XMPPStream.h"
#import "NSXMLElement+XMPP.h"
#import "XMPPIQ.h"
#import "PushConfiguration.h"
#import "RainbowUserDefaults.h"
#import "XMPPStream+Resume.h"

#define kPushConfigurationSerializationKey @"pushConfiguration"
#define kPushDeviceToken @"pushDeviceToken"
#define kVoIPPushDeviceToken @"voIPPushDeviceToken"

#if TARGET_OS_IPHONE
#import "UIDevice+VersionCheck.h"
#endif

#define XMPP_PUSH_COMMAND_NS @"http://jabber.org/protocol/commands"
#define XMPP_PUSH_X_DATA_NS @"jabber:x:data"
#define XMPP_PUSH_NS @"urn:xmpp:push:0"
#define XMPP_PUSH_PUBSUB_PROTOCOL @"http://jabber.org/protocol/pubsub#publish-options"

#if ! __has_feature(objc_arc)
#warning This file must be compiled with ARC. Use -fobjc-arc flag (or convert project to ARC).
#endif

@interface XMPPPush ()
@property (nonatomic, strong) NSString *registerPushIQID;
@property (nonatomic, strong) NSString *unRegisterPushIQID;
@property (nonatomic, strong) NSString *enablePushIQID;
@property (nonatomic, strong) NSString *deviceToken;
@property (nonatomic, strong) NSString *voipDeviceToken;
@property (nonatomic, strong) PushConfiguration *pushConfiguration;
@end

@implementation XMPPPush

#pragma mark - Init/Dealloc
- (id)initWithDispatchQueue:(dispatch_queue_t)queue {
    if((self = [super initWithDispatchQueue:queue])) {
        
        NSData *savedObject = [[RainbowUserDefaults sharedInstance] objectForKey:kPushConfigurationSerializationKey];
        PushConfiguration *savedConfiguration = nil;
        if(savedObject)
            savedConfiguration = (PushConfiguration*)[NSKeyedUnarchiver unarchiveObjectWithData:savedObject];
        
        if(savedConfiguration){
            _pushConfiguration = savedConfiguration;
            _deviceToken = _pushConfiguration.deviceToken;
            _voipDeviceToken = _pushConfiguration.voipToken;
        } else
            _pushConfiguration = [PushConfiguration new];
    }
    
    return self;
}

#pragma mark - XMPPModule
- (BOOL)activate:(XMPPStream *)aXmppStream {
    if ([super activate:aXmppStream]) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didUpdatePushCredentials:) name:kPushKitDidUpdatePushCredentialsNotification object:nil];
        return YES;
    }
    
    return NO;
}

- (void)deactivate {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kPushKitDidUpdatePushCredentialsNotification object:nil];
    [super deactivate];
}

-(void) cleanPushConfiguration {
    NSLog(@"[PUSH] clean push configuration");
    // Device token must be keept, but we must clean nodeID and secret.
    _deviceToken = _pushConfiguration.deviceToken;
    _pushConfiguration.nodeID = nil;
    _pushConfiguration.secret = nil;
    
    [[RainbowUserDefaults sharedInstance] removeObjectForKey:kPushConfigurationSerializationKey];
}

-(void) setPushToken:(NSData *)pushToken {
    _pushToken = pushToken;
    NSString *base64Token = [pushToken base64EncodedStringWithOptions:0];
    NSLog(@"[PUSH] didRegisterForRemoteNotificationsWithDeviceToken %@ (Base64 Token %@)",pushToken, base64Token);
    [[RainbowUserDefaults sharedInstance] setObject:base64Token forKey:kPushDeviceToken];
    
    dispatch_block_t block = ^{
        _deviceToken = base64Token;
        if(![_pushConfiguration.deviceToken isEqualToString:base64Token]){
            // If there is an old token we must unregister it.
            if(_pushConfiguration.nodeID.length > 0)
                [self unregisterPushTokenAtNodeID:_pushConfiguration.nodeID];
            // We must send the new token to server
            [self registerPushToken];
        } else {
            // Same token nothing todo
        }
    };
    
    if (dispatch_get_specific(moduleQueueTag))
        block();
    else
        dispatch_sync(moduleQueue, block);
}

-(void) didUpdatePushCredentials:(NSNotification *) notification {
    NSData *deviceToken = (NSData *) notification.object;
    NSString *base64Token = [deviceToken base64EncodedStringWithOptions:0];
    NSLog(@"[PUSH] didUpdatePushCredentials %@ (Base64 Token %@)", deviceToken, base64Token);
    [[RainbowUserDefaults sharedInstance] setObject:base64Token forKey:kVoIPPushDeviceToken];
    
    // We don't upload the token now, we will do it during authentication steps
    _voipDeviceToken = base64Token;
}

/**
 *  <iq type='set' id='124' to='app.pingme.sqanet.fr' xmlns='jabber:client'>
        <command xmlns='http://jabber.org/protocol/commands' node='unregister-push' action='execute'>
            <x xmlns='jabber:x:data' type='submit'>
                <field type='list-multi' var='nodes'>
                    <value>11556610914108912939</value>
                </field>
                <field var='device-id'>
                    <value>100500</value>
                </field>
            </x>
        </command>
    </iq>
 */
-(void) unregisterPushTokenAtNodeID:(NSString *) nodeID {
    _unRegisterPushIQID = [XMPPStream generateUUID];
    
    NSXMLElement *xElement = [NSXMLElement elementWithName:@"x" xmlns:XMPP_PUSH_X_DATA_NS];
    [xElement addAttributeWithName:@"type" stringValue:@"submit"];
    
    NSXMLElement *fieldElement = [NSXMLElement elementWithName:@"field"];
    [fieldElement addAttributeWithName:@"type" stringValue:@"list-multi"];
    [fieldElement addAttributeWithName:@"var" stringValue:@"nodes"];
    
    NSXMLElement *valueElement = [NSXMLElement elementWithName:@"value" stringValue:nodeID];
    [fieldElement addChild:valueElement];
    
    [xElement addChild:fieldElement];
    
    NSXMLElement *unRegisterCommand = [NSXMLElement elementWithName:@"command"];
    [unRegisterCommand setXmlns:XMPP_PUSH_COMMAND_NS];
    [unRegisterCommand addAttributeWithName:@"node" stringValue:@"unregister-push"];
    [unRegisterCommand addAttributeWithName:@"action" stringValue:@"execute"];
    [unRegisterCommand addChild:xElement];
    
    XMPPIQ *unRegisterIQ = [XMPPIQ iqWithType:@"set" to:nil elementID:_unRegisterPushIQID child:unRegisterCommand];
    
    NSLog(@"[PUSH] Sending unregister push IQ %@", unRegisterIQ);
    
    [xmppStream sendElement:unRegisterIQ];
}

/**
 *  <iq type='set' id='127' to='app.pingme.sqanet.fr' xmlns='jabber:client'>
        <command xmlns='http://jabber.org/protocol/commands' node='register-push-gcm' action='execute'>
            <x xmlns='jabber:x:data' type='submit'>
                <field var='token'>
                    <value>xxx</value>
                </field>
                <field var='device-name'>
                    <value>AAAA</value>
                </field>
                <field var='device-id'>
                    <value>100500</value>
                </field>
            </x>
        </command>
    </iq>
 
    <iq type="set" id="6A8C60AF-0C41-4273-A19D-FC4A0C0B7542">
        <command xmlns="http://jabber.org/protocol/commands" node="register-push-apns" action="execute">
            <x xmlns="jabber:x:data" type="submit">
                <field var="token">
                    <value>snu4Ixpw+gN5MY4GZ/RJjbEK3z5ryBOjXgeHVjChkz8=</value>
                </field>
                <field var="device-name">
                    <value>iPhone</value>
                </field>
            </x>
        </command>
    </iq>
 */
-(void) registerPushToken {
    _registerPushIQID = [XMPPStream generateUUID];
    
    NSXMLElement *xElement = [NSXMLElement elementWithName:@"x" xmlns:XMPP_PUSH_X_DATA_NS];
    [xElement addAttributeWithName:@"type" stringValue:@"submit"];
    
    NSXMLElement *fieldTokenElement = [NSXMLElement elementWithName:@"field"];
    [fieldTokenElement addAttributeWithName:@"var" stringValue:@"token"];
    
    NSXMLElement *valueTokenElement = [NSXMLElement elementWithName:@"value" stringValue:_deviceToken];
    [fieldTokenElement addChild:valueTokenElement];
    
    // app-id field
    if(_applicationID && ![_applicationID isEqualToString:@""]){
        NSXMLElement *fieldAppIdElement = [NSXMLElement elementWithName:@"field"];
        [fieldAppIdElement addAttributeWithName:@"var" stringValue:@"app-id"];
        
        NSXMLElement *valueAppIdElement = [NSXMLElement elementWithName:@"value" stringValue:_applicationID];
        [fieldAppIdElement addChild:valueAppIdElement];
        
        [xElement addChild:fieldAppIdElement];
    }
    
    NSXMLElement *fieldDeviceNameElement = [NSXMLElement elementWithName:@"field"];
    [fieldDeviceNameElement addAttributeWithName:@"var" stringValue:@"device-name"];
    
    NSXMLElement *valueDeviceNameElement = [NSXMLElement elementWithName:@"value" stringValue:@"iPhone"];
    [fieldDeviceNameElement addChild:valueDeviceNameElement];
    
    [xElement addChild:fieldTokenElement];
    [xElement addChild:fieldDeviceNameElement];
    
    // <field var='device-id'><value>100501</value></field>
    NSXMLElement *fieldDeviceIDElement = [NSXMLElement elementWithName:@"field"];
    [fieldDeviceIDElement addAttributeWithName:@"var" stringValue:@"device-id"];
    
    NSXMLElement *valueDeviceIDElement = [NSXMLElement elementWithName:@"value" stringValue:xmppStream.myJID.resource];
    [fieldDeviceIDElement addChild:valueDeviceIDElement];
    
    [xElement addChild:fieldDeviceIDElement];
    
#if TARGET_OS_IPHONE
    //    <field var='os-version'><value>iOS10</value></field>
    NSXMLElement *fieldOsVersionElement = [NSXMLElement elementWithName:@"field"];
    [fieldOsVersionElement addAttributeWithName:@"var" stringValue:@"os-version"];
    
    NSXMLElement *valueOsVersionElement = [NSXMLElement elementWithName:@"value" stringValue:[NSString stringWithFormat:@"%lu",(unsigned long)[UIDevice currentDevice].systemMajorVersion]];
    [fieldOsVersionElement addChild:valueOsVersionElement];
    
    [xElement addChild:fieldOsVersionElement];
#endif
    
    if(_voipDeviceToken.length > 0){
        //    <field var='token-voip'>
        NSXMLElement *fieldTokenVoIPElement = [NSXMLElement elementWithName:@"field"];
        [fieldTokenVoIPElement addAttributeWithName:@"var" stringValue:@"token-voip"];
        
        NSXMLElement *valueTokenVoIPElement = [NSXMLElement elementWithName:@"value" stringValue:_voipDeviceToken];
        [fieldTokenVoIPElement addChild:valueTokenVoIPElement];
        
        [xElement addChild:fieldTokenVoIPElement];
    }
    
    NSXMLElement *registerCommand = [NSXMLElement elementWithName:@"command"];
    [registerCommand setXmlns:XMPP_PUSH_COMMAND_NS];
    [registerCommand addAttributeWithName:@"node" stringValue:@"register-push-apns"];
    [registerCommand addAttributeWithName:@"action" stringValue:@"execute"];
    [registerCommand addChild:xElement];
    
    
    XMPPIQ *registerIQ = [XMPPIQ iqWithType:@"set" to:xmppStream.myJID.domainJID elementID:_registerPushIQID child:registerCommand];
    
    NSLog(@"[PUSH] Sending register push IQ %@", registerIQ);
    
    [xmppStream sendElement:registerIQ];
}

/**
 *  <iq type='set' id='127' xmlns='jabber:client'>
        <enable xmlns='urn:xmpp:push:0' node='11556610914108912939' jid='pubsub.app.pingme.sqanet.fr'>
            <x xmlns='jabber:x:data'>
                <field var='FORM_TYPE'>
                    <value>http://jabber.org/protocol/pubsub#publish-options</value>
                </field>
                <field var='secret'>
                    <value>10577847020188401951</value>
                </field>
            </x>
        </enable>
    </iq>
 */
-(void) enablePush {
    if(_enablePushIQID.length == 0 && _pushConfiguration.isRegistered){
        NSLog(@"PUSH CONFIGURATION %@", _pushConfiguration);
        _enablePushIQID = [XMPPStream generateUUID];
        NSXMLElement *enableElement = [NSXMLElement elementWithName:@"enable" xmlns:XMPP_PUSH_NS];
        [enableElement addAttributeWithName:@"node" stringValue:_pushConfiguration.nodeID];
        NSString *pubsubJID = [NSString stringWithFormat:@"pubsub.%@", xmppStream.myJID.domain];
        [enableElement addAttributeWithName:@"jid" stringValue:pubsubJID];
        
        NSXMLElement *xElement = [NSXMLElement elementWithName:@"x" xmlns:XMPP_PUSH_X_DATA_NS];
        [xElement addAttributeWithName:@"type" stringValue:@"submit"];
        
        NSXMLElement *fieldFormTypeElement = [NSXMLElement elementWithName:@"field"];
        [fieldFormTypeElement addAttributeWithName:@"var" stringValue:@"FORM_TYPE"];
        
        NSXMLElement *valueFormTypeElement = [NSXMLElement elementWithName:@"value" stringValue:XMPP_PUSH_PUBSUB_PROTOCOL];
        [fieldFormTypeElement addChild:valueFormTypeElement];
        
        NSXMLElement *fieldSecretElement = [NSXMLElement elementWithName:@"field"];
        [fieldSecretElement addAttributeWithName:@"var" stringValue:@"secret"];
        
        NSXMLElement *valueSecretElement = [NSXMLElement elementWithName:@"value" stringValue:_pushConfiguration.secret];
        [fieldSecretElement addChild:valueSecretElement];
        
        [xElement addChild:fieldFormTypeElement];
        [xElement addChild:fieldSecretElement];
        
        [enableElement addChild:xElement];
        
        XMPPIQ *enableIQ = [XMPPIQ iqWithType:@"set" to:nil elementID:_enablePushIQID child:enableElement];
        
        NSLog(@"[PUSH] Sending enable push IQ %@", enableIQ);
        
        [xmppStream sendElement:enableIQ];
    } else {
        if(_enablePushIQID.length > 0)
            NSLog(@"[PUSH] We already sent the enable push IQ, don't do it again");
        
        if(_pushConfiguration.isRegistered)
            NSLog(@"[PUSH] We don't have node ID, that means we never register the push token");
    }
}

-(void) disablePush {
    // TODO: Ask andrei example for this IQ
}

#pragma mark - xmppStream delegate
- (void)xmppStreamDidAuthenticate:(XMPPStream *)sender {
    if([sender isAuthenticated]){
        NSLog(@"[PUSH] We are authenticated so enable push");
        dispatch_block_t block = ^{
            _enablePushIQID = nil;
            if(_deviceToken.length > 0)
                [self registerPushToken];
            else {
                if(_deviceToken.length == 0)
                    NSLog(@"[PUSH] we don't have the device token");
            }
        };
        if (dispatch_get_specific(moduleQueueTag))
            block();
        else
            dispatch_sync(moduleQueue, block);
    }
}

- (BOOL) xmppStream:(XMPPStream *)sender didReceiveIQ:(XMPPIQ *)iq {
    if([iq.elementID isEqualToString:_registerPushIQID]){
        NSLog(@"[PUSH] did receive register push IQ response %@", iq);
        NSXMLElement *commandElement = [iq elementForName:@"command" xmlns:XMPP_PUSH_COMMAND_NS];
        NSString *status = [commandElement attributeForName:@"status"].stringValue;
        if([status isEqualToString:@"completed"]){
            NSXMLElement *xElement = [commandElement elementForName:@"x" xmlns:XMPP_PUSH_X_DATA_NS];
            NSArray *allFields = [xElement elementsForName:@"field"];
            NSString *secret = @"";
            NSString *node = @"";
            for (NSXMLElement *field in allFields) {
                if([[field attributeForName:@"var"].stringValue isEqualToString:@"node"]){
                    node = [field childAtIndex:0].stringValue;
                }
                if([[field attributeForName:@"var"].stringValue isEqualToString:@"secret"]){
                    secret = [field childAtIndex:0].stringValue;
                }
            }
            
            _pushConfiguration = [PushConfiguration new];
            _pushConfiguration.deviceToken = _deviceToken;
            _pushConfiguration.voipToken = _voipDeviceToken;
            _pushConfiguration.secret = secret;
            _pushConfiguration.nodeID = node;
            
            [[RainbowUserDefaults sharedInstance] setObject:[NSKeyedArchiver archivedDataWithRootObject:_pushConfiguration] forKey:kPushConfigurationSerializationKey];
            
            // We must now send the enable
            [self enablePush];
            return YES;
        } else {
            NSLog(@"[PUSH] DID RECEIVE IQ BUT NOT THE EXPECTED ONE %@", iq);
            return NO;
        }
    }
    
    if([iq.elementID isEqualToString:_unRegisterPushIQID]){
        NSLog(@"[PUSH] did receive unregister push IQ response %@", iq);
        NSXMLElement *commandElement = [iq elementForName:@"command" xmlns:XMPP_PUSH_COMMAND_NS];
        NSString *status = [commandElement attributeForName:@"status"].stringValue;
        if([status isEqualToString:@"completed"]){
            [self cleanPushConfiguration];
            return YES;
        } else {
            NSLog(@"[PUSH] DID RECEIVE IQ BUT NOT THE EXPECTED ONE %@", iq);
            return NO;
        }
    }
    
    if([iq.elementID isEqualToString:_enablePushIQID]){
        NSLog(@"[PUSH] We receive a response to our enable Push IQ but we don't treat it specially %@", iq);
        
        return YES;
    }
    
    return NO;
}

@end
