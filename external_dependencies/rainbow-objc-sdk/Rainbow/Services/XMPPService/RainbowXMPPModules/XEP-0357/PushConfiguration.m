/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "PushConfiguration.h"

@implementation PushConfiguration

- (void)encodeWithCoder:(NSCoder *) encoder {
    [encoder encodeObject:_deviceToken forKey:@"deviceToken"];
    [encoder encodeObject:_nodeID forKey:@"nodeID"];
    [encoder encodeObject:_secret forKey:@"secret"];
    if(_voipToken){
        [encoder encodeObject:_voipToken forKey:@"voipToken"];
    }
}

- (instancetype)initWithCoder:(NSCoder *) decoder {
    self = [super init];
    if(self){
        _deviceToken = [decoder decodeObjectForKey:@"deviceToken"];
        _nodeID = [decoder decodeObjectForKey:@"nodeID"];
        _secret = [decoder decodeObjectForKey:@"secret"];
        if([decoder decodeObjectForKey:@"voipToken"])
            _voipToken = [decoder decodeObjectForKey:@"voipToken"];
    }
    return self;
}
-(NSString *) description {
    return [NSString stringWithFormat:@"Push Configuration %p : <Device Token (Base64 encoded) %@ VoIP Token (Base64 encoded) %@ nodeID %@ secret %@>", self, _deviceToken, _voipToken, _nodeID, _secret];
}

-(BOOL) isRegistered {
    return _nodeID.length > 0 && _secret.length > 0;
}
@end
