/*
 * Rainbow
 *
 * Copyright (c) 2017, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "XMPPStream+Resume.h"
#import "XMPPStream+Internal.h"
#import "XMPPStreamManagement.h"

@implementation XMPPStream (Resume)
-(BOOL) isResumed {
    __block BOOL result = NO;
    
    dispatch_block_t block = ^{
        [self enumerateModulesOfClass:[XMPPStreamManagement class] withBlock:^(XMPPModule *module, NSUInteger idx, BOOL *stop) {
            result = [((XMPPStreamManagement*)module) didResume];
            *stop = YES;
        }];
    };
    
    if (dispatch_get_specific(self.xmppQueueTag))
        block();
    else
        dispatch_sync(self.xmppQueue, block);
    
    return result;
}
@end
