#import <Foundation/Foundation.h>

#import "XMPPCoreDataStorage.h"
#import "XMPPMessageArchiveManagement.h"
#import "XMPPMessageArchiveManagement_Message_CoreDataObject.h"
#import "XMPPMessageArchiveManagement_Contact_CoreDataObject.h"
#import "XMPPService.h"

#define XMLNS_XMPP_ARCHIVE @"urn:xmpp:mam:1"
#define XMLNS_XMPP_ARCHIVE_BULK @"urn:xmpp:mam:1:bulk"

@interface XMPPMessageArchiveManagementCoreDataStorage : XMPPCoreDataStorage <XMPPMessageArchiveManagementStorage>
{
	/* Inherited protected variables from XMPPCoreDataStorage
	
	NSString *databaseFileName;
	NSUInteger saveThreshold;
	
	dispatch_queue_t storageQueue;
	 
	*/
}

/**
 * Convenience method to get an instance with the default database name.
 * 
 * IMPORTANT:
 * You are NOT required to use the sharedInstance.
 * 
 * If your application uses multiple xmppStreams, and you use a sharedInstance of this class,
 * then all of your streams share the same database store. You might get better performance if you create
 * multiple instances of this class instead (using different database filenames), as this way you can have
 * concurrent writes to multiple databases.
**/
+ (instancetype)sharedInstance;

@property (nonatomic, weak) XMPPService *xmppService;


@property (strong) NSString *messageEntityName;
@property (strong) NSString *contactEntityName;

- (NSEntityDescription *)messageEntity:(NSManagedObjectContext *)moc;
- (NSEntityDescription *)contactEntity:(NSManagedObjectContext *)moc;

- (NSArray<Message*> *)fetchMessagesForJID:(XMPPJID *)jid xmmpStream:(XMPPStream *) xmppStream maxSize:(NSNumber *) maxSize offset:(NSNumber *) offset;

- (NSArray<Message*> *)fetchLastMessagesForJID:(XMPPJID *)jid xmmpStream:(XMPPStream *) xmppStream;

-(Message *) fetchMessagesForJID:(XMPPJID *)jid wtihMessageId:(NSString *)messageId xmmpStream:(XMPPStream *) xmppStream;
-(void) cleanAllMessages;

-(NSArray<CallLog *> *) fetchCallLogsForJid:(XMPPJID *)jid xmppStream:(XMPPStream *) xmppStream;
-(void)deleteAllCallLogWithPeerJID:(NSString *) jid xmmpStream:(XMPPStream *) xmppStream;
- (void)updateFileWithURL:(NSString *) url  withFileStatus:(BOOL) isDownloadAvailable;
/* Inherited from XMPPCoreDataStorage
 * Please see the XMPPCoreDataStorage header file for extensive documentation.
 
- (id)initWithDatabaseFilename:(NSString *)databaseFileName storeOptions:(NSDictionary *)storeOptions;
- (id)initWithInMemoryStore;

@property (readonly) NSString *databaseFileName;
 
@property (readwrite) NSUInteger saveThreshold;

@property (readonly) NSManagedObjectModel *managedObjectModel;
@property (readonly) NSPersistentStoreCoordinator *persistentStoreCoordinator;

@property (readonly) NSManagedObjectContext *mainThreadManagedObjectContext;
 
*/

@end
