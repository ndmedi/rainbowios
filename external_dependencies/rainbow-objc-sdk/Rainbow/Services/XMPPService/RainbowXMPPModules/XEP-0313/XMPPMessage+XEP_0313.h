/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "XMPPMessage.h"

@interface XMPPMessage (XEP_0313)
-(BOOL) isArchivedMessage;
-(NSDate *) archivedMessageDate;
-(NSString *) archivedMamMessageID;
-(BOOL) isResultStanza;
-(NSXMLElement *) resultStanza;
-(XMPPMessage *) resultForwardedMessage;

-(NSXMLElement *) resultStanzaWithNamespace:(NSString *) aNamespace;
-(BOOL) isResultStanzaWithNamespace:(NSString *) aNamespace;

// Custom Rainbow <ack/> markup in MAM messages
-(BOOL) isArchivedMessageSent;
-(BOOL) isArchivedMessageReceived;
-(NSDate *) archivedMessageReceivedDate;
-(BOOL) isArchivedMessageRead;
-(NSDate *) archivedMessageReadDate;
-(BOOL) isArchivedMessageDelivered;
-(NSDate *) archivedMessageDeliveredDate;
-(NSDate *) timestampMessageDate;
-(NSDate *) getTimeFromString :(NSString *) date;
@end
