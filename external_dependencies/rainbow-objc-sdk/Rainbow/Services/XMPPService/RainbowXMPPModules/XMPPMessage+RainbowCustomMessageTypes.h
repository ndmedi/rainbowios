/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "XMPPMessage.h"

#define kXMPPJabberXAudioConferenceNS @"jabber:x:audioconference"

@interface XMPPMessage (RainbowCustomMessageTypes)
-(BOOL) isFileTranfer;
-(BOOL) isWebRTCCall;
-(BOOL) isManagementMessage;
-(BOOL) isWebRTCCallWithSupportedBody;
-(BOOL) isWebRTCCallWithCallLogInfo;
-(BOOL) isConferenceManagementMessage;
-(BOOL) isConferenceReminder;
-(BOOL) isChatMessageWithChangedNotification;
-(BOOL) isChatMessageWithRecordingState;
-(BOOL) isCallServiceMessage;
-(BOOL) isCallServiceWithNomadicStatus;
-(BOOL) isChatMessageWithMarkAllAsReadNotification;
@end
