/*
 * Rainbow
 *
 * Copyright (c) 2018, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "XMPPUserMemoryStorageObject+HybridStorage.h"
#import "GCDMulticastDelegate.h"

@implementation XMPPRosterMemoryStorage (HybridStorage)
-(void) populateRosterItems:(NSArray <XMPPUserMemoryStorageObject*> *) items {
    for (XMPPUserMemoryStorageObject *user in items) {
        XMPPJID *jid = user.jid.bareJID;
        self->roster[jid] = user;
        [[self multicastDelegate] xmppRoster:self didAddUser:user];
        [[self multicastDelegate] xmppRosterDidChange:self];
    }
}
@end
