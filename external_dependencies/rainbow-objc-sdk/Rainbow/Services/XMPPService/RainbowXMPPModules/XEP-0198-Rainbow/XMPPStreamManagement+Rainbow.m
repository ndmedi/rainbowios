/*
 * Rainbow
 *
 * Copyright (c) 2017, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "XMPPStreamManagement+Rainbow.h"

@interface XMPPStreamManagement (){
@protected
    NSDate *disconnectDate;
}
- (void)processSentElement:(XMPPElement *)element;
@end

@implementation XMPPStreamManagement (Rainbow)
-(void) disconnect {
    self->disconnectDate = nil;
    [self.storage removeAllForStream:self.xmppStream];
}

- (void)xmppStream:(XMPPStream *)sender didSendIQ:(XMPPIQ *)iq {
    [self processSentElement:iq];
}

- (void)xmppStream:(XMPPStream *)sender didSendMessage:(XMPPMessage *)message {
    [self processSentElement:message];
}

- (void)xmppStream:(XMPPStream *)sender didSendPresence:(XMPPPresence *)presence {
    [self processSentElement:presence];
}

- (void)removeAllForStream:(XMPPStream *)stream {
    [self.storage removeAllForStream:stream];
}
@end
