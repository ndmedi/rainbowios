/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "XMPPMessage+XEP_0184EX.h"
#import "NSXMLElement+XMPP.h"
#import "XMPPDateTimeProfiles.h"

@implementation XMPPMessage (XEP_0184EX)

/*
 * Example of Rainbow message Extented :
 <message 
  xmlns='jabber:client'
  from='1000@app.pingme.sqanet.fr/2659144'
  to='1001@app.pingme.sqanet.fr/18383405'
  id='UwbqyVfvSqxLzCYY1030'>
    <timestamp 
      xmlns='urn:xmpp:receipts' 
      value='2016-09-23T07:31:52.938726Z'/>
    <received 
      xmlns='urn:xmpp:receipts' 
      id='SOtH8osyoIVWl0Op1025' 
      event='read' 
      entity='client' 
      type='im'/>
 </message>
 */

-(BOOL) isReceivedByServerACKMessage {
    NSXMLElement *receiptResponse = [self elementForName:@"received" xmlns:@"urn:xmpp:receipts"];
    BOOL event_received = [[[receiptResponse attributeForName:@"event"] stringValue] isEqualToString:@"received"];
    BOOL entity_server = [[[receiptResponse attributeForName:@"entity"] stringValue] isEqualToString:@"server"];
    return event_received && entity_server;
}

-(BOOL) isReceivedByClientACKMessage {
    NSXMLElement *receiptResponse = [self elementForName:@"received" xmlns:@"urn:xmpp:receipts"];
    BOOL event_received = [[[receiptResponse attributeForName:@"event"] stringValue] isEqualToString:@"received"];
    BOOL entity_client = [[[receiptResponse attributeForName:@"entity"] stringValue] isEqualToString:@"client"];
    return event_received && entity_client;
}

-(BOOL) isReadByClientACKMessage {
    NSXMLElement *receiptResponse = [self elementForName:@"received" xmlns:@"urn:xmpp:receipts"];
    BOOL event_read = [[[receiptResponse attributeForName:@"event"] stringValue] isEqualToString:@"read"];
    BOOL entity_client = [[[receiptResponse attributeForName:@"entity"] stringValue] isEqualToString:@"client"];
    return event_read && entity_client;
}

-(NSDate *) timestampACKMessage {
    NSXMLElement *timestamp = [self elementForName:@"timestamp" xmlns:@"urn:xmpp:receipts"];
    if (timestamp)
    {
        NSString *stampValue = [timestamp attributeStringValueForName:@"value"];
        
        // There are other considerations concerning XEP-0082.
        // For example, it may optionally contain milliseconds.
        // And it may possibly express UTC as "+00:00" instead of "Z".
        //
        // Thankfully there is already an implementation that takes into account all these possibilities.
        
        return [XMPPDateTimeProfiles parseDateTime:stampValue];
    }
    return nil;
}

@end
