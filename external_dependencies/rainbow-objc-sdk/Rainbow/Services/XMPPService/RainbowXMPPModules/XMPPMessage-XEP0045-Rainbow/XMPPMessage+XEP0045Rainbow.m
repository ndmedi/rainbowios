/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "XMPPMessage+XEP0045Rainbow.h"
#import "XMPPMessage+XEP0045.h"
#import "NSXMLElement+XMPP.h"

@implementation XMPPMessage (XEP0045Rainbow)

-(BOOL) isGroupChatMessageWithBodyWithEvent {
    if ([self isGroupChatMessageWithBody]){
        NSArray *event = [self elementsForName:@"event"];
        
        return ([event count] > 0);
    }
    return NO;
}

- (BOOL)isEvent
{
    return ([self elementForName:@"event"] != nil);
}
@end
