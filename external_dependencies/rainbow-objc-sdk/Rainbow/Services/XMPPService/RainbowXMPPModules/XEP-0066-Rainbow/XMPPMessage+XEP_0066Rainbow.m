//
//  XMPPMessage+XEP_0066Rainbow.m
//  Rainbow
//
//  Created by Vladimir Vyskocil on 15/03/17.
//  Copyright © 2017 ALE. All rights reserved.
//

#import "XMPPMessage+XEP_0066Rainbow.h"
#import "XMPPMessage+XEP_0066.h"
#import "NSXMLElement+XMPP.h"

#if ! __has_feature(objc_arc)
#warning This file must be compiled with ARC. Use -fobjc-arc flag (or convert project to ARC).
#endif

#define NAME_OUT_OF_BAND @"x"
#define XMLNS_OUT_OF_BAND @"jabber:x:oob"

@implementation  XMPPMessage (XEP_0066Rainbow)

- (void)addOutOfBandURI:(NSString *)URI fileName:(NSString *)fileName mimeType:(NSString *)mimeType size:(NSUInteger)size ownerId :(NSString *)ownerId{
    NSXMLElement *outOfBand = [NSXMLElement elementWithName:NAME_OUT_OF_BAND xmlns:XMLNS_OUT_OF_BAND];
    
    if([URI length]){
        NSXMLElement *URLElement = [NSXMLElement elementWithName:@"url" stringValue:URI];
        [outOfBand addChild:URLElement];
    }
    
    if([fileName length]){
        NSXMLElement *descElement = [NSXMLElement elementWithName:@"filename" stringValue:fileName];
        [outOfBand addChild:descElement];
    }
    
    if([mimeType length]){
        NSXMLElement *mimeTypeElement = [NSXMLElement elementWithName:@"mime" stringValue:mimeType];
        [outOfBand addChild:mimeTypeElement];
    }
    
    NSXMLElement *lengthElement = [NSXMLElement elementWithName:@"size" stringValue:[NSString stringWithFormat:@"%ld", size]];
    [outOfBand addChild:lengthElement];
    
    NSXMLElement *ownerElement = [NSXMLElement elementWithName:@"ownerId" stringValue:ownerId];
    [outOfBand addChild:ownerElement];
    
    [self addChild:outOfBand];
}

- (NSString *)outOfBandMimeType {
    NSXMLElement *outOfBand = [self elementForName:NAME_OUT_OF_BAND xmlns:XMLNS_OUT_OF_BAND];
    NSXMLElement *mimeTypeElement = [outOfBand elementForName:@"mime"];
    NSString *mimeTypeString = [mimeTypeElement stringValue];
    return mimeTypeString;
}

- (NSString *)outOfBandFileName {
    NSXMLElement *outOfBand = [self elementForName:NAME_OUT_OF_BAND xmlns:XMLNS_OUT_OF_BAND];
    NSXMLElement *fileNameElement = [outOfBand elementForName:@"filename"];
    NSString *fileNameString = [fileNameElement stringValue];
    return fileNameString;
    
}

- (NSString *)outOfBandOwnerId {
    NSXMLElement *outOfBand = [self elementForName:NAME_OUT_OF_BAND xmlns:XMLNS_OUT_OF_BAND];
    NSXMLElement *ownerIdElement = [outOfBand elementForName:@"ownerId"];
    NSString *ownerIdString = [ownerIdElement stringValue];
    return ownerIdString;
    
}

- (NSUInteger)outOfBandFileSize {
    NSXMLElement *outOfBand = [self elementForName:NAME_OUT_OF_BAND xmlns:XMLNS_OUT_OF_BAND];
    NSXMLElement *fileLengthElement = [outOfBand elementForName:@"size"];
    NSInteger fileLength = [fileLengthElement stringValueAsUInt64];
    return fileLength;
}

- (BOOL) hasValidOutOfBandData {
    return self.outOfBandURL.absoluteString.length > 0 && self.outOfBandMimeType.length >0 && self.outOfBandFileSize > 0;
}

@end
