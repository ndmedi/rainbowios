//
//  XMPPMessage+XEP_0066Rainbow.h
//  Rainbow
//
//  Created by Vladimir Vyskocil on 15/03/17.
//  Copyright © 2017 ALE. All rights reserved.
//

#import "XMPPMessage.h"

@interface XMPPMessage (XEP_0066Rainbow)
- (void)addOutOfBandURI:(NSString *)URI fileName:(NSString *)fileName mimeType:(NSString *)mimeType size:(NSUInteger)size ownerId :(NSString *)ownerId;

- (NSString *)outOfBandMimeType;
- (NSString *)outOfBandFileName;
- (NSUInteger)outOfBandFileSize;
- (NSString *)outOfBandOwnerId;
- (BOOL) hasValidOutOfBandData;

@end
