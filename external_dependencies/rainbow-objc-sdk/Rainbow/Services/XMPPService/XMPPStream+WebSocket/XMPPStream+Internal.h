/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "XMPPStream.h"
#import "XMPPInternal.h"
#import "XMPPIDTracker.h"

#define WS_TAG_XMPP_WRITE_STREAM       202

@interface XMPPStream (SRWebSocket)
@property (nonatomic, readwrite) dispatch_queue_t xmppQueue;
@property (nonatomic, readwrite) void *xmppQueueTag;

@end

@interface XMPPStream () {
    @protected
        GCDMulticastDelegate <XMPPStreamDelegate> *multicastDelegate;
        NSXMLElement *rootElement;
        XMPPParser *parser;
        XMPPStreamState state;
        uint64_t numberOfBytesSent;
        uint64_t numberOfBytesReceived;
    	NSCountedSet *customElementNames;
        XMPPPresence *myPresence;
        NSString *hostName;
        UInt16 hostPort;
        id <XMPPCustomBinding> customBinding;
        BOOL skipStartSession;
        BOOL validatesResponses;
        XMPPIDTracker *idTracker;
        NSError *otherError;
    
        dispatch_queue_t willSendIqQueue;
        dispatch_queue_t willSendMessageQueue;
        dispatch_queue_t willSendPresenceQueue;
    
        dispatch_queue_t willReceiveStanzaQueue;
}

- (void)commonInit;
- (void)startConnectTimeout:(NSTimeInterval)timeout;
- (void)endConnectTimeout;
- (BOOL)didStartNegotiation;
- (void)setDidStartNegotiation:(BOOL)flag;
- (BOOL)isP2P;
- (void)setIsAuthenticated:(BOOL)flag;
- (void)setMyJID_setByServer:(XMPPJID *)newMyJID;
- (void)receivePresence:(XMPPPresence *)presence;
- (void)receiveMessage:(XMPPMessage *)message;
- (void)receiveIQ:(XMPPIQ *)iq;
- (BOOL)connectToHost:(NSString *)host onPort:(UInt16)port withTimeout:(NSTimeInterval)timeout error:(NSError **)errPtr;
- (void)xmppParser:(XMPPParser *)sender didReadRoot:(NSXMLElement *)root;
- (void)xmppParserDidEnd:(XMPPParser *)sender;
- (void)xmppParser:(XMPPParser *)sender didFail:(NSError *)error;
- (void)xmppParserDidParseData:(XMPPParser *)sender;
- (void)xmppParser:(XMPPParser *)sender didReadElement:(NSXMLElement *)element;
- (void)sendElement:(NSXMLElement *)element withTag:(long)tag;
- (void)continueSendElement:(NSXMLElement *)element withTag:(long)tag;
- (void)continueSendIQ:(XMPPIQ *)iq withTag:(long)tag;
- (void)continueSendMessage:(XMPPMessage *)message withTag:(long)tag;
- (void)continueSendPresence:(XMPPPresence *)presence withTag:(long)tag;
- (BOOL)supportsAuthenticationMechanism:(NSString *)mechanismType;
- (void)handleAuth:(NSXMLElement *)authResponse;
- (BOOL)connectWithTimeout:(NSTimeInterval)timeout error:(NSError **)errPtr;
- (void)continueReceiveMessage:(XMPPMessage *)message;
@end
