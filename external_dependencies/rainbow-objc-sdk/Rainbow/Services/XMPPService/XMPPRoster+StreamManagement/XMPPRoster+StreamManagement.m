/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "XMPPRoster+StreamManagement.h"

@interface XMPPRoster ()
- (void)_setRequestedRoster:(BOOL)flag;
- (void)_setHasRoster:(BOOL)flag;
@end

@implementation XMPPRoster (StreamManagement)
- (void)xmppStreamDidDisconnect:(XMPPStream *)sender withError:(NSError *)error {
    dispatch_block_t block = ^{
        @autoreleasepool {
            NSLog(@"[XMPPRoster+StremManagement] Did disconnect, but clean nothing");
            [self _setRequestedRoster:NO];
            
            [earlyPresenceElements removeAllObjects];
        }
    };
    if (dispatch_get_specific(self.moduleQueueTag))
        block();
    else
        dispatch_sync(self.moduleQueue, block);
}

-(void) clearRosterContent {
    dispatch_block_t block = ^{
        @autoreleasepool {
            NSLog(@"[XMPPRoster+StremManagement] Clear roster content");
            if([self autoClearAllUsersAndResources]) {
                [xmppRosterStorage clearAllUsersAndResourcesForXMPPStream:xmppStream];
            } else {
                [xmppRosterStorage clearAllResourcesForXMPPStream:xmppStream];
            }
            [self _setRequestedRoster:NO];
            [self _setHasRoster:NO];
            
            [earlyPresenceElements removeAllObjects];
        }
    };
    
    if (dispatch_get_specific(self.moduleQueueTag))
        block();
    else
        dispatch_sync(self.moduleQueue, block);
}

-(void) clearAllResources {
    dispatch_block_t block = ^{
        @autoreleasepool {
            NSLog(@"[XMPPRoster+StremManagement] Clear all ressources");
            [xmppRosterStorage clearAllResourcesForXMPPStream:xmppStream];
        }
    };
    
    if (dispatch_get_specific(self.moduleQueueTag))
        block();
    else
        dispatch_sync(self.moduleQueue, block);
}
@end
