/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "GroupsService.h"
#import "DownloadManager.h"
#import "ApiUrlManagerService.h"
#import "ContactsManagerService+Internal.h"
#import "XMPPService.h"

@interface GroupsService ()
@property (nonatomic, strong) DownloadManager *downloadManager;
@property (nonatomic, strong) ApiUrlManagerService *apiUrlManagerService;
@property (nonatomic, strong) ContactsManagerService *contactsManagerService;
@property (nonatomic, strong) XMPPService *xmppService;
@property (nonatomic, strong, readwrite) NSMutableArray<Group *> *groups;
@property (nonatomic, strong) NSObject *groupsMutex;
@property (nonatomic) BOOL cacheLoaded;

-(instancetype) initWithDownloadManager:(DownloadManager *) downloadManager apiUrlManagerService:(ApiUrlManagerService *) apiUrlManagerService contactsManagerService:(ContactsManagerService *) contactsManagerService xmppService:(XMPPService *) xmppService;
-(Group *) getGroupByRainbowID:(NSString *) rainbowID;
-(Group *) createGroupWithRainbowID:(NSString *) rainbowID;
-(void) removeGroup:(Group *) group;
-(void) insertContact:(Contact *) contact inGroup:(Group *) group;
-(void) deleteContact:(Contact *) contact inGroup:(Group *) group;
-(void) updateGroup:(Group *) group withName:(NSString *) name andComment:(NSString *) comment;
@end
