/*
 * Rainbow
 *
 * Copyright (c) 2017, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "CallLogsService+Internal.h"
#import "LoginManager+Internal.h"
#import "CallLog+Internal.h"
#import <Rainbow/RainbowUserDefaults.h>

NSString *const kCallLogsServiceDidFetchCallLogs = @"didFetchCallLogs";
NSString *const kCallLogsServiceDidAddCallLog = @"didAddCallLog";
NSString *const kCallLogsServiceDidUpdateCallLog = @"didUpdateCallLog";
NSString *const kCallLogsServiceDidRemoveCallLog = @"didRemoveCallLog";
NSString *const kCallLogsServiceDidRemoveAllCallLogs = @"didRemoveAllCallLogs";
NSString *const kCallLogsServiceDidRemoveCallLogs = @"didRemoveCallLogs";
NSString *const kCallLogsServiceDidUpdateCallLogsUnreadCount = @"didUpdateCallLogsUnreadCount";
NSString *const kCallLogsFullyLoaded = @"callLogsFullyLoaded";

@implementation CallLogsService
-(instancetype) initWithXMPPService:(XMPPService *) xmppService myUser:(MyUser *) myUser {
    self = [super init];
    if(self){
        _cacheLoaded = NO;
        _xmppService = xmppService;
        _xmppService.callLogsDelegate = self;
        _myUser = myUser;
        _callLogs = [NSMutableArray array];
        _peers = [NSMutableArray array];
        _callLogsMutex = [NSObject new];
        _callLogFullyLoaded = [[RainbowUserDefaults sharedInstance] boolForKey:kCallLogsFullyLoaded];;

        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didLogin:) name:kLoginManagerInternalDidLoginSucceeded object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didLogout:) name:kLoginManagerDidLogoutSucceeded object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(willLogin:) name:kLoginManagerInternalWillLogin object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReconnect:) name:kLoginManagerDidReconnect object:nil];
    }
    return self;
}

-(void) dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerInternalDidLoginSucceeded object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidLogoutSucceeded object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerInternalWillLogin object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidReconnect object:nil];
    _xmppService.callLogsDelegate = nil;
    _xmppService = nil;
    _myUser = nil;
    _callLogsMutex = nil;
    [_callLogs removeAllObjects];
    [_peers removeAllObjects];
}

-(void) willLogin:(NSNotification *) notification {
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0), ^{
        if (!_cacheLoaded) {
            NSLog(@"Pre-load callLogs");
            NSArray<CallLog*> *callLogs = [_xmppService fetchCallLogsInCacheForJid:_myUser.contact.jid];
            @synchronized (_callLogsMutex) {
                [self populateCallLogsFromArray:callLogs];
            }
            NSLog(@"Pre-load callLogs done");
            _cacheLoaded = NO;
        }
    });
}

-(void) didLogin:(NSNotification *) notification {
    [self fetchCallLogs];
}

-(void) didReconnect:(NSNotification *) notification {
    if (!_callLogFullyLoaded) {
         NSLog(@"CallLogs not fully loaded => reload");
        [self fetchCallLogs];
    }
}

-(void) didLogout:(NSNotification *) notification {
    [_callLogs removeAllObjects];
    [_peers removeAllObjects];
    [[NSNotificationCenter defaultCenter] postNotificationName:kCallLogsServiceDidRemoveAllCallLogs object:nil];
}

-(void) populateCallLogsFromArray:(NSArray<CallLog *> *) array {

    @synchronized (_callLogsMutex) {
        [array enumerateObjectsUsingBlock:^(CallLog * aCallLog, NSUInteger idx, BOOL * stop) {
            if(![_callLogs containsObject:aCallLog] && aCallLog.peer && (![aCallLog.peer.jid containsString:@"janusgateway"] || ![aCallLog.peer.jid containsString:@"mp_"])){
                [_callLogs addObject:aCallLog];
                if(![_peers containsObject:aCallLog.peer])
                    [_peers addObject:aCallLog.peer];
            }
        }];
        NSLog(@"Nb unread callLogs %ld", (unsigned long)[self getUnreadCallLogs].count);
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:kCallLogsServiceDidUpdateCallLogsUnreadCount object:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:kCallLogsServiceDidFetchCallLogs object:nil];
}

#pragma mark - public api
-(void) deleteCallLogWithPeer:(Peer *) peer {
    [_xmppService deleteCallLogsWithPeer:peer];
    [self xmppService:_xmppService didRemoveCallLogsWithPeer:peer];
}

-(void) deleteAllCallLogs {
    [_xmppService deleteAllCallLogs];
    [self didRemoveAllCallLogs:_xmppService];
}

-(NSInteger) totalNbOfUnreadCallLogs {
    return [self getUnreadCallLogs].count;
}

-(void) markAllCallLogsAsRead {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW,0), ^{
        @synchronized (_callLogsMutex) {
            NSArray <CallLog *> * unreadCallLogs = [self getUnreadCallLogs];
            [unreadCallLogs enumerateObjectsUsingBlock:^(CallLog * callLog, NSUInteger idx, BOOL * stop) {
                [_xmppService markAsReadCallLogWithId:callLog.callLogId];
            }];
        }
        [[NSNotificationCenter defaultCenter] postNotificationName:kCallLogsServiceDidUpdateCallLogsUnreadCount object:nil];
    });
}

#pragma mark - utilities
-(NSArray <CallLog *> *) getCallLogsForPeer:(Peer *) peer {
    NSMutableArray <CallLog *> *calllogs = [NSMutableArray array];
    @synchronized (_callLogsMutex) {
        if([_peers containsObject:peer]){
            [_callLogs enumerateObjectsUsingBlock:^(CallLog * aCallLog, NSUInteger idx, BOOL * stop) {
                if([aCallLog.peer isEqual:peer]){
                    if(![calllogs containsObject:aCallLog])
                        [calllogs addObject:aCallLog];
                }
            }];
        }
    }
    return calllogs;
}

-(void) fetchCallLogs {
    NSLog(@"Fetching callLogs");
    _callLogFullyLoaded = NO;
    [[RainbowUserDefaults sharedInstance] setBool:_callLogFullyLoaded forKey:kCallLogsFullyLoaded];
    [_xmppService loadAllCallLogsWithCompletionHandler:^(NSArray<CallLog *> *archive, NSError *error, NSString *firstElementRetreived, NSString *lastElementRetreived, NSNumber *totalCount) {
        if(!error){
            [self populateCallLogsFromArray:archive];
            _callLogFullyLoaded = YES;
             [[RainbowUserDefaults sharedInstance] setBool:_callLogFullyLoaded forKey:kCallLogsFullyLoaded];
            NSLog(@"Fetching callLogs done ...");
        } else {
            NSLog(@"Error while fetching calllogs %@",error);
        }
    }];
}

-(NSArray <CallLog *> *) getUnreadCallLogs {
    NSMutableArray <CallLog*> * callLogsUnread = [NSMutableArray array];
    @synchronized (_callLogsMutex) {
        [_callLogs enumerateObjectsUsingBlock:^(CallLog * aCallLog, NSUInteger idx, BOOL * stop) {
            if(!aCallLog.isRead && !aCallLog.isOutgoing && aCallLog.state == CallLogStateMissed && ![callLogsUnread containsObject:aCallLog]){
                [callLogsUnread addObject:aCallLog];
            }
        }];
    }
    return callLogsUnread;
}

-(CallLog *) getCallLogByCallLogId:(NSString *) callLogID {
    __block CallLog * callLog = nil;
    @synchronized (_callLogsMutex) {
        [_callLogs enumerateObjectsUsingBlock:^(CallLog * aCallLog, NSUInteger idx, BOOL * stop) {
            if([aCallLog.callLogId isEqualToString:callLogID]){
                callLog = aCallLog;
                *stop = YES;
            }
        }];
    }
    return callLog;
}

#pragma mark - XMPPService delegate
-(void) xmppService:(XMPPService *) service didAddCallLog:(CallLog *) callLog {
    BOOL notify = NO;
    @synchronized (_callLogsMutex) {
        if(![_callLogs containsObject:callLog] && callLog.peer){
            [_callLogs addObject:callLog];
            if(![_peers containsObject:callLog.peer])
                [_peers addObject:callLog.peer];
            notify = YES;
        }
    }
    if(notify){
        [[NSNotificationCenter defaultCenter] postNotificationName:kCallLogsServiceDidAddCallLog object:callLog];
        [[NSNotificationCenter defaultCenter] postNotificationName:kCallLogsServiceDidUpdateCallLogsUnreadCount object:nil];
    }
}

-(void) xmppService:(XMPPService *) service didRemoveCallLogsWithPeer:(Peer *) peer {
    NSArray<CallLog*> * calllogs = nil;
    @synchronized (_callLogsMutex) {
        calllogs = [self getCallLogsForPeer:peer];
        [_callLogs removeObjectsInArray:calllogs];
        NSArray <CallLog *> *callLogsAfterRemove = [self getCallLogsForPeer:peer];
        if(callLogsAfterRemove.count == 0)
            [_peers removeObject:peer];
        if(calllogs.count > 0){
            [[NSNotificationCenter defaultCenter] postNotificationName:kCallLogsServiceDidRemoveCallLogs object:calllogs];
            [[NSNotificationCenter defaultCenter] postNotificationName:kCallLogsServiceDidUpdateCallLogsUnreadCount object:nil];
        }
    }
}

-(void) xmppService:(XMPPService *) service didUpdateCallLogReadState:(NSString *) callLogID {
    CallLog *aCallLog = [self getCallLogByCallLogId:callLogID];
    aCallLog.isRead = YES;
    [[NSNotificationCenter defaultCenter] postNotificationName:kCallLogsServiceDidUpdateCallLog object:aCallLog];
    [[NSNotificationCenter defaultCenter] postNotificationName:kCallLogsServiceDidUpdateCallLogsUnreadCount object:nil];
}

-(void) didRemoveAllCallLogs:(XMPPService *)service {
    @synchronized (_callLogsMutex) {
        [_callLogs removeAllObjects];
        [_peers removeAllObjects];
        [[NSNotificationCenter defaultCenter] postNotificationName:kCallLogsServiceDidRemoveAllCallLogs object:nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:kCallLogsServiceDidUpdateCallLogsUnreadCount object:nil];
    }
}
@end
