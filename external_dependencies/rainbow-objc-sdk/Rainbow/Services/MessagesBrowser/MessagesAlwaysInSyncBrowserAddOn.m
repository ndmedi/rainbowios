/*
 * Rainbow
 *
 * Copyright (c) 2017, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "MessagesAlwaysInSyncBrowserAddOn.h"
#import "LoginManager+Internal.h"

@interface MessagesAlwaysInSyncBrowserAddOn ()
@property (nonatomic) BOOL upToDate;
#if (TARGET_OS_IPHONE || TARGET_IPHONE_SIMULATOR)
@property (nonatomic) UIBackgroundTaskIdentifier resyncTaskID;
#endif
@end

@implementation MessagesAlwaysInSyncBrowserAddOn

-(instancetype) init {
    self = [super init];
    if (self) {
        [self commonInit];
    }
    return self;
}

-(void) commonInit {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didLostNetwork:) name:kLoginManagerInternalDidLostConnection object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReconnect:) name:kLoginManagerDidReconnect object:nil];
    
    _upToDate = YES;
}

-(void) dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidReconnect object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidLostConnection object:nil];
    _browser = nil;
} 

-(void) didLostNetwork:(NSNotification *) notification {
    _upToDate = NO;
#if (TARGET_OS_IPHONE || TARGET_IPHONE_SIMULATOR)
    NSLog(@"[BROWSER-ADDON] didLostNetwork %lu", (unsigned long)_resyncTaskID);
    if(_resyncTaskID != UIBackgroundTaskInvalid){
        [[UIApplication sharedApplication] endBackgroundTask:_resyncTaskID];
        NSLog(@"[BROWSER-ADDON][LRT] *** ending RESYNC (LRTid:%lu) ***", (unsigned long)_resyncTaskID);
    }
#endif
}

-(void) didReconnect:(NSNotification *) notification {
    NSLog(@"[BROWSER-ADDON] didReconnect");
    [self performBrowserResync];
}

-(void) performBrowserResync {
    _upToDate = YES;
    if(_browser){
#if (TARGET_OS_IPHONE || TARGET_IPHONE_SIMULATOR)
        _resyncTaskID = UIBackgroundTaskInvalid;
        _resyncTaskID = [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:^{
            NSLog(@"[BROWSER-ADDON][LRT] **** RESYNC long running task %lu expired ! ***", (unsigned long)_resyncTaskID);
            [[UIApplication sharedApplication] endBackgroundTask:_resyncTaskID];
        }];
#endif
        NSLog(@"[BROWSER-ADDON][LRT] *** starting RESYNC for %@ (LRTid:%lu) ***", _browser, (unsigned long)_resyncTaskID);
        [_browser resyncBrowsingCacheWithCompletionHandler:^(NSArray *addedCacheItems, NSArray *removedCacheItems, NSArray *updatedCacheItems, NSError *error) {
            if (error)
                _upToDate = NO;
            else {
                // invoke the delegate as if added items were events
                if ([addedCacheItems count]>0) {
                    NSLog(@"[BROWSER-ADDON] Simulating items added server events for %@: %@", _browser, addedCacheItems);
                    [_browser.delegate itemsBrowser:_browser didReceiveItemsAddedEvent:addedCacheItems];
                }
                if ([addedCacheItems count]==0 && [removedCacheItems count]==0 && [updatedCacheItems count]==0) {
                    NSLog(@"[BROWSER-ADDON] No change detected during RESYNC for %@", _browser);
                }
            }
#if (TARGET_OS_IPHONE || TARGET_IPHONE_SIMULATOR)
            NSLog(@"[BROWSER-ADDON][LRT] *** ending RESYNC (LRTid:%lu) ***", (unsigned long)_resyncTaskID);
            [[UIApplication sharedApplication] endBackgroundTask:_resyncTaskID];
#endif
        }];
    } else {
        NSLog(@"[BROWSER-ADDON] no resync done because no browser");
    }
}

@end
