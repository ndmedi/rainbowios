/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "CKBrowsingOperation.h"
#import "Peer.h"
#import "XMPPService.h"

typedef void (^MessagePageOperationCompletionHandler) (NSString *firstElementRetreived, NSString * lastElementRetreived, BOOL isCompleted, NSNumber *totalCount);

@interface MessagesPageOperation : CKBrowsingOperation
@property (nonatomic, strong) NSDate *beforeDate;
@property (nonatomic, strong) NSString *lastMessageID;
-(instancetype) initWithPeer:(Peer *) peer withXMPPService:(XMPPService *) xmppService withCompletionHandler:(MessagePageOperationCompletionHandler) completionHandler;

@end
