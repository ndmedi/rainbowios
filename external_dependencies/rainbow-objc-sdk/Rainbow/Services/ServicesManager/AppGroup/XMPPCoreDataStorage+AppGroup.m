/*
 * Rainbow
 *
 * Copyright (c) 2018, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "XMPPCoreDataStorage.h"
#import "Tools.h"

@implementation XMPPCoreDataStorage (AppGroup)

- (NSString *)persistentStoreDirectory {
    NSString *appSupportDirectory = [NSSearchPathForDirectoriesInDomains(NSApplicationSupportDirectory, NSUserDomainMask, YES) firstObject];
    NSString *rainbowAppGroupPath = [Tools rainbowAppGroupPath];
    NSString *appSupportGroupDirectory = [rainbowAppGroupPath stringByAppendingString:@"/Library/Application Support"];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    // Peristent Story Directory now uses the Bundle Identifier
    NSBundle *bundle = [NSBundle mainBundle];
    if ([[bundle.bundleURL pathExtension] isEqualToString:@"appex"]) {
        // Peel off two directory levels - MY_APP.app/PlugIns/MY_APP_EXTENSION.appex
        bundle = [NSBundle bundleWithURL:[[bundle.bundleURL URLByDeletingLastPathComponent] URLByDeletingLastPathComponent]];
    }
    NSString *bundleIdentifier = [bundle objectForInfoDictionaryKey:@"CFBundleIdentifier"];
    NSString *defaultPersistentStoreDirectory  = [appSupportDirectory stringByAppendingPathComponent:bundleIdentifier];
    if (![fileManager fileExistsAtPath:defaultPersistentStoreDirectory]) {
        [fileManager createDirectoryAtPath:defaultPersistentStoreDirectory withIntermediateDirectories:YES attributes:nil error:nil];
    }
    
    NSString *groupPersistentStoreDirectory = defaultPersistentStoreDirectory;
    // Can be nil if there is no suiteName define so we use the default application support directory in that case
    if(rainbowAppGroupPath){
        groupPersistentStoreDirectory= [appSupportGroupDirectory stringByAppendingPathComponent:bundleIdentifier];
        if (![fileManager fileExistsAtPath:groupPersistentStoreDirectory]) {
            [fileManager createDirectoryAtPath:groupPersistentStoreDirectory withIntermediateDirectories:YES attributes:nil error:nil];
        }
        
        NSArray *groupPersistentStoreDirectoryContents = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:groupPersistentStoreDirectory error:nil];
        NSArray *defaultPersistentStoreDirectoryContents = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:defaultPersistentStoreDirectory error:nil];
        
        if(groupPersistentStoreDirectoryContents.count == 0 && defaultPersistentStoreDirectoryContents.count > 0){
            // Need to move content of default cache directory into the group directory
            for (NSString *content in defaultPersistentStoreDirectoryContents) {
                NSError *error = nil;
                NSString *source = [NSString stringWithFormat:@"%@/%@", defaultPersistentStoreDirectory, content];
                NSString *dest = [NSString stringWithFormat:@"%@/%@", groupPersistentStoreDirectory, content];
                [fileManager moveItemAtPath:source toPath:dest error:&error];
                NSLog(@"Error while moving coredatastorage content %@", error);
            }
        }
    }
    return groupPersistentStoreDirectory;
}
@end
