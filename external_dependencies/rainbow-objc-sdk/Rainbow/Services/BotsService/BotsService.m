/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "BotsService.h"
#import "LoginManager+Internal.h"
#import "ContactsManagerService+Internal.h"
#import "NSData+JSON.h"
#import "PINCache.h"

@interface BotsService ()
@property (nonatomic, strong) ContactsManagerService *contactsManager;
@property (nonatomic, strong) DownloadManager *downloadManager;
@property (nonatomic, strong) ApiUrlManagerService *apiUrlManager;
@property (nonatomic, strong) PINCache *botsCache;
@property (nonatomic) BOOL cacheLoaded;

@end

@implementation BotsService

-(instancetype) initWithContactsManagerService:(ContactsManagerService *) contactsManager downloadManager:(DownloadManager *) downloadManager apiUrlManagerService:(ApiUrlManagerService *) apiUrlManager {
    self = [super init];
    if(self){
        _cacheLoaded = NO;
        _contactsManager = contactsManager;
        _downloadManager = downloadManager;
        _apiUrlManager = apiUrlManager;
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didLogin:) name:kLoginManagerInternalDidLoginSucceeded object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didLogout:) name:kLoginManagerDidLogoutSucceeded object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didLostConnection:) name:kLoginManagerDidLostConnection object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(willLogin:) name:kLoginManagerInternalWillLogin object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didChangeUser:) name:kLoginManagerDidChangeUser object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didChangeServer:) name:kLoginManagerDidChangeServer object:nil];
        
        _botsCache = [[PINCache alloc] initWithName:@"botsCache"];
    }
    
    return self;
}


-(void) dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerInternalDidLoginSucceeded object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidLogoutSucceeded object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidLostConnection object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerInternalWillLogin object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidChangeServer object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidChangeUser object:nil];
    _contactsManager = nil;
    _downloadManager = nil;
    _apiUrlManager = nil;
    _botsCache = nil;
}

-(void) didLogin:(NSNotification *) notification {
    NSURL *url = [_apiUrlManager getURLForService:ApiServicesBots];
    NSDictionary *headers = [_apiUrlManager getHTTPHeaderParametersForService:ApiServicesBots];
    NSLog(@"Get bots list");
    [_downloadManager getRequestWithURL:url requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        if (error) {
            NSLog(@"Get bots request returned an error: %@", error);
        } else {
            NSDictionary *response = [receivedData objectFromJSONData];
            if(!response) {
                NSLog(@"Get bots JSON parsing failed");
            } else {
                NSDictionary *data = [response objectForKey:@"data"];
                NSLog(@"Get bots response data ...");
                for (NSDictionary *bots in data) {
                    /*
                     Information are not name like other "users" so rename them
                     id = 578f6554cd9416f80d719886;
                     jid = "emily.openrainbow.org";
                     name = Emily;
                     */
                    NSString *jid = bots[@"jid"];
                    NSString *name = bots[@"name"];
                    NSString *rainbowID = bots[@"id"];
                    [_contactsManager createOrUpdateRainbowBotContactFromJSON:@{@"jid_im": jid, @"lastName": name, @"id": rainbowID}];
                    [_botsCache setObjectAsync:bots forKey:jid completion:nil];
                }
            }
        }
    }];
}

-(void) didLogout:(NSNotification *) notification {
    
}

-(void) didLostConnection:(NSNotification *) notification {
    
}

-(void) willLogin:(NSNotification *) notification {
    if (!_cacheLoaded) {
        NSMutableArray<NSString*> *existingKeys = [NSMutableArray array];
        [_botsCache.diskCache enumerateObjectsWithBlockAsync:^(NSString *key, NSURL *fileURL, BOOL *stop) {
            [existingKeys addObject:key];
        } completionBlock:^(id<PINCaching> cache) {
            for (NSString * jid in existingKeys) {
                NSDictionary *bots = [_botsCache objectForKey:jid];
                NSString *jid = bots[@"jid"];
                NSString *name = bots[@"name"];
                NSString *rainbowID = bots[@"id"];
                [_contactsManager createOrUpdateRainbowBotContactFromJSON:@{@"jid_im": jid, @"lastName": name, @"id": rainbowID}];
            }
        }];
        _cacheLoaded = NO;
    }
}

-(void) didChangeUser:(NSNotification *) notification {
    [_botsCache removeAllObjects];
}

-(void) didChangeServer:(NSNotification *) notification {
    [_botsCache removeAllObjects];
}
@end
