/*
 * Rainbow
 *
 * Copyright (c) 2018, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import <WebRTC/WebRTC.h>
#import "RTCService+Internal.h"

@implementation RTCPeerConnection (Description)
-(NSString *) description {
    return [NSString stringWithFormat:@"RTCPeerConnection <%p configuration %@ ICEGatheringState %@ ICEConnectionState %@ RTCSignalingState %@>",self, self.configuration, [RTCService stringForRTCIceGatheringState:self.iceGatheringState], [RTCService stringForRTCIceConnectionState:self.iceConnectionState], [RTCService stringForRTCSignalingState:self.signalingState]];
}
@end
