//
//  PhoneStateOneActiveCall.m
//  Rainbow
//
//  Created by Jean-Luc on 21/05/2018.
//  Copyright © 2018 ALE. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PhoneStateInitMakeCall.h"
#import "AllPhoneStates.h"


@implementation PhoneStateOneActiveCall

- (void) startMakeCall
{
    OTCLog (@"event startMakeCall : new business call ?");
}


- (void) releaseEvent:(Call*) aCall
{
    OTCLog (@"releaseEvent : change to Idle");
    [self.telService removeCall:aCall];
//    BOOL isActiveCall = ([self.telService.currentActiveCall isEqual:aCall]);  // JLF : euh, can still exist after remove ???
//    if (isActiveCall)
    [self.telService setState:[PhoneStateIdle class]];
}


- (void) gsmIdle
{
    OTCLog (@"gsmIdle : change to Idle");
    [self.telService removeCall:nil];   // all calls ???
    [self.telService setState:[PhoneStateIdle class]];
}


- (void) heldEvent:(Call*) aCall
{
    OTCLog (@"heldEvent : no state change, notify");
    [self.telService notifyCallToDisplay:aCall];
}


- (void) activeEvent:(Call*) aCall
{
    OTCLog (@"activeEvent : retrieve ? Notify");
    [self.telService notifyCallToDisplay:aCall];
}

@end
