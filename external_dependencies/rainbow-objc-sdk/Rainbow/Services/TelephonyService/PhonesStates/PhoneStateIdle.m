//
//  PhoneStateIdle.m
//  Rainbow
//
//  Created by Jean-Luc on 21/05/2018.
//  Copyright © 2018 ALE. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PhoneStateIdle.h"
#import "AllPhoneStates.h"


@implementation PhoneStateIdle

- (void) doEnter
{
    [self.telService stopTimer];
}


- (void) startMakeCall
{
    OTCLog (@"startMakeCall : change to InitMakeCall");
    [self.telService setState:[PhoneStateInitMakeCall class]];
}


- (void) incomingRingEvent:(Call*) aCall
{
    OTCLog (@"incomingRingEvent : change to EvtRingingIncoming");
    [self.telService setState:[PhoneStateEvtRingingIncoming class]];
}


- (void) gsmRinging:(CXCall *) call
{
    OTCLog (@"gsmRinging : change to GsmRinging");
    [self.telService setState:[PhoneStateGsmRinging class]];
}

/**
 * The phone is off hook.

- (void) gsmConnected:(CXCall *) call
{
    OTCLog (@"gsmConnected : change to GsmConnected");
    [self.telService setState:[PhoneStateGsmConnected class]];
}
 */

@end

