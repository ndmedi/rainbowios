//
//  PhoneStateGsmRinging.m
//  Rainbow
//
//  Created by Jean-Luc on 21/05/2018.
//  Copyright © 2018 ALE. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PhoneStateGsmRinging.h"
#import "AllPhoneStates.h"


@implementation PhoneStateGsmRinging

- (void) doEnter
{
}


- (void) gsmRinging:(CXCall*) call
{
    OTCLog (@"event GsmRinging : simultaneous incoming GSM and webRtc call ?");
}


- (void) gsmConnected:(CXCall*) call
{
    OTCLog (@"gsmConnected : change to GsmConnected");
    [self.telService setState:[PhoneStateGsmConnected class]];
}


- (void) incomingRingEvent:(Call*) aCall
{
    if (aCall.isMediaPillarCall)
    {
        OTCLog (@"incomingRingEvent : show rtcCall in CallKit");
        [self.telService addRtcCallAsOutgoing:NO];
    }
    OTCLog (@"incomingRingEvent : change to RingingIncoming");
    [self.telService setState:[PhoneStateRingingIncoming class]];
}

@end

