//
//  PhoneStateGsmConnected.m
//  Rainbow
//
//  Created by Jean-Luc on 21/05/2018.
//  Copyright © 2018 ALE. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PhoneStateGsmConnected.h"
#import "AllPhoneStates.h"


@implementation PhoneStateGsmConnected

- (void) gsmRinging:(CXCall*) cxCall
{
    if (cxCall)
    {
        OTCLog (@"event gsmRinging -> new GSM call ?");
    }
    else
    {
        OTCLog (@"event gsmRinging during private GSM call -> new MP call !");
        [self.telService setState:[PhoneStateGsmRinging class]];
    }
}


- (void) activeEvent:(Call*) aCall
{
    OTCLog (@"activeEvent : change to ActiveCall");
    [self.telService notifyCallToDisplay:aCall];
    [self.telService setState:[PhoneStateOneActiveCall class]];
}

@end
