//
//  PhoneStateOutgoingRinging.m
//  Rainbow
//
//  Created by Jean-Luc on 21/05/2018.
//  Copyright © 2018 ALE. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PhoneStateInitMakeCall.h"
#import "AllPhoneStates.h"
#import "Call+Internal.h"


@implementation PhoneStateOutgoingRinging

- (void) doEnter
{
    [self.telService stopTimer];
}


- (void) startMakeCall
{
    OTCLog (@"PhoneState event : startMakeCall -> new business call ?");
}


-(void) outgoingRingEvent:(Call*) aCall callCause:(NSString*) callCause
{
    OTCLog (@"outgoingRingEvent no change, update call");
    [self.telService notifyCallToDisplay:aCall];
}


- (void) activeEvent:(Call*) aCall
{
    OTCLog (@"activeEvent : change to ActiveCall");
    [self.telService notifyCallToDisplay:aCall];
    [self.telService setState:[PhoneStateOneActiveCall class]];
}


- (void) releaseEvent:(Call*) aCall
{
    OTCLog (@"releaseEvent : change to Idle");
    [self.telService removeCall:aCall];
    [self.telService setState:[PhoneStateIdle class]];
}

@end

