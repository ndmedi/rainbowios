/*
 * Rainbow
 *
 * Copyright (c) 2018, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "TelephonyService+Internal.h"
#import "ServicesManager+Internal.h"
#import "RTCService+Internal.h"
#import "DownloadManager.h"
#import "PhoneNumberInternal.h"
#import "NomadicStatus+Internal.h"
#import "MediaPillarStatus+Internal.h"
#import "MyUser+Internal.h"
#import "NSData+JSON.h"
#import "NSDictionary+JSONString.h"
#import "CallForwardStatus+Internal.h"
#import "Call+Internal.h"
#import "Peer+Internal.h"
#import "ContactInternal.h"
#import "ContactsManagerService+Internal.h"
#import "AllPhoneStates.h"
#import "NSString+URLEncode.h"
#import "XMPPServiceProtocol.h"
#import "CallEvent.h"
#import "NSObject+NotNull.h"
#import "CallEvent+Internal.h"
#import "CallParticipant+Internal.h"


NSString *const kTelephonyServiceDidAddCallNotification = @"TelephonyServiceDidAddCallNotification";
NSString *const kTelephonyServiceDidUpdateCallNotification = @"TelephonyServiceDidUpdateCallNotification";
NSString *const kTelephonyServiceDidRemoveCallNotification = @"TelephonyServiceDidRemoveCallNotification";
NSString *const kTelephonyServiceDidFailedToCallNotification = @"TelephonyServiceDidFailedToCallNotification";

typedef NS_ENUM (NSInteger, SnapshotStatus) {
    SnapshotStatusUnknown,
    SnapshotStatusInProgress,
    SnapshotStatusDone,
    SnapshotStatusFailed
};


@interface TelephonyService () <XMPPServiceTelephonyDelegate, CXCallObserverDelegate>

@property (nonatomic, strong) MyUser *myUser;
@property (nonatomic, strong) DownloadManager *downloadManager;
@property (nonatomic, strong) ApiUrlManagerService *apiUrlManagerService;
@property (nonatomic, strong) XMPPService *xmppService;
@property (nonatomic, strong) ContactsManagerService *contactsManagerService;
@property (nonatomic, strong) RTCService *rtcService;

#if TARGET_OS_IPHONE
@property (nonatomic, strong) CXCallObserver *callObserver;
@property (nonatomic) UIBackgroundTaskIdentifier waitingGsmTaskId;
#endif
@property (nonatomic, strong) PhoneState* currentPhoneState;
@property (nonatomic, strong) NSObject *currentPhoneStateMutex;

@property (nonatomic, strong) NSMutableArray *calls;
@property (nonatomic, strong) NSObject *callsMutex;
@property (nonatomic, strong) NSString* currentOutgoingCallRef;
@property (nonatomic, strong) NSString* currentOutgoingCallNumber;
@property (nonatomic, strong) NSString* rtcSessionId;
@property (nonatomic, strong) NSString* rtcRessource;
@property (nonatomic, strong) Peer* rtcPeer;
@property (nonatomic, copy) NSUUID* rtcCallId;

@property (nonatomic, copy, readwrite) TelephonyServiceMakeCallCompletionHandler makeCallCompletionHandler;
@property (nonatomic, strong) NSTimer* makeCallTimer;
@property SnapshotStatus getTelephonicStateStatus;

@end


#define SIMUL_BACKGROUND 0


@implementation TelephonyService

-(instancetype) initWithXMPPService:(XMPPService *) xmppService myUser:(MyUser*) myUser downloadManager:(DownloadManager *) downloadManager apiUrlManagerService:(ApiUrlManagerService *) apiUrlManagerService contactsManagerService:(ContactsManagerService *) contactsManagerService rtcService:(RTCService * _Nonnull) rtcService {
    self = [super init];
    if (self) {
        _xmppService = xmppService;
        _myUser = myUser;
        _xmppService.telephonyDelegate = self;
        _downloadManager = downloadManager;
        _apiUrlManagerService = apiUrlManagerService;
        _contactsManagerService = contactsManagerService;
        _rtcService = rtcService;
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didMyUserFeatureDidUpdate:) name:kMyUserFeatureDidUpdate object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loginManagerDidReconnect:) name:kLoginManagerDidReconnect object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loginManagerDidReconnect:) name:kLoginManagerDidLoginSucceeded object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onRtcSessionEnded:) name:kRTCServiceDidReceiveEndedSession object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onRtcSessionConnected:) name:kRTCServiceDidReceiveConnectedSession object:nil];

        _currentPhoneStateMutex = [NSObject new];
        
        @synchronized(_currentPhoneStateMutex) {
            _currentPhoneState = [PhoneStateIdle new];
            _currentPhoneState.telService = self;
            _currentPhoneState.rtcService = _rtcService;
        }
#if (TARGET_OS_IPHONE || TARGET_IPHONE_SIMULATOR)
        _callObserver = [CXCallObserver new];
        [_callObserver setDelegate:self queue:dispatch_get_main_queue()];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidBecomeActive:) name:UIApplicationDidBecomeActiveNotification object:nil];
#endif
        
        _calls = [NSMutableArray array];
        _callsMutex = [NSObject new];
        self.waitingGsmTaskId = UIBackgroundTaskInvalid;
        _getTelephonicStateStatus = SnapshotStatusUnknown;
    }
    return self;
}


-(void) dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kMyUserFeatureDidUpdate object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidReconnect object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidLoginSucceeded object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kRTCServiceDidReceiveEndedSession object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kRTCServiceDidReceiveConnectedSession object:nil];

    _downloadManager = nil;
    _apiUrlManagerService = nil;
    _myUser = nil;
    _xmppService.telephonyDelegate = nil;
    _xmppService = nil;
    _contactsManagerService = nil;
#if (TARGET_OS_IPHONE || TARGET_IPHONE_SIMULATOR)
    _callObserver = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidBecomeActiveNotification object:nil];
#endif
    _calls = nil;
    _callsMutex = nil;
    
    _currentPhoneState.telService = nil;
    _currentPhoneState = nil;
    _currentPhoneStateMutex = nil;
}


/**
 *  On login event, we fetch nomadic status if required.
 */
-(void) didMyUserFeatureDidUpdate:(NSNotification*) notification {
    [self fetchNomadicStatus];
    [self fetchForwardStatus];
    [self fetchMediaPillarStatus];
}


#pragma mark - mediapillar routing

-(void) fetchMediaPillarStatus {
    if (![_myUser isAllowedToUseTelephonyWebRTCtoPSTN]) {
        OTCLog (@"TelephonyWebRTCGateway (MediaPillar) not allowed.");
        return;
    }
    
    [_rtcService fetchMediaPillarDataWithCompletionHandler:^(NSString *mediaPillarJid, NSString *rainbowPhoneNumber, NSString *prefix, NSError *error) {
        MediaPillarStatus *newStatus = _myUser.mediaPillarStatus;
        newStatus.jid = mediaPillarJid;
        newStatus.rainbowPhoneNumber = rainbowPhoneNumber;
        newStatus.prefix = prefix;
        newStatus.featureActivated = [_myUser isAllowedToUseTelephonyWebRTCtoPSTN];
        
        if (!error) {
            OTCLog (@"fetchMediaPillarStatus - Register to webRTCGateway");
            [_rtcService webRTCGatewayRegisterRequest:mediaPillarJid number:rainbowPhoneNumber displayName:_myUser.contact.fullName secret:rainbowPhoneNumber];
        }
    }];
}


-(NSString *) getMediaPillarNumberWithPrefix {
    NSString *mediaPillarNumberWithPrefix = @"";
    
    if (_myUser.mediaPillarStatus)
        mediaPillarNumberWithPrefix = [_myUser.mediaPillarStatus.prefix stringByAppendingString:_myUser.mediaPillarStatus.rainbowPhoneNumber];
    
    return mediaPillarNumberWithPrefix;
}

#pragma mark - nomadic routing

-(void) fetchNomadicStatus {
    if (![_myUser isAllowedToUseTelephonyNomadicMode]) {
        OTCLog  (@"TelephonyNomadicMode not allowed.");
        return;
    }
    
    NSURL *url = [_apiUrlManagerService getURLForService:ApiServicesTelephony,@"nomadic"];
    NSDictionary *headers = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesTelephony];
    OTCLog (@"Get telephony nomadic status");
    [_downloadManager getRequestWithURL:url requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        if (error) {
            OTCLog (@"Error: Cannot get my user telephony nomadic status in REST : %@ %@", error, [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding]);
            return;
        }
        
        NSDictionary *jsonResponse = [receivedData objectFromJSONData];
        if (!jsonResponse) {
            OTCLog (@"Error: Cannot get my user telephony nomadic status in REST due to JSON parsing failure");
            return;
        }
        
        OTCLog (@"Updating nomadic status");
        NSDictionary *status = [jsonResponse objectForKey:@"data"];
        NSString *destinationString = [status objectForKey:@"destination"];
        NomadicStatus *newStatus = _myUser.nomadicStatus;
        
        if (destinationString && [destinationString length] > 0) {
            if ([destinationString isEqualToString:[self getMediaPillarNumberWithPrefix]]) {
                // Nomadic is configured on mediapillar
                // Update the mediapillar status to activated
                _myUser.mediaPillarStatus.activated = YES;
            } else {
                _myUser.mediaPillarStatus.activated = NO;
            }
            
            PhoneNumber *phone = [PhoneNumber new];
            phone.numberE164 = destinationString;
            newStatus.destination = phone;
        }
        
        newStatus.activated = ([[status objectForKey:@"modeActivated"] isEqualToString:@"true"]) ? YES : NO;
        newStatus.featureActivated = ([[status objectForKey:@"featureActivated"] isEqualToString:@"true"]) ? YES : NO;
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kMyUserNomadicStatusDidUpdate object:newStatus];
    }];
}


-(void) nomadicLoginWithPhoneString:(NSString * _Nonnull)phoneNumber withVoIP:(BOOL)voip withCompletionBlock:(void (^ _Nullable)(NSError * _Nullable error))completionBlock {
    [self changeNomadicModeActived:YES withPhoneNumber:phoneNumber withVoIP:voip withCompletionBlock:completionBlock];
}


-(void) nomadicLoginWithPhoneNumber:(PhoneNumber * _Nonnull)phoneNumber withVoIP:(BOOL)voip withCompletionBlock:(void (^ _Nullable)(NSError * _Nullable error))completionBlock {
    [self changeNomadicModeActived:YES withPhoneNumber:phoneNumber.number withVoIP:voip withCompletionBlock:completionBlock];
}


-(void) nomadicLogoutWithCompletionBlock:(void (^ _Nullable)(NSError * _Nonnull error))completionBlock {
    [self changeNomadicModeActived:NO withPhoneNumber:nil withVoIP:NO withCompletionBlock:completionBlock];
}


-(void) changeNomadicModeActived:(BOOL)activate withPhoneNumber:(NSString*) phoneNumber withVoIP:(BOOL)voip withCompletionBlock:(void (^ _Nullable)(NSError * _Nullable error))completionBlock {
    NSURL *url;
    NSDictionary *body = nil;
    NSString *thePhoneNumber = phoneNumber;
    if (activate) {
        if (voip) {
            thePhoneNumber = [self getMediaPillarNumberWithPrefix];
        }
        if ([thePhoneNumber length] > 0) {
            url = [_apiUrlManagerService getURLForService:ApiServicesTelephony,@"nomadic/login"];
            body = @{@"destinationExtNumber": thePhoneNumber};
            OTCLog (@"Update nomadic status to login");
        }
        else {
            if (completionBlock)
                completionBlock([NSError errorWithDomain:@"TelephonyService" code:400 userInfo:@{NSLocalizedDescriptionKey:@"Nomadic mode login require a phone number"}]);
            OTCLog (@"Login to telephony nomadic require a phone number");
            return;
        }
    }
    else {
        url = [_apiUrlManagerService getURLForService:ApiServicesTelephony,@"nomadic/logout"];
        OTCLog (@"Update nomadic status to logout");
    }
    
    NSDictionary *headers = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesTelephony];
    [_downloadManager putRequestWithURL:url body:[body jsonStringWithPrettyPrint:NO] requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        if (error) {
            OTCLog (@"Error: Cannot change telephony nomadic status in REST : %@ %@", error, [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding]);
            if (completionBlock)
                completionBlock(error);
            return;
        }
        
        NSDictionary *jsonResponse = [receivedData objectFromJSONData];
        if (!jsonResponse) {
            OTCLog (@"Error: Cannot change telephony nomadic status in REST due to JSON parsing failure");
            if (completionBlock)
                completionBlock(error);
            return;
        }
        
        //NSDictionary *loginStatus = [jsonResponse objectForKey:@"nomadicLogin"];
        NSString *requestStatus = [jsonResponse objectForKey:@"status"];
        NomadicStatus *newStatus = _myUser.nomadicStatus;
        newStatus.featureActivated = YES;
        BOOL updateDone = NO;
        
        //if ([[loginStatus objectForKey:@"login"] isEqualToString:@"false"]) {
        if (activate && [requestStatus isEqualToString:@"Nomadic Login successfully sent"]) {
            PhoneNumber *phone = [PhoneNumber new];
            phone.number = phoneNumber;
            newStatus.destination = phone;
            newStatus.activated = YES;
            updateDone = YES;
            _myUser.mediaPillarStatus.activated = voip;
            
        } else if (!activate && [requestStatus isEqualToString:@"Nomadic Logout successfully sent"]) {
            newStatus.activated = NO;
            updateDone = YES;
            _myUser.mediaPillarStatus.activated = NO;
        }
        
        if (!updateDone) {
            if (completionBlock)
                completionBlock([NSError errorWithDomain:@"TelephonyService" code:500 userInfo:@{NSLocalizedDescriptionKey:@"Nomadic mode login/logout failed"}]);
        } else {
            [[NSNotificationCenter defaultCenter] postNotificationName:kMyUserNomadicStatusDidUpdate object:newStatus];
            if (completionBlock)
                completionBlock(nil);
        }
    }];
}


-(void) xmppService:(XMPPService *) service didReceiveNomadicStatusUpdate:(NomadicStatus *) status {
    OTCLog (@"Did receive Nomadic status update %@", status);
    if (![status isEqual:_myUser.nomadicStatus]) {
        _myUser.nomadicStatus = status;
        _myUser.mediaPillarStatus.activated = [status.destination.number isEqual:[self getMediaPillarNumberWithPrefix]];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kMyUserNomadicStatusDidUpdate object:status];
    }
}


#pragma mark - forward routing

-(void) fetchForwardStatus {
    if (![_myUser isAllowedToUseTelephonyCallForward])
        return;
    
    NSURL *url = [_apiUrlManagerService getURLForService:ApiServicesTelephony,@"forward"];
    NSDictionary *headers = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesTelephony];
    OTCLog (@"Get telephony forward status");
    [_downloadManager getRequestWithURL:url requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        if (error) {
            OTCLog (@"Error: Cannot get my user telephony call forward status in REST : %@ %@", error, [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding]);
            return;
        }
    }];
    
}


-(void) xmppService:(XMPPService *) service didReceiveCallForwardStatusUpdate:(CallForwardStatus *) status {
    OTCLog (@"Did receive message for call forward %@", status);
    if (![status isEqual:_myUser.callForwardStatus]) {
        _myUser.callForwardStatus = status;
        [[NSNotificationCenter defaultCenter] postNotificationName:kMyUserCallForwardDidUpdate object:status];
    }
}


-(void) setForwardToExternalPhoneNumber:(PhoneNumber * _Nonnull)phoneNumber withCompletionBlock:(void (^ _Nullable)(NSError * _Nullable error))completionBlock {
    [self setForwardTo:@{@"calleeExtNumber": phoneNumber.number}  withCompletionBlock:completionBlock];
}


-(void) setForwardToExternalPhoneString:(NSString * _Nonnull)phoneNumber withCompletionBlock:(void (^ _Nullable)(NSError * _Nullable error))completionBlock {
    [self setForwardTo:@{@"calleeExtNumber": phoneNumber}  withCompletionBlock:completionBlock];
}


-(void) setForwardToVoicemailWithCompletionBlock:(void (^ _Nullable)(NSError * _Nullable error))completionBlock {
    if (_myUser.voiceMailNumber == nil) {
        completionBlock( [NSError errorWithDomain:@"TelephonyService" code:400 userInfo:@{NSLocalizedDescriptionKey:@"Unable to update call forward to voicemail, phone number not defined in user profile."}]);
        return;
    }
    
    [self setForwardTo:@{@"calleeIntNumber": _myUser.voiceMailNumber } withCompletionBlock:completionBlock];
}


-(void) cancelForwardWithCompletionBlock:(void (^ _Nullable)(NSError * _Nullable error))completionBlock {
    [self setForwardTo:@{@"calleeIntNumber": @"CANCELFORWARD"} withCompletionBlock:completionBlock];
}


-(void) setForwardTo:(NSDictionary * _Nonnull)calleeDic withCompletionBlock:(void (^ _Nullable)(NSError *_Nullable error))completionBlock {
    NSURL *url;
    NSDictionary *body = nil;
    
    if (calleeDic) {
        url = [_apiUrlManagerService getURLForService:ApiServicesTelephony,@"forward"];
        body = calleeDic;
        OTCLog (@"Update telephony forward");
    } else {
        if (completionBlock)
            completionBlock([NSError errorWithDomain:@"TelephonyService" code:400 userInfo:@{NSLocalizedDescriptionKey:@"Forward to device require a phone number"}]);
        OTCLog (@"Forward to device require a phone number");
        return;
    }
    
    NSDictionary *headers = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesTelephony];
    [_downloadManager putRequestWithURL:url body:[body jsonStringWithPrettyPrint:NO] requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        if (error) {
            OTCLog (@"Error: Cannot change telephony forward in REST : %@ %@", error, [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding]);
            if (completionBlock)
                completionBlock(error);
            return;
        }
        
        NSDictionary *jsonResponse = [receivedData objectFromJSONData];
        if (!jsonResponse) {
            OTCLog (@"Error: Cannot change telephony forward in REST due to JSON parsing failure");
            if (completionBlock)
                completionBlock(error);
            return;
        }
        
        if (completionBlock)
            completionBlock(nil);
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kMyUserCallForwardDidUpdate object:nil];
    }];
}

#pragma mark - Voicemail count

-(void) xmppService:(XMPPService *) service didReceiveVoicemailCountUpdate:(NSInteger) count {
    OTCLog (@"Did receive Voicemail count update %ld", (long)count);
    if (count != _myUser.voiceMailCount) {
        _myUser.voiceMailCount = count;
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kMyUserVoicemailCountDidUpdate object:nil];
    }
}


#pragma mark - Calls management

-(NSArray*) currentCallsToDisplay
{
    NSMutableArray* callsToDisplay = [NSMutableArray new];
    if (_calls.count > 1)
        OTCLog (@"_calls are %@", _calls);
    for (Call* eachCall in [_calls copy])
    {
        if (eachCall.readyToDisplay)
            [callsToDisplay addObject:eachCall];
    }
    OTCLog (@"----> currentCallsToDisplay : %@", callsToDisplay);
    if (callsToDisplay.count)
        return callsToDisplay;
//    else
//        return _rtcService.calls;

    return nil;
}


-(Call*) currentActiveCall
{
    Call* activeCall = nil;
    for (Call* eachCall in [_calls copy])
    {
        if (eachCall.status == CallStatusEstablished)
            activeCall = eachCall;
    }
    if (activeCall)
        OTCLog (@"currentActiveCall is : %@", activeCall);
    else
        OTCLog (@"No currentActiveCall");
    return activeCall;
}


-(void) notifyCallToDisplay:(Call* _Nullable) aCall
{
    if (_makeCallCompletionHandler)
    {
        _makeCallCompletionHandler(nil);
        _makeCallCompletionHandler = nil;
    }
    
    if (!aCall)
    {
        @synchronized(_callsMutex)
        {
            aCall = [_calls firstObject];
        }
    }
    if (aCall.readyToDisplay)
    {
        OTCLog (@"notifyCallToDisplay : update %@", aCall);
        [[NSNotificationCenter defaultCenter] postNotificationName:kTelephonyServiceDidUpdateCallNotification object:aCall];
    }
    else
    {
        OTCLog (@"notifyCallToDisplay : add %@", aCall);
        aCall.readyToDisplay = YES;
        [[NSNotificationCenter defaultCenter] postNotificationName:kTelephonyServiceDidAddCallNotification object:aCall];
    }
}


// addCall and updateCall are deprecated !
-(void) addCall:(Call* _Nullable) newCall {
    // We don't add the object into the calls array that is done automatically when the call object is created by createOrUpdateCall methods
    Call * theCall = newCall;
    OTCLog (@"Add : %@", theCall);
    if (_makeCallCompletionHandler) {
        _makeCallCompletionHandler(nil);
        _makeCallCompletionHandler = nil;
    }
    
    if (!theCall) {
        @synchronized(_callsMutex) {
            theCall = [_calls firstObject];
        }
    }
    if (newCall.readyToDisplay)
        [[NSNotificationCenter defaultCenter] postNotificationName:kTelephonyServiceDidAddCallNotification object:theCall];
}


-(void) updateCall:(Call*) updatedCall {
    OTCLog (@"Update : %@", updatedCall);
    if (_makeCallCompletionHandler) {
        _makeCallCompletionHandler(nil);
        _makeCallCompletionHandler = nil;
    }
    if (updatedCall.readyToDisplay)
        [[NSNotificationCenter defaultCenter] postNotificationName:kTelephonyServiceDidUpdateCallNotification object:updatedCall];
}


-(void) removeCall:(Call*) oldCall
{
    OTCLog (@"Remove : %@", oldCall);
    __block Call* theCall = oldCall;
    if (!theCall)
    {
        @synchronized(_callsMutex) {
            theCall = [_calls firstObject];
            if (theCall)
                OTCLog (@"Call to remove is nil, assume that is a cxCall, use the first call (%@)", theCall.callRef);   // Remove all calls ? JLF : à revoir en double-appel !
            else
            {
                OTCLog (@"Call to remove is unknown (%@)", oldCall.callRef);   // Remove all calls ? JLF : à revoir en double-appel !
                return;
            }
        }
    }
    
    @synchronized(_callsMutex) {
        [_calls removeObject:theCall];
        OTCLog (@"call removed (%@), _calls.count = %lu", theCall.callRef, (unsigned long)_calls.count);
    }
    // Take care that incoming call is done on 2 callRef, on mobile answer, the 2nd one is released, and then we pass active on 1st one : we have no call during a few time, but RTCWeb session is still active
    if (_calls.count == 0 && _rtcService.calls.count == 0)
        _rtcSessionId = nil;

    // CRRAINB-3828 (logs 8/11) : the callRef in callEvents is not the one received on makeCall ! And is never reset, then incoming mp calls are seen as outgoing !
    if ([theCall.callRef isEqualToString:_currentOutgoingCallRef] || _calls.count == 0)
    {
        _currentOutgoingCallRef = nil;
        _currentOutgoingCallNumber = nil;
    }
    if (_makeCallCompletionHandler)
    {
        NSError* error = nil;
        if ( (theCall.status == CallStatusCanceled || theCall.status == CallStatusHangup) && theCall.callCause && ![theCall.callCause isEqualToString:@"NORMALCLEARING"] && ![theCall.callCause isEqualToString:@"ReleaseDuringMakeCall"] )
        {
            error = [NSError errorWithDomain:@"Telephony" code:500 userInfo:@{NSLocalizedFailureReasonErrorKey:theCall.callCause}];
        }
        _makeCallCompletionHandler(error);
        _makeCallCompletionHandler = nil;
    }
    if (theCall.readyToDisplay)
    {
        OTCLog (@"notify removed call '%@'", theCall.callRef);
        theCall.status = CallStatusHangup;   // in case removeCall is called for a CxCall, will induce dismiss of RTCcallVC
        [[NSNotificationCenter defaultCenter] postNotificationName:kTelephonyServiceDidRemoveCallNotification object:theCall];
    }
    if (_calls.count == 0 )
        _getTelephonicStateStatus = SnapshotStatusUnknown;
}


-(void) getTelephonicState
{
    if (_getTelephonicStateStatus == SnapshotStatusInProgress)
    {
        OTCLog (@"getTelephonicState already in progress...");
        return;
    }

    NSURL* url = [_apiUrlManagerService getURLForService:ApiServicesTelephony,@"snapshotdevice?deviceType=SECONDARY"];
    NSMutableDictionary* headers = [NSMutableDictionary dictionaryWithDictionary:[_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesTelephony]];
    [headers removeObjectForKey:@"Content-Type"];
    
    OTCLog (@"getTelephonicState request....");
    _getTelephonicStateStatus = SnapshotStatusInProgress;
    [_downloadManager getRequestWithURL:url requestHeadersParameter:headers completionBlock:^(NSData* receivedData, NSError* error, NSURLResponse* response) {
        if (error)
        {
            OTCLog (@"Error: Cannot getTelephonicState in REST : %@ %@", error, [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding]);
            _getTelephonicStateStatus = SnapshotStatusFailed;
            return;
        }
        
        NSDictionary* jsonResponse = [receivedData objectFromJSONData];
        if (!jsonResponse)
        {
            OTCLog (@"Error: Cannot getTelephonicState in REST due to JSON parsing failure");
            _getTelephonicStateStatus = SnapshotStatusFailed;
            return;
        }

        NSDictionary* dictData = jsonResponse[@"data"];
        if (dictData)
        {
            NSMutableArray* callRefs = [NSMutableArray new];
            NSArray* connections = [[dictData objectForKey:@"connections"] notNull];
            OTCLog (@"getTelephonicState done : %lu connection(s)", (unsigned long)connections.count);
            for (NSDictionary* aConnection in connections) {
                OTCLog (@"getTelephonicState has connection : %@", aConnection);
                NSString* callID = [[aConnection objectForKey:@"callId"] notNull];
                NSString* endpointIM = [[aConnection objectForKey:@"endpointIm"] notNull];
                NSString* endpointLci = [[aConnection objectForKey:@"endpointLci"] notNull];
                NSString* endpointTel = [[aConnection objectForKey:@"endpointTel"] notNull];
                NSDictionary* identity = [[aConnection objectForKey:@"identity"] notNull];
                NSString* firstName = [[identity objectForKey:@"firstName"] notNull];
                NSString* lastName = [[identity objectForKey:@"lastName"] notNull];
                if (!firstName.length && !lastName.length)
                    lastName = [[identity objectForKey:@"displayName"] notNull];
                NSString* lci = [[aConnection objectForKey:@"lci"] notNull];
                
                CallEvent* aCallEvent = [CallEvent new];
                aCallEvent.callReference = callID;
                [callRefs addObject:callID];
                
                if ([[aConnection objectForKey:@"participants"] notNull]) {
                    // TODO: Parse participants, for conferences ??
                } else {
                    CallParticipant* callParticipant = [CallParticipant new];
                    callParticipant.jid = endpointIM;
                    callParticipant.number = endpointTel;
                    callParticipant.firstname = firstName;
                    callParticipant.lastname = lastName;
                    
                    [aCallEvent.callParticipants addObject:callParticipant];
                }
                
                if ([lci isEqualToString:@"LCI_CONNECTED"] && ([endpointLci isEqualToString:@"LCI_CONNECTED"] || [endpointLci isEqualToString:@"LCI_UNKNOWN"])) {
                    aCallEvent.state = CallStateActive;
                } else if ([lci isEqualToString:@"LCI_HELD"] && [endpointLci isEqualToString:@"LCI_CONNECTED"]) {
                    aCallEvent.state = CallStateHeld;
                } else if ([lci isEqualToString:@"LCI_CONNECTED"] && [endpointLci isEqualToString:@"LCI_HELD"]) {
                    aCallEvent.state = CallStateHeld;
                } else if ([lci isEqualToString:@"LCI_CONNECTED"] && [endpointLci isEqualToString:@"LCI_ALERTING"]) {
                    aCallEvent.state = CallStateRingingOutgoing;
                } else if ([lci isEqualToString:@"LCI_QUEUED"] && [endpointLci isEqualToString:@"LCI_CONNECTED"]) {
                    aCallEvent.state = CallStateRingingIncoming;
                } else if ([lci isEqualToString:@"LCI_HELD"]) {
                    aCallEvent.state = CallStateRingingIncoming;
                } else if ([lci isEqualToString:@"LCI_ALERTING"]) {
                    aCallEvent.state = CallStateRingingIncoming;
                } else
                    aCallEvent.state = CallStateUnknown;
                
                [self xmppService:nil didReceiveCallEvent:aCallEvent];
            }
            
            // Check now if we still have a released call :
            for (Call* ourCall in [_calls copy])
            {
                BOOL stillExist = NO;
                for (NSString* aCallRef in callRefs)
                {
                    if ([aCallRef isEqualToString:ourCall.callRef])
                        stillExist = YES;
                }
                if (stillExist)
                {
                    OTCLog (@"getTelephonicState : we still have the call '%@'.", ourCall.callRef);
                }
                else
                {
                    OTCLog (@"getTelephonicState : call '%@' has disappeared ! Remove it.", ourCall.callRef);
                    [self removeCall:ourCall];
                }
            }
            _getTelephonicStateStatus = SnapshotStatusDone;
        }
        else
        {
            OTCLog (@"getTelephonicState done, no data retrieved");
        }
    }];
}


-(void) addRtcCallAsOutgoing:(BOOL) isOutgoingCall
{
    if ([_rtcService hasActiveCalls])
    {
        OTCLog (@"cannot addRtcCall, there are already rtcCalls : %@", _rtcService.calls);
        return;
    }

    _rtcPeer.rtcJid = _rtcPeer.jid;
    Call* firstCall =  [_calls firstObject];
    firstCall.isMediaPillarCall = YES;
    Contact* peerContact = (Contact*)firstCall.peer;
    PhoneNumber* firsNumber = peerContact.phoneNumbers.firstObject;
    NSString* peerNumber = peerContact.calledNumber;
    if (!peerNumber.length)
    {
        if (_currentOutgoingCallNumber.length)
            peerNumber = _currentOutgoingCallNumber;
        else
            peerNumber = firsNumber.number;
    }
    if (peerNumber.length)
        _rtcPeer.calledNumber = peerNumber;
//    else
//       keep an eventual number from propose ?
//        peerNumber = _rtcPeer.calledNumber;

    NSString* dispName = peerContact.displayName;
    if (!dispName.length && _currentOutgoingCallNumber.length)
    {
        Contact* aContact = [_contactsManagerService createOrUpdateRainbowContactWithPhoneNumber:_currentOutgoingCallNumber];
        if (aContact.displayName)
            dispName = aContact.displayName;
        OTCLog (@"addRtcCall outgoing with displayName '%@'", dispName);
    }
    if (!dispName.length)
    {
        if (peerNumber.length)
        {
            OTCLog (@"addRtcCall with number as displayName '%@'", peerNumber);
            _rtcPeer.displayName = peerNumber;
        }
        else if (_rtcPeer.displayName.length)
            OTCLog (@"addRtcCall with propose displayName '%@' (may be false...)", _rtcPeer.displayName);
        else
            OTCLog (@"addRtcCall no displayName ? From call '%@'", firstCall);
    }
    else
        _rtcPeer.displayName = dispName;

    NSString* mpMedia = isOutgoingCall ? @"mediaPillarMakeCall" : @"mediaPillar";
    OTCLog (@"addRtcCall with sessionId '%@', displayName '%@', number '%@' and ressource '%@' (media : '%@')", _rtcSessionId, _rtcPeer.displayName, _rtcPeer.calledNumber, _rtcRessource, mpMedia);
    [_rtcService didReceiveProposeMsg:_rtcSessionId withMedias:@[@"audio", mpMedia] from:_rtcPeer resource:_rtcRessource];
}


- (RTCCall*) rtcCallFromCxCallOne:(CXCall*) cxCall
{
    if (!cxCall)
    {
        OTCLog (@"rtcCallFromCxCallOne 'nil' : use first rtcCall");
        return _rtcService.calls.firstObject;
    }
    
    NSArray* rtcCalls = [_rtcService.calls copy];
    for (RTCCall* rtCall in rtcCalls)
    {
        if ([rtCall.callID isEqual:cxCall.UUID])
            return rtCall;
    }
    
    OTCLog (@"rtcCallFromCxCallOne from cxCall.UUID '%@' not found", cxCall.UUID);
    return nil;
}


- (void) onRtcSessionEnded:(NSNotification*) notification
{
    NSString* sessionId = notification.object;
    if ([sessionId isEqualToString:_rtcSessionId]) // && _currentPhoneState.class == PhoneStateGsmRinging.class) // JLF : all states if no CallKit (China)
    {
        OTCLog (@"simulate 'gsmIdle:nil' event onRtcSessionEnded (%@)", sessionId);
        _rtcSessionId = nil;
        [_currentPhoneState gsmIdle:nil];
    }
}


// if no CallKit (China), we'll never receive the CxCall.hasConnected
- (void) onRtcSessionConnected:(NSNotification*) notification
{
    NSString* sessionId = notification.object;
    if ([sessionId isEqualToString:_rtcSessionId])
    {
        OTCLog (@"simulate 'gsmConnected:nil' event onRtcSessionConnected (%@)", sessionId);
        [_currentPhoneState gsmConnected:nil];
    }
}


#pragma mark - 3PCC calls management methods

-(void) makeCallTo:(PhoneNumber*) phoneNumber fallBackHandler:(TelephonyServiceMakeCallFallBack) fallbackHandler completionHandler:(TelephonyServiceMakeCallCompletionHandler) completionHandler {
    
    if (!_myUser.isAllowedToUseTelephony) {
        if (fallbackHandler) {
            NSError* error = [NSError errorWithDomain:@"TelephonyService" code:500 userInfo:@{NSLocalizedFailureReasonErrorKey:@"User don't have the right to use this feature"}];
            fallbackHandler(error, phoneNumber);
            return;
        }
    }
    
    if (!_myUser.isNomadicModeActivated) {
        if (fallbackHandler) {
            NSError* error = [NSError errorWithDomain:@"TelephonyService" code:500 userInfo:@{NSLocalizedFailureReasonErrorKey:@"User does not have the nomadic mode activated"}];
            fallbackHandler(error, phoneNumber);
            return;
        }
    }
    
    if (_currentPhoneState.class != PhoneStateIdle.class)
    {
        if (completionHandler)
        {
            NSError* error = [NSError errorWithDomain:@"TelephonyService" code:502 userInfo:@{NSLocalizedFailureReasonErrorKey:@"Cannot make call : not in idle state (temporary)"}];
            completionHandler(error);
        }
        return;
    }

    NSURL* url = [_apiUrlManagerService getURLForService:ApiServicesTelephony, @"calls"];
    NSDictionary* headers = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesTelephony];
    NSDictionary* body = @{@"calleeExtNumber":phoneNumber.number, @"secondaryDeviceMakeCall":@"true"};
    OTCLog (@"makeCall (%@ ?) with body %@", (_myUser.mediaPillarStatus.activated ? @"webRTC" : @"cellular"), body);
    _currentOutgoingCallNumber = phoneNumber.number;

    @synchronized(_currentPhoneStateMutex) {
        [_currentPhoneState startMakeCall];
    }
    
    if (completionHandler) {
        _makeCallCompletionHandler = nil;
        _makeCallCompletionHandler = [completionHandler copy];
    }
    
    [self stopTimer];
    __weak __typeof__(self) weakSelf = self;
    _makeCallTimer = [NSTimer scheduledTimerWithTimeInterval:30 repeats:NO block:^(NSTimer* timer) {
        OTCLog (@"MAKE CALL TIMER TIMEOUT AFTER 30s");
        @synchronized(_currentPhoneStateMutex) {
            [weakSelf cancelOutgoingCall];
            [_currentPhoneState timerOff];
        }
        if (fallbackHandler) {
            NSError* error = [NSError errorWithDomain:@"TelephonyService" code:500 userInfo:@{NSLocalizedFailureReasonErrorKey:@"Make call action failed in allowed delay (30s) fallback"}];
            fallbackHandler(error, phoneNumber);
        }
    }];
    OTCLog  (@"Make call timer started");

    [_downloadManager postRequestWithURL:url body:[body jsonStringWithPrettyPrint:NO] requestHeadersParameter:headers completionBlock:^(NSData* receivedData, NSError* error, NSURLResponse* response) {
        if (error) {
            NSString* errDesc = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
            OTCLog (@"Error: Cannot make call in REST : %@ %@", error, errDesc);
            @synchronized(_currentPhoneStateMutex) {
                [weakSelf.currentPhoneState abortMakeCall];
            }
            if (_makeCallCompletionHandler)
            {
                error = [NSError errorWithDomain:@"Telephony" code:error.code userInfo:@{NSLocalizedFailureReasonErrorKey:errDesc}];
                _makeCallCompletionHandler(error);
                _makeCallCompletionHandler = nil;
            }
            return;
        }

        NSDictionary* jsonResponse = [receivedData objectFromJSONData];
        if (!jsonResponse) {
            OTCLog (@"Error: Cannot make call in REST due to JSON parsing failure");
            @synchronized(_currentPhoneStateMutex) {
                [weakSelf.currentPhoneState abortMakeCall];
            }
            if (_makeCallCompletionHandler)
            {
                NSError* error = [NSError errorWithDomain:@"TelephonyService" code:501 userInfo:@{NSLocalizedFailureReasonErrorKey:@"Cannot make call in REST due to JSON parsing failure"}];
                _makeCallCompletionHandler(error);
                _makeCallCompletionHandler = nil;
            }
            return;
        }

        OTCLog (@"MAKE CALL DONE %@", jsonResponse);
        NSDictionary* jsonData = jsonResponse[@"data"];
        if (jsonData) {
            NSString* callRef = jsonData[@"callId"];
            if (callRef.length)
                weakSelf.currentOutgoingCallRef = callRef;
        }
    }];
}


-(void) cancelOutgoingCall
{
    if (_currentOutgoingCallRef == nil)
    {
        OTCLog (@"cannot cancel outgoing call, no currentOutgoingCallRef");
    }

    Call* outgoingCall = [self getCallByCallReference:_currentOutgoingCallRef];
    if (!outgoingCall)
    {
        outgoingCall = [Call new];
        outgoingCall.callRef = _currentOutgoingCallRef;
    }
    OTCLog (@"cancel outgoing call '%@'", outgoingCall.callRef);
    [self stopTimer];
    [self releaseCall:outgoingCall];
}


-(void) releaseCall:(Call *) call {
    // For test only
    call.status = CallStatusHangup;
    [[NSNotificationCenter defaultCenter] postNotificationName:kTelephonyServiceDidUpdateCallNotification object:call];

    if ([call.callRef isEqualToString:_currentOutgoingCallRef])
    {
        _currentOutgoingCallRef = nil;
        _currentOutgoingCallNumber = nil;
    }

    NSURL *url = [_apiUrlManagerService getURLForService:ApiServicesTelephony, [NSString stringWithFormat:@"calls/%@", [call.callRef urlEncode]]];
    NSDictionary *headers = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesTelephony];
    
    [_downloadManager deleteRequestWithURL:url requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        if (error) {
            OTCLog (@"Error: Cannot release call in REST : %@ %@", error, [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding]);
            return;
        }
        
        NSDictionary *jsonResponse = [receivedData objectFromJSONData];
        if (!jsonResponse) {
            OTCLog (@"Error: Cannot release call in REST due to JSON parsing failure");
            return;
        }
        
        OTCLog (@"Release call done %@", jsonResponse);
    }];
    if (call.isMediaPillarCall && _calls.count == 1)
    {
        OTCLog (@"Release unique call, also release RTC one ? (as Android)");
        RTCCall* rtCall = [self rtcCallFromCxCallOne:nil];
        [_rtcService hangupCall:rtCall];
    }
}


-(void) answerCall:(Call *) call {
    NSURL *url = [_apiUrlManagerService getURLForService:ApiServicesTelephony, [NSString stringWithFormat:@"calls/%@/answer", [call.callRef urlEncode]]];
    NSDictionary *headers = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesTelephony];
    [_downloadManager putRequestWithURL:url body:nil requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        if (error) {
            OTCLog (@"Error: Cannot answer call in REST : %@ %@", error, [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding]);
            return;
        }
        
        NSDictionary *jsonResponse = [receivedData objectFromJSONData];
        if (!jsonResponse) {
            OTCLog (@"Error: Cannot answer call in REST due to JSON parsing failure");
            return;
        }
        
        OTCLog (@"Answer call done %@", jsonResponse);
    }];
}


-(void) deflectCall:(Call*) call toPhoneNumber:(PhoneNumber*) phoneNumber {
    if ([call isKindOfClass:[RTCCall class]])
    {
        Call* incCall = [self getCallByState:CallStatusRinging];
        if (!incCall)
        {
            OTCLog (@"Deflect request received for RtcCall, no incoming call");
            return;
        }
        OTCLog (@"Deflect request received for RtcCall, use incoming call '%@'", incCall.callRef);
        call = incCall;
    }
    
    NSURL* url = [_apiUrlManagerService getURLForService:ApiServicesTelephony,[NSString stringWithFormat:@"calls/%@/deflect", [call.callRef urlEncode]]];
    NSDictionary* headers = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesTelephony];
    
    NSDictionary* body = @{@"calleeExtNumber":phoneNumber.number};    // VM : int ou ext ? Les 2 OK... --> Ext as Android !
    [_downloadManager putRequestWithURL:url body:[body jsonStringWithPrettyPrint:NO] requestHeadersParameter:headers completionBlock:^(NSData* receivedData, NSError* error, NSURLResponse* response) {
        if (error) {
            OTCLog (@"Error: Cannot deflect call in REST : %@ %@", error, [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding]);
            return;
        }
        
        NSDictionary* jsonResponse = [receivedData objectFromJSONData];
        if (!jsonResponse) {
            OTCLog (@"Error: Cannot deflect call in REST due to JSON parsing failure");
            return;
        }
        
        OTCLog (@"Deflect call done %@", jsonResponse);
    }];
}


-(void) holdCall:(Call*) call
{
    if ([call isKindOfClass:[RTCCall class]])
    {
        Call* activeCall = [self getCallByState:CallStatusEstablished];
        if (!activeCall)
        {
            OTCLog (@"Hold received for RtcCall, no active call");
            return;
        }
        OTCLog (@"Hold received for RtcCall, use active call '%@'", activeCall.callRef);
        call = activeCall;
   }

    NSURL* url = [_apiUrlManagerService getURLForService:ApiServicesTelephony, [NSString stringWithFormat:@"calls/%@/hold", [call.callRef urlEncode]]];
    NSDictionary* headers = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesTelephony];
    
    [_downloadManager putRequestWithURL:url body:nil requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        if (error)
        {
            OTCLog (@"Error: Cannot hold call in REST : %@ %@", error, [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding]);
            return;
        }
        
        NSDictionary *jsonResponse = [receivedData objectFromJSONData];
        if (!jsonResponse)
        {
            OTCLog (@"Error: Cannot hold call in REST due to JSON parsing failure");
            return;
        }
        
        OTCLog (@"Hold call done %@", jsonResponse);
    }];
}


-(void) retrieveCall:(Call*) call
{
    if ([call isKindOfClass:[RTCCall class]])
    {
        Call* heldCall = [self getCallByState:CallStatusHeldByUser];
        if (!heldCall)
        {
            OTCLog (@"Retrieve received for RtcCall, no held call");
            return;
        }
        OTCLog (@"Retrieve received for RtcCall, use held call '%@'", heldCall.callRef);
        call = heldCall;
    }
    NSURL* url = [_apiUrlManagerService getURLForService:ApiServicesTelephony, [NSString stringWithFormat:@"calls/%@/retrieve", [call.callRef urlEncode]]];
    NSDictionary* headers = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesTelephony];
    
    OTCLog (@"retrieve call request for call '%@'", call.callRef);
    [_downloadManager putRequestWithURL:url body:nil requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        if (error)
        {
            OTCLog (@"Error: Cannot retrieve call in REST : %@ %@", error, [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding]);
            return;
        }
        
        NSDictionary *jsonResponse = [receivedData objectFromJSONData];
        if (!jsonResponse)
        {
            OTCLog (@"Error: Cannot retrieve call in REST due to JSON parsing failure");
            return;
        }
        
        OTCLog (@"retrieve call done %@", jsonResponse);
    }];
}


-(void) transferCall:(Call *) call toHeldCall:(Call *) heldCall {
    NSURL *url = [_apiUrlManagerService getURLForService:ApiServicesTelephony,[NSString stringWithFormat:@"calls/%@/transfer/%@", [call.callRef urlEncode], [heldCall.callRef urlEncode]]];
    NSDictionary *headers = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesTelephony];
    
    [_downloadManager putRequestWithURL:url body:nil requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        if (error) {
            OTCLog (@"Error: Cannot transfer call in REST : %@ %@", error, [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding]);
            return;
        }
        
        NSDictionary *jsonResponse = [receivedData objectFromJSONData];
        if (!jsonResponse) {
            OTCLog (@"Error: Cannot transfer call in REST due to JSON parsing failure");
            return;
        }
        
        OTCLog (@"Transfer call done %@", jsonResponse);
    }];
}


#pragma mark - State machine

-(void) setState:(Class) newPhoneState {
    @synchronized(_currentPhoneStateMutex) {
        _currentPhoneState.telService = nil;
        _currentPhoneState.rtcService = nil;
        _currentPhoneState = nil;
        _currentPhoneState = [newPhoneState new];
        _currentPhoneState.telService = self;
        _currentPhoneState.rtcService = _rtcService;
        [_currentPhoneState doEnter];
    }
}


//-(void) startTimerOf:(NSTimeInterval) duration withBlock:(TelephonyServiceMakeCallCompletionHandler _Nullable) fireBlock
-(void) startTimerOf:(NSTimeInterval) duration
{
    [self stopTimer];

    _makeCallTimer = [NSTimer scheduledTimerWithTimeInterval:duration repeats:NO block:^(NSTimer * timer) {
        OTCLog  (@"call timer fired");
        @synchronized(_currentPhoneStateMutex) {
            [_currentPhoneState timerOff];
        }
    }];
    OTCLog  (@"call timer started");
}


-(void) stopTimer
{
    if (_makeCallTimer)
    {
        OTCLog  (@"call timer invalidated");
        [_makeCallTimer invalidate];
        _makeCallTimer = nil;
    }
}


#pragma mark - Xmpp telephony events

-(void) xmppService:(XMPPService*) service didReceiveCallEvent:(CallEvent*) callEvent
// JLF : mettre une operationQueue au lieu de tous ces "@synchronized" !??
{
    if ([callEvent.deviceType isEqualToString:@"MAIN"])   // Warning : no deviceType on makeCall
    {
//        OTCLog (@"state machine ignores MAIN device %@", callEvent);
        if ([callEvent.callReference isEqualToString:_currentOutgoingCallRef])   // JLF : should never arrive ?
            _currentOutgoingCallRef = nil;
        return;
    }
#if SIMUL_BACKGROUND
    OTCLog (@"didReceiveCallEvent ignored, no _rtcSessionId (SIMUL_BACKGROUND)");
    if (!_rtcSessionId)
        return;
#endif

    OTCLog (@"in state machine %@, did receive %@", _currentPhoneState, callEvent);
    Call* concernedCall = [self createOrUpdateCallWithCallEvent:callEvent];
    if (!concernedCall)
    {
        OTCLog (@"didReceiveCallEvent ignored, no concernedCall");
        return;
    }

    switch (callEvent.state) {
        case CallStateActive: {
            // Don't reset the timer if we already have the information (from callkit) ??? Mainly after a hold/retrieve
            if (!concernedCall.connectionDate)
                concernedCall.connectionDate = [NSDate date];
            
            @synchronized(_currentPhoneStateMutex) {
                    [_currentPhoneState activeEvent:concernedCall];
            }
            break;
        }
        case CallStateIdle:
        case CallStateReleasing:
        {
            @synchronized(_currentPhoneStateMutex) {
                [_currentPhoneState releaseEvent:concernedCall];
            }
            break;
        }
            
        case CallStateRingingIncoming:
        {
            [_currentPhoneState incomingRingEvent:concernedCall];
            break;
        }
            
        case CallStateUnknown:
            OTCLog (@"state 'Unknown', participant displayName may have been updated : '%@'", concernedCall.peer.displayName);
            if (concernedCall.status == CallStatusEstablished)
                [self updateCall:concernedCall];
            break;
            
        case CallStateOffHook:
            break;

        case CallStateDialing:
            break;

        case CallStateHeld:
            @synchronized(_currentPhoneStateMutex) {
                [_currentPhoneState heldEvent:concernedCall];
            }
            break;

        case CallStateRingingOutgoing: {
            @synchronized(_currentPhoneStateMutex) {
                [_currentPhoneState outgoingRingEvent:concernedCall callCause:callEvent.callCause];
            }
            break;
        }
        default:
            break;
    }
}


-(void) xmppService:(XMPPService*) service didReceiveMpCall:(NSString*) sessionID withPeer:(Peer*) mpPeer andResource:(NSString*) resource
{
    self.rtcSessionId = sessionID;
    self.rtcPeer = mpPeer;
    self.rtcRessource = resource;
    @synchronized(_currentPhoneStateMutex) {
        OTCLog (@"mpIncomingCall -> GSMRinging %@", _currentPhoneState);
        [_currentPhoneState gsmRinging:nil];
    }
    if (!_xmppService.isXmppConnected)
        [self getTelephonicState];
    [self testAndRestartXmpp];
#if SIMUL_BACKGROUND
    [NSThread sleepForTimeInterval:1];
    [self getTelephonicState];
#endif
}


#pragma mark - CXCallObserver Delegate

/*
 Invoked on __main__ thread (euh... Sure ?)
-(void) onGsmCallStateChanged:(CTCall*) ctCall currentPhoneState:(PhoneState *) currentPhoneState {
    OTCLog  (@"[TELEPHONYSVC] CT Center callback for call : %@", ctCall);
    if ([ctCall.callState isEqualToString:CTCallStateIncoming]) {
        [_currentPhoneState gsmRinging];
    }
    else if ([ctCall.callState isEqualToString:CTCallStateDialing]) {
    }
    else if ([ctCall.callState isEqualToString:CTCallStateConnected]) {
        [currentPhoneState gsmConnected];
        [self stopTimer];
    }
    else if ([ctCall.callState isEqualToString:CTCallStateDisconnected]) {
        [_currentPhoneState gsmIdle];
    }
}
 */


- (void) callObserver:(CXCallObserver*) callObserver callChanged:(CXCall*) cxCall
{
    BOOL isCertainlyOurRtcSession = _rtcSessionId.length && [cxCall.UUID.UUIDString hasPrefix:@"FA00"];
    OTCLog (@"callObserver CALL %@%@.", cxCall, isCertainlyOurRtcSession ? @" (ourRtcSession)" : @"");

    // Check ended before connected as an ended call is generally connected before !
    if (cxCall.hasEnded)
    {
        if ([cxCall.UUID isEqual:_rtcCallId])
        {
            OTCLog (@"callObserver : our rtcCall has been released");
            _rtcCallId = nil;
        }
        else if (_calls.count > 0 && !_rtcCallId) // || _currentPhoneState.class != PhoneStateIdle.class ???
        {
            OTCLog (@"callObserver : our GSM call has been released");// KO si call mp mis en garde par privé GSM !
        }
        else if (_currentPhoneState.class == PhoneStateGsmRinging.class || _currentPhoneState.class == PhoneStateGsmConnected.class)
        {
            OTCLog (@"callObserver : a private gsm or webRTC call has been released, go to PhoneStateIdle");
        }
        else
        {
            OTCLog (@"callObserver : an unknown (private gsm or webRTC 1PCC ?) call has been released, ignore it");
            return;
        }

        @synchronized(_currentPhoneStateMutex) {
            OTCLog (@"callObserver -> GSMIdle %@", _currentPhoneState);
            [_currentPhoneState gsmIdle:cxCall];
        }
        return;
    }
    if (cxCall.hasConnected) {
        @synchronized(_currentPhoneStateMutex) {
            OTCLog (@"callObserver -> GSMConnected %@", _currentPhoneState);
            [_currentPhoneState gsmConnected:cxCall];
//            [self stopWaitingGsmLRT]; maybe in a few seconds ? After the GUI has displayed the localNotif...
        }
        [self stopTimer];
        return;
    }
    if (cxCall.onHold) {
        OTCLog (@"callObserver -> cxCall on hold, wait event %@", _currentPhoneState);
        // TODO: Implement onHold state for call --> OK ?
        return;
    }
    if (cxCall.outgoing) {
        // TODO: implement GSM outgoing call
        OTCLog (@"callObserver -> GSMOutgoing %@", _currentPhoneState);
        if (isCertainlyOurRtcSession)
            self.rtcCallId = cxCall.UUID;
        return;
    }
    // if we don't exit earlier that means we have an incoming call
    @synchronized(_currentPhoneStateMutex) {
        OTCLog (@"callObserver -> GSMRinging %@", _currentPhoneState);
        if (isCertainlyOurRtcSession)
            self.rtcCallId = cxCall.UUID;
        [_currentPhoneState gsmRinging:cxCall];
        [self testAndRestartXmpp];
        [self startWaitingGsmLRTIfNeeded];
    }
}


// If app is woken-up by push, and sees an incoming gsm, start a LRT waiting GSM answer to show local notif "back to app"
-(void) startWaitingGsmLRTIfNeeded
{
#if (TARGET_OS_IPHONE || TARGET_IPHONE_SIMULATOR)
    UIApplication* app = [UIApplication sharedApplication];
    UIApplicationState appState = app.applicationState;

    if (appState != UIApplicationStateActive && !_xmppService.isXmppConnected)  // && _waitingGsmTaskId == UIBackgroundTaskInvalid)
    {
        // better use this instead of LRT with sleep, so we'll close correctly the socket
        [[ServicesManager sharedInstance] applicationWillResignActive:[UIApplication sharedApplication]];
        return;
/*
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0), ^{
            _waitingGsmTaskId = [app beginBackgroundTaskWithExpirationHandler:^{
                OTCLog (@" **** waitingGsm background task expired ! (terminating id: %lu ) ***", (unsigned long)_waitingGsmTaskId);
                [app endBackgroundTask:_waitingGsmTaskId];
                _waitingGsmTaskId = UIBackgroundTaskInvalid;
            }];
            OTCLog (@" **** starting waitingGsm background task (id: %lu) ***", (unsigned long)_waitingGsmTaskId);
            [NSThread sleepForTimeInterval:30];  // to keep app awake until user's answer
            OTCLog (@" **** normal end of waitingGsm background task (id: %lu) ***", (unsigned long)_waitingGsmTaskId);
            [app endBackgroundTask:_waitingGsmTaskId];
            _waitingGsmTaskId = UIBackgroundTaskInvalid;
        });
*/
    }
#endif
}


-(void) stopWaitingGsmLRT
{
#if (TARGET_OS_IPHONE || TARGET_IPHONE_SIMULATOR)
    if (_waitingGsmTaskId != UIBackgroundTaskInvalid)
    {
        OTCLog (@" *** end of vibration background task (id: %lu) ***", (unsigned long)_waitingGsmTaskId);
        [[UIApplication sharedApplication] endBackgroundTask:_waitingGsmTaskId];
        _waitingGsmTaskId = UIBackgroundTaskInvalid;
    }
#endif
}


#pragma mark - Application/Session life

-(void) applicationDidBecomeActive:(NSNotification *) notification
{
}


-(void) loginManagerDidReconnect:(NSNotification*) notification
{
    // We can have missed a new call... Or a release.
    if (_callObserver.calls.count > 0 || _calls.count > 0 || _currentPhoneState.class != PhoneStateIdle.class)
    {
        if (_getTelephonicStateStatus == SnapshotStatusInProgress)
        {
            OTCLog (@"login done, waiting getTelephonicState response...");
        }
        else
        {
            OTCLog (@"login done and getTelephonicState old or failed, an event may have been lost, check again...");
            [self getTelephonicState];
        }
    }
}


- (void) testAndRestartXmpp
{
    if (!_xmppService.isXmppConnected)
    {
        OTCLog(@"need XMPP re-connection");
        [[ServicesManager sharedInstance] checkTokenExpirationAndRenewIt];
    }
}


#pragma mark - CRUD calls

-(Call*) getCallByCallReference:(NSString*) callReference
{
    NSString* shortCallRef = [[callReference componentsSeparatedByString:@"#"] firstObject];   // the 2nd part can be the peer.
    Call* call = nil;
    @synchronized(_callsMutex) {
        for (Call* aCall in _calls)
        {
            if ([aCall.shortCallRef isEqualToString:shortCallRef])
            {
                call = aCall;
                break;
            }
        }
    }
    
    return call;
}


-(Call*) getCallByState:(CallStatus) callState
{
    Call* call = nil;
    @synchronized(_callsMutex) {
        for (Call* aCall in _calls)
        {
            if (aCall.status == callState)
            {
                call = aCall;
                break;
            }
        }
    }
    
    return call;
}


-(Call*) createOrUpdateCallWithCallEvent:(CallEvent*) callEvent
{
    Call* aCall = nil;
    BOOL newCall = NO;
    @synchronized(_callsMutex) {
        aCall = [self getCallByCallReference:callEvent.callReference];
        if (!aCall && callEvent.callNewRef.length)
        {
            NSString* shortOutgoingCallRef = [[_currentOutgoingCallRef componentsSeparatedByString:@"#"] firstObject];
            NSString* shortEventRef = [[callEvent.callReference componentsSeparatedByString:@"#"] firstObject];
            if ([shortEventRef isEqualToString:shortOutgoingCallRef])
                _currentOutgoingCallRef = callEvent.callReference;
        }

        if (!aCall)
        {
            if (callEvent.state == CallStateUnknown && ![_currentOutgoingCallRef isEqualToString:callEvent.callReference])
            {
                OTCLog (@"createOrUpdateCallWithCallEvent : update for another call '%@' !", callEvent.callReference);
                return nil;
            }
            if (callEvent.state == CallStateReleasing && ![_currentOutgoingCallRef isEqualToString:callEvent.callReference])
            {
                OTCLog (@"createOrUpdateCallWithCallEvent : release for an unknown (or already released) call '%@'...", callEvent.callReference);
                return nil;
            }

            aCall = [Call new];
            aCall.callRef = callEvent.callReference;
            [_calls addObject:aCall];
            newCall = YES;
            OTCLog (@"createOrUpdateCallWithCallEvent : new call added (%@), _calls.count = %lu", aCall.callRef, (unsigned long)_calls.count);
        }
    }
    
    if (callEvent.callNewRef.length)
    {
        OTCLog (@"CallRef '%@' is replaced by '%@'", callEvent.callReference, callEvent.callNewRef);
        aCall.callRef = callEvent.callNewRef;
    }
    
    if (_rtcSessionId.length)
    {
        aCall.isMediaPillarCall = YES;
    }
    else
    {
        aCall.isMediaPillarCall = NO;
    }
//    OTCLog (@"call '%@' isMediaPillarCall : %@ (rtcSessionId = '%@')", aCall.callRef, aCall.isMediaPillarCall ? @"YES" : @"NO", _rtcSessionId);

    if (callEvent.callParticipants.count > 0)
    {
        // we create only one peer for now
        // Search for the rainbow contact based on the jid
        BOOL canUpdateCallPeer = YES;
        CallParticipant* aParticipant = [callEvent.callParticipants firstObject];
        Contact* currentCallContact = (Contact*)(aCall.peer);

        if (!currentCallContact)
        {
            currentCallContact = [_contactsManagerService getContactWithJid:aParticipant.jid];
            if (currentCallContact)
                OTCLog (@"createOrUpdateCallWithCallEvent : contact found from jid : %@", currentCallContact);
            if (!currentCallContact && aParticipant.jid)
            {
                currentCallContact = [_contactsManagerService createOrUpdateRainbowContactWithJid:aParticipant.jid];
                if (currentCallContact)
                {
                    OTCLog (@"createOrUpdateCallWithCallEvent : contact created from jid : %@", currentCallContact);
                    if (aParticipant.number.length && ![currentCallContact getPhoneNumberFromString:aParticipant.number])
                    {
                        PhoneNumber* newPhNbr = [[PhoneNumber alloc] initWithPhoneNumberString: aParticipant.number];  // No deviceType ?
                        [currentCallContact.rainbow addPhoneNumberObject:newPhNbr];
//                        [ContactsManagerServiceToolKit insertOrUpdatePhoneNumber:newPhNbr forRainbowContact:currentCallContact];
                        OTCLog (@"createOrUpdateCallWithCallEvent : phone added to %@", currentCallContact);
                    }
                }
            }
            
            if (!currentCallContact)
            {
                currentCallContact = [_contactsManagerService createOrUpdateRainbowContactWithPhoneNumber:aParticipant.number];
                OTCLog (@"createOrUpdateCallWithCallEvent : contact created from phoneNumber : %@", currentCallContact);
            }
            currentCallContact.calledNumber = aParticipant.number;
        }

        aCall.peer = currentCallContact;
        if (currentCallContact)
        {
            if (![currentCallContact getPhoneNumberFromString:aParticipant.number])  // ignore update if different number, cf Cédric
            {
                OTCLog (@"createOrUpdateCallWithCallEvent : don't update call from participant with different number '%@' !", aParticipant.number);
                canUpdateCallPeer = NO;
            }
            
            // On garde l'ancien code au cas où ???
            if ([aParticipant.lastname containsString:@"REX"] || [aParticipant.lastname containsString:@"REX"])  // ignorer lastName REX 304, number 314, voir Cédric
            {
                OTCLog (@"createOrUpdateCallWithCallEvent : don't update call from participant with 'REX' !");
                canUpdateCallPeer = NO;
            }

            if (canUpdateCallPeer)   // A terme : if (peerNumber && ![aParticipant.number isEqualToString:peerNumber])  // ignore update if different number, cf Cédric
            {
                NSMutableDictionary* dic = [NSMutableDictionary new];
                if (!currentCallContact.firstName && aParticipant.firstname.length)
                    [dic setObject:aParticipant.firstname forKey:@"firstName"];
                if (!currentCallContact.lastName && aParticipant.lastname.length)
                    [dic setObject:aParticipant.lastname forKey:@"lastName"];
                if (dic.count)
                {
                    [_contactsManagerService updateRainbowContact:currentCallContact withJSONDictionary:dic];
                    OTCLog (@"createOrUpdateCallWithCallEvent : contact updated : %@", currentCallContact);
                }
            }
        }
        else
            OTCLog (@"createOrUpdateCallWithCallEvent : we cannot find/create a contact from the callEvent informations %@", callEvent);
    }

    if (callEvent.callCause)
        aCall.callCause = [NSString stringWithString:callEvent.callCause];
    
    switch (callEvent.state)
    {
        case CallStateUnknown: {
            
            break;
        }
        case CallStateOffHook: {
            
            break;
        }
        case CallStateIdle: {
            aCall.status = CallStatusCanceled;
            break;
        }
        case CallStateReleasing: {
            aCall.status = CallStatusHangup;
            break;
        }
        case CallStateDialing: {
            aCall.status = CallStatusConnecting;
            aCall.isIncoming = NO;
            break;
        }
        case CallStateRingingOutgoing: {
            aCall.status = CallStatusRinging;
            aCall.isIncoming = NO;
            break;
        }
        case CallStateRingingIncoming: {
            aCall.status = CallStatusRinging;
            aCall.isIncoming = YES;
            break;
        }
        case CallStateActive: {
            aCall.status = CallStatusEstablished;
            break;
        }
        case CallStateHeld: {
            if ([callEvent.callReference isEqualToString:aCall.callRef])
            {
                OTCLog (@"call '%@' has been held by the user", callEvent.callReference);
                aCall.status = CallStatusHeldByUser;
            }
            else
            {
                OTCLog (@"call '%@' has been held by the remote", callEvent.callReference);
                aCall.status = CallStatusHeldByRemote;
            }
            break;
        }
    }
    
    return aCall;
}

@end
