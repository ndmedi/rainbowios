/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import <Foundation/Foundation.h>

typedef BOOL(^XMPPTestBlock)(NSDictionary * request);
typedef NSArray<NSString*> *(^XMPPResponseBlock)(NSDictionary * request);

@interface XMPPStubDescriptor : NSObject
@property(atomic, copy) XMPPTestBlock testBlock;
@property(atomic, copy) XMPPResponseBlock responseBlock;
@end

@interface XMPPStub : NSObject
+ (instancetype)sharedInstance;
@property(atomic, copy) NSMutableArray<XMPPStubDescriptor*> *stubDescriptors;
@property(atomic, copy) NSMutableArray<NSString*> *incomingMessages;
+(XMPPStubDescriptor*)stubRequestsPassingTest:(XMPPTestBlock)testBlock withStubResponse:(XMPPResponseBlock)responseBlock;
+(void)removeStub:(XMPPStubDescriptor *)stubDesc;
+(void)removeAllStubs;
+(void)addIncomingMessage:(NSString *)message;
@end
