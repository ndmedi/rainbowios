/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "XMPPStub.h"
#import "SRWebSocket.h"
#import <objc/runtime.h>
#import <objc/message.h>
#import "XMLReader.h"

// Empty XMPPStub
@implementation XMPPStubDescriptor
@end


@implementation XMPPStub

- (instancetype)init {
    self = [super init];
    if (self) {
        _stubDescriptors = [NSMutableArray array];
        _incomingMessages = [NSMutableArray array];
    }
    return self;
}

+ (instancetype)sharedInstance {
    static XMPPStub *sharedInstance = nil;
    
    static dispatch_once_t predicate;
    dispatch_once(&predicate, ^{
        sharedInstance = [[self alloc] init];
    });
    
    return sharedInstance;
}

+(XMPPStubDescriptor*)stubRequestsPassingTest:(XMPPTestBlock)testBlock withStubResponse:(XMPPResponseBlock)responseBlock {
    XMPPStubDescriptor* stub = [XMPPStubDescriptor new];
    stub.testBlock = testBlock;
    stub.responseBlock = responseBlock;
    
    [XMPPStub.sharedInstance.stubDescriptors addObject:stub];
    return stub;
}

+(void)removeStub:(XMPPStubDescriptor *)stubDesc {
    return [XMPPStub.sharedInstance.stubDescriptors removeObject:stubDesc];
}

+(void)removeAllStubs {
    [XMPPStub.sharedInstance.stubDescriptors removeAllObjects];
}

+(void)addIncomingMessage:(NSString *)message {
    return [XMPPStub.sharedInstance.incomingMessages addObject:message];
}
@end


// SocketRocket Category
// With Method swizzling.
static dispatch_group_t group;
static BOOL SRClosed;

@implementation SRWebSocket (Swizzling)

/**
 *  This new function exchange 2 method implementations of the class.
 *
 *  @param originalSelector The original method to be replaced
 *  @param overrideSelector The new method which will replace the previous one.
 */
+(void) swizzle_method:(SEL) originalSelector with:(SEL) overrideSelector {
    Method originalMethod = class_getInstanceMethod(self, originalSelector);
    Method overrideMethod = class_getInstanceMethod(self, overrideSelector);
    if (class_addMethod(self, originalSelector, method_getImplementation(overrideMethod), method_getTypeEncoding(overrideMethod))) {
        class_replaceMethod(self, overrideSelector, method_getImplementation(originalMethod), method_getTypeEncoding(originalMethod));
    } else {
        method_exchangeImplementations(originalMethod, overrideMethod);
    }
}

/**
 *  Swizzling has to be done in the init method !
 */
+ (void)load {
    // We have to swizzle the principal methods of the SocketRocket class.
//    [self swizzle_method:@selector(open) with:@selector(swizzled_open)];
//    [self swizzle_method:@selector(close) with:@selector(swizzled_close)];
//    [self swizzle_method:@selector(closeCauseNetworkLost) with:@selector(swizzled_closeCauseNetworkLost)];
//    [self swizzle_method:@selector(send:) with:@selector(swizzled_send:)];
    group = dispatch_group_create();
}

#pragma mark - Method Swizzling

- (void)swizzled_open {
    NSLog(@"SocketRocket-Swizzled : open.");
    
    SRClosed = NO;
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        // Simulate the opening of the socket.
        if ([self.delegate respondsToSelector:@selector(webSocketDidOpen:)]) {
            [self.delegate webSocketDidOpen:self];
        };
    });
    
    // Our incoming message loop.
    dispatch_group_async(group, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        while (!SRClosed) {
            for (NSString *message in XMPPStub.sharedInstance.incomingMessages) {
                [self.delegate webSocket:self didReceiveMessage:message];
            }
            [XMPPStub.sharedInstance.incomingMessages removeAllObjects];
            [NSThread sleepForTimeInterval:.2];
        }
        NSLog(@"SocketRocket-Swizzled : open loop stopped.");
    });
}

- (void) swizzled_close {
    NSLog(@"SocketRocket-Swizzled : close.");
    
    SRClosed = YES;
    dispatch_group_wait(group, DISPATCH_TIME_FOREVER);
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        // Simulate the closing of the socket.
        if ([self.delegate respondsToSelector:@selector(webSocket:didCloseWithCode:reason:wasClean:)]) {
            [self.delegate webSocket:self didCloseWithCode:SRStatusCodeNormal reason:nil wasClean:YES];
        }
    });
}

- (void) swizzled_closeCauseNetworkLost {
    NSLog(@"SocketRocket-Swizzled : closeCauseNetworkLost.");
    
    SRClosed = YES;
    dispatch_group_wait(group, DISPATCH_TIME_FOREVER);
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        // Simulate the closing of the socket.
        if ([self.delegate respondsToSelector:@selector(webSocket:didCloseWithCode:reason:wasClean:)]) {
            [self.delegate webSocket:self didCloseWithCode:SRStatusCodeProtocolError reason:@"Network lost" wasClean:YES];
        }
    });
}

- (void)swizzled_send:(id)data {
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        // Convert the data (which can be NSString or NSData)
        NSString *strData = nil;
        if ([data isKindOfClass:[NSString class]]) {
            strData = (NSString*) data;
        }
        if ([data isKindOfClass:[NSData class]]) {
            strData = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        }
        
        // Parse the outgoing XML message to a dict
        // so it will be easy to use in the testBlocks and responseBlocks of the stubs.
        NSError *parseError = nil;
        NSDictionary *dict = [XMLReader dictionaryForXMLString:strData error:parseError];
        NSLog(@"Outgoing message captured : %@", dict);
        
        // Check all the registred stubs
        for (XMPPStubDescriptor* descriptor in XMPPStub.sharedInstance.stubDescriptors) {
            // If the testBlock success
            if (descriptor.testBlock(dict)) {
                // Then use its responseBlock result as response.
                //dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    // The responseBlock can return multiple messages.
                    NSArray<NSString*> *responses = descriptor.responseBlock(dict);
                    for (NSString *response in responses) {
                        [self.delegate webSocket:self didReceiveMessage:response];
                    }
                //});
                return;
            }
        }
        
        NSLog(@"Warning ! Un-handled outgoing message : %@", dict);
    });
}

@end
