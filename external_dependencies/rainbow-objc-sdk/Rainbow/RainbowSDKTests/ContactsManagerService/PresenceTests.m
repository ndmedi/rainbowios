/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "RainbowSDKTestAbstract.h"
#import "XMPPStub.h"

@interface PresenceTests : RainbowSDKTestAbstract

@end

@implementation PresenceTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.


}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

#pragma mark - Presence update vcard or avatar
/**
 * For vcard
 <presence xmlns="jabber:client"><x xmlns="vcard-temp:x:update"><data/></x><actor xmlns="jabber:iq:configuration"/></presence>
 */
-(void) testPresenceUpdateVcard {
    
    __block BOOL didSendPresenceStanza = NO;
    ContactAPI *contact = [ContactAPI new];
    contact.ID = @"rainbow_1234";
    contact.jid = @"1234@openrainbow.com";
    contact.firstname = @"Marcel";
    contact.lastname = @"Dupont";
    contact.loginEmail = @"1234@test.com";
    contact.inRoster = YES;
    contact.isPending = NO;
    [self.server addContact:contact];
    [self expectationForNotification:kLoginManagerDidLoginSucceeded object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            
            NSString *presenceStanza = [NSString stringWithFormat:@"<presence xmlns='jabber:client' from='1234@openrainbow.com/desktop_osx_abcd' to='%@'><x xmlns='vcard-temp:x:update'><data/></x><actor xmlns='jabber:iq:configuration'/></presence>", [self.server myJID]];
            [self.server receiveMessage:presenceStanza];
            didSendPresenceStanza = YES;
        });
        
        return YES;
    }];
    
    [self expectationForNotification:kContactsManagerServiceDidUpdateContact object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        NSDictionary *userInfo = (NSDictionary *)notification.object;
        Contact *contact = [userInfo objectForKey:kContactKey];
        NSLog(@"DID UPDATE CONTACT %@", contact);
        if([contact.jid isEqualToString:@"1234@openrainbow.com"] && didSendPresenceStanza){
            return YES;
        }
        return NO;
    }];
    
    // Start the SDK
    [self doLoginAndStartXMPP];
    
    // Wait for the message to be sent
    [self waitForExpectationsWithTimeout:30.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
}
/**
 * For Avatar
 <presence xmlns="jabber:client"><x xmlns="vcard-temp:x:update"><avatar/></x><actor xmlns="jabber:iq:configuration"/></presence>
 */
-(void) testPresenceUpdateAvatar {
    // Online presence
    ContactAPI *contact = [ContactAPI new];
    contact.ID = @"rainbow_1234";
    contact.jid = @"1234@openrainbow.com";
    contact.firstname = @"Marcel";
    contact.lastname = @"Dupont";
    contact.loginEmail = @"1234@test.com";
    contact.inRoster = YES;
    contact.isPending = NO;
    contact.presence = @"Online";
    
    __block BOOL didSendPresenceStanza = NO;
    [self expectationForNotification:kLoginManagerDidLoginSucceeded object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            didSendPresenceStanza = YES;
            [self.server receiveMessage:[NSString stringWithFormat:@"<presence xmlns='jabber:client' from='1234@openrainbow.com/desktop_osx_abcd' to='%@'><x xmlns='vcard-temp:x:update'><avatar/></x><actor xmlns='jabber:iq:configuration'/></presence>", [self.server myJID]]];
        });
        return YES;
    }];
    [self expectationForNotification:kContactsManagerServiceDidUpdateContact object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        NSDictionary *userInfo = (NSDictionary *)notification.object;
        Contact *contact = [userInfo objectForKey:kContactKey];
        NSArray<NSString *> *changedKeys = [userInfo objectForKey:kChangedAttributesKey];
        
        NSLog(@"DID UPDATE CONTACT %@", contact);
        if([contact.jid isEqualToString:@"1234@openrainbow.com"] && didSendPresenceStanza && [changedKeys containsObject:@"photoData"]) {
            return YES;
        }
        return NO;
    }];
    
    // Start the SDK without loading stubs
    [self doLoginAndStartXMPP];
    
    // Wait for the message to be sent
    [self waitForExpectationsWithTimeout:30.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
}

#pragma mark - Presence algo
/**
 *  <presence xmlns='jabber:client' from='tel_c29a429ffa4446d48e81bbebbeac8bcf@openrainbow.net/phone' to='c29a429ffa4446d48e81bbebbeac8bcf@openrainbow.net/web_win_1.14.4_LmDxZARb' xmlns:ns2='jabber:client'> <show>chat</show> <status>EVT_CONNECTION_CLEARED</status> </presence>
 
 
 si tu recois un status
 EVT_SERVICE_INITIATED
 ou EVT_ESTABLISHED
 et bien tu est occupé
 sinon tu es libre (au sens téléphonique)
 
 if you have a status
   EVT_SERVICE_INITIATED
   or EVT_ESTABLISHED
   Well, you're busy
   otherwise you are free (in the sense of the phone)

 */
-(void)testPresenceBusyPhoneTelephony {
    
    ContactAPI *contact = [ContactAPI new];
    contact.ID = @"rainbow_1234";
    contact.jid = @"1234@openrainbow.com";
    contact.firstname = @"Marcel";
    contact.lastname = @"Dupont";
    contact.loginEmail = @"1234@test.com";
    contact.inRoster = YES;
    contact.isPending = NO;
    contact.hasSubscribe = YES;
    [self.server addContact:contact];
    
    [self expectationForNotification:kLoginManagerDidLoginSucceeded object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
       NSString *busyStanza = [NSString stringWithFormat:@"<presence xmlns='jabber:client' from='tel_1234@openrainbow.com/phone' to='%@' xmlns:ns2='jabber:client'><show>chat</show> <status>EVT_ESTABLISHED</status> </presence>", [self.server myJID]];
            [self.server receiveMessage:busyStanza];
            });
        return YES;

    }];
    
    [self expectationForNotification:kContactsManagerServiceDidUpdateContact object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        NSDictionary *userInfo = (NSDictionary *)notification.object;
        Contact *contact = [userInfo objectForKey:kContactKey];
        NSLog(@"DID UPDATE CONTACT %@ %@", contact.jid,contact.presence);
        if([contact.jid isEqualToString:@"1234@openrainbow.com"] && contact.presence && contact.presence.presence == ContactPresenceBusy){
            XCTAssertTrue([contact.presence.status isEqualToString:@"phone"], @"Contact status must be phone");
            XCTAssertFalse(contact.isConnectedWithMobile, @"Contact is not connected on mobile");
            return YES;
        }
        return NO;
    }];
    
    // Start the SDK
    [self doLoginAndStartXMPP];
    
    // Wait for the message to be sent
    [self waitForExpectationsWithTimeout:30.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
}

-(void) testPresenceOnline {
    
    ContactAPI *contact = [ContactAPI new];
    contact.ID = @"rainbow_1234";
    contact.jid = @"1234@openrainbow.com";
    contact.firstname = @"Marcel";
    contact.lastname = @"Dupont";
    contact.loginEmail = @"1234@test.com";
    contact.inRoster = YES;
    contact.isPending = NO;
    contact.hasSubscribe = YES;
    [self.server addContact:contact];
    // <presence xmlns='jabber:client' from='contact.fulljid' to='me.fulljid'><priority>5</priority><show/><status>mode=auto</status></presence>

     [self expectationForNotification:kLoginManagerDidLoginSucceeded object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        NSString* PresenceMsg = [NSString stringWithFormat:@"<presence xmlns='jabber:client' from='1234@openrainbow.com/desk_osx_1.14.4_liSa5qGJ' to='%@'><priority xmlns='jabber:client'>5</priority><show/><status>mode=auto</status></presence>", [self.server myJID]];
        NSLog(@"PresenceMessage send: %@",PresenceMsg);
        [self.server receiveMessage:PresenceMsg];
        return YES;
       
    }];
    [self expectationForNotification:kContactsManagerServiceDidUpdateContact object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        NSDictionary *userInfo = (NSDictionary *)notification.object;
        Contact *contact = [userInfo objectForKey:kContactKey];
        NSLog(@"DID UPDATE CONTACT %@ %@",contact.jid,[Presence stringForContactPresence:contact.presence]);
        
        if([contact.jid isEqualToString:@"1234@openrainbow.com"] && contact.presence && contact.presence.presence == ContactPresenceAvailable){
            return YES;
        }
        return NO;
    }];
    
    // Start the SDK
    [self doLoginAndStartXMPP];
    
    // Wait for the message to be sent
    [self waitForExpectationsWithTimeout:30.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
}

-(void)testPresenceOnlineTwoDesktop {
    
    ContactAPI *contact = [ContactAPI new];
    contact.ID = @"rainbow_1234";
    contact.jid = @"1234@openrainbow.com";
    contact.firstname = @"Marcel";
    contact.lastname = @"Dupont";
    contact.loginEmail = @"1234@test.com";
    contact.inRoster = YES;
    contact.isPending = NO;
    [self.server addContact:contact];
    
    [self expectationForNotification:kLoginManagerDidLoginSucceeded object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
         [self.server receiveMessage:[NSString stringWithFormat:@"<presence xmlns='jabber:client' from='1234@openrainbow.com/desk_osx_1.14.4_liSa5qGJ' to='%@'><status>mode=auto</status></presence>", [self.server myJID]]];
          [self.server receiveMessage:[NSString stringWithFormat:@"<presence xmlns='jabber:client' from='1234@openrainbow.com/desk_osx_1.14.4_abcd' to='%@'><status>mode=auto</status></presence>", [self.server myJID]]];
        });
        return YES;
    }];
    
    [self expectationForNotification:kContactsManagerServiceDidUpdateContact object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        NSDictionary *userInfo = (NSDictionary *)notification.object;
        Contact *contact = [userInfo objectForKey:kContactKey];
        NSLog(@"DID UPDATE CONTACT %@", contact.presence);
        if([contact.jid isEqualToString:@"1234@openrainbow.com"] && contact.presence && contact.presence.presence == ContactPresenceAvailable){
            XCTAssertFalse(contact.isConnectedWithMobile, @"Contact is not connected on mobile");
            return YES;
        }
        
        return NO;
    }];
    
    // Start the SDK
    [self doLoginAndStartXMPP];
    
    // Wait for the message to be sent
    [self waitForExpectationsWithTimeout:30.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
}


/**
 *  There is at least one online ressource so we must have a online presence and we are not isConnectedMobile set to false
 */
-(void)testPresenceOnlineOneDesktopOneMobile {
    
    ContactAPI *contact = [ContactAPI new];
    contact.ID = @"rainbow_1234";
    contact.jid = @"1234@openrainbow.com";
    contact.firstname = @"Marcel";
    contact.lastname = @"Dupont";
    contact.loginEmail = @"1234@test.com";
    contact.inRoster = YES;
    contact.isPending = NO;
    [self.server addContact:contact];
    
    [self expectationForNotification:kLoginManagerDidLoginSucceeded object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
         [self.server receiveMessage:[NSString stringWithFormat:@"<presence xmlns='jabber:client' from='1234@openrainbow.com/desk_osx_1.14.4_liSa5qGJ' to='%@'><status>mode=auto</status></presence>", [self.server myJID]]];
          [self.server receiveMessage:[NSString stringWithFormat:@"<presence xmlns='jabber:client' from='1234@openrainbow.com/mobile_iOS_1.14.4_abcdef' to='%@'><status>mode=auto</status></presence>", [self.server myJID]]];
        });
        
        return YES;
    }];
    
    [self expectationForNotification:kContactsManagerServiceDidUpdateContact object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        NSDictionary *userInfo = (NSDictionary *)notification.object;
        Contact *contact = [userInfo objectForKey:kContactKey];
        NSLog(@"DID UPDATE CONTACT %@", contact);
        if([contact.jid isEqualToString:@"1234@openrainbow.com"] && contact.presence && contact.presence.presence == ContactPresenceAvailable){
            XCTAssertFalse(contact.isConnectedWithMobile, @"Contact is not connected on mobile");
            return YES;
        }
        
        return NO;
    }];
    
    // Start the SDK
    [self doLoginAndStartXMPP];
    
    // Wait for the message to be sent
    [self waitForExpectationsWithTimeout:30.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
}

-(void)testPresenceOnlineMobileOneMobile {
    
    ContactAPI *contact = [ContactAPI new];
    contact.ID = @"rainbow_1234";
    contact.jid = @"1234@openrainbow.com";
    contact.firstname = @"Marcel";
    contact.lastname = @"Dupont";
    contact.loginEmail = @"1234@test.com";
    contact.inRoster = YES;
    contact.isPending = NO;
    [self.server addContact:contact];
    
    [self expectationForNotification:kLoginManagerDidLoginSucceeded object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [self.server receiveMessage:[NSString stringWithFormat:@"<presence xmlns='jabber:client' from='1234@openrainbow.com/mobile_iOS_1.14.4_abcdef' to='%@'><status>mode=auto</status></presence>", [self.server myJID]]];
        });
        
        return YES;
    }];
    
    [self expectationForNotification:kContactsManagerServiceDidUpdateContact object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        NSDictionary *userInfo = (NSDictionary *)notification.object;
        Contact *contact = [userInfo objectForKey:kContactKey];
        NSLog(@"DID UPDATE CONTACT %@", contact);
        if([contact.jid isEqualToString:@"1234@openrainbow.com"] && contact.presence && contact.presence.presence == ContactPresenceAvailable){
            XCTAssertTrue(contact.isConnectedWithMobile, @"Contact is not connected on mobile");
            return YES;
        }
        
        return NO;
    }];
    
    // Start the SDK
    [self doLoginAndStartXMPP];
    
    // Wait for the message to be sent
    [self waitForExpectationsWithTimeout:30.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
}

-(void)testPresenceOnlineMobileOneMobileOneDesktopAway {
    
    ContactAPI *contact = [ContactAPI new];
    contact.ID = @"rainbow_1234";
    contact.jid = @"1234@openrainbow.com";
    contact.firstname = @"Marcel";
    contact.lastname = @"Dupont";
    contact.loginEmail = @"1234@test.com";
    contact.inRoster = YES;
    contact.isPending = NO;
    [self.server addContact:contact];
    
    [self expectationForNotification:kLoginManagerDidLoginSucceeded object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [self.server receiveMessage:[NSString stringWithFormat:@"<presence xmlns='jabber:client' from='1234@openrainbow.com/mobile_iOS_1.14.4_abcdef' to='%@'><status>mode=auto</status></presence>", [self.server myJID]]];
            [self.server receiveMessage:[NSString stringWithFormat:@"<presence xmlns='jabber:client' from='1234@openrainbow.com/desk_osx_1.14.4_liSa5qGJ' to='%@'><show>away</show></presence>", [self.server myJID]]];
        });
        return YES;
    }];
    
    [self expectationForNotification:kContactsManagerServiceDidUpdateContact object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        NSDictionary *userInfo = (NSDictionary *)notification.object;
        Contact *contact = [userInfo objectForKey:kContactKey];
        NSLog(@"DID UPDATE CONTACT %@ %@", contact.jid,contact.presence);
        if([contact.jid isEqualToString:@"1234@openrainbow.com"] && contact.presence.presence == ContactPresenceAvailable && contact.isConnectedWithMobile){
            return YES;
        }
        return NO;
    }];
    
    // Start the SDK
    [self doLoginAndStartXMPP];
    
    // Wait for the message to be sent
    [self waitForExpectationsWithTimeout:30.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
}


-(void) testOneDeviceOnlinePhonePresenceBusy {
    ContactAPI *contact = [ContactAPI new];
    contact.ID = @"rainbow_1234";
    contact.jid = @"1234@openrainbow.com";
    contact.firstname = @"Marcel";
    contact.lastname = @"Dupont";
    contact.loginEmail = @"1234@test.com";
    contact.inRoster = YES;
    contact.isPending = NO;
    [self.server addContact:contact];
    
    [self expectationForNotification:kLoginManagerDidLoginSucceeded object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
       dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
         [self.server receiveMessage:[NSString stringWithFormat:@"<presence xmlns='jabber:client' from='1234@openrainbow.com/phone' to='%@' xmlns:ns2='jabber:client'> <show>chat</show> <status>EVT_ESTABLISHED</status> </presence>", [self.server myJID]]];
       });
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 4 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
         [self.server receiveMessage:[NSString stringWithFormat:@"<presence xmlns='jabber:client' from='1234@openrainbow.com/mobile_iOS_1.14.4_abcdef' to='%@'></presence>", [self.server myJID]]];
        });
        return YES;
    }];
    
    [self expectationForNotification:kContactsManagerServiceDidUpdateContact object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        NSDictionary *userInfo = (NSDictionary *)notification.object;
        Contact *contact = [userInfo objectForKey:kContactKey];
        NSLog(@"DID UPDATE CONTACT %@ %@", contact.jid,contact.presence);
        if([contact.jid isEqualToString:@"1234@openrainbow.com"] && contact.presence.presence == ContactPresenceBusy){
            XCTAssertTrue([contact.presence.status isEqualToString:@"phone"], @"Contact status mut be phone");
            return YES;
        }
        
        return NO;
    }];
    
    // Start the SDK
    [self doLoginAndStartXMPP];
    
    // Wait for the message to be sent
    [self waitForExpectationsWithTimeout:30.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
}

-(void) testPresenceOnlineThisDeviceOtherRessourceUnavailable {
    
    [self expectationForNotification:kLoginManagerDidLoginSucceeded object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        
        [self.server receiveMessage:[NSString stringWithFormat:@"<presence xmlns='jabber:client' from='myjid@domain.com/mobile_ios_sdk_EF6C5641-7315-4F62-A208-47CF9E29B931' to='myjid@domain.com/mobile_ios_sdk_EF6C5641-7315-4F62-A208-47CF9E29B931' xmlns:ns2='jabber:client'></presence>"]];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [self.server receiveMessage:[NSString stringWithFormat:@"<presence xmlns='jabber:client' from='myjid@domain.com/mobile_ios_sdk_EF6C5641-7315-4F62-A208-47CF9E29B931' to='myjid@domain.com/mobile_ios_sdk_EF6C5641-7315-4F62-A208-47CF9E29B931' type='unavailable'><delay xmlns='urn:xmpp:delay' from='openrainbow.com' stamp='2016-10-06T16:02:27.640Z'>Resent</delay></presence>"]];
        });
        
        return YES;
    }];
    
    [self expectationForNotification:kContactsManagerServiceDidUpdateMyContact object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        NSDictionary *userInfo = (NSDictionary *)notification.object;
        Contact *contact = [userInfo objectForKey:kContactKey];
        NSLog(@"DID UPDATE MY CONTACT JID %@ %@", contact.jid ,_myUser.contact.jid);
        if([contact isEqual:_myUser.contact] && contact.presence && contact.presence.presence == ContactPresenceAvailable){
            XCTAssertTrue(contact.isConnectedWithMobile, @"Contact is connected on mobile");
            return YES;
        }
        return NO;
    }];
    
    // Start the SDK
    [self doLoginAndStartXMPP];
    
    // Wait for the message to be sent
    [self waitForExpectationsWithTimeout:60.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
}

-(void) testPresentationMode {
    
    ContactAPI *contact = [ContactAPI new];
    contact.ID = @"rainbow_1234";
    contact.jid = @"1234@openrainbow.com";
    contact.firstname = @"Marcel";
    contact.lastname = @"Dupont";
    contact.loginEmail = @"1234@test.com";
    contact.inRoster = YES;
    contact.isPending = NO;
    [self.server addContact:contact];
    
    [self expectationForNotification:kLoginManagerDidLoginSucceeded object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        [self.server receiveMessage:[NSString stringWithFormat:@"<presence xmlns='jabber:client' from='1234@openrainbow.com/mobile_iOS_1.14.4_abcdef' to='%@'></presence>", [self.server myJID]]];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [self.server receiveMessage:[NSString stringWithFormat:@"<presence xmlns='jabber:client' from='1234@openrainbow.com/desktop_osx_1.14.4_abcdef' to='%@'><show>dnd</show><status>presentation</status></presence>", [self.server myJID]]];
        });
        
        return YES;
    }];
    
    [self expectationForNotification:kContactsManagerServiceDidUpdateContact object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        NSDictionary *userInfo = (NSDictionary *)notification.object;
        Contact *contact = [userInfo objectForKey:kContactKey];
        NSLog(@"DID UPDATE CONTACT %@ %@", contact.presence,contact.jid);
        if([contact.jid isEqualToString:@"1234@openrainbow.com"] && contact.presence && contact.presence.presence == ContactPresenceBusy){
            XCTAssertTrue([contact.presence.status isEqualToString:@"presentation"], @"Contact status must be presentation");
            XCTAssertTrue(contact.isConnectedWithMobile, @"Contact is connected on mobile");
            return YES;
        }
        
        return NO;
    }];
    
    // Start the SDK
    [self doLoginAndStartXMPP];
    
    // Wait for the message to be sent
    [self waitForExpectationsWithTimeout:30.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
}

-(void) testAudioWebRTCCall {
    
    ContactAPI *contact = [ContactAPI new];
    contact.ID = @"rainbow_1234";
    contact.jid = @"1234@openrainbow.com";
    contact.firstname = @"Marcel";
    contact.lastname = @"Dupont";
    contact.loginEmail = @"1234@test.com";
    contact.inRoster = YES;
    contact.isPending = NO;
    [self.server addContact:contact];
    
    [self expectationForNotification:kLoginManagerDidLoginSucceeded object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        [self.server receiveMessage:[NSString stringWithFormat:@"<presence xmlns='jabber:client' from='1234@openrainbow.com/mobile_iOS_1.14.4_abcdef' to='%@'></presence>", [self.server myJID]]];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [self.server receiveMessage:[NSString stringWithFormat:@"<presence xmlns='jabber:client' from='1234@openrainbow.com/desktop_osx_1.14.4_abcdef' to='%@'><show>dnd</show><status>audio</status></presence>", [self.server myJID]]];
        });
        
        return YES;
    }];
    
    [self expectationForNotification:kContactsManagerServiceDidUpdateContact object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        NSDictionary *userInfo = (NSDictionary *)notification.object;
        Contact *contact = [userInfo objectForKey:kContactKey];
        NSLog(@"DID UPDATE CONTACT %@ %@ %@", contact.presence,contact.jid,contact.presence.status);
        if([contact.jid isEqualToString:@"1234@openrainbow.com"] && contact.presence && contact.presence.presence == ContactPresenceBusy){
            return YES;
        }
        return NO;
    }];
    
    // Start the SDK
    [self doLoginAndStartXMPP];
    
    // Wait for the message to be sent
    [self waitForExpectationsWithTimeout:30.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
}

-(void) testVideoWebRTCCall {
    
    ContactAPI *contact = [ContactAPI new];
    contact.ID = @"rainbow_1234";
    contact.jid = @"1234@openrainbow.com";
    contact.firstname = @"Marcel";
    contact.lastname = @"Dupont";
    contact.loginEmail = @"1234@test.com";
    contact.inRoster = YES;
    contact.isPending = NO;
    [self.server addContact:contact];
    
    [self expectationForNotification:kLoginManagerDidLoginSucceeded object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        [self.server receiveMessage:[NSString stringWithFormat:@"<presence xmlns='jabber:client' from='1234@openrainbow.com/mobile_iOS_1.14.4_abcdef' to='%@'></presence>", [self.server myJID]]];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [self.server receiveMessage:[NSString stringWithFormat:@"<presence xmlns='jabber:client' from='1234@openrainbow.com/desktop_osx_1.14.4_abcdef' to='%@'><show>dnd</show><status>video</status></presence>", [self.server myJID]]];
        });
        
        return YES;
    }];
    
    [self expectationForNotification:kContactsManagerServiceDidUpdateContact object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        NSDictionary *userInfo = (NSDictionary *)notification.object;
        Contact *contact = [userInfo objectForKey:kContactKey];
        NSLog(@"DID UPDATE CONTACT %@ %@ %@", contact.presence,contact.jid,contact.presence.status);
        if([contact.jid isEqualToString:@"1234@openrainbow.com"] && contact.presence && contact.presence.presence == ContactPresenceBusy){
            return YES;
        }
        return NO;
    }];
    
    // Start the SDK
    [self doLoginAndStartXMPP];
    
    // Wait for the message to be sent
    [self waitForExpectationsWithTimeout:30.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
}

-(void) testSharingWebRTCCall {
    
    ContactAPI *contact = [ContactAPI new];
    contact.ID = @"rainbow_1234";
    contact.jid = @"1234@openrainbow.com";
    contact.firstname = @"Marcel";
    contact.lastname = @"Dupont";
    contact.loginEmail = @"1234@test.com";
    contact.inRoster = YES;
    contact.isPending = NO;
    [self.server addContact:contact];
    
    [self expectationForNotification:kLoginManagerDidLoginSucceeded object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        
        [self.server receiveMessage:[NSString stringWithFormat:@"<presence xmlns='jabber:client' from='1234@openrainbow.com/mobile_iOS_1.14.4_abcdef' to='%@'></presence>", [self.server myJID]]];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [self.server receiveMessage:[NSString stringWithFormat:@"<presence xmlns='jabber:client' from='1234@openrainbow.com/desktop_osx_1.14.4_abcdef' to='%@'><show>dnd</show><status>sharing</status></presence>", [self.server myJID]]];
        });
        
        return YES;
    }];
    
    [self expectationForNotification:kContactsManagerServiceDidUpdateContact object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        NSDictionary *userInfo = (NSDictionary *)notification.object;
        Contact *contact = [userInfo objectForKey:kContactKey];
        NSLog(@"DID UPDATE CONTACT %@ %@", contact.jid,contact.presence);
        if([contact.jid isEqualToString:@"1234@openrainbow.com"] && contact.presence && contact.presence.presence == ContactPresenceBusy){
            return YES;
        }
        
        return NO;
    }];
    
    // Start the SDK
    [self doLoginAndStartXMPP];
    
    // Wait for the message to be sent
    [self waitForExpectationsWithTimeout:30.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
}


-(void) testDesktopAwayAuto {
    
    ContactAPI *contact = [ContactAPI new];
    contact.ID = @"rainbow_1234";
    contact.jid = @"1234@openrainbow.com";
    contact.firstname = @"Marcel";
    contact.lastname = @"Dupont";
    contact.loginEmail = @"1234@test.com";
    contact.inRoster = YES;
    contact.isPending = NO;
    [self.server addContact:contact];
    
    [self expectationForNotification:kLoginManagerDidLoginSucceeded object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [self.server receiveMessage:[NSString stringWithFormat:@"<presence xmlns='jabber:client' from='1234@openrainbow.com/desktop_osx_1.14.4_abcdef' to='%@'><show>away</show></presence>", [self.server myJID]]];
        });
        
        return YES;
    }];
    
    [self expectationForNotification:kContactsManagerServiceDidUpdateContact object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        NSDictionary *userInfo = (NSDictionary *)notification.object;
        Contact *contact = [userInfo objectForKey:kContactKey];
        NSLog(@"DID UPDATE CONTACT %@ %@", contact.jid,contact.presence);
        if([contact.jid isEqualToString:@"1234@openrainbow.com"] && contact.presence && contact.presence.presence == ContactPresenceAway){
            return YES;
        }
        
        return NO;
    }];
    
    // Start the SDK
    [self doLoginAndStartXMPP];
    
    // Wait for the message to be sent
    [self waitForExpectationsWithTimeout:30.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
}

-(void) testPresenceDND {
    
    ContactAPI *contact = [ContactAPI new];
    contact.ID = @"rainbow_1234";
    contact.jid = @"1234@openrainbow.com";
    contact.firstname = @"Marcel";
    contact.lastname = @"Dupont";
    contact.loginEmail = @"1234@test.com";
    contact.inRoster = YES;
    contact.isPending = NO;
    [self.server addContact:contact];
    
    [self expectationForNotification:kLoginManagerDidLoginSucceeded object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        NSString* dndPresenceMsg = [NSString stringWithFormat:@"<presence xmlns='jabber:client' from='1234@openrainbow.com/desktop_osx_1.14.4_abcdef' to='%@'><priority xmlns='jabber:client'>5</priority><show>dnd</show></presence>", [self.server myJID]];
        [self.server receiveMessage: dndPresenceMsg];
        return YES;
    }];
    
    [self expectationForNotification:kContactsManagerServiceDidUpdateContact object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        NSDictionary *userInfo = (NSDictionary *)notification.object;
        Contact *contact = [userInfo objectForKey:kContactKey];
        NSLog(@"DID UPDATE CONTACT %@ %@", contact.jid,contact.presence);
        if([contact.jid isEqualToString:@"1234@openrainbow.com"] && contact.presence && contact.presence.presence == ContactPresenceDoNotDisturb){
            XCTAssertTrue(contact.presence.presence == ContactPresenceDoNotDisturb, @"Contact presence must be in DND");
            return YES;
        }
        
        return NO;
    }];
    
    // Start the SDK
    [self doLoginAndStartXMPP];
    
    // Wait for the message to be sent
    [self waitForExpectationsWithTimeout:30.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
}

-(void) testDNDOneRessourceXAEarlier {
    
    ContactAPI *contact = [ContactAPI new];
    contact.ID = @"rainbow_1234";
    contact.jid = @"1234@openrainbow.com";
    contact.firstname = @"Marcel";
    contact.lastname = @"Dupont";
    contact.loginEmail = @"1234@test.com";
    contact.inRoster = YES;
    contact.isPending = NO;
    [self.server addContact:contact];
    
    [self expectationForNotification:kLoginManagerDidLoginSucceeded object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        
        [self.server  receiveMessage:[NSString stringWithFormat:@"<presence xmlns='jabber:client' from='1234@openrainbow.com/desk_osx_1.14.4_liSa5qGJ' to='%@'><delay xmlns='urn:xmpp:delay' from='cccc61ca12504f039cd142c5121b5a8e@openrainbow.com/desk_osx_1.14.4_liSa5qGJ' stamp='2016-09-27T12:30:01.811Z'/><show>xa</show></presence>", [self.server myJID]]];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [self.server  receiveMessage:[NSString stringWithFormat:@"<presence xmlns='jabber:client' from='1234@openrainbow.com/desktop_osx_1.14.4_abcdef' to='%@'><show>dnd</show></presence>", [self.server myJID]]];
        });
        
        return YES;
    }];
    
    [self expectationForNotification:kContactsManagerServiceDidUpdateContact object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        NSDictionary *userInfo = (NSDictionary *)notification.object;
        Contact *contact = [userInfo objectForKey:kContactKey];
        NSLog(@"DID UPDATE CONTACT %@", contact);
        if([contact.jid isEqualToString:@"1234@openrainbow.com"] && contact.presence && contact.presence.presence == ContactPresenceDoNotDisturb){
            XCTAssertTrue(contact.presence.presence == ContactPresenceDoNotDisturb, @"Contact presence must be in DND");
            return YES;
        }
        return NO;
    }];
    
    // Start the SDK
    [self doLoginAndStartXMPP];
    
    // Wait for the message to be sent
    [self waitForExpectationsWithTimeout:30.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
}

-(void) testPresenceXA {
    
    ContactAPI *contact = [ContactAPI new];
    contact.ID = @"rainbow_1234";
    contact.jid = @"1234@openrainbow.com";
    contact.firstname = @"Marcel";
    contact.lastname = @"Dupont";
    contact.loginEmail = @"1234@test.com";
    contact.inRoster = YES;
    contact.isPending = NO;
    [self.server addContact:contact];

    [self expectationForNotification:kLoginManagerDidLoginSucceeded object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        [self.server receiveMessage:[NSString stringWithFormat:@"<presence xmlns='jabber:client' from='1234@openrainbow.com/desktop_osx_1.14.4_abcdef' to='%@'></presence>", [self.server myJID]]];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [self.server receiveMessage:[NSString stringWithFormat:@"<presence xmlns='jabber:client' from='1234@openrainbow.com/desktop_osx_1.14.4_abcdef' to='%@'><show>xa</show></presence>", [self.server myJID]]];
        });
        
        return YES;
    }];
    
    [self expectationForNotification:kContactsManagerServiceDidUpdateContact object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        NSDictionary *userInfo = (NSDictionary *)notification.object;
        Contact *contact = [userInfo objectForKey:kContactKey];
        NSLog(@"DID UPDATE CONTACT %@", contact);
        if([contact.jid isEqualToString:@"1234@openrainbow.com"] && contact.presence && contact.presence.presence == ContactPresenceUnavailable){
            return YES;
        }
        
        return NO;
    }];
    
    // Start the SDK
    [self doLoginAndStartXMPP];
    
    // Wait for the message to be sent
    [self waitForExpectationsWithTimeout:30.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
}

-(void) testPresenceXAOneRessourceDNDEarlier {
    ContactAPI *contact = [ContactAPI new];
    contact.ID = @"rainbow_1234";
    contact.jid = @"1234@openrainbow.com";
    contact.firstname = @"Marcel";
    contact.lastname = @"Dupont";
    contact.loginEmail = @"1234@test.com";
    contact.inRoster = YES;
    contact.isPending = NO;
    [self.server addContact:contact];
    [self expectationForNotification:kLoginManagerDidLoginSucceeded object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        
        [self.server receiveMessage:[NSString stringWithFormat:@"<presence xmlns='jabber:client' from='1234@openrainbow.com/desktop_osx_1.14.4_abcdef' to='%@'></presence>", [self.server myJID]]];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [self.server receiveMessage:[NSString stringWithFormat:@"<presence xmlns='jabber:client' from='1234@openrainbow.com/desk_osx_1.14.4_liSa5qGJ' to='%@'><delay xmlns='urn:xmpp:delay' from='cccc61ca12504f039cd142c5121b5a8e@openrainbow.com/desk_osx_1.14.4_liSa5qGJ' stamp='2016-09-27T12:30:01.811Z'/><show>dnd</show></presence>", [self.server myJID]]];

            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 4 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                
                [XMPPStub addIncomingMessage:[NSString stringWithFormat:@"<presence xmlns='jabber:client' from='1234@openrainbow.com/desktop_osx_1.14.4_abcdef' to='%@'><show>xa</show></presence>", [self.server myJID]]];
            });
        });
        
        return YES;
    }];
    
    [self expectationForNotification:kContactsManagerServiceDidUpdateContact object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        NSDictionary *userInfo = (NSDictionary *)notification.object;
        Contact *contact = [userInfo objectForKey:kContactKey];
        NSLog(@"DID UPDATE CONTACT %@", contact);
        if([contact.jid isEqualToString:@"1234@openrainbow.com"] && contact.presence && contact.presence.presence == ContactPresenceUnavailable){
            return YES;
        }
        
        return NO;
    }];
    
    // Start the SDK
    [self doLoginAndStartXMPP];
    
    // Wait for the message to be sent
    [self waitForExpectationsWithTimeout:30.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
}


-(void) testAway {
    
    ContactAPI *contact = [ContactAPI new];
    contact.ID = @"rainbow_1234";
    contact.jid = @"1234@openrainbow.com";
    contact.firstname = @"Marcel";
    contact.lastname = @"Dupont";
    contact.loginEmail = @"1234@test.com";
    contact.inRoster = YES;
    contact.isPending = NO;
    [self.server addContact:contact];
    
    [self expectationForNotification:kLoginManagerDidLoginSucceeded object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        NSString* presenceMessage = [NSString stringWithFormat:@"<presence xmlns='jabber:client' from='1234@openrainbow.com/desk_osx_1.14.4_liSa5qGJ' to='%@'><show>away</show></presence>",[self.server myJID]];
       [self.server receiveMessage: presenceMessage];
        return YES;
    }];
    
    [self expectationForNotification:kContactsManagerServiceDidUpdateContact object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        NSDictionary *userInfo = (NSDictionary *)notification.object;
        Contact *contact = (Contact*)[userInfo objectForKey:kContactKey];
        NSLog(@"DID UPDATE CONTACT %@ %@", contact.jid, contact.presence);
        if([contact.jid isEqualToString:@"1234@openrainbow.com"] && contact.presence && contact.presence.presence == ContactPresenceAway){
            XCTAssertTrue(contact.presence.presence == ContactPresenceAway, @"Contact presence must be in AWAY");
            return YES;
        }
        
        return NO;
    }];
    
    // Start the SDK
    [self doLoginAndStartXMPP];
    
    // Wait for the message to be sent
    [self waitForExpectationsWithTimeout:30.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
}

-(void) testMyPresenceDNDReceiveOnlineManual {
    
    __block BOOL didSendPresenceStanza = NO;
    [self expectationForNotification:kLoginManagerDidLoginSucceeded object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [self.server receiveMessage:[NSString stringWithFormat:@"<presence xmlns='jabber:client' from='myjid@domain.com/mobile_ios_sdk_EF6C5641-7315-4F62-A208-47CF9E29B931' to='myjid@domain.com/mobile_ios_sdk_EF6C5641-7315-4F62-A208-47CF9E29B931'><show>dnd</show></presence>"]];
        NSLog(@"GetFull: %@", _myUser.contact.jid);
        });
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 4* NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [self.server receiveMessage:[NSString stringWithFormat:@"<presence xmlns='jabber:client' from='myjid@domain.com/mobile_ios_sdk_EF6C5641-7315-4F62-A208-47CF9E29B931' to='myjid@domain.com/mobile_ios_sdk_EF6C5641-7315-4F62-A208-47CF9E29B931'><show></show><status>mode=auto</status></presence>"]];
        didSendPresenceStanza = YES;
        });

        return YES;
    }];
    
    [self expectationForNotification:kContactsManagerServiceDidUpdateMyContact object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        NSDictionary *userInfo = (NSDictionary *)notification.object;
        Contact *contact = [userInfo objectForKey:kContactKey];
        NSLog(@"DID UPDATE CONTACT DND %@", contact.presence);
        if(contact.presence && contact.presence.presence == ContactPresenceDoNotDisturb){
            return YES;
        }

        return NO;
    }];
    
    [self expectationForNotification:kContactsManagerServiceDidUpdateMyContact object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        NSDictionary *userInfo = (NSDictionary *)notification.object;
        Contact *contact = [userInfo objectForKey:kContactKey];

        NSLog(@"DID UPDATE CONTACT ONLINE %@", contact.presence);
        if(contact.presence && contact.presence.presence == ContactPresenceAvailable && didSendPresenceStanza){
            XCTAssertTrue(contact.presence.status.length == 0, @"No status must be set");
            return YES;
        }

        return NO;
    }];
    
    // Start the SDK
    [self doLoginAndStartXMPP];
    
    // Wait for the message to be sent
    [self waitForExpectationsWithTimeout:30.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
}

-(void) testMyPresenceDNDReceiveAwayManual {
    __block BOOL didSendPresenceStanza = NO;
    
    [self expectationForNotification:kLoginManagerDidLoginSucceeded object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [self.server receiveMessage:[NSString stringWithFormat:@"<presence xmlns='jabber:client' from='myjid@domain.com/mobile_ios_sdk_EF6C5641-7315-4F62-A208-47CF9E29B931' to='myjid@domain.com/mobile_ios_sdk_EF6C5641-7315-4F62-A208-47CF9E29B931'><show>dnd</show></presence>"]];
        });
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 4 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [self.server receiveMessage:[NSString stringWithFormat:@"<presence xmlns='jabber:client' from='myjid@domain.com/mobile_ios_sdk_EF6C5641-7315-4F62-A208-47CF9E29B931' to='myjid@domain.com/mobile_ios_sdk_EF6C5641-7315-4F62-A208-47CF9E29B931'><show>xa</show><status>away</status></presence>"]];
            didSendPresenceStanza = YES;
        });
        
        return YES;
    }];
    
    [self expectationForNotification:kContactsManagerServiceDidUpdateMyContact object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        NSDictionary *userInfo = (NSDictionary *)notification.object;
        Contact *contact = [userInfo objectForKey:kContactKey];
        NSLog(@"DID UPDATE CONTACT DND %@", contact);
        if(contact.presence && contact.presence.presence == ContactPresenceDoNotDisturb){
            return YES;
        }
        return NO;
    }];
    
    [self expectationForNotification:kContactsManagerServiceDidUpdateMyContact object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        NSDictionary *userInfo = (NSDictionary *)notification.object;
        Contact *contact = [userInfo objectForKey:kContactKey];
        NSLog(@"DID UPDATE CONTACT AWAY MANUAL %@", contact);
        if(contact.presence && contact.presence.presence == ContactPresenceAway && didSendPresenceStanza){
             XCTAssertTrue(contact.presence.status.length == 0, @"No status must be set");
            return YES;
        }
        
        return NO;
    }];
    
    // Start the SDK
    [self doLoginAndStartXMPP];
    
    // Wait for the message to be sent
    [self waitForExpectationsWithTimeout:30.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Timeout Error: %@", error);
        }
    }];
}
@end
