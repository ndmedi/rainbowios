/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "ConferenceInfo.h"
#import "ConferenceInfo+Internal.h"
#import "ConferenceState.h"
#import "ConferenceParticipant.h"
#import "ConferenceParticipant+Internal.h"
#import "ConferenceParticipantsTable.h"

@implementation ConferenceInfo

-(instancetype) init {
    self = [super init];
    if(self){
        _eventType = ConferenceEventTypeUnknown;
        _confId = nil;
        _state = [[ConferenceState alloc] init];
        _participants = [[ConferenceParticipantsTable alloc] init];
        _removedParticipants = [[NSMutableArray alloc] init];
        _talkers = [[NSMutableArray alloc] init];
    }
    return self;
}

-(void) addOrUpdateParticipant:(ConferenceParticipant *)participant {
    [_participants addOrUpdateParticipant:participant];
}

-(void) addRemovedParticipant:(ConferenceParticipantId *)id {
    if(id){
        [(NSMutableArray *)_removedParticipants addObject:id];
    }
}

-(void) addTalker:(ConferenceParticipantId *)id {
    if(id){
        [(NSMutableArray *)_talkers addObject:id];
    }
}

@end
