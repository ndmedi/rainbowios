/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "ConferenceParticipant.h"

@interface ConferenceInfo ()
@property (readwrite, strong) NSString *confId;
@property (readwrite) ConferenceEventType eventType;
@property (readwrite, strong) ConferenceState *state;
@property (readwrite, strong) ConferenceParticipantsTable *participants;
@property (readwrite, strong) NSMutableArray<ConferenceParticipantId *> *removedParticipants;
@property (readwrite, strong) NSMutableArray<ConferenceParticipantId *> *talkers;
@end
