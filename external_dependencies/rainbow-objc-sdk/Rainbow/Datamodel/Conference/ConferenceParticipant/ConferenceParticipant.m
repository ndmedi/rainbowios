/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "ConferenceParticipant.h"
#import "ConferenceParticipant+Internal.h"
#import "ServicesManager.h"
#import "ContactsManagerService.h"
#import "ContactsManagerService+Internal.h"

@implementation ConferenceParticipant

-(instancetype) initWithParticipantId:(NSString *)id jidIM:(NSString *)jidIM phoneNumber:(NSString *)phoneNumber role:(ParticipantRole)role state:(ParticipantState)state muted:(BOOL) muted hold:(BOOL) hold {
    self = [super init];
    if(self){
        _contact = nil;
        _participantId = id;
        _jidIM = jidIM;
        _phoneNumber = phoneNumber;
        _role = role;
        _state = state;
        _muted = muted;
        _hold = hold;
        [self retrieveContact];
    }
    return self;
}

-(void) retrieveContact {
    ContactsManagerService *contactsManagerService = [ServicesManager sharedInstance].contactsManagerService;
    self.contact = [contactsManagerService createOrUpdateRainbowContactWithJid:_jidIM];
}

+(ParticipantRole) participantRoleFromNSString:(NSString *)roleStr{
    return [roleStr isEqualToString:@"moderator"] ? ParticipantRoleModerator : ParticipantRoleMember;
}

+(ParticipantState) participantStateFromNSString:(NSString *)stateStr {
    if([stateStr isEqualToString:@"ringing"])
        return ParticipantStateRinging;
    else if ([stateStr isEqualToString:@"connected"])
        return ParticipantStateConnected;
    else if ([stateStr isEqualToString:@"disconnected"])
        return ParticipantStateDisconnected;
    else
        return ParticipantStateUnknown;
}

+(NSString *) stringFromParticipantRole:(ParticipantRole) participantRole {
    switch (participantRole) {
        case ParticipantRoleModerator:
            return @"moderator";
            break;
        case ParticipantRoleMember:
            return @"member";
        default:
            break;
    }
}

+(NSString *) stringFromParticipantState:(ParticipantState) participantState {
    switch (participantState) {
        case ParticipantStateRinging:
            return @"ringing";
            break;
        case ParticipantStateConnected:
            return @"connected";
        case ParticipantStateDisconnected:
            return @"disconnected";
        case ParticipantStateUnknown:
        default:
            return @"unknown";
            break;
    }
}

@end
