/*
 * Rainbow
 *
 * Copyright (c) 2017, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "CallLog+Internal.h"
#import "Tools.h"

@implementation CallLog

-(NSString *) description {
    @synchronized(self){
        return [NSString stringWithFormat:@"Call Log <%p> Type %@ Date %@ duration %@ Peer %@ media %@ state %@ isOutgoing %@ isRead %@", self, [CallLog stringForCallLogType:_type], _date, self.duration, self.peer, [CallLog stringForCallLogMedia:_media], [CallLog stringForCallLogState:_state], NSStringFromBOOL(_isOutgoing), NSStringFromBOOL(_isRead)];
    }
}

-(NSDictionary *) dictionaryDescription {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setObject:[CallLog stringForCallLogType:_type] forKey:@"state"];
    if(_realDuration)
       [dic setObject:_realDuration forKey:@"duration"];
    return dic;
}

+(NSString *) stringForCallLogState:(CallLogState) state {
    switch (state) {
        case CallLogStateMissed:
            return @"missed";
            break;
        case CallLogStateAnswered:
            return @"answered";
            break;
        case CallLogTypeUnknown:
        default:
            return @"Unknown";
            break;
    }
}

+(CallLogState) callLogStateFromEventTypeString:(NSString *) eventType {
    
    if([eventType isEqualToString:@"missedCall"] || [eventType isEqualToString:@"missed"]){
        return CallLogStateMissed;
    }
    if ([eventType isEqualToString:@"activeCallMsg"] || [eventType isEqualToString:@"answered"]){
        return CallLogStateAnswered;
    }
    
    return CallLogStateUnknown;
}

+(CallLogMedia) callLogMediaFromString:(NSString *) mediaString {
    if([mediaString isEqualToString:@"audio"]){
        return CallLogMediaAudio;
    } else if ([mediaString isEqualToString:@"video"]){
        return CallLogMediaVideo;
    } else {
        return CallLogMediaUnknown;
    }
}

+(NSString *) stringForCallLogMedia:(CallLogMedia) media {
    switch (media) {
        case CallLogMediaAudio:
            return @"audio";
            break;
        case CallLogMediaVideo:
            return @"video";
            break;
        case CallLogMediaUnknown:
        default:
            return @"unknown";
            break;
    }
}

+(NSString *) stringForCallLogType:(CallLogType) type {
    switch (type) {
        case CallLogTypePhone:
            return @"phone";
            break;
            case CallLogTypeWebRTC:
            return @"webrtc";
            break;
        case CallLogTypeUnknown:
        default:
            return @"unknown";
            break;
    }
}

+(CallLogType) callLogTypeFromString:(NSString *) type {
    if([type isEqualToString:@"phone"]){
        return CallLogTypePhone;
    } else if ([type isEqualToString:@"webrtc"]){
        return CallLogTypeWebRTC;
    } else
        return CallLogTypeUnknown;
}

+(CallLogService) callLogServiceFromString:(NSString *) service {
    if([service isEqualToString:@"conference"]){
        return CallLogServiceConference;
    } else
        return CallLogServiceUnknown;
}

-(NSString *) duration {
    NSInteger duration = [_realDuration integerValue];
    NSString *durationInSecAsString = [NSString stringWithFormat:@"%ld", duration/1000];
    return durationInSecAsString;
}

-(BOOL)isEqual:(CallLog *)aCallLog {
    return [self.callLogId isEqualToString:aCallLog.callLogId];
}

-(Peer *) peer {
    return self.isOutgoing?_callee:_caller;
}
@end
