/*
 * Rainbow
 *
 * Copyright (c) 2018, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import <Foundation/Foundation.h>

/**
 *  The different scenario for room invitation
 */
typedef NS_ENUM(NSInteger, InvitationScenario) {
    /**
     *  We try to move a set of users known by their email only, to a CHAT room.
     */
    InvitationChat = 1,
    /**
     *  We try to notify a set of users known by their rainbowId or their email, to join a pstn conference.
     */
    InvitationPstnConference,
    /**
     *  In this scenario, we try to notify a set of users known by their rainbowId or their email, to join a video conference.
     */
    InvitationVideoConference
};

@interface RoomInvitationScenario : NSObject

+(NSString *) stringFromRoomInvitationScenario:(InvitationScenario) scenario;

@end
