/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "Channel.h"
#import "Channel+Internal.h"
#import "ChannelItem+Internal.h"
#import "NSDate+JSONString.h"
#import <UIKit/UIImage.h>

@interface Channel ()
@property (nonatomic, strong, readwrite) NSMutableArray<ChannelItem *> *items;
@end

@implementation Channel

static NSMutableDictionary *imageCategories;
static NSMutableDictionary *colorCategories;

+(UIImage *) imageForCategory:(ChannelCategory) category {
    if (!imageCategories)
        return nil;

    return [imageCategories objectForKey:[self stringFromChannelCategory:category]];
}

+(void) setImage:(UIImage *) image forCategory:(ChannelCategory) category {
    if (!imageCategories)
        imageCategories = [NSMutableDictionary new];
    
    [imageCategories setValue:image forKey:[self stringFromChannelCategory:category]];
}

+(int) imageCategoriesCount {
    if (!imageCategories)
        return 0;
    
    return (int)[imageCategories count];
}

+(UIColor *) colorForCategory:(ChannelCategory) category {
    return [colorCategories objectForKey:[self stringFromChannelCategory:category]];
}

+(void) setColor:(UIColor *) color forCategory:(ChannelCategory) category {
    if (!colorCategories)
        colorCategories = [NSMutableDictionary new];
    
    [colorCategories setValue:color forKey:[self stringFromChannelCategory:category]];
}

+(int) colorCategoriesCount {
    if (!colorCategories)
        return 0;
    
    return (int)[colorCategories count];
}

+(NSString *)stringFromChannelCategory:(ChannelCategory) category {
    NSString *categoryStr = nil;
    
    switch (category) {
        case ChannelCategoryGlobalNews:
            categoryStr = @"globalnews";
            break;
        case ChannelCategoryDesign:
            categoryStr = @"design";
            break;
        case ChannelCategoryInnovation:
            categoryStr = @"innovation";
            break;
        case ChannelCategorySocialMedia:
            categoryStr = @"social";
            break;
        case ChannelCategoryScience:
            categoryStr = @"science";
            break;
        case ChannelCategoryCulture:
            categoryStr = @"culture";
            break;
        case ChannelCategoryBusiness:
            categoryStr = @"business";
            break;
        case ChannelCategoryHitech:
            categoryStr = @"hitech";
            break;
        case ChannelCategoryMarketing:
            categoryStr = @"marketing";
            break;
        case ChannelCategoryTravel:
            categoryStr = @"travel";
            break;
    }
    
    return categoryStr;
}

+(ChannelCategory) channelCategoryFromString:(NSString *) str {
    if ([str isEqualToString:@"design"]) {
        return ChannelCategoryDesign;
    } else if ([str isEqualToString:@"innovation"]) {
        return ChannelCategoryInnovation;
    } else if ([str isEqualToString:@"social"]) {
        return ChannelCategorySocialMedia;
    } else if ([str isEqualToString:@"science"]) {
        return ChannelCategoryScience;
    } else if ([str isEqualToString:@"culture"]) {
        return ChannelCategoryCulture;
    } else if ([str isEqualToString:@"business"]) {
        return ChannelCategoryBusiness;
    } else if ([str isEqualToString:@"hitech"]) {
        return ChannelCategoryHitech;
    } else if ([str isEqualToString:@"marketing"]) {
        return ChannelCategoryMarketing;
    } else if ([str isEqualToString:@"travel"]) {
        return ChannelCategoryTravel;
    } else {
        return ChannelCategoryGlobalNews;
    }
}

+(NSString *)stringFromChannelVisibility:(ChannelVisibility)visibility {
    NSString *visibilityStr = nil;
    switch(visibility){
        case ChannelVisibilityPublic:
            visibilityStr = @"public";
            break;
        case ChannelVisibilityCompany:
            visibilityStr = @"company";
            break;
        case ChannelVisibilityPrivate:
            visibilityStr = @"private";
            break;
    }
    return visibilityStr;
}

+(ChannelVisibility)channelVisibilityFromString:(NSString *)str {
    ChannelVisibility visibility = ChannelVisibilityPublic;
    if([str isEqualToString:@"private"]){
        visibility = ChannelVisibilityPrivate;
    } else if([str isEqualToString:@"company"]){
        visibility = ChannelVisibilityCompany;
    }
    return visibility;
}


+(NSString *)stringFromChannelUserType:(ChannelUserType)userType {
    NSString *userTypeStr = nil;
    switch(userType){
        case ChannelUserTypeNone:
            userTypeStr = @"none";
            break;
        case ChannelUserTypeMember:
            userTypeStr = @"member";
            break;
        case ChannelUserTypePublisher:
            userTypeStr = @"publisher";
            break;
        case ChannelUserTypeOwner:
            userTypeStr = @"owner";
            break;
    }
    return userTypeStr;
}

+(ChannelUserType)channelUserTypeFromString:(NSString *)str {
    ChannelUserType userType = ChannelUserTypeNone;
    if([str isEqualToString:@"member"]){
        userType = ChannelUserTypeMember;
    } else if([str isEqualToString:@"publisher"]){
        userType = ChannelUserTypePublisher;
    } else if([str isEqualToString:@"owner"]){
        userType = ChannelUserTypeOwner;
    }
    return userType;
}

-(instancetype)init {
    self = [super init];
    if(self){
        _items = [[NSMutableArray alloc] init];
    }
    return self;
}

-(ChannelItem *)findItemById:(NSString *)id {
    for(ChannelItem *item in self.items){
        if([id isEqualToString:item.itemId]){
            return item;
        }
    }
    return nil;
}

-(void)addOrUpdateItem:(ChannelItem *)item {
    ChannelItem *oldItem = [self findItemById:item.itemId];
    if(oldItem){
        oldItem.title = item.title;
        oldItem.message = item.message;
        oldItem.url = item.url;
    } else {
        [(NSMutableArray *)self.items addObject:item];
    }
}

-(void)removeItem:(ChannelItem *)item {
    ChannelItem *oldItem = [self findItemById:item.itemId];
    if(oldItem){
        [(NSMutableArray *)self.items removeObject:oldItem];
    }
}

-(NSDictionary *) dictionaryRepresentation {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    
    if (_id)
        [dic setObject:_id forKey:@"id"];
    if (_name)
        [dic setObject:_name forKey:@"name"];
    if (_topic)
        [dic setObject:_topic forKey:@"topic"];
    if (_visibility)
        [dic setObject:[Channel stringFromChannelVisibility:_visibility] forKey:@"visibility"];
    if (_category)
        [dic setObject:[Channel stringFromChannelCategory:_category] forKey:@"category"];
    if (_companyId)
        [dic setObject:_companyId forKey:@"companyId"];
    if (_creatorId)
        [dic setObject:_creatorId forKey:@"creatorId"];
    if (_creationDate)
        [dic setObject:[NSDate jsonStringFromDate:_creationDate] forKey:@"creationDate"];
    if (_usersCount)
        [dic setObject:[NSNumber numberWithInt:_usersCount] forKey:@"users_count"];
    if (_subscribersCount)
        [dic setObject:[NSNumber numberWithInt:_subscribersCount] forKey:@"subscribers_count"];
    if (_userType)
        [dic setObject:[Channel stringFromChannelUserType:_userType] forKey:@"type"];
    if (_lastAvatarUpdateDate)
        [dic setObject:[NSDate jsonStringFromDate:_lastAvatarUpdateDate] forKey:@"lastAvatarUpdateDate"];
    if (_maxItems)
        [dic setObject:[NSNumber numberWithInt:_maxItems] forKey:@"maxItems"];
    if (_maxPayloadSize)
        [dic setObject:[NSNumber numberWithInt:_maxPayloadSize] forKey:@"maxPayloadSize"];
    if (_subscribed)
        [dic setObject:[NSNumber numberWithBool:_subscribed] forKey:@"subscribed"];
    if (_invited)
        [dic setObject:[NSNumber numberWithBool:_invited] forKey:@"invited"];
    
    return dic;
}

@end
