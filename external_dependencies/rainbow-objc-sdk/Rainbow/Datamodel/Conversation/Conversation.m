/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "Conversation.h"
#import "Conversation+Internal.h"
#import "NSDate+JSONString.h"
#import "CallLog+Internal.h"
#import "NSDate+Utilities.h"

@implementation Conversation
-(instancetype) init {
    self = [super init];
    if(self){
        _unReadMessageIDs = [NSMutableSet new];
        _status = ConversationStatusInactive;
        _isMuted = NO;
        _isSynchronized = NO;
        _hasActiveCall = NO;
    }
    return self;
}

-(void) dealloc {
    _lastMessage = nil;
    _unReadMessageIDs = nil;
}

// Type depends on the instanciated 'peer' object
-(ConversationType) type {
    if ([_peer isKindOfClass:[Contact class]]) {
        if(((Contact *)_peer).isBot)
            return ConversationTypeBot;
        return ConversationTypeUser;
    }
    if ([_peer isKindOfClass:[Room class]]) {
        return ConversationTypeRoom;
    }
    return ConversationTypeUnknown;
}

-(Contact *) contactCast {
    return (Contact *) _peer;
}

-(Room *) roomCast {
    return (Room *) _peer;
}

-(void) setLastMessage:(Message *)lastMessage {
    
    if(_lastMessage && _lastMessage.date  && lastMessage && lastMessage.date){
        if([_lastMessage.date isEarlierThanDate:lastMessage.date] || (_lastMessage.attachment &&!_lastMessage.attachment.isDownloadAvailable)){
            
            [self willChangeValueForKey:kConversationLastMessage];
            _lastMessage = nil;
            _lastMessage = lastMessage;
            [self didChangeValueForKey:kConversationLastMessage];
        } else {
            NSLog(@"Don't update last message, new date %@ is earlier than old date %@", lastMessage.date, _lastMessage.date);
        }
    } else {
        if(lastMessage && lastMessage.date){
            [self willChangeValueForKey:kConversationLastMessage];
            _lastMessage = nil;
            _lastMessage = lastMessage;
            [self didChangeValueForKey:kConversationLastMessage];
        } else {
            if(!lastMessage){
                // We are treating a delete all case ?
                [self willChangeValueForKey:kConversationLastMessage];
                _lastMessage = nil;
                [self didChangeValueForKey:kConversationLastMessage];
            } else
                NSLog(@"Don't update last message, don't have the good info _lastMessage %@ newLastMessage %@",_lastMessage, lastMessage);
        }
    }
}

-(NSDate *) lastUpdateDate {
    NSDate *date = nil;
    if(!_lastMessage){
        if([_peer isKindOfClass:[Room class]]){
            date = [self roomCast].creationDate;
        } else if(self.creationDate) {
            date = self.creationDate;
        }
    } else
        date = _lastMessage.date;
    
    return date;
}

-(BOOL) isEqual:(Conversation *) aConversation {
    if(self == aConversation)
        return YES;
    return [self.conversationId isEqualToString:aConversation.conversationId];
}

-(NSUInteger)hash {
    return [self.conversationId hash];
}

-(NSString*) stringForConversationType {
    if (self.type == ConversationTypeUser)
        return @"user";
    if (self.type == ConversationTypeRoom)
        return @"room";
    if (self.type == ConversationTypeBot)
        return @"bot";
    return @"";
}

-(NSString*) stringForConversationStatus {
    if (_status == ConversationStatusActive)
        return @"active";
    if (_status == ConversationStatusInactive)
        return @"inactive";
    if (_status == ConversationStatusComposing)
        return @"composing";
    return nil;
}

-(NSString *) description {
    @synchronized(self){
        return [NSString stringWithFormat:@"Conversation %@ type %@ lastMessage %@ lastUpdateDate %@  peer %@ unreadMessagesCount %ld", _conversationId, [self stringForConversationType], _lastMessage, self.lastUpdateDate, _peer, (long)_unreadMessagesCount];
    }
}


-(NSString *) debugDescription {
    @synchronized(self){
        return [NSString stringWithFormat:@"\tConversation %@ type %@ lastMessageText %@ unreadMessagesCount %ld", _conversationId, [self stringForConversationType], [_lastMessage debugDescription], (long)_unreadMessagesCount];
    }
}

-(NSDictionary *) dictionaryRepresentation {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    if(_conversationId)
        [dic setObject:_conversationId forKey:@"id"];
    
    if(_lastMessage){
        if(_lastMessage.body.length > 0)
            [dic setObject:_lastMessage.body forKey:@"lastMessageText"];
        if(_lastMessage.date)
            [dic setObject:[NSDate jsonStringFromDate:_lastMessage.date] forKey:@"lastMessageDate"];
        if(self.type == ConversationTypeRoom){
            if(_lastMessage.peer){
                [dic setObject:_lastMessage.peer.jid forKey:@"lastMessageSender"];
            }
            [dic setObject:_lastMessage.isOutgoing?@YES:@NO forKey:@"isOutgoing"];
        }
        
        if(_lastMessage.callLog){
            [dic setObject:[_lastMessage.callLog dictionaryDescription] forKey:@"call"];
        }
    }
    
    if(self.type != ConversationTypeUser && _peer.displayName){
        [dic setObject:_peer.displayName forKey:@"name"];
    }
    
    if (_peer.rainbowID)
        [dic setObject:_peer.rainbowID forKey:@"peerId"];
    
    if (_peer.jid)
        [dic setObject:_peer.jid forKey:@"jid_im"];
    
    if ([self stringForConversationType])
        [dic setObject:[self stringForConversationType] forKey:@"type"];
    
    [dic setObject:_isMuted?@YES:@NO forKey:@"mute"];
    
    [dic setObject:[NSNumber numberWithInteger:_unreadMessagesCount] forKey:@"unreadMessagesCount"];
    
    return dic;
}
@end
