/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "Group.h"
#import "Group+Internal.h"
#import "NSDate+JSONString.h"

@implementation Group

-(instancetype) init {
    self = [super init];
    if(self){
        _users = [NSMutableArray array];
    }
    return self;
}

-(void) dealloc {
    _users = nil;
}

-(NSString *) description {
    @synchronized(self){
        return [NSString stringWithFormat:@"Group : %p, ID: %@ name: %@ comment %@ owner: %@ creationDate: %@ users: %@", self, _rainbowID, _name, _comment, _owner, _creationDate, _users];
    }
}

-(BOOL) isEqual:(Group *) otherGroup {
    if(self == otherGroup)
        return YES;
    
    return [_rainbowID isEqual:otherGroup.rainbowID];
}

-(NSDictionary *) dictionarayRepresentation {
    NSMutableDictionary *dic = [NSMutableDictionary new];
    @synchronized(self){
        if(_rainbowID)
            [dic setObject:_rainbowID forKey:@"id"];
        if(_name)
            [dic setObject:_name forKey:@"name"];
        
        if(_creationDate)
            [dic setObject:[NSDate jsonStringFromDate:_creationDate] forKey:@"creationDate"];
        
        if(_owner.rainbowID)
            [dic setObject:_owner.rainbowID forKey:@"owner"];
        
        if(_comment)
            [dic setObject:_comment forKey:@"comment"];
        
        NSMutableArray *users = [NSMutableArray new];
        for (Contact *aContact in _users) {
            [users addObject:aContact.rainbowID];
        }
        [dic setObject:users forKey:@"users"];
    }
    return dic;
}
@end
