/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import <Foundation/Foundation.h>
#import "GenericContact.h"


/**
 *  This class adds the properties
 *  that exists only for local contacts and 
 *  never for Rainbow contacts.
 */
@interface LocalContact : GenericContact

/**
 *  The user's ABRecord ID
 */
@property (nonatomic) NSString *addressBookRecordID;

/**
 *  The user's postal addresses
 */
@property (nonatomic, strong) NSMutableArray<PostalAddress*> *addresses;

/**
 *  The user's websites
 */
@property (nonatomic, strong) NSMutableArray<NSString *> *webSitesURL;

/**
 *  String representation of the ABRecord ID
 *
 *  @return String representation of the ABRecord ID
 */
-(NSString *) contactLocalID;

@end
