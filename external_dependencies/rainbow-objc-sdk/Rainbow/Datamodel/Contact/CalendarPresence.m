/*
 * Rainbow
 *
 * Copyright (c) 2017, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "CalendarPresence.h"
#import "CalendarPresenceInternal.h"

@implementation CalendarAutomaticReply

-(instancetype) init {
    self = [super init];
    if(self) {
        _isEnabled = NO;
    }
    return self;
}

@end

@implementation CalendarPresence

-(instancetype) init {
    self = [super init];
    if(self){
        _presence = CalendarPresenceUnavailable;
        _automaticReply = [[CalendarAutomaticReply alloc] init];
    }
    return self;
}

+(NSString *) stringForCalendarPresence:(CalendarPresence*) presence {
    switch (presence.presence) {
        default:
        case CalendarPresenceUnavailable: {
            return @"Offline";
            break;
        }
        case CalendarPresenceAvailable: {
            return @"Available";
            break;
        }
        case CalendarPresenceBusy: {
            return @"Busy";
            break;
        }
        case CalendarPresenceOutOfOffice: {
            return @"OutOfOffice";
            break;
        }
    }
}

-(NSString *) description {
    @synchronized(self){
        return [NSString stringWithFormat:@"%@", [CalendarPresence stringForCalendarPresence:self]];
    }
}

-(BOOL) isEqual:(CalendarPresence *) presence {
    if([presence isKindOfClass:[CalendarPresence class]]){
        return (self.presence == presence.presence && [self.until isEqualToDate:presence.until]);
    }
    return NO;
}

+(CalendarPresence *) presenceAvailable {
    CalendarPresence *available = [CalendarPresence new];
    available.presence = CalendarPresenceAvailable;
    return available;
}

+(CalendarPresence *) presenceBusy {
    CalendarPresence *available = [CalendarPresence new];
    available.presence = CalendarPresenceBusy;
    return available;
}

+(CalendarPresence *) presenceOutOfOffice {
    CalendarPresence *available = [CalendarPresence new];
    available.presence = CalendarPresenceOutOfOffice;
    return available;
}
@end
