/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "GenericContact.h"
#import "NSDictionary+JSONString.h"

@interface GenericContact ()
@property (nonatomic, strong, readwrite) NSMutableArray<PhoneNumber*> *phoneNumbers;
@property (nonatomic, strong, readwrite) NSMutableArray<EmailAddress*> *emailAddresses;
@end

@implementation GenericContact

-(instancetype) init {
    if (self = [super init]) {
        _phoneNumbers = [NSMutableArray array];
        _emailAddresses = [NSMutableArray array];
    }
    return self;
}

-(void) dealloc {
    [_phoneNumbers removeAllObjects];
    _phoneNumbers = nil;
    [_emailAddresses removeAllObjects];
    _emailAddresses = nil;
}

-(NSString *) jsonRepresentation {
    NSMutableDictionary *jsonDic = [NSMutableDictionary dictionary];
    
    if(self.firstName)
        [jsonDic setObject:self.firstName forKey:@"firstName"];
    if(self.lastName)
        [jsonDic setObject:self.lastName forKey:@"lastName"];
    if(self.nickName)
        [jsonDic setObject:self.nickName forKey:@"nickName"];
    if(self.companyName)
        [jsonDic setObject:self.companyName forKey:@"company"];
    if(self.jobTitle)
        [jsonDic setObject:self.jobTitle forKey:@"jobTitle"];
    
    if(self.phoneNumbers.count > 0){
        NSMutableArray *phoneNumbersToJson = [NSMutableArray array];
        for (PhoneNumber *phoneNumber in self.phoneNumbers) {
            [phoneNumbersToJson addObject:[phoneNumber jsonRepresentation]];
        }
        [jsonDic setObject:phoneNumbersToJson forKey:@"phoneNumbers"];
    }
    if(self.emailAddresses.count > 0){
        NSMutableArray *emailAddressToJson = [NSMutableArray array];
        for (EmailAddress *emailAddress in self.emailAddresses) {
            [emailAddressToJson addObject:[emailAddress jsonRepresentation]];
        }
        [jsonDic setObject:emailAddressToJson forKey:@"emails"];
    }
    
    return [jsonDic jsonStringWithPrettyPrint:NO];
}

-(NSArray <EmailAddress*>*) emailAddresses {
    @synchronized(_emailAddresses){
        NSMutableSet *set = [NSMutableSet set];
        [set addObjectsFromArray:_emailAddresses];
        return [set allObjects];
    }
}

-(NSArray <PhoneNumber *> *) phoneNumbers {
    @synchronized(_phoneNumbers){
        NSMutableSet *set = [NSMutableSet set];
        [set addObjectsFromArray:_phoneNumbers];
        return [set allObjects];
    }
}

-(BOOL) phoneNumbersContainsObject:(PhoneNumber *) phoneNumber {
    BOOL found = NO;
    @synchronized(_phoneNumbers){
        found = [_phoneNumbers containsObject:phoneNumber];
    }
    return found;
}

-(void) addPhoneNumberObject:(PhoneNumber *)phoneNumber {
    @synchronized(_phoneNumbers){
        [_phoneNumbers addObject:phoneNumber];
    }
}

-(void) removePhoneNumberObject:(PhoneNumber *)phoneNumber {
    @synchronized(_phoneNumbers){
        [_phoneNumbers removeObject:phoneNumber];
    }
}

-(void) removePhoneNumbersObjectFromArray:(NSArray<PhoneNumber*>*) phoneNumbers {
    @synchronized(_phoneNumbers){
        [_phoneNumbers removeObjectsInArray:phoneNumbers];
    }
}

-(void) removeAllPhoneNumbers {
    @synchronized(_phoneNumbers){
        [_phoneNumbers removeAllObjects];
    }
}

-(BOOL) emailAddressesContainsObject:(EmailAddress *) emailAddress {
    BOOL found = NO;
    @synchronized(_emailAddresses){
        found = [_emailAddresses containsObject:emailAddress];
    }
    return found;
}

-(void) addEmailAddressObject:(EmailAddress *)emailAdress {
    @synchronized(_emailAddresses){
        [_emailAddresses addObject:emailAdress];
    }
}

-(void) removeEmailAddressObject:(EmailAddress *)emailAdress {
    @synchronized(_emailAddresses){
        [_emailAddresses removeObject:emailAdress];
    }
}

-(void) removeEmailAddressesObjectFromArray:(NSArray<EmailAddress*>*) emails {
    @synchronized(_emailAddresses){
        [_emailAddresses removeObjectsInArray:emails];
    }
}

-(void) removeAllEmailAddresses {
    @synchronized(_emailAddresses){
        [_emailAddresses removeAllObjects];
    }
}

@end
