/*
 * Rainbow
 *
 * Copyright (c) 2017, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "CalendarPresence.h"

@interface CalendarPresence ()
@property (nonatomic, strong, readwrite) NSString *status;
@property (nonatomic, readwrite) ContactCalendarPresence presence;
@property (nonatomic, readwrite) NSDate *until;
@end

@interface CalendarAutomaticReply()
@property (nonatomic, readwrite) NSString *message;
@property (nonatomic, readwrite) NSDate *untilDate;
@property (nonatomic, readwrite) BOOL isEnabled;
@property (nonatomic, readwrite) NSDate *lastUpdateDate;
@end
