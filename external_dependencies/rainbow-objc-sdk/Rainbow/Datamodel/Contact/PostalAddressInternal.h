/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "PostalAddress.h"

@interface PostalAddress ()
@property (nonatomic, strong, readwrite) NSString *pobox;
@property (nonatomic, strong, readwrite) NSString *street;
@property (nonatomic, strong, readwrite) NSString *city;
@property (nonatomic, strong, readwrite) NSString *state;
@property (nonatomic, strong, readwrite) NSString *postalCode;
@property (nonatomic, strong, readwrite) NSString *country;
@property (nonatomic, strong, readwrite) NSString *countryCode;
@property (nonatomic, readwrite) PostalAddressType type;

+(PostalAddress *) postalAddressFromDictionary:(NSDictionary *) dictionary;
+(PostalAddressType) typeFromPostalAddressLabel:(NSString *) label;
+(NSString *) stringForPostalAddressType:(PostalAddressType) type;
@end
