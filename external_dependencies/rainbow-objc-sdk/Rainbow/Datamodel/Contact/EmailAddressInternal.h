/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "EmailAddress.h"

@interface EmailAddress () <NSCopying>
@property (nonatomic, strong, readwrite) NSString *address;
@property (nonatomic, readwrite) EmailAddressType type;
@property (nonatomic, readwrite) BOOL isPrefered;
@property (nonatomic, readwrite) BOOL isVisible;
-(instancetype) initWithEmailAddress:(EmailAddress *) emailAddress;
-(void) updateWith:(EmailAddress *) emailAddress;
@end
