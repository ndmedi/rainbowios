/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "RainbowContact.h"

@implementation RainbowContact

-(instancetype) init {
    if (self = [super init]) {
        _groups = [NSMutableArray array];
        _lastUpdateDate = nil;
        _isBot = NO;
    }
    return self;
}

-(void) dealloc {
    [_groups removeAllObjects];
    _groups = nil;
    _requestedInvitation = nil;
    _sentInvitation = nil;
    _companyInvitation = nil;
}

-(void) setCalendarPresence:(CalendarPresence *)calendarPresence {
    _calendarPresence = calendarPresence;
}

@end
