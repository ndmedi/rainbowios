/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "UserSettings.h"
#import "KeychainItemWrapper.h"
#import "Tools.h"
#import "NSDictionary+JSONString.h"
#import "NSData+JSON.h"


static UserSettings* userSettings;

@implementation UserSettings
@synthesize username = _username, password = _password, token = _token;
@synthesize jidIm = _jidIm, jidPwd = _jidPwd, userID = _userID, jidTel = _jidTel;
@synthesize companyID = _companyID, companyName = _companyName, isInitialized = _isInitialized, isGuest = _isGuest, isReadyToCreateConference = _isReadyToCreateConference;
@synthesize userSettingsVersion = _userSettingsVersion;
@synthesize iceServersCredentials = _iceServersCredentials;
@synthesize resourceUniqueID = _resourceUniqueID;

+(UserSettings *) sharedInstance {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        userSettings = [[UserSettings alloc] init];
    });
    return userSettings;
}

-(id) init {
    self = [super init];
    if (self) {
        [self readSettings];
    }
    return self;
}


-(NSString *) username {
    return _username;
}

-(void) setUsername:(NSString *)newUsername {
    if(newUsername != nil && ![_username isEqualToString:newUsername] && _username != newUsername){
        _username = newUsername;
        [[KeychainItemWrapper defaultKeychain] setString:_username forKey:kUsername forService:kServiceApp];
//        NSLog(@"Saving username %@", _username);
    }
}

-(NSString *) password {
    return _password;
}

-(void) setPassword:(NSString *)newPassword {
    if(![_password isEqualToString:newPassword] && _password != newPassword){
        _password = newPassword;
        [[KeychainItemWrapper defaultKeychain] setString:_password forKey:kPassword forService:kServiceApp];
//        NSLog(@"Saving password %@", _password);
    }
}

-(NSString *) token {
    return _token;
}

-(void) setToken:(NSString *)newToken {
    if(![_token isEqualToString:newToken] && _token != newToken){
        _token = newToken;
        [[KeychainItemWrapper defaultKeychain] setString:_token forKey:kToken forService:kServiceToken];
    }
}

-(NSString *) jidIm {
    return _jidIm;
}

-(void) setJidIm:(NSString *)newJidIm {
    if(![_jidIm isEqualToString:newJidIm] && _jidIm != newJidIm){
        _jidIm = newJidIm;
        [[KeychainItemWrapper defaultKeychain] setString:_jidIm forKey:kUsername forService:kServiceXmpp];
    }
}

-(NSString *) jidPwd {
    return _jidPwd;
}

-(void) setJidPwd:(NSString *)newJidPwd {
    if(![_jidPwd isEqualToString:newJidPwd] && _jidPwd != newJidPwd){
        _jidPwd = newJidPwd;
        [[KeychainItemWrapper defaultKeychain] setString:_jidPwd forKey:kPassword forService:kServiceXmpp];
    }
}

-(NSString *) userID {
    return _userID;
}

-(void) setUserID:(NSString *)newUserID {
    if(![_userID isEqualToString:newUserID] && _userID != newUserID){
        _userID = newUserID;
        [[KeychainItemWrapper defaultKeychain] setString:_userID forKey:kUserId forService:kServiceApp];
    }
}

-(NSString *) jidTel {
    return _jidTel;
}

-(void) setJidTel:(NSString *)newJidTel {
    if(![_jidTel isEqualToString:newJidTel] && _jidTel != newJidTel){
        _jidTel = newJidTel;
        [[KeychainItemWrapper defaultKeychain] setString:_jidTel forKey:kJidTel forService:kServiceXmpp];
    }
}

-(NSString *) companyID {
    return _companyID;
}

-(void) setCompanyID:(NSString *)newCompanyID {
    if(![_companyID isEqualToString:newCompanyID] && _companyID != newCompanyID){
        _companyID = newCompanyID;
        [[KeychainItemWrapper defaultKeychain] setString:_companyID forKey:kCompanyID forService:kServiceApp];
    }
}

-(NSString *) companyName {
    return _companyName;
}

-(void) setCompanyName:(NSString *)newCompanyName {
    if(![_companyName isEqualToString:newCompanyName] && _companyName != newCompanyName){
        _companyName = newCompanyName;
        [[KeychainItemWrapper defaultKeychain] setString:_companyName forKey:kCompanyName forService:kServiceApp];
    }
}

-(BOOL) isInitialized {
    return _isInitialized;
}

-(void) setIsInitialized:(BOOL)isInitialized {
    _isInitialized = isInitialized;
    [[KeychainItemWrapper defaultKeychain] setString:NSStringFromBOOL(_isInitialized) forKey:kIsInitialized forService:kServiceApp];
}

-(BOOL) isGuest {
    return _isGuest;
}

-(void) setIsGuest:(BOOL)guestMode {
    _isGuest = guestMode;
    [[KeychainItemWrapper defaultKeychain] setString:NSStringFromBOOL(_isGuest) forKey:kGuestMode forService:kServiceApp];
}

-(BOOL) isReadyToCreateConference {
    return _isReadyToCreateConference;
}

-(void) setIsReadyToCreateConference:(BOOL)isReadyToCreateConference {
    _isReadyToCreateConference = isReadyToCreateConference;
    [[KeychainItemWrapper defaultKeychain] setString:NSStringFromBOOL(_isReadyToCreateConference) forKey:kIsReadyToCreateConference forService:kServiceApp];
}

-(NSString *) userSettingsVersion {
    return _userSettingsVersion;
}

-(void) setUserSettingsVersion:(NSString *)newUserSettingsVersion {
    _userSettingsVersion = newUserSettingsVersion;
    [[KeychainItemWrapper defaultKeychain] setString:_userSettingsVersion forKey:kUserSettingsVersion forService:kServiceApp];
}

-(NSDictionary *) iceServersCredentials {
    return _iceServersCredentials;
}

-(void)setIceServersCredentials:(NSDictionary *)newIceServersCredentials {
    NSString *currentIceServerCredentialsRep = [_iceServersCredentials jsonStringWithPrettyPrint:NO];
    NSString *newIceServerCredentialsRep = [newIceServersCredentials jsonStringWithPrettyPrint:NO];
    if(![currentIceServerCredentialsRep isEqualToString:newIceServerCredentialsRep]){
        _iceServersCredentials = newIceServersCredentials;
        [[KeychainItemWrapper defaultKeychain] setString:newIceServerCredentialsRep forKey:kIceServersCredentials forService:kServiceIceServer];
    }
}

-(NSString *) resourceUniqueID {
    return _resourceUniqueID;
}

-(void) setResourceUniqueID:(NSString *)resourceUniqueID {
    _resourceUniqueID = resourceUniqueID;
    [[KeychainItemWrapper defaultKeychain] setString:_resourceUniqueID forKey:kResourceUniqueID forService:kServiceDeviceResourceUniqueID];
}

-(void) readSettings {
    _username = [[KeychainItemWrapper defaultKeychain] getStringForKey:kUsername forService:kServiceApp];
    _password = [[KeychainItemWrapper defaultKeychain] getStringForKey:kPassword forService:kServiceApp];
    _token = [[KeychainItemWrapper defaultKeychain] getStringForKey:kToken forService:kServiceToken];
    _jidIm = [[KeychainItemWrapper defaultKeychain] getStringForKey:kUsername forService:kServiceXmpp];
    _jidPwd = [[KeychainItemWrapper defaultKeychain] getStringForKey:kPassword forService:kServiceXmpp];
    _jidTel = [[KeychainItemWrapper defaultKeychain] getStringForKey:kJidTel forService:kServiceXmpp];
    _userID = [[KeychainItemWrapper defaultKeychain] getStringForKey:kUserId forService:kServiceApp];
    _companyID = [[KeychainItemWrapper defaultKeychain] getStringForKey:kCompanyID forService:kServiceApp];
    _companyName = [[KeychainItemWrapper defaultKeychain] getStringForKey:kCompanyName forService:kServiceApp];
    NSString *isInitialized = [[KeychainItemWrapper defaultKeychain] getStringForKey:kIsInitialized forService:kServiceApp];
    _isInitialized = [isInitialized isEqualToString:@"YES"]?YES:NO;
    NSString *guestMode = [[KeychainItemWrapper defaultKeychain] getStringForKey:kGuestMode forService:kServiceApp];
    _isGuest = [guestMode isEqualToString:@"YES"]?YES:NO;
    _userSettingsVersion = [[KeychainItemWrapper defaultKeychain] getStringForKey:kUserSettingsVersion forService:kServiceApp];
    NSString *iceServerCredentialsString = [[KeychainItemWrapper defaultKeychain] getStringForKey:kIceServersCredentials forService:kServiceIceServer] ;
    if(iceServerCredentialsString)
        _iceServersCredentials = [[iceServerCredentialsString dataUsingEncoding:NSUTF8StringEncoding] objectFromJSONData];
    NSString *isReadyToCreateConference = [[KeychainItemWrapper defaultKeychain] getStringForKey:kIsReadyToCreateConference forService:kServiceApp];
    _isReadyToCreateConference = [isReadyToCreateConference isEqualToString:@"YES"]?YES:NO;
    _resourceUniqueID = [[KeychainItemWrapper defaultKeychain] getStringForKey:kResourceUniqueID forService:kServiceDeviceResourceUniqueID];
}

-(void) reset {
    [[KeychainItemWrapper defaultKeychain] removeAllCreds];
    _username = nil;
    _password = nil;
    _token = nil;
    _jidIm = nil;
    _jidPwd = nil;
    _userID = nil;
    _jidTel = nil;
    _companyID = nil;
    _companyName = nil;
    _isInitialized = NO;
    _isGuest = NO;
    _userSettingsVersion = kUserSettingsCurrentVersion;
    _iceServersCredentials = nil;
    _isReadyToCreateConference = NO;
    // Resource name is not reset because we need to keep it to avoid multiple ressource issues
    //_resourceUniqueID = nil;
}

@end
