/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import <Foundation/Foundation.h>

#define kUserSettingsCurrentVersion @"5"

@interface UserSettings : NSObject

+(UserSettings *) sharedInstance;
@property (nonatomic, strong) NSString *username;
@property (nonatomic, strong) NSString *password;
@property (nonatomic, strong) NSString *token;
@property (nonatomic, strong) NSString *jidIm;
@property (nonatomic, strong) NSString *jidPwd;
@property (nonatomic, strong) NSString *userID;
@property (nonatomic, strong) NSString *jidTel;
@property (nonatomic, strong) NSString *companyID;
@property (nonatomic, strong) NSString *companyName;
@property (nonatomic) BOOL isInitialized;
@property (nonatomic) BOOL isGuest;
@property (nonatomic) BOOL isReadyToCreateConference;

@property (nonatomic, strong) NSString *userSettingsVersion;

@property (nonatomic, strong) NSDictionary *iceServersCredentials;

@property (nonatomic, strong) NSString *resourceUniqueID;

-(void) reset;
@end
