/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

@interface ConferenceParticipant ()
@property (nonatomic, strong, readwrite) ConferenceParticipantId *participantId;
@property (nonatomic, strong, readwrite) NSString *jidIM;
@property (nonatomic, strong, readwrite) NSString *phoneNumber;
@property (nonatomic, readwrite) ParticipantRole role;
@property (nonatomic, readwrite) BOOL muted;
@property (nonatomic, readwrite) BOOL hold;
@property (nonatomic, readwrite) BOOL isTalking;
@property (nonatomic, readwrite) ParticipantState state;
@property (nonatomic, strong, readwrite) Contact *contact;

-(NSDictionary *) dictionaryRepresentation;

-(void) updateParticipantWithNewParticipant:(ConferenceParticipant *) newParticipant;
@end
