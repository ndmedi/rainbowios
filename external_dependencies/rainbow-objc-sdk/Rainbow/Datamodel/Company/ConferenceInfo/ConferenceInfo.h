/*
 * Rainbow
 *
 * Copyright (c) 2017, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import <Foundation/Foundation.h>
#import "ConferenceParticipant.h"

/**
 *  The different conference event types
 */
typedef NS_ENUM(NSInteger, ConferenceEventType) {
    /**
     *  Type unknown
     */
    ConferenceEventTypeUnknown = 0,
    /**
     *  Type conference state
     */
    ConferenceEventTypeState,
    /**
     *  Type participants added
     */
    ConferenceEventTypeParticipantsAdded,
    /**
     *  Type participants update
     */
    ConferenceEventTypeParticipantsUpdate,
    /**
     *  Type participants removed
     */
    ConferenceEventTypeParticipantsRemoved,
    /**
     *  Type conference talkers
     */
    ConferenceEventTypeTalkers,
    /**
     *  Event for added publishers
     */
    ConferenceEventPublishersAdded,
    /**
     *  Event for removed publishers
     */
    ConferenceEventPublishersRemoved
};


@interface ConferenceInfo : NSObject
@property (nonatomic, readonly) NSString *confId;
@property (nonatomic, readonly) ConferenceEventType eventType;
@property (nonatomic, readonly) BOOL isActive;
@property (nonatomic, readonly) BOOL isRecording;
@property (nonatomic, readonly) BOOL isTalkerActive;
@property (nonatomic, readonly) NSArray<ConferenceParticipant *> *addedParticipants;
@property (nonatomic, readonly) NSArray<ConferenceParticipant *> *updatedParticipants;
@property (nonatomic, readonly) NSArray<NSString *> *removedParticipants;
@property (nonatomic, readonly) NSArray<NSString *> *talkers;
@property (nonatomic, readonly) NSArray<ConferenceParticipant *> *addedPublishers;
@property (nonatomic, readonly) NSArray<ConferenceParticipant *> *removedPublishers;

+(NSString *) stringForConferenceEventType:(ConferenceEventType) eventType;
@end
