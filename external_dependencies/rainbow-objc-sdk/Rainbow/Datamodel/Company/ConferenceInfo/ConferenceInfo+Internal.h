/*
 * Rainbow
 *
 * Copyright (c) 2017, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "ConferenceParticipant.h"

@interface ConferenceInfo ()
@property (readwrite, strong) NSString *confId;
@property (readwrite) ConferenceEventType eventType;
@property (nonatomic, strong, readwrite) NSMutableArray<ConferenceParticipant *> *addedParticipants;
@property (nonatomic, strong, readwrite) NSMutableArray<ConferenceParticipant *> *updatedParticipants;
@property (nonatomic, strong, readwrite) NSMutableArray<NSString *> *removedParticipants;
@property (nonatomic, strong, readwrite) NSMutableArray<NSString *> *talkers;
@property (nonatomic, strong, readwrite) NSArray<ConferenceParticipant *> *addedPublishers;
@property (nonatomic, strong, readwrite) NSArray<ConferenceParticipant *> *removedPublishers;
@property (nonatomic, readwrite) BOOL isActive;
@property (nonatomic, readwrite) BOOL isRecording;
@property (nonatomic, readwrite) BOOL isTalkerActive;
@end
