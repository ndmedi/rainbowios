//
//  AppDelegate.h
//  RainbowSDKTestApp
//
//  Created by Jerome Heymonet on 05/04/2016.
//  Copyright © 2016 ALE. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

