/*
 * Rainbow
 *
 * Copyright (c) 2017, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */
#import "NSArray+MD5.h"

@implementation NSArray (MD5)


// We compute the MD5 checksum on an array as the MD5 checksum of the
// string concatentation of the MD5 checksums of each element.
//
// We exclude any keys/value where the key is contained in exclusionKeySet
//
- (NSString *)MD5ExcludingKeysFromSet:(NSSet *)exclusionKeySet
{
    NSString *resultStr = @"";
    
    NSString *concatenateStr = @"";
    
    for (id obj in self) {
        
        
        if([obj isKindOfClass:[NSArray class]]) {
            NSArray *arr = (NSArray *)obj;
            concatenateStr = [concatenateStr stringByAppendingString:[arr MD5ExcludingKeysFromSet:exclusionKeySet]];
        }
        
        else if([obj isKindOfClass:[NSDictionary class]]) {
            NSDictionary *dict = (NSDictionary *)obj;
            concatenateStr = [concatenateStr stringByAppendingString:[dict MD5ExcludingKeysFromSet:exclusionKeySet]];
        }
        
        else if([obj isKindOfClass:[NSString class]]) {
            NSString *str = (NSString *)obj;
            concatenateStr = [concatenateStr stringByAppendingString:[str MD5]];
        }
        
        else if([obj isKindOfClass:[NSDate class]]) {
            NSDate *date = (NSDate *)obj;
            concatenateStr = [concatenateStr stringByAppendingString:[date MD5]];
        }
        
        else if([obj isKindOfClass:[NSNumber class]]) {
            NSNumber *num = (NSNumber *)obj;
            concatenateStr = [concatenateStr stringByAppendingString:[num MD5]];
        }
        
        else if([obj isKindOfClass:[NSData class]]) {
            NSData *data = (NSData *)obj;
            concatenateStr = [concatenateStr stringByAppendingString:[data MD5]];
        }
        
        else {
            NSLog(@"MD5 not defined for class %@",NSStringFromClass([obj class]));
        }
    }
    
    // Crunch all our concatenated substring hashes into one MD5 hash
    resultStr = [concatenateStr MD5];
    
    return resultStr;
}


// Get a concatenated string of what we would normally use for an MD5 calculation for debugging.
//
// We exclude any keys/value where the key is contained in exclusionKeySet
//
- (NSString *)stringExcludingKeysFromSet:(NSSet *)exclusionKeySet;
{
    NSString *concatenateStr = @"";
    
    for (id obj in self) {
        if([obj isKindOfClass:[NSArray class]]) {
            NSArray *arr = (NSArray *)obj;
            concatenateStr = [concatenateStr stringByAppendingString:[arr stringExcludingKeysFromSet:exclusionKeySet]];
        }
        
        else if([obj isKindOfClass:[NSDictionary class]]) {
            NSDictionary *dict = (NSDictionary *)obj;
            concatenateStr = [concatenateStr stringByAppendingString:[dict stringExcludingKeysFromSet:exclusionKeySet]];
        }
        
        else if([obj isKindOfClass:[NSString class]]) {
            NSString *str = (NSString *)obj;
            concatenateStr = [concatenateStr stringByAppendingString:[str string]];
        }
        
        else if([obj isKindOfClass:[NSDate class]]) {
            NSDate *date = (NSDate *)obj;
            concatenateStr = [concatenateStr stringByAppendingString:[date string]];
        }
        
        else if([obj isKindOfClass:[NSNumber class]]) {
            NSNumber *num = (NSNumber *)obj;
            concatenateStr = [concatenateStr stringByAppendingString:[num string]];
        }
        
        // Method not defined for NSData
//        else if([obj isKindOfClass:[NSData class]]) {
//            NSData *data = (NSData *)obj;
//            concatenateStr = [concatenateStr stringByAppendingString:[data string]];
//        }
        
        else {
            NSLog(@"string method not defined for class %@",NSStringFromClass([obj class]));
        }
    }
    
    return concatenateStr;
}

@end
