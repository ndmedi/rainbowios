/*
 * Rainbow
 *
 * Copyright (c) 2017, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "NSNumber+MD5.h"

@implementation NSNumber (MD5)

- (NSString *)MD5
{
    // NSLog(@"stringValue of NSNumber=%@",[self stringValue]);
    
    return [[self stringValue] MD5];
    
}

- (NSString *)string
{
    return [self stringValue];
}

@end


