/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "NSDate+Distance.h"
#import "NSDate+Utilities.h"

// from NSDate+Utilities.m
static const unsigned componentFlags = (NSCalendarUnitYear| NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitWeekOfYear |  NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond | NSCalendarUnitWeekday | NSCalendarUnitWeekdayOrdinal);

@implementation NSDate (Distance)
- (NSInteger) distanceInSecondsToDate:(NSDate *) anotherDate {
    if([self isEarlierThanDate:anotherDate]){
        NSTimeInterval distanceBetweenDates = [anotherDate timeIntervalSinceDate:self];
        return distanceBetweenDates;
    } else {
        NSTimeInterval distanceBetweenDates = [self timeIntervalSinceDate:anotherDate];
        return distanceBetweenDates;
    }
}

- (NSInteger) distanceInSecondsToNow {
    return [self distanceInSecondsToDate:[NSDate date]];
}

- (NSDate *) dateAtEndOfYear
{
    NSDateComponents *components = [[NSDate currentCalendar] components:componentFlags fromDate:self];
    components.month = 12;
    components.day = 31;
    components.hour = 23;
    components.minute = 59;
    components.second = 59;
    return [[NSDate currentCalendar] dateFromComponents:components];
}
@end
