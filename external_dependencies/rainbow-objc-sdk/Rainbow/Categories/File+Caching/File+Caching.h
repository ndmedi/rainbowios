//
//  File+Caching.h
//  Rainbow
//
//  Created by Alaa Bzour on 12/13/18.
//  Copyright © 2018 ALE. All rights reserved.
//

#import <Rainbow/Rainbow.h>

@interface File (Caching)

-(void)saveFileDataInCache;

@end
