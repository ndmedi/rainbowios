/*
 * Rainbow
 *
 * Copyright (c) 2017, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "NSDate+Seconds.h"

@implementation NSDate (Seconds)
- (NSDate *) dateByAddingSeconds: (NSInteger) dSeconds {
    NSDateComponents *components = [[NSDateComponents alloc] init];
    components.second = dSeconds;
    return [[NSCalendar currentCalendar] dateByAddingComponents:components toDate:self options:0];
}
- (NSDate *) dateBySubtractingSeconds: (NSInteger) dSeconds {
    return [self dateByAddingSeconds:-dSeconds];
}
@end
