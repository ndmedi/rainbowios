/*
 * Rainbow
 *
 * Copyright (c) 2017, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import <UIKit/UIKit.h>
#import <Rainbow/Peer.h>
#import <Rainbow/Conversation.h>
#import "UIAvatarView.h"

#define kTableViewReusableKey @"UIRainbowGenericTableViewCell"
#define kRainbowGenericTableViewCellHeight 74

typedef void(^RightButtonTapHandler)(UIButton* sender);


@protocol UIRainbowGenericTableViewCellProtocol <NSObject>
@required
-(NSString *) mainLabel;
-(NSString *) subLabel;
-(NSDate *) date;
-(NSString *) bottomRightLabel;
-(Peer *) avatar;
-(NSArray<NSString *> *) observablesKeyPath;
-(NSInteger) badgeValue;
-(NSString *) cellButtonTitle;
-(UIImage *) cellButtonImage;
-(UIImage *) rightIcon;
-(UIColor *) rightIconTintColor;

@optional
-(UIColor *) cellButtonImageColor;
-(UIColor *) mainLabelTextColor;
-(UIFont *) mainLabelFont;
-(UIColor *) dateLabelTextColor;
-(BOOL) showMaskedView;
-(UIImage *) maskedImage;
-(UIColor *) maskedViewColor;
-(NSString *) midLabel;
-(UIColor *) midLabelTextColor;
@end

@interface UIRainbowGenericTableViewCell : UITableViewCell
@property (nonatomic,strong) NSObject <UIRainbowGenericTableViewCellProtocol> *cellObject;
@property (nonatomic,strong) RightButtonTapHandler cellButtonTapHandler;
@property (nonatomic) BOOL showMidLabel;
@end
