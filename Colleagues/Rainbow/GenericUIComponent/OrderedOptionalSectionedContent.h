/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import <Foundation/Foundation.h>

@interface OrderedOptionalSectionedContent<ObjectType> : NSObject

/**
 *  If you set this, the filter is applied before returning the
 *  data on sectionForKey: and allNotEmptySections
 *  To cancel the filtering, set this to nil
 */
@property (nonatomic, strong) NSPredicate *filteringPredicate;

/**
 *  Initialize the Component with a list of section names
 *
 *  @param sections the array of section names *ordered*
 *
 *  @return The initialized component
 */
-(instancetype) initWithSections:(NSArray<NSString*> *) sections;

/**
 *  Add a new object to the component, in a given section
 *  The section is created and added at the end if it does not exists
 *
 *  @param anObject The object to add
 *  @param section  The section where the object should be added
 */
- (void)addObject:(ObjectType)anObject toSection:(NSString *) section;

/**
 *  Remove an object from the given section
 *
 *  @param anObject The object to remove
 *  @param section  The section where the object should be removed
 */
-(void) removeObject:(id) anObject fromSection:(NSString *) section;

/**
 *  Remove an object from any section.
 *
 *  @param anObject The object to remove
 */
-(void) removeObject:(id) anObject;

-(BOOL) containsObject:(id) anObject inSection:(NSString *) section;

/**
 *  Remove all the objects stored in the component
 *  but this does not remove the sections (and thus the order of the sections)
 */
-(void)removeAllObjects;

/**
 *  Remove all the objects for a given section
 *
 *  @param section
 */
-(void) removeAllObjectsInSection:(NSString *) section;

/**
 *  Return all the object for a given section
 *  filtering is applied if filteringPredicate is set
 *
 *  @param section The section
 *
 *  @return Array of all the objects for this section
 */
-(NSMutableArray *)sectionForKey:(NSString *) section;

/**
 *  Return all the sections which are NOT empty
 *  filtering is applied if filteringPredicate is set
 *
 *  @return Array of all the non-empty sections
 */
-(NSArray<NSString *> *) allNotEmptySections;

@end
