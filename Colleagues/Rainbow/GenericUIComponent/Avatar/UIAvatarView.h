/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import <Rainbow/Peer.h>
#import <UIKit/UIKit.h>

@interface UIAvatarView : UIView
@property (nonatomic, readwrite) BOOL asCircle;
@property (nonatomic, readwrite, strong) Peer* peer;
@property (nonatomic, readwrite) BOOL showPresence;
@property (nonatomic, readwrite) BOOL withBorder;
@property (nonatomic) NSInteger cornerRadius;
@property (nonatomic) BOOL blackAndWhite;
-(void) setSelected:(BOOL)selected animated:(BOOL)animated;
-(void) setHighlighted:(BOOL)highlighted animated:(BOOL)animated;
+(void) setNoAvatarImageName:(NSString *)imageName;
-(void) drawPhotoFromOrderedList:(NSOrderedSet*) orderedList;
-(void) setAvatarFromData:(NSData*) data;
@end
