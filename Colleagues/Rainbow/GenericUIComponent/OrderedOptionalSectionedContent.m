/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "OrderedOptionalSectionedContent.h"

@interface OrderedOptionalSectionedContent ()
@property (nonatomic, strong) NSMutableArray<NSString *> *sectionNames;
@property (nonatomic, strong) NSMutableDictionary<NSString *, NSMutableArray *> *sectionContents;
@end


@implementation OrderedOptionalSectionedContent

-(instancetype) init {
    self = [super init];
    if(self) {
        [self commonInit];
    }
    return self;
}

-(instancetype) initWithSections:(NSArray<NSString *> *)sections {
    self = [super init];
    if (self) {
        [self commonInit];
        
        for (NSString *section in sections) {
            if (section) {
                [self addSection:section];
            }
        }
    }
    return self;
}

-(void) commonInit {
    _sectionNames = [NSMutableArray new];
    _sectionContents = [NSMutableDictionary new];
}

-(void) dealloc {
    [self removeAllSections];
    _sectionNames = nil;
    _sectionContents = nil;
    _filteringPredicate = nil;
}

-(void) addSection:(NSString *) section {
    if(![_sectionNames containsObject:section]) {
        [_sectionNames addObject:section];
        [_sectionContents setObject:[NSMutableArray new] forKey:section];
    }
}

-(void) addObject:(id)anObject toSection:(NSString *)section {
    NSMutableArray *sectionArray = [_sectionContents objectForKey:section];
    
    if (sectionArray) {
        [sectionArray addObject:anObject];
    } else {
        // This section doesn't exist so create it
        [self addSection:section];
        [self addObject:anObject toSection:section];
    }
}

-(void) removeObject:(id) anObject fromSection:(NSString *) section {
    [[_sectionContents objectForKey:section] removeObject:anObject];
}

-(void) removeObject:(id) anObject {
    __block NSMutableArray<NSString *> *sectionsOfObject = [NSMutableArray new];
    
    [_sectionContents enumerateKeysAndObjectsUsingBlock:^(NSString * section, NSMutableArray * sectionContent, BOOL * stopContent) {
        [sectionContent enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL * stopObject) {
            if([anObject isEqual:obj]){
                [sectionsOfObject addObject:section];
                *stopObject = YES;
            }
        }];
    }];
    
    for (NSString *section in sectionsOfObject) {
        [self removeObject:anObject fromSection:section];
    }
}

-(BOOL) containsObject:(id) anObject inSection:(NSString *) section {
    return [[_sectionContents objectForKey:section] containsObject:anObject];
}

-(NSArray<NSString *> *) allNotEmptySections {
    NSMutableArray *notEmptySections = [NSMutableArray new];
    
    // Use section names to have an ordered list of names.
    for (NSString *section in _sectionNames) {
        NSArray *sectionArray = [_sectionContents objectForKey:section];
        if(_filteringPredicate) {
            sectionArray = [sectionArray filteredArrayUsingPredicate:_filteringPredicate];
        }
        if ([sectionArray count]) {
            [notEmptySections addObject:section];
        }
    }
    return notEmptySections;
}

- (void)removeAllSections {
    [self removeAllObjects];
    [_sectionNames removeAllObjects];
    [_sectionContents removeAllObjects];
}

- (void)removeAllObjects {
    // Empty all the sections
    for (NSString *section in _sectionContents) {
        [[_sectionContents objectForKey:section] removeAllObjects];
    }
}

-(void) removeAllObjectsInSection:(NSString *) section {
    [[_sectionContents objectForKey:section] removeAllObjects];
}

- (NSMutableArray *)sectionForKey:(NSString *) section {
    NSMutableArray *sectionContent = [_sectionContents objectForKey:section];
    
    if(_filteringPredicate) {
        [sectionContent filterUsingPredicate:_filteringPredicate];
    }
    
    return sectionContent;
}

-(NSString *) description {
    return [NSString stringWithFormat:@"%@ %p : <%@>", [self class], self, _sectionContents];
}

@end
