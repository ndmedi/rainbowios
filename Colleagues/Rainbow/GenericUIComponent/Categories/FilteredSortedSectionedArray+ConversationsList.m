//
//  FilteredSortedSectionedArray+ConversationsList.m
//  Rainbow-iOS
//
//  Created by Asal Tech on 7/24/18.
//  Copyright © 2018 ALE International. All rights reserved.
//

#import "FilteredSortedSectionedArray+ConversationsList.h"
#import <Rainbow/Rainbow.h>

@implementation FilteredSortedSectionedArray (ConversationsList)

-(BOOL)containsConversation:(Conversation *)converastion {
    NSString *theConversationId = converastion.conversationId;
    NSArray *conversationsIds = [self.objects valueForKey:@"conversationId"];
    if ([conversationsIds containsObject:theConversationId]) {
        return YES;
    }
    else {
        __block BOOL alreadyExists = [self containsObject:converastion];
        [self enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            Conversation *aConversation = obj;
            if ([aConversation.peer.jid isEqualToString:converastion.peer.jid]) {
                if ([aConversation.conversationId containsString:@"fake_"]) {
                    [self removeObject:aConversation];
                    alreadyExists = NO;
                }
                else {
                    alreadyExists = YES;
                }
                *stop = YES;
            }
        }];
        return alreadyExists;
    }
}

@end
