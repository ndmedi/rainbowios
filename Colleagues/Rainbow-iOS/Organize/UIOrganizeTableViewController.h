//
//  UIOrganizeTableViewController.h
//  Rainbow-iOS
//
//  Created by AsalTech on 3/13/18.
//  Copyright © 2018 ALE International. All rights reserved.
//

#import <UIKit/UIKit.h>
@class UIOrganizeTableViewController;

@protocol OrganizeControllerDelegate <NSObject>
@required

-(void) willSortBySortOrder:(NSInteger) selctedRow;
-(void) willFilterByFilterType:(NSInteger) selctedRow;
@optional
-(void) didFooterPressed;

@end

@interface UIOrganizeTableViewController : UITableViewController

@property (nonatomic, assign) id<OrganizeControllerDelegate> delegate;

@property (nonatomic, strong) NSArray *sortArray;
@property (nonatomic, strong) NSArray *filterArray;
@property (nonatomic) NSInteger selectedSortIndex;
@property (nonatomic) NSInteger selectedFilterIndex;
@property (nonatomic) BOOL isSubView;

-(void) setViewHorizontalPosition:(CGFloat) horizontalPosition;



@end
