//
//  MyMeetingsCell.m
//  Rainbow-iOS
//
//  Created by Le Trong Nghia Huynh on 08/02/2019.
//  Copyright © 2019 ALE International. All rights reserved.
//

#import <Rainbow/ServicesManager.h>
#import "MyMeetingsCell.h"
#import "UITools.h"

@implementation MyMeetingsCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    // Update badge for meetings
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(shouldUpdateRoomBadgeValue:) name:kRoomsServiceDidReceiveRoomInvitation object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(shouldUpdateRoomBadgeValue:) name:kRoomsServiceDidRoomInvitationStatusChanged object:nil];
    
    _counterNewMeetingsLabel.clipsToBounds = YES;
    _counterNewMeetingsLabel.layer.cornerRadius = _counterNewMeetingsLabel.frame.size.height/2;
    _counterNewMeetingsLabel.backgroundColor = [UITools notificationBadgeColor];
    [self shouldUpdateRoomBadgeValue:nil];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void) shouldUpdateRoomBadgeValue:(NSNotification *) notification {
    NSUInteger totalConferenceRooms = [[ServicesManager sharedInstance].roomsService numberOfPendingConferenceRoomInvitations];
    _counterNewMeetings = (int)totalConferenceRooms;
    _counterNewMeetingsLabel.hidden = _counterNewMeetings <= 0;

    if (_counterNewMeetings > 9)
        [_counterNewMeetingsLabel setText:@"9+"];
    else if (_counterNewMeetings > 0)
        [_counterNewMeetingsLabel setText:[NSString stringWithFormat:@"%i", _counterNewMeetings]];
    else
        [_counterNewMeetingsLabel setText:@""];
}

@end
