/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import <Foundation/Foundation.h>
#import "ChangePasswordViewController.h"
#import "UITools.h"
#import <Rainbow/ServicesManager.h>


@interface ACFloatingTextField (internal)
-(void)floatTheLabel;
@end


@interface ChangePasswordViewController ()
@property (weak, nonatomic) IBOutlet UIBarButtonItem *saveButton;
@property (weak, nonatomic) IBOutlet ACFloatingTextField *currentPasswordField;
@property (weak, nonatomic) IBOutlet ACFloatingTextField *typedPasswordField;
@property (weak, nonatomic) IBOutlet ACFloatingTextField *confirmPasswordField;
@end

@implementation ChangePasswordViewController

- (void) viewDidLoad {
    [super viewDidLoad];
    
    self.title = NSLocalizedString(@"Change my password", nil);
    self.navigationController.navigationBar.translucent = NO;
    [self.view setBackgroundColor:[UITools defaultBackgroundColor]];
    
    _saveButton.enabled = NO;
    
    [_currentPasswordField setPlaceholder:NSLocalizedString(@"Current password", nil)];
    [UITools applyCustomFontToTextField:_currentPasswordField];
    _currentPasswordField.lineColor = [UITools defaultTintColor];
    _currentPasswordField.selectedLineColor = [UITools defaultTintColor];
    _currentPasswordField.placeHolderColor = [UITools defaultTintColor];
    _currentPasswordField.selectedPlaceHolderColor = [UITools defaultTintColor];
    [_currentPasswordField setDelegate:self];
    
    [_typedPasswordField setPlaceholder:NSLocalizedString(@"New password", nil)];
    [UITools applyCustomFontToTextField:_typedPasswordField];
    _typedPasswordField.lineColor = [UITools defaultTintColor];
    _typedPasswordField.selectedLineColor = [UITools defaultTintColor];
    _typedPasswordField.placeHolderColor = [UITools defaultTintColor];
    _typedPasswordField.selectedPlaceHolderColor = [UITools defaultTintColor];
    [_typedPasswordField setDelegate:self];
    
    [_confirmPasswordField setPlaceholder:NSLocalizedString(@"Confirm your new password", nil)];
    [UITools applyCustomFontToTextField:_typedPasswordField];
    _confirmPasswordField.lineColor = [UITools defaultTintColor];
    _confirmPasswordField.selectedLineColor = [UITools defaultTintColor];
    _confirmPasswordField.placeHolderColor = [UITools defaultTintColor];
    _confirmPasswordField.selectedPlaceHolderColor = [UITools defaultTintColor];
    [_confirmPasswordField setDelegate:self];
    
}

- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [_currentPasswordField becomeFirstResponder];
}

- (IBAction)tappedChangeButton:(UIButton *)sender {
    [self savePassword];
}

-(void) savePassword {

    NSString *oldPassword = _currentPasswordField.text;
    NSString *newPassword = _typedPasswordField.text;

    // display a loading spinner
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.labelText = NSLocalizedString(@"Saving", nil);
    hud.removeFromSuperViewOnHide = YES;
    [hud setLabelFont:[UIFont fontWithName:[UITools defaultFontName] size:14.0f]];
    
    // Send request to server
    if( [newPassword isEqualToString: _confirmPasswordField.text] ) {
        [[ServicesManager sharedInstance].loginManager sendChangePassword:oldPassword newPassword:newPassword completionHandler:^(NSDictionary *jsonResponse, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if(error) {
                    // Error to treat
                    NSLog(@"Change password error %@", error);
                    [hud hide:YES];
                    [self checkErrorInJsonResponse:jsonResponse];
                } else {
                    NSLog(@"Change password success");
                    
                    // Update with the new password
                    [[ServicesManager sharedInstance].loginManager setUsername: [ServicesManager sharedInstance].myUser.username andPassword:newPassword];
                    [[ServicesManager sharedInstance].loginManager connect];
                    
                    // Modification succeed. Return to previous view
                    hud.mode = MBProgressHUDModeCustomView;
                    hud.labelText = NSLocalizedString(@"Password changed", nil);
                    hud.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Checkmark"]];
                    [hud showAnimated:YES whileExecutingBlock:^{sleep(1);} completionBlock:^{
                        [self.navigationController popViewControllerAnimated:YES];
                    }];
                }
                
            });
            
        }];
    } else {
        NSLog(@"%@ != %@", _typedPasswordField, _confirmPasswordField);
        [hud hide:YES];
        [self showErrorAlertWithMessage:NSLocalizedString(@"Invalid password", nil) exitOnError:YES];
    }
}

- (UIStatusBarStyle) preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (BOOL) shouldAutorotate {
    return NO;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return UIInterfaceOrientationPortrait;
}

-(void) showErrorAlertWithTitle:(NSString *) title andMessage:(NSString *)message exitOnError:(BOOL) exit {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
    }]];
    [self presentViewController:alert animated:YES completion:nil];
}

-(void) showErrorAlertWithMessage:(NSString *) message exitOnError:(BOOL) exit{
    [self showErrorAlertWithTitle:NSLocalizedString(@"Error", nil) andMessage:message exitOnError:exit];
}

-(void) checkErrorInJsonResponse:(NSDictionary *) jsonResponse {
    if(jsonResponse){
        NSNumber *errorCode = (NSNumber*)jsonResponse[@"errorCode"];
        
        if(errorCode.integerValue == 400) {
            NSNumber *detailedCode = (NSNumber *)jsonResponse[@"errorDetailsCode"];
            
            if(detailedCode.integerValue == 400400){
                [self showErrorAlertWithMessage:NSLocalizedString(@"Invalid password", nil) exitOnError:YES];
            } else if(detailedCode.integerValue == 400000){
                [self showErrorAlertWithTitle:NSLocalizedString(@"Invalid password",nil) andMessage:NSLocalizedString(@"Password must be more than 8 characters and contain \n•1 lowercase letter \n•1 uppercase letter \n•1 number \n•1 special charater",nil) exitOnError:NO];
            } else {
                [self showErrorAlertWithMessage:NSLocalizedString(@"An unknown error occured", nil) exitOnError:YES];
            }
            
        } else if(errorCode.integerValue == 401) {
            NSNumber *detailedCode = (NSNumber *)jsonResponse[@"errorDetailsCode"];
            
            if(detailedCode.integerValue == 401202){
                [self showErrorAlertWithMessage:NSLocalizedString(@"Unauthorized access", nil) exitOnError:YES];
            } else if(detailedCode.integerValue == 401510){
                [self showErrorAlertWithMessage:NSLocalizedString(@"User not found", nil) exitOnError:YES];
            } else if(detailedCode.integerValue == 401520){
                [self showErrorAlertWithMessage:NSLocalizedString(@"User not activated", nil) exitOnError:YES];
            } else if(detailedCode.integerValue == 401521){
                [self showErrorAlertWithMessage:NSLocalizedString(@"Company not activated", nil) exitOnError:YES];
            } else {
                [self showErrorAlertWithMessage:NSLocalizedString(@"An unknown error occured", nil) exitOnError:YES];
            }
        }
    }
}

- (IBAction)textFieldEditingChanged:(UITextField *)sender {
    if( _currentPasswordField.text.length > 0 && _typedPasswordField.text.length > 0 && _confirmPasswordField.text.length > 0 ) {
        _saveButton.enabled = YES;
    } else {
        _saveButton.enabled = NO;
    }
    
    if( _typedPasswordField.text.length > 0 && _confirmPasswordField.text.length > 0 && ![_typedPasswordField.text isEqualToString:_confirmPasswordField.text] ) {
        _typedPasswordField.lineColor = [UIColor redColor];
        _typedPasswordField.selectedLineColor = [UIColor redColor];
        _confirmPasswordField.lineColor = [UIColor redColor];
        _confirmPasswordField.selectedLineColor = [UIColor redColor];
    } else {
        _typedPasswordField.lineColor = [UITools defaultTintColor];
        _typedPasswordField.selectedLineColor = [UITools defaultTintColor];
        _confirmPasswordField.lineColor = [UITools defaultTintColor];
        _confirmPasswordField.selectedLineColor = [UITools defaultTintColor];
    }
    
    [_currentPasswordField floatTheLabel];
    [_typedPasswordField floatTheLabel];
    [_confirmPasswordField floatTheLabel];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if( textField == _currentPasswordField ) {
        [_typedPasswordField becomeFirstResponder];
    } else if( textField == _typedPasswordField ) {
        [_confirmPasswordField becomeFirstResponder];

    } else if( textField == _confirmPasswordField) {
        if( _saveButton.enabled ) {
            [self savePassword];
        }
    }
    
    return YES;
}

@end
