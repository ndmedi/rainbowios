/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "MyPresenceCell.h"
#import <Rainbow/Tools.h>
#import "UITools.h"
#import <Rainbow/ServicesManager.h>

@interface MyPresenceCell ()
@property (nonatomic, weak) IBOutlet UIImageView *presenceView;
@property (nonatomic, weak) IBOutlet UIImageView *checkMarkView;
@end

@implementation MyPresenceCell

- (void)awakeFromNib {
    [super awakeFromNib];
    _presenceView.layer.borderWidth = 3.0f;
    _presenceView.layer.borderColor = [UIColor whiteColor].CGColor;
    _presenceView.layer.cornerRadius = _presenceView.frame.size.width/2.0;
    [UITools applyCustomFontTo:_presenceLabel];
    [_checkMarkView setTintColor:[UITools defaultTintColor]];
    _checkMarkView.hidden = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    _presenceView.backgroundColor = [UITools colorForPresence:_presence];
}

-(void) setHighlighted:(BOOL)highlighted animated:(BOOL)animated {
    [super setHighlighted:highlighted animated:animated];
    _presenceView.backgroundColor = [UITools colorForPresence:_presence];
}

-(void) setPresence:(Presence *)presence {
    _presence = nil;
    _presence = presence;
    _presenceView.backgroundColor = [UITools colorForPresence:presence];
    if(presence.presence == ContactPresenceDoNotDisturb)
        _presenceView.image = [UIImage imageNamed:@"dnd"];
    else
        _presenceView.image = nil;
    
    _presenceLabel.text = NSLocalizedString([Presence stringForContactPresence:presence],nil);
    if(presence.presence == ContactPresenceAway)
        _presenceLabel.text = NSLocalizedString(@"Manual Away",nil);
    if([presence isEqual:[Presence presenceExtendedAway]]){
        _presenceView.layer.borderColor = [UIColor lightGrayColor].CGColor;
        _presenceView.layer.borderWidth = 3.0f;
    } else {
        _presenceView.layer.borderColor = [UIColor whiteColor].CGColor;
        _presenceView.layer.borderWidth = 3.0f;
    }
}

-(void) setIsCheckMarkEnabled:(BOOL)isCheckMarkEnabled {
    _isCheckMarkEnabled = isCheckMarkEnabled;
    _checkMarkView.hidden = !_isCheckMarkEnabled;
    self.accessibilityValue = _isCheckMarkEnabled ? @"Active" : @"Inactive";
}

@end
