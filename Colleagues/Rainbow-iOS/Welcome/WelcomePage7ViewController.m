/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import <UIKit/UIKit.h>
#import "WelcomePageCommonViewController.h"
#import "UITools.h"
#import <Rainbow/ServicesManager.h>
#import <Rainbow/defines.h>

@interface WelcomePage7ViewController : WelcomePageCommonViewController
@end

@interface WelcomePage7ViewController ()
@property (weak, nonatomic) IBOutlet UILabel *presentationLabel2;
@property (weak, nonatomic) IBOutlet UILabel *bottomLabel;
@end

@implementation WelcomePage7ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [UITools applyCustomFontTo:_presentationLabel2];
    
    self.presentationLabel.text = NSLocalizedString(@"Communicate with all your contacts in audio", nil);
    _presentationLabel2.text = NSLocalizedString(@"Rainbow needs access to your microphone to allow you to call them with Rainbow", nil);
    [UITools applyThinCustomFontTo:_bottomLabel];
    _bottomLabel.text = NSLocalizedString(@"This settings can be changed at any time in Rainbow settings", nil);
    self.continueButton.enabled = YES;
    self.continueButton.alpha = 1.0f;

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(microphoneAccessChanged:) name:kRTCServiceDidAllowMicrophoneNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(microphoneAccessChanged:) name:kRTCServiceDidRefuseMicrophoneNotification object:nil];
}

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

-(void) dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kRTCServiceDidRefuseMicrophoneNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kRTCServiceDidAllowMicrophoneNotification object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)continueButtonTapped:(UIButton *)sender {
    if(![ServicesManager sharedInstance].rtcService.microphoneAccessAlreadyDetermined){
        [[ServicesManager sharedInstance].rtcService requestMicrophoneAccess];
    } else {
        [self.delegate nextPage];
    }
}

- (void)didTapInBackground:(UITapGestureRecognizer *)sender {
    // Nothing to do
}

-(void)microphoneAccessChanged:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self microphoneAccessChanged:notification];
        });
        return;
    }
    
    if(![ServicesManager sharedInstance].rtcService.microphoneAccessGranted){
        _presentationLabel2.text = NSLocalizedString(@"We will not use your microphone", nil);
        _bottomLabel.textColor = [UIColor redColor];
        _bottomLabel.font = [UIFont fontWithName:[UITools defaultFontName] size:15];
    } else {
        [self.delegate nextPage];
    }
}
// should display microphone authorisation view
-(BOOL) shouldDisplay {
    if(self.delegate.errorDuringWizardSteps)
        return NO;
    if([ServicesManager sharedInstance].rtcService.microphoneAccessAlreadyDetermined){
        NSLog(@"Microphone access already determined, skip this screen");
        return NO;
    }
    
    return YES;
}

@end
