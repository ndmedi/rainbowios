/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import <UIKit/UIKit.h>
#import "WelcomePageCommonViewController.h"
#import "UITools.h"
#import <Rainbow/defines.h>
#import "UINotificationManager.h"
#import <Rainbow/ServicesManager.h>

@interface WelcomePage5ViewController : WelcomePageCommonViewController
@end

@interface WelcomePage5ViewController ()
@property (weak, nonatomic) IBOutlet UILabel *presentationLabel2;
@property (weak, nonatomic) IBOutlet UILabel *bottomLabel;
@end

@implementation WelcomePage5ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [UITools applyCustomFontTo:_presentationLabel2];
    
    self.presentationLabel.text = NSLocalizedString(@"Rainbow will send you notifications", nil);
    _presentationLabel2.text = NSLocalizedString(@"Please enable access to notification for rainbow application", nil);
    [UITools applyThinCustomFontTo:_bottomLabel];
    _bottomLabel.text = NSLocalizedString(@"This settings can be changed at any time in Rainbow settings", nil);
    self.continueButton.enabled = YES;
    self.continueButton.alpha = 1.0f;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didRegisterForRemoteWithDeviceToken:) name:UIApplicationDidRegisterForRemoteNotificationWithDeviceToken object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didFailToRegisterForRemoteNotificationsWithError:) name:UIApplicationDidFailToRegisterForRemoteNotificationsWithError object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didRegisterUserNotificationSettings:) name:UIApplicationDidRegisterUserNotificationSettings object:nil];
}

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

-(void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidRegisterForRemoteNotificationWithDeviceToken object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidFailToRegisterForRemoteNotificationsWithError object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidRegisterUserNotificationSettings object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)continueButtonTapped:(UIButton *)sender {
    BOOL hasAlreadyDisplayNotificationScreen = [[NSUserDefaults standardUserDefaults] boolForKey:kRemotePushNotificationsWizardPageSeen];
    if(![UIApplication sharedApplication].isRegisteredForRemoteNotifications && !hasAlreadyDisplayNotificationScreen){
#if TARGET_IPHONE_SIMULATOR
        [self.delegate nextPage];
#else
        [self registerForUserNotification];
#endif
    } else {
        if (!hasAlreadyDisplayNotificationScreen)
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kRemotePushNotificationsWizardPageSeen];
        
        [self.delegate nextPage];
    }
}

- (void)didTapInBackground:(UITapGestureRecognizer *)sender {
    // Nothing to do
}

#pragma mark - Remote push notifications
-(void) didRegisterForRemoteWithDeviceToken:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didRegisterForRemoteWithDeviceToken:notification];
        });
        return;
    }
    
    NSString *deviceToken = (NSString *) notification.object;
    NSLog(@"didRegisterForRemoteNotificationsWithDeviceToken %@",deviceToken);
    [self.delegate nextPage];
}

-(void) didFailToRegisterForRemoteNotificationsWithError:(NSNotification *) notification {
    NSError *err = (NSError *) notification.object;
    NSLog(@"didFailToRegisterForRemoteNotificationsWithError %@",[err description]);
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didFailToRegisterForRemoteNotificationsWithError:notification];
        });
        return;
    }
    
    _presentationLabel2.text = NSLocalizedString(@"We will not send you notifications", nil);
    _bottomLabel.textColor = [UIColor redColor];
    _bottomLabel.font = [UIFont fontWithName:[UITools defaultFontName] size:15];
}

#pragma mark - Notifications management
-(void) registerForUserNotification {
    [[ServicesManager sharedInstance].notificationsManager registerForUserNotificationsSettings];
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kRemotePushNotificationsWizardPageSeen];
}

-(void) didRegisterUserNotificationSettings:(NSNotification *) notification {
    UIUserNotificationSettings *notificationSettings = (UIUserNotificationSettings*)notification.object;
    if(notificationSettings.types == UIUserNotificationTypeNone){
        _presentationLabel2.text = NSLocalizedString(@"We will not send you notifications", nil);
        _bottomLabel.textColor = [UIColor redColor];
        _bottomLabel.font = [UIFont fontWithName:[UITools defaultFontName] size:15];
    } else {
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
}

-(void) showErrorAlertWithMessage:(NSString *) message {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Notifications", nil) message:message preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        [self.delegate nextPage];
    }]];
    [self presentViewController:alert animated:YES completion:nil];
}

// Should display notifications ??
-(BOOL) shouldDisplay {
#if TARGET_IPHONE_SIMULATOR
    return NO;
#endif
    if ([ServicesManager sharedInstance].myUser.isGuest){
        if(!self.invitationInformations)
            return NO;
        else
            return YES;
    }
    
    if(self.delegate.errorDuringWizardSteps)
        return NO;
    BOOL hasAlreadyDisplayNotificationScreen = [[NSUserDefaults standardUserDefaults] boolForKey:kRemotePushNotificationsWizardPageSeen];
    if(hasAlreadyDisplayNotificationScreen){
        NSLog(@"Notifications are already registered, skip this screen");
        return NO;
    }
    return YES;
}
@end
