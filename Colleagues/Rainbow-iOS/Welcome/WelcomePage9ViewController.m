/*
 * Rainbow
 *
 * Copyright (c) 2017, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import <UIKit/UIKit.h>
#import "WelcomePageCommonViewController.h"
#import "UITools.h"
#import <Rainbow/ServicesManager.h>
#import <Rainbow/defines.h>
#import "GoogleAnalytics.h"

@interface WelcomePage9ViewController : WelcomePageCommonViewController
@end

@interface WelcomePage9ViewController ()
@property (weak, nonatomic) IBOutlet UILabel *presentationLabel2;
@property (weak, nonatomic) IBOutlet UILabel *bottomLabel;
@end

@implementation WelcomePage9ViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    [UITools applyCustomFontTo:_presentationLabel2];
    
    self.presentationLabel.text = NSLocalizedString(@"Report usage statistics", nil);
    _presentationLabel2.text = NSLocalizedString(@"Rainbow request your approval to enable sending anonymous statistics about feature usages in the application", nil);
    [UITools applyThinCustomFontTo:_bottomLabel];
    _bottomLabel.text = NSLocalizedString(@"This settings can be changed at any time in Rainbow settings", nil);
    self.continueButton.enabled = YES;
    self.continueButton.alpha = 1.0f;
}

- (IBAction)continueButtonTapped:(UIButton *)sender {
    if(![GoogleAnalytics sharedInstance].authorizationGranted && [GoogleAnalytics sharedInstance].authorizationAlreadyRequested){
        [self.delegate nextPage];
    } else {
        NSString *message = NSLocalizedString(@"Do you allow Rainbow to report anonymized usage statistics ?", nil);
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Notifications", nil) message:message preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"No", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
            [GoogleAnalytics sharedInstance].authorizationAlreadyRequested = YES;
            [GoogleAnalytics sharedInstance].authorizationGranted = NO;
            _presentationLabel2.text = NSLocalizedString(@"We will not send statistics", nil);
            _bottomLabel.textColor = [UIColor redColor];
            _bottomLabel.font = [UIFont fontWithName:[UITools defaultFontName] size:15];
        }]];
        [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Yes", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
            [GoogleAnalytics sharedInstance].authorizationAlreadyRequested = YES;
            [GoogleAnalytics sharedInstance].authorizationGranted = YES;
            [self.delegate nextPage];
        }]];
        [self presentViewController:alert animated:YES completion:nil];
    }
}

- (void)didTapInBackground:(UITapGestureRecognizer *)sender {
    // Nothing to do
}
// should display analytics authorisation view
-(BOOL) shouldDisplay {
    if(self.delegate.errorDuringWizardSteps)
        return NO;
    if (self.isGuestMode || [ServicesManager sharedInstance].myUser.isGuest)
        return NO;
    if([GoogleAnalytics sharedInstance].authorizationAlreadyRequested){
        NSLog(@"Google Analytics access already determined, skip this screen");
        return NO;
    }
    
    return YES;
}

@end
