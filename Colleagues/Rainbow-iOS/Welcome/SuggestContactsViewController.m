/*
 * Rainbow
 *
 * Copyright (c) 2018, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE (i love beer) International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import <UIKit/UIKit.h>
#import "UIRainbowGenericTableViewCell.h"
#import "SuggestContactsViewController.h"
#import "UITools.h"
#import <Rainbow/ServicesManager.h>
#import <Rainbow/Contact.h>
#import "FilteredSortedSectionedArray.h"
#import "Contact+Extensions.h"
#import <Rainbow/RainbowUserDefaults.h>

@interface UIContactsSuggestionsTableViewCell : UITableViewCell
@property (strong, nonatomic) Contact *contact;
@property (strong, nonatomic) IBOutlet UIAvatarView *avatarView;
@property (weak, nonatomic) IBOutlet UILabel *mainLabel;
@property (weak, nonatomic) IBOutlet UILabel *subLabel;
@property (weak, nonatomic) IBOutlet UIButton *rightButton;
@property (nonatomic,strong) RightButtonTapHandler cellButtonTapHandler;
@end

@implementation UIContactsSuggestionsTableViewCell
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [UITools applyCustomFontTo: _mainLabel];
    [UITools applyCustomFontTo: _subLabel];
    [_rightButton setTintColor:[UITools defaultTintColor]];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didUpdateContact:) name:kContactsManagerServiceDidUpdateContact object:nil];
}

-(void) dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kContactsManagerServiceDidUpdateContact object:nil];
}

-(void)setContact:(Contact *)contact {
    _contact = contact;
    [self updateRightButtonIcon:_contact.isInRoster];
}

- (IBAction)didTapRightButton:(UIButton *)sender {
    if(_cellButtonTapHandler) {
        _cellButtonTapHandler(sender);
    }
}

-(void) didUpdateContact: (NSNotification *) notification {
    if(![[NSThread currentThread] isEqual:[NSThread mainThread]]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didUpdateContact:notification];
        });
        return;
    }
    
    NSDictionary *userInfo = notification.object;
    Contact *contact = [userInfo objectForKey:kContactKey];
    if ([contact.rainbowID isEqual:_contact.rainbowID]) {
        [self updateRightButtonIcon:contact.isInRoster];
    }
}

-(void)updateRightButtonIcon:(BOOL)inRoster {
    if(inRoster) {
        [_rightButton setImage:[UIImage imageNamed:@"Checkmark_status"] forState:UIControlStateNormal];
        [_rightButton setImage:[UIImage imageNamed:@"Checkmark_status"] forState:UIControlStateHighlighted];
        _rightButton.tintColor = [UITools colorFromHexa:0x34B233FF];
    } else {
        [_rightButton setImage:[UIImage imageNamed:@"Add"] forState:UIControlStateNormal];
        [_rightButton setImage:[UIImage imageNamed:@"Add"] forState:UIControlStateHighlighted];
        _rightButton.tintColor = [UITools defaultTintColor];
    }
}

-(void) prepareForReuse {
    [super prepareForReuse];
    [self updateRightButtonIcon:NO];
}
@end

@interface SuggestContactsViewController () <UITableViewDelegate, UITableViewDataSource>
@property (strong, nonatomic) NSMutableArray<Contact *> *contactsToInvite;
@property (weak, nonatomic) MBProgressHUD *hud;
@property (nonatomic) BOOL dismissViewWhenFinished;
@property (nonatomic) BOOL canSkipSuggestions;
@end

@implementation SuggestContactsViewController

-(instancetype) initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateSuggestions:) name:@"suggestionsToPresent" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didFailedInviteContact:) name:kContactsManagerServiceDidFailedToInviteContact object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didInviteContact:) name:kContactsManagerServiceDidInviteContact object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didUpdateContact:) name:kContactsManagerServiceDidUpdateContact object:nil];
    }
    
    return self;
}

-(void) dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"suggestionsToPresent" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kContactsManagerServiceDidFailedToInviteContact object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kContactsManagerServiceDidInviteContact object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kContactsManagerServiceDidUpdateContact object:nil];
    [_contactsToInvite removeAllObjects];
    _contactsToInvite = nil;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [UITools applyCustomFontTo:_presentationLabel];
    [UITools applyCustomFontTo:_presentationLabel2];
    
    self.tableView.rowHeight = kRainbowGenericTableViewCellHeight;
    self.title = NSLocalizedString(@"Add contacts", nil);
    [_skipButton setTitle:NSLocalizedString(@"Skip", nil)];
    [_addAllButton setTitle:NSLocalizedString(@"Add all", nil) forState:UIControlStateNormal];
    
    [self updateLabels];
    [_presentationLabel2 setText:NSLocalizedString(@"Add, then communicate with your colleagues", nil)];
    
    // A little trick for removing the cell separators
    self.tableView.tableFooterView = [UIView new];
    self.tableView.sectionIndexColor = [UITools defaultTintColor];
    self.tableView.backgroundColor = [UITools defaultBackgroundColor];
    
//    [self.tableView registerNib:[UINib nibWithNibName:@"UIRainbowGenericTableViewCell" bundle:nil] forCellReuseIdentifier:kTableViewReusableKey];

    _contactsToInvite = [NSMutableArray new];
    _dismissViewWhenFinished = NO;
    _canSkipSuggestions = YES;
}

-(void)viewDidAppear:(BOOL)animated {
    if(_suggestedContactsArray && [_suggestedContactsArray count] > 0){
        [self.tableView reloadData];
    }
}

-(void)updateSuggestions:(NSNotification*) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self updateSuggestions:notification];
        });
        return;
    }
    
    NSArray<Contact*> * suggestions = (NSArray<Contact*> *)notification.object;
    _suggestedContactsArray = nil;
    _suggestedContactsArray = [NSMutableArray arrayWithArray:suggestions];
    _canSkipSuggestions = YES;
  
    if([self isViewLoaded]) {
        [self.tableView reloadData];
        [self updateLabels];
    }
}

-(void)updateLabels {
    if([_suggestedContactsArray count] > 1) {
        [_addAllButton setHidden:NO];
        [_presentationLabel setText:[NSString stringWithFormat:NSLocalizedString(@"We have found %d contacts in your company for you to add", nil), [_suggestedContactsArray count]]];
    } else {
        [_addAllButton setHidden:YES];
        [_presentationLabel setText:NSLocalizedString(@"We have found 1 contact in your company for you to add", nil)];
    }
    if(_canSkipSuggestions)
        [_skipButton setTitle:NSLocalizedString(@"Skip", nil)];
    else
        [_skipButton setTitle:NSLocalizedString(@"Ok", nil)];
}

- (IBAction)didTapAddAllButton:(UIButton *)sender {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Add all contacts", nil) message:NSLocalizedString(@"All contacts will be added to your network.", nil) preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style:UIAlertActionStyleDefault handler:nil]];
    [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        [self inviteAll];
    }]];
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (IBAction)didTapUIBarButton:(UIBarButtonItem *)sender {
    //
    if([sender isEqual:_cancelButton]) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    //
    else if([sender isEqual:_skipButton]) {
        if(_canSkipSuggestions) {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Skip adding contacts", nil) message:NSLocalizedString(@"Don’t ask me to add contacts the next time I will log in.", nil) preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style:UIAlertActionStyleDefault handler:nil]];
            [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                [[RainbowUserDefaults sharedInstance] setBool:YES forKey:@"suggestContactSkipped"];
                [self dismissViewControllerAnimated:YES completion:nil];
            }]];
            
            [self presentViewController:alert animated:YES completion:nil];
        }
        
        else {
            [self dismissViewControllerAnimated:YES completion:nil];
        }
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    Contact *contact = [_suggestedContactsArray objectAtIndex:indexPath.row];

    UIContactsSuggestionsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"UIContactsSuggestionsTableViewCell"];
    cell.avatarView.peer = contact;
    cell.avatarView.asCircle = YES;
    cell.avatarView.showPresence = NO;
    cell.mainLabel.text = contact.displayBestInfo;
    cell.subLabel.text = contact.jobTitle;
    cell.contact = contact;
    
    __weak typeof(cell) weakCell = cell;
    cell.cellButtonTapHandler = ^(UIButton *sender) {
        Contact *contactToInvite = weakCell.contact;
        if(!contactToInvite.isInRoster)
            [self inviteContact:contactToInvite];
    };
    
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_suggestedContactsArray count];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - Send invitations

// Add a contact to the list of contacts to invite
-(void)inviteContact:(Contact *)contact {
    self.hud = [self showHUDWithMessage:NSLocalizedString(@"Sending invitation ...", nil) mode:MBProgressHUDModeIndeterminate dismissCompletionBlock:^{}];
    [self.hud show:YES];
    _canSkipSuggestions = NO;
    [self updateLabels];
    
    @synchronized (_contactsToInvite) {
        if(![_contactsToInvite containsObject:contact]) {
            [_contactsToInvite addObject:contact];
            [self inviteRemainingContacts];
        }
    }
}

// Add all contacts to the list
// NO API FOR INVITATATIONS BY BULK WITH RAINBOWID
//
-(void)inviteAll {
    if([_suggestedContactsArray count] > 0) {
        self.hud = [self showHUDWithMessage:NSLocalizedString(@"Sending invitation ...", nil) mode:MBProgressHUDModeIndeterminate dismissCompletionBlock:^{}];
        [self.hud show:YES];
        
        @synchronized (_contactsToInvite) {
            _dismissViewWhenFinished = YES;
            
            // Create list of contacts to invite
            [_contactsToInvite removeAllObjects];
            _contactsToInvite = [NSMutableArray arrayWithArray:_suggestedContactsArray];
            // Loop through all added contacts to list, one by one.
            [self inviteRemainingContacts];
        }
    }
}

// Check if the contacts to invite has elements to invite
-(void)inviteRemainingContacts {
    @synchronized (_contactsToInvite) {
        if([_contactsToInvite count] > 0) {
            Contact *contactToInvite = [_contactsToInvite objectAtIndex:0];
            
            if(!contactToInvite.isInRoster) {
                [[ServicesManager sharedInstance].contactsManagerService inviteContact:contactToInvite];
                [_contactsToInvite removeObjectAtIndex:0];
            } else {
                [_contactsToInvite removeObjectAtIndex:0];
                [self inviteRemainingContacts];
            }
            
        } else {
            [self hideHUD];
            
            if(_dismissViewWhenFinished) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    self.hud = [self showHUDWithMessage:NSLocalizedString(@"All contacts have been added", nil) mode:MBProgressHUDModeCustomView dismissCompletionBlock:^{
                        [self dismissViewControllerAnimated:NO completion:nil];
                    }];
                });
            }
        }
    }
}

-(MBProgressHUD *) showHUDWithMessage:(NSString *) message mode:(MBProgressHUDMode) mode dismissCompletionBlock:(void (^)()) dismissCompletion {
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = mode;
    hud.removeFromSuperViewOnHide = YES;
    if( message ) {
        hud.labelText = message;
    }
    [hud setLabelFont:[UIFont fontWithName:[UITools defaultFontName] size:14.0f]];
    if(mode == MBProgressHUDModeCustomView){
        hud.mode = MBProgressHUDModeCustomView;
        hud.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Checkmark"]];
        [hud showAnimated:YES whileExecutingBlock:^{sleep(1);} completionBlock:dismissCompletion];
    }
    return hud;
}

-(void) hideHUD {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self hideHUD];
        });
        return;
    }
    
    if(self.hud) {
        [self.hud hide:YES];
        self.hud = nil;
    }
}

#pragma mark - Notifications for invitation result
-(void)didInviteContact:(NSNotification*) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didInviteContact:notification];
        });
        return;
    }
    
    //
    Contact *contactInRoster = (Contact*) notification.object;
    [self updateSuggestedContact:contactInRoster];
    
    NSLog(@"didInviteContact");
    [self.tableView reloadData];
    [self inviteRemainingContacts];
 }

-(void)didFailedInviteContact:(NSNotification*) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didFailedInviteContact:notification];
        });
        return;
    }
    
    NSLog(@"didFailedInviteContact");
    [self.tableView reloadData];
    [self inviteRemainingContacts];
}

-(void) didUpdateContact: (NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didUpdateContact:notification];
        });
        return;
    }
    
    NSDictionary *userInfo = notification.object;
    NSArray<NSString *> *changedKeys = [userInfo objectForKey:kChangedAttributesKey];

    if([changedKeys containsObject:@"isInRoster"]) {
        if([self isViewLoaded])
            [self.tableView reloadData];
    }
}

-(void)updateSuggestedContact:(Contact *)contactToUpdate {
    @synchronized(_suggestedContactsArray) {
        [[_suggestedContactsArray copy] enumerateObjectsWithOptions:NSEnumerationReverse usingBlock:^(Contact * suggestedContact, NSUInteger idx, BOOL * stop) {
            if([suggestedContact.rainbowID isEqual:contactToUpdate.rainbowID]) {
                // Add the newly added to roster contact to the list (cache, etc...)
                [[ServicesManager sharedInstance].contactsManagerService createOrUpdateRainbowContactFromSuggested:contactToUpdate];
                // Update the temporary suggested list with this contact object
                [_suggestedContactsArray setObject:contactToUpdate atIndexedSubscript:idx];
                
                *stop = YES;
            }
        }];
    }
}

@end
