/*
 * Rainbow
 *
 * Copyright (c) 2017, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "UITools.h"
#import "UIImageView+Letters.h"
#import "UICallLogsDetailsCell.h"
#import <Rainbow/ServicesManager.h>
#import <Rainbow/CallLog.h>


@interface UICallLogsDetailsCell ()
@property (weak, nonatomic) IBOutlet UIImageView *callTypeIcon;
@property (weak, nonatomic) IBOutlet UILabel *callTypeLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *durationLabel;
@end

@implementation UICallLogsDetailsCell

-(void) awakeFromNib {
    [super awakeFromNib];
    
    [self setTintColor:[UITools defaultTintColor]];
    self.contentView.backgroundColor = [UITools defaultBackgroundColor];
    self.backgroundColor = [UITools defaultBackgroundColor];
}

-(void) prepareForReuse {
    [super prepareForReuse];
    _callLog = nil;
}

-(void) setCallLog:(CallLog *)callLog {
    _callLog = callLog;
    
    
    NSString *label;
    UIImage *icon;
    
    if( callLog.service == CallLogServiceConference ) {
        icon = [UIImage imageNamed:@"conference"];
        label = NSLocalizedString(@"Conference", nil);
        _callTypeIcon.tintColor = [UITools defaultTintColor];
    } else if( callLog.state == CallLogStateMissed && !callLog.isOutgoing) {
        icon = (callLog.type == CallLogTypeWebRTC) ? [UIImage imageNamed:@"incomingMissedCall"] : [UIImage imageNamed:@"incomingMissedCallPBX"];
        label = NSLocalizedString(@"Missed call", nil);
        _callTypeIcon.tintColor = [UIColor redColor];
    } else if( callLog.state == CallLogStateMissed && callLog.isOutgoing) {
        icon = (callLog.type == CallLogTypeWebRTC) ? [UIImage imageNamed:@"outgoingMissedCall"] : [UIImage imageNamed:@"outgoingMissedCallPBX"];
        label = NSLocalizedString(@"Canceled call", nil);
        _callTypeIcon.tintColor = [UIColor redColor];
    } else if( callLog.state == CallLogStateAnswered && !callLog.isOutgoing) {
        icon = (callLog.type == CallLogTypeWebRTC) ? [UIImage imageNamed:@"incomingCall"] : [UIImage imageNamed:@"incomingCallPBX"];
        label = NSLocalizedString(@"Incoming call", nil);
        _callTypeIcon.tintColor = [UITools defaultTintColor];
    } else {
        icon = (callLog.type == CallLogTypeWebRTC) ? [UIImage imageNamed:@"outgoingCall"] : [UIImage imageNamed:@"outgoingCallPBX"];
        label = NSLocalizedString(@"Outgoing call", nil);
        _callTypeIcon.tintColor = [UITools defaultTintColor];
    }
    
    _callTypeLabel.text = label;
    _callTypeIcon.image = icon;
    _dateLabel.text = [UITools optimizedFormatForDate:callLog.date];
    
    if(![callLog.duration isEqual: @"0"]) {
        _durationLabel.text = [UITools timeFormatConvertToSeconds:callLog.duration zeroFormattingBehaviorunitsStyle:NSDateComponentsFormatterZeroFormattingBehaviorPad unitsStyle:NSDateComponentsFormatterUnitsStyleShort];
    } else {
        _durationLabel.text = @"";
    }
}
        
@end
