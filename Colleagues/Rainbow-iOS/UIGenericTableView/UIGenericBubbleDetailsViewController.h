/*
 * Rainbow
 *
 * Copyright (c) 2017, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import <UIKit/UIKit.h>
#import <Rainbow/ServicesManager.h>

@interface UIGenericBubbleDetailsViewController : UIViewController<UITextFieldDelegate,UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) Room *room;
@property (nonatomic, strong) NSString *roomName;
@property (nonatomic, strong) NSString *roomTopic;
@property (nonatomic, strong) NSArray<Contact*> *roomParticipants;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic) BOOL meeting;
-(void) textFieldDidChange:(UITextField *) textField;
@end
