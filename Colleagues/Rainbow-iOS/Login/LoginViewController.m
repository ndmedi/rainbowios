/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "WelcomePageCommonViewController.h"
#import "WelcomePageProtocol.h"
#import "LoginViewController.h"
#import <Rainbow/ServicesManager.h>
#import <Rainbow/defines.h>
#import "MBProgressHUD.h"
#import "UITools.h"
#import <Rainbow/Tools.h>
#import <Rainbow/LogsRecorder.h>
#import <MessageUI/MessageUI.h>
#import "UIDevice-Hardware.h"
#import <Rainbow/UIDevice+VersionCheck.h>
#import "MFMailComposeViewController+StatusBarStyle.h"
#import "debuggerCheck.h"
#import "UIPageViewController+StatusBarStyle.h"
#import "UIStoryboardManager.h"
#import "ACFloatingTextField.h"
#import "SuggestContactsViewController.h"
#import "SamlLoginViewController.h"

static NSString *CellIdentifier = @"loginCell";

@interface LoginViewController () <UITextFieldDelegate, WelcomePageProtocol, MFMailComposeViewControllerDelegate>
@property (nonatomic, weak) IBOutlet UIImageView *logo;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *logoViewCenterConstraint;
@property (nonatomic, weak) IBOutlet UIView *loginView;
@property (nonatomic, strong) UIPageViewController *pageViewCtrl;
@property (nonatomic) NSInteger pageNb;
@property (nonatomic, weak) IBOutlet UIButton *loginButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *loginButtonConstraint;
@property (nonatomic, strong) ServicesManager *servicesManager;
@property (nonatomic, strong) LoginManager *loginManager;
@property (nonatomic, weak) IBOutlet UIButton *cancelWizardButton;
@property (weak, nonatomic) IBOutlet UILabel *integrationLabel;
@property (nonatomic, strong) UITabBarController *mainScreenTabBarCtrl;
@property (nonatomic, weak) IBOutlet ACFloatingTextField *usernameTextField;
@property (nonatomic, weak) IBOutlet ACFloatingTextField *passwordTextField;
@property (nonatomic, strong) MBProgressHUD *hud;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *loginViewCenterConstraint;
@property (weak, nonatomic) IBOutlet UIButton *createAnAccountButton;
@property (nonatomic, strong) MyUser *myUser;
@property (nonatomic, strong) NSURL *applicationLogUrl;
@property (weak, nonatomic) IBOutlet UIButton *lostPasswordButton;
@property (weak, nonatomic) IBOutlet UIView *bottomView;
@property (nonatomic) BOOL lostPasswordTapped;
@property (nonatomic) BOOL guestLogin;
@property (nonatomic, strong) NSDictionary *invitationInformations;
@property (nonatomic, strong) NSError *errorDuringWizard;
@property (nonatomic, strong) NSString *loginType;
@end

@implementation LoginViewController {
    CGFloat keyboardLoginButtonDistance;
    CGFloat keyboardOverlapValue;
}

-(void) awakeFromNib {
    [super awakeFromNib];
    _servicesManager = [ServicesManager sharedInstance];
    _loginManager = _servicesManager.loginManager;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didLogin:) name:kLoginManagerDidLoginSucceeded object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didLogout:) name:kLoginManagerDidLogoutSucceeded object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didFailedToAuthenticate:) name:kLoginManagerDidFailedToAuthenticate object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didChangeServer:) name:kLoginManagerDidChangeServer object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didChangerUser:) name:kLoginManagerDidChangeUser object:nil];
    [[NSNotificationCenter defaultCenter] addObserverForName:NSUserDefaultsDidChangeNotification object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *note) {
        [self readEMMValues];
    }];
    _pageNb = 6;
}

- (void)readEMMValues {
    // Retreive key value dictionnary from EMM
    NSDictionary *EMMConfig = [[NSUserDefaults standardUserDefaults] dictionaryForKey:@"com.apple.configuration.managed"];
    // Set login and password
    if (EMMConfig) {
        _usernameTextField.text = EMMConfig[@"login"];
        _passwordTextField.text = EMMConfig[@"password"];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self readEMMValues];
    self.view.backgroundColor = [UITools defaultBackgroundColor];
    _loginView.alpha = 0;
    _createAnAccountButton.alpha = 0;
    _cancelWizardButton.alpha = 0;
    
    [UITools applyCustomFontTo:_loginButton.titleLabel];
    [UITools applyCustomFontTo:_createAnAccountButton.titleLabel];
    [UITools applyCustomFontTo:_cancelWizardButton.titleLabel];
    
    _usernameTextField.placeholder = NSLocalizedString(@"Email address", nil);
    _passwordTextField.placeholder = NSLocalizedString(@"Password", nil);
    _passwordTextField.hidden = YES;
    
    [_loginButton setTitle:[NSLocalizedString(@"Next", nil) uppercaseString] forState:UIControlStateNormal];
    _loginButton.backgroundColor = [UITools defaultTintColor];
    _loginButton.layer.cornerRadius = 8.0f;
    [_lostPasswordButton setTitle:NSLocalizedString(@"Lost your password ?", nil) forState:UIControlStateNormal];
    _lostPasswordButton.tintColor = [UITools defaultTintColor];
    _lostPasswordButton.hidden = YES;
    [UITools applyCustomFontTo:_lostPasswordButton.titleLabel];
    
    [_createAnAccountButton setTitle:NSLocalizedString(@"Create account", nil) forState:UIControlStateNormal];
    [_cancelWizardButton setTitle:NSLocalizedString(@"Already have an account ?", nil) forState:UIControlStateNormal];
    [_createAnAccountButton setTitleColor:[UITools defaultTintColor] forState:UIControlStateNormal];
    [_createAnAccountButton setTintColor:[UITools defaultTintColor]];
    [_cancelWizardButton setTitleColor:[UITools defaultTintColor] forState:UIControlStateNormal];
    
    _loginButton.enabled = YES;
    _loginButton.alpha = 1.0;
    [UITools applyCustomFontTo:_integrationLabel];
    _myUser = _servicesManager.myUser;
    _integrationLabel.hidden = YES;
    
    _lostPasswordTapped = NO;
    
    _loginView.alpha = 1.0;
    _createAnAccountButton.alpha = 1.0;
    [self displayIntegrationLabel];
    
    if([[[UIDevice currentDevice] model] hasPrefix:@"iPad"]){
        _logo.transform = CGAffineTransformMakeScale(0.7, 0.7);
    }
}

-(void) dealloc {
    _myUser = nil;
    _servicesManager = nil;
    _loginManager = nil;
    _pageViewCtrl = nil;
    _invitationInformations = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidLoginSucceeded object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidLogoutSucceeded object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidFailedToAuthenticate object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidChangeServer object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NSUserDefaultsDidChangeNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidChangeUser object:nil];
}

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    _logo.hidden = NO;
    _usernameTextField.borderStyle = UITextBorderStyleNone;
    _passwordTextField.borderStyle = UITextBorderStyleNone;
    
    _usernameTextField.lineColor = [UITools foregroundGrayColor];
    _usernameTextField.selectedLineColor = [UITools defaultTintColor];
    _usernameTextField.placeHolderColor = [UITools foregroundGrayColor];
    _usernameTextField.selectedPlaceHolderColor = [UITools defaultTintColor];
    
    _passwordTextField.lineColor = [UITools foregroundGrayColor];
    _passwordTextField.selectedLineColor = [UITools defaultTintColor];
    _passwordTextField.placeHolderColor = [UITools foregroundGrayColor];
    _passwordTextField.selectedPlaceHolderColor = [UITools defaultTintColor];
    
    if(_servicesManager.myUser.username)
        _usernameTextField.text = _servicesManager.myUser.username;
    if(_servicesManager.myUser.password)
        _passwordTextField.text = _servicesManager.myUser.password;
}

-(void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    _loginView.hidden = NO;
}

-(void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.view.layer removeAllAnimations];
    _loginView.alpha = 0;
    _loginView.hidden = YES;
    _logo.hidden = YES;
    [self checkLoginButtonState];
}

-(void) viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    _loginType = nil;
    [self hideLoginViewWhilePresentingWelcomePage:NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void) didChangeServer:(NSNotification *) notification {
    if(![[NSThread currentThread] isEqual:[NSThread mainThread]]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didChangeServer:notification];
        });
        return;
    }
    
    _logo.image = [UIImage imageNamed:@"Logo_rainbow"];
    _loginButton.backgroundColor = [UITools defaultTintColor];
    
    [self displayIntegrationLabel];
    if(_servicesManager.myUser.username)
        _usernameTextField.text = _servicesManager.myUser.username;
    if(_servicesManager.myUser.password)
        _passwordTextField.text = _servicesManager.myUser.password;
    [self checkLoginButtonState];
}

-(void) didChangerUser:(NSNotification *) notification {
    if(![[NSThread currentThread] isEqual:[NSThread mainThread]]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didChangerUser:notification];
        });
        return;
    }
    if(_servicesManager.myUser.username)
        _usernameTextField.text = _servicesManager.myUser.username;
    if(_servicesManager.myUser.password)
        _passwordTextField.text = _servicesManager.myUser.password;
    [self checkLoginButtonState];
}

#pragma mark - Rotation
-(BOOL) shouldAutorotate {
    return NO;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return UIInterfaceOrientationPortrait;
}

-(UIStatusBarStyle) preferredStatusBarStyle {
    return UIStatusBarStyleDefault;
}

#pragma mark - login

- (IBAction)loginButtonClicked:(UIButton *)sender {
    
    NSString * username = _usernameTextField.text;
    NSString * password = _passwordTextField.hidden ? @"" : _passwordTextField.text;
    [_loginManager setUsername:username andPassword:password];
    
    if (!_loginType.length) {
        
        [_loginManager getUserAuthenticationURLs:_usernameTextField.text completionHandler:^(NSString *type, NSURL *loginUrl, NSDictionary *headersParameters, NSError *error) {
            if (!error) {
                _loginType = type;
                if ([type isEqualToString:@"RAINBOW"]) {
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        _passwordTextField.hidden = NO;
                        _lostPasswordButton.hidden = NO;
                        [_passwordTextField becomeFirstResponder];
                        [_loginButton setTitle:[NSLocalizedString(@"Sign in", nil) uppercaseString] forState:UIControlStateNormal];
                        [self checkLoginButtonState];
                        return ;
                    });
                }
                
                if ([type isEqualToString:@"SAML"]) {
                    
                    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:loginUrl];
                    [request setAllHTTPHeaderFields:headersParameters];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        SamlLoginViewController *samlLoginViewController = [[SamlLoginViewController alloc] initWithURLRequest:request];
                        [self presentViewController:samlLoginViewController animated:YES completion:nil];
                        
                        samlLoginViewController.isLoggedWithSAMLBlock = ^(BOOL isLogged, int errorCode) {
                            if (isLogged) {
                                [self showConnectingHUD];
                            } else {
                                if(errorCode > 0) {
                                    [UITools showErrorPopupWithTitle:NSLocalizedString(@"Error while login", nil) message:NSLocalizedString(@"Internal server error", nil) inViewController:self];
                                }
                            }
                        };
                    });
                }
            } else {
                _loginType = nil;
                NSString *errorMessage = NSLocalizedString(error.code == 404 ? @"User does not exist." : @"An error occurred while connecting to the server.", nil);
                [UITools showErrorPopupWithTitle:NSLocalizedString(@"Error while login", nil) message:errorMessage inViewController:self];
            }
        }];
    } else {
        [self showConnectingHUD];
        [_loginManager connect];
    }
}

-(void) showConnectingHUD {
    [_usernameTextField resignFirstResponder];
    [_passwordTextField resignFirstResponder];
    [self.view setUserInteractionEnabled:NO];
    
    _hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    _hud.mode = MBProgressHUDModeIndeterminate;
    _hud.labelText = NSLocalizedString(@"Connecting", nil);
    _hud.removeFromSuperViewOnHide = NO;
    [_hud setLabelFont:[UIFont fontWithName:[UITools defaultFontName] size:14.0f]];
}

-(void) didLogin:(NSNotification *) notification {
    if(![[NSThread currentThread] isEqual:[NSThread mainThread]]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didLogin:notification];
        });
        return;
    }
    _lostPasswordTapped = NO;
    [self.view setUserInteractionEnabled:YES];
    [_hud hide:NO];
}

-(void) didLogout:(NSNotification *) notification {

}

-(void) didFailedToAuthenticate:(NSNotification *) notification {
    if(![[NSThread currentThread] isEqual:[NSThread mainThread]]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didFailedToAuthenticate:notification];
        });
        return;
    }
    
    [self.view setUserInteractionEnabled:YES];
    [_hud hide:YES];
}

- (IBAction)cancelWizardTapped:(UIButton *)sender {
    [UIView animateWithDuration:0.5 delay:0.2 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
        _pageViewCtrl.view.alpha = 0.0f;
        [self hideLoginViewWhilePresentingWelcomePage:NO];
    } completion:^(BOOL finished) {
        [_pageViewCtrl.view removeFromSuperview];
        [_pageViewCtrl removeFromParentViewController];
        _pageViewCtrl = nil;
    }];
    if(_loginManager.isConnected){
        [self dismissViewControllerAnimated:YES completion:nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"loginViewDismissed" object:nil];
    }
    
    _lostPasswordTapped = NO;
    [self checkLoginButtonState];
}

#pragma mark - Page controller

- (WelcomePageCommonViewController *)viewControllerAtIndex:(NSUInteger)index loadPage:(BOOL) loadPage {
    WelcomePageCommonViewController *viewCtrl = [self.storyboard instantiateViewControllerWithIdentifier:[NSString stringWithFormat:@"page%ldViewID", (long)index]];
    viewCtrl.delegate = self;
    viewCtrl.lostPasswordMode = _lostPasswordTapped;
    viewCtrl.invitationInformations = _invitationInformations;
    viewCtrl.isGuestMode = _guestLogin;
    viewCtrl.pageTag = index;
    return viewCtrl;
}

#pragma mark - WelcomePage protocol
-(void) nextPage {
    WelcomePageCommonViewController *currentViewController = (WelcomePageCommonViewController *)[_pageViewCtrl.viewControllers objectAtIndex:0];
    currentViewController.invitationInformations = nil;
    
    NSInteger index = currentViewController.pageTag +1;
    if(index < _pageNb) {
        // Skip avatar setup for guest user
        if (_guestLogin && !_guestAccountFinalizationNeeded && index == 3) {
            index++;
        }
        WelcomePageCommonViewController *viewCtrl = [self viewControllerAtIndex:index loadPage:YES];
        if([viewCtrl shouldDisplay]){
            [_pageViewCtrl setViewControllers:@[viewCtrl] direction:UIPageViewControllerNavigationDirectionForward animated:YES completion:nil];
        } else {
            [_pageViewCtrl setViewControllers:@[viewCtrl] direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
            [self nextPage];
        }
    }
    else {
        // Done
        NSLog(@"Nothing more to display exit wizard");
        [self cancelWizard];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kWizardCompleted];
        [[NSNotificationCenter defaultCenter] postNotificationName:kWizardCompleted object:nil];
    }
}

-(void) cancelWizard {
    [self cancelWizardTapped:_cancelWizardButton];
}

-(void) gotAnErrorDuringWizardSteps:(NSError *)error {
    _errorDuringWizard = error;
}

-(NSError *) errorDuringWizardSteps {
    return _errorDuringWizard;
}

-(void) resetInvitationInformations {
    _invitationInformations = nil;
}

#pragma mark - TextField delegate
-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    if(textField == _usernameTextField){
        if(_passwordTextField.text.length == 0)
            [_passwordTextField becomeFirstResponder];
        else
            [self loginButtonClicked:nil];
    } else {
        if (_usernameTextField.text.length == 0)
            [_usernameTextField becomeFirstResponder];
        else
            [self loginButtonClicked:nil];
    }
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    if(textField == _usernameTextField)
        textField.keyboardType = UIKeyboardTypeEmailAddress;
    else
        textField.keyboardType = UIKeyboardTypeDefault;
    return YES;
}

-(void) textFieldDidEndEditing:(UITextField *)textField {
    [textField resignFirstResponder];
}

- (IBAction)textFieldValueChanged:(UITextField *)sender {
    if(_passwordTextField.text.length > 0 && _usernameTextField.text.length > 0)
        sender.returnKeyType = UIReturnKeyDone;
    else
        sender.returnKeyType = UIReturnKeyNext;
    [sender reloadInputViews];
}

-(IBAction) onTextChanged:(UITextField *) sender {
    [self checkLoginButtonState];
}

-(void) checkLoginButtonState {
    if(_usernameTextField.text.length > 0 && (_passwordTextField.text.length > 0 || _passwordTextField.hidden)){
        _loginButton.enabled = YES;
        _loginButton.alpha = 1.0;
    } else {
        _loginButton.enabled = NO;
        _loginButton.alpha = 0.5;
    }
}

-(void) displayIntegrationLabel {
    if(_myUser.server && !_myUser.server.defaultServer){
        _integrationLabel.text = [NSString stringWithFormat:@"Domain : %@", _myUser.server.serverDisplayedName];
        _integrationLabel.hidden = NO;
    } else {
        _integrationLabel.hidden = YES;
    }
}

-(void) keyboardWillShow:(NSNotification *) notification {
    // Calculate space between loginView bottom and login button.
    if(keyboardLoginButtonDistance == 0)
        keyboardLoginButtonDistance = _loginButton.frame.origin.y - (_loginView.frame.origin.y + _loginView.frame.size.height);
    
    [UIView animateWithDuration:[notification.userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue] delay:0 options:[[[notification userInfo] objectForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue] animations:^{
        // To manage the show/hide/show events when user tap on "next" key, the calculated value is defined at first time (when the keyboard is shown for the first time. Calculation at other frame positions may cause bad or difficulties for well positionning the view.
        if(keyboardOverlapValue == 0) {
            // Virtual frame encapsulating login view + button view
            // Height is current frame height + button view height + margin between both
            CGRect expectedViewFrame = CGRectMake(_loginView.frame.origin.x,
                                                  _loginView.frame.origin.y,
                                                  _loginView.frame.size.width,
                                                  _loginView.frame.size.height + _loginButton.frame.size.height + 20 + 40);
            
            // Calculate Keyboard/Expected frame intersection. Is this virtual frame hidden by the keyboard?
            CGRect intersec = [self calculateKeyboardIntersec:notification withViewFrame:expectedViewFrame];
            keyboardOverlapValue = intersec.size.height;
        }
        
        if(keyboardOverlapValue > 0) {
            _loginViewCenterConstraint.constant = -(keyboardOverlapValue -10);
            _logoViewCenterConstraint.constant = -(keyboardOverlapValue -10);
            // Remove the space between the login view and the login button
            _loginButtonConstraint.constant = -(keyboardOverlapValue -10 + keyboardLoginButtonDistance);
            [_logo setHidden:YES];
            [_loginView layoutIfNeeded];
        }
        
    } completion:^(BOOL finished) {
    }];
}

-(void) keyboardWillHide:(NSNotification *) notification {
    [UIView animateWithDuration:[notification.userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue] delay:0 options:[[[notification userInfo] objectForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue] animations:^{
        _loginViewCenterConstraint.constant = 20;
        _logoViewCenterConstraint.constant = 40;
        _loginButtonConstraint.constant = 0;
        [_logo setHidden:NO];
        [_loginView layoutIfNeeded];
    } completion:^(BOOL finished) {
    }];
}

-(CGRect) calculateKeyboardIntersec:(NSNotification *)notification withViewFrame:(CGRect)frame {
    CGRect keyboardRect = [[notification.userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGRect keyboardIntersection = CGRectZero;
    
    if( [notification.name isEqualToString:UIKeyboardWillShowNotification] ) {
        CGRect intersection = CGRectIntersection(frame, keyboardRect);
        
        if(!CGRectIsNull(intersection)) {
            keyboardIntersection = CGRectMake(0, 0, 0, intersection.size.height);
        }
    }
    
    return keyboardIntersection;
}

- (IBAction)didTapInBackgroundView:(UITapGestureRecognizer *)sender {
    if(_usernameTextField.isFirstResponder)
        [_usernameTextField resignFirstResponder];
    
    if(_passwordTextField.isFirstResponder)
        [_passwordTextField resignFirstResponder];
}

- (IBAction)createAccountTapped:(UIButton *)sender {
    if(_pageViewCtrl)
        _pageViewCtrl = nil;
    if(![self isViewLoaded]){
        // load the view :/
        [self view];
    }

    [self createWelcomePageControllerWithBottomOffset:80];
    
    [_pageViewCtrl setViewControllers:@[[self viewControllerAtIndex:0 loadPage:YES]] direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    
    [UIView animateWithDuration:0.5 delay:0.2 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
        _pageViewCtrl.view.alpha = 0.0;
        [self addChildViewController:_pageViewCtrl];
        [self.view addSubview:_pageViewCtrl.view];
        [_pageViewCtrl didMoveToParentViewController:self];
        _pageViewCtrl.view.alpha = 1.0;
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.2 animations:^{
            [self hideLoginViewWhilePresentingWelcomePage:YES];
        }];
    }];
}

-(void) showWelcomeWizard {
    
    if(_pageViewCtrl){
        [_pageViewCtrl.view removeFromSuperview];
        [_pageViewCtrl removeFromParentViewController];
        _pageViewCtrl = nil;
    }
    if(!_lostPasswordTapped && !_guestAccountFinalizationNeeded){
        [self createWelcomePageControllerWithBottomOffset:0];
        
        // Determine on which page we must start the wizard.
        NSInteger indexOfFirstPageToDisplay = [self indexOfNextWizardPageToDipslay];
        WelcomePageCommonViewController *viewCtrl = [self viewControllerAtIndex:indexOfFirstPageToDisplay loadPage:YES];
        [_pageViewCtrl setViewControllers:@[viewCtrl] direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    } else {
        NSInteger offset = ((CGRect)[[UIScreen mainScreen] bounds]).size.height - _bottomView.frame.origin.y;
        NSInteger index = 0;
        if (_guestAccountFinalizationNeeded) {
            offset = 0;
            index = 1;
            _guestLogin = NO;
            _guestAccountFinalizationNeeded = NO;
        }
        [self createWelcomePageControllerWithBottomOffset: offset];
        [_pageViewCtrl setViewControllers:@[[self viewControllerAtIndex:index loadPage:YES]] direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    }
    
    [UIView animateWithDuration:0.5 delay:0.2 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
        _pageViewCtrl.view.alpha = 0.0;
        [self addChildViewController:_pageViewCtrl];
        [self.view addSubview:_pageViewCtrl.view];
        [_pageViewCtrl didMoveToParentViewController:self];
        _pageViewCtrl.view.alpha = 1.0;
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.2 animations:^{
            [self hideLoginViewWhilePresentingWelcomePage:YES];
        }];
    }];
}

-(BOOL) shouldShowWelcomeWizard {
    return [self indexOfNextWizardPageToDipslay] != -1;
}

-(NSInteger) indexOfNextWizardPageToDipslay {
    NSInteger indexOfFirstPageToDisplay = -1;
    // We start counting at index 2, to jump the 2 pages used for creating accounts
    for (int i = 2; i<_pageNb; i++) {
        WelcomePageCommonViewController *viewCtrl = [self viewControllerAtIndex:i loadPage:NO];
        if([viewCtrl shouldDisplay]){
            indexOfFirstPageToDisplay = i;
            break;
        }
        viewCtrl = nil;
    }
    return indexOfFirstPageToDisplay;
}

-(void) hideLoginViewWhilePresentingWelcomePage:(BOOL) hide {
    _loginView.alpha = hide?0:1;
    _loginButton.alpha = hide?0:1;
    _createAnAccountButton.alpha = hide?0:1;
    _logo.alpha = hide?0:1;
    _cancelWizardButton.alpha = !hide?0:1;
}

-(void) createWelcomePageControllerWithBottomOffset:(NSInteger) offset {
    _pageViewCtrl = [[UIStoryboardManager sharedInstance].welcomePagesStoryBoard instantiateViewControllerWithIdentifier:@"WelcomePageID"];
    _pageViewCtrl.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height - offset);
}

- (BOOL)canBecomeFirstResponder {
    return YES;
}

- (void)motionEnded:(UIEventSubtype)motion withEvent:(UIEvent *)event {
    if(![[NSThread currentThread] isEqual:[NSThread mainThread]]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self motionEnded:motion withEvent:event];
        });
        return;
    }
    
    if (motion == UIEventSubtypeMotionShake){
        NSLog(@"SHAKE DETECTED");
        if(!_hud)
            _hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        
        _hud.mode = MBProgressHUDModeIndeterminate;
        [_hud showAnimated:YES whileExecutingBlock:^{
            dispatch_async(dispatch_get_main_queue(), ^{
                [self sendLogsByMail];
            });
        }];
    }
}

- (void) sendLogsByMail {
    // If no configure mail account
    if (![MFMailComposeViewController canSendMail]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Could not send mail", nil) message:NSLocalizedString(@"No configured mail account", nil) preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleCancel handler:nil];
            [alertController addAction:okAction];
            [self presentViewController:alertController animated:YES completion:nil];
        });
        return;
    }
    // Stop record before zipping to got the complete file
    [[LogsRecorder sharedInstance] stopRecord];
    _applicationLogUrl = [[LogsRecorder sharedInstance] zippedApplicationLogs];
    if (!_applicationLogUrl) {
        [[LogsRecorder sharedInstance] startRecord];
        dispatch_async(dispatch_get_main_queue(), ^{
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Could not send mail", nil) message:NSLocalizedString(@"No logs file found.", nil) preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleCancel handler:nil];
            [alertController addAction:okAction];
            [self presentViewController:alertController animated:YES completion:nil];
        });
        return;
    }
    // restat record after zipping
    if(AmIBeingDebugged() == FALSE)
        [[LogsRecorder sharedInstance] startRecord];
    
    NSError *dataError = nil;
    NSData *attachmentData = [NSData dataWithContentsOfFile:_applicationLogUrl.path options:0 error:&dataError];
    if (dataError || !attachmentData) {
        long chunkSize = 1024*1024;
        [UITools readDataForFilePath:_applicationLogUrl.path chunkSize:chunkSize completion:^(NSData *fileData) {
            if (fileData) {
                [self openMailBrowserWithLogsData:fileData];
            }
            else {
                NSLog(@"Something went wrong while try loading logs data!");
            }
        }];
    }
    else {
        [self openMailBrowserWithLogsData:attachmentData];
    }
}

-(void)openMailBrowserWithLogsData:(NSData *)attachmentData {
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd_HH:mm:ss"];
    NSString* date = [dateFormatter stringFromDate:[NSDate date]];
    
    NSString* bodyContent = [NSString stringWithFormat:@"\n\n\n%@\n------\nProduct : %@\nVersion: %@\n Date: %@\niOS version : %@\niPhone model : %@", NSLocalizedString(@"Do not modify anythings below this line", nil), [Tools applicationName], [Tools applicationVersion], date, [UIDevice currentDevice].systemVersion, [UIDevice currentDevice].modelName];
    NSString* attachedFileName = [NSString stringWithFormat:@"%@-%@-logs.txt.zip", [Tools applicationName], [Tools applicationVersion]];
    
    // Compute username
    NSString* userName = [ServicesManager sharedInstance].myUser.username;
    
    // Send logs
    MFMailComposeViewController *mailComposer = [[MFMailComposeViewController alloc] init];
    mailComposer.mailComposeDelegate = self;
    [mailComposer setModalPresentationStyle:UIModalPresentationFormSheet];
    [mailComposer setSubject:[NSString stringWithFormat:@"%@ %@ %@ application logs for user %@",[Tools applicationName], [Tools applicationVersion], [Tools currentOS], userName]];
    [mailComposer setMessageBody:bodyContent isHTML:NO];
    if (attachmentData)
        [mailComposer addAttachmentData:attachmentData mimeType:@"application/x-gzip" fileName:attachedFileName];
    mailComposer.navigationBar.tintColor = [UIColor whiteColor];
    mailComposer.navigationBar.translucent = NO;
    [self presentViewController:mailComposer animated:YES completion:^{
        [_hud hide:YES];
    }];
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
    if(_applicationLogUrl){
        [[NSFileManager defaultManager] removeItemAtURL:_applicationLogUrl error:nil];
        _applicationLogUrl = nil;
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)didTapLostPasswordButton:(UIButton *)sender {
    [_usernameTextField resignFirstResponder];
    [_passwordTextField resignFirstResponder];
    _lostPasswordTapped = YES;
    [self showWelcomeWizard];
}

-(void) showWelcomeWizardWithInfo:(NSDictionary *) userInfo {
    if(_pageViewCtrl){
        [_pageViewCtrl.view removeFromSuperview];
        [_pageViewCtrl removeFromParentViewController];
        _pageViewCtrl = nil;
    }
    
    if(![self isViewLoaded]){
        // load the view :/
        [self view];
    }
    
    _invitationInformations = userInfo;
    
    [self createWelcomePageControllerWithBottomOffset:0];
    
    // Determine on which page we must start the wizard.
    NSInteger indexOfFirstPageToDisplay = 1;
    
    // Bubble invitation or PGI meeting invitation
    if ([_invitationInformations[@"scenario"] isEqualToString:@"chat"] || [_invitationInformations[@"scenario"] isEqualToString:@"pstn-conference"]) {
        
        if (_invitationInformations[@"invitationID"]) {
            indexOfFirstPageToDisplay = 2;
            _guestLogin = YES;
            
            // We are in guest mode, we loggedi-in the user imediatelly to try to display the informations about the invitation and the room in the next screen
            NSString *loginEmail = _invitationInformations[@"loginEmail"];
            NSString *password = [UITools generateUniquePassword];
            
            [[ServicesManager sharedInstance].loginManager sendSelfRegisterRequestWithLoginEmail:loginEmail password:password invitationId:_invitationInformations[@"invitationID"] visibility:@"none" completionHandler:^(NSDictionary *jsonResponse, NSError *error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    if(error){
                        NSLog(@"Guest self-register error : %@", jsonResponse);
                        // Goto wizard page number 10
                        // Save in the loginViewController that we have an error during the wizard
                        [self gotAnErrorDuringWizardSteps:error];
                        [self nextPage];
                    } else {
                        // Everything ok, saving given username and password
                        [[ServicesManager sharedInstance].loginManager setUsername:loginEmail andPassword:password];
                        // Now login the guest user
                        [[ServicesManager sharedInstance].loginManager connect];
                        // We don't go to the next page, this will be done by didLogin notification into CustomTabBarController
                    }
                });
            }];
        }
    }
    
    WelcomePageCommonViewController *viewCtrl = [self viewControllerAtIndex:indexOfFirstPageToDisplay loadPage:YES];
    [_pageViewCtrl setViewControllers:@[viewCtrl] direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
        _pageViewCtrl.view.alpha = 0.0;
        [self addChildViewController:_pageViewCtrl];
        [self.view addSubview:_pageViewCtrl.view];
        [_pageViewCtrl didMoveToParentViewController:self];
        _pageViewCtrl.view.alpha = 1.0;
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.1 animations:^{
            [self hideLoginViewWhilePresentingWelcomePage:YES];
        }];
    }];
}

@end
