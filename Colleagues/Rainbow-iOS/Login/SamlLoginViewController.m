/*
 * Rainbow
 *
 * Copyright (c) 2019, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "SamlLoginViewController.h"
#import <Rainbow/ServicesManager.h>
#import "UITools.h"
#import <Rainbow/Tools.h>

@interface SamlLoginViewController ()

@property (nonatomic, strong) IBOutlet WKWebView *SAMLWebView;
@property (nonatomic, strong) IBOutlet UINavigationBar *navigationBar;
@property (nonatomic, strong) NSURLRequest *SAMLRequest;
@property (nonatomic, strong) MBProgressHUD *hud;


@end

@implementation SamlLoginViewController

-(id)initWithURLRequest:(NSURLRequest *)request
{
    self = [super init];
    if (self) {
        self.SAMLRequest = request;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _navigationBar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, [UIApplication sharedApplication].statusBarFrame.size.height, self.view.frame.size.width, 50)];
    _navigationBar.barTintColor = [UITools defaultTintColor];
    _navigationBar.delegate = self;
    
    UINavigationItem *navItem = [[UINavigationItem alloc] initWithTitle:NSLocalizedString(@"Login", nil)];

    UIBarButtonItem *cancelBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(onTapCancel:)];
    navItem.leftBarButtonItem = cancelBtn;
    
    [_navigationBar setItems:@[navItem]];
    [self.view addSubview:_navigationBar];
    
    _SAMLWebView = [[WKWebView alloc] initWithFrame:CGRectMake(0, [UIApplication sharedApplication].statusBarFrame.size.height + 50, self.view.frame.size.width, self.view.frame.size.height - [UIApplication sharedApplication].statusBarFrame.size.height - 50)];
    _SAMLWebView.navigationDelegate = self;
    _SAMLWebView.scrollView.delegate = self;

    [_SAMLWebView loadRequest:_SAMLRequest];
    [self.view addSubview:_SAMLWebView];

}

-(void)onTapCancel:(UIBarButtonItem*)item{
    [self dismissViewControllerAnimated:YES completion:^{
        _isLoggedWithSAMLBlock(NO, 0);
    }];
}

- (UIBarPosition)positionForBar:(id <UIBarPositioning>)bar {
    return UIBarPositionTopAttached;
}

#pragma mark - WKNavigationDelegate

-(void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation {
    if (_hud)
        [_hud hide:YES];
    
    NSString *urlString = webView.URL.absoluteString;
    // Remove "#" (fragment) to provide a valid url format for NSURLComponents
    NSString *urlWithoutHash = [urlString stringByReplacingOccurrencesOfString:@"#" withString:@""];
    NSURL *formatedUrl = [NSURL URLWithString:urlWithoutHash];
    
    if ([urlString containsString:@"?tkn="]) {
        
        NSString *token = [Tools valueForKey:@"tkn" fromURL:formatedUrl];
        
        if (token.length > 0) {
            [[ServicesManager sharedInstance].loginManager connectWithToken:token];
            [self dismissViewControllerAnimated:YES completion:^{
                _isLoggedWithSAMLBlock(YES, 0);
            }];
        }
    }
    else if ([urlString containsString:@"?errtype="]) {
        
         int errorCode = [[Tools valueForKey:@"errcode" fromURL:formatedUrl] intValue];
        
        [self dismissViewControllerAnimated:YES completion:^{
            _isLoggedWithSAMLBlock(NO, errorCode);
        }];
    }
}

- (void)webView:(WKWebView *)webView didStartProvisionalNavigation:(WKNavigation *)navigation {
    _hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    _hud.mode = MBProgressHUDModeIndeterminate;
    _hud.removeFromSuperViewOnHide = YES;
    [_hud setLabelFont:[UIFont fontWithName:[UITools defaultFontName] size:14.0f]];
}

- (void)webView:(WKWebView *)webView didFailNavigation:(WKNavigation *)navigation withError:(NSError *)error {
    if (_hud)
        [_hud hide:YES];
}

- (void)webView:(WKWebView *)webView didFailProvisionalNavigation:(WKNavigation *)navigation withError:(NSError *)error {
    if (_hud)
        [_hud hide:YES];
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewWillBeginZooming:(UIScrollView *)scrollView withView:(UIView *)view {
    scrollView.pinchGestureRecognizer.enabled = NO;
}

@end
