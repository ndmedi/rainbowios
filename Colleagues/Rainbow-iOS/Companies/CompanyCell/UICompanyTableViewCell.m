/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "UICompanyTableViewCell.h"
#import "UICompanyAvatarView.h"
#import "UITools.h"
#import <Rainbow/ServicesManager.h>

@interface UICompanyTableViewCell ()
@property (weak, nonatomic) IBOutlet UICompanyAvatarView *companyLogo;
@property (weak, nonatomic) IBOutlet UILabel *companyName;

@end

@implementation UICompanyTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    _companyLogo.showPresence = NO;
    
    self.backgroundColor = [UITools defaultBackgroundColor];
    [UITools applyCustomFontTo:_companyName];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didUpdateCompany:) name:kCompaniesServiceDidUpdateCompany object:nil];
}

-(void) dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kCompaniesServiceDidUpdateCompany object:nil];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void) setCompany:(Company *)company {
    _company = company;
    _companyName.text = _company.name;
    _companyLogo.company = _company;
    _companyLogo.asCircle = _company.logoIsCircle;
}

-(void) didUpdateCompany:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didUpdateCompany:notification];
        });
        return;
    }

    Company *company = (Company *) notification.object;
    if(company == _company){
        _companyLogo.company = _company;
    }
}
@end
