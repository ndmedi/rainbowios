/*
 * Rainbow
 *
 * Copyright (c) 2017, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import <UIKit/UIKit.h>
#import <Rainbow/Company.h>
@class UICompanyContactUsTableViewCell;

@protocol UICompanyContactUsTableViewCellProtocoll <NSObject>

-(void) didPressChatButton:(UICompanyContactUsTableViewCell *)myCell;

@end

@interface UICompanyContactUsTableViewCell : UITableViewCell{
   
}
@property (nonatomic,assign) id<UICompanyContactUsTableViewCellProtocoll> delegationListener;
@property (nonatomic, strong) Company *company;
@property (nonatomic, weak) id fromView;
-(CGFloat) computedHeight;
@end
