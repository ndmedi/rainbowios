/*
 * Rainbow
 *
 * Copyright (c) 2017, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "UICompanyInfosTableViewCell.h"
#import "UITools.h"
#import "UILabel+Clickable.h"
#import <Rainbow/OrderedDictionary.h>

@interface UICompanyInfosTableViewCell ()
@property (weak, nonatomic) IBOutlet UILabel *companyName;
@property (weak, nonatomic) IBOutlet UILabel *companySlogan;

@property (nonatomic, weak) IBOutlet UIView *companyCountryView;
@property (nonatomic, weak) IBOutlet UIView *companyCategoryView;
@property (nonatomic, weak) IBOutlet UIView *companyEmployeesNumberView;
@property (nonatomic, weak) IBOutlet UIView *companyWebsiteView;

@property (weak, nonatomic) IBOutlet UILabel *companyCountryName;
@property (weak, nonatomic) IBOutlet UIImageView *countryLogo;
@property (weak, nonatomic) IBOutlet UILabel *companyWebSite;
@property (weak, nonatomic) IBOutlet UIImageView *websiteLogo;
@property (weak, nonatomic) IBOutlet UILabel *companyEmployeesNumber;
@property (weak, nonatomic) IBOutlet UIImageView *employeesNumberLogo;
@property (weak, nonatomic) IBOutlet UILabel *companyCategory;
@property (weak, nonatomic) IBOutlet UIImageView *categoryLogo;
@property (nonatomic, strong) UITapGestureRecognizer *tapGesture;
@property (nonatomic, strong) OrderedDictionary *countryList;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *companySloganHeigthContraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *companyCountryHeigthContraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *companyCategoryHeigthContraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *companyEmployeesNumberHeigthContraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *companyWebsiteHeigthContraint;

@end

@implementation UICompanyInfosTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [UITools applyCustomFontTo:_companyName];
    [UITools applyCustomBoldFontTo:_companySlogan];
    [UITools applyCustomFontTo:_companyCountryName];
    [UITools applyCustomFontTo:_companyWebSite];
    [UITools applyCustomFontTo:_companyEmployeesNumber];
    
    _countryLogo.tintColor = [UITools defaultTintColor];
    _categoryLogo.tintColor = [UITools defaultTintColor];
    _websiteLogo.tintColor = [UITools defaultTintColor];
    _employeesNumberLogo.tintColor = [UITools defaultTintColor];
    
    NSString *countryListFilePath = [[NSBundle bundleForClass:[self class]] pathForResource: @"country_list" ofType: @"plist"];
    _countryList = [OrderedDictionary dictionaryWithContentsOfFile:countryListFilePath];
}

-(void) dealloc {
    [_companyWebSite removeGestureRecognizer:_tapGesture];
    _tapGesture = nil;
}

-(void) setCompany:(Company *)company {
    _company = company;
    _companyName.text = _company.name;
    
    if(_company.slogan.length > 0)
        _companySlogan.text = _company.slogan;
    else {
        _companySloganHeigthContraint.constant = 0;
        _companySlogan.hidden = YES;
    }
    
    _companyCategoryHeigthContraint.constant = 0;
    _companyCategoryView.hidden = YES;
    
    if (_company.country.length > 0){
        NSString *countryName = [_countryList objectForKey:_company.country];
        _companyCountryName.text = countryName;
    } else {
        _companyCountryHeigthContraint.constant = 0;
        _companyCountryView.hidden = YES;
    }

    if(_company.websiteURL.length > 0){
        NSURL *url = [NSURL URLWithString:_company.websiteURL];
        if(![url scheme])
            url = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@",_company.websiteURL]];
        
        NSMutableAttributedString *webSiteTextAttributed = [[NSMutableAttributedString alloc] initWithString:[[url.absoluteString stringByReplacingOccurrencesOfString:@"https://" withString:@""] stringByReplacingOccurrencesOfString:@"http://" withString:@""]];
        NSRange webSiteRange = NSMakeRange(0, webSiteTextAttributed.length);
    
        NSMutableDictionary *linkAttributes = [NSMutableDictionary dictionaryWithDictionary:@{NSFontAttributeName:[UIFont fontWithName:[UITools defaultFontName] size:14] ,  NSUnderlineStyleAttributeName : @(NSUnderlineStyleSingle), NSLinkAttributeName:url, NSForegroundColorAttributeName: [UITools defaultTintColor]}];
        
        [webSiteTextAttributed setAttributes:linkAttributes range:webSiteRange];
        
        _tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapOnWebSite:)];
        [_companyWebSite addGestureRecognizer:_tapGesture];
        _companyWebSite.userInteractionEnabled = YES;
        
        _companyWebSite.attributedText = webSiteTextAttributed;
    } else {
        _companyWebsiteHeigthContraint.constant = 0;
        _companyWebsiteView.hidden = YES;
    }
    
    if(_company.companySize.length > 0)
        _companyEmployeesNumber.text = [NSString stringWithFormat:@"%@ employees", _company.companySize];
    else {
        _companyEmployeesNumberHeigthContraint.constant = 0;
        _companyEmployeesNumberView.hidden = YES;
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

-(void) handleTapOnWebSite:(UITapGestureRecognizer *)tapGesture {
    CGPoint locationOfTouchInLabel = [tapGesture locationInView:tapGesture.view];
    [_companyWebSite openTappedLinkAtLocation:locationOfTouchInLabel];
}

-(CGFloat) computedHeight {
    CGFloat size = _companyName.frame.size.height + _companySloganHeigthContraint.constant + _companyCountryHeigthContraint.constant + _companyCategoryHeigthContraint.constant + _companyEmployeesNumberHeigthContraint.constant + _companyWebsiteHeigthContraint.constant + (8*6);
    return size;
}

@end
