/*
 * Rainbow
 *
 * Copyright (c) 2019, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "UIMessageMenuActionsViewController.h"
#import "UITools.h"

@implementation UIMessageMenuAction
@end

@interface UIMessageMenuActionsViewController ()
@property (nonatomic, strong) NSMutableArray<UIMessageMenuAction *> *actionsList;
@end

@implementation UIMessageMenuActionsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _actionsTableView.sectionHeaderHeight = 0.0f;
    _actionsTableView.sectionFooterHeight = 0.0f;
    _actionsTableView.tableHeaderView = nil;
    _actionsTableView.tableFooterView = [UIView new];
    _actionsTableView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
    _actionsTableView.backgroundColor = [UITools colorFromHexa:0xF5F5F5FF];

    _actionsList = [NSMutableArray<UIMessageMenuAction*> new];
}

-(void) viewWillAppear:(BOOL)animated {
    [self proposeActionsToAdd];
    [super viewWillAppear:animated];
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    if(self.view.superview)
        self.view.superview.layer.cornerRadius = 5.0f;
}

-(CGSize) prepareForContentSize {
    [self proposeActionsToAdd];
    return CGSizeMake(150, ([_actionsList count] * kTableViewCellHeight)-1);
}

-(void) proposeActionsToAdd {
    if(_delegate) {
        if(_actionsList)
            [_actionsList removeAllObjects];
        else
            _actionsList = [NSMutableArray<UIMessageMenuAction*> new];
        
        // The order this is defined will set the order of items in the menu
        if([_delegate messageMenuShouldAddAction:UIMessageMenuActionTypeReply])
            [self addMenuAction:UIMessageMenuActionTypeReply withLabel:NSLocalizedString(@"Reply", nil)];
        if([_delegate messageMenuShouldAddAction:UIMessageMenuActionTypeShare])
            [self addMenuAction:UIMessageMenuActionTypeShare withLabel:NSLocalizedString(@"Share", nil)];
        if([_delegate messageMenuShouldAddAction:UIMessageMenuActionTypeForward])
            [self addMenuAction:UIMessageMenuActionTypeForward withLabel:NSLocalizedString(@"Forward", nil)];
        if([_delegate messageMenuShouldAddAction:UIMessageMenuActionTypeCopy])
            [self addMenuAction:UIMessageMenuActionTypeCopy withLabel:NSLocalizedString(@"Copy", nil)];
        if([_delegate messageMenuShouldAddAction:UIMessageMenuActionTypeModify])
            [self addMenuAction:UIMessageMenuActionTypeModify withLabel:NSLocalizedString(@"Modify", nil)];
        if([_delegate messageMenuShouldAddAction:UIMessageMenuActionTypeRemove])
            [self addMenuAction:UIMessageMenuActionTypeRemove withLabel:NSLocalizedString(@"Delete", nil)];
        
        [_actionsTableView reloadData];
    }
}

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kTableViewReusableKey];
    cell.userInteractionEnabled = YES;
    cell.textLabel.alpha = 1;
    cell.alpha = 1;
    cell.textLabel.text = [[_actionsList objectAtIndex:indexPath.row] label];
    
    return cell;
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_actionsList count];
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self dismissViewControllerAnimated:YES completion:NULL];
    
    if(_delegate) {
        UIMessageMenuAction *action = [_actionsList objectAtIndex:[indexPath row]];
        // Callback for selected action
        [_delegate messageMenuDidSelectAction: action.type ];
        // Callback for dismissed menu view
        [_delegate messageMenuDidHide];
    }
}

-(void) addMenuAction:(UIMessageMenuActionType) type withLabel:(NSString *) label {
    UIMessageMenuAction *action = [UIMessageMenuAction new];
    action.label = label;
    action.type = type;
    
    if(![_actionsList containsObject:action])
        [_actionsList addObject:action];
}

@end
