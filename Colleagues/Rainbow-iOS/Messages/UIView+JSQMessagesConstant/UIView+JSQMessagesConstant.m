/*
 * Rainbow
 *
 * Copyright (c) 2017, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "UIView+JSQMessagesConstant.h"
#import "UIView+JSQMessages.h"

@implementation UIView (JSQMessagesConstant)
- (void)jsq_pinSubview:(UIView *)subview toEdge:(NSLayoutAttribute)attribute constant:(CGFloat) constant {
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self
                                                     attribute:attribute
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:subview
                                                     attribute:attribute
                                                    multiplier:1.0f
                                                      constant:constant]];
}

@end
