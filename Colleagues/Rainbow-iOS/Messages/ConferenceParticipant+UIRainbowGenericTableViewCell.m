/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "ConferenceParticipant+UIRainbowGenericTableViewCell.h"
#import "UITools.h"

@implementation ConferenceParticipant (UIRainbowGenericTableViewCellProtocol)
-(NSString *) mainLabel {
    if(self.contact)
        return self.contact.displayName;
    else
        return self.phoneNumber;
}

-(UIColor *) mainLabelTextColor {
    if( self.contact.calendarPresence.presence == CalendarPresenceOutOfOffice || self.contact.calendarPresence.automaticReply.isEnabled) {
        return [UITools defaultTintColor];
    } else if( self.contact.calendarPresence.presence == CalendarPresenceBusy ) {
        return [UITools defaultTintColor];
    } else {
        return [UIColor blackColor];
    }
}

-(NSString *) subLabel {
    if( self.contact.isPresenceSubscribed ) {
        return [UITools getPresenceText:self.contact];
    } else if( self.contact.companyName ) {
        if([self.contact.companyName isEqualToString:self.contact.displayName])
            return nil;
        return self.contact.companyName;
    }
    
    return nil;
}

-(NSDate *) date {
    return nil;
}

-(NSString *) bottomRightLabel {
    return nil;
}

-(Peer *) avatar {
    if(self.contact)
        return (Peer*)self.contact;
    return nil;
}

-(NSArray *) observablesKeyPath {
    return @[kContactLastNameKey,kContactFirstNameKey];
}

-(NSInteger) badgeValue {
    return 0;
}

-(NSString *) cellButtonTitle {
    return nil;
}

-(UIImage *) cellButtonImage {
    return nil;
}

-(UIImage *) rightIcon {
    return nil;
}
-(UIColor *) rightIconTintColor {
    return nil;
}

-(BOOL) showMaskedView {
    return self.muted;
}

-(UIImage *) maskedImage {
    if(self.muted)
        return [[UIImage imageNamed:@"muteAudio"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    else
        return nil;
}

-(UIColor *) maskedViewColor {
    return [[UIColor blackColor] colorWithAlphaComponent:0.4];
}
@end
