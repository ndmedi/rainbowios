/*
 * Rainbow
 *
 * Copyright (c) 2017, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import <UIKit/UIKit.h>
#import <Rainbow/File.h>


@class UIMyFilesOnServerTableViewCell;

@protocol UIMyFilesOnServerDelegate <NSObject>
-(void) didTapOnImageForFile:(File *) file forCell:(UIMyFilesOnServerTableViewCell *) cell;

@end

@interface UIMyFilesOnServerTableViewCell : UITableViewCell
@property (nonatomic, strong) File *file;
@property (nonatomic, assign) id<UIMyFilesOnServerDelegate> delegate;
@end
