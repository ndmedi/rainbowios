/*
 * Rainbow
 *
 * Copyright (c) 2019, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import <UIKit/UIKit.h>

#define kTableViewReusableKey @"actionCell"
#define kTableViewCellHeight 44

typedef NS_ENUM(NSInteger, UIMessageMenuActionType) {
    UIMessageMenuActionTypeCopy = 0,
    UIMessageMenuActionTypeShare,
    UIMessageMenuActionTypeForward,
    UIMessageMenuActionTypeReply,
    UIMessageMenuActionTypeModify,
    UIMessageMenuActionTypeRemove
};

@interface UIMessageMenuAction : NSObject
@property (nonatomic, weak) NSString *label;
@property (nonatomic) UIMessageMenuActionType type;
@end;

@protocol UIMessageMenuActionsViewControllerDelegate <NSObject>
@required
-(BOOL) messageMenuShouldAddAction:(UIMessageMenuActionType) actionType;
-(void) messageMenuDidSelectAction:(UIMessageMenuActionType) actionType;
-(void) messageMenuDidHide;
@end

@interface UIMessageMenuActionsViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *actionsTableView;
@property (nonatomic, weak) id<UIMessageMenuActionsViewControllerDelegate> delegate;
-(CGSize) prepareForContentSize;
@end
