//
//  Created by Jesse Squires
//  http://www.jessesquires.com
//
//
//  Documentation
//  http://cocoadocs.org/docsets/JSQMessagesViewController
//
//
//  GitHub
//  https://github.com/jessesquires/JSQMessagesViewController
//
//
//  License
//  Copyright (c) 2014 Jesse Squires
//  Released under an MIT license: http://opensource.org/licenses/MIT
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

/**
 *  Modified version of JSQMessagesTypingIndicatorFooterView
 *  to add an avatar next to the typing indicator.
 */
@interface JSQMessagesTypingIndicatorFooterViewWithAvatar : UICollectionReusableView

#pragma mark - Class methods

+ (UINib *)nib;

+ (NSString *)footerReuseIdentifier;

#pragma mark - Typing indicator

- (void)configureWithAvatar:(NSArray<UIImage *> *) avatars
              ellipsisColor:(UIColor *)ellipsisColor
                messageBubbleColor:(UIColor *)messageBubbleColor
                 forCollectionView:(UICollectionView *)collectionView;

@end
