/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "UIMessagesSearchViewController.h"
#import <Rainbow/Room.h>
#import <Rainbow/Message.h>
#import <Rainbow/ConversationsManagerService.h>
#import "UITools.h"
#import "UIContactDetailsViewController.h"
#import "UIRoomDetailsViewController.h"
#import <Rainbow/MessagesBrowser.h>
#import <Rainbow/Message+Browsing.h>
#import <Rainbow/Tools.h>
#import <Rainbow/ServicesManager.h>
#import <Rainbow/NSDate+Utilities.h>
#import "Contact+Extensions.h"
#import "UIViewController+Visible.h"
#import "JSQMessagesCollectionViewCellOutgoing.h"
#import "GroupedMessages.h"
#import <Rainbow/NSDate+Distance.h>
#import "Message-JSQMessageData.h"
#import "SORelativeDateTransformer.h"
#import <MarqueeLabel/MarqueeLabel.h>
#import "UIRoomsTableViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "JSQMessagesTypingIndicatorFooterViewWithAvatar.h"
#import "JSQRainbowOutgoingMessagesCollectionViewCell.h"
#import "JSQRainbowIncomingMessagesCollectionViewCell.h"
#import "JSQRainbowRoomIncomingMessagesCollectionViewCell.h"
#import "JSQMessagesViewController.h"
#import "JSQMessagesAutoLoadEarlierHeaderView.h"
#import "JSQGroupChatEventCell.h"
#import "CustomJSQMessagesBubblesSizeCalculator.h"
#import "MBProgressHUD.h"
#import "Conversation+RainbowID.h"
#import "JSQMessagesComposerTextView+Paste.h"
#import "CustomNavigationController.h"
#import "JSQWebRTCEventsCell.h"
#import <Photos/Photos.h>
#import "SCLAlertView.h"
#import "SCLAlertView+Rotation.h"
#import "UIDevice+JSQMessages.h"
#import "File+DefaultImage.h"
#import "UIPreviewQuickLookController.h"
#import "JSQMessagesInputToolbar+HitTest.h"
#import "UIMyFilesOnServerTableViewController.h"
#import <Rainbow/NSData+MimeType.h>
#import <Rainbow/NSDictionary+JSONString.h>
#import "GoogleAnalytics.h"
#import "Message+BodyForGroupChatEvent.h"
#import "UIPagingMessagesViewController.h"
#import <Rainbow/MessagesAlwaysInSyncBrowserAddOn.h>
#import "UIStoryboardManager.h"
#import "UIGenericBubbleDetailsViewController.h"
#import "Room+Extensions.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import "JoinMeetingViewController.h"
#import "UIStoryboardManager.h"
#import "CustomSearchController.h"
#import "ForwardViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "UIGuestModeViewController.h"
#import "Firebase.h"
#import "SDWebImage/UIImageView+WebCache.h"
#import <SDWebImage/SDAnimatedImageView.h>
#import "RecentsConversationsTableViewController.h"

typedef void (^progressBlock) (JSQRainbowMessagesCollectionViewCell *cell, double totalBytesSent, double totalBytesExpectedToSend);
typedef void (^downloadBlock) (File *aFile);

#define kPageSize 20
#define kMaxTextLength 1024

@interface UIMessagesSearchViewController () <CustomSearchControllerDelegate,JSQMessagesComposerTextViewPasteDelegate, CKItemsBrowserDelegate, UIGestureRecognizerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, JSQMessagesCollectionViewCellDelegate, QLPreviewControllerDataSource, QLPreviewControllerDelegate, UIDocumentPickerDelegate, UIPopoverPresentationControllerDelegate, UITabBarControllerDelegate> {
    BOOL isNetworkConnected;
    BOOL isRemoveRoom;
    CGFloat topPadding;
    CGFloat bottomPadding;
}

- (void)jsq_adjustInputToolbarForComposerTextViewContentSizeChange:(CGFloat)dy;
- (void)jsq_adjustInputToolbarHeightConstraintByDelta:(CGFloat)dy;
- (void)jsq_updateCollectionViewInsets;
- (void)jsq_setCollectionViewInsetsTopValue:(CGFloat)top bottomValue:(CGFloat)bottom;
- (void)collectionView:(JSQMessagesCollectionView *)collectionView accessibilityForCell:(JSQMessagesCollectionViewCell*)cell indexPath:(NSIndexPath *)indexPath message:(id<JSQMessageData>)messageItem;

@property (nonatomic, strong) ContactsManagerService *contactsManagerService;
@property (nonatomic, strong) ConversationsManagerService *conversationsManagerService;
@property (nonatomic, strong) FileSharingService *fileManagerService;

@property (nonatomic, strong) JSQMessagesBubbleImageFactory *bubbleFactory;
@property (nonatomic, strong) JSQMessagesBubbleImage *outgoingBubbleImageData;
// the incoming bubble images, for each senderId (i.e. jid)
@property (nonatomic, strong) NSMutableDictionary<NSString *, JSQMessagesBubbleImage *> *incomingBubbleImageDataDict;
@property (nonatomic, strong) UIImage *groupChatEventBubbleImage;

@property (nonatomic, strong) JSQMessagesAvatarImage *meAvatar;
@property (nonatomic, strong) NSMutableDictionary<NSString *, JSQMessagesAvatarImage *> *contactAvatarDict;

@property (nonatomic, strong) MessagesBrowser *messagesBrowser;
@property (nonatomic) BOOL shouldAnimateAddedItems;
@property (nonatomic) BOOL isRetrievingNextPage;
@property (nonatomic) BOOL needsAvatar;
@property (nonatomic, strong) NSMutableArray  *filesDownloading;
@property (nonatomic, strong) NSIndexPath *tappedIndexPath;
@property (nonatomic, strong) UIAlertController *moreMenuActionSheet;
@property (nonatomic, strong) NSTimer *updateBannerRefreshTimer;
@property (nonatomic, strong) NSMutableArray<Contact *> *typingContacts;
@property (nonatomic) BOOL allowAutomaticallyScrollsToMostRecentMessage;

@property (nonatomic, strong) UIAlertController *leftAccessoryButtonActionSheet;

@property (nonatomic, strong) UIAlertController *conferenceCallBackAlert;
@property (nonatomic, strong) MessagesAlwaysInSyncBrowserAddOn *alwaysInSyncAddon;
@property (nonatomic) BOOL stopShowingHeaderView;
@property (nonatomic) BOOL startShowingSharingView;
@property (nonatomic, strong) UIColor *previousNavBarTintColor;
@property (nonatomic, strong) NSDictionary *previousTitleTextAttributesColor;
@property (nonatomic) NSUInteger searchMessageOccurencesCount;
@property (nonatomic) NSMutableArray<NSDate *> *searchMessageOccurencesTimestamps;
@property (nonatomic) NSUInteger searchMessageFirstOccurence;
@property (nonatomic) NSUInteger searchMessageLastOccurence;
@property (nonatomic) NSUInteger searchMessageCurrentOccurence;
@property (nonatomic) NSIndexPath *searchMessageCurrentOccurenceIndexPath;
@property (nonatomic, strong) IBOutlet UIView *searchMessageViewBar;
@property (nonatomic, weak) IBOutlet UIButton *searchMessageButtonPrevious;
@property (nonatomic, weak) IBOutlet UIButton *searchMessageButtonNext;
@property (nonatomic, weak) IBOutlet UIButton *searchMessageButtonSearch;
@property (nonatomic, weak) IBOutlet UIButton *searchMessageButtonClose;
@property (nonatomic, weak) IBOutlet UITextField *searchMessageTextfield;
@property (nonatomic, weak) IBOutlet UILabel *searchMessageLabel;

@end

@implementation UIMessagesSearchViewController

-(instancetype) initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if(self){
        isNetworkConnected = YES;
        _needsAvatar = NO;
        isRemoveRoom = NO;
        _contactsManagerService = [ServicesManager sharedInstance].contactsManagerService;
        _conversationsManagerService = [ServicesManager sharedInstance].conversationsManagerService;
        _fileManagerService=[ServicesManager sharedInstance].fileSharingService;
        _messages = [NSMutableArray array];
        _bubbleFactory = [[JSQMessagesBubbleImageFactory alloc] initWithBubbleImage:[UIImage jsq_bubbleRainbowImage] capInsets:UIEdgeInsetsZero];
        _outgoingBubbleImageData = [_bubbleFactory outgoingMessagesBubbleImageWithColor:[UIColor jsq_messageBubbleLightGrayColor]];
        _incomingBubbleImageDataDict = [NSMutableDictionary new];
        _contactAvatarDict = [NSMutableDictionary new];
        _tappedIndexPath = nil;
        _typingContacts = [NSMutableArray new];
        _filesDownloading = [NSMutableArray array];
        _conferenceCallBackAlert = nil;
        
//        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReceiveComposingMessage:) name:kConversationsManagerDidReceiveComposingMessage object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didUpdateContact:) name:kContactsManagerServiceDidUpdateContact object:nil];
//        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didUpdateRoom:) name:kRoomsServiceDidUpdateRoom object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didLostConnection:) name:kLoginManagerDidLostConnection object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didLogin:) name:kLoginManagerDidLoginSucceeded object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReconnect:) name:kLoginManagerDidReconnect object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(UITextViewDidChange:) name:UITextViewTextDidChangeNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardDidHideNotification object:nil];
//        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didAddCall:) name:kTelephonyServiceDidAddCallNotification object:nil];
//        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didUpdateCall:) name:kTelephonyServiceDidUpdateCallNotification object:nil];
//        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didRemoveCall:) name:kTelephonyServiceDidRemoveCallNotification object:nil];
//        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didUpdateConference:) name:kConferencesManagerDidUpdateConference object:nil];
//        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReceiveConferenceReminder:) name:kConversationsManagerDidReceiveConferenceReminderInConversation object:nil];
//        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(doJoinConference:) name:@"joinConference" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didUpdateProgress:) name:kFileSharingServiceDidUpdateUploadedBytesSent object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didUpdateDownloadedProgress:) name:kFileSharingServiceDidUpdateDownloadedBytes object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(shouldUpdateAttachment:) name:kFileSharingServiceDidUpdateFile object:nil];
//        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didRemoveRoom:) name:kRoomsServiceDidRemoveRoom object:nil];
//        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didFailRemoveRoom:) name:kRoomsServiceDidFailRemoveRoom object:nil];
//        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didRemoveFile:) name:kFileSharingServiceDidRemoveFile object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didFoundResultsInConversation:) name:kSearchDidFoundResultsInAConversation object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didFoundMessagesInConversation:) name:kSearchDidFoundMessagesInAConversation object:nil];
    }
    return self;
}

-(void) dealloc {
    _conferenceCallBackAlert = nil;
//    [_messagesBrowser reset];
//    [_messages removeAllObjects];
    _messages = nil;
//    _alwaysInSyncAddon.browser = nil;
//    _alwaysInSyncAddon = nil;
//    _messagesBrowser.delegate = nil;
//    _messagesBrowser = nil;
    _outgoingBubbleImageData = nil;
    [_incomingBubbleImageDataDict removeAllObjects];
    _incomingBubbleImageDataDict = nil;
    _tappedIndexPath = nil;
    [_contactAvatarDict removeAllObjects];
    _contactAvatarDict = nil;
    [_typingContacts removeAllObjects];
    _typingContacts = nil;
//    [[NSNotificationCenter defaultCenter] removeObserver:self name:kConversationsManagerDidReceiveComposingMessage object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kContactsManagerServiceDidUpdateContact object:nil];
//    [[NSNotificationCenter defaultCenter] removeObserver:self name:kRoomsServiceDidUpdateRoom object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidLostConnection object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidLoginSucceeded object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidReconnect object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UITextViewTextDidChangeNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidHideNotification object:nil];
//    [[NSNotificationCenter defaultCenter] removeObserver:self name:kConferencesManagerDidUpdateConference object:nil];
//    [[NSNotificationCenter defaultCenter] removeObserver:self name:kConversationsManagerDidReceiveConferenceReminderInConversation object:nil];
    
//    [[NSNotificationCenter defaultCenter] removeObserver:self name:kTelephonyServiceDidAddCallNotification object:nil];
//    [[NSNotificationCenter defaultCenter] removeObserver:self name:kTelephonyServiceDidUpdateCallNotification object:nil];
//    [[NSNotificationCenter defaultCenter] removeObserver:self name:kTelephonyServiceDidRemoveCallNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kFileSharingServiceDidUpdateUploadedBytesSent object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kFileSharingServiceDidUpdateDownloadedBytes object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"joinConference" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kFileSharingServiceDidUpdateFile object:nil];
//    [[NSNotificationCenter defaultCenter] removeObserver:self name:kRoomsServiceDidRemoveRoom object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kFileSharingServiceDidRemoveFile object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kSearchDidFoundResultsInAConversation object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kSearchDidFoundMessagesInAConversation object:nil];
    
//    [_conversation removeObserver:self forKeyPath:kConversationIsMuted];
//    [_conversation removeObserver:self forKeyPath:kConversationUnreadMessagesCount];
    
//    if(_updateBannerRefreshTimer){
//        [_updateBannerRefreshTimer invalidate];
//        _updateBannerRefreshTimer = nil;
//    }
    _bubbleFactory = nil;
//    [_notificationDisabledView removeFromSuperview];
//    _notificationDisabledView = nil;
//
//    _attachmentFileToSend = nil;
//    _attachmentPreview = nil;
    _conversationsManagerService = nil;
    _contactsManagerService = nil;
    _fileManagerService=nil;
}

-(void) setAvatarForContact:(Contact *) contact {
    UIImage *contactImage = nil;
    if(contact.photoData != nil)
        contactImage = [UIImage imageWithData:contact.photoData];
    
    JSQMessagesAvatarImage *avatar = nil;
    if(contactImage)
        avatar = [JSQMessagesAvatarImageFactory avatarImageWithImage:contactImage diameter:kJSQMessagesCollectionViewAvatarSizeDefault];
    else
        avatar = [JSQMessagesAvatarImageFactory avatarImageWithUserInitials:contact.initials backgroundColor:[UITools colorForString:contact.displayBestInfo] textColor:[UIColor whiteColor] font:[UIFont fontWithName:[UITools defaultFontName] size:12] diameter:kJSQMessagesCollectionViewAvatarSizeDefault];
    
    [_contactAvatarDict setObject:avatar forKey:contact.jid];
    [self reloadCollectionView];
}

-(void) setAvatarAndBubbleForContact:(Contact *) contact {
    [_incomingBubbleImageDataDict setObject:[_bubbleFactory incomingMessagesBubbleImageWithColor:[UITools colorForString:contact.displayBestInfo]] forKey:contact.jid];
    [self setAvatarForContact:contact];
}

-(void) setConversation:(Conversation *)conversation {
//    [_conversation removeObserver:self forKeyPath:kConversationIsMuted];
//    [_conversation removeObserver:self forKeyPath:kConversationUnreadMessagesCount];
    
    // flush the existing registred bubbles and avatars
    [_incomingBubbleImageDataDict removeAllObjects];
    [_contactAvatarDict removeAllObjects];
//    _alwaysInSyncAddon.browser = nil;
//    _alwaysInSyncAddon = nil;
//    [_messagesBrowser reset];
//    _messagesBrowser.delegate = nil;
//    _messagesBrowser = nil;
    
    _conversation = nil;
    _conversation = conversation;
    
    // now, do additional stuff depending on the conversation type.
    if (_conversation.type == ConversationTypeUser || _conversation.type == ConversationTypeBot) {
        // register its incoming bubble, with a default color.
        [_incomingBubbleImageDataDict setObject:[_bubbleFactory incomingMessagesBubbleImageWithColor:[UITools defaultTintColor]] forKey:_conversation.peer.jid];
        [self setAvatarAndBubbleForContact:(Contact*)_conversation.peer];
    } else if (_conversation.type == ConversationTypeRoom) {
        //[self toggleDisableWhenMuted];
        // register a different incoming bubble color for each participant.
        Room *room = (Room*) _conversation.peer;
        for (Participant *participant in room.participants) {
            if(participant.contact.jid)
                [self setAvatarAndBubbleForContact:participant.contact];
        }
    }
    
//    [_conversation addObserver:self forKeyPath:kConversationIsMuted options:NSKeyValueObservingOptionNew context:nil];
//    [_conversation addObserver:self forKeyPath:kConversationUnreadMessagesCount options:NSKeyValueObservingOptionNew context:nil];
    
//    _messagesBrowser = [_conversationsManagerService messagesBrowserForConversation:_conversation withPageSize:kPageSize preloadMessages:NO];
//    _messagesBrowser.delegate = self;
    
//    _alwaysInSyncAddon = [[MessagesAlwaysInSyncBrowserAddOn alloc] init];
//    _alwaysInSyncAddon.browser = _messagesBrowser;
    
    _allowAutomaticallyScrollsToMostRecentMessage = YES;
    [self reloadCollectionView];
    
    // The room name could have been changed in bubble's detail view
    if (_conversation.type == ConversationTypeRoom) {
        Room *room = (Room *) _conversation.peer;
        if(self.navigationController.title != room.displayName){
            [self setConversationTitle:room.displayName];
        }
    } else {
        self.navigationController.navigationBar.barTintColor = [UITools colorForString:_conversation.peer.displayName];
        Contact *contact = (Contact*)_conversation.peer;
        
        // Fetch potentially missing informations about the contact
        [[ServicesManager sharedInstance].contactsManagerService fetchRemoteContactDetail:contact];
        
//        // Request calendar Automatic reply
//        if( contact.isPresenceSubscribed ) {
//            [[ServicesManager sharedInstance].contactsManagerService fetchCalendarAutomaticReply:contact];
//        }
    }
}

-(void) setMessages:(NSMutableArray<GroupedMessages *> *)messages {
    _messages = [NSMutableArray arrayWithArray:messages];
}

-(void) observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context {
    // Update the status banner on presence or  last-activity date update
    if([keyPath isEqualToString:kConversationIsMuted]) {
        if(![NSThread isMainThread]){
            dispatch_async(dispatch_get_main_queue(), ^{
                [self updateBanner];
//                [self showHideCallButton];
            });
            return;
        } else {
            [self updateBanner];
//            [self showHideCallButton];
        }
    } else if ([keyPath isEqualToString:kConversationUnreadMessagesCount]){
//        if(![NSThread isMainThread]){
//            dispatch_async(dispatch_get_main_queue(), ^{
//                [self showScrollToBottomButton:!_allowAutomaticallyScrollsToMostRecentMessage];
//            });
//        } else {
//            [self showScrollToBottomButton:!_allowAutomaticallyScrollsToMostRecentMessage];
//        }
    } else {
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    }
}

-(void) didUpdateContact:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didUpdateContact:notification];
        });
        return;
    }
    
    NSDictionary *userInfo = (NSDictionary *)notification.object;
    Contact *contact = [userInfo objectForKey:kContactKey];
    NSArray<NSString *> *changedKeys = [userInfo objectForKey:kChangedAttributesKey];
    
    // Dont update if not needed (change in photo or displayName)
    if (![changedKeys containsObject:@"photoData"] &&
        ![changedKeys containsObject:@"displayName"] &&
        ![changedKeys containsObject:@"calendarPresence"] &&
        ![changedKeys containsObject:@"sentInvitation"] &&
        ![changedKeys containsObject:@"presence"] &&
        ![changedKeys containsObject:@"firstName"] &&
        ![changedKeys containsObject:@"lastName"]&&
        ![changedKeys containsObject:@"isConnectedWithMobile"]
        
        ) {
        return;
    }
    
    if (_conversation.type == ConversationTypeUser || _conversation.type == ConversationTypeBot) {
        if([contact isEqual:_conversation.peer]) {
            if([self isViewLoaded]){
                if ([changedKeys containsObject:@"presence"] || [changedKeys containsObject:@"lastActivityDate"]|| [changedKeys containsObject:@"calendarPresence"] || [changedKeys containsObject:@"sentInvitation"] || [changedKeys containsObject:@"isConnectedWithMobile"] || [changedKeys containsObject:@"isInRoster"] || [changedKeys containsObject:@"isPresenceSubscribed"]) {
                    [self updateBanner];
//                    [self showHideCallButton];
                }
                [self setAvatarForContact:contact];
                [self setTitleView:contact.displayBestInfo];
            }
        }
    }
    
    if (_conversation.type == ConversationTypeRoom) {
        Room *room = (Room*) _conversation.peer;
        for (Participant *participant in room.participants) {
            if ([contact isEqual:participant.contact]) {
                [self setAvatarAndBubbleForContact:contact];
                return;
            }
        }
    }
}

//-(void) didUpdateRoom:(NSNotification *) notification {
//    if(![NSThread isMainThread]){
//        dispatch_async(dispatch_get_main_queue(), ^{
//            [self didUpdateRoom:notification];
//        });
//        return;
//    }
//
//    NSDictionary *userInfo = (NSDictionary *)notification.object;
//    Room *room = [userInfo objectForKey:kRoomKey];
//    NSArray<NSString *> *changedKeys = [userInfo objectForKey:kRoomChangedAttributesKey];
//
//    if([_conversation.peer isEqual:room]){
//        if([changedKeys containsObject:@"users"]){
//            [self toggleDisableWhenOutOfTheRoom];
//            for (Participant *participant in room.participants) {
//                if(participant.contact.jid)
//                    [self setAvatarAndBubbleForContact:participant.contact];
//            }
//        }
//        if([changedKeys containsObject:@"name"]){
//            self.title = _conversation.peer.displayName;
//            [self parentViewController].title = _conversation.peer.displayName;
//            [self setConversationTitle:_conversation.peer.displayName];
//        }
//        if([changedKeys containsObject:@"creator"] || [changedKeys containsObject:@"confEndpoints"]){
//            [self showHideCallButton];
//        }
//    }
//}

//-(void) didUpdateConference:(NSNotification *) notification {
//    if(![NSThread isMainThread]){
//        dispatch_async(dispatch_get_main_queue(), ^{
//            [self didUpdateConference:notification];
//        });
//        return;
//    }
//    NSDictionary *userInfo = (NSDictionary *)notification.object;
//    Conference *conference = [userInfo objectForKey:kConferenceKey];
//    NSArray<NSString *> *changedKeys = [userInfo objectForKey:kConferenceChangedAttributesKey];
//
//    if([_conversation.peer isKindOfClass:[Room class]]){
//        if([((Room *)_conversation.peer).conference isEqual:conference]){
//            if([changedKeys containsObject:@"canJoin"] || [changedKeys containsObject:@"isActive"])
//                [self showHideCallButton];
//        }
//    }
//}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Set the title of the view and after replace it by a button like that the navigation bar calculate the correct size of the back button, the title is always centered correctly.
    [self setTitleView:_conversation.peer.displayName];
    
    self.senderId = [ServicesManager sharedInstance].myUser.contact.jid;
    self.senderDisplayName = [ServicesManager sharedInstance].myUser.contact.firstName ? [ServicesManager sharedInstance].myUser.contact.firstName : [ServicesManager sharedInstance].myUser.contact.jid;
    
    UIImage *meImage = nil;
    if([ServicesManager sharedInstance].myUser.contact.photoData != nil)
        meImage = [UIImage imageWithData:[ServicesManager sharedInstance].myUser.contact.photoData];
    else
        meImage = [UIImage imageNamed:@"ContactTabBar"];
    
    _meAvatar = [JSQMessagesAvatarImageFactory avatarImageWithImage:meImage diameter:kJSQMessagesCollectionViewAvatarSizeDefault];
    
    // input toolbar
    self.inputToolbar.contentView.textView.jsqPasteDelegate = self;
    
    self.inputToolbar.contentView.textView.font = [UIFont fontWithName:[UITools defaultFontName] size:self.inputToolbar.contentView.textView.font.pointSize];
    [UITools applyCustomBoldFontTo:self.inputToolbar.contentView.rightBarButtonItem.titleLabel];
    self.inputToolbar.maximumHeight = 150;
    
    // input toolbar let button : upload images, files,...
    [self.inputToolbar.contentView.leftBarButtonItem setTitle:@"" forState:UIControlStateDisabled];
    [self.inputToolbar.contentView.leftBarButtonItem setTitle:@"" forState:UIControlStateNormal];
    [self.inputToolbar.contentView.leftBarButtonItem setImage:[UIImage imageNamed:@"add_file"] forState:UIControlStateNormal];
    [self.inputToolbar.contentView.leftBarButtonItem setImage:[UIImage imageNamed:@"add_file"] forState:UIControlStateHighlighted];
    self.inputToolbar.contentView.leftBarButtonItem.contentMode = UIViewContentModeScaleAspectFit;
    [self.inputToolbar.contentView.leftBarButtonItem.imageView setTintColor:[UITools defaultTintColor]];
    self.inputToolbar.contentView.leftBarButtonItemWidth = 30;
    self.inputToolbar.contentView.leftContentPadding = 10;
    
    // input toolbar right button : send
    [self.inputToolbar.contentView.rightBarButtonItem setTitle:@"" forState:UIControlStateDisabled];
    [self.inputToolbar.contentView.rightBarButtonItem setTitle:@"" forState:UIControlStateNormal];
    [self.inputToolbar.contentView.rightBarButtonItem setImage:[UIImage imageNamed:@"Send"] forState:UIControlStateNormal];
    [self.inputToolbar.contentView.rightBarButtonItem setImage:[UIImage imageNamed:@"Send_disabled"] forState:UIControlStateDisabled];
    [self.inputToolbar.contentView.rightBarButtonItem.imageView setTintColor:[UITools defaultTintColor]];
    
    self.collectionView.collectionViewLayout.outgoingAvatarViewSize = CGSizeZero;
    self.collectionView.collectionViewLayout.messageBubbleFont = [UIFont fontWithName:[UITools defaultFontName] size:14];
    
    [self.collectionView registerNib:[JSQMessagesTypingIndicatorFooterViewWithAvatar nib] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:[JSQMessagesTypingIndicatorFooterViewWithAvatar footerReuseIdentifier]];
    
    // subclass JSQMessagesAutoLoadEarlierHeaderView to load automatically the old messages when scrolling
    [self.collectionView registerNib:[JSQMessagesAutoLoadEarlierHeaderView nib] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:[JSQMessagesAutoLoadEarlierHeaderView headerReuseIdentifier]];
    
    if (_conversation.type == ConversationTypeRoom) {
        [self.collectionView registerNib:[JSQGroupChatEventCell nib] forCellWithReuseIdentifier:[JSQGroupChatEventCell cellReuseIdentifier]];
        [self.collectionView registerNib:[JSQRainbowRoomIncomingMessagesCollectionViewCell nib] forCellWithReuseIdentifier:[JSQRainbowRoomIncomingMessagesCollectionViewCell cellReuseIdentifier]];
        self.incomingCellIdentifier = [JSQRainbowRoomIncomingMessagesCollectionViewCell cellReuseIdentifier];
    } else {
        [self.collectionView registerNib:[JSQRainbowIncomingMessagesCollectionViewCell nib] forCellWithReuseIdentifier:[JSQRainbowIncomingMessagesCollectionViewCell cellReuseIdentifier]];
        self.incomingCellIdentifier = [JSQRainbowIncomingMessagesCollectionViewCell cellReuseIdentifier];
    }
    
    self.groupChatEventBubbleImage = [self eventBubbleCellImage];
    
    [self.collectionView registerNib:[JSQRainbowOutgoingMessagesCollectionViewCell nib] forCellWithReuseIdentifier:[JSQRainbowOutgoingMessagesCollectionViewCell cellReuseIdentifier]];
    self.outgoingCellIdentifier = [JSQRainbowOutgoingMessagesCollectionViewCell cellReuseIdentifier];
    
    [self.collectionView registerNib:[JSQWebRTCEventsCell nib] forCellWithReuseIdentifier:[JSQWebRTCEventsCell cellReuseIdentifier]];
    
    // preconfigure status label.
    self.statusLabel.translatesAutoresizingMaskIntoConstraints = NO;
    [UITools applyCustomFontTo:self.statusLabel];
    self.statusLabel.textColor = [UIColor whiteColor];
    self.statusLabel.textAlignment = NSTextAlignmentCenter;
    self.statusLabel.backgroundColor = [UIColor darkGrayColor];
    self.statusLabel.lineBreakMode = NSLineBreakByWordWrapping;
    self.statusLabel.numberOfLines = 0;
    //
//    self.iconImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"AddContact"]];
//    self.iconImageView.tintColor = [UIColor whiteColor];
//    self.iconImageView.frame = CGRectMake(self.statusLabel.frame.size.width/2-(20/2), 4.0f, 20, 20);
//    self.iconImageView.contentMode = UIViewContentModeScaleAspectFill;
//    self.iconImageView.clipsToBounds = YES;
//    [self.statusLabel addSubview: self.iconImageView];
    
    [self toggleDisableWhenOutOfTheRoom];
    
//    _notificationDisabledView.hidden = YES;
    
    [self showSearchMessageViewBarWithSearchField:!([_searchMessageText length]>0)];

    if(_searchMessageText) {
        [self searchInConversation:_conversation withText:_searchMessageText];
    }
    
    // update the status banner
    [self updateBanner];
    
    CustomJSQMessagesBubblesSizeCalculator *customJSQMEssagesBubblesSizeCalculator = [CustomJSQMessagesBubblesSizeCalculator new];
    self.collectionView.collectionViewLayout.bubbleSizeCalculator = customJSQMEssagesBubblesSizeCalculator;
    customJSQMEssagesBubblesSizeCalculator.isMUC = (_conversation.type == ConversationTypeRoom) ? YES : NO;
    
//    [self addScrollToBottomButton];
//    [self addActionsView];
    
//    _closeButton.tintColor = [UITools defaultTintColor];
//    UIImage *close = [UIImage imageNamed:@"close_conversation"];
//    [_closeButton setImage:[close imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
//    _closeButton.layer.cornerRadius = _closeButton.frame.size.height/2;
//    _closeButton.backgroundColor = [UIColor clearColor];
//    [_closeButton addTarget:self action:@selector(closeAttachementButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
//
//    _attachmentView.backgroundColor = [UITools defaultBackgroundColor];
    
//    [self adjustAttachmentViewFrameWithHeight:200];
//    [UITools applyCustomFontTo:_attachmentFileNameLabel];
    
    topPadding = 0;
    bottomPadding = 0;
    if (@available(iOS 11.0, *)) {
        UIWindow *window = UIApplication.sharedApplication.keyWindow;
        topPadding = window.safeAreaInsets.top;
        bottomPadding = window.safeAreaInsets.bottom;
    }
    
}

/**
 * change the navigation bar title
 */
-(void)setConversationTitle:(NSString *)text {
    UINavigationItem *navItem = [self goodNavigationItem];
    if([navItem.titleView isKindOfClass:[UIButton class]]){
//        [(UIButton *)navItem.titleView setTitle:text forState:UIControlStateNormal];
    }
}
-(void)setTitleView:(NSString *)title {
    self.title = title;
    //[self parentViewController].title = title;
    self.definesPresentationContext = YES;
//    UIColor *whiteAlpha90 = [UIColor colorWithRed:1 green:1 blue:1 alpha:0.9];
//    UIBarButtonItem *titleLabelButton = [UIBarButtonItem buttonWithType:UIButtonTypeCustom];
//    [titleLabelButton setTitle:@"te" forState:UIControlStateNormal];
//    [UITools applyCustomBoldFontTo:titleLabelButton.titleLabel];
//    [titleLabelButton setTitleColor:whiteAlpha90 forState:UIControlStateNormal];
//    [titleLabelButton addTarget:self action:@selector(didTapTitleButton:) forControlEvents:UIControlEventTouchUpInside];
//    titleLabelButton.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
//    UINavigationItem *item = [self goodNavigationItem];
}

-(UINavigationItem *) goodNavigationItem {
    return self.navigationItem;
}

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
//    [self parentViewController].title = _conversation.peer.displayName;
//    self.showLoadEarlierMessagesHeader = YES;
    self.navigationController.navigationBar.tintColor = [[UIColor whiteColor] colorWithAlphaComponent:0.9];
    [[UINavigationBar appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName:[[UIColor whiteColor] colorWithAlphaComponent:0.9], NSFontAttributeName:[UIFont fontWithName:[UITools boldFontName] size:17.0]}];
    
//    // Mark the conversation as active
//    // uncomment this to enable XEP-085 chat-state
//    [_conversationsManagerService setStatus:ConversationStatusActive forConversation:_conversation];
    
    // look if there is a last message entered for this conversation
    NSDictionary * dic = [[NSUserDefaults standardUserDefaults] objectForKey:@"lastMessagesEntered"];
    if([dic objectForKey:_conversation.conversationId]){
        self.inputToolbar.contentView.textView.text = dic[_conversation.conversationId];
        [self.inputToolbar.contentView.textView.delegate textViewDidChange:self.inputToolbar.contentView.textView];
        [self performSelectorOnMainThread:@selector(adjustTextView) withObject:nil waitUntilDone:NO];
        
        NSMutableDictionary *latestMessagesEntereded = [NSMutableDictionary dictionaryWithDictionary:dic];
        [latestMessagesEntereded removeObjectForKey:_conversation.conversationId];
        [[NSUserDefaults standardUserDefaults] setObject:latestMessagesEntereded forKey:@"lastMessagesEntered"];
    }
    
//    [self showHideCallButton];
    [self updateConversationStatusAndToggleSendButton];
    
    
    [[UIApplication sharedApplication] setStatusBarStyle: UIStatusBarStyleLightContent];
    
    [[UINavigationBar appearanceWhenContainedInInstancesOfClasses:@[[UIViewController class]]] setTintColor:[UITools defaultTintColor]];
    
    [[UINavigationBar appearanceWhenContainedInInstancesOfClasses:@[[UINavigationController class]]] setTintColor:[UIColor whiteColor]];
    
    [[UINavigationBar appearanceWhenContainedInInstancesOfClasses:@[[UITabBarController class]]] setTintColor:[UIColor whiteColor]];
    
    
}

-(void) adjustTextView {
    UITextView *textView = self.inputToolbar.contentView.textView;
    CGSize oldSize = textView.frame.size;
    
    CGFloat fixedWidth = textView.frame.size.width;
    CGSize newSize = [textView sizeThatFits:CGSizeMake(fixedWidth, MAXFLOAT)];
    CGRect newFrame = textView.frame;
    newFrame.size = CGSizeMake(fmaxf(newSize.width, fixedWidth), newSize.height);
    
    [self jsq_adjustInputToolbarForComposerTextViewContentSizeChange:newFrame.size.height - oldSize.height];
    [self jsq_updateCollectionViewInsets];
    
    [self scrollToBottomAnimated:NO];
}

-(void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    //[FIRAnalytics setScreenName:@"chat_screen" screenClass:@"UIMessagesViewController"];
//    _moreMenuButton.enabled = [self willShowSomethingInMoreMenu];
    if(![ServicesManager sharedInstance].loginManager.isConnected){
        [self didLostConnection:nil];
    }
}

-(void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [UITools customizeAppAppearanceWithGlobalColor:[UITools defaultTintColor]];
    self.navigationController.navigationBar.barTintColor = [UITools defaultTintColor];
    
    // Mark the conversation as inactive
    // uncomment this to enable XEP-085 chat-state
    //[_conversationsManagerService setStatus:ConversationStatusInactive forConversation:_conversation];
    
    if(![ServicesManager sharedInstance].loginManager.isConnected){
        [self didReconnect:nil];
    }
    
    if(_updateBannerRefreshTimer){
        [_updateBannerRefreshTimer invalidate];
        _updateBannerRefreshTimer = nil;
    }
    
    if (_moreMenuActionSheet) {
        [_moreMenuActionSheet dismissViewControllerAnimated:NO completion:nil];
        _moreMenuActionSheet = nil;
    }
    
    if(_conversation && [_conversation.conversationId length] > 0 && self.inputToolbar.contentView.textView.text.length > 0){
        NSMutableDictionary *latestMessagesEntered = nil;
        NSDictionary * dic = [[NSUserDefaults standardUserDefaults] objectForKey:@"lastMessagesEntered"];
        if(dic){
            latestMessagesEntered = [NSMutableDictionary dictionaryWithDictionary:dic];
            [latestMessagesEntered setObject:self.inputToolbar.contentView.textView.text forKey:_conversation.conversationId];
            [[NSUserDefaults standardUserDefaults] setObject:latestMessagesEntered forKey:@"lastMessagesEntered"];
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(UIStatusBarStyle) preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

#pragma mark - Login manager notifications
-(void) didLostConnection:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didLostConnection:notification];
        });
        return;
    }
    
    if([self isViewLoaded]){
        // Move the UI down
        //self.topContentAdditionalInset += 60;
//        self.showLoadEarlierMessagesHeader = NO;
        self.inputToolbar.contentView.leftBarButtonItem.enabled = NO;
        isNetworkConnected = NO;
    }
}

-(void) didLogin:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didReconnect:notification];
        });
        return;
    }
    
    if([self isViewLoaded]){
        // Move the UI up
        //self.topContentAdditionalInset -= 60;
//        self.showLoadEarlierMessagesHeader = YES;
        self.inputToolbar.contentView.leftBarButtonItem.enabled = YES;
        isNetworkConnected = YES;
    }
}

-(void) didReconnect:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didReconnect:notification];
        });
        return;
    }
    
    if([self isViewLoaded]){
        // Move the UI up
        //self.topContentAdditionalInset -= 60;
//        self.showLoadEarlierMessagesHeader = YES;
        self.inputToolbar.contentView.leftBarButtonItem.enabled = YES;
        isNetworkConnected = YES;
    }
}

#pragma mark - Navigation
- (IBAction)showDetails:(UIBarButtonItem *)sender {
    [self parentViewController].title = @" ";
    if (_conversation.type == ConversationTypeUser || _conversation.type == ConversationTypeBot) {
        UIContactDetailsViewController* controller = (UIContactDetailsViewController*)[[UIStoryboardManager sharedInstance].contactsDetailsStoryBoard instantiateViewControllerWithIdentifier:@"contactDetailsViewControllerID"];
        controller.contact = (Contact *) _conversation.peer;
        controller.fromView = self;
        [self.navigationController pushViewController:controller animated:YES];
    } else if (_conversation.type == ConversationTypeRoom) {
        UIRoomDetailsViewController *controller = [[UIStoryboardManager sharedInstance].roomDetailsStoryBoard instantiateViewControllerWithIdentifier:@"roomDetailsViewControllerID"];
        controller.room = (Room *) _conversation.peer;
        controller.fromView = self;
        if(_parentNavigationController){
            [_parentNavigationController pushViewController:controller animated:YES];
        } else {
            [self.navigationController pushViewController:controller animated:YES];
        }
    }
}

#pragma mark - Audio conference actions
-(BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender {
    return NO;
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
//    if([[segue identifier] isEqualToString:@"joinMeetingPopoverSegueID"]){
//        JoinMeetingViewController *joinMeetingController = [segue destinationViewController];
//        joinMeetingController.room = (Room*)_conversation.peer;
//        joinMeetingController.popoverPresentationController.delegate = self;
//        joinMeetingController.delegate = self;
//        [self.inputToolbar.contentView.textView resignFirstResponder];
//    }
}

- (UIModalPresentationStyle) adaptivePresentationStyleForPresentationController: (UIPresentationController * ) controller {
    return UIModalPresentationNone;
}

-(void)popoverPresentationControllerDidDismissPopover:(UIPopoverPresentationController *)popoverPresentationController {
    NSLog(@"Did dissmiss popover");
}

#pragma mark -

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator {
    [coordinator animateAlongsideTransition:^(id<UIViewControllerTransitionCoordinatorContext> context)
     {} completion:^(id<UIViewControllerTransitionCoordinatorContext> context) {
         [self toggleDisableWhenOutOfTheRoom];
         [self updateBanner];
         [self adjustScrollToBottomButtonFrame];
         
         if(!self.searchMessageViewBar.hidden) {
             [self hideSearchMessageViewBar];
             [self showSearchMessageViewBarWithSearchField:!_searchMessageTextfield.hidden];
         }
//         if(_attachmentView.superview)
//             [self showAttachmentView];
//         if(!self.iconImageView.hidden) // Adjust icon position
//             self.iconImageView.frame = CGRectMake(self.statusLabel.frame.size.width/2-(self.iconImageView.frame.size.width/2), self.iconImageView.frame.origin.y, self.iconImageView.frame.size.width, self.iconImageView.frame.size.height);
     }];
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
}


-(void) toggleDisableWhenOutOfTheRoom {
    [self.inputToolbar setHidden:YES];
}

#pragma mark - Status banner
/*
 * Decide whether to show or no the top status banner
 * and to update it's content.
 */
-(void) updateBanner {
    if (!self.statusLabel)
        return;
    
    // Right now, enable the banner only for conversation of type user with non guest user.
    if (_conversation.type == ConversationTypeUser){
        
        Contact *contact = (Contact*) _conversation.peer;
//        self.iconImageView.hidden = YES;
        
        NSMutableString *textToDisplay = [[NSMutableString alloc] initWithString:@""];
        
        if(!contact.isPresenceSubscribed && ![ServicesManager sharedInstance].myUser.isGuest && ![_contactsManagerService isTheContactInMyNetwork:contact]) {
            if(!contact.isPresenceSubscribed && !contact.sentInvitation){
//                self.iconImageView.hidden = NO;
                [textToDisplay appendString:NSLocalizedString(@"Add to my network", nil)];
            } else if(contact.sentInvitation.status == InvitationStatusPending) {
                [textToDisplay appendString:NSLocalizedString(@"Invitation sent", nil)];
            }
            
//            if(!self.iconImageView.hidden)
//                self.statusLabel.text = [NSString stringWithFormat:@"\n%@", NSLocalizedString(textToDisplay, nil)];
//            else
                self.statusLabel.text = NSLocalizedString(textToDisplay,nil);
            
            self.statusLabel.backgroundColor = [UIColor grayColor];
        }
        
        else {
            NSString *presence = @"";
            
            if( contact.presence.presence != ContactPresenceAvailable ) {
                presence = [UITools getPresenceText:contact];
                [textToDisplay appendString: NSLocalizedString(presence,nil)];
                self.statusLabel.backgroundColor = [UITools colorForPresence:contact.presence];
            }
            
            if((contact.calendarPresence.presence != CalendarPresenceAvailable && contact.calendarPresence.presence != CalendarPresenceUnavailable) || contact.calendarPresence.automaticReply.isEnabled) {
                if( ![presence isEqualToString:@""] ) {
                    [textToDisplay appendString: @"\n"];
                } else {
                    self.statusLabel.backgroundColor = [UIColor darkGrayColor];
                }
                
                NSString *calendarPresence = @"";
                
                // Calendar Out of office
                if( contact.calendarPresence.automaticReply.isEnabled ) {
                    if( contact.calendarPresence.automaticReply.untilDate != nil ) {
                        calendarPresence = [NSString stringWithFormat:NSLocalizedString(@"Out of office until %@", nil), [UITools optimizedFormatForCalendarPresenceDate: contact.calendarPresence.automaticReply.untilDate]];
                    } else {
                        calendarPresence = [NSString stringWithFormat:NSLocalizedString(@"Out of office", nil)];
                    }
                }
                
                // Calendar presence
                else if( contact.calendarPresence.presence == CalendarPresenceBusy ) {
                    calendarPresence = [NSString stringWithFormat:NSLocalizedString(@"Appointment until %@", nil), [UITools optimizedFormatForCalendarPresenceDate: contact.calendarPresence.until]];
                } else if( contact.calendarPresence.presence == CalendarPresenceOutOfOffice ) {
                    calendarPresence = [NSString stringWithFormat:NSLocalizedString(@"Out of office until %@", nil), [UITools optimizedFormatForCalendarPresenceDate: contact.calendarPresence.until]];
                }
                
                [textToDisplay appendString: NSLocalizedString(calendarPresence,nil)];
            }
            
            self.statusLabel.text = textToDisplay;
        }
        
        if (contact.lastActivityDate) {
            _updateBannerRefreshTimer = [NSTimer scheduledTimerWithTimeInterval:D_MINUTE target:self selector:@selector(updateBanner) userInfo:nil repeats:NO];
        }
        
        if(!contact.isPresenceSubscribed) {
            UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTapOnStatusLabel)];
            tap.numberOfTapsRequired = 1;
            tap.numberOfTouchesRequired = 1;
            tap.cancelsTouchesInView = NO;
            [self.statusLabel addGestureRecognizer:tap];
            self.statusLabel.userInteractionEnabled = YES;
        }
        
        // Show the banner if a message has been set
        if( [self.statusLabel.text isEqualToString:@""] ) {
            self.statusLabel.hidden = YES;
        } else {
            self.statusLabel.hidden = NO;
        }
        
        // Refresh layout - needed otherwise frame size calculation are not up to date
        [self.view layoutIfNeeded];
        // Use the label height as the offset of the JSQMessagesViewController
        [self calculateTopContentAdditionalInset];
    } else {
        self.statusLabel.hidden = YES;
        [self calculateTopContentAdditionalInset];
        if(self.statusLabel.gestureRecognizers.count > 0)
            [self.statusLabel.gestureRecognizers enumerateObjectsUsingBlock:^(__kindof UIGestureRecognizer *gesture, NSUInteger idx, BOOL * stop) {
                [self.statusLabel removeGestureRecognizer:gesture];
            }];
        self.statusLabel.userInteractionEnabled = NO;
    }
    
//    if(_conversation.isMuted)
//        [self showNotificationDisabledView];
//    else
//        [self hideNotificationDisabledView];
}

-(void) didTapOnStatusLabel {
    NSLog(@"didTapOnStatusLabel");
    
    // Right now, enable the banner only for conversation of type user.
    if (_conversation.type != ConversationTypeUser)
        return;
    
    Contact *contact = (Contact*) _conversation.peer;
    
    if(!contact.isPresenceSubscribed && !contact.sentInvitation){
        [_contactsManagerService inviteContact:contact];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self updateBanner];
        });
    }
    
    // We must update the more button status in case of changes of one of his conditions
//    _moreMenuButton.enabled = [self willShowSomethingInMoreMenu];
}

-(void) calculateTopContentAdditionalInset {
    NSInteger value = 0;
    if(!self.searchMessageViewBar.hidden){
        value += self.searchMessageViewBar.frame.size.height;
    }
    if(!self.statusLabel.hidden){
        value += self.statusLabel.frame.size.height;
    }
    if ([ServicesManager sharedInstance].myUser.isGuest)
        value += kGuestModeHeight;
    self.topContentAdditionalInset = value;
}
- (IBAction)unmuteConversation:(UIButton *)sender {
    [[ServicesManager sharedInstance].conversationsManagerService unmuteConversation:_conversation];
}

#pragma mark - JSQMessagesViewController method overrides

- (void)didPressSendButton:(UIButton *)button withMessageText:(NSString *)text senderId:(NSString *)senderId senderDisplayName:(NSString *)senderDisplayName date:(NSDate *)date {
    NSLog(@"Did press send button");
    //TODO: return back to real conversation
}

// We override the original method to use our own typing indicator
// Because we want an avatar next to the indicator to see who's typing (particulary in rooms)
- (UICollectionReusableView *)collectionView:(JSQMessagesCollectionView *)collectionView
           viewForSupplementaryElementOfKind:(NSString *)kind
                                 atIndexPath:(NSIndexPath *)indexPath
{
    if (self.showTypingIndicator && [kind isEqualToString:UICollectionElementKindSectionFooter]) {
        JSQMessagesTypingIndicatorFooterViewWithAvatar *footer = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:[JSQMessagesTypingIndicatorFooterViewWithAvatar footerReuseIdentifier] forIndexPath:indexPath];
    
        NSMutableArray<UIImage *> *avatars = [NSMutableArray new];
        for (Contact *contact in _typingContacts) {
            id image = [_contactAvatarDict objectForKey:contact.jid].avatarImage;
            if (image != nil)
                [avatars addObject:image];
        }
        [footer configureWithAvatar:avatars
                      ellipsisColor:[collectionView typingIndicatorEllipsisColor]
                 messageBubbleColor:[collectionView typingIndicatorMessageBubbleColor]
                  forCollectionView:collectionView];
    
        return footer;
    }
    else if (self.showLoadEarlierMessagesHeader && [kind isEqualToString:UICollectionElementKindSectionHeader]) {
        JSQMessagesAutoLoadEarlierHeaderView *header = [collectionView dequeueReusableSupplementaryViewOfKind: UICollectionElementKindSectionHeader withReuseIdentifier: [JSQMessagesAutoLoadEarlierHeaderView headerReuseIdentifier] forIndexPath:indexPath];
        return header;
    }

    return nil;
}


#pragma mark - JSQMessagesComposerTextViewPasteDelegate methods
- (BOOL)composerTextView:(JSQMessagesComposerTextView *)textView shouldPasteWithSender:(id)sender {
    //    if ([UIPasteboard generalPasteboard].image) {
    //        // If there's an image in the pasteboard, construct a media item with that image and `send` it.
    //        JSQPhotoMediaItem *item = [[JSQPhotoMediaItem alloc] initWithImage:[UIPasteboard generalPasteboard].image];
    //        JSQMessage *message = [[JSQMessage alloc] initWithSenderId:self.senderId senderDisplayName:self.senderDisplayName date:[NSDate date] media:item];
    ////        [self.demoData.messages addObject:message];
    //        [self finishSendingMessage];
    //        return NO;
    //    }
    return YES;
}

#pragma mark - Check for textview changes
// This enables XEP-085 chat-state
// triggered by UITextViewTextDidChangeNotification
-(void) UITextViewDidChange:(NSNotification *)notification {
    
    UITextView *textView = notification.object;

    if (textView == self.inputToolbar.contentView.textView) {
        [self updateConversationStatusAndToggleSendButton];
    }
}

-(void) updateConversationStatusAndToggleSendButton {
//    NSString *composed = [self.inputToolbar.contentView.textView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
//    if ([composed length]) {
//        [_conversationsManagerService setStatus:ConversationStatusComposing forConversation:_conversation];
//    } else {
//        [_conversationsManagerService setStatus:ConversationStatusActive forConversation:_conversation];
//    }
//
    BOOL hasText = [self.inputToolbar.contentView.textView hasText]; //  || [_attachmentView superview]
    if (self.inputToolbar.sendButtonOnRight) {
        self.inputToolbar.contentView.rightBarButtonItem.enabled = hasText;
    }
    else {
        self.inputToolbar.contentView.leftBarButtonItem.enabled = hasText;
    }
}

#pragma mark - facilitors for browser cache and grouped messages good index finder
-(NSIndexPath*) rowIndexFromBrowserCacheIndex:(NSInteger)idx {
    return [NSIndexPath indexPathForRow:[_messages count]-idx-1 inSection:0];
}

-(NSIndexPath*) rowIndexFromGroupedMessagesIndex:(NSInteger)idx {
    return [NSIndexPath indexPathForRow:[_messages count]-idx-1 inSection:0];
}

-(NSInteger) groupedMessagesIndexForRowIndex:(NSIndexPath*)idx {
    return [_messages count]-idx.item-1;
}

-(NSInteger) insertMessageAtProperIndex:(GroupedMessages *) groupedMessages {
    NSInteger idxForInsertion = NSNotFound;
    if ([_messages indexOfObject:groupedMessages]==NSNotFound) {
        idxForInsertion = [self properIndexForItem:groupedMessages];
        
        if (groupedMessages.allGroupedMessages.count) {
            [_messages insertObject:groupedMessages atIndex:idxForInsertion];
        }
        
    }
    return idxForInsertion;
}

-(NSInteger) properIndexForItem:(GroupedMessages *) groupedMessages {
    __block NSInteger insertIndex = [_messages count];
    // if we don't find a proper place in the list, this means we should add it at the end.
    [_messages enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        GroupedMessages *item = obj;
        // if item.date is not later than aNewItem.date
        NSDate *itemDate = item.date;
        if (item.lastMessage.isOutgoing && [item.lastMessage deliveryDateForState:MessageDeliveryStateDelivered]) {
            itemDate = [item.lastMessage deliveryDateForState:MessageDeliveryStateDelivered];
        }
        NSDate *messageDate = groupedMessages.date;
        if (groupedMessages.lastMessage.isOutgoing && [groupedMessages.lastMessage deliveryDateForState:MessageDeliveryStateDelivered]) {
            messageDate = [groupedMessages.lastMessage deliveryDateForState:MessageDeliveryStateDelivered];
        }
        
        if (itemDate!=nil && ![item isEqual:groupedMessages] && [itemDate compare:messageDate]!=NSOrderedDescending) {
            *stop = YES;
            insertIndex = idx;
        }
    }];
    return insertIndex;
}


#pragma mark - JSQMessages CollectionView DataSource

- (id<JSQMessageData>)collectionView:(JSQMessagesCollectionView *)collectionView messageDataForItemAtIndexPath:(NSIndexPath *)indexPath {
    return [_messages objectAtIndex:[self groupedMessagesIndexForRowIndex:indexPath]];
}

- (void)collectionView:(JSQMessagesCollectionView *)collectionView didDeleteMessageAtIndexPath:(NSIndexPath *)indexPath {
    //    [self.demoData.messages removeObjectAtIndex:indexPath.item];
}

- (id<JSQMessageBubbleImageDataSource>)collectionView:(JSQMessagesCollectionView *)collectionView messageBubbleImageDataForItemAtIndexPath:(NSIndexPath *)indexPath {
    GroupedMessages *message = [_messages objectAtIndex:[self groupedMessagesIndexForRowIndex:indexPath]];
    
    if ([message.senderId isEqualToString:self.senderId]) {
        if([self isTheNextMessageHasSameSender:indexPath]){
            _bubbleFactory = [[JSQMessagesBubbleImageFactory alloc] initWithBubbleImage:[UIImage jsq_bubbleRegularTaillessImage] capInsets:UIEdgeInsetsZero];
            _outgoingBubbleImageData = [_bubbleFactory outgoingMessagesBubbleImageWithColor:[UIColor jsq_messageBubbleLightGrayColor]];
            return _outgoingBubbleImageData;
        }
        else {
            _bubbleFactory = [[JSQMessagesBubbleImageFactory alloc] initWithBubbleImage:[UIImage jsq_bubbleRainbowImage] capInsets:UIEdgeInsetsZero];
            _outgoingBubbleImageData = [_bubbleFactory outgoingMessagesBubbleImageWithColor:[UIColor jsq_messageBubbleLightGrayColor]];
            return _outgoingBubbleImageData;
        }
        
    }
    
    if (message.senderId) {
        UIColor *bubbleColor = [UITools defaultTintColor];
        
        if ([_conversation.peer isKindOfClass:[Room class]] || [[NSUserDefaults standardUserDefaults] boolForKey:@"randomColorsInPeerToPeerConversations"])
            bubbleColor = [UITools colorForString:message.lastMessage.peer.displayName];
        
        JSQMessagesBubbleImage *incomingBubbleImageData;
        if([self isTheNextMessageHasSameSender:indexPath]){
            _bubbleFactory = [[JSQMessagesBubbleImageFactory alloc] initWithBubbleImage:[UIImage jsq_bubbleRegularTaillessImage] capInsets:UIEdgeInsetsZero];
            incomingBubbleImageData = [_bubbleFactory incomingMessagesBubbleImageWithColor:bubbleColor];
            return incomingBubbleImageData;
        }
        else {
            _bubbleFactory = [[JSQMessagesBubbleImageFactory alloc] initWithBubbleImage:[UIImage jsq_bubbleRainbowImage] capInsets:UIEdgeInsetsZero];
            incomingBubbleImageData = [_bubbleFactory incomingMessagesBubbleImageWithColor:bubbleColor];
            return incomingBubbleImageData;
        }
    }
    return nil;
}

- (id<JSQMessageAvatarImageDataSource>)collectionView:(JSQMessagesCollectionView *)collectionView avatarImageDataForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    GroupedMessages *message = [_messages objectAtIndex:[self groupedMessagesIndexForRowIndex:indexPath]];
    
    if ([message.senderId isEqualToString:self.senderId]) {
        return _meAvatar;
    }
    
    if (message.senderId)
        return [_contactAvatarDict objectForKey:message.senderId];
    return nil;
}

// Not used, height is 0
- (NSAttributedString *)collectionView:(JSQMessagesCollectionView *)collectionView attributedTextForCellTopLabelAtIndexPath:(NSIndexPath *)indexPath {
    GroupedMessages *message = [_messages objectAtIndex:[self groupedMessagesIndexForRowIndex:indexPath]];
    
    if(indexPath.row % 5 == 0){
        return [[JSQMessagesTimestampFormatter sharedFormatter] attributedTimestampForDate:message.date];
    }
    return nil;
}

/**
 *  Show the remote contact firstname, only if this is a "Room" conversation (i.e. with multiple persons)
 */
- (NSAttributedString *)collectionView:(JSQMessagesCollectionView *)collectionView attributedTextForMessageBubbleTopLabelAtIndexPath:(NSIndexPath *)indexPath {
    /*
     if (_conversation.type == ConversationTypeRoom) {
     GroupedMessages *message = [_messages objectAtIndex:[self groupedMessagesIndexForRowIndex:indexPath]];
     if (!message.lastMessage.isOutgoing) {
     Peer *peer = message.lastMessage.peer;
     
     if ([peer isKindOfClass:[Contact class]]) {
     Contact *contact = (Contact *) peer;
     return [[NSAttributedString alloc] initWithString:contact.displayName attributes:@{NSFontAttributeName:[UIFont fontWithName:[UITools defaultFontName] size:12]}];
     }
     }
     }
     */
    return nil;
}

/*
 *  The small text under the messages.
 *  We use it to show if the message has been delivered and seen
 */
- (NSAttributedString *)collectionView:(JSQMessagesCollectionView *)collectionView attributedTextForCellBottomLabelAtIndexPath:(NSIndexPath *)indexPath {
    GroupedMessages *message = [_messages objectAtIndex:[self groupedMessagesIndexForRowIndex:indexPath]];
    if (![message.senderId isEqualToString:self.senderId]) {
        return nil;
    }
    if (!_tappedIndexPath || [_tappedIndexPath compare:indexPath] != NSOrderedSame) {
        return nil;
    }
    
    NSMutableAttributedString *string = [NSMutableAttributedString new];
    
    // TODO: remove this once ACK mecanism will be defined for Rooms
    MessageDeliveryState state = message.lastMessage.state;
    if (_conversation.type == ConversationTypeRoom) {
        // We dont show Received and Read in case of Room conv.
        if (state == MessageDeliveryStateReceived || state == MessageDeliveryStateRead) {
            state = MessageDeliveryStateDelivered;
        }
    }
    // --
    
    switch(state) {
        case MessageDeliveryStateSent: {
            [string.mutableString setString:NSLocalizedString(@"Sending", nil)];
            break;
        }
        case MessageDeliveryStateDelivered: {
            NSDate *timestamp = [message.lastMessage deliveryDateForState:MessageDeliveryStateDelivered];
            if (!timestamp) {
                [string.mutableString setString:NSLocalizedString(@"Sent", nil)];
                break;
            }
            
            if([timestamp isToday]) {
                [string.mutableString setString:[NSString stringWithFormat:NSLocalizedString(@"Sent at %@", nil), [UITools formatTimeForDate:timestamp]]];
            } else {
                [string.mutableString setString:[NSString stringWithFormat:NSLocalizedString(@"Sent on %@ at %@", nil), [UITools shortFormatForDate:timestamp], [UITools formatTimeForDate:timestamp]]];
            }
            break;
        }
        case MessageDeliveryStateReceived: {
            NSDate *timestamp = [message.lastMessage deliveryDateForState:MessageDeliveryStateReceived];
            if (!timestamp) {
                [string.mutableString setString:NSLocalizedString(@"Received", nil)];
                break;
            }
            
            if([timestamp isToday]) {
                [string.mutableString setString:[NSString stringWithFormat:NSLocalizedString(@"Received at %@", nil), [UITools formatTimeForDate:timestamp]]];
            } else {
                [string.mutableString setString:[NSString stringWithFormat:NSLocalizedString(@"Received on %@ at %@", nil), [UITools shortFormatForDate:timestamp], [UITools formatTimeForDate:timestamp]]];
            }
            break;
        }
        case MessageDeliveryStateRead: {
            NSDate *timestamp = [message.lastMessage deliveryDateForState:MessageDeliveryStateRead];
            if (!timestamp) {
                [string.mutableString setString:NSLocalizedString(@"Read", nil)];
                break;
            }
            
            if([timestamp isToday]) {
                [string.mutableString setString:[NSString stringWithFormat:NSLocalizedString(@"Read at %@", nil), [UITools formatTimeForDate:timestamp]]];
            } else {
                [string.mutableString setString:[NSString stringWithFormat:NSLocalizedString(@"Read on %@ at %@", nil), [UITools shortFormatForDate:timestamp], [UITools formatTimeForDate:timestamp]]];
            }
            break;
        }
        case MessageDeliveryStateFailed: {
            [string.mutableString setString:NSLocalizedString(@"Failed", nil)];
            break;
        }
    }
    
    [string setAttributes:@{NSFontAttributeName:[UIFont fontWithName:[UITools defaultFontName]  size:12]} range:NSMakeRange(0, string.length)];
    return string;
}

-(MBProgressHUD *) showHUDWithMessage:(NSString *) message mode:(MBProgressHUDMode) mode dismissCompletionBlock:(void (^)()) dismissCompletion {
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = mode;
    hud.removeFromSuperViewOnHide = YES;
    hud.labelText = message;
    [hud setLabelFont:[UIFont fontWithName:[UITools defaultFontName] size:14.0f]];
    if(mode == MBProgressHUDModeCustomView){
        hud.mode = MBProgressHUDModeCustomView;
        hud.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Checkmark"]];
        [hud showAnimated:YES whileExecutingBlock:^{sleep(1);} completionBlock:dismissCompletion];
    }
    return hud;
}

-(MBProgressHUD *) showHUDWithErrorMessage:(NSString *) message mode:(MBProgressHUDMode) mode dismissCompletionBlock:(void (^)()) dismissCompletion {
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = mode;
    hud.removeFromSuperViewOnHide = YES;
    hud.labelText = message;
    [hud setLabelFont:[UIFont fontWithName:[UITools defaultFontName] size:14.0f]];
    if(mode == MBProgressHUDModeCustomView){
        hud.mode = MBProgressHUDModeCustomView;
        hud.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"error"]];
        [hud showAnimated:YES whileExecutingBlock:^{sleep(1);} completionBlock:dismissCompletion];
    }
    return hud;
}


#pragma mark - Responding to UICollectionViewDelegate protocol
- (void)collectionView:(UICollectionView *)collectionView willDisplaySupplementaryView:(UICollectionReusableView *)view
        forElementKind:(NSString *)elementKind atIndexPath:(NSIndexPath *)indexPath {
    self.stopShowingHeaderView = NO;
//    if([elementKind isEqualToString:UICollectionElementKindSectionHeader] &&
//       [view isKindOfClass:[JSQMessagesAutoLoadEarlierHeaderView class]]) {
//        if(!_isRetrievingNextPage){
//
//            NSLog(@"Load earlier messages!");
//            //[self loadMessages];
//        } else {
//            NSLog(@"Already retrieving a page");
//        }
//    }
}

- (void)collectionView:(UICollectionView *)collectionView didEndDisplayingSupplementaryView:(UICollectionReusableView *)view forElementOfKind:(NSString *)elementKind atIndexPath:(NSIndexPath *)indexPath {
    self.stopShowingHeaderView = YES;
}

- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath {
//    if(self.automaticallyScrollsToMostRecentMessage)
//        [_conversationsManagerService markAsReadByMeAllMessageForConversation:_conversation];
}

#pragma mark - Adjusting cell label heights
- (CGFloat)collectionView:(JSQMessagesCollectionView *)collectionView layout:(JSQMessagesCollectionViewFlowLayout *)collectionViewLayout heightForCellBottomLabelAtIndexPath:(NSIndexPath *)indexPath {
    if ([self collectionView:collectionView attributedTextForCellBottomLabelAtIndexPath:indexPath]) {
        return 12.f;
    }
    return 0.0f;
}

- (CGFloat)collectionView:(JSQMessagesCollectionView *)collectionView
                   layout:(JSQMessagesCollectionViewFlowLayout *)collectionViewLayout heightForCellTopLabelAtIndexPath:(NSIndexPath *)indexPath {
    return 0.0f;
}

- (CGFloat)collectionView:(JSQMessagesCollectionView *)collectionView layout:(JSQMessagesCollectionViewFlowLayout *)collectionViewLayout heightForMessageBubbleTopLabelAtIndexPath:(NSIndexPath *)indexPath {
    
    if([self isThePrevoiusMessageHasSameSender:indexPath]){
        return kJSQMessagesCollectionViewCellLabelHeightGroup;
    }
    else {
        return kJSQMessagesCollectionViewCellLabelHeightDefault;
    }
}

#pragma mark - UICollectionView DataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [_messages count];
}

/**
 * return the strechable background image for group chat events
 */
-(UIImage *)eventBubbleCellImage {
    UIImage *bubbleImage = [[UIImage jsq_bubbleRegularStrokedTaillessImage] jsq_imageMaskedWithColor:[UIColor lightGrayColor]];
    // make image stretchable from center point
    CGPoint center = CGPointMake(bubbleImage.size.width / 2.0f, bubbleImage.size.height / 2.0f);
    UIEdgeInsets capInsets = UIEdgeInsetsMake(center.y, center.x, center.y, center.x);
    bubbleImage = [bubbleImage resizableImageWithCapInsets:capInsets resizingMode:UIImageResizingModeStretch];
    
    return bubbleImage;
}

- (JSQGroupChatEventCell *)groupChatEventCellFor:(JSQMessagesCollectionView *)collectionView atIndexPath:(NSIndexPath *)indexPath message:(Message *)message{
    id<JSQMessageData> messageItem = [collectionView.dataSource collectionView:collectionView messageDataForItemAtIndexPath:indexPath];
    JSQGroupChatEventCell *groupChatEventCell = (JSQGroupChatEventCell *)[collectionView dequeueReusableCellWithReuseIdentifier: [JSQGroupChatEventCell cellReuseIdentifier] forIndexPath:indexPath];
    
    groupChatEventCell.messageBubbleImageView.image = self.groupChatEventBubbleImage;
    
    [groupChatEventCell setTimestamp: messageItem.date];
    
    if (message.groupChatEventPeer.jid != nil) {
        id<JSQMessageAvatarImageDataSource> avatarImageDataSource = [_contactAvatarDict objectForKey:message.groupChatEventPeer.jid];
        
        if (avatarImageDataSource != nil) {
            
            UIImage *avatarImage = [avatarImageDataSource avatarImage];
            if (avatarImage == nil) {
                groupChatEventCell.avatarImageView.image = [avatarImageDataSource avatarPlaceholderImage];
                groupChatEventCell.avatarImageView.highlightedImage = nil;
            }
            else {
                groupChatEventCell.avatarImageView.image = avatarImage;
                groupChatEventCell.avatarImageView.highlightedImage = [avatarImageDataSource avatarHighlightedImage];
            }
        }
    }
    
    
    if([message.groupChatEventPeer isKindOfClass:[Contact class]]){
        [groupChatEventCell setCompanyName:((Contact *)message.groupChatEventPeer).companyName];
    } else {
        [groupChatEventCell setCompanyName:@""];
    }
    
    [groupChatEventCell setMessage: message.bodyForGroupChatEvent];
    groupChatEventCell.textView.textColor = [UIColor lightGrayColor];
    
    return groupChatEventCell;
}

- (JSQWebRTCEventsCell *)callRecordingEventCellFor:(JSQMessagesCollectionView *)collectionView atIndexPath:(NSIndexPath *)indexPath message:(Message *)message {
    JSQWebRTCEventsCell *webRTCEventCell = (JSQWebRTCEventsCell *)[collectionView dequeueReusableCellWithReuseIdentifier: [JSQWebRTCEventsCell cellReuseIdentifier] forIndexPath:indexPath];
    
    webRTCEventCell.messageBubbleImageView.image = self.groupChatEventBubbleImage;
    
    id<JSQMessageAvatarImageDataSource> avatarImageDataSource = [_contactAvatarDict objectForKey:message.peer.jid];
    
    if (avatarImageDataSource != nil) {
        UIImage *avatarImage = [avatarImageDataSource avatarImage];
        if (avatarImage == nil) {
            webRTCEventCell.avatarImageView.image = [avatarImageDataSource avatarPlaceholderImage];
            webRTCEventCell.avatarImageView.highlightedImage = nil;
        } else {
            webRTCEventCell.avatarImageView.image = avatarImage;
            webRTCEventCell.avatarImageView.highlightedImage = [avatarImageDataSource avatarHighlightedImage];
        }
    }
    
    [webRTCEventCell setTimestamp: message.date];
    [webRTCEventCell setEventTypeText:@""];
    
    switch (message.type) {
        case MessageTypeCallRecordingStart:{
            [webRTCEventCell setMessage:[NSString stringWithFormat:NSLocalizedString(@"%@ started to record you", nil), message.peer.displayName]];
            [webRTCEventCell setEventTypeColor:[UIColor lightGrayColor]];
            break;
        }
        case MessageTypeCallRecordingStop:{
            [webRTCEventCell setMessage:[NSString stringWithFormat:NSLocalizedString(@"%@ stoped to record you", nil), message.peer.displayName]];
            [webRTCEventCell setEventTypeColor:[UIColor lightGrayColor]];
            break;
        }
        default:{
            break;
        }
    }
    
    return webRTCEventCell;
}

- (JSQWebRTCEventsCell *)webRTCEventCellFor:(JSQMessagesCollectionView *)collectionView atIndexPath:(NSIndexPath *)indexPath message:(Message *)message {
    JSQWebRTCEventsCell *webRTCEventCell = (JSQWebRTCEventsCell *)[collectionView dequeueReusableCellWithReuseIdentifier: [JSQWebRTCEventsCell cellReuseIdentifier] forIndexPath:indexPath];
    
    webRTCEventCell.messageBubbleImageView.image = self.groupChatEventBubbleImage;
    [webRTCEventCell setTimestamp:message.callLog.date];
    if(message.isOutgoing){
        // Avatar is a headset
        webRTCEventCell.avatarImageView.image = [UIImage imageNamed:@"headset"];
        webRTCEventCell.avatarImageView.tintColor = [UITools defaultTintColor];
    } else {
        id<JSQMessageAvatarImageDataSource> avatarImageDataSource = [_contactAvatarDict objectForKey:message.peer.jid];
        
        if (avatarImageDataSource != nil) {
            UIImage *avatarImage = [avatarImageDataSource avatarImage];
            if (avatarImage == nil) {
                webRTCEventCell.avatarImageView.image = [avatarImageDataSource avatarPlaceholderImage];
                webRTCEventCell.avatarImageView.highlightedImage = nil;
            } else {
                webRTCEventCell.avatarImageView.image = avatarImage;
                webRTCEventCell.avatarImageView.highlightedImage = [avatarImageDataSource avatarHighlightedImage];
            }
        }
    }
    
    
    switch (message.callLog.state) {
        case CallLogStateMissed:{
            if(message.isOutgoing){
                [webRTCEventCell setEventTypeText:NSLocalizedString(@"Canceled call", nil)];
                [webRTCEventCell setEventTypeColor:[UIColor lightGrayColor]];
            } else {
                [webRTCEventCell setEventTypeText:NSLocalizedString(@"Missed call", nil)];
                [webRTCEventCell setEventTypeColor:[UITools notificationBadgeColor]];
            }
            break;
        }
        case CallLogStateAnswered:{
            NSString *duration = message.callLog.duration;
            NSString *durationReadable = [UITools timeFormatConvertToSeconds:duration zeroFormattingBehaviorunitsStyle:NSDateComponentsFormatterZeroFormattingBehaviorDropAll unitsStyle:NSDateComponentsFormatterUnitsStyleAbbreviated];
            [webRTCEventCell setEventTypeText:[NSString stringWithFormat:NSLocalizedString(@"Duration %@", nil), durationReadable]];
            [webRTCEventCell setEventTypeColor:[UIColor lightGrayColor]];
            break;
        }
        case CallLogStateUnknown:
        default:{
            [webRTCEventCell setEventTypeText:@""];
            break;
        }
    }
    
    [webRTCEventCell setMessage:[message body]];
    
    return webRTCEventCell;
}

-(JSQRainbowOutgoingMessagesCollectionViewCell *) outgoingCellFor:(JSQMessagesCollectionView *) collectionView atIndexPath:(NSIndexPath *) indexPath {
    GroupedMessages *message = [_messages objectAtIndex:[self groupedMessagesIndexForRowIndex:indexPath]];
    
    JSQRainbowOutgoingMessagesCollectionViewCell *outgoingCell = (JSQRainbowOutgoingMessagesCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier: self.outgoingCellIdentifier forIndexPath:indexPath];
    
    Message *lastMessage = message.lastMessage;
    
    // We need the attachment to configure correclty the cell
    outgoingCell.attachment = lastMessage.attachment;
    outgoingCell.progressView.tag = indexPath.row;
    
     outgoingCell.textView.textColor = [UIColor blackColor];
    
    [self configureCell:outgoingCell forCollectionView:collectionView indexPath:indexPath];
 
    // The timestamp INSIDE the message is always the delivered timestamp.
    // The timestamp UNDER the message (once clicked) is the current status timestamp
    [outgoingCell setTimestamp: [lastMessage deliveryDateForState:MessageDeliveryStateDelivered]];
    
    
    // Hide status image for file transfer message and webrtc
    if(lastMessage.type == MessageTypeWebRTC || lastMessage.type == MessageTypeFileTransfer){
        outgoingCell.status.hidden = YES;
    } else {
        outgoingCell.status.hidden = NO;
    }
    
    // TODO: remove this once ACK mecanism will be defined for Rooms
    MessageDeliveryState state = lastMessage.state;
    if (_conversation.type == ConversationTypeRoom) {
        // We dont show Received and Read in case of Room conv.
        if (state == MessageDeliveryStateReceived || state == MessageDeliveryStateRead) {
            state = MessageDeliveryStateDelivered;
        }
    }
    // --
    
    switch(state) {
        case MessageDeliveryStateSent: {
            outgoingCell.status.image = [UIImage imageNamed:@"message_sent"];
            [outgoingCell.status setTintColor:[UIColor lightGrayColor]];
            [outgoingCell setTimestamp: [lastMessage deliveryDateForState:state]];
            break;
        }
        case MessageDeliveryStateDelivered: {
            outgoingCell.status.image = [UIImage imageNamed:@"message_received_server"];
            [outgoingCell.status setTintColor:[UIColor lightGrayColor]];
            break;
        }
        case MessageDeliveryStateReceived: {
            outgoingCell.status.image = [UIImage imageNamed:@"message_received_read_client"];
            [outgoingCell.status setTintColor:[UIColor lightGrayColor]];
            break;
        }
        case MessageDeliveryStateRead: {
            outgoingCell.status.image = [UIImage imageNamed:@"message_received_read_client"];
            [outgoingCell.status setTintColor:[UITools defaultTintColor]];
            break;
        }
        case MessageDeliveryStateFailed: {
            outgoingCell.status.image = [UIImage imageNamed:@"message_ko"];
            [outgoingCell.status setTintColor:[UIColor redColor]];
            break;
        }
    }
    
    if (lastMessage.attachment != nil && lastMessage.attachment.value < 1.0) {
        
        if (( state == MessageDeliveryStateSent) || ([_filesDownloading containsObject:outgoingCell.attachment] )) {
            outgoingCell.progressView.hidden = NO;
            outgoingCell.cellDateLabel.hidden = YES;
            [outgoingCell setProgressViewWithValue:lastMessage.attachment.value];
        }
        else{
            outgoingCell.progressView.hidden = YES;
            outgoingCell.cellDateLabel.hidden = NO;
        }
    }
    else{
        outgoingCell.progressView.hidden = YES;
        outgoingCell.cellDateLabel.hidden = NO;
        
    }
    
    return outgoingCell;
}

-(JSQRainbowIncomingMessagesCollectionViewCell *) incomingCellFor:(JSQMessagesCollectionView *) collectionView atIndexPath:(NSIndexPath *) indexPath {
    GroupedMessages *message = [_messages objectAtIndex:[self groupedMessagesIndexForRowIndex:indexPath]];
    
    JSQRainbowIncomingMessagesCollectionViewCell *incomingCell = (JSQRainbowIncomingMessagesCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier: self.incomingCellIdentifier forIndexPath:indexPath];
    Message *lastMessage = message.lastMessage;
    incomingCell.attachment = lastMessage.attachment;
    _needsAvatar = YES;
    if (_messages.count > 0) {
        if ([self isTheNextMessageHasSameSender:indexPath]) {
            _needsAvatar = NO;
        }
    }
    
     incomingCell.textView.textColor = [UIColor whiteColor];
    
    [self configureCell:incomingCell forCollectionView:collectionView indexPath:indexPath];
    
    [incomingCell setTimestamp: message.lastMessage.date];
    incomingCell.progressView.tag = indexPath.row;
    
    if (lastMessage.attachment != nil && lastMessage.attachment.value < 1.0 && [_filesDownloading containsObject:incomingCell.attachment]) {
        
        incomingCell.progressView.hidden = NO;
        incomingCell.cellDateLabel.hidden = YES;
        [incomingCell setProgressViewWithValue:lastMessage.attachment.value];
        
    }
    else{
        
        incomingCell.cellDateLabel.hidden = NO;
        incomingCell.progressView.hidden = YES;
    }
    
    return incomingCell;
}

-(JSQRainbowRoomIncomingMessagesCollectionViewCell *) roomIncomingCellFor:(JSQMessagesCollectionView *) collectionView atIndexPath:(NSIndexPath *) indexPath {
    GroupedMessages *message = [_messages objectAtIndex:[self groupedMessagesIndexForRowIndex:indexPath]];
    
    JSQRainbowRoomIncomingMessagesCollectionViewCell *roomIncomingCell = (JSQRainbowRoomIncomingMessagesCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier: self.incomingCellIdentifier forIndexPath:indexPath];
    Message *lastMessage = message.lastMessage;
    roomIncomingCell.attachment = lastMessage.attachment;
    _needsAvatar = YES;
    if (_messages.count > 0) {
        GroupedMessages *currentMessageGroup = [_messages objectAtIndex:_messages.count-indexPath.item-1];
        Message * currentMessage = currentMessageGroup.lastMessage;
        if (_messages.count - indexPath.item  >= 2) {
            GroupedMessages *nextMessageGroup = [_messages objectAtIndex:_messages.count - indexPath.item - 2];
            Message * nextMessage = nextMessageGroup.lastMessage;
            if([currentMessage.senderId isEqualToString:nextMessage.senderId] && !currentMessage.callLog && !nextMessage.callLog){
                _needsAvatar = NO;
            }
        }
    }
    
    roomIncomingCell.textView.textColor = [UIColor whiteColor];
    
    [self configureCell:roomIncomingCell forCollectionView:collectionView indexPath:indexPath];
    
    [roomIncomingCell setTimestamp: message.lastMessage.date];
    
    if (!message.lastMessage.isOutgoing) {
        Peer *peer = message.lastMessage.peer;
        if ([peer isKindOfClass:[Contact class]]) {
            Contact *contact = (Contact *) peer;
            [roomIncomingCell setUserName:contact.displayBestInfo];
        }
    }
    
    roomIncomingCell.progressView.tag = indexPath.row;
    
    if (lastMessage.attachment != nil && lastMessage.attachment.value < 1.0 && [_filesDownloading containsObject:roomIncomingCell.attachment]) {
        roomIncomingCell.progressView.hidden = NO;
        roomIncomingCell.cellDateLabel.hidden = YES;
        [roomIncomingCell setProgressViewWithValue:lastMessage.attachment.value];
        
    }
    else{
        roomIncomingCell.progressView.hidden = YES;
        roomIncomingCell.cellDateLabel.hidden = NO;
    }
    
    return roomIncomingCell;
}


-(void) configureCell:(JSQMessagesCollectionViewCell *) cell forCollectionView:(JSQMessagesCollectionView *) collectionView indexPath:(NSIndexPath *) indexPath {
    id<JSQMessageData> messageItem = [collectionView.dataSource collectionView:collectionView messageDataForItemAtIndexPath:indexPath];
    NSParameterAssert(messageItem != nil);
    BOOL isOutgoingMessage = [self isOutgoingMessage:messageItem];
    BOOL isMediaMessage = [((GroupedMessages*)messageItem) isMediaMessage];
    
    cell.delegate = collectionView;
    
    if([messageItem isKindOfClass:[GroupedMessages class]]){
        GroupedMessages *message = (GroupedMessages *)messageItem;
        Message *lastMessage = message.lastMessage;
        cell.textView.hidden = NO;
        if ([[messageItem text] isEqualToString:lastMessage.attachment.fileName]) {
            cell.textView.hidden = YES;
        }
        
        if (lastMessage.imageUrl) {
            if (lastMessage.staticGifUrl) {
                if ([cell isKindOfClass:[JSQRainbowMessagesCollectionViewCell class]]) {
                    JSQRainbowMessagesCollectionViewCell *rainbowCell = (JSQRainbowMessagesCollectionViewCell *)cell;
                    BOOL autoAnimateSetting = [[NSUserDefaults standardUserDefaults] boolForKey:@"automaticallyPlayGif"];
                    rainbowCell.animating = autoAnimateSetting;
                    cell.mediaView = [self getImageViewFromUrl:autoAnimateSetting ? lastMessage.imageUrl : lastMessage.staticGifUrl];
                    if (!autoAnimateSetting)
                        [rainbowCell addGifIcon:[_conversation.peer isKindOfClass:[Room class]]];
                }
            } else {
                cell.mediaView = [self getImageViewFromUrl:lastMessage.imageUrl];
            }
        }
        
        if([lastMessage replacedDate] != nil && [lastMessage body] == nil && [lastMessage bodyInMarkdown] == nil) {
            [cell.textView setAttributedText:[self deletionAttributedStringForMessage:lastMessage]];
        }
        //if (!cell.mediaView) { // why we need to check this??!
        else {
            if (message.attributedString) {
                cell.textView.attributedText = message.attributedString;
                if (lastMessage.isOutgoing) {
                    cell.textView.textColor = [UIColor blackColor];
                }
                else {
                    cell.textView.textColor = [UIColor whiteColor];
                }
            } else {
                cell.textView.text = [messageItem text];
                NSParameterAssert(cell.textView.text != nil);
            }
        }
        
        if (lastMessage.attachment && !lastMessage.attachment.isDownloadAvailable) {
            [cell.textView setAttributedText:[self deletionAttributedStringForMessage:lastMessage]];
        }
        
        if(message.answeredMessage) {
            [(JSQRainbowMessagesCollectionViewCell*)cell addRepliedMessageSubView:message.answeredMessage isOutgoing:isOutgoingMessage withSenderLabel:(!isOutgoingMessage && ![_conversation.peer isKindOfClass:[Contact class]])];
        }
    }
    
    if (isMediaMessage) {
        id<JSQMessageMediaData> messageMedia = [messageItem media];
        cell.mediaView = [messageMedia mediaView]? :[messageMedia mediaPlaceholderView];
        NSParameterAssert(cell.mediaView != nil);
        NSLog(@"message media is shown %@",messageMedia);
    }
    
    id<JSQMessageBubbleImageDataSource> bubbleImageDataSource = [collectionView.dataSource collectionView:collectionView messageBubbleImageDataForItemAtIndexPath:indexPath];
    cell.messageBubbleImageView.image = [bubbleImageDataSource messageBubbleImage];
    cell.messageBubbleImageView.highlightedImage = [bubbleImageDataSource messageBubbleHighlightedImage];
    
    //BOOL needsAvatar = YES;
    if (isOutgoingMessage && CGSizeEqualToSize(collectionView.collectionViewLayout.outgoingAvatarViewSize, CGSizeZero)) {
        _needsAvatar = NO;
    }
    else if (!isOutgoingMessage && CGSizeEqualToSize(collectionView.collectionViewLayout.incomingAvatarViewSize, CGSizeZero)) {
        _needsAvatar = NO;
        
    }
    
    id<JSQMessageAvatarImageDataSource> avatarImageDataSource = nil;
    if (_needsAvatar) {
        avatarImageDataSource = [collectionView.dataSource collectionView:collectionView avatarImageDataForItemAtIndexPath:indexPath];
        if (avatarImageDataSource != nil) {
            
            UIImage *avatarImage = [avatarImageDataSource avatarImage];
            if (avatarImage == nil) {
                cell.avatarImageView.image = [avatarImageDataSource avatarPlaceholderImage];
                cell.avatarImageView.highlightedImage = nil;
            }
            else {
                cell.avatarImageView.image = avatarImage;
                cell.avatarImageView.highlightedImage = [avatarImageDataSource avatarHighlightedImage];
            }
        }
    }
    
    cell.cellTopLabel.attributedText = [collectionView.dataSource collectionView:collectionView attributedTextForCellTopLabelAtIndexPath:indexPath];
    cell.messageBubbleTopLabel.attributedText = [collectionView.dataSource collectionView:collectionView attributedTextForMessageBubbleTopLabelAtIndexPath:indexPath];
    cell.cellBottomLabel.attributedText = [collectionView.dataSource collectionView:collectionView attributedTextForCellBottomLabelAtIndexPath:indexPath];
    
    CGFloat bubbleTopLabelInset = (avatarImageDataSource != nil) ? 40.0f : 15.0f;
    
    if (isOutgoingMessage) {
        cell.messageBubbleTopLabel.textInsets = UIEdgeInsetsMake(0.0f, 0.0f, 0.0f, bubbleTopLabelInset);
    }
    else {
        cell.messageBubbleTopLabel.textInsets = UIEdgeInsetsMake(0.0f, bubbleTopLabelInset, 0.0f, 0.0f);
    }
    
    cell.textView.dataDetectorTypes = UIDataDetectorTypeAll;
    
    cell.backgroundColor = [UIColor clearColor];
    cell.layer.rasterizationScale = [UIScreen mainScreen].scale;
    cell.layer.shouldRasterize = YES;
    
    
    [self collectionView:collectionView accessibilityForCell:cell indexPath:indexPath message:messageItem];
}

- (NSAttributedString *)deletionAttributedStringForMessage:(Message *)aMessage {
    // action string
    NSString *actionString;
    if ([aMessage replacedDate] != nil && [aMessage body] == nil && [aMessage bodyInMarkdown] == nil) {
        if(aMessage.isOutgoing) {
            actionString = NSLocalizedString(@"You deleted this message", nil);
            
        } else {
            actionString = NSLocalizedString(@"This message has been deleted", nil);
            
        }
    }
    else if (aMessage.attachment && !aMessage.attachment.isDownloadAvailable) {
        actionString = NSLocalizedString(@"File deleted", nil);
    }
    NSDictionary *actionAttributesDictionary = @{NSForegroundColorAttributeName: (aMessage.isOutgoing) ? [UIColor grayColor] : [UIColor whiteColor], NSFontAttributeName:[UIFont italicSystemFontOfSize:13.0]};
    NSAttributedString *actionAttributedString = [[NSAttributedString alloc] initWithString:actionString attributes:actionAttributesDictionary];
    
    // attachment String
    NSTextAttachment *textAttachment = [[NSTextAttachment alloc] init];
    textAttachment.bounds = CGRectMake(0, - 3, 15, 15);
    textAttachment.image = [UIImage imageNamed:@"message_Delete"];
    NSAttributedString *attachmentAtributtedString = [NSAttributedString attributedStringWithAttachment:textAttachment];
    
    // body string
    NSString *bodyString = (aMessage.body.length) ? aMessage.body : @" ";
    NSDictionary *bodyAttributesDictionary = @{NSForegroundColorAttributeName : (aMessage.isOutgoing) ? (aMessage.body.length) ? [UIColor blackColor] : [UIColor grayColor]: [UIColor whiteColor], NSFontAttributeName : [UIFont fontWithName:[UITools defaultFontName] size:14.0]};
    
    NSMutableAttributedString *mutableAttributedString = [[NSMutableAttributedString alloc] initWithString:bodyString attributes:bodyAttributesDictionary];
    
    if (aMessage.body.length) {
        [mutableAttributedString appendAttributedString:[[NSAttributedString alloc] initWithString:@"\n\n"]];
        [mutableAttributedString appendAttributedString:attachmentAtributtedString];
        NSRange bodyRange = [mutableAttributedString.string rangeOfString:bodyString];
        if (bodyRange.location != NSNotFound) {
            NSRange actionRange = NSMakeRange(bodyRange.length, mutableAttributedString.length - bodyRange.length);
            if (actionRange.location != NSNotFound) {
                [mutableAttributedString addAttributes:@{ NSForegroundColorAttributeName : (aMessage.isOutgoing) ? [UIColor grayColor] : [UIColor whiteColor]} range:actionRange];
            }
        }
    }
    else {
        [mutableAttributedString appendAttributedString:attachmentAtributtedString];
    }
    [mutableAttributedString appendAttributedString:[[NSAttributedString alloc] initWithString:@" "]];
    [mutableAttributedString appendAttributedString:actionAttributedString];
    
    return mutableAttributedString;
}

-(UIView *) getImageViewFromUrl:(NSString *) url {
    CGSize size = CGSizeMake(([[UIScreen mainScreen] bounds].size.width)* 0.66 + 20, ([[UIScreen mainScreen] bounds].size.width)* 0.66);
    SDAnimatedImageView *imageView = [SDAnimatedImageView new];
    [imageView sd_setImageWithURL:[NSURL URLWithString:url] placeholderImage:[[UIImage imageNamed:@"noimg"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    
    imageView.contentMode = UIViewContentModeScaleAspectFill;
    imageView.frame = CGRectMake(0.0f, 0.0f, size.width, size.height);
    
    imageView.clipsToBounds = YES;
    
    [imageView.heightAnchor constraintEqualToConstant:size.height].active = YES;
    
    imageView.layer.cornerRadius = 12.0f;
    imageView.layer.masksToBounds = YES;
    return imageView;
}


- (UICollectionViewCell *)collectionView:(JSQMessagesCollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    GroupedMessages *message = [_messages objectAtIndex:[self groupedMessagesIndexForRowIndex:indexPath]];
    JSQMessagesCollectionViewCell *cell = nil;
    
    // If the message is a group chat event we should build a custom cell for it
    if(message.firstMessage && message.firstMessage.type == MessageTypeGroupChatEvent){
        cell = [self groupChatEventCellFor:collectionView atIndexPath:indexPath message:message.firstMessage];
    } else if(message.firstMessage && message.firstMessage.type == MessageTypeWebRTC){
        cell = [self webRTCEventCellFor:collectionView atIndexPath:indexPath message:message.firstMessage];
    } else if(message.firstMessage && (message.firstMessage.type == MessageTypeCallRecordingStart || message.firstMessage.type == MessageTypeCallRecordingStop)){
        cell = [self callRecordingEventCellFor:collectionView atIndexPath:indexPath message:message.firstMessage];
    } else {
        if ([message.senderId isEqualToString:self.senderId]) {
            
            cell = [self outgoingCellFor:collectionView atIndexPath:indexPath];
        } else {
            if(_conversation.type == ConversationTypeRoom)
                cell = [self roomIncomingCellFor:collectionView atIndexPath:indexPath];
            else
                cell = [self incomingCellFor:collectionView atIndexPath:indexPath];
        }
        
        cell.textView.linkTextAttributes = @{ NSForegroundColorAttributeName : cell.textView.textColor, NSUnderlineStyleAttributeName : @(NSUnderlineStyleSingle | NSUnderlinePatternSolid) };
    }
    
    //TODO: Move to contextual hightlight
    if([self compareSearchMessageTimestamp:_searchMessageCurrentOccurence withDate:message.firstMessage.timestamp]) {
        cell.textView.attributedText = [self hightlightText:_searchMessageText withCell:cell];
        _searchMessageCurrentOccurenceIndexPath = indexPath;
    } else {
        if(cell.textView.attributedText) {
            cell.textView.attributedText = [self resetAttributedTextWithCell:cell];
        }
    }
    
    return cell;
}


#pragma mark - scroll to bottom button
/**
 * Add  to the view and setup the scroll to bottom button
 */
//-(void) addScrollToBottomButton {
//    [UITools applyCustomFontTo:_scrollToBottomButton.titleLabel];
//    _scrollToBottomButton.backgroundColor = [UIColor darkGrayColor];
//    _scrollToBottomButton.layer.cornerRadius = _scrollToBottomButton.frame.size.height/2;
//    //    self.scrollToBottomButton.translatesAutoresizingMaskIntoConstraints = YES;
//    [_scrollToBottomButton setTintColor:[UIColor whiteColor]];
//    [_scrollToBottomButton setImage:[UIImage imageNamed:@"scroll_down"] forState:UIControlStateNormal];
//    [_scrollToBottomButton setImageEdgeInsets:UIEdgeInsetsMake(4, -10, 4, 0)];
//    [self.view addSubview:_scrollToBottomButton];
//    _scrollToBottomButton.hidden = YES;
//    _scrollToBottomButton.alpha = 0.0;
//}

/**
 * Update the frame of the scroll to bottom button
 */
-(void)adjustScrollToBottomButtonFrame {
//    CGFloat viewWidth = self.view.frame.size.width;
//    CGFloat buttonHeight = self.scrollToBottomButton.frame.size.height;
//    CGFloat buttonWidth = self.scrollToBottomButton.frame.size.width;
//    CGPoint inputToolbarOrigin = self.inputToolbar.frame.origin;
//    self.scrollToBottomButton.frame = CGRectMake(viewWidth/2 - buttonWidth/2, inputToolbarOrigin.y - buttonHeight - 16, buttonWidth, buttonHeight);
}

/**
 * Set the scroll to bottom button label and adjust it size accordingly
 */
-(void)setScrollToBottomButtonLabel:(NSString *)text {
//    CGFloat viewWidth = self.view.frame.size.width;
//    [self.scrollToBottomButton setTitle:text forState:UIControlStateNormal];
//    CGFloat buttonHeight = self.scrollToBottomButton.frame.size.height;
//    [UITools sizeButtonToText:self.scrollToBottomButton availableSize:CGSizeMake(viewWidth-16, buttonHeight)  padding:UIEdgeInsetsMake(8, 16, 8, 16)];
//    [self adjustScrollToBottomButtonFrame];
}

/**
 * Show/hide the scroll to bottom button depending of the show boolean
 * Add the button to the view if needed
 */
-(void) showScrollToBottomButton:(BOOL)show {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self showScrollToBottomButton:show];
        });
        return;
    }
    
    NSString *text = @"";
    
    if(_conversation.unreadMessagesCount > 0){
        if(_conversation.unreadMessagesCount == 1)
            text = NSLocalizedString(@"You have a new message", nil);
        else
            text = [NSString stringWithFormat:NSLocalizedString(@"You have %ld messages", nil), _conversation.unreadMessagesCount];
    } else {
        if(!_allowAutomaticallyScrollsToMostRecentMessage)
            text = NSLocalizedString(@"Jump to the last message", nil);
    }
    
//    if(show){
//        [self setScrollToBottomButtonLabel:text];
//        self.scrollToBottomButton.hidden = NO;
//    }
    
//    [UIView animateWithDuration:0.5 animations:^(){
//        if(show){
//            self.scrollToBottomButton.alpha = 1.0;
//        } else {
//            self.scrollToBottomButton.alpha = 0.0;
//        }
//    } completion:^(BOOL finished) {
//        if(!show){
//
//            self.scrollToBottomButton.hidden = YES;
//            [self setScrollToBottomButtonLabel:text];
//        }
//    }];
}

/**
 * UIScrollView delegate called when the scroll view will stopped
 */
- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset{
    CGFloat contentHeight = scrollView.contentSize.height;
    CGFloat height = scrollView.frame.size.height;
    CGFloat offsetFromBottom = contentHeight - targetContentOffset->y - height;
    _allowAutomaticallyScrollsToMostRecentMessage = offsetFromBottom >= 40.0 ? NO : YES;
    self.automaticallyScrollsToMostRecentMessage = _allowAutomaticallyScrollsToMostRecentMessage;
    
    [self showScrollToBottomButton:!_allowAutomaticallyScrollsToMostRecentMessage];
}

- (void)scrollViewDidScrollToTop:(UIScrollView *)scrollView {
    // Delegate invoked when tapping in the status bar to got to top of the list
    _allowAutomaticallyScrollsToMostRecentMessage = NO;
    self.automaticallyScrollsToMostRecentMessage = _allowAutomaticallyScrollsToMostRecentMessage;
    [self showScrollToBottomButton:YES];
}

#pragma mark - Responding to collection view tap events

- (void)collectionView:(JSQMessagesCollectionView *)collectionView didTapAvatarImageView:(UIImageView *)avatarImageView atIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"Tapped avatar!");
}

- (void)collectionView:(JSQMessagesCollectionView *)collectionView didTapMessageBubbleAtIndexPath:(NSIndexPath *)indexPath {
    // We can only tap on our bubbles.
    if (!_startShowingSharingView) {
        GroupedMessages *message = [_messages objectAtIndex:[self groupedMessagesIndexForRowIndex:indexPath]];
        if (![message.senderId isEqualToString:self.senderId]) {
            return;
        }
        // did we re-tapped the same bubble ?
        if (_tappedIndexPath && [indexPath compare:_tappedIndexPath] == NSOrderedSame) {
            _tappedIndexPath = nil;
        } else {
            _tappedIndexPath = indexPath;
        }
        
        [self reloadCollectionView];
        
        // If we click on the last cell, the bottom-label is not fully visible, so scroll.
        NSIndexPath *lastCell = [NSIndexPath indexPathForItem:([self.collectionView numberOfItemsInSection:0] - 1) inSection:0];
        if ([indexPath compare:lastCell] == NSOrderedSame) {
            [self scrollToBottomAnimated:NO];
        }
    }
    
}

-(void) didShareAttachment:(File *) file atCell:(JSQRainbowMessagesCollectionViewCell *) cell  {
//
//    if (!_startShowingSharingView) {
//        NSIndexPath *path = [self.collectionView indexPathForCell:cell];
//        GroupedMessages *message = [_messages objectAtIndex:[self groupedMessagesIndexForRowIndex:path]];
//        _selectedMessage = message.lastMessage;
//
//        if([self.inputToolbar.contentView.textView isFirstResponder])
//            [self.inputToolbar.contentView.textView resignFirstResponder];
//
//        _selectedMedia = file;
//        [self setupActionsViewAtCell:cell];
//
//        _startShowingSharingView = YES;
//        [self.collectionView setScrollEnabled:NO];
//        self.inputToolbar.hidden = YES;
//        [_scrollToBottomButton setEnabled:NO];
//
//        [UIView animateWithDuration:0.4 animations:^{
//            // Use the origin.y which is modified when there is the green call banner to display the actions view
//            CGFloat callInProgressBannerHeight = [UIApplication sharedApplication].windows[0].frame.origin.y;
//            _actionsView.frame = CGRectMake(0, [UIScreen mainScreen].bounds.size.height - 128 - topPadding - callInProgressBannerHeight, [UIScreen mainScreen].bounds.size.width, 64 + bottomPadding);
//        }];
//
//        CGRect frame = self.view.frame;
//        // frame.size.height -= _actionsView.frame.size.height; // remove this because at sometime user can see the different btw actionsViewBackgroundView and under the actionsView
//        _actionsViewBackgroundView = [[UIView alloc] initWithFrame:frame];
//        _actionsViewBackgroundView.backgroundColor = [[UIColor lightGrayColor] colorWithAlphaComponent:0.6];
//        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
//        [_actionsViewBackgroundView addGestureRecognizer:tap];
//        [self.view addSubview:_actionsViewBackgroundView];
//
//        if(cell.mediaView){
//            CGRect pos = [cell.mediaView convertRect:cell.mediaView.bounds toView:self.view];
//            UIView *containerView = [[UIView alloc] initWithFrame:pos];
//            UIView *mediaView = [file performSelector:@selector(mediaView)];
//            mediaView.tintColor = [file tintColor];
//            pos.origin.x = pos.origin.y = 0;
//            mediaView.frame = pos;
//            containerView.layer.shadowColor = [UIColor blackColor].CGColor;
//            containerView.layer.shadowRadius = 4.0f;
//            containerView.layer.shadowOpacity = 0.5;
//            containerView.layer.shadowOffset = CGSizeMake(0, 3);
//            containerView.tag = [file.tag longLongValue];
//            [containerView addSubview:mediaView];
//            [_actionsViewBackgroundView addSubview:containerView];
//        }
//
//
//        [self.view bringSubviewToFront:_actionsView];
//    }
}

//- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer {
//    [self dismissActionView];
//}

//- (void) hideFileThumbmailForDeletedFile {
//    UIView *thumbnailImageView = [_actionsViewBackgroundView viewWithTag:[_selectedMedia.tag longLongValue]];
//    thumbnailImageView = nil;
//    [thumbnailImageView removeFromSuperview];
//}

//- (void) dismissActionView {
//    if(![NSThread isMainThread]){
//        dispatch_async(dispatch_get_main_queue(), ^{
//            [self dismissActionView];
//        });
//        return;
//    }
//
//    [_scrollToBottomButton setEnabled:YES];
//    _startShowingSharingView = NO;
//    [self.collectionView setScrollEnabled:YES];
//    [_actionsViewBackgroundView removeFromSuperview];
//    _actionsViewBackgroundView = nil;
//
//    [UIView animateWithDuration:0.2 animations:^{
//        _actionsView.frame = CGRectMake(0, [UIScreen mainScreen].bounds.size.height - topPadding, [UIScreen mainScreen].bounds.size.width, 64);
//    }];
//
//    self.inputToolbar.hidden = NO;
//}
-(void) didFailedSendAttachment:(File *) file atCell:(JSQRainbowMessagesCollectionViewCell *) cell  {
    if ([cell.status.image isEqual:[UIImage imageNamed:@"message_ko"]]) {
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Your message was not sent.", nil) message:@"" preferredStyle:UIAlertControllerStyleActionSheet];
        UIAlertAction *sendAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Try Again",nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction __unused *action) {
            NSIndexPath *path = [self.collectionView indexPathForCell:cell];
            GroupedMessages *message = [_messages objectAtIndex:[self groupedMessagesIndexForRowIndex:path]];
            Message *lastMessage = message.lastMessage;
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.inputToolbar.contentView.leftBarButtonItem setEnabled:YES];
                if ([file.tag  isEqualToString: lastMessage.messageID]) {
                    [_messages removeObject:message];
                    [self reloadCollectionView];
                }
            });
            
            [_conversationsManagerService reSendMessage:lastMessage to:_conversation completionHandler:^(Message *message, NSError *error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.inputToolbar.contentView.leftBarButtonItem setEnabled:YES];
                    if (!error) {
                        [self reloadCollectionView];
                    }
                });
                
            } attachmentUploadProgressHandler:^(Message *message, double totalBytesSent, double totalBytesExpectedToSend) {
                
            }];
        }];
        
        UIAlertAction *deleteAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Delete",nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction __unused *action) {
            NSIndexPath *path = [self.collectionView indexPathForCell:cell];
            GroupedMessages *message = [_messages objectAtIndex:[self groupedMessagesIndexForRowIndex:path]];
            Message *lastMessage = message.lastMessage;
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.inputToolbar.contentView.leftBarButtonItem setEnabled:YES];
                if ([file.tag  isEqualToString: lastMessage.messageID]) {
                    [_messages removeObject:message];
                    [self reloadCollectionView];
                }
            });
            
            [_conversationsManagerService deleteFailedMessage:lastMessage to:_conversation];
        }];
        
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel",@"Cancel") style:UIAlertActionStyleCancel handler:^(UIAlertAction __unused *action) {
        }];
        
        [alertController addAction:sendAction];
        [alertController addAction:deleteAction];
        [alertController addAction:cancelAction];
        [self presentViewController:alertController animated:YES completion:nil];
    }
}


-(void) didLongPressOnImageForAttachment:(File *) file atCell:(JSQRainbowMessagesCollectionViewCell *) cell {
    NSIndexPath *path = [self.collectionView indexPathForCell:cell];
    GroupedMessages *message = [_messages objectAtIndex:[self groupedMessagesIndexForRowIndex:path]];
    Message *lastMessage = message.lastMessage;
    if (lastMessage.state == MessageDeliveryStateSent || lastMessage.state == MessageDeliveryStateFailed) {
        [self didFailedSendAttachment:file atCell:cell];
    }
    else{
//        [self didShareAttachment:file atCell:cell];
    }
    
}

-(void) didTapOnImageForAttachment:(File *) file atCell:(JSQRainbowMessagesCollectionViewCell *) cell {
    [[GoogleAnalytics sharedInstance] dispatchEventWithCategory:@"chat" action:@"show file" label:nil value:nil];
    if (file.isDownloadAvailable) {
        if(!file.data){
            NSIndexPath *path = [self.collectionView indexPathForCell:cell];
            GroupedMessages *message = [_messages objectAtIndex:[self groupedMessagesIndexForRowIndex:path]];
            Message *lastMessage = message.lastMessage;
            file.tag = lastMessage.messageID;
            file.value = 0.01;
            [_filesDownloading addObject:file];
            __weak typeof (self) weakSelf = self;
            [[ServicesManager sharedInstance].fileSharingService downloadDataForFile:file withCompletionHandler:^(File *aFile, NSError *error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    if(!error){
                        [_filesDownloading removeObject:file];
                        [self reloadCollectionView];
                        [weakSelf openPreviewControllerForAttachment:aFile];
                    } else {
                        MBProgressHUD *errorHud = [UITools showHUDWithMessage:NSLocalizedString(@"Error",nil) forView:self.view mode:MBProgressHUDModeCustomView imageName:@"Failed" dismissCompletionBlock:nil];
                        [errorHud showAnimated:YES whileExecutingBlock:^{
                            [NSThread sleepForTimeInterval:3];
                        } completionBlock:^{
                            [_filesDownloading removeObject:file];
                            [self reloadCollectionView];
                        }];
                    }
                });

            } progressHandler:^(File *file, double progressPercent) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    cell.progressView.hidden = NO;
                    cell.cellDateLabel.hidden = YES;
                    [cell setProgressViewWithValue:(float)progressPercent ];
                });
            }];
        } else {
            [self reloadCollectionView];
            [self openPreviewControllerForAttachment:file];
        }
    } else if (cell.mediaView && [cell.mediaView isKindOfClass:[SDAnimatedImageView class]]) {
        NSIndexPath *path = [self.collectionView indexPathForCell:cell];
        GroupedMessages *message = [_messages objectAtIndex:[self groupedMessagesIndexForRowIndex:path]];
        Message *lastMessage = message.lastMessage;
        if (lastMessage.staticGifUrl) {
            SDWebImageManager *manager = [SDWebImageManager sharedManager];
            if (cell.animating) {
                // Stop the animation by setting the static image + add the gif icon
                [manager loadImageWithURL:[NSURL URLWithString:lastMessage.staticGifUrl]
                                  options:SDWebImageQueryMemoryData
                                 progress:nil
                                completed:^(UIImage *image, NSData *data, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                                    if (image) {
                                        ((SDAnimatedImageView *)cell.mediaView).image = image;
                                        [cell addGifIcon:[lastMessage.via isKindOfClass:[Room class]]];
                                    }
                                }];
            } else {
                // Start the animation by setting the real gif image + remove the gif icon
                [manager loadImageWithURL:[NSURL URLWithString:lastMessage.imageUrl]
                                  options:SDWebImageQueryMemoryData
                                 progress:nil
                                completed:^(UIImage *image, NSData *data, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                                    if (image) {
                                        ((SDAnimatedImageView *)cell.mediaView).image = image;
                                        [cell removeGifIcon];
                                    }
                                }];
            }
            cell.animating = !cell.animating;
        }
    }
}

-(void) reloadCollectionView {
    if([self isViewLoaded]){
        [self.collectionView.collectionViewLayout invalidateLayoutWithContext:[JSQMessagesCollectionViewFlowLayoutInvalidationContext context]];
        [self.collectionView reloadData];
    }
}

-(void) openPreviewControllerForAttachment:(File *) attachment {
    // Instanciate a QLPreview​Controller
    //    MBProgressHUD *openingHud = [UITools showHUDWithMessage:NSLocalizedString(@"Opening file ...",nil) forView:self.view mode:MBProgressHUDModeIndeterminate imageName:nil dismissCompletionBlock:nil];
    //    [openingHud show:YES];
    UIPreviewQuickLookController *previewController = [UIPreviewQuickLookController new];
    previewController.attachment = attachment;
    [self presentViewController:previewController animated:YES completion:^{
        // [openingHud hide:YES];
    }];
}

#pragma mark - Browsing delegate

-(void) itemsBrowser:(CKItemsBrowser*)browser didAddCacheItems:(NSArray*)newItems atIndexes:(NSIndexSet*)indexes {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self itemsBrowser:browser didAddCacheItems:newItems atIndexes:indexes];
        });
        return;
    }
    NSLog(@"[didAddCacheItems] number of new Items: %ld", newItems.count);
    NSLog(@"%@ %@", NSStringFromSelector(_cmd), newItems);
    NSUInteger oldLoadedMessagesCount = [_messages count];
    for (Message *message in newItems) {
        
        if(message.type == MessageTypeGroupChatEvent){
            // for each message check if we have an avatar in our list, we don't have them for event in case we invite some one and we remove it imediately.
            if([message.groupChatEventPeer isKindOfClass:[Contact class]]){
                [self setAvatarForContact:(Contact *)message.groupChatEventPeer];
            }
            
            if (message.isDisplayable == NO)
                continue;
            
        }
        
        // Each one on these new messages could be at a different index
        // Which could be anywhere inside our current grouped messages.
        // For example we could have a new message from A which should appear between 2 messages (already grouped) from B.
        // Then we have to split the grouped message from B in 2 grouped messages and add the message from A in
        // the middle.
        Message *previousMessage = nil;
        GroupedMessages *previousGroupedMessages = nil;
        Message *nextMessage = nil;
        GroupedMessages *nextGroupedMessages = nil;
        
        // We have to enumerate in reverse order because in _messages, the first item is the most recent one.
        for (GroupedMessages *groupedMessages in [_messages reverseObjectEnumerator]) {
            for (Message *msg in groupedMessages.allGroupedMessages) {
                // Dont forget to handle the equal case !
                if ([msg.date compare:message.date] == NSOrderedAscending || [msg.date compare:message.date] == NSOrderedSame) {
                    previousMessage = msg;
                    previousGroupedMessages = groupedMessages;
                }
                if ([msg.date compare:message.date] == NSOrderedDescending && !nextMessage) {
                    nextMessage = msg;
                    nextGroupedMessages = groupedMessages;
                }
            }
        }
        
        if (previousMessage) {
            if (nextMessage) {
                // we arrive here when we have a previous AND a next message.
                // This new message is somewhere in the middle of our list.
                // This is the most complicated case, we might have to un-merge already grouped messages, etc.
                
                // First case is when the previous and next are in the same groupedmessage
                if (previousGroupedMessages == nextGroupedMessages) {
                    // We can add the new message to the existing grouped if same peer, etc.
                    if ([self shouldGroupMessage:previousGroupedMessages.lastMessage with:message]) {
                        [previousGroupedMessages addMessage:message];
                    } else {
                        // HERE we have to un-merge the grouped message and add a new between.
                        GroupedMessages *previousGroup = [[GroupedMessages alloc] initWithMessage:previousGroupedMessages.firstMessage];
                        GroupedMessages *newGroup = [[GroupedMessages alloc] initWithMessage:message];
                        GroupedMessages *nextGroup = [[GroupedMessages alloc] initWithMessage:nextMessage];
                        
                        for (Message *m in [previousGroupedMessages allGroupedMessages]) {
                            // Dont forget the equal case !
                            if ([m.date compare:message.date] == NSOrderedAscending || [m.date compare:message.date] == NSOrderedSame) {
                                [previousGroup addMessage:m];
                            }
                            if ([m.date compare:message.date] == NSOrderedDescending) {
                                [nextGroup addMessage:m];
                            }
                        }
                        [_messages removeObject:previousGroupedMessages];
                        [self insertMessageAtProperIndex:previousGroup];
                        [self insertMessageAtProperIndex:newGroup];
                        [self insertMessageAtProperIndex:nextGroup];
                    }
                } else {
                    // prev and next are not the same grouped, then its easy, we can have 3 cases :
                    // put the new message in the previous, in the next, or create a new one.
                    if ([self shouldGroupMessage:previousGroupedMessages.lastMessage with:message]) {
                        [previousGroupedMessages addMessage:message];
                    } else if ([self shouldGroupMessage:nextGroupedMessages.lastMessage with:message]) {
                        [nextGroupedMessages addMessage:message];
                    } else {
                        // create a new grouped message
                        GroupedMessages *newGroup = [[GroupedMessages alloc] initWithMessage:message];
                        [self insertMessageAtProperIndex:newGroup];
                    }
                }
                
            } else {
                // This is the most common case, we have a previous but no next
                // This is a new message at the bottom of the list.
                if ([self shouldGroupMessage:previousGroupedMessages.firstMessage with:message]) {
                    [previousGroupedMessages addMessage:message];
                } else {
                    // create a new grouped message
                    GroupedMessages *newGroup = [[GroupedMessages alloc] initWithMessage:message];
                    [self insertMessageAtProperIndex:newGroup];
                }
            }
        } else {
            if (nextMessage) {
                // We have no previous message but a next message
                // This message goes to the top of the list (it is the new oldest message)
                if ([self shouldGroupMessage:nextGroupedMessages.firstMessage with:message]) {
                    [nextGroupedMessages addMessage:message];
                } else {
                    // create a new grouped message
                    GroupedMessages *newGroup = [[GroupedMessages alloc] initWithMessage:message];
                    [self insertMessageAtProperIndex:newGroup];
                }
            } else {
                // No previous and no next message ?
                // This is the first message of the list.
                GroupedMessages *newGroup = [[GroupedMessages alloc] initWithMessage:message];
                [self insertMessageAtProperIndex:newGroup];
            }
        }
    }
    NSInteger newLoadedMessagesCount = [_messages count] - oldLoadedMessagesCount;
    
    [self reloadCollectionView];
    
    if(_isRetrievingNextPage && newLoadedMessagesCount>0 && !self.automaticallyScrollsToMostRecentMessage){
        NSInteger topMessageIndex = newLoadedMessagesCount>1 ? newLoadedMessagesCount-1 : newLoadedMessagesCount;
        
        NSIndexPath *cellIdx = [NSIndexPath indexPathForItem:topMessageIndex inSection:0];
        [self scrollToIndexPath:cellIdx animated:NO];
    } else {
        if(self.automaticallyScrollsToMostRecentMessage)
            [self scrollToBottomAnimated:NO];
    }
    
    if(!self.automaticallyScrollsToMostRecentMessage){
        self.automaticallyScrollsToMostRecentMessage = self.allowAutomaticallyScrollsToMostRecentMessage;
        // TODO : We need to display the jump to bottom button
        
    }
    
    // We must update the more button status in case of changes of one of his conditions
//    _moreMenuButton.enabled = [self willShowSomethingInMoreMenu];
    
    
}

-(void) itemsBrowser:(CKItemsBrowser*)browser didRemoveCacheItems:(NSArray*)removedItems atIndexes:(NSIndexSet*)indexes {
    if(browser != _messagesBrowser)
        return;
    
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self itemsBrowser:browser didRemoveCacheItems:removedItems atIndexes:indexes];
        });
        return;
    }
    
    NSLog(@"%@ %@", NSStringFromSelector(_cmd), removedItems);
    
    for (Message *removeMessage in removedItems) {
        NSInteger idx = [self indexPathOfGroupedMessageForMessage:removeMessage];
        if(idx != NSNotFound ){ // here check if this is a filed attachment , don't remove it
            if (!(removeMessage.state == MessageDeliveryStateFailed && removeMessage.attachment)) {
                GroupedMessages *grpMsgs = [_messages objectAtIndex:idx];
                [grpMsgs removeMessage:removeMessage];
                if(grpMsgs.allGroupedMessages.count == 0)
                    [_messages removeObject:grpMsgs];
            }
            else{
                NSLog(@"%@ this message %@ is failed attachment so we have to display it", NSStringFromSelector(_cmd),removeMessage.messageID);
            }
            
        }
    }
    
    [self finishReceivingMessage];
    [self setShowLoadEarlierMessagesHeader:NO];
    
    // We must update the more button status in case of changes of one of his conditions
//    _moreMenuButton.enabled = [self willShowSomethingInMoreMenu];
}

-(void) itemsBrowser:(CKItemsBrowser*)browser didUpdateCacheItems:(NSArray*)changedItems atIndexes:(NSIndexSet*)indexes {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self itemsBrowser:browser didUpdateCacheItems:changedItems atIndexes:indexes];
        });
        return;
    }
    NSLog(@"%@ %@", NSStringFromSelector(_cmd), changedItems);
    for (Message *message in changedItems) {
        NSString* messageStatus = [Message stringForDeliveryState:message.state];
        NSString* action = @"";
        if (message.state != _messages.lastObject.lastMessage.state) {
            //send firebase event for message status
            NSLog(@"messageStatus %@ ", messageStatus);
            if(message.state == MessageDeliveryStateReceived) {
                action = @"recieve";
            }
            else if (message.state == MessageDeliveryStateDelivered) {
                action = @"deleiverd";
            }
            else if (message.state == MessageDeliveryStateFailed) {
                action = @"failed";
            }
            else if (message.state == MessageDeliveryStateRead) {
                action = @"read";
            }
        }
        if(![message attachment]) {
            // send to firebase when messages Received
            [FIRAnalytics logEventWithName:[NSString stringWithFormat:@"%@_IM",action]
                                parameters:@{
                                             @"action" :[NSString stringWithFormat:@"%@_text_message",action],
                                             @"category":@"chat_screen"
                                             }];
        }
        else {
//            [FIRAnalytics logEventWithName:[NSString stringWithFormat:@"%@_IM",action]
//                                parameters:@{
//                                             @"action" :[NSString stringWithFormat:@"%@_%@_message",action,[self getSelectedFileType:[message attachment]]],
//                                             @"category":@"chat_screen"
//                                             }];
        }
        
    }
    
    [self finishReceivingMessage];
}

-(void) shouldUpdateAttachment:(NSNotification *) notification {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self reloadCollectionView];
    });
}

-(void) itemsBrowser:(CKItemsBrowser*)browser didReorderCacheItemsAtIndexes:(NSArray*)oldIndexes toIndexes:(NSArray*)newIndexes {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self itemsBrowser:browser didReorderCacheItemsAtIndexes:oldIndexes toIndexes:newIndexes];
        });
        return;
    }
    
    NSLog(@"%@", NSStringFromSelector(_cmd));
    [self finishReceivingMessage];
}

-(void) itemsBrowser:(CKItemsBrowser *)browser didReceiveItemsAddedEvent:(NSArray *)addedItems {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self itemsBrowser:browser didReceiveItemsAddedEvent:addedItems];
        });
        return;
    }
    
    NSLog(@"[UI-IM] DID RECEIVE ITEM ADDED EVENT %@", addedItems);
    Message *message = (Message*)addedItems.firstObject;
    // This message is for this conversation
    NSLog(@"Treating notification %@", [Message stringForDeliveryState:message.state]);
    //Added when receive new message while conversation is opened
    if(message.state == MessageDeliveryStateReceived){
        
        if(![message attachment]) {
            // send to firebase Received actions
            [FIRAnalytics logEventWithName:@"recieve_IM"
                                parameters:@{
                                             @"action" :@"recieve_text_message",
                                             @"category":@"chat_screen"
                                             }];
        }
        else {
//            NSLog(@"recieveMsg %@",[NSString stringWithFormat:@"recieve_%@_message",[self getSelectedFileType:[message attachment]]]);
//            [FIRAnalytics logEventWithName:@"recieve_IM"
//                                parameters:@{
//                                             @"action" :[NSString stringWithFormat:@"recieve_%@_message",[self getSelectedFileType:[message attachment]]],
//                                             @"category":@"chat_screen"
//                                             }];
        }
    }
    
    // The notification (in case of background) or sound-alert is handled by the ConversationTableViewController
    [self finishReceivingMessageAnimated:YES];
}

#pragma mark - Grouped messages
-(BOOL) isMessageHasAttachment:(Message *) msg{
    if (msg.body != nil) {
        if (([msg.body rangeOfString:@"IMG_"].location != NSNotFound && [msg.body rangeOfString:@"."].location != NSNotFound) || [msg.body isEqualToString:@""]) {
            return YES;
        }
        return NO;
    }
    return YES;
}
-(BOOL) shouldGroupMessage:(Message *) message1 with:(Message *) message2 {
    return NO;
}

-(NSInteger) indexPathOfGroupedMessageForMessage:(Message *) message {
    __block NSInteger index = NSNotFound;
    [_messages enumerateObjectsUsingBlock:^(GroupedMessages * groupedMessages, NSUInteger idx, BOOL * stop) {
        if([groupedMessages.allGroupedMessages containsObject:message]){
            index = idx;
            *stop = YES;
        }
    }];
    return index;
}

-(BOOL)isTheNextMessageHasSameSender:(NSIndexPath*)currentIndex {
    if (_messages.count > 0) {
        GroupedMessages *currentMessageGroup = [_messages objectAtIndex:_messages.count-currentIndex.item-1];
        Message * currentMessage = currentMessageGroup.lastMessage;
        if (!currentMessage.senderId)
            return NO;
        if (_messages.count - currentIndex.item  >= 2) {
            GroupedMessages *nextMessageGroup = [_messages objectAtIndex:_messages.count - currentIndex.item - 2];
            Message *nextMessage = nextMessageGroup.lastMessage;
            
            if(nextMessage.senderId && [currentMessage.senderId isEqualToString:nextMessage.senderId] && !currentMessage.callLog && !nextMessage.callLog && (currentMessage.type == MessageTypeChat || currentMessage.type == MessageTypeGroupChat)){
                return YES;
            }
        }
    }
    return NO;
}
-(BOOL)isThePrevoiusMessageHasSameSender:(NSIndexPath*)currentIndex {
    
    if (currentIndex.item > 0 && _messages.count > 1) {
        GroupedMessages *currentMessageGroup = [_messages objectAtIndex:[_messages count]-currentIndex.item-1];
        Message * currentMessage = currentMessageGroup.lastMessage;
        if (!currentMessage.senderId)
            return NO;
        if (_messages.count >= currentIndex.item) {
            GroupedMessages *nextMessageGroup = [_messages objectAtIndex:[_messages count]-currentIndex.item];
            Message * nextMessage = nextMessageGroup.lastMessage;
            if(nextMessage.senderId && [currentMessage.senderId isEqualToString:nextMessage.senderId] && !currentMessage.callLog && !nextMessage.callLog && (currentMessage.type == MessageTypeChat || currentMessage.type == MessageTypeGroupChat)){
                return YES;
            }
        }
    }
    return NO;
}
- (IBAction)didTapTitleButton:(UIButton *)sender {
    [self parentViewController].title = @" ";
    if(_conversation.type == ConversationTypeRoom) {
        // Open room details
        UIRoomDetailsViewController *roomDetailsViewController = [[UIStoryboardManager sharedInstance].roomDetailsStoryBoard instantiateViewControllerWithIdentifier:@"roomDetailsViewControllerID"];
        roomDetailsViewController.room = (Room *)_conversation.peer;
        roomDetailsViewController.fromView = self;
        if(_parentNavigationController){
            [_parentNavigationController pushViewController:roomDetailsViewController animated:YES];
        } else {
            [self.navigationController pushViewController:roomDetailsViewController animated:YES];
        }
    } else {
        // Open contact details
        UIContactDetailsViewController* contactDetailsViewController = (UIContactDetailsViewController*)[[UIStoryboardManager sharedInstance].contactsDetailsStoryBoard instantiateViewControllerWithIdentifier:@"contactDetailsViewControllerID"];
        contactDetailsViewController.contact = (Contact *) _conversation.peer;
        contactDetailsViewController.fromView = self;
        [self.navigationController pushViewController:contactDetailsViewController animated:YES];
    }
}

#pragma mark - 3D Touch actions

- (NSArray<id<UIPreviewActionItem>> *)previewActionItems {
    UIPreviewAction *muteAction = [UIPreviewAction actionWithTitle:NSLocalizedString(@"Silence", nil) style:UIPreviewActionStyleDefault handler:^(UIPreviewAction * _Nonnull action, UIViewController * _Nonnull previewViewController) {
        [_conversationsManagerService muteConversation:_conversation];
    }];
    
    UIPreviewAction *unmuteAction = [UIPreviewAction actionWithTitle:NSLocalizedString(@"Alert", nil) style:UIPreviewActionStyleDefault handler:^(UIPreviewAction * _Nonnull action, UIViewController * _Nonnull previewViewController) {
        [_conversationsManagerService unmuteConversation:_conversation];
    }];
    
    UIPreviewAction *closeAction = [UIPreviewAction actionWithTitle:NSLocalizedString(@"Close",nil) style:UIPreviewActionStyleDestructive handler:^(UIPreviewAction * _Nonnull action, UIViewController * _Nonnull previewViewController) {
        [_conversationsManagerService stopConversation:_conversation];
    }];
    
    if(_conversation.isMuted)
        return @[unmuteAction, closeAction];
    else
        return @[muteAction, closeAction];
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    NSUInteger newLength = (textView.text.length - range.length) + text.length;
    if(newLength <= kMaxTextLength) {
        return YES;
    } else {
        NSUInteger emptySpace = kMaxTextLength - (textView.text.length - range.length);
        textView.text = [[[textView.text substringToIndex:range.location]
                          stringByAppendingString:[text substringToIndex:emptySpace]]
                         stringByAppendingString:[textView.text substringFromIndex:(range.location + range.length)]];
        return NO;
    }
}

#pragma mark - Scroll to Bottom button actions
- (IBAction)didTapScrollToBottomButton:(UIButton *)sender {
    self.allowAutomaticallyScrollsToMostRecentMessage = YES;
    self.automaticallyScrollsToMostRecentMessage = YES;
    [self showScrollToBottomButton:NO];
    [self scrollToBottomAnimated:YES];
}

#pragma mark - Keyboard show/hide notifications
- (void)keyboardDidShow: (NSNotification *) notif{
    [self adjustScrollToBottomButtonFrame];
}

- (void)keyboardDidHide: (NSNotification *) notif{
    [self adjustScrollToBottomButtonFrame];
}

#pragma mark - Call WebRTC buttons

//-(void) showHideCallButton {
//    if(![NSThread isMainThread]){
//        dispatch_async(dispatch_get_main_queue(), ^{
//            [self showHideCallButton];
//        });
//        return;
//    }
//    NSMutableArray <UIBarButtonItem*> *buttons = [NSMutableArray array];
////    if (_moreMenuButton)
////        [buttons addObject:_moreMenuButton];
//
//    if(_conversation.type == ConversationTypeUser){
//        Contact *aContact  = (Contact *)_conversation.peer;
//        BOOL isAllowedToUseWebRTCMobile = aContact.canCallInWebRTC;
//        if (isAllowedToUseWebRTCMobile || aContact.phoneNumbers.count) {
////            if (_moreMenuButton)
////                [_moreMenuButton setImageInsets:UIEdgeInsetsMake(0, -15, 0, 0)];
////            [_callWebRTCButton setImageInsets:UIEdgeInsetsMake(0, 0, 0, -15)];
////            [buttons addObject:_callWebRTCButton];
//        }
//
//    } else if(_conversation.type == ConversationTypeRoom){
//        Room *room = (Room *)_conversation.peer;
//
//        BOOL isAllowedToJoin =  [[ServicesManager sharedInstance].rtcService hasActiveCalls];
//
//        if(room.canJoin && !isAllowedToJoin){
////            if (_moreMenuButton)
////                [_moreMenuButton setImageInsets:UIEdgeInsetsMake(0, -15, 0, 0)];
////            [_joinConferenceButton setImageInsets:UIEdgeInsetsMake(0, 0, 0, -15)];
////            _joinConferenceButton.enabled = YES;
////            [buttons addObject:_joinConferenceButton];
//        }
//    }
////    UINavigationItem *navItem = [self goodNavigationItem];
////    navItem.rightBarButtonItems = buttons;
//}

//- (IBAction)callWebRTCButtonClicked:(UIBarButtonItem *)sender {
//    if (isNetworkConnected) {
//        UIAlertController *ctrl = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
//
//        BOOL isAllowedToUseWebRTCMobile = ((Contact *)_conversation.peer).canCallInWebRTC;
//
//        if (isAllowedToUseWebRTCMobile) {
//            UIAlertAction *callVideoAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"RTC video call", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//                [self makeCallTo:(Contact *)_conversation.peer features:(RTCCallFeatureAudio|RTCCallFeatureLocalVideo)];
//                // [FIRAnalytics logEventWithName:@"start_webRTC_Call" parameters:@{@"typeOfCall":@"RTC video call"}];
//
//            }];
//            callVideoAction.enabled = ((Contact*)_conversation.peer).canCallInWebRTCVideo;
//
//            [callVideoAction setValue:[[UIImage imageNamed:@"video"]  imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forKey:@"image"];
//            [ctrl addAction:callVideoAction];
//
//            UIAlertAction *callAudioAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"RTC audio call", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//                //[FIRAnalytics logEventWithName:@"start_webRTC_Call" parameters:@{@"typeOfCall":@"RTC audio call"}];
//                [self makeCallTo:(Contact *)_conversation.peer features:RTCCallFeatureAudio];
//            }];
//
//            [callAudioAction setValue:[[UIImage imageNamed:@"headset"]  imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forKey:@"image"];
//            [ctrl addAction:callAudioAction];
//        }
//
//        // Contact (caller or callee) phone numbers
//        Contact *contact = (Contact*)_conversation.peer;
//        [contact.phoneNumbers enumerateObjectsUsingBlock:^(PhoneNumber * aPhoneNumber, NSUInteger idx, BOOL * stop) {
//            NSString *label = aPhoneNumber.label;
//            if (aPhoneNumber.deviceType == PhoneNumberDeviceTypeMobile){
//                switch (aPhoneNumber.type) {
//                    case PhoneNumberTypeHome:
//                        label = NSLocalizedString(@"Personal Mobile", nil);
//                        break;
//                    case PhoneNumberTypeWork:
//                        label = NSLocalizedString(@"Professional Mobile", nil);
//                        break;
//                    case PhoneNumberTypeOther:
//                        label = NSLocalizedString(@"Professional Mobile", nil);
//                        break;
//                    default:
//                        label = NSLocalizedString(@"Cell", nil);
//                        break;
//                }
//            } else if (aPhoneNumber.deviceType == PhoneNumberDeviceTypeLandline){
//                switch (aPhoneNumber.type) {
//                    case PhoneNumberTypeHome:
//                        label = NSLocalizedString(@"Personal", nil);
//                        break;
//                    case PhoneNumberTypeWork:
//                        label = NSLocalizedString(@"Professional", nil);
//                        break;
//                    default:
//                        label = NSLocalizedString(@"Work", nil);
//                        break;
//                }
//            } else {
//                label = NSLocalizedString(aPhoneNumber.label, nil);
//            }
//
//            UIAlertAction *callAction = [UIAlertAction actionWithTitle:[NSString stringWithFormat:@"%@ : %@",label, aPhoneNumber.number] style:UIAlertActionStyleDefault handler: ^(UIAlertAction * action) {
//                //[FIRAnalytics logEventWithName:@"start_normal_Call" parameters:@{@"typeOfCall":@"phone call"}];
//
//                [UITools makeCallToPhoneNumber:aPhoneNumber inController:self];
//            }];
//
//            [ctrl addAction:callAction];
//        }];
//
//        UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style:UIAlertActionStyleCancel handler:nil];
//        [ctrl addAction:cancel];
//
//        [ctrl.view setTintColor:[UITools defaultTintColor]];
//        if (ctrl.actions.count > 1) {
//            [self presentViewController:ctrl animated:YES completion:nil];
//        }
//    }
//    else{
//        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"You need to be connected to your network to make calls" message:@"Can't Make Call" preferredStyle:UIAlertControllerStyleAlert];
//        UIAlertAction *ok =[UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault handler:nil];
//
//        [alertController addAction:ok];
//        [self presentViewController:alertController animated:YES completion:nil];
//    }
//
//}

//-(void) makeCallTo:(Contact *) contact features:(RTCCallFeatureFlags) features {
//    if([ServicesManager sharedInstance].rtcService.microphoneAccessGranted){
//        [[ServicesManager sharedInstance].rtcService beginNewOutgoingCallWithPeer:contact withFeatures:features];
//
//    } else {
//        [UITools showMicrophoneBlockedPopupInController:self];
//    }
//}

#pragma mark - join conference button
//- (IBAction)joinConferenceButtonClicked:(UIBarButtonItem *)sender {
//    [self didSelectPhoneNumber:nil callBack:NO];
//}

#pragma mark - JSQMessages left accessory button (upload files)
- (void)showInputBarLeftAccessoryButtonMenu {
    if (_leftAccessoryButtonActionSheet) {
        [_leftAccessoryButtonActionSheet dismissViewControllerAnimated:NO completion:nil];
        _leftAccessoryButtonActionSheet = nil;
    }
    
    _leftAccessoryButtonActionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    __weak __typeof__(self) weakSelf = self;
    
    UIAlertAction *iCloudAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"iCloud Drive", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        [weakSelf didTapICloudDrive:action];
    }];
    [iCloudAction setValue:[[UIImage imageNamed:@"iCloud"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forKey:@"image"];
    
    [_leftAccessoryButtonActionSheet addAction:iCloudAction];
    
    UIAlertAction *myFilesFromServerAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"My Rainbow share", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        [weakSelf didTapMyFilesAction:action];
    }];
    [myFilesFromServerAction setValue:[UIImage imageNamed:@"folderRainbow"] forKey:@"image"];
    
    [_leftAccessoryButtonActionSheet addAction:myFilesFromServerAction];
    
    UIAlertAction* uploadImageFromLibraryAction = [UIAlertAction actionWithTitle: NSLocalizedString(@"Gallery", @"")  style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        [weakSelf didTapUploadImageAction:action withSource:UIImagePickerControllerSourceTypePhotoLibrary];
    }];
    [uploadImageFromLibraryAction setValue:[[UIImage imageNamed:@"folder"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forKey:@"image"];
    [_leftAccessoryButtonActionSheet addAction:uploadImageFromLibraryAction];
    
    UIAlertAction* uploadImageFromCameraAction = [UIAlertAction actionWithTitle: NSLocalizedString(@"Camera", nil)  style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        [weakSelf didTapUploadImageAction:action withSource:UIImagePickerControllerSourceTypeCamera];
    }];
    [uploadImageFromCameraAction setValue:[[UIImage imageNamed:@"camera"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forKey:@"image"];
#if TARGET_OS_IPHONE && !TARGET_IPHONE_SIMULATOR
    [_leftAccessoryButtonActionSheet addAction:uploadImageFromCameraAction];
#endif
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style:UIAlertActionStyleCancel handler:nil];
    [_leftAccessoryButtonActionSheet addAction:cancelAction];
    
    // show the menu.
    [_leftAccessoryButtonActionSheet.view setTintColor:[UITools defaultTintColor]];
    [self presentViewController:_leftAccessoryButtonActionSheet animated:YES completion:nil];
}

- (void)didPressAccessoryButton:(UIButton *)sender{
    [self showInputBarLeftAccessoryButtonMenu];
}

-(void) showAllowAccessPhotoAndCameraPopup {
    SCLAlertView *alert = [[SCLAlertView alloc] initWithNewWindow];
    
    [alert addButton:NSLocalizedString(@"Go to Settings", nil) actionBlock:^(void) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString] options:@{} completionHandler:nil];
    }];
    
    alert.customViewColor = [UITools defaultTintColor];
    alert.iconTintColor = [UITools defaultBackgroundColor];
    alert.backgroundViewColor = [UITools defaultBackgroundColor];
    alert.statusBarStyle = UIStatusBarStyleLightContent;
    [alert setTitleFontFamily:[UITools boldFontName] withSize:16.0];
    [alert setBodyTextFontFamily:[UITools defaultFontName] withSize:13.0];
    alert.shouldDismissOnTapOutside = YES;
    
    [alert showInfo:self
              title:NSLocalizedString(@"Access photos", nil)
           subTitle:NSLocalizedString(@"Please allow access to photos and camera from your device settings", nil)
   closeButtonTitle:NSLocalizedString(@"Close", nil)
           duration:0.0f];
}

#pragma mark - left menu actions
-(void) didTapMyFilesAction:(UIAlertAction *) sender {
    UINavigationController *viewController = (UINavigationController*) [[UIStoryboardManager sharedInstance].myFilesStoryBoard instantiateViewControllerWithIdentifier:@"myFilesOnServerNavControllerID"];
    viewController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    
    [self presentViewController:viewController animated:YES completion:nil];
}

- (void)didTapUploadImageAction:(UIAlertAction *)sender withSource:(UIImagePickerControllerSourceType)source {
    
    if (source == UIImagePickerControllerSourceTypeCamera) {
        [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if (granted) {
                    // Already authorized
                    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
                    imagePickerController.delegate = self;
                    imagePickerController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
                    imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
                    imagePickerController.mediaTypes = [NSArray arrayWithObjects:(NSString *)kUTTypeMovie, (NSString *)kUTTypeImage, nil];
                    [self presentViewController:imagePickerController animated:YES completion:nil];
                    
                } else {
                    [self showAllowAccessPhotoAndCameraPopup];
                }
            });
        }];
        
    } else {
        // Else photo access
        [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if(status == PHAuthorizationStatusAuthorized) {
                    // Already authorized
                    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
                    imagePickerController.delegate = self;
                    imagePickerController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
                    if (source == UIImagePickerControllerSourceTypePhotoLibrary && [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary])
                        imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                    else if(source == UIImagePickerControllerSourceTypePhotoLibrary)
                        imagePickerController.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
                    
                    imagePickerController.mediaTypes = [NSArray arrayWithObjects:(NSString *)kUTTypeMovie, (NSString *)kUTTypeImage, nil];
                    
                    [self presentViewController:imagePickerController animated:YES completion:nil];
                    
                } else {
                    [self showAllowAccessPhotoAndCameraPopup];
                }
            });
        }];
    }
}

-(void) didTapICloudDrive:(UIAlertAction *) sender {
    UIDocumentPickerViewController *documentPicker = [[UIDocumentPickerViewController alloc] initWithDocumentTypes:@[@"public.data"] inMode:UIDocumentPickerModeImport];
    documentPicker.delegate = self;
    
    documentPicker.modalPresentationStyle = UIModalPresentationFormSheet;
    documentPicker.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    
    [self presentViewController:documentPicker animated:YES completion:^{
        if (@available(iOS 11, *)) {
            // iOS 11 (or newer)
            NSObject<UIAppearance> *appearance = [UINavigationBar appearance];
            self.previousNavBarTintColor = [(UINavigationBar *)appearance tintColor];
            _previousTitleTextAttributesColor = [(UINavigationBar *)appearance titleTextAttributes];
            [(UINavigationBar *)appearance setTintColor:[UITools defaultTintColor]];
        }
    }];
}

#pragma mark - UIImagePickerControllerDelegate protocol
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info{
    
    
    NSURL *assetURL;
    if([info objectForKey:UIImagePickerControllerReferenceURL])
        assetURL = info[UIImagePickerControllerReferenceURL];
    else if([info objectForKey:UIImagePickerControllerMediaURL])
        assetURL = info[UIImagePickerControllerMediaURL];
    
    NSString *mediaType = [info objectForKey: UIImagePickerControllerMediaType];
    
    PHFetchResult<PHAsset *> *asset = nil;
    
    if([picker sourceType] == UIImagePickerControllerSourceTypeCamera) {
        __block NSString *assetLocalIdentifier;
        [[PHPhotoLibrary sharedPhotoLibrary] performChangesAndWait:^{
            PHAssetChangeRequest *assetChangeRequest = nil;
            if([mediaType isEqualToString:(NSString *)kUTTypeVideo] || [mediaType isEqualToString:(NSString *)kUTTypeMovie]){
                assetChangeRequest = [PHAssetChangeRequest creationRequestForAssetFromVideoAtFileURL:assetURL];
            } else {
                UIImage *pickedImage = info[UIImagePickerControllerOriginalImage];
                assetChangeRequest = [PHAssetChangeRequest creationRequestForAssetFromImage:pickedImage];
            }
            
            NSLog(@"PlaceHolder %@",assetChangeRequest.placeholderForCreatedAsset);
            assetLocalIdentifier = assetChangeRequest.placeholderForCreatedAsset.localIdentifier;
            
        } error:nil];
        
        if (assetLocalIdentifier)
            asset = [PHAsset fetchAssetsWithLocalIdentifiers:@[assetLocalIdentifier] options:nil];
    } else {
        if(assetURL){
            asset = [PHAsset fetchAssetsWithALAssetURLs:@[assetURL] options:nil];
        }
    }
    
    
    __block NSString *fileName = nil;
    if(asset){
        fileName = [asset.firstObject valueForKey:@"filename"];
    }
    
    __block NSData *dataToSend = nil;
    __block NSURL * cacheURL = nil;
    if(asset.count > 0){
        NSURL *cache = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/%@",[UITools applicationDocumentsCache], fileName]];
        cacheURL = cache;
        //TODO: add loader , here we are blocking the UI
        dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);
        [self downloadAsset:asset.firstObject toURL:cache completion:^{
            dataToSend = [NSData dataWithContentsOfURL:cache];
            dispatch_semaphore_signal(semaphore);
        }];
        dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
    }
    
    [picker dismissViewControllerAnimated:YES completion:^{
        // If no filename or image has been converted - redefine the name
        if(fileName.length==0 || ![[dataToSend extension] isEqualToString:[assetURL pathExtension]]){
            NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
            [dateFormat setDateFormat:@"ddMMyyyy_HHmm"];
            fileName = [NSString stringWithFormat:@"IMG_%@.%@", [dateFormat stringFromDate:[NSDate date]], [dataToSend extension]];
        }
        
//        _attachmentFileToSend = [[ServicesManager sharedInstance].fileSharingService createTemporaryFileWithFileName:fileName andData:dataToSend andURL:cacheURL];
//
//        if(asset && _attachmentFileToSend.type == FileTypeVideo)
//            _attachmentPreview = [self generateThumbnailFromAvAsset:asset.firstObject];
        
//        [self showAttachmentView];
    }];
}

-(UIImage *) generateThumbnailFromAvAsset:(PHAsset *) videoAsset {
    dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);
    PHVideoRequestOptions *option = [PHVideoRequestOptions new];
    __block AVAsset *resultAsset;
    [[PHImageManager defaultManager] requestAVAssetForVideo:videoAsset options:option resultHandler:^(AVAsset * _Nullable asset, AVAudioMix * _Nullable audioMix, NSDictionary * _Nullable info) {
        resultAsset = asset;
        dispatch_semaphore_signal(semaphore);
    }];
    
    dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
    
    AVAssetImageGenerator *imageGenerator = [[AVAssetImageGenerator alloc]initWithAsset:resultAsset];
    CMTime time = CMTimeMake(1, 1);
    CGImageRef imageRef = [imageGenerator copyCGImageAtTime:time actualTime:NULL error:NULL];
    UIImage *thumbnail = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);  // CGImageRef won't be released by ARC
    
    return thumbnail;
}

- (void)downloadAsset:(PHAsset *)asset toURL:(NSURL *)url completion:(void (^)(void))completion {
    if (asset.mediaType == PHAssetMediaTypeImage) {
        PHImageRequestOptions *options = [PHImageRequestOptions new];
        options.networkAccessAllowed = YES;
        options.version = PHImageRequestOptionsVersionCurrent;
        options.deliveryMode = PHImageRequestOptionsDeliveryModeHighQualityFormat;
        options.synchronous = YES;
        [[PHImageManager defaultManager] requestImageDataForAsset:asset options:options resultHandler:^(NSData * _Nullable imageData, NSString * _Nullable dataUTI, UIImageOrientation orientation, NSDictionary * _Nullable info) {
            // Convert HEIC to JPEG for not iOS device compatibility
            if( [[url pathExtension] isEqualToString:@"HEIC"] || [[url pathExtension] isEqualToString:@"HEIF"]) {
                UIImage *im = [UIImage imageWithData:imageData];
                imageData = UIImageJPEGRepresentation(im, 0.9);
            }
            
            if ([info objectForKey:PHImageErrorKey] == nil && [[NSFileManager defaultManager] createFileAtPath:url.path contents:imageData attributes:nil]) {
                NSLog(@"downloaded photo:%@", url.path);
            }
            completion();
        }];
    } else if (asset.mediaType == PHAssetMediaTypeVideo) {
        PHVideoRequestOptions *options = [PHVideoRequestOptions new];
        options.networkAccessAllowed = YES;
        options.version = PHVideoRequestOptionsVersionCurrent;
        options.deliveryMode = PHVideoRequestOptionsDeliveryModeAutomatic;
        [[PHImageManager defaultManager] requestExportSessionForVideo:asset options:options exportPreset:AVAssetExportPresetHighestQuality resultHandler:^(AVAssetExportSession * _Nullable exportSession, NSDictionary * _Nullable info) {
            if ([info objectForKey:PHImageErrorKey] == nil)
            {
                exportSession.outputURL = url;
                
                NSArray<PHAssetResource *> *resources = [PHAssetResource assetResourcesForAsset:asset];
                for (PHAssetResource *resource in resources)
                {
                    exportSession.outputFileType = resource.uniformTypeIdentifier;
                    if (exportSession.outputFileType != nil)
                        break;
                }
                
                [exportSession exportAsynchronouslyWithCompletionHandler:^{
                    if (exportSession.status == AVAssetExportSessionStatusCompleted){
                        NSLog(@"downloaded video:%@", url.path);
                    }
                    completion();
                }];
            }
        }];
    }
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [picker dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Attachment View
//-(void) showAttachmentView {
////    if (!_scrollToBottomButton.isHidden) {
////        _scrollToBottomButton.hidden = YES;
////    }
////
////    if(_attachmentView.superview){
////        [_attachmentView removeFromSuperview];
////        _attachmentImageView.image = nil;
////    }
//    BOOL isLandscape = UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation);
////    if(_attachmentFileToSend.type != FileTypeImage && _attachmentFileToSend.type != FileTypeVideo){
////        _attachmentImageHeight.constant = isLandscape?44:64;
////        _attachmentFileNameLabel.hidden = NO;
////    } else {
////        _attachmentImageHeight.constant = isLandscape?100:200;
////        _attachmentFileNameLabel.hidden = YES;
////    }
//    CGFloat height = _attachmentImageHeight.constant;
//    if(_attachmentFileToSend.type != FileTypeImage && _attachmentFileToSend.type != FileTypeVideo)
//        height += _attachmentFileNameLabel.frame.size.height + 20;
//
//    [self adjustAttachmentViewFrameWithHeight:height];
//    _attachmentFileNameLabel.text = _attachmentFileToSend.fileName;
//    if(_attachmentPreview)
//        _attachmentImageView.image = _attachmentPreview;
//    else
//        _attachmentImageView.image = _attachmentFileToSend.thumbnailBig;
//    _attachmentImageView.tintColor = [UITools defaultTintColor];
//
//    [self.inputToolbar.contentView addSubview:_attachmentView];
//    // Very important ! don't forget to enable user interaction to handle events.
//    _attachmentView.userInteractionEnabled = YES;
//
//    [self jsq_updateCollectionViewInsets];
//    [self updateConversationStatusAndToggleSendButton];
//}

-(void)jsq_updateCollectionViewInsets {
    CGFloat inputBar = CGRectGetMinY(self.inputToolbar.frame);
    [self jsq_setCollectionViewInsetsTopValue:self.topLayoutGuide.length + self.topContentAdditionalInset bottomValue:CGRectGetMaxY(self.collectionView.frame) - inputBar];
}

//-(IBAction)closeAttachementButtonClicked:(UIButton *)sender {
//    if(_attachmentView.superview){
//        if (_scrollToBottomButton.isHidden) {
//            _scrollToBottomButton.hidden = NO;
//        }
//        [_attachmentView removeFromSuperview];
//        _attachmentImageView.image = nil;
//        [self jsq_updateCollectionViewInsets];
//        [self updateConversationStatusAndToggleSendButton];
//        _attachmentFileToSend = nil;
//        _attachmentPreview = nil;
//    }
//}

//-(void) adjustAttachmentViewFrameWithHeight:(CGFloat) heigth {
//    CGRect frame = _attachmentView.frame;
//    frame.size.height = heigth;
//    frame.origin.y = -frame.size.height;
//    frame.size.width = [UIScreen mainScreen].bounds.size.width;
//
//    _attachmentView.frame = frame;
//}
//- (IBAction)didTapOnAttachmentViewImage:(UITapGestureRecognizer *)sender {
//    if(_attachmentFileToSend)
//        [self openPreviewControllerForAttachment:_attachmentFileToSend];
//}

#pragma mark - iCloud files
- (void)documentPicker:(UIDocumentPickerViewController *)controller didPickDocumentAtURL:(NSURL *)url {
    if (controller.documentPickerMode == UIDocumentPickerModeImport) {
//        NSData *data = [NSData dataWithContentsOfURL:url];
//        _attachmentFileToSend = [[ServicesManager sharedInstance].fileSharingService createTemporaryFileWithFileName:[url lastPathComponent] andData:data andURL:url];
//
//        [self showAttachmentView];
        
    }
    else if (controller.documentPickerMode == UIDocumentPickerModeExportToService){
        MBProgressHUD *openingHud = [UITools showHUDWithMessage:NSLocalizedString(@"Saved",@"Saved") forView:self.view mode:MBProgressHUDModeCustomView imageName:@"Checkmark" dismissCompletionBlock:nil];
        [openingHud show:YES];
        
    }
    if (@available(iOS 11, *)) {
        [[UINavigationBar appearance] setTintColor:self.previousNavBarTintColor];
        [[UINavigationBar appearance] setBarTintColor:self.previousNavBarTintColor];
        [[UINavigationBar appearance] setTitleTextAttributes:_previousTitleTextAttributesColor];
    }
}

-(void)documentPickerWasCancelled:(UIDocumentPickerViewController *)controller {
    if (@available(iOS 11, *)) {
        [[UINavigationBar appearance] setTintColor:self.previousNavBarTintColor];
        [[UINavigationBar appearance] setBarTintColor:self.previousNavBarTintColor];
        [[UINavigationBar appearance] setTitleTextAttributes:_previousTitleTextAttributesColor];
    }
}

-(void) didUpdateProgress:(NSNotification *) notification {
    
    NSDictionary * objects = (NSDictionary *) notification.object;
    NSNumber * valueNumber = (NSNumber *)[objects objectForKey:@"value"];
    NSString * tag =[objects objectForKey:@"tag"];
    dispatch_async(dispatch_get_main_queue(), ^{
        for (JSQRainbowOutgoingMessagesCollectionViewCell *cell in self.collectionView.visibleCells) {
            NSIndexPath *path = [self.collectionView indexPathForCell:cell];
            GroupedMessages *message = [_messages objectAtIndex:[self groupedMessagesIndexForRowIndex:path]];
            Message *lastMessage = message.lastMessage;
            if ([tag  isEqualToString: lastMessage.messageID]) {
                [cell setProgressViewWithValue:[valueNumber floatValue]];
            }
        }
    });
}

-(void) didUpdateDownloadedProgress:(NSNotification *) notification {
    
    NSDictionary * objects = (NSDictionary *) notification.object;
    NSNumber * valueNumber = (NSNumber *)[objects objectForKey:@"value"];
    NSString * tag = [objects objectForKey:@"tag"];
    NSString * fileURL = [NSString stringWithFormat:@"%@",[objects objectForKey:@"fileURL"]];
    dispatch_async(dispatch_get_main_queue(), ^{
        
        for (JSQRainbowMessagesCollectionViewCell *cell in self.collectionView.visibleCells) {
            NSIndexPath *path = [self.collectionView indexPathForCell:cell];
            GroupedMessages *message = [_messages objectAtIndex:[self groupedMessagesIndexForRowIndex:path]];
            Message *lastMessage = message.lastMessage;
            if (([tag  isEqualToString: lastMessage.messageID]) && ([fileURL isEqualToString:[NSString stringWithFormat:@"%@",cell.attachment.url]])) {
                cell.progressView.hidden = NO;
                cell.cellDateLabel.hidden = YES;
                [cell setProgressViewWithValue:[valueNumber floatValue] ];
            }
        }
    });
}

-(void) showErrorPopupWithTitle:(NSString *) title messsage:(NSString *) message {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self showErrorPopupWithTitle:title messsage:message];
        });
        return;
    }
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
    
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
}

#pragma mark - File Actions

//-(void) addActionsView {
//
//    _actionsView.frame = CGRectMake(0, [UIScreen mainScreen].bounds.size.height - topPadding , [UIScreen mainScreen].bounds.size.width, 64);
//    [self.view addSubview:_actionsView];
//}

- (void) setupActionsViewAtCell:(JSQRainbowMessagesCollectionViewCell *) cell  {
//    _actionViewTabBar.tintColor = [[UITools defaultTintColor] colorWithAlphaComponent:0.8];
//    if (@available(iOS 10, *)) {
//        [_actionViewTabBar setUnselectedItemTintColor:[UITools defaultTintColor]];
//    }
//
//    // remove the delete if it's not my file
//    NSIndexPath *path = [self.collectionView indexPathForCell:cell];
//    GroupedMessages *message = [_messages objectAtIndex:[self groupedMessagesIndexForRowIndex:path]];
//    Message *lastMessage = message.lastMessage;
//    NSMutableArray* items = [NSMutableArray arrayWithArray:[_actionViewTabBar items]];
//    if(!lastMessage.isOutgoing){
//        if([items count] == 4)
//            [items removeLastObject];
//    } else {
//        if(![items containsObject:_deleteTabBarItem])
//            [items insertObject:_deleteTabBarItem atIndex:3];
//    }
//    for (UITabBarItem *item in items) {
//        item.title = NSLocalizedString(item.title, nil);
//    }
//    [self.actionViewTabBar setItems:items];
}

- (GroupedMessages *) getMessageForFile:(File *)file{
    for (GroupedMessages * theMessage in _messages) {
        Message * aMessage = theMessage.lastMessage;
        if (aMessage.attachment == file) {
            return theMessage;
        }
    }
    return nil;
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
    [self.collectionView.collectionViewLayout invalidateLayoutWithContext:[JSQMessagesCollectionViewFlowLayoutInvalidationContext context]];
}

#pragma mark - Search messages

-(void)showSearchMessageViewBarWithSearchField:(BOOL)showSearchBar {
    CGRect frame = _searchMessageViewBar.frame;
    frame.size.width = [UIScreen mainScreen].bounds.size.width;
    _searchMessageViewBar.frame = frame;
    _searchMessageViewBar.hidden = NO;
    _searchMessageViewBar.translatesAutoresizingMaskIntoConstraints = YES;
    [_searchMessageViewBar setBackgroundColor:[UITools backgroundGrayColor]];
    [_searchMessageTextfield setPlaceholder:NSLocalizedString(@"Search", nil)];

    [self setTextfieldVisible:showSearchBar];
    [self updateSearchMessageButtonsActive];
    
    _searchMessageButtonPrevious.tintColor = [UITools defaultTintColor];
    _searchMessageButtonNext.tintColor = [UITools defaultTintColor];
    _searchMessageButtonSearch.tintColor = [UITools defaultTintColor];
    _searchMessageButtonClose.tintColor = [UITools defaultTintColor];
    
    [self.view addSubview:_searchMessageViewBar];
    [_searchMessageViewBar setNeedsLayout];
    [self calculateTopContentAdditionalInset];
}

-(void)setTextfieldVisible:(BOOL)visible {
    [_searchMessageTextfield setHidden:!visible];
    [_searchMessageLabel setHidden:visible];
    
    if(visible) {
        if([_searchMessageTextfield.text length] == 0 && [_searchMessageText length] > 0)
            [_searchMessageTextfield setText:_searchMessageText];
        
        [_searchMessageTextfield becomeFirstResponder];
    } else {
        [_searchMessageTextfield resignFirstResponder];
    }
}

-(void)hideSearchMessageViewBar {
    if(_searchMessageViewBar.hidden)
        return;
    
    [_searchMessageTextfield resignFirstResponder];
    _searchMessageViewBar.hidden = YES;
    CGRect frame = _searchMessageViewBar.frame;
    frame.origin.y = 0;
    _searchMessageViewBar.frame = frame;
    [_searchMessageViewBar removeFromSuperview];
    [self calculateTopContentAdditionalInset];
}

- (IBAction)didTapSearchMessageButtonPreviousNext:(UIButton *)sender {
    if([sender isEqual:_searchMessageButtonPrevious]) {
        [self moveResultPositionToIndex:_searchMessageCurrentOccurence+1];
    } else if([sender isEqual:_searchMessageButtonNext]) {
        [self moveResultPositionToIndex:_searchMessageCurrentOccurence-1];
    }
}

- (IBAction)didTapSearchMessageButtonSearch:(UIButton *)sender {
    if(_searchMessageTextfield.hidden) {
        [self setTextfieldVisible:YES];
    }
}

- (IBAction)didEndEditingSearchMessageText:(UITextField *)sender {
    if(_searchMessageTextfield.hidden)
        return;
    // Send search request
    [self searchInConversation:_conversation withText:sender.text];
}

-(void)searchInConversation:(Conversation *)conversation withText:(NSString *) textToSearch {
    if([textToSearch length] < 3)
        return;
    
    //
    _searchMessageLabel.text = NSLocalizedString(@"Searching ...", nil);
    [self resetSearchMessageState];

    if(_searchMessageOccurencesTimestamps)
       [_searchMessageOccurencesTimestamps removeAllObjects];
    else
        _searchMessageOccurencesTimestamps = [NSMutableArray new];
    
    // Send search request
    [[ServicesManager sharedInstance].conversationsSearchHelper searchInConversationWithPeer:_conversation.peer textToSearch:textToSearch];
    
    // Update layout
    [self showSearchMessageViewBarWithSearchField:NO];
}

- (IBAction)didTapSearchMessageButtonClose:(UIButton *)sender {
    if(_fromView)
        [self.navigationController popViewControllerAnimated:YES];
    else {
        [self.navigationController popViewControllerAnimated:NO];
        UIWindow* window = [[UIApplication sharedApplication].windows firstObject];
        [RecentsConversationsTableViewController openConversationViewForConversation:_conversation inViewController:[window.rootViewController.childViewControllers firstObject]];
    }
}

-(void) resetSearchMessageState {
    _searchMessageOccurencesCount = 0;
    _searchMessageFirstOccurence = 0;
    _searchMessageLastOccurence = 0;
    _searchMessageCurrentOccurence = 0;
    _searchMessageCurrentOccurenceIndexPath = nil;
}

-(void)didFoundResultsInConversation:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didFoundResultsInConversation:notification];
        });
        return;
    }
    
    NSDictionary *results = (NSDictionary*)notification.object;
    _searchMessageText = [results valueForKey:@"string"];
    _searchMessageOccurencesCount = [[results valueForKey:@"count"] longLongValue];
    
    if(_searchMessageOccurencesCount > 0 && [_searchMessageOccurencesTimestamps count] > 0) {
        _searchMessageFirstOccurence = [[results valueForKey:@"firstOccurence"] longLongValue];
        _searchMessageLastOccurence = [[results valueForKey:@"lastOccurence"] longLongValue];
        _searchMessageCurrentOccurence = 0;
        _searchMessageCurrentOccurenceIndexPath = nil;
        
        [self moveResultPositionToIndex:_searchMessageCurrentOccurence];
        
        NSLog(@"didFoundResultsInConversation count: %lu first: %lu last: %lu", (unsigned long)_searchMessageOccurencesCount, (unsigned long)_searchMessageFirstOccurence, (unsigned long)_searchMessageLastOccurence);
    }
    
    else {
        _searchMessageLabel.text = NSLocalizedString(@"No result found", nil);
        [self setTextfieldVisible:NO];
    }
}

-(void)didFoundMessagesInConversation:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didFoundMessagesInConversation:notification];
        });
        return;
    }
    
    NSDictionary *results = (NSDictionary*)notification.object;
    
    if([results valueForKey:@"occurences"]) {
        NSArray<Message*> *foundOccurences = (NSArray<Message *> *)[results valueForKey:@"occurences"];

        [foundOccurences enumerateObjectsUsingBlock:^(Message * _Nonnull message, NSUInteger idx, BOOL * _Nonnull stop) {
            if(![_searchMessageOccurencesTimestamps containsObject:message.timestamp])
                [_searchMessageOccurencesTimestamps insertObject:message.timestamp atIndex:0];
        }];
        
    }
    
    else if([results valueForKey:@"messages"]) {
        NSArray<Message*> *foundMessages = (NSArray<Message *> *)[results valueForKey:@"messages"];

        @synchronized(_messages) {
            [_messages removeAllObjects];
            __block NSMutableArray <GroupedMessages*> *messagesCopy = [NSMutableArray new];
            
            [foundMessages enumerateObjectsUsingBlock:^(Message * _Nonnull message, NSUInteger idx, BOOL * _Nonnull stop) {
                GroupedMessages *gMessage = [[GroupedMessages alloc] initWithMessage:message];
                
                if(![messagesCopy containsObject:gMessage]) {
                    [messagesCopy addObject:gMessage];
                    
                    if(_searchMessageCurrentOccurence == 0 && [self compareSearchMessageTimestamp:_searchMessageCurrentOccurence withDate:gMessage.firstMessage.timestamp]) {
                        _searchMessageCurrentOccurenceIndexPath = [NSIndexPath indexPathForRow:[messagesCopy indexOfObject:gMessage] inSection:0];
                    }
                }
            }];
        
            _messages = [NSMutableArray arrayWithArray:messagesCopy];
            
            [self finishReceivingMessage];
            
            if(_searchMessageCurrentOccurenceIndexPath != nil) {
                [self scrollToIndexPath:_searchMessageCurrentOccurenceIndexPath animated:YES];
            }
        }
    }
}


-(NSAttributedString*) hightlightText:(NSString *)text withCell:(JSQMessagesCollectionViewCell *)cell {
    
    NSRange range = [cell.textView.text rangeOfString:text options:NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch];
    NSMutableAttributedString *attrText = [[NSMutableAttributedString alloc] initWithString:cell.textView.text];
    NSUInteger stringLength = [cell.textView.text length];
    
    // Font
    [attrText addAttribute:NSFontAttributeName value:[UIFont fontWithName:[UITools defaultFontName] size:14] range:NSMakeRange(0, stringLength)];
    [attrText addAttribute:NSForegroundColorAttributeName value:cell.textView.textColor range:NSMakeRange(0, stringLength)];
    
    if (range.location != NSNotFound) {
        // Underline text
        [attrText addAttribute:NSBackgroundColorAttributeName value:[UITools colorFromHexa:0xFBDD52FF] range:range];
        [attrText addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:range];
    }
    
    return attrText;
}

-(NSAttributedString *) resetAttributedTextWithCell:(JSQMessagesCollectionViewCell *)cell {
    NSMutableAttributedString *attrText = [[NSMutableAttributedString alloc] initWithAttributedString:cell.textView.attributedText];
    NSUInteger stringLength = [cell.textView.text length];
    
    [attrText removeAttribute:NSBackgroundColorAttributeName range:NSMakeRange(0, stringLength)];
    
    return attrText;
}

-(void) updateSearchMessageButtonsActive {
    [_searchMessageButtonPrevious setEnabled: (_searchMessageOccurencesCount > 0 && _searchMessageCurrentOccurence < _searchMessageOccurencesCount-1)];
    [_searchMessageButtonNext setEnabled: (_searchMessageCurrentOccurence > 0)];
}

-(BOOL) compareSearchMessageTimestamp:(NSUInteger)index withDate:(NSDate *) date {
    if(_searchMessageOccurencesTimestamps && [_searchMessageOccurencesTimestamps count] > index) {
        if([[_searchMessageOccurencesTimestamps objectAtIndex:index] isEqualToDate:date])
            return YES;
    }
    
    return NO;
}

-(void) moveResultPositionToIndex:(NSInteger)index {
    if(_searchMessageOccurencesTimestamps && [_searchMessageOccurencesTimestamps count] > index) {
        _searchMessageCurrentOccurence = index;
        
        if(_searchMessageOccurencesCount > 1)
            _searchMessageLabel.text = [NSString stringWithFormat: NSLocalizedString(@"%d of %d for « %@ »", nil) , (unsigned long)index+1, (unsigned long)_searchMessageOccurencesCount, _searchMessageText];
        else
            _searchMessageLabel.text = [NSString stringWithFormat: NSLocalizedString(@"1 result for « %@ »", nil), _searchMessageText];
        
        [self setTextfieldVisible:NO];
        [self updateSearchMessageButtonsActive];
        
        NSDate *d = [_searchMessageOccurencesTimestamps objectAtIndex:index];
        NSUInteger t = d.timeIntervalSince1970*10E5;
        
        [[ServicesManager sharedInstance].conversationsSearchHelper searchMessagesWithPeer:_conversation.peer beforeAndAfter:t];
    }
}

- (void)scrollToIndexPath:(NSIndexPath *)indexPath animated:(BOOL)animated
{
    if ([self.collectionView numberOfSections] <= indexPath.section) {
        return;
    }
    
    NSInteger numberOfItems = [self.collectionView numberOfItemsInSection:indexPath.section];
    if (numberOfItems == 0) {
        return;
    }
    
    CGFloat collectionViewContentHeight = [self.collectionView.collectionViewLayout collectionViewContentSize].height;
    BOOL isContentTooSmall = (collectionViewContentHeight < CGRectGetHeight(self.collectionView.bounds));
    
    if (isContentTooSmall) {
        //  workaround for the first few messages not scrolling
        //  when the collection view content size is too small, `scrollToItemAtIndexPath:` doesn't work properly
        //  this seems to be a UIKit bug, see #256 on GitHub
        [self.collectionView scrollRectToVisible:CGRectMake(0.0, collectionViewContentHeight - 1.0f, 1.0f, 1.0f)
                                        animated:animated];
        return;
    }
    
    NSInteger item = MAX(MIN(indexPath.item, numberOfItems - 1), 0);
    indexPath = [NSIndexPath indexPathForItem:item inSection:0];
    
    [self.collectionView scrollToItemAtIndexPath:indexPath
                                atScrollPosition:UICollectionViewScrollPositionCenteredVertically
                                        animated:animated];
}

@end
