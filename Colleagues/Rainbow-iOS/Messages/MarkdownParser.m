/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "MarkdownParser.h"
#import "XNGMarkdownParser.h"
#import "UITools.h"

static MarkdownParser *singleton = nil;

@interface MarkdownParser ()
    @property (nonatomic, strong) XNGMarkdownParser *markdownParser;
@end

@implementation MarkdownParser

+(MarkdownParser*) sharedInstance {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        singleton = [[MarkdownParser alloc] init];
    });
    return singleton;
}

-(instancetype) init {
    self = [super init];
    if(self){
        _markdownParser  = [[XNGMarkdownParser alloc] init];
        _markdownParser.paragraphFont = [UIFont fontWithName:UITools.defaultFontName size:14];
        _markdownParser.codeFontName = UITools.codeFontName;
        _markdownParser.boldFontName = UITools.boldFontName;
        _markdownParser.linkFontName = UITools.defaultFontName;
        [_markdownParser setFont:[UIFont fontWithName:UITools.defaultFontName size:24] forHeader:XNGMarkdownParserHeader1];
        [_markdownParser setFont:[UIFont fontWithName:UITools.defaultFontName size:20] forHeader:XNGMarkdownParserHeader2];
        [_markdownParser setFont:[UIFont fontWithName:UITools.defaultFontName size:18] forHeader:XNGMarkdownParserHeader3];
        [_markdownParser setFont:[UIFont fontWithName:UITools.defaultFontName size:16] forHeader:XNGMarkdownParserHeader4];
        [_markdownParser setFont:[UIFont fontWithName:UITools.defaultFontName size:14] forHeader:XNGMarkdownParserHeader5];
        [_markdownParser setFont:[UIFont fontWithName:UITools.defaultFontName size:14] forHeader:XNGMarkdownParserHeader6];
    }
    return self;
}

-(NSAttributedString *)attributedStringFromMarkDown:(NSString *)markdown {
    return [_markdownParser attributedStringFromMarkdownString:markdown];
}

@end
