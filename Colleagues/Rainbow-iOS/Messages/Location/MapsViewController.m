//
//  MapsViewController.m
//  Rainbow-iOS
//
//  Created by Alaa Bzour on 2/11/19.
//  Copyright © 2019 ALE International. All rights reserved.
//

#import "MapsViewController.h"
#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <Rainbow/ServicesManager.h>
#import <Rainbow/Location.h>

#define ZOOM_IN 150
#define ZOOM_OUT 400

@interface MapsViewController ()<MKMapViewDelegate> {
    double  longitude;
    double  latitude;
    CLPlacemark *placemark;
    MKPointAnnotation *userAnnotation;
    BOOL zoom;
}
@property (weak, nonatomic) IBOutlet UIToolbar *toolbar;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *serachBarButtonItem;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *shareBarButtonItem;
@property (weak, nonatomic) IBOutlet MKMapView *mapView;

@end

@implementation MapsViewController

#pragma mark - Application LifeCycle
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    zoom = NO;
    if (_selectedMessage.location.latitude && _selectedMessage.location.longitude) {
        longitude = _selectedMessage.location.longitude;
        latitude = _selectedMessage.location.latitude;
    }
    [self setupMapView];
    [self setupNavigationTitle];
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
                                initWithTitle:nil
                                style:UIBarButtonItemStylePlain
                                target:self
                                action:nil];
    
    self.navigationController.navigationBar.topItem.backBarButtonItem= backButton;
    
    NSArray *segmentItemsArray = [NSArray arrayWithObjects: NSLocalizedString(@"Map", nil), NSLocalizedString(@"Hybrid",nil), NSLocalizedString(@"Satellite",nil), nil];
    UISegmentedControl *mapsStyleSegmentedControl = [[UISegmentedControl alloc] initWithItems:segmentItemsArray];
    mapsStyleSegmentedControl.frame = CGRectMake(0, 0, 200, 30);
    mapsStyleSegmentedControl.selectedSegmentIndex = 0;
    [mapsStyleSegmentedControl addTarget:self action:@selector(mapStylesegmentedControlAction:) forControlEvents:UIControlEventValueChanged];
    UIBarButtonItem *segmentedControlButtonItem = [[UIBarButtonItem alloc] initWithCustomView:(UIView *)mapsStyleSegmentedControl];
    UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    NSArray *barArray = [NSArray arrayWithObjects: self.shareBarButtonItem, flexibleSpace, segmentedControlButtonItem, flexibleSpace,self.serachBarButtonItem, nil];
    [self.toolbar setItems:barArray];

}

- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.mapView.showsUserLocation = YES;
    
}

- (void) setupMapView {
    self.mapView.delegate = self;
    self.mapView.mapType = MKMapTypeStandard;
    self.mapView.showsUserLocation = YES;
    
    userAnnotation = [[MKPointAnnotation alloc] init];
    userAnnotation.coordinate = CLLocationCoordinate2DMake(latitude, longitude);
    if (_selectedMessage.isOutgoing) {
        userAnnotation.title =  [NSString stringWithFormat:@"%@ (%@)",[[[[ServicesManager sharedInstance]myUser] contact] displayName],NSLocalizedString(@"You", nil)];
    }
    else {
        userAnnotation.title = _selectedMessage.peer.displayName;
    }
 
    [self.mapView addAnnotation:userAnnotation];
}

- (void) getAddressFromLatLon:(CLLocation *)bestLocation
{

    
    CLGeocoder *geocoder = [[CLGeocoder alloc] init] ;
    [geocoder reverseGeocodeLocation:bestLocation
                   completionHandler:^(NSArray *placemarks, NSError *error)
     {
         if (error){
             NSLog(@"Geocode failed with error: %@", error);
             return;
         }
         placemark = [placemarks objectAtIndex:0];
         NSLog(@"placemark.ISOcountryCode %@",placemark.ISOcountryCode);
         NSLog(@"locality %@",placemark.locality);
         NSLog(@"postalCode %@",placemark.postalCode);
         
         [self updateNavigationTitle];
         
     }];
    
}

- (void) setupNavigationTitle {

    CLLocation * location = [[CLLocation alloc]initWithLatitude:latitude longitude:longitude];
    [self getAddressFromLatLon:location];
   
    UIActivityIndicatorView *activityView = [[UIActivityIndicatorView alloc]
                                             initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    
    activityView.center = self.view.center;
    self.navigationItem.titleView = activityView;
    [activityView startAnimating];
    

}

-(void) updateNavigationTitle {
    
    CGRect headerTitleSubtitleFrame = CGRectMake(0, 0, 200, 44);
    UIView* _headerTitleSubtitleView = [[UILabel alloc] initWithFrame:headerTitleSubtitleFrame] ;
    _headerTitleSubtitleView.backgroundColor = [UIColor clearColor];
    _headerTitleSubtitleView.autoresizesSubviews = YES;
    
    
    CGRect titleFrame = CGRectMake(0, 2, 200, 24);
    if (!placemark.locality.length) {
        titleFrame = CGRectMake(0, 0, 200, 44);
    }
    UILabel *titleView = [[UILabel alloc] initWithFrame:titleFrame] ;
    titleView.backgroundColor = [UIColor clearColor];
    titleView.font = [UIFont boldSystemFontOfSize:20];
    titleView.textAlignment = NSTextAlignmentCenter;
    titleView.textColor = [UIColor whiteColor];
    titleView.adjustsFontSizeToFitWidth = YES;
    [_headerTitleSubtitleView addSubview:titleView];
    
    CGRect subtitleFrame = CGRectMake(0, 24, 200, 20);
    UILabel *subtitleView = [[UILabel alloc] initWithFrame:subtitleFrame] ;
    subtitleView.backgroundColor = [UIColor clearColor];
    subtitleView.font = [UIFont boldSystemFontOfSize:13];
    subtitleView.textAlignment = NSTextAlignmentCenter;
    subtitleView.textColor = [UIColor whiteColor];
    
    if (@available(iOS 11.0, *)) {
        titleView.text = placemark.name;
        subtitleView.text = [NSString stringWithFormat:@"%@ %@", placemark.postalCode, placemark.locality];
    } else {
        titleView.text = placemark.country;
        subtitleView.text = [NSString stringWithFormat:@"%@",placemark.locality];
    }
    
    subtitleView.adjustsFontSizeToFitWidth = YES;
    if (placemark.locality.length) {
         [_headerTitleSubtitleView addSubview:subtitleView];
    }
   
    _headerTitleSubtitleView.autoresizingMask = (UIViewAutoresizingFlexibleLeftMargin |
                                                 UIViewAutoresizingFlexibleRightMargin |
                                                 UIViewAutoresizingFlexibleTopMargin |
                                                 UIViewAutoresizingFlexibleBottomMargin);
    
    self.navigationItem.titleView = _headerTitleSubtitleView;

}
- (void) viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.mapView = nil;
    self.selectedMessage = nil;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setMessage:(Message *)selectedMessage {
    _selectedMessage = selectedMessage;
}

#pragma mark - MKMapViewDelegate

- (void)mapView:(MKMapView *)mapView didFailToLocateUserWithError:(NSError *)error {
    NSLog(@"didFailToLocateUserWithError");
}

- (void)mapViewWillStartLoadingMap:(MKMapView *)mapView {
    NSLog(@"mapViewWillStartLoadingMap");
    if (self.mapView.showsUserLocation) {
        MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(userAnnotation.coordinate, ZOOM_OUT, ZOOM_OUT);
        [self.mapView setRegion:[self.mapView regionThatFits:region] animated:YES];
        // to stop tracking user location
        self.mapView.showsUserLocation = NO;
    }
}

#pragma mark - actions

- (void)openInMapsAction {
    //open maps app or google map
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel",nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Open in Maps",nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        NSString *locationAsGoogleMapsString = [NSString stringWithFormat:@"http://maps.apple.com/?ll=%f,%f&z=18&q=%%20",userAnnotation.coordinate.latitude,userAnnotation.coordinate.longitude ];
        NSURL *locationURL = [[NSURL alloc] initWithString:locationAsGoogleMapsString];
        [[UIApplication sharedApplication] openURL:locationURL options:@{} completionHandler:nil];
        
    }]];
    
    if ([[UIApplication sharedApplication] canOpenURL:
         [NSURL URLWithString:@"comgooglemaps://"]]) {
        [actionSheet addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Open in Google Maps",nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            NSURL *googleMapsURL = [NSURL URLWithString:[NSString stringWithFormat:@"https://www.google.com/maps/search/?api=1&query=%f,%f", userAnnotation.coordinate.latitude, userAnnotation.coordinate.longitude]];
            [[UIApplication sharedApplication] openURL:googleMapsURL options:@{} completionHandler:nil];
        }]];
    }
    
    [self presentViewController:actionSheet animated:YES completion:nil];
    
}

- (IBAction)searchLocationToolbarAction:(id)sender {
    MKCoordinateRegion region;
    if (!zoom) {
        region = MKCoordinateRegionMakeWithDistance(userAnnotation.coordinate, ZOOM_IN, ZOOM_IN);
    }
    else
    {
        region = MKCoordinateRegionMakeWithDistance(userAnnotation.coordinate, ZOOM_OUT, ZOOM_OUT);
    }
    
    [self.mapView setRegion:[self.mapView regionThatFits:region] animated:YES];
    
    zoom = !zoom;
}

- (IBAction)shareLocationInMapsAction:(id)sender {
    [self openInMapsAction];
}

- (IBAction)mapStylesegmentedControlAction:(UISegmentedControl*)sender {
    
    if (sender.selectedSegmentIndex == 0) {
        _mapView.mapType = MKMapTypeStandard;
        
    }
    else if (sender.selectedSegmentIndex == 1) {
        
        _mapView.mapType = MKMapTypeHybrid;
    }
    else if (sender.selectedSegmentIndex == 2) {
        
        _mapView.mapType = MKMapTypeSatellite;
    }
}

@end

