/*
 * Rainbow
 *
 * Copyright (c) 2019, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 *
 */

#import "RainbowMessageView.h"
#import "Message-JSQMessageData.h"
#import "MarkdownParser.h"
#import "Room+Extensions.h"
#import "UITools.h"

@implementation RainbowMessageView

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.textView.textContainer.maximumNumberOfLines = 2;
    self.textView.textContainer.lineBreakMode = NSLineBreakByTruncatingTail;
    self.textView.textContainerInset = UIEdgeInsetsMake(4, 0, 4, 0);
    self.textView.layer.cornerRadius = 5;
    //[self.backgroundView setHidden:YES];
}

-(void) setAnsweredMessage: (Message *) message {
    NSMutableAttributedString *string = [NSMutableAttributedString new];
    NSString *displayName = @"";
    
    if([message.via isKindOfClass:[Room class]]) {
        displayName = [NSString stringWithFormat:@"%@", message.peer.displayName];
    }
    
    if(message.bodyInMarkdown) {
        self.textView.attributedText = [MarkdownParser.sharedInstance attributedStringFromMarkDown:message.bodyInMarkdown];
    }
    
    else {
        NSString * bodyString = (message.body == nil && message.attachment != nil) ? message.attachment.fileName : message.body;
        [string.mutableString setString: [NSString stringWithFormat:(displayName.length > 0) ? @"%@\n%@" : @"%@%@", displayName, bodyString]];
        [string setAttributes:@{NSFontAttributeName: [UIFont fontWithName:[UITools defaultFontName] size:13]} range:NSMakeRange(0, string.length)];
        [string addAttribute:NSFontAttributeName value:[UIFont fontWithName:[UITools boldFontName] size:13] range:NSMakeRange(0, displayName.length)];
        
        self.textView.attributedText = string;
    }
}

@end
