/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "JSQWebRTCEventsCell.h"
#import "UITools.h"

@interface JSQWebRTCEventsCell ()
@property (weak, nonatomic) IBOutlet JSQMessagesLabel *eventTypeLabel;

@end

@implementation JSQWebRTCEventsCell
#pragma mark - Class methods

+ (UINib *)nib {
    return [UINib nibWithNibName:NSStringFromClass([JSQWebRTCEventsCell class]) bundle:[NSBundle bundleForClass:[JSQWebRTCEventsCell class]]];
}

+ (NSString *)cellReuseIdentifier {
    return NSStringFromClass([JSQWebRTCEventsCell class]);
}

#pragma mark - Instance methods

-(void) awakeFromNib {
    [super awakeFromNib];
    [UITools applyCustomBoldFontTo:_eventTypeLabel];
    self.textView.clipsToBounds = NO;
    self.textView.textContainer.maximumNumberOfLines = 0;
    self.textView.textContainer.lineBreakMode = NSLineBreakByClipping;
}

- (void)setMessage:(NSString *)message {
    self.textView.text = message;
    self.textView.textColor = [UIColor lightGrayColor];
}

- (void) setEventTypeText:(NSString *) eventTypeText {
    _eventTypeLabel.text = eventTypeText;
}

- (void) setEventTypeColor:(UIColor *) color {
    _eventTypeLabel.textColor = color;
}
@end
