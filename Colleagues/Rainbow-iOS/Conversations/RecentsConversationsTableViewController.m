/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "RecentsConversationsTableViewController.h"
#import <DZNEmptyDataSet/DZNEmptyDataSet.h>
#import "UITools.h"
#import <Rainbow/ServicesManager.h>
#import <Rainbow/Conversation.h>
#import "UIRainbowGenericTableViewCell.h"
#import "CustomNavigationController.h"
#import "UIMessagesViewController.h"
#import "UIMessagesSearchViewController.h"
#import "UIPagingMessagesViewController.h"
#import "BGTableViewRowActionWithImage.h"
#import <Rainbow/defines.h>
#import <Rainbow/ContactsManagerService.h>
#import <Rainbow/Message.h>
#import <Rainbow/Room.h>
#import <Rainbow/Tools.h>
#import <Rainbow/ConversationsManagerService.h>
#import <Rainbow/FavoritesManager.h>
#import "Contact+Extensions.h"
#import "CustomSearchController.h"
#import "UIViewController+TopViewController.h"
#import <JSQSystemSoundPlayer/JSQSystemSoundPlayer.h>
#import "UINotificationManager.h"
#import "UIContactDetailsViewController.h"
#import "UIRoomDetailsViewController.h"
#import "UIGroupDetailsViewController.h"
#import <Rainbow/UIDevice+VersionCheck.h>
#import "UINavigationController+PreviewActions.h"
#import "MyInfoNavigationItem.h"
#import "FilteredSortedSectionedArray.h"
#import "UIStoryboardManager.h"
#import "UIRoomsTableViewController.h"
#import "UIViewController+Visible.h"
#import "UIGuestModeViewController.h"
#import "UINetworkLostViewController.h"
#import "FilteredSortedSectionedArray+ConversationsList.h"
#import "Firebase.h"
#import "UIConferencesViewController.h"
#import "MyInfosTableViewController.h"

#import "FavoriteConversationViewCell.h"

#define kActiveCallsSection @"Active calls"
#define kMyConversationsSection @"My conversations"
#define kMyFavoriteConversationsSection @"Favorite Conversations"

@interface RecentsConversationsTableViewController () <DZNEmptyDataSetDelegate, DZNEmptyDataSetSource, UIViewControllerPreviewingDelegate, UISearchResultDisplayDetailControllerProtocol, UICollectionViewDelegate, UICollectionViewDataSource,
                UICollectionViewDragDelegate, UICollectionViewDropDelegate, UITableViewDropDelegate>
@property (nonatomic, strong) ServicesManager *serviceManager;
@property (nonatomic, strong) ContactsManagerService *contactManagerService;
@property (nonatomic, strong) ConversationsManagerService *conversationManagerService;

@property (nonatomic, strong) NSString *lastMessageIdReceiveByPush;
@property (nonatomic, strong) id previewingContext;
@property (nonatomic) BOOL populated;

// Data model
@property (nonatomic, strong) FilteredSortedSectionedArray<Conversation *> *conversations;
@property (nonatomic, strong) NSObject *conversationsMutex;
@property (nonatomic, strong) NSPredicate *globalFilterAll;
@property (nonatomic, strong) SectionNameComputationBlock sectionedByName;
@property (nonatomic, strong) NSSortDescriptor *sortSectionAsc;

@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstraint;
@property (nonatomic) BOOL lostNetworkViewAsBeenScrolled;
@property (nonatomic) BOOL guestBannerViewAsBeenScrolled;
@property (nonatomic) BOOL isNetworkConnected;
@property (nonatomic) BOOL isConversationCacheLoaded;
@property (nonatomic) BOOL isRoomCacheLoaded;
@property (nonatomic) BOOL isPresenceCacheLoaded;

@property (nonatomic, strong) NSMutableArray *userFavorites;
@property (nonatomic, weak) IBOutlet UIView *favoriteView;
@property (nonatomic, weak) IBOutlet UICollectionView *favoriteCollectionView;

@end

@implementation RecentsConversationsTableViewController
-(instancetype) initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if(self){
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didEndLoadingConversations:) name:kConversationsManagerDidEndLoadingConversations object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didEndLoadingConversationsFromCache:) name:kConversationsManagerDidEndLoadingConversationsFromCache object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didEndLoadingRoomsFromCache:) name:kConversationsManagerDidEndLoadingRoomsFromCache object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didEndLoadingPresencesFromCache:) name:kConversationsManagerDidEndLoadingPresencesFromCache object:nil];
        
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didAddConversation:) name:kConversationsManagerDidAddConversation object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didRemoveConversation:) name:kConversationsManagerDidRemoveConversation object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didRemoveAllConversations:) name:kConversationsManagerDidRemoveAllConversations object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didUpdateConversation:) name:kConversationsManagerDidUpdateConversation object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didStartConversation:) name:kConversationsManagerDidStartConversation object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didStopConversation:) name:kConversationsManagerDidStopConversation object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReceiveNewMessageForConversation:) name:kConversationsManagerDidReceiveNewMessageForConversation object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleLocalNotification:) name:kNotificationsManagerHandleNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didLogin:) name:kLoginManagerDidLoginSucceeded object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didLogout:) name:kLoginManagerDidLogoutSucceeded object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didLostConnection:) name:kLoginManagerDidLostConnection object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReconnect:) name:kLoginManagerDidReconnect object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeContactDisplaySettings:) name:kContactsManagerServiceDidChangeContactDisplayUserSettings object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationWillResignActive:) name:UIApplicationWillResignActiveNotification object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleLocalNotification:) name:UIApplicationDidReceiveLocalNotification object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didUpdateConference:) name:kConferencesManagerDidUpdateConference object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(dumpConversations) name:@"dumpConversations" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didRemoveRoom:) name:kRoomsServiceDidRemoveRoom object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(willShowNetworkLostView:) name:CustomNavigationControllerWillShowNetworkLostView object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didChangeNetworkLostViewFrame:) name:CustomNavigationControllerDidChangeNetworkLostViewFrame object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(willShowGuestModeView:) name:CustomNavigationControllerWillShowGuestModeView object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didHideGuestModeView:) name:CustomNavigationControllerDidHideGuestModeView object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didUpdateRoom:) name:kRoomsServiceDidUpdateRoom object:nil];

        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didUpdateFavoriteConversation:) name:kFavoritesDidCreate object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didUpdateFavoriteConversation:) name:kFavoritesDidUpdate object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didUpdateFavoriteConversation:) name:kFavoritesDidDelete object:nil];

        _sectionedByName = ^NSString*(Conversation *conversation) {
            if(conversation.hasActiveCall)
                return kActiveCallsSection;
            if([conversation.peer isKindOfClass:[Room class]]){

                Conference *conf = ((Room*)conversation.peer).conference;
                
                if ([((Room*)conversation.peer) isWebRTCConferenceRoom] && conf.myConferenceParticipant.state == ParticipantStateConnected && conf.canJoin)
                    return kActiveCallsSection;
                
               else if ([((Room*)conversation.peer) isPGIConferenceRoom] && conf.myConferenceParticipant.state == ParticipantStateConnected && conf.isActive)
                    return kActiveCallsSection;
            }
            return kMyConversationsSection;
        };
        
        _globalFilterAll = [NSPredicate predicateWithBlock:^BOOL(Conversation *conversation, NSDictionary<NSString *,id> * bindings) {
            if([conversation.peer isKindOfClass:[Room class]]){
                if([((Room *)conversation.peer).topic isEqualToString:@"Rainbow_OutlookCreation_InternalUseOnly"])
                    return NO;
            }
            return YES;
        }];
        
        _sortSectionAsc = [NSSortDescriptor sortDescriptorWithKey:@"self" ascending:YES comparator:^NSComparisonResult(NSString* obj1, NSString* obj2) {
            if([obj1 isEqualToString:kActiveCallsSection])
                return NSOrderedAscending;
            if([obj2 isEqualToString:kMyConversationsSection])
                return NSOrderedDescending;
            return [obj1 compare:obj2 options:NSCaseInsensitiveSearch];

        }];
        
        _conversations = [FilteredSortedSectionedArray new];
        _conversationsMutex = [NSObject new];
        
        
        // Default filters/sorter/.. values
        _conversations.sectionNameFromObjectComputationBlock = _sectionedByName;
        _conversations.globalFilteringPredicate = _globalFilterAll;
        _conversations.sectionSortDescriptor = _sortSectionAsc;
        
        // __default__ has a special meaning : any other sections not found in the dictionary
        _conversations.objectSortDescriptorForSection = @{@"__default__": @[[UITools sortDescriptorByLastMessageDate]]};
        
        _serviceManager = [ServicesManager sharedInstance];
        _contactManagerService = _serviceManager.contactsManagerService;
        _conversationManagerService = _serviceManager.conversationsManagerService;
        
        _populated = NO;
        
        _isConversationCacheLoaded = _conversationManagerService.isConversationCacheLoaded;
        _isRoomCacheLoaded = [_serviceManager roomsService].minimalCacheLoaded;
        _isPresenceCacheLoaded = _contactManagerService.minimalPresencesCacheLoaded;
        
        // Favorites
        _userFavorites = [NSMutableArray new];
        
        OTCLog(@"isConversationCacheLoaded %@ ; isRoomCacheLoaded %@ ; isPresenceCacheLoaded %@",
               _isConversationCacheLoaded ? @"YES" : @"NO",
               _isRoomCacheLoaded ? @"YES" : @"NO",
               _isPresenceCacheLoaded ? @"YES" : @"NO");
    }
    return self;
}

-(void) dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kConversationsManagerDidEndLoadingConversations object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kConversationsManagerDidEndLoadingConversationsFromCache object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kConversationsManagerDidEndLoadingRoomsFromCache object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kConversationsManagerDidEndLoadingPresencesFromCache object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kConversationsManagerDidAddConversation object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kConversationsManagerDidRemoveConversation object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kConversationsManagerDidRemoveAllConversations object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kConversationsManagerDidUpdateConversation object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kConversationsManagerDidStartConversation object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kConversationsManagerDidStopConversation object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kConversationsManagerDidReceiveNewMessageForConversation object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidLogoutSucceeded object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidLostConnection object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidReconnect object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kContactsManagerServiceDidChangeContactDisplayUserSettings object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificationsManagerHandleNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillResignActiveNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidReceiveLocalNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kConferencesManagerDidUpdateConference object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"dumpConversations" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:CustomNavigationControllerDidHideNetworkLostView object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:CustomNavigationControllerWillShowNetworkLostView object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:CustomNavigationControllerDidChangeNetworkLostViewFrame object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:CustomNavigationControllerDidHideGuestModeView object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:CustomNavigationControllerWillShowGuestModeView object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kRoomsServiceDidUpdateRoom object:nil];
    
    [self didLogout:nil];
    
    @synchronized (_conversationsMutex) {
        [_conversations removeAllObjects];
        
        if(_userFavorites)
            [_userFavorites removeAllObjects];
    }
    
    _conversationsMutex = nil;
    
    _contactManagerService = nil;
    _conversationManagerService = nil;
    _serviceManager = nil;
}

#pragma mark - Logout notification

-(void) didLogin:(NSNotification *) notification {
    if(![[NSThread currentThread] isEqual:[NSThread mainThread]]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didLogin:notification];
        });
        return;
    }
    _serviceManager = [ServicesManager sharedInstance];
    _contactManagerService = _serviceManager.contactsManagerService;
    _conversationManagerService = _serviceManager.conversationsManagerService;
    _populated = NO; // Display only the empty message if the fetch of all conversations returns nothing (keep white screen even if the cache is empty)

    
    if ([self isViewLoaded]) {
        [self.tableView reloadData];
        [self reloadCollectionViewAndChangeViewVisibility];
    }
}

-(void) didLogout:(NSNotification *) notification {
    if(![[NSThread currentThread] isEqual:[NSThread mainThread]]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didLogout:notification];
        });
        return;
    }
    
    _populated = NO;
    _isConversationCacheLoaded = NO;
    _isRoomCacheLoaded = NO;
    _isPresenceCacheLoaded = NO;
    
    @synchronized (_conversationsMutex) {
        [_conversations removeAllObjects];
        if(_userFavorites)
            [_userFavorites removeAllObjects];
    }
    
    if ([self isViewLoaded]) {
        [self.tableView reloadData];
        [self reloadCollectionViewAndChangeViewVisibility];
    }
    
    self.tabBarController.selectedIndex = 0;
    [self.navigationController popToRootViewControllerAnimated:NO];
}

-(void) didLostConnection:(NSNotification *) notification {
    if(![[NSThread currentThread] isEqual:[NSThread mainThread]]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didLostConnection:notification];
        });
        return;
    }
    
    if([self isViewLoaded]){
    }
}

-(void) didReconnect:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didReconnect:notification];
        });
        return;
    }
    
    if([self isViewLoaded]){
    }
}

-(void) didChangeNetworkLostViewFrame:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didHideNetworkLostView:notification];
        });
        return;
    }
}

-(void) willShowNetworkLostView:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didHideNetworkLostView:notification];
        });
        return;
    }
    
    if([self isViewLoaded] && !_lostNetworkViewAsBeenScrolled){
        self.topConstraint.constant += kNetworkLostHeight;
        _lostNetworkViewAsBeenScrolled = YES;
        [UIView animateWithDuration:0.25f animations:^{
            [self.view setNeedsLayout];
        } completion:^(BOOL finished) {
            _isNetworkConnected = NO;
        }];
    }
}

-(void) didHideNetworkLostView:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didHideNetworkLostView:notification];
        });
        return;
    }
    
    if([self isViewLoaded] && _lostNetworkViewAsBeenScrolled){
        self.topConstraint.constant -= kNetworkLostHeight;
        _lostNetworkViewAsBeenScrolled = NO;
        [UIView animateWithDuration:0.75f animations:^{
            [self.view setNeedsLayout];
        } completion:^(BOOL finished) {
            _isNetworkConnected = YES;
        }];
    }
}

-(void) didHideGuestModeView:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didHideGuestModeView:notification];
        });
        return;
    }
    
    if([self isViewLoaded] && _guestBannerViewAsBeenScrolled){
        self.topConstraint.constant -= kGuestModeHeight;
        _guestBannerViewAsBeenScrolled = NO;
        [UIView animateWithDuration:0.75f animations:^{
            [self.view setNeedsLayout];
        } completion:nil];
    }
}

-(void) willShowGuestModeView:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self willShowGuestModeView:notification];
        });
        return;
    }
    
    if([self isViewLoaded] && !_guestBannerViewAsBeenScrolled){
        self.topConstraint.constant += kGuestModeHeight;
        _guestBannerViewAsBeenScrolled = YES;
        [UIView animateWithDuration:0.25f animations:^{
            [self.view setNeedsLayout];
        } completion:^(BOOL finished) {
            _isNetworkConnected = NO;
        }];
    }
}

-(void) didRemoveAllMessagesFromConversation:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didRemoveConversation:notification];
        });
        return;
    }
    
    Conversation *theConversation = (Conversation *) notification.object;
    @synchronized (_conversationsMutex) {
        if([_conversations containsObject:theConversation]){
            [_conversationManagerService markAsReadByMeAllMessageForConversation:theConversation];
            [_conversationManagerService stopConversation:theConversation];
        }
    }
    
    if ([self isViewLoaded])
        [self.tableView reloadData];
}

-(void) changeContactDisplaySettings:(NSNotification *) notification {
    if(![[NSThread currentThread] isEqual:[NSThread mainThread]]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self changeContactDisplaySettings:notification];
        });
        return;
    }
    if ([self isViewLoaded]) {
        NSLog(@"Reload UI on contact display settings changed %@", NSStringFromBOOL(_contactManagerService.displayFirstNameFirst));
        [self.tableView reloadData];
    }
}

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

}

-(void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
}

-(void) viewDidLoad {
    [super viewDidLoad];
    ((MyInfoNavigationItem*)self.navigationItem).parentViewController = self;
    self.tableView.rowHeight = kRainbowGenericTableViewCellHeight;
    self.title = NSLocalizedString(@"Conversations", nil);
    self.tableView.emptyDataSetSource = self;
    self.tableView.emptyDataSetDelegate = self;
    
    self.tableView.tableFooterView = [UIView new];
    [self.tableView setBackgroundColor:[UIColor whiteColor]];
    self.tableView.sectionHeaderHeight = 30;
    self.definesPresentationContext = YES;
    self.extendedLayoutIncludesOpaqueBars = YES;
    if ([UITools isForceTouchAvailableForTraitCollection:self.traitCollection]) {
        self.previewingContext = [self registerForPreviewingWithDelegate:self sourceView:self.view];
    }
    if([[ServicesManager sharedInstance].myUser.profilesName count] > 0) {
        NSString *profileNames = [[ServicesManager sharedInstance].myUser.profilesName componentsJoinedByString:@","];
        [FIRAnalytics setUserPropertyString:profileNames forName:@"rainbow_service_plan"];
    }

    [self.tableView registerNib:[UINib nibWithNibName:@"UIRainbowGenericTableViewCell" bundle:nil] forCellReuseIdentifier:kTableViewReusableKey];
    
    self.tableView.contentInset = UIEdgeInsetsMake(0, 0, self.tabBarController.tabBar.frame.size.height, 0);
    
    if (@available(iOS 11.0, *)){
        self.favoriteCollectionView.dragInteractionEnabled = YES;
        self.favoriteCollectionView.reorderingCadence = UICollectionViewReorderingCadenceFast;
        self.favoriteCollectionView.dragDelegate = self;
        self.favoriteCollectionView.dropDelegate = self;
        self.tableView.dragInteractionEnabled = YES;
        self.tableView.dropDelegate = self;
    }
    else
    {
        // this is for ios 10 or older
        self.automaticallyAdjustsScrollViewInsets  = NO;
    }
    
}

-(void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [FIRAnalytics setScreenName:@"conversations_screen" screenClass:@"RecentsConversationsTableViewController"];
}


-(UIStatusBarStyle) preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

-(BOOL) shouldAutorotate {
    return NO;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return UIInterfaceOrientationPortrait;
}

#pragma mark - Application notification

-(void) applicationWillResignActive:(NSNotification *) notification {
    if([UIDevice currentDevice].systemMajorVersion > 9.0){
        if(self.traitCollection.forceTouchCapability == UIForceTouchCapabilityAvailable){
            NSLog(@"3D touch actions are available");
            // Register the 3 latest conversations as 3D Touch quick action
            NSArray<Conversation *> *subset = nil;
            @synchronized (_conversationsMutex) {
                NSInteger size = 3;
                // TODO : Change me to use always the my conversation section
                NSArray<Conversation *> *myConversationSection = [_conversations objectsInSection:kMyConversationsSection];
                if([myConversationSection count] < 3)
                    size = [myConversationSection count];
                
                subset = [myConversationSection subarrayWithRange:NSMakeRange(0, size)];
            }
            NSMutableArray *shortcuts = [NSMutableArray array];
            [subset enumerateObjectsUsingBlock:^(Conversation * aConversation, NSUInteger idx, BOOL * stop) {
                NSString *name = aConversation.peer.displayName;
                if (aConversation.peer.jid) {
                    UIApplicationShortcutIcon *icon = [UIApplicationShortcutIcon iconWithTemplateImageName:[aConversation.peer isKindOfClass:[Contact class]]?@"chat3DTouch":@"Bubble3DTouch"];
                    [shortcuts addObject:[[UIApplicationShortcutItem alloc] initWithType:name localizedTitle:name localizedSubtitle:nil icon:icon userInfo:@{@"conversationJid":aConversation.peer.jid}]];
                }
            }];
        
            [UIApplication sharedApplication].shortcutItems = shortcuts;
        } else {
            NSLog(@"3D touch actions are NOT available");
        }
    }
}

#pragma mark - Conversations Manager notifications
-(void) didEndLoadingConversations:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didEndLoadingConversations:notification];
        });
        return;
    }
    
    if (!_isConversationCacheLoaded)
        _isConversationCacheLoaded = _conversationManagerService.isConversationCacheLoaded;
    
    if (!_isRoomCacheLoaded)
        _isRoomCacheLoaded = [_serviceManager.roomsService minimalCacheLoaded];
    
    if (!_isPresenceCacheLoaded)
        _isPresenceCacheLoaded = _contactManagerService.minimalPresencesCacheLoaded;
    
    if (!_isConversationCacheLoaded || !_isRoomCacheLoaded || !_isPresenceCacheLoaded) {
        OTCLog(@"The conversations cannot be populated (_isConversationCacheLoaded %@ ; _isRoomCacheLoaded %@ ; _isPresenceCacheLoaded %@)", _isConversationCacheLoaded ? @"YES" : @"NO", _isRoomCacheLoaded ? @"YES" : @"NO", _isPresenceCacheLoaded ? @"YES" : @"NO");
        return;
    }
    _populated = YES;
    
    @synchronized(_conversationsMutex){
        [_conversations removeAllObjects];

        _userFavorites = (NSMutableArray *)[[ServicesManager sharedInstance].favoritesManager favorites];

        [_conversationManagerService.conversations enumerateObjectsUsingBlock:^(Conversation * conversation, NSUInteger idx, BOOL * stop) {
            if(![_conversations containsConversation:conversation]){
                [_conversations addObject:conversation];
            }
        }];
        
        [_conversations reloadData];
    }
    
    OTCLog(@"The conversations should now be populated with %@ conversations.", @(_conversations.count));
    
    if ([self isViewLoaded]) {
        [self.tableView reloadData];
        [self reloadCollectionViewAndChangeViewVisibility];
    }
}

-(void) didAddConversation:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didAddConversation:notification];
        });
        return;
    }

    if (!_isConversationCacheLoaded || !_isRoomCacheLoaded || !_isPresenceCacheLoaded)
        return;
    
    Conversation *theConversation = (Conversation *) notification.object;
    @synchronized (_conversationsMutex) {
        if(![_conversations containsConversation:theConversation]){
            [_conversations addObject:theConversation];
            [_conversations reloadData];
        }
        
        if([_userFavorites containsObject:theConversation.peer])
            [self syncFavoriteWithConversation:theConversation];
    }
    
    if ([self isViewLoaded])
        [self.tableView reloadData];
}

-(void) didRemoveConversation:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didRemoveConversation:notification];
        });
        return;
    }
    
    Conversation *theConversation = (Conversation *) notification.object;
    @synchronized (_conversationsMutex) {
        if([_conversations containsConversation:theConversation]){
            [_conversations removeObject:theConversation];
            [_conversations reloadData];
        }
    }
    
    if ([self isViewLoaded])
        [self.tableView reloadData];
}

-(void) didEndLoadingConversationsFromCache:(NSNotification *) notification {
    _isConversationCacheLoaded = YES;
    NSLog(@" XXXXX didEndLoadingConversationsFromCache");
    [self didEndLoadingConversations:notification];
}

-(void) didEndLoadingRoomsFromCache:(NSNotification *) notification {
    _isRoomCacheLoaded = YES;
    NSLog(@" XXXXX didEndLoadingRoomsFromCache");
    [self didEndLoadingConversations:notification];
}

-(void) didEndLoadingPresencesFromCache: (NSNotification *) notification {
    _isPresenceCacheLoaded = YES;
    NSLog(@" XXXXX didEndLoadingPresencesFromCache");
    [self didEndLoadingConversations:notification];
}

-(void) didRemoveAllConversations:(NSNotification *) notification {
    if(![[NSThread currentThread] isEqual:[NSThread mainThread]]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didRemoveAllConversations:notification];
        });
        return;
    }
    
    _populated = NO;
    
    @synchronized (_conversationsMutex) {
        [_conversations removeAllObjects];
    }
    
    if ([self isViewLoaded])
        [self.tableView reloadData];
}

-(void) didUpdateConversation:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didUpdateConversation:notification];
        });
        return;
    }
    
    if (!_isConversationCacheLoaded || !_isRoomCacheLoaded || !_isPresenceCacheLoaded)
        return;

    Conversation *theConversation = (Conversation *) notification.object;
    
    if(theConversation) { // DO NOT add a nil conversation to avoid crash
        @synchronized (_conversationsMutex) {
            if(![_conversations containsConversation:theConversation]){
                [_conversations addObject:theConversation];
            }
            [_conversations reloadData];
            
            if([_userFavorites containsObject:theConversation.peer])
                [self syncFavoriteWithConversation:theConversation];
        }
    }
    if ([self isViewLoaded])
        [self.tableView reloadData];
}

-(void) didStartConversation:(NSNotification *) notification {
    [self didStartConversation:notification openFromNotification:NO];
}

-(void) didStartConversation:(NSNotification *)notification openFromNotification:(BOOL)openFromNotification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didStartConversation:notification openFromNotification:openFromNotification];
        });
        return;
    }
    
    // We don't want to open the conversation view if the application is in background, case of message send from a push notification.
    if([UIApplication sharedApplication].applicationState == UIApplicationStateBackground)
        return;
    
    if(!self.isVisible && !openFromNotification)
        return;
    
    if(![self.tabBarController.selectedViewController isEqual:self])
        self.tabBarController.selectedIndex = 0;
    
    Conversation *theConversation = (Conversation *)notification.object;
    
    /////////// Disable Google Analytics ///////////
//    if([(theConversation.peer) isKindOfClass:[Contact class]]) {
//        [FIRAnalytics setUserPropertyString:((Contact *)theConversation.peer).companyName forName:@"company"];
//    }
    
    [self startConversation:theConversation];
}

-(void) startConversation:(Conversation *) theConversation {
    __weak __typeof__ (self) weakSelf = self;
    [UIView animateWithDuration:0.2 animations:^{
        NSInteger pos;
        NSInteger section;
        @synchronized (_conversationsMutex) {
            pos = [_conversations indexOfObjectInHisSection:theConversation];
            section = [[_conversations sections] count] > 1 ? 1:0;
        }
        if(pos != NSNotFound)
            [weakSelf.tableView moveRowAtIndexPath:[NSIndexPath indexPathForRow:pos inSection:section] toIndexPath:[NSIndexPath indexPathForRow:0 inSection:section]];
    } completion:^(BOOL finished){
        if(theConversation.stringForConversationType)
            [FIRAnalytics logEventWithName:@"start_conversation" parameters: @{@"conversation_type":theConversation.stringForConversationType, @"category":@"conversations_screen"}];
        [[weakSelf class] openConversationViewForConversation:theConversation inViewController:weakSelf.navigationController];
    }];
}

-(void) didStopConversation:(NSNotification *) notification {
    dispatch_async(dispatch_get_main_queue(), ^{
        if ([self isViewLoaded])
            [self.tableView reloadData];
    });
  
}

#pragma mark - Conference notifications
-(void) didUpdateConference:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didUpdateConference:notification];
        });
        return;
    }
    
    NSDictionary *userInfo = (NSDictionary *)notification.object;
    NSArray<NSString *> *changedKeys = [userInfo objectForKey:kConferenceChangedAttributesKey];
    NSLog(@"DID UPDATE CONFERENCE IN CONVERSATION TAB %@", userInfo);
    if([changedKeys containsObject:@"isActive"] ||
       [changedKeys containsObject:@"canJoin"] ||
       [changedKeys containsObject:@"scheduledStartDate"] ||
       [changedKeys containsObject:@"scheduledEndDate"]){
         @synchronized (_conversationsMutex) {
             [_conversations reloadData];
         }
        
        if ([self isViewLoaded])
            [self.tableView reloadData];
    }
}
#pragma mark - Room notification
-(void) didRemoveRoom:(NSNotification *) notification {
    dispatch_async(dispatch_get_main_queue(), ^{
        if ([self isViewLoaded])
            [self.tableView reloadData];
    });
    
}

-(void) didUpdateRoom:(NSNotification *) notification {
    dispatch_async(dispatch_get_main_queue(), ^{
        if ([self isViewLoaded])
            [self.tableView reloadData];
    });
}

#pragma mark - Table view data source

-(void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section {
    view.tintColor = [UITools colorFromHexa:0xF8F8F8FF];
    UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *)view;
    [header.textLabel setFont:[UIFont fontWithName:[UITools defaultFontName] size:20]];
    [header.textLabel setTextColor:[UITools colorFromHexa:0xB8B8B8FF]];
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    NSString *sectionName;
    @synchronized (_conversationsMutex) {
        sectionName = [[_conversations sections] objectAtIndex:section];
        if([_conversations objectsInSection:sectionName].count > 0){
            if([[_conversations sections] count] == 2 || [sectionName isEqualToString:kActiveCallsSection])
                return NSLocalizedString(sectionName, nil);
        }
        return @"";
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    @synchronized (_conversationsMutex) {
        return [[_conversations sections] count];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSString *key;
    @synchronized (_conversationsMutex) {
        key = [[_conversations sections] objectAtIndex:section];
        return [[_conversations objectsInSection:key] count];
    }
} 

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UIRainbowGenericTableViewCell *cell = (UIRainbowGenericTableViewCell*)[tableView dequeueReusableCellWithIdentifier:kTableViewReusableKey forIndexPath:indexPath];
    
    NSString *key;
    Conversation *conversation;
    @synchronized (_conversationsMutex) {
        key = [[_conversations sections] objectAtIndex:indexPath.section];
        conversation = [[_conversations objectsInSection:key] objectAtIndex:indexPath.row];
    }
    cell.cellObject = (NSObject<UIRainbowGenericTableViewCellProtocol>*)conversation;
    cell.showMidLabel = YES;
    return cell;
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSString *key;
    Conversation *theConversation;
    @synchronized (_conversationsMutex) {
        if (indexPath && indexPath.section < [[_conversations sections] count]) {
            key = [[_conversations sections] objectAtIndex:indexPath.section];
            if (indexPath.row < [[_conversations objectsInSection:key] count])
                theConversation = [[_conversations objectsInSection:key] objectAtIndex:indexPath.row];
            else
                return;
        } else {
            return;
        }
    }
    [_conversationManagerService sendMarkAllMessagesAsReadFromConversation:theConversation];
    
    UIPagingMessagesViewController *pagingMsgView = [[UIStoryboardManager sharedInstance].messagesStoryBoard instantiateViewControllerWithIdentifier:@"pagingMessageViewControllerID"];
    pagingMsgView.conversation = theConversation;
    UIMessagesViewController *msgView = pagingMsgView.messagesViewController;
    [[self class] prepareMessageView:msgView withConversation:theConversation];
    [pagingMsgView setViewControllers];
    [self.navigationController pushViewController:pagingMsgView animated:YES];
}

-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *key;
    Conversation *theConversation;
    @synchronized (_conversationsMutex) {
        if (indexPath && indexPath.section < [[_conversations sections] count]) {
            key = [[_conversations sections] objectAtIndex:indexPath.section];
            if (indexPath.row < [[_conversations objectsInSection:key] count])
                theConversation = [[_conversations objectsInSection:key] objectAtIndex:indexPath.row];
            else
                return NO;
        } else {
            return NO;
        }
    }
    if(theConversation.hasActiveCall)
        return NO;
    return YES;
}

#ifdef __IPHONE_11_0
- (nullable UISwipeActionsConfiguration *)tableView:(UITableView *)tableView leadingSwipeActionsConfigurationForRowAtIndexPath:(NSIndexPath *)indexPath API_AVAILABLE(ios(11.0)) API_UNAVAILABLE(tvos) {
    NSString *key;
    Conversation *theConversation;
    @synchronized (_conversationsMutex) {
        key = [[_conversations sections] objectAtIndex:indexPath.section];
        theConversation = (Conversation *)[[_conversations objectsInSection:key] objectAtIndex:indexPath.row];
    }
    
    if(![[ServicesManager sharedInstance].favoritesManager isValidForFavorite:theConversation.peer]) {
        return nil;
    }
    
    Favorite *theConversationFavorite = [[ServicesManager sharedInstance].favoritesManager getFavoriteByPeerId:theConversation.peer.rainbowID];
    
    UIContextualAction *favoriteRowAction = [UIContextualAction contextualActionWithStyle:UIContextualActionStyleNormal title:NSLocalizedString(theConversationFavorite ? @"Remove from favorites" :  @"Add to favorites", nil) handler:^(UIContextualAction * _Nonnull action, __kindof UIView * _Nonnull sourceView, void (^ _Nonnull completionHandler)(BOOL))
    {
        // Delete favorite
        if(theConversationFavorite) {
            [[ServicesManager sharedInstance].favoritesManager deleteFavorite:theConversation.peer completionHandler:nil];
        } else {
            if ([[[ServicesManager sharedInstance].favoritesManager favorites] count] < [[ServicesManager sharedInstance].myUser maxFavoriteConversationsCount]) {
                [[ServicesManager sharedInstance].favoritesManager createFavorite:theConversation.peer completionHandler:nil];
            } else {
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:[NSString stringWithFormat: NSLocalizedString(@"You already have %ld favorites, a new favorite cannot be added", nil), [[ServicesManager sharedInstance].myUser maxFavoriteConversationsCount]] preferredStyle:UIAlertControllerStyleAlert];
                [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleCancel handler:nil]];
                [self presentViewController:alert animated:YES completion:nil];
            }
        }
        
        completionHandler (YES);
    }];
    
    favoriteRowAction.backgroundColor = [UIColor lightGrayColor];
    
    UISwipeActionsConfiguration *actions = [UISwipeActionsConfiguration configurationWithActions:@[favoriteRowAction]];
    
    return actions;
}
#endif

- (NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *key;
    Conversation *theConversation;
    @synchronized (_conversationsMutex) {
        key = [[_conversations sections] objectAtIndex:indexPath.section];
        theConversation = (Conversation *)[[_conversations objectsInSection:key] objectAtIndex:indexPath.row];
    }
    
    BGTableViewRowActionWithImage *close = [BGTableViewRowActionWithImage rowActionWithStyle:UITableViewRowActionStyleDestructive title:NSLocalizedString(@"Close", nil) backgroundColor:[UITools defaultTintColor] image:[UIImage imageNamed:@"close_conversation"] forCellHeight:kRainbowGenericTableViewCellHeight+10 handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
        [_conversationManagerService markAsReadByMeAllMessageForConversation:theConversation];
        [_conversationManagerService stopConversation:theConversation];
        [FIRAnalytics logEventWithName:@"close_conversation"
                            parameters:@{@"conversation_type":theConversation.stringForConversationType,@"category":@"conversations_screen"}];
        dispatch_async(dispatch_get_main_queue(), ^{
            self.tableView.editing = NO;
        });
    }];
    close.accessibilityLabel = NSLocalizedString(@"Close", nil);
    
    BGTableViewRowActionWithImage *leaveBubble = [BGTableViewRowActionWithImage rowActionWithStyle:UITableViewRowActionStyleDestructive title:NSLocalizedString(@"Quit", nil) backgroundColor:[UITools redColor] image:[UIImage imageNamed:@"LeaveBubble"] forCellHeight:kRainbowGenericTableViewCellHeight+10 handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
        [UIRoomsTableViewController leaveAction:(Room *)theConversation.peer inController:self completionHandler:^{
            [FIRAnalytics logEventWithName:@"leave_bubble"
                                parameters:@{@"conversation_type":theConversation.stringForConversationType,@"category":@"conversations_screen" }];
            dispatch_async(dispatch_get_main_queue(), ^{
                self.tableView.editing = NO;
            });
        }];
    }];
    leaveBubble.accessibilityLabel = NSLocalizedString(@"Quit", nil);
    
    BGTableViewRowActionWithImage *archiveBubble = [BGTableViewRowActionWithImage rowActionWithStyle:UITableViewRowActionStyleDestructive title:NSLocalizedString(@"Archive", nil) backgroundColor:[UITools redColor] image:[UIImage imageNamed:@"ArchiveBubble"] forCellHeight:kRainbowGenericTableViewCellHeight+10 handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
        [UIRoomsTableViewController archiveAction:(Room *)theConversation.peer inController:self completionHandler:^{
            [FIRAnalytics logEventWithName:@"archive_bubble"
                                parameters:@{@"conversation_type":theConversation.stringForConversationType,@"category":@"conversations_screen"}];
            dispatch_async(dispatch_get_main_queue(), ^{
                self.tableView.editing = NO;
            });
        }];
    }];
    archiveBubble.accessibilityLabel = NSLocalizedString(@"Quit", nil);
    
    BGTableViewRowActionWithImage *mute = [BGTableViewRowActionWithImage rowActionWithStyle:UITableViewRowActionStyleDestructive title:NSLocalizedString(@"Silence", nil) backgroundColor:[UIColor lightGrayColor] image:[UIImage imageNamed:@"MuteConversation"] forCellHeight:kRainbowGenericTableViewCellHeight+10 handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
        [_conversationManagerService muteConversation:theConversation];
        [FIRAnalytics logEventWithName:@"enable_notification" parameters:@{@"category":@"conversations_screen"}];

        dispatch_async(dispatch_get_main_queue(), ^{
            self.tableView.editing = NO;
        });
    }];
    mute.accessibilityLabel = NSLocalizedString(@"Silence", nil);
    
    BGTableViewRowActionWithImage *unmute = [BGTableViewRowActionWithImage rowActionWithStyle:UITableViewRowActionStyleDestructive title:NSLocalizedString(@"Alert", nil) backgroundColor:[UIColor lightGrayColor] image:[UIImage imageNamed:@"UnmuteConversation"] forCellHeight:kRainbowGenericTableViewCellHeight+10 handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
        [_conversationManagerService unmuteConversation:theConversation];
        [FIRAnalytics logEventWithName:@"disable_notification" parameters:@{@"category":@"conversations_screen"}];

        dispatch_async(dispatch_get_main_queue(), ^{
            
        });
        self.tableView.editing = NO;
    }];
    unmute.accessibilityLabel = NSLocalizedString(@"Alert", nil);
    
    NSMutableArray *actions = [NSMutableArray array];
    if (theConversation.type == ConversationTypeRoom && ((Room*)theConversation.peer).myStatusInRoom == ParticipantStatusAccepted){
        Room *aRoom = (Room*)theConversation.peer;
        if(!aRoom.conference || (aRoom.conference && aRoom.conference.endpoint.mediaType == ConferenceEndPointMediaTypeWebRTC)){
            if(aRoom.isMyRoom)
                [actions addObject:archiveBubble];
            else
                [actions addObject:leaveBubble];
        }
    }
    
    [actions addObject:close];
    
    if(theConversation.isMuted)
        [actions addObject:unmute];
    else
        [actions addObject:mute];
    
    return actions;
}

-(void) didReceiveNewMessageForConversation:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didReceiveNewMessageForConversation:notification];
        });
        return;
    }
    Conversation *theConversation = (Conversation *)notification.object;
    Message *theMessage = theConversation.lastMessage;
    
    // Ignore if it's a composing message or it has a zero length or it's a message from me.
    if(theMessage.isComposing || theMessage.body.length == 0 || theMessage.isOutgoing)
        return;
    
    if ([[UIApplication sharedApplication] applicationState] == UIApplicationStateBackground && [theMessage.groupChatEventPeer.jid isEqualToString:[ServicesManager sharedInstance].myUser.contact.jid]) {
        OTCLog(@"The application is in background and we receive a group chat event but from me. Do not display notification.");
        return;
    }
    
    if([_lastMessageIdReceiveByPush isEqualToString:theMessage.messageID] || theMessage.hasBeenPresentedInPush || theMessage.isResentMessage){
        NSLog(@"We are treating the message that we already received by push, so don't play sound");
        return;
    }
    
    // Sync Favorite Peer with this conversation to listen to unread message count
    if([_userFavorites containsObject:theConversation.peer]) {
        [self syncFavoriteWithConversation:theConversation];
    }
    
    BOOL notificationsAreEnabled = [UINotificationManager canShowNotifications];
    
    if(!notificationsAreEnabled){
        NSLog(@"Sound notification are disabled or no notification enabled, don't play sound");
        return;
    }
    
    BOOL canPlaySound = [UINotificationManager canPlaySoundForNotification];
    
    if ([[UIApplication sharedApplication] applicationState] == UIApplicationStateActive) {
        if(!theConversation.isMuted && theMessage.groupChatEventType == MessageGroupChatEventNone && canPlaySound && !theMessage.hasBeenPresentedInPush && theMessage.type != MessageTypeWebRTC){
            NSLog(@"Application is in foreground so play a sound");
            [JSQSystemSoundPlayer jsq_playMessageReceivedAlert];
        }
    } else if ([[UIApplication sharedApplication] applicationState] == UIApplicationStateBackground && !theMessage.hasBeenPresentedInPush && theMessage.type != MessageTypeWebRTC && !theConversation.isMuted && !theMessage.isResentMessage) {
        if(!theMessage.via.jid){
            NSLog(@"No jid for this message %@", theMessage);
            return;
        }
            
        // We are not active, so use a local notification instead
        NSString *body = theMessage.body;
        NSString *displayName = theMessage.via.displayName;
        NSLog(@"Will display local notification");
        NSDictionary *userInfo = nil;
        switch(theConversation.type) {
            case ConversationTypeUser: {
                userInfo = @{@"type" : @"user", @"from" : theMessage.via.jid};
                break;
            }
            case ConversationTypeRoom: {
                userInfo = @{@"type" : @"room", @"from" : theMessage.via.jid};
                body = [NSString stringWithFormat:@"%@ : %@", theMessage.peer.displayName, theMessage.body];
                break;
            }
            default:
                break;
        }
        [UINotificationManager presentUILocalNotificationWithBody:body title:displayName category:@"im_category" userInfo:userInfo playSound:!theConversation.isMuted];
    }
}

#pragma mark - Local notification
-(void) handleLocalNotification:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        __weak __typeof__(self) weakSelf = self;
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakSelf handleLocalNotification:notification];
        });
        return;
    }
    
    NSDictionary *userInfo = (NSDictionary*)notification.object;
    NSString *category = userInfo[@"category"];
    NSString *from = userInfo[@"from"];
    if([category isEqualToString:@"im_category"] || [category isEqualToString:@"event"]) {
        if(((NSString *)userInfo[@"roomJid"]).length > 0)
            from = userInfo[@"roomJid"];
        if(((NSString *)userInfo[@"room-jid"]).length > 0)
            from = userInfo[@"room-jid"];
        _lastMessageIdReceiveByPush = userInfo[@"messageID"];
        Conversation *theConversation = [_conversationManagerService getConversationWithPeerJID:from];
        __block BOOL needToOpenMeetingScreen = NO;
        
         dispatch_group_t createGroup = dispatch_group_create();
        if ([userInfo[@"last-event-type"] isEqualToString:@"room_invitation"]) {
            // This event can be for bubble or meeting
            // Get or fetch the room with conference information and then, go to bubbles or meetings tab
           
            dispatch_group_enter(createGroup);
            [[_serviceManager roomsService] fetchRoomWithJid:from withCompletionHandler:^(Room *room, NSError *error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                   
                    if (room && room.conference && room.conference.confId && room.conference.mediaType == ConferenceMediaTypePSTN)
                        needToOpenMeetingScreen = YES;
                    else if (!error)
                        self.tabBarController.selectedIndex = 2;
                     dispatch_group_leave(createGroup);
                });
            }];
        }
        else if ([userInfo[@"last-event-type"] isEqualToString:@"room_reminder"] && theConversation == nil) {
            // go to meetings tab
            needToOpenMeetingScreen = YES;
            dispatch_group_leave(createGroup);
        }
        else{
            // open conversation
            // check if we are in UIMessageViewController with right Conversation, return
            if (theConversation) {
                UIViewController *topViewController = [UITools topViewController];
                if ([topViewController isKindOfClass:[UIPagingMessagesViewController class]]) {
                    // check if we are at the right conversation ..
                    UIPagingMessagesViewController * pagingViewController = (UIPagingMessagesViewController *)topViewController;
                    if ([pagingViewController.conversation.conversationId isEqualToString:theConversation.conversationId]) {
                        return;
                    }
                }
                
                if ([topViewController isKindOfClass:[MyInfosTableViewController class]]) {
                    [topViewController dismissViewControllerAnimated:NO completion:nil];
                }

                [self.navigationController popToRootViewControllerAnimated:YES];
                [self didStartConversation:[NSNotification notificationWithName:kConversationsManagerDidStartConversation object:theConversation] openFromNotification:YES];
            }
            
        }
        dispatch_group_notify(createGroup, dispatch_get_main_queue(), ^{
            if (needToOpenMeetingScreen) {
                
                UIViewController *topViewController = [UITools topViewController];
                if ([topViewController isKindOfClass:[MyInfosTableViewController class]]) {
                    [topViewController performSegueWithIdentifier:@"MeetingIdentifier" sender:topViewController];
                    return;
                }
                
                [self.navigationController popToRootViewControllerAnimated:YES];
                
                if (![self.tabBarController.selectedViewController isEqual:self]) {
                    self.tabBarController.selectedIndex = 0;
                }
                
                CustomNavigationController *meetingVC = (CustomNavigationController*)[[UIStoryboardManager sharedInstance].meetingStoryBoard instantiateViewControllerWithIdentifier:@"MeetingsViewControllerID"];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.navigationController pushViewController:meetingVC animated:YES];
                });
            }
        });
    }
}

#pragma mark - DZNEmptyDataSet
- (UIImage *)imageForEmptyDataSet:(UIScrollView *)scrollView {
    return [UIImage imageNamed:@"EmptyRecentsConversations"];
}

- (UIColor *)imageTintColorForEmptyDataSet:(UIScrollView *)scrollView {
    return [UITools defaultTintColor];
}

- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView {
    NSString *text = NSLocalizedString(@"You have no recent conversations.",nil);
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont fontWithName:[UITools defaultFontName] size:16.0f], NSForegroundColorAttributeName: [UIColor darkGrayColor]};
    
    return [[NSAttributedString alloc] initWithString:text attributes:attributes];
}

- (NSAttributedString *)descriptionForEmptyDataSet:(UIScrollView *)scrollView {
    return nil;
}

- (NSAttributedString *)buttonTitleForEmptyDataSet:(UIScrollView *)scrollView forState:(UIControlState)state {
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont fontWithName:[UITools defaultFontName] size:16.0f], NSForegroundColorAttributeName : [UIColor whiteColor]};
    
    return [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Start new conversation", nil) attributes:attributes];
}

-(UIImage *) buttonBackgroundImageForEmptyDataSet:(UIScrollView *)scrollView forState:(UIControlState)state {
    return [UITools imageWithColor:[UITools defaultTintColor] size:CGSizeMake(1, 40)];
}

- (UIColor *)backgroundColorForEmptyDataSet:(UIScrollView *)scrollView {
    return [UITools defaultBackgroundColor];
}

- (CGFloat)verticalOffsetForEmptyDataSet:(UIScrollView *)scrollView {
    CGFloat offset = CGRectGetHeight([UIApplication sharedApplication].statusBarFrame);
    offset += CGRectGetHeight(self.navigationController.navigationBar.frame);
    return -offset;
}

- (CGFloat)spaceHeightForEmptyDataSet:(UIScrollView *)scrollView {
    return 20.0f;
}

- (BOOL)emptyDataSetShouldDisplay:(UIScrollView *)scrollView {
    return _populated;
}

- (BOOL)emptyDataSetShouldAllowTouch:(UIScrollView *)scrollView {
    return YES;
}

- (BOOL)emptyDataSetShouldAllowScroll:(UIScrollView *)scrollView {
    return NO;
}

- (BOOL) emptyDataSetShouldAllowImageViewAnimate:(UIScrollView *)scrollView {
    return YES;
}

- (void)emptyDataSet:(UIScrollView *)scrollView didTapButton:(UIButton *)button {
    [((MyInfoNavigationItem*)self.navigationItem) focusSearchController];
}

- (void) presentContactDetailController:(Contact*) contact{
    UIContactDetailsViewController* controller = (UIContactDetailsViewController*)[[UIStoryboardManager sharedInstance].contactsDetailsStoryBoard instantiateViewControllerWithIdentifier:@"contactDetailsViewControllerID"];
    controller.contact = contact;
    controller.fromView = self;
    [self.navigationController pushViewController:controller animated:YES];
}

#pragma mark - 3D touch actions
- (void)traitCollectionDidChange:(UITraitCollection *)previousTraitCollection {
    [super traitCollectionDidChange:previousTraitCollection];
    if ([UITools isForceTouchAvailableForTraitCollection:self.traitCollection]) {
        if (!self.previewingContext) {
            self.previewingContext = [self registerForPreviewingWithDelegate:self sourceView:self.view];
        }
    } else {
        if (self.previewingContext) {
            [self unregisterForPreviewingWithContext:self.previewingContext];
            self.previewingContext = nil;
        }
    }
}


- (UIViewController *)previewingContext:(id<UIViewControllerPreviewing>)previewingContext viewControllerForLocation:(CGPoint)location {
    
    if ([self.presentedViewController isKindOfClass:[UIPagingMessagesViewController class]]) {
        return nil;
    }
    
    CGPoint translatedLocation = [self.view convertPoint:location toView:self.tableView];
    
    NSIndexPath *idx = [self.tableView indexPathForRowAtPoint:translatedLocation];
    if(idx){
        UIRainbowGenericTableViewCell *tableCell = (UIRainbowGenericTableViewCell*)[self.tableView cellForRowAtIndexPath:idx];
        Conversation *theConversation = ((Conversation*)tableCell.cellObject);
        
        UIPagingMessagesViewController *pagingMsgView = [[UIStoryboardManager sharedInstance].messagesStoryBoard instantiateViewControllerWithIdentifier:@"pagingMessageViewControllerID"];
        pagingMsgView.conversation = theConversation;
        
        UIMessagesViewController *msgView = pagingMsgView.messagesViewController;
        
        [[self class] prepareMessageView:msgView withConversation:theConversation];
        [pagingMsgView setViewControllers];
        CustomNavigationController *navCtrl = [[CustomNavigationController alloc] initWithRootViewController:pagingMsgView];
        
        previewingContext.sourceRect = [self.view convertRect:tableCell.frame fromView:self.tableView];
        
        return navCtrl;
    }
    return nil;
}

- (void)previewingContext:(id<UIViewControllerPreviewing>)previewingContext commitViewController:(UIViewController *)viewControllerToCommit {
    if([viewControllerToCommit isKindOfClass:[CustomNavigationController class]]){
        [self.navigationController pushViewController:((CustomNavigationController*)viewControllerToCommit).topViewController animated:YES];
    }
}

-(void) dumpConversations {
    NSLog(@"DUMP CONVERSATIONS : %@", [_conversations description]);
}

+(void) prepareMessageView:(UIMessagesViewController *) messageView withConversation:(Conversation *) conversation {
    messageView.conversation = conversation;
    messageView.fromView = self;
    
    // If we open a conversation with somebody which have no presence and no last-activity
    // get the last-activity of the user.
    if (conversation.type == ConversationTypeUser) {
        Contact *contact = (Contact*) conversation.peer;
        if (contact.presence.presence == ContactPresenceUnavailable &&
            contact.lastActivityDate == nil &&
            contact.isPresenceSubscribed && !contact.sentInvitation) {
            [[ServicesManager sharedInstance].contactsManagerService getLastActivityForContact:contact];
        }
    }
}

+(void) openConversationViewForConversation:(Conversation *) conversation inViewController:(UINavigationController *) navigationController {
    UIPagingMessagesViewController *pagingMsgView = [[UIStoryboardManager sharedInstance].messagesStoryBoard instantiateViewControllerWithIdentifier:@"pagingMessageViewControllerID"];
    pagingMsgView.conversation = conversation;
    
    [RecentsConversationsTableViewController prepareMessageView:pagingMsgView.messagesViewController withConversation:conversation];
    [pagingMsgView setViewControllers];
    [navigationController pushViewController:pagingMsgView animated:YES];

}

-(void) scrollToTop {
    NSInteger count = [[_conversationManagerService conversations] count];
    if (count > 0)
        [self.tableView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:YES];
}

#pragma mark - Favorite conversations list
-(void) didUpdateFavoriteConversation:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        __weak __typeof__(self) weakSelf = self;
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakSelf didUpdateFavoriteConversation:notification];
        });
        return;
    }
    
    @synchronized(_conversationsMutex) {
        if(_userFavorites)
            [_userFavorites removeAllObjects];
        else
            _userFavorites = [NSMutableArray new];
        
        _userFavorites = (NSMutableArray *)[[ServicesManager sharedInstance].favoritesManager favorites];
    }
    
    [self reloadCollectionViewAndChangeViewVisibility];
}

-(void) reloadCollectionViewAndChangeViewVisibility {
    if(!self.viewLoaded)
        return;
    
    [self.favoriteCollectionView reloadData];
    
    // Show the collection if favorite list is not empty,
    if([_userFavorites count] > 0) {
        [self.favoriteView setHidden:NO];
        self.topConstraint.constant = 70;
    } else {
        [self.favoriteView setHidden:YES];
        self.topConstraint.constant = 0;
    }
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (nonnull __kindof UICollectionViewCell *)collectionView:(nonnull UICollectionView *)collectionView cellForItemAtIndexPath:(nonnull NSIndexPath *)indexPath {
    NSString *reuseIdentifier = @"favoriteConversationViewCell";
    FavoriteConversationViewCell *favoriteCell = (FavoriteConversationViewCell*) [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    
    Peer *favorite = [_userFavorites objectAtIndex:indexPath.row];
    [favoriteCell setFavorite:favorite];
    
    Conversation *conversation = [_conversationManagerService getConversationWithPeerJID:favorite.jid];
    if(conversation)
        [favoriteCell setConversation:conversation];
    
    favoriteCell.alpha = 1;
    
    return favoriteCell;
}

- (NSInteger)collectionView:(nonnull UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    @synchronized(_conversationsMutex) {
        if (_userFavorites) {
            return [_userFavorites count];
        }
    }
    return 0;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    [collectionView deselectItemAtIndexPath:indexPath animated:YES];
    
    Peer *favoritePeer = [_userFavorites objectAtIndex:indexPath.row];
    [_conversationManagerService startConversationWithPeer:favoritePeer withCompletionHandler:nil];
}

-(void)syncFavoriteWithConversation:(Conversation *) conversation {
    if(_userFavorites) {
        [_userFavorites enumerateObjectsUsingBlock:^(Peer* _Nonnull peer, NSUInteger idx, BOOL * _Nonnull stop) {
            if([peer.rainbowID isEqualToString:conversation.peer.rainbowID]) {
                // Find the associated collectionview cell for favorites
                FavoriteConversationViewCell *cell = (FavoriteConversationViewCell*)[self.favoriteCollectionView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:idx inSection:0]];
                // set the associated conversation
                if(cell)
                    [cell setConversation:conversation];
                
                *stop = YES;
            }
        }];
    }
}


#pragma mark - Drag & dop for favorites

- (BOOL)collectionView:(UICollectionView *)collectionView canHandleDropSession:(id<UIDropSession>)session  API_AVAILABLE(ios(11.0)){
    if(session.localDragSession) {
        return YES;
    }
    return NO;
}

// Create dragged item from Favorites list
- (nonnull NSArray<UIDragItem *> *)collectionView:(nonnull UICollectionView *)collectionView itemsForBeginningDragSession:(nonnull id<UIDragSession>)session atIndexPath:(nonnull NSIndexPath *)indexPath  API_AVAILABLE(ios(11.0)){
    NSItemProvider *provider = [NSItemProvider new];
    UIDragItem *dragItem = [[UIDragItem alloc] initWithItemProvider:provider];
    
    Peer *peer = [_userFavorites objectAtIndex:indexPath.row];
    dragItem.localObject = peer;
//    dragItem.previewProvider = ^UIDragPreview * _Nullable{
//        return [self dragPreviewFromPeer:peer withFrame:CGRectMake(0, 0, 50, 50)];
//    };
    
    return @[dragItem];
}

// Create dragged item from Conversations list
- (nonnull NSArray<UIDragItem *> *)tableView:(nonnull UITableView *)tableView itemsForBeginningDragSession:(nonnull id<UIDragSession>)session atIndexPath:(nonnull NSIndexPath *)indexPath  API_AVAILABLE(ios(11.0)){
    NSItemProvider *provider = [NSItemProvider new];
    UIDragItem *dragItem = [[UIDragItem alloc] initWithItemProvider:provider];

    NSString *key = [[_conversations sections] objectAtIndex:indexPath.section];
    Conversation *theConversation = (Conversation *)[[_conversations objectsInSection:key] objectAtIndex:indexPath.row];

    dragItem.localObject = theConversation;
//    dragItem.previewProvider = ^UIDragPreview * _Nullable{
//        return [self dragPreviewFromPeer:theConversation.peer withFrame:CGRectMake(0, 0, 70, 70)];
//    };
    
    return @[dragItem];
}

// UIDragPreview * _Nullable (^previewProvider)(void)
-(UIDragPreview * _Nullable)dragPreviewFromPeer:(Peer *) peer withFrame:(CGRect) frame  API_AVAILABLE(ios(11.0)){
    UIAvatarView *avatarView = [[UIAvatarView alloc] initWithFrame: frame];
    avatarView.peer = peer;
    avatarView.asCircle = YES;
    avatarView.showPresence = NO;
    return [[UIDragPreview alloc] initWithView: avatarView];
}

- (UICollectionViewDropProposal *)collectionView:(UICollectionView *)collectionView dropSessionDidUpdate:(id<UIDropSession>)session withDestinationIndexPath:(NSIndexPath *)destinationIndexPath  API_AVAILABLE(ios(11.0)) {
    if(session.localDragSession == nil)
        return [[UICollectionViewDropProposal alloc] initWithDropOperation:UIDropOperationForbidden];

    if([[session.localDragSession items] count] > 0) {
        UIDragItem *dragItem = [[session.localDragSession items] firstObject];
        
        // Drag session is done from conversation list. Check if Peer is already in Favorites list
        if([dragItem.localObject isKindOfClass:[Conversation class]]) {
            // This Peer is already in Favorites, CANCEL
            if([_userFavorites containsObject:((Conversation*)dragItem.localObject).peer]) {
                return [[UICollectionViewDropProposal alloc] initWithDropOperation:UIDropOperationCancel];
            }
            if(![[ServicesManager sharedInstance].favoritesManager isValidForFavorite:((Conversation*)dragItem.localObject).peer] ) {
                return [[UICollectionViewDropProposal alloc] initWithDropOperation:UIDropOperationForbidden];
            }
        }
    }
    
    return [[UICollectionViewDropProposal alloc] initWithDropOperation:UIDropOperationMove intent:UICollectionViewDropIntentInsertAtDestinationIndexPath];
}

// Customize the dragged item
//- (UITargetedDragPreview *)dragInteraction:(UIDragInteraction *)interaction previewForLiftingItem:(UIDragItem *)item session:(id<UIDragSession>)session  API_AVAILABLE(ios(11.0)){
//    UITargetedDragPreview *preview = [[UITargetedDragPreview alloc] initWithView: [UIView new]];
//
//    return preview;
//}

//- (nullable UIDragPreviewParameters *)collectionView:(UICollectionView *)collectionView dropPreviewParametersForItemAtIndexPath:(NSIndexPath *)indexPath  API_AVAILABLE(ios(11.0)){
//    UIDragPreviewParameters *preview = [[UIDragPreviewParameters alloc] init];
//    preview.backgroundColor = [UIColor clearColor];
//
//    return preview;
//}

//- (nullable UIDragPreviewParameters *)tableView:(UITableView *)tableView dropPreviewParametersForItemAtIndexPath:(NSIndexPath *)indexPath  API_AVAILABLE(ios(11.0)){
//    return nil;
//}

// Perform the item drop in the Favorites collectionView and the appropriate action (update position or create)
- (void)collectionView:(UICollectionView *)collectionView performDropWithCoordinator:(id<UICollectionViewDropCoordinator>)coordinator  API_AVAILABLE(ios(11.0)){
    UIDragItem *dragItem = [[coordinator items] firstObject].dragItem;
    
    NSIndexPath *sourceIndexPath = [[[coordinator items] firstObject] sourceIndexPath];
    NSIndexPath *destinationIndexPath = [coordinator destinationIndexPath];
    Peer *peerToHandle;
    
    if(sourceIndexPath == destinationIndexPath)
        return;
    
    // Tip to know from where the drop is done:
    // Drop from Favorite: Peer object
    if([dragItem.localObject isKindOfClass:[Peer class]]) {
        peerToHandle = (Peer*)dragItem.localObject;
    }
    // Drop from Conversation: Conversation Object
    else if([dragItem.localObject isKindOfClass:[Conversation class]]) {
        peerToHandle = ((Conversation*)dragItem.localObject).peer;
    }
    
    if(destinationIndexPath)
    {
        [collectionView performBatchUpdates:^{
            if(sourceIndexPath)
                [_userFavorites removeObjectAtIndex:sourceIndexPath.row];
        
            if(destinationIndexPath.row < [_userFavorites count])
                [_userFavorites insertObject:peerToHandle atIndex:destinationIndexPath.row];
            else
                [_userFavorites addObject:peerToHandle];

            if(sourceIndexPath)
                [collectionView deleteItemsAtIndexPaths:@[sourceIndexPath]];
            
            [collectionView insertItemsAtIndexPaths:@[destinationIndexPath]];
            
        } completion:^(BOOL finished) {
            [coordinator dropItem:dragItem toItemAtIndexPath:destinationIndexPath];
        }];
    }
    
    if(sourceIndexPath) {
        [[ServicesManager sharedInstance].favoritesManager updateFavorite:peerToHandle toPosition:destinationIndexPath.row completionHandler:nil];
    } else if(destinationIndexPath) {
        [[ServicesManager sharedInstance].favoritesManager createFavorite:peerToHandle atPosition:destinationIndexPath.row completionHandler:nil];
    } else {
        [[ServicesManager sharedInstance].favoritesManager createFavorite:peerToHandle completionHandler:nil];
    }
}

// Handle drop from Collection to "Outside" (-> tableView) for removing Favorites from list
- (UITableViewDropProposal *)tableView:(UITableView *)tableView dropSessionDidUpdate:(id<UIDropSession>)session withDestinationIndexPath:(NSIndexPath *)destinationIndexPath  API_AVAILABLE(ios(11.0)){
    if(session.localDragSession == nil)
        return [[UITableViewDropProposal alloc] initWithDropOperation:UIDropOperationForbidden];

    return [[UITableViewDropProposal alloc] initWithDropOperation:UIDropOperationMove intent:UITableViewDropIntentUnspecified];
}

- (void)tableView:(nonnull UITableView *)tableView performDropWithCoordinator:(nonnull id<UITableViewDropCoordinator>)coordinator  API_AVAILABLE(ios(11.0)){
    UIDragItem *dragItem = [[coordinator items] firstObject].dragItem;
    Peer *peerToHandle;

    // Tip to know from where the drop is done:
    // Drop from Favorite: Peer object
    if([dragItem.localObject isKindOfClass:[Peer class]]) {
        peerToHandle = (Peer*)dragItem.localObject;
        
        if(peerToHandle)
            [[ServicesManager sharedInstance].favoritesManager deleteFavorite:peerToHandle completionHandler:nil];
    }
}

@end
