/*
 * Rainbow
 *
 * Copyright (c) 2019, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "FavoriteConversationViewCell.h"
#import <Rainbow/ServicesManager.h>
#import <Rainbow/Room.h>
#import <Rainbow/Contact.h>


@interface FavoriteConversationViewCell()

@property (nonatomic, strong) Conversation *associatedConversation;

@end


@implementation FavoriteConversationViewCell

- (void) awakeFromNib {
    [super awakeFromNib];
    
    _avatar.asCircle = YES;
    
    // Unread messages badge
    _badgeView.backgroundColor = [UITools notificationBadgeColor];
    _badgeView.layer.cornerRadius = _badgeView.frame.size.width / 2;
    [_badgeView setHidden:YES];
}

- (void) prepareForReuse {
    [super prepareForReuse];
    [self setConversation:nil];
    [self updateBadgeView];
}

-(void)dealloc {
    [self setConversation:nil];
    [_badgeView setHidden:YES];
}


-(void) setFavorite:(Peer * _Nonnull) favorite {
    if(favorite) {
        if(_avatar.peer) {
            if ([_avatar.peer isKindOfClass:[Contact class]]) {
                [_avatar.peer removeObserver:self forKeyPath:kContactLastNameKey context:nil];
                [_avatar.peer removeObserver:self forKeyPath:kContactFirstNameKey context:nil];
            }
            if ([_avatar.peer isKindOfClass:[Room class]]) {
                [_avatar.peer removeObserver:self forKeyPath:kRoomParticipantsKey];
            }
        }
        
        [self setLabel:favorite];
        _avatar.peer = favorite;
        
        if([_avatar.peer isKindOfClass:[Contact class]]) {
            [_avatar.peer addObserver:self forKeyPath:kContactLastNameKey options:NSKeyValueObservingOptionNew context:nil];
            [_avatar.peer addObserver:self forKeyPath:kContactFirstNameKey options:NSKeyValueObservingOptionNew context:nil];
        } else if ([_avatar.peer isKindOfClass:[Room class]]) {
            [_avatar.peer addObserver:self forKeyPath:kRoomParticipantsKey options:NSKeyValueObservingOptionNew context:nil];
        }
        
        [self setConversation:nil];
    }
}


-(void) setConversation:(Conversation *) conversation {
    if (_associatedConversation)
        [_associatedConversation removeObserver:self forKeyPath:@"unreadMessagesCount"];
    _associatedConversation = conversation;
    if (conversation)
        [_associatedConversation addObserver:self forKeyPath:@"unreadMessagesCount" options:NSKeyValueObservingOptionNew context:nil];

    [self updateBadgeView];
}


-(void) setLabel:(Peer *) peer {
    if([peer isKindOfClass:[Room class]]) {
        _displayName.text = peer.displayName;
    }
    
    else {
        NSString *displayName = @"";
        if([[ServicesManager sharedInstance].contactsManagerService displayFirstNameFirst])
            displayName = [((Contact*)peer) firstName];
        else
            displayName = [((Contact*)peer) lastName];
        
        _displayName.text = [displayName capitalizedString];
    }
    
    [self setNeedsLayout];
}

-(void) updateBadgeView {
    if (_associatedConversation)
        [_badgeView setHidden: !(_associatedConversation.unreadMessagesCount > 0)];
    else
        [_badgeView setHidden:YES];
}

-(void) observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context {
    // Unread message in conversation
    if([keyPath isEqualToString:@"unreadMessagesCount"]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self updateBadgeView];
        });
    }
    // Contact names update
    if([keyPath isEqualToString:kContactLastNameKey] || [keyPath isEqualToString:kContactFirstNameKey]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self setLabel:_avatar.peer];
        });
    }
    // Room participants update
    else if([keyPath isEqualToString:kRoomParticipantsKey]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self setLabel:_avatar.peer];
        });
    }
}
@end
