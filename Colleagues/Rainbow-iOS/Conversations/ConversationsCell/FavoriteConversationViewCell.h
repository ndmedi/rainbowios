/*
 * Rainbow
 *
 * Copyright (c) 2019, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import <UIKit/UIKit.h>
#import "UITools.h"
#import "UIAvatarView.h"
#import <Rainbow/MyUser.h>
#import <Rainbow/Conversation.h>

@interface FavoriteConversationViewCell : UICollectionViewCell
@property (nonatomic, strong) IBOutlet UILabel *displayName;
@property (nonatomic, strong) IBOutlet UIAvatarView *avatar;
@property (nonatomic, strong) IBOutlet UIView *badgeView;

-(void) setFavorite:(Peer *_Nonnull) favorite;
-(void) setConversation:(Conversation *) conversation;
@end
