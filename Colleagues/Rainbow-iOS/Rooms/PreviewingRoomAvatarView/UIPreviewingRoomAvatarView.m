/*
 * Rainbow
 *
 * Copyright (c) 2017, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "UIPreviewingRoomAvatarView.h"

@interface UIAvatarView (protected)
@property (nonatomic, strong) UIImageView* contactPictureView;
@end

@implementation UIPreviewingRoomAvatarView

-(void) setParticipantList:(NSArray <Contact*> *) participants {
    
    NSMutableOrderedSet<Contact *> *participantsToDisplay = nil;
    participantsToDisplay = [NSMutableOrderedSet orderedSetWithArray:participants];
    
    [self drawPhotoFromOrderedList:participantsToDisplay];
}

@end
