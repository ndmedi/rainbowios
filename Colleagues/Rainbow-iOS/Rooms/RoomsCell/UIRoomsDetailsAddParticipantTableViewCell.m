/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "UIRoomsDetailsAddParticipantTableViewCell.h"
#import "UIRoomsDetailsAddParticipantTableViewCell+Internal.h"

@implementation UIRoomsDetailsAddParticipantTableViewCell

-(void) awakeFromNib {
    [super awakeFromNib];
    [UITools applyCustomFontTo:_addParticipantButton.titleLabel];
    _addParticipantButton.tintColor = [UITools defaultTintColor];
    [_addParticipantButton setTitleColor:[UITools defaultTintColor] forState:UIControlStateNormal];
    [_addParticipantButton setTitle:NSLocalizedString(@"Add attendees", nil) forState:UIControlStateNormal];
    [self setTintColor:[UITools defaultTintColor]];
    self.contentView.backgroundColor = [UITools defaultBackgroundColor];
}

@end
