/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import <UIKit/UIKit.h>
#import <Rainbow/Room.h>
#import "UINoConnectionViewController.h"

@interface UIRoomsTableViewController : UINoConnectionViewController
+(BOOL) hasAcceptedParticipantsInRoom:(Room *) room;
+(void)leaveAction:(Room *)room inController:(UIViewController *) controller completionHandler:(void (^ __nullable)())completionHandler;
+(void)archiveAction:(Room *)room inController:(UIViewController *) controller completionHandler:(void (^ __nullable)())completionHandler;
+(void)deleteAction:(Room *)room inController:(UIViewController *) controller completionHandler:(void (^ __nullable)())completionHandler;
@end
