/*
 * Rainbow
 *
 * Copyright (c) 2017, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "UICallInProgressViewController.h"
#import "UITools.h"

@interface UICallInProgressViewController ()
@property (weak, nonatomic) IBOutlet UIButton *callInProgressButton;
@property (nonatomic, strong) NSTimer *durationRefreshTimer;
@end

@implementation UICallInProgressViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UITools colorFromHexa:0x34B233FF];
    _callInProgressButton.backgroundColor = [UITools colorFromHexa:0x34B233FF];
    _callInProgressButton.userInteractionEnabled = YES;
}

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    _callInProgressButton.tintColor = [UIColor whiteColor];
    [UITools applyCustomFontTo:_callInProgressButton.titleLabel];
    [self updateDuration];
    
    if(!_durationRefreshTimer)
        _durationRefreshTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateDuration) userInfo:nil repeats:YES];
}

-(void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [_durationRefreshTimer invalidate];
    _durationRefreshTimer = nil;
}

-(void) updateDuration {
    if(_call.connectionDate){
        NSTimeInterval interval = [[NSDate date] timeIntervalSinceDate:_call.connectionDate];
        NSString *duration = [UITools timeFormatConvertToSeconds:[NSString stringWithFormat:@"%f", interval] zeroFormattingBehaviorunitsStyle:NSDateComponentsFormatterZeroFormattingBehaviorNone unitsStyle:NSDateComponentsFormatterUnitsStylePositional];
        [_callInProgressButton setTitle:[NSString stringWithFormat:NSLocalizedString(@"Call in progress %@", nil), duration] forState:UIControlStateNormal];
    } else {
        [_callInProgressButton setTitle:[NSString stringWithFormat:NSLocalizedString(@"Call in progress", nil)] forState:UIControlStateNormal];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)callInProgressButtonClicked:(UIButton *)sender {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"showCallView" object:_call];
}

-(UIStatusBarStyle) preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

-(BOOL) shouldAutorotate {
    return NO;
}

- (BOOL)prefersStatusBarHidden {
    return NO;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return UIInterfaceOrientationPortrait;
}

@end
