/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "CustomSearchController.h"
#import "UISearchResultController.h"
#import "UITools.h"
#import <Rainbow/Tools.h>
#import <Rainbow/RainbowUserDefaults.h>
#import <Rainbow/ServicesManager.h>
#import <Rainbow/ContactsManagerService.h>
#import "UISearchController+StatusBarStyle.h"
#import "Firebase.h"
@interface CustomSearchController ()  <UISearchBarDelegate, UISearchControllerDelegate, UISearchResultControllerProtocol>

@property (nonatomic, strong) NSArray *rosterSearchedUsers;
@property (nonatomic, strong) NSArray *localSearchedContacts;
@property (nonatomic, strong) NSArray *serverSearchedUsers;
@property (nonatomic, strong) ServicesManager *serviceManager;
@property (nonatomic, strong) ContactsManagerService *contactsManager;
@property (nonatomic, weak) id<CustomSearchControllerDelegate> customSearchDelegate;
@property (nonatomic, strong) NSOperationQueue *searchOperationQueue;
@property (nonatomic) SearchScope userDefinedSearchScope;
@property (nonatomic, strong) UIAlertController *filterSearchMenuActionSheet;
@end

@implementation CustomSearchController

-(instancetype) initWithSearchResultsController:(UISearchResultController*)searchResultController customSearchControllerDelegate:(id<CustomSearchControllerDelegate>) customSearchDelegate {
    self = [super initWithSearchResultsController:searchResultController];
    if(self){
        searchResultController.controllerDelegate = self;
        _customSearchDelegate = customSearchDelegate;
        self.delegate = self;
        self.hidesNavigationBarDuringPresentation = NO;
        self.searchBar.showsCancelButton = NO;
        self.searchBar.showsBookmarkButton = YES;
        [self.searchBar setImage:[UIImage imageNamed:@"filter_off"] forSearchBarIcon:UISearchBarIconBookmark state:UIControlStateNormal];
        [self.searchBar setValue:NSLocalizedString(@"Done", @"Done") forKey:@"_cancelButtonText"];
        self.searchBar.hidden = NO;
        self.searchBar.translucent = YES;
        self.searchBar.backgroundImage = [UITools imageFromColor:[UIColor clearColor]];
        self.searchBar.barTintColor = [UITools defaultBackgroundColor];
        self.searchBar.tintColor = [UITools defaultTintColor];
        self.searchBar.placeholder = NSLocalizedString(@"People and conversations", nil);
        self.searchBar.searchTextPositionAdjustment = UIOffsetMake(5.0f, 0.0f);
        self.searchBar.delegate = self;
        [self.searchBar setBackgroundImage:[UIImage new]];
        [self.searchBar setScopeBarBackgroundImage:[UIImage new]];
        
        CGSize size = CGSizeMake(30, 30);
        UIGraphicsBeginImageContextWithOptions(size, NO, 1);
        [[UIBezierPath bezierPathWithRoundedRect:CGRectMake(0,0,30,30) cornerRadius:2.0] addClip];
        [[UIColor whiteColor] setFill];
        
        UIRectFill(CGRectMake(0, 0, size.width, size.height));
        UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        [self.searchBar setSearchFieldBackgroundImage:image forState:UIControlStateNormal];
    
        self.searchBar.barStyle = UIBarStyleDefault;
        if([[UIDevice currentDevice].systemVersion compare:@"9.1" options:NSNumericSearch] == NSOrderedDescending){
            self.obscuresBackgroundDuringPresentation = YES;
        }
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userDefinedSearchScopeDidChanged:) name:@"userDefinedSearchScopeChanged" object:nil];
        
        _serviceManager = [ServicesManager sharedInstance];
        _contactsManager = _serviceManager.contactsManagerService;
        _searchOperationQueue = [NSOperationQueue new];
        _searchOperationQueue.maxConcurrentOperationCount = 1;
        
        _handleDismissOnCancel = YES;
        _userDefinedSearchScope = SearchScopeContacts | SearchScopeConversations | SearchScopeRooms;
        
        NSNumber *userDefinedSearchScope = [[RainbowUserDefaults sharedInstance] objectForKey:@"userDefinedSearchScope"];
        if(userDefinedSearchScope)
           [self applyFilter:[userDefinedSearchScope integerValue] sendNotification:NO];
        
        _searchScope = _userDefinedSearchScope;
    }
    return self;
}

-(void) dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"userDefinedSearchScopeChanged" object:nil];
    
    [self.searchBar removeFromSuperview];
    [_searchOperationQueue cancelAllOperations];
    _searchOperationQueue = nil;
    _rosterSearchedUsers = nil;
    _serverSearchedUsers = nil;
    _localSearchedContacts = nil;
    _contactsManager = nil;
    _serviceManager = nil;
}

-(void)userDefinedSearchScopeDidChanged:(NSNotification *)notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self userDefinedSearchScopeDidChanged:notification];
        });
        return;
    }
    
    NSNumber *userDefinedSearchScope = [[RainbowUserDefaults sharedInstance] objectForKey:@"userDefinedSearchScope"];
    if(userDefinedSearchScope && [userDefinedSearchScope integerValue] != _userDefinedSearchScope) {
        if([userDefinedSearchScope integerValue] != _userDefinedSearchScope)
            _userDefinedSearchScope = [userDefinedSearchScope integerValue];
        
        [self applyFilter:_userDefinedSearchScope sendNotification:NO];
    }
}

#pragma mark - UISearchBarDelegate
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    CustomSearchController * __weak blockSelf = self;
    // you can cancel existing operations here if you want
    [_searchOperationQueue cancelAllOperations];
    // Reset also the UI
    ((UISearchResultController*)self.searchResultsController).textToSearch = searchText;
    ((UISearchResultController*)self.searchResultsController).isTyping = YES;
    ((UISearchResultController*)self.searchResultsController).results = nil;
    // Reset search scope if text is cleared
    if([searchText length] == 0)
        self.searchScope = _userDefinedSearchScope;
    NSString *outsideTextAsItWasWhenStarted = [NSString stringWithString:searchText];
//    NSLog(@"Queueing with '%@'", outsideTextAsItWasWhenStarted);
    [_searchOperationQueue addOperationWithBlock:^{
        NSString *textAsItWasWhenStarted = [NSString stringWithString:outsideTextAsItWasWhenStarted] ;
        [NSThread sleepForTimeInterval:0.5];
        if (blockSelf.searchOperationQueue.operationCount == 1) {
            dispatch_sync(dispatch_get_main_queue(), ^{
                if (![textAsItWasWhenStarted isEqualToString:blockSelf.searchBar.text]) {
//                    NSLog(@"NOT up to date with '%@'", textAsItWasWhenStarted);
                } else {
//                    NSLog(@"Up to date with '%@'", textAsItWasWhenStarted);
                    [blockSelf performSelectorInBackground:@selector(performSearchWithSearchText:) withObject:textAsItWasWhenStarted];
                }
            });
        } else {
//            NSLog(@"Skipped as out of date with '%@'", textAsItWasWhenStarted);
        }
    }];
}
- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar {
    // Set keyboard type here, before becoming first responder
    [self.searchBar setKeyboardType:[[NSUserDefaults standardUserDefaults] integerForKey:@"searchKeyboardType"]];
    return YES;
}

- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar {
    return YES;
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    [self.searchBar setImage:[UITools imageFromColor:[UIColor clearColor]] forSearchBarIcon:UISearchBarIconSearch state:UIControlStateNormal];
    self.searchBar.searchTextPositionAdjustment = UIOffsetMake(-10.0f, 0.0f);
    ((UISearchResultController*)self.searchResultsController).textToSearch = self.searchBar.text;
    ((UISearchResultController*)self.searchResultsController).isTyping = YES;
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar {
    [self.searchBar setImage:nil forSearchBarIcon:UISearchBarIconSearch state:UIControlStateNormal];
    self.searchBar.searchTextPositionAdjustment = UIOffsetMake(5.0f, 0.0f);
    ((UISearchResultController*)self.searchResultsController).textToSearch = nil;
    ((UISearchResultController*)self.searchResultsController).isTyping = NO;
    self.searchScope = _userDefinedSearchScope;
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    [self dismissSearchController];
   // NSMutableArray * selectedResultsArray = ((UISearchResultController*)self.searchResultsController).selectedresultsMuttableArray;
    [_customSearchDelegate customSearchController:self didPressCancelButton:searchBar];
}

#pragma mark - UISearchControllerDelegate
- (void)willPresentSearchController:(UISearchController *)searchController {
    if([_customSearchDelegate respondsToSelector:@selector(willPresentSearchController:)])
        [_customSearchDelegate willPresentSearchController:self];
}

- (void)didPresentSearchController:(UISearchController *)searchController {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.searchBar becomeFirstResponder];
    });
}

- (void)willDismissSearchController:(UISearchController *)searchController {
    if([_customSearchDelegate respondsToSelector:@selector(willDismissSearchController:)])
        [_customSearchDelegate willDismissSearchController:self];
}

- (void)didDismissSearchController:(UISearchController *)searchController {
    [self.searchBar resignFirstResponder];
    
    if(_handleDismissOnCancel){
        // This happens when we click on the gray zone, and we want to act like a cancel.
        if (((UISearchResultController*)self.searchResultsController).results.count == 0){
            [self dismissSearchController];
            [_customSearchDelegate customSearchController:self didPressCancelButton:self.searchBar];
        }
    }
}

- (void) changeKeyboardType:(UIKeyboardType) type {
    [[NSUserDefaults standardUserDefaults] setInteger:type forKey:@"searchKeyboardType"];
    [self.searchBar resignFirstResponder];
    [self.searchBar setKeyboardType:type];
    [self.searchBar becomeFirstResponder];
}

-(void) setSearchScope:(SearchScope)searchScope {
    if(_searchScope != searchScope) {
        _searchScope = searchScope;
    }
    [self performSearchWithSearchText:self.searchBar.text];
}

-(void) performSearchWithSearchText:(NSString *) searchText {
    if(searchText.length > 0){
        NSMutableArray *results = [NSMutableArray array];
        
        if(_searchScope & SearchScopeMessages) {
            if(searchText.length >= 3){
                [_searchOperationQueue cancelAllOperations];
                
                // Search messages in all conversations
                [[ServicesManager sharedInstance].conversationsSearchHelper searchConversationsWithText: searchText];

            }
        } else if((_searchScope & SearchScopeContacts) || (_searchScope & SearchScopeRemoteContacts)) {
            [results addObjectsFromArray:[self filterContactsResult: [_contactsManager searchContactsWithPattern:searchText]]];
        }
        
        if (_searchScope & SearchScopeLocalContacts)
            [results addObjectsFromArray:[_contactsManager searchAdressBookWithPattern:searchText]];
        
        if(_searchScope & SearchScopeConversations)
            [results addObjectsFromArray:[[ServicesManager sharedInstance].conversationsManagerService searchConversationWithPattern:searchText]];
        
        if(_searchScope & SearchScopeRooms)
            [results addObjectsFromArray:[[ServicesManager sharedInstance].roomsService searchRoomWithPattern:searchText]];
        
        if(_searchScope & SearchScopeGroups)
            [results addObjectsFromArray:[[ServicesManager sharedInstance].groupsService searchGroupsWithPattern:searchText]];
        
        // SearchScopeCall - We don't check the scope because we do this search in all cases
        // Trim whitespace and unicode special characters
        NSString *trimmedPhoneNumber = [Tools trimAndRemoveUnicodeFormatFromNumber:searchText];
        if([trimmedPhoneNumber rangeOfString:@"^([(\\+)|(00)|(\\*)||(\\#)]?)[0-9]{2,16}$" options:NSRegularExpressionSearch].location != NSNotFound){
            PhoneNumber *aPhoneNumber = [[PhoneNumber alloc] initWithPhoneNumberString:trimmedPhoneNumber];
            [results addObject:aPhoneNumber];
        }
        
        if(searchText.length >= 2){
            if(_searchScope & SearchScopeRemoteContacts){
                [_contactsManager searchRemoteContactsWithPattern:searchText withCompletionHandler:^(NSString *searchPattern, NSArray<Contact *> *foundContacts) {
                    if(_searchScope & SearchScopeMessages) // Do not handle these results when searching messages
                        return;
                    NSMutableArray *result2 = [NSMutableArray arrayWithArray:((UISearchResultController*)self.searchResultsController).results];
                    [[self filterContactsResult:foundContacts] enumerateObjectsUsingBlock:^(Contact * contact, NSUInteger idx, BOOL * stop) {
                        if(![result2 containsObject:contact])
                            [result2 addObject:contact];
                    }];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        ((UISearchResultController*)self.searchResultsController).results = result2;
                    });
                }];
            }
            
            if(_searchScope & SearchScopeCompanies){
                [[ServicesManager sharedInstance].companiesService searchRainbowCompaniesWithPattern:searchText withCompletionBlock:^(NSString *searchPattern, NSArray<Company *> *foundCompanies) {
                    if(_searchScope & SearchScopeMessages) // Do not handle these results when searching messages
                        return;
                    
                    NSMutableArray *resultsWithCompanies = [NSMutableArray arrayWithArray:((UISearchResultController*)self.searchResultsController).results];
                    [resultsWithCompanies addObjectsFromArray:foundCompanies];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        ((UISearchResultController*)self.searchResultsController).results = resultsWithCompanies;
                    });
                }];
            }
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            ((UISearchResultController*)self.searchResultsController).results = results;
            ((UISearchResultController*)self.searchResultsController).isTyping = NO;
        });
    }
}

-(NSMutableArray *)filterContactsResult:(NSArray *)contacts {
    NSMutableArray *filteredContactsResult = [NSMutableArray new];
    
    [contacts enumerateObjectsUsingBlock:^(Contact * contact, NSUInteger idx, BOOL * stop) {
        // Filter contacts found by company id
        if(_userDefinedSearchScope == (SearchScopeRemoteContacts)) {
            if([[ServicesManager sharedInstance].myUser.contact.companyId isEqual: contact.companyId] || contact.isPBXContact)
                [filteredContactsResult addObject:contact];
        }
        // Do not filter contact by company
        else {
            [filteredContactsResult addObject:contact];
        }
    }];
    
    return filteredContactsResult;
}

- (NSMutableArray *) removeContactsAlreadyInRoster:(NSArray *) searchResult {
    NSMutableArray* contacts = [NSMutableArray new];
    [searchResult enumerateObjectsUsingBlock:
     ^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
         Contact* contact = obj;
         if ( ! [_rosterSearchedUsers containsObject:contact] ) {
             [contacts addObject:contact];
         }
     }
     ];
    return contacts;
}

-(void) dismissSearchControllerAndPerformAction:(void (^ __nullable)(void)) action {
    if(_handleDismissOnCancel){
        [self dismissViewControllerAnimated:YES completion:^{
            if(action)
                action();
        }];
    } else {
        if(action)
            action();
    }
    self.active = NO;
}

- (void) dismissSearchController {
    if(self.active && _handleDismissOnCancel){
            [self dismissViewControllerAnimated:YES completion:nil];
        self.active = NO;
    }
}


-(void) searchBarBookmarkButtonClicked:(UISearchBar *)searchBar {
    if (_filterSearchMenuActionSheet) {
        [_filterSearchMenuActionSheet dismissViewControllerAnimated:NO completion:nil];
        _filterSearchMenuActionSheet = nil;
    }
    
    _filterSearchMenuActionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    // Filter 1
    UIAlertAction* filter1 = [UIAlertAction actionWithTitle:NSLocalizedString(@"People and conversations", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        [self applyFilter:SearchScopeConversations|SearchScopeRooms|SearchScopeContacts sendNotification:YES];
    }];
    
    if(_userDefinedSearchScope == (SearchScopeConversations|SearchScopeRooms|SearchScopeContacts)) {
        [filter1 setValue:[[UIImage imageNamed:@"Checkmark_status"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forKey:@"image"];
    }
    [_filterSearchMenuActionSheet addAction:filter1];

    // Filter 2
    UIAlertAction* filter2 = [UIAlertAction actionWithTitle:NSLocalizedString(@"Members of my company", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        [self applyFilter:SearchScopeRemoteContacts sendNotification:YES];
    }];
    if(_userDefinedSearchScope == (SearchScopeRemoteContacts)) {
        [filter2 setValue:[[UIImage imageNamed:@"Checkmark_status"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forKey:@"image"];
    }
    [_filterSearchMenuActionSheet addAction:filter2];

    // Filter 3
    UIAlertAction* filter3 = [UIAlertAction actionWithTitle:NSLocalizedString(@"Companies", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        [self applyFilter:SearchScopeCompanies sendNotification:YES];
    }];
    if(_userDefinedSearchScope == (SearchScopeCompanies)) {
        [filter3 setValue:[[UIImage imageNamed:@"Checkmark_status"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forKey:@"image"];
    }
    [_filterSearchMenuActionSheet addAction:filter3];
    
    // Cancel action
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style:UIAlertActionStyleCancel handler:nil];
    [_filterSearchMenuActionSheet addAction:cancel];
    
    // Show ui
    self.active = YES;
    
    [_filterSearchMenuActionSheet.view setTintColor:[UITools defaultTintColor]];
    [self presentViewController:_filterSearchMenuActionSheet animated:YES completion:nil];
}

-(void)applyFilter:(SearchScope) scope sendNotification:(BOOL)sendNotification {
    if(scope != _userDefinedSearchScope) {
        if(sendNotification) {
            [[RainbowUserDefaults sharedInstance] setObject:[NSNumber numberWithInteger:scope] forKey:@"userDefinedSearchScope"];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"userDefinedSearchScopeChanged" object:nil];
        }
        _userDefinedSearchScope = scope;
    }
    
    // F1: SearchScopeConversations|SearchScopeRooms|SearchScopeContacts
    // F2: SearchScopeRemoteContacts
    // F3: SearchScopeCompanies

    if([self isDefaultFilter]) {
        [self.searchBar setImage:[UIImage imageNamed:@"filter_off"] forSearchBarIcon:UISearchBarIconBookmark state:UIControlStateNormal];
        self.searchBar.placeholder = NSLocalizedString(@"People and conversations", nil);
    } else {
        [self.searchBar setImage:[UIImage imageNamed:@"filter_on"] forSearchBarIcon:UISearchBarIconBookmark state:UIControlStateNormal];
        if(_userDefinedSearchScope == SearchScopeRemoteContacts)
            self.searchBar.placeholder = NSLocalizedString(@"Members of my company", nil);
        if(_userDefinedSearchScope == SearchScopeCompanies)
            self.searchBar.placeholder = NSLocalizedString(@"Companies", nil);
    }
    
    [self setSearchScope:scope];
}

-(BOOL)isDefaultFilter {
    if(_userDefinedSearchScope == (SearchScopeConversations|SearchScopeRooms|SearchScopeContacts))
        return YES;
    return NO;
}

@end
