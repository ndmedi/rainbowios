/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import <Foundation/Foundation.h>

@interface GoogleAnalytics : NSObject

@property (nonatomic, readwrite) BOOL authorizationGranted;
@property (nonatomic, readwrite) BOOL authorizationAlreadyRequested;

+(instancetype) sharedInstance;
-(void) dispatchEventWithCategory:(NSString *)category action:(NSString *)action label:(NSString *)label value:(NSNumber *)value;

@end
