//
//  UIMyInfoAvatarView.m
//  Rainbow-iOS
//
//  Created by Le Trong Nghia Huynh on 08/02/2019.
//  Copyright © 2019 ALE International. All rights reserved.
//

#import "UIMyInfoAvatarView.h"
#import "UITools.h"

#define kNotificationBadgeLabelWidth 14
#define kContactPresencePaddingLeft 4
#define kContactPresencePaddingTop 1

@interface UIAvatarView (protected)
@property (nonatomic, strong) UIImageView* contactPictureView;
@end

@interface UIMyInfoAvatarView ()
@property (nonatomic, strong) UILabel *notificationBadgeLabel;
@end

@implementation UIMyInfoAvatarView

-(id) initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    [self initNotificationBadgeLabel];
    return self;
}

-(instancetype) initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    [self initNotificationBadgeLabel];
    return self;
}

-(void) initNotificationBadgeLabel {
    _notificationBadgeLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.contactPictureView.frame.size.width - kNotificationBadgeLabelWidth, -4, kNotificationBadgeLabelWidth, kNotificationBadgeLabelWidth)];
    _notificationBadgeLabel.layer.masksToBounds = YES;
    _notificationBadgeLabel.layer.cornerRadius = _notificationBadgeLabel.frame.size.width/2.0;
    _notificationBadgeLabel.backgroundColor = [UITools notificationBadgeColor];
    _notificationBadgeLabel.textColor = [UIColor whiteColor];
    [UITools applyCustomFontTo:_notificationBadgeLabel andSize:9];
    _notificationBadgeLabel.isAccessibilityElement = YES;
    _notificationBadgeLabel.accessibilityLabel = @"NotificationBadgeIcon";
    _notificationBadgeLabel.hidden = YES;
    _notificationBadgeLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:_notificationBadgeLabel];
}

-(void) displayNotificationBadgeWithCount:(int) count {
    if (count > 0) {
        [_notificationBadgeLabel setHidden:NO];
        [_notificationBadgeLabel setText:[@(count) stringValue]];
    } else {
        [_notificationBadgeLabel setHidden:YES];
        [_notificationBadgeLabel setText:@""];
    }
}

@end
