//
//  UIChannelsDiscoverByCategoryFlowLayout.h
//  Rainbow-iOS
//
//  Created by Le Trong Nghia Huynh on 26/02/2019.
//  Copyright © 2019 ALE International. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIChannelsDiscoverByCategoryFlowLayout : UICollectionViewFlowLayout

@property (nonatomic, assign) int padding;
-(instancetype) initWithPadding:(int) padding;

@end
