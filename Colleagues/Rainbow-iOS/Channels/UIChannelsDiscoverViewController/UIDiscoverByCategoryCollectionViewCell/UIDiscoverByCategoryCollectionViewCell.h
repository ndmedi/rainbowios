//
//  UIDiscoverByCategoryCollectionViewCell.h
//  Rainbow-iOS
//
//  Created by Le Trong Nghia Huynh on 26/02/2019.
//  Copyright © 2019 ALE International. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <Rainbow/ChannelsService.h>
#import "Channel+Rainbow.h"

@interface UIDiscoverByCategoryCollectionViewCell : UICollectionViewCell

@property (nonatomic) ChannelCategory category;

@end
