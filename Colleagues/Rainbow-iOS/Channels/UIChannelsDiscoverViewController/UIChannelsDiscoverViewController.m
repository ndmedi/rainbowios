//
//  UIChannelsDiscoverViewController.m
//  Rainbow-iOS
//
//  Created by Le Trong Nghia Huynh on 26/02/2019.
//  Copyright © 2019 ALE International. All rights reserved.
//

#import "UIChannelsDiscoverViewController.h"
#import "UIChannelsListViewController.h"
#import "UIChannelsItemsViewController.h"
#import "UIChannelsDiscoverByCategoryFlowLayout.h"
#import "UIDiscoverByCategoryCollectionViewCell.h"
#import "UIChannelsGridFlowLayout.h"
#import "UIChannelCollectionViewCell.h"
#import "UIChannelViewMoreCollectionViewCell.h"
#import "FilteredSortedSectionedArray.h"
#import <Rainbow/ServicesManager.h>
#import <Rainbow/ChannelsService.h>
#import "UITools.h"

#define kCollectionViewDiscoverByCategoryReusableKey @"discoverByCategoryCellID"
#define kCollectionViewCellReusableKey @"channelCellID"
#define kCollectionViewMoreCellReusableKey @"channelViewMoreCellID"
#define kTagByCategoriesCollectionView 0
#define kTagMostFollowedCollectionView 1
#define kTagMostRecentsCollectionView 2

#define kNumberOfMostAndRecentItems 10

@interface UIChannelsDiscoverViewController () <UICollectionViewDelegate, UICollectionViewDataSource>

@property (nonatomic, strong) ChannelsService *channelsService;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *contentView;


// Explore by categories
@property (nonatomic, strong) FilteredSortedSectionedArray *categories;
@property (weak, nonatomic) IBOutlet UICollectionView *categoriesCollectionView;
@property (nonatomic, strong) UIChannelsDiscoverByCategoryFlowLayout *discoverByCategoryFlowLayout;
@property (weak, nonatomic) IBOutlet UILabel *exploreChannelsByCategoriesLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *categoriesCollectionViewHeightConstraint;

// Most followed
@property (nonatomic, strong) FilteredSortedSectionedArray *mostFollowedChannels;
@property (weak, nonatomic) IBOutlet UICollectionView *mostFollowedChannelsCollectionView;
@property (nonatomic, strong) UIChannelsGridFlowLayout *mostFollowedFlowLayout;
@property (weak, nonatomic) IBOutlet UILabel *mostFollowedChannelsLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *mostFollowedCollectionViewHeightConstraint;

// Most recents
@property (nonatomic, strong) FilteredSortedSectionedArray *mostRecentsChannels;
@property (weak, nonatomic) IBOutlet UICollectionView *mostRecentsChannelsCollectionView;
@property (nonatomic, strong) UIChannelsGridFlowLayout *mostRecentsFlowLayout;
@property (weak, nonatomic) IBOutlet UILabel *mostRecentChannelsLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *mostRecentsCollectionViewHeightConstraint;

@property (nonatomic, strong) SectionNameComputationBlock sectionedByName;
@property (nonatomic, strong) NSSortDescriptor *sortSectionAsc;

@end

@implementation UIChannelsDiscoverViewController

-(instancetype) initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    
    if (self) {
//        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didLogin:) name:kLoginManagerDidLoginSucceeded object:nil];
//        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didLogout:) name:kLoginManagerDidLogoutSucceeded object:nil];
//        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didLostConnection:) name:kLoginManagerDidLostConnection object:nil];
//        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReconnect:) name:kLoginManagerDidReconnect object:nil];
//
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didAddChannel:) name:kChannelsServiceDidAddChannel object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didRemoveChannel:) name:kChannelsServiceDidRemoveChannel object:nil];
//
//        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didScrollToTop:) name:@"channelsServiceDidScrollToTop" object:nil];
        
        _channelsService = [ServicesManager sharedInstance].channelsService;
        _categories = [FilteredSortedSectionedArray new];
        _mostFollowedChannels = [FilteredSortedSectionedArray new];
        _mostRecentsChannels = [FilteredSortedSectionedArray new];
        
        _sectionedByName = ^NSString*(Channel *channel) {
            return @"CategorySection";
        };
        
        _sortSectionAsc = [NSSortDescriptor sortDescriptorWithKey:@"self" ascending:NO comparator:^NSComparisonResult(NSString* obj1, NSString* obj2) {
            return [obj1 compare:obj2 options:NSCaseInsensitiveSearch];
        }];
        
        NSPredicate *filter = [NSPredicate predicateWithBlock:^BOOL(Channel *channel, NSDictionary<NSString *,id> * bindings) {
            return YES;
        }];
        
        _categories.sectionNameFromObjectComputationBlock = _sectionedByName;
        _categories.globalFilteringPredicate = filter;
        _categories.sectionSortDescriptor = _sortSectionAsc;
        _categories.objectSortDescriptorForSection = @{@"__default__": @[[UITools noSortDescriptorInAscending:NO]]};
        
        // The padding will automatically resize the channel object
        _discoverByCategoryFlowLayout = [[UIChannelsDiscoverByCategoryFlowLayout alloc] initWithPadding:5];
        _mostFollowedFlowLayout = [[UIChannelsGridFlowLayout alloc] initWithPadding:5 scrollDirection:UICollectionViewScrollDirectionHorizontal];
        _mostRecentsFlowLayout = [[UIChannelsGridFlowLayout alloc] initWithPadding:5 scrollDirection:UICollectionViewScrollDirectionHorizontal];
        
        [_categories addObject:[Channel stringFromChannelCategory:ChannelCategoryGlobalNews]];
        [_categories addObject:[Channel stringFromChannelCategory:ChannelCategoryDesign]];
        [_categories addObject:[Channel stringFromChannelCategory:ChannelCategoryInnovation]];
        [_categories addObject:[Channel stringFromChannelCategory:ChannelCategorySocialMedia]];
        [_categories addObject:[Channel stringFromChannelCategory:ChannelCategoryScience]];
        [_categories addObject:[Channel stringFromChannelCategory:ChannelCategoryCulture]];
        [_categories addObject:[Channel stringFromChannelCategory:ChannelCategoryBusiness]];
        [_categories addObject:[Channel stringFromChannelCategory:ChannelCategoryHitech]];
        [_categories addObject:[Channel stringFromChannelCategory:ChannelCategoryMarketing]];
        [_categories addObject:[Channel stringFromChannelCategory:ChannelCategoryTravel]];
        [_categories reloadData];
        
        _mostFollowedChannels.sectionNameFromObjectComputationBlock = _sectionedByName;
        _mostFollowedChannels.globalFilteringPredicate = filter;
        _mostFollowedChannels.sectionSortDescriptor = _sortSectionAsc;
        _mostFollowedChannels.objectSortDescriptorForSection = @{@"__default__": @[[UITools sortDescriptorByInt:@"subscribersCount" ascending:NO]]};
        
        _mostRecentsChannels.sectionNameFromObjectComputationBlock = _sectionedByName;
        _mostRecentsChannels.globalFilteringPredicate = filter;
        _mostRecentsChannels.sectionSortDescriptor = _sortSectionAsc;
        _mostRecentsChannels.objectSortDescriptorForSection = @{@"__default__": @[[UITools sortDescriptorByCreationDate]]};
        
        [self refreshMostFollowedListWithBlock:nil];
        [self refreshMostRecentsListWithBlock:nil];
    }
    
    return self;
}

-(void) dealloc {
//    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidLoginSucceeded object:nil];
//    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidLogoutSucceeded object:nil];
//    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidLostConnection object:nil];
//    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidReconnect object:nil];
//
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kChannelsServiceDidAddChannel object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kChannelsServiceDidRemoveChannel object:nil];
//
//    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"channelsServiceDidScrollToTop" object:nil];
    
    @synchronized (_categories) {
        [_categories removeAllObjects];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    // Setup categories collection view
    _categoriesCollectionView.dataSource = self;
    _categoriesCollectionView.delegate = self;
    _categoriesCollectionView.collectionViewLayout = _discoverByCategoryFlowLayout;
    _categoriesCollectionView.tag = kTagByCategoriesCollectionView;
    
    // Setup most followed collection view
    _mostFollowedChannelsCollectionView.dataSource = self;
    _mostFollowedChannelsCollectionView.delegate = self;
    _mostFollowedChannelsCollectionView.collectionViewLayout = _mostFollowedFlowLayout;
    _mostFollowedChannelsCollectionView.tag = kTagMostFollowedCollectionView;
    _mostFollowedCollectionViewHeightConstraint.constant = _mostFollowedFlowLayout.itemSize.height;
    [_mostFollowedChannelsCollectionView registerNib:[UINib nibWithNibName:@"UIChannelCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:kCollectionViewCellReusableKey];
    [_mostFollowedChannelsCollectionView registerNib:[UINib nibWithNibName:@"UIChannelViewMoreCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:kCollectionViewMoreCellReusableKey];
    
    // Setup most recents collection view
    _mostRecentsChannelsCollectionView.dataSource = self;
    _mostRecentsChannelsCollectionView.delegate = self;
    _mostRecentsChannelsCollectionView.collectionViewLayout = _mostRecentsFlowLayout;
    _mostRecentsChannelsCollectionView.tag = kTagMostRecentsCollectionView;
    _mostRecentsCollectionViewHeightConstraint.constant = _mostRecentsFlowLayout.itemSize.height;
    [_mostRecentsChannelsCollectionView registerNib:[UINib nibWithNibName:@"UIChannelCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:kCollectionViewCellReusableKey];
    [_mostRecentsChannelsCollectionView registerNib:[UINib nibWithNibName:@"UIChannelViewMoreCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:kCollectionViewMoreCellReusableKey];
    
    // Set color, font and text for label
    [UITools applyCustomFontTo:_exploreChannelsByCategoriesLabel];
    [_exploreChannelsByCategoriesLabel setTextColor:[UITools defaultTintColor]];
    [_exploreChannelsByCategoriesLabel setText:NSLocalizedString(@"Explore channels by categories", nil)];
    
    // Set color, font and text for label
    [UITools applyCustomFontTo:_mostFollowedChannelsLabel];
    [_mostFollowedChannelsLabel setTextColor:[UITools defaultTintColor]];
    [_mostFollowedChannelsLabel setText:NSLocalizedString(@"Most followed channels", nil)];
    
    // Set color, font and text for label
    [UITools applyCustomFontTo:_mostRecentChannelsLabel];
    [_mostRecentChannelsLabel setTextColor:[UITools defaultTintColor]];
    [_mostRecentChannelsLabel setText:NSLocalizedString(@"Most recent channels", nil)];

    _scrollView.contentInset = UIEdgeInsetsMake(0, 0, 50, 0);
    
    // Pull to refresh
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(refreshMostFollowedAndRecentsList:) forControlEvents:UIControlEventValueChanged];
    _scrollView.refreshControl = refreshControl;
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    int y = CGRectGetMaxY(((UIView*)[_contentView.subviews lastObject]).frame);
    _scrollView.contentSize = CGSizeMake(CGRectGetWidth(_scrollView.frame), y);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Collection view delegates

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    if (collectionView.tag == kTagByCategoriesCollectionView && _categories && [_categories sections])
        return [[_categories sections] count];
    
    if (collectionView.tag == kTagMostFollowedCollectionView && _mostFollowedChannels && [_mostFollowedChannels sections])
        return [[_mostFollowedChannels sections] count];
    
    if (collectionView.tag == kTagMostRecentsCollectionView && _mostRecentsChannels && [_mostRecentsChannels sections])
        return [[_mostRecentsChannels sections] count];
    
    return 0;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if (collectionView.tag == kTagByCategoriesCollectionView && _categories)
        return [[_categories objectsInSection:[[_categories sections] objectAtIndex:section]] count];
    
    if (collectionView.tag == kTagMostFollowedCollectionView && _mostFollowedChannels) {
        NSInteger count = [[_mostFollowedChannels objectsInSection:[[_mostFollowedChannels sections] objectAtIndex:section]] count];
        return count == kNumberOfMostAndRecentItems ? count + 1 : count; // +1 for the More button
    }
    
    
    if (collectionView.tag == kTagMostRecentsCollectionView && _mostRecentsChannels) {
        NSInteger count = [[_mostRecentsChannels objectsInSection:[[_mostRecentsChannels sections] objectAtIndex:section]] count];
        return count == kNumberOfMostAndRecentItems ? count + 1 : count; // +1 for the More button
    }
    
    return 0;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == kNumberOfMostAndRecentItems && (collectionView.tag == kTagMostFollowedCollectionView || collectionView.tag == kTagMostRecentsCollectionView)) {
        UIChannelViewMoreCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kCollectionViewMoreCellReusableKey forIndexPath:indexPath];
        return cell;
    } else {
        if (collectionView.tag == kTagByCategoriesCollectionView) {
            NSString *key = [[_categories sections] objectAtIndex:indexPath.section];
            NSString *categoryString = [[_categories objectsInSection:key] objectAtIndex:indexPath.row];
            UIDiscoverByCategoryCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kCollectionViewDiscoverByCategoryReusableKey forIndexPath:indexPath];
            cell.category = [Channel channelCategoryFromString:categoryString];
            return cell;
        } else if (collectionView.tag == kTagMostFollowedCollectionView) {
            NSString *key = [[_mostFollowedChannels sections] objectAtIndex:indexPath.section];
            Channel *channel = [[_mostFollowedChannels objectsInSection:key] objectAtIndex:indexPath.row];
            UIChannelCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kCollectionViewCellReusableKey forIndexPath:indexPath];
            cell.discoverMode = YES;
            cell.channel = channel;
            return cell;
        } else if (collectionView.tag == kTagMostRecentsCollectionView) {
            NSString *key = [[_mostRecentsChannels sections] objectAtIndex:indexPath.section];
            Channel *channel = [[_mostRecentsChannels objectsInSection:key] objectAtIndex:indexPath.row];
            UIChannelCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kCollectionViewCellReusableKey forIndexPath:indexPath];
            cell.discoverMode = YES;
            cell.channel = channel;
            return cell;
        }
    }
    
    return nil;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row != kNumberOfMostAndRecentItems && (collectionView.tag == kTagMostFollowedCollectionView || collectionView.tag == kTagMostRecentsCollectionView)) {
        Channel *channel = nil;
        
        if (collectionView.tag == kTagMostFollowedCollectionView) {
            NSString *key = [[_mostFollowedChannels sections] objectAtIndex:indexPath.section];
            channel = [[_mostFollowedChannels objectsInSection:key] objectAtIndex:indexPath.row];
        } else {
            NSString *key = [[_mostRecentsChannels sections] objectAtIndex:indexPath.section];
            channel = [[_mostRecentsChannels objectsInSection:key] objectAtIndex:indexPath.row];
        }
        
        if (channel) {
            UIChannelsItemsViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"channelsItemsViewControllerID"];
            viewController.selectedChannel = channel;
            [self.navigationController pushViewController:viewController animated:YES];
        }
    } else {
        UIChannelsListViewController *channelsViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"channelsListViewControllerID"];
        
        if (collectionView.tag == kTagByCategoriesCollectionView) {
            NSString *key = [[_categories sections] objectAtIndex:indexPath.section];
            NSString *categoryString = [[_categories objectsInSection:key] objectAtIndex:indexPath.row];
            [channelsViewController loadChannelsForCategory:[Channel channelCategoryFromString:categoryString]];
        } else if (collectionView.tag == kTagMostFollowedCollectionView) {
            [channelsViewController loadMostFollowedChannels];
        } else if (collectionView.tag == kTagMostRecentsCollectionView) {
            [channelsViewController loadMostRecentChannels];
        }
        
        [self.navigationController pushViewController:channelsViewController animated:YES];
    }
}

#pragma mark - Refresh methods

-(void) refreshMostFollowedListWithBlock:(void(^)()) block {
    
    [_channelsService fetchMostFollowedChannels:kNumberOfMostAndRecentItems ignoreSubscribed:YES completionHandler:^(NSArray<Channel *> *channels, NSError *error) {
        if (!error && channels) {
            [_mostFollowedChannels removeAllObjects];
            
            for (Channel *channel in channels) {
                [_mostFollowedChannels addObject:channel];
            }
            [_mostFollowedChannels reloadData];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [_mostFollowedChannelsCollectionView reloadData];
            });
            
            if (block)
                block();
        }
    }];
}

-(void) refreshMostRecentsListWithBlock:(void(^)()) block {
    [_channelsService fetchMostRecentChannels:kNumberOfMostAndRecentItems ignoreSubscribed:YES completionHandler:^(NSArray<Channel *> *channels, NSError *error) {
        if (!error && channels) {
            [_mostRecentsChannels removeAllObjects];
            
            for (Channel *channel in channels) {
                [_mostRecentsChannels addObject:channel];
            }
            [_mostRecentsChannels reloadData];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [_mostRecentsChannelsCollectionView reloadData];
            });
            
            if (block)
                block();
        }
    }];
}

-(void) refreshMostFollowedAndRecentsList:(UIRefreshControl *) refreshControl {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [NSThread sleepForTimeInterval:1];
        
        [self refreshMostRecentsListWithBlock:^{
            [self refreshMostFollowedListWithBlock:^{
                dispatch_async(dispatch_get_main_queue(), ^{
                    [refreshControl endRefreshing];
                });
            }];
        }];
    });
}

#pragma mark - Notifications channels management

-(void) didAddChannel:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didAddChannel:notification];
        });
        return;
    }
    
    if (!notification.object)
        return;
    
    Channel *channel = (Channel *)[notification.object objectForKey:kChannelKey];

    if (channel) {
        Channel *followedChannel = [self getFollowedChannelById:channel.id];
        Channel *recentChannel = [self getRecentChannelById:channel.id];
        
        if (followedChannel) {
            [_mostFollowedChannels removeObject:followedChannel];
            [_mostFollowedChannels addObject:channel];
            [_mostFollowedChannels reloadData];
            
            if ([self isViewLoaded])
                [_mostFollowedChannelsCollectionView reloadData];
        }
        
        if (recentChannel) {
            [_mostRecentsChannels removeObject:recentChannel];
            [_mostRecentsChannels addObject:channel];
            [_mostRecentsChannels reloadData];
            
            if ([self isViewLoaded])
                [_mostRecentsChannelsCollectionView reloadData];
        }
    }
}

-(void) didRemoveChannel:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didRemoveChannel:notification];
        });
        return;
    }
    
    if (!notification.object)
        return;
    
    Channel *channel = (Channel *)[notification.object objectForKey:kChannelKey];

    if (channel) {
        if ([_mostFollowedChannels containsObject:channel]) {
            [_mostFollowedChannels reloadData];
            if ([self isViewLoaded])
                [_mostFollowedChannelsCollectionView reloadData];
        }
        
        if ([_mostRecentsChannels containsObject:channel]) {
            [_mostRecentsChannels reloadData];
            if ([self isViewLoaded])
                [_mostRecentsChannelsCollectionView reloadData];
        }
    }
}

-(Channel *) getFollowedChannelById:(NSString *) channelId {
    for (Channel *channel in _mostFollowedChannels) {
        if ([channel.id isEqualToString:channelId])
            return channel;
    }
    
    return nil;
}

-(Channel *) getRecentChannelById:(NSString *) channelId {
    for (Channel *channel in _mostRecentsChannels) {
        if ([channel.id isEqualToString:channelId])
            return channel;
    }
    
    return nil;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
