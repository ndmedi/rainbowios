//
//  UIChannelsItemsViewController.m
//  Rainbow-iOS
//
//  Created by Le Trong Nghia Huynh on 04/02/2019.
//  Copyright © 2019 ALE International. All rights reserved.
//

#import "UIChannelsItemsViewController.h"
#import "FilteredSortedSectionedArray.h"
#import "UIChannelItemTableViewCell.h"
#import <DZNEmptyDataSet/DZNEmptyDataSet.h>
#import "UITools.h"
#import <Rainbow/ServicesManager.h>
#import "UITableView+LoadingFooter.h"
#import "ChannelItemCellProtocol.h"
#import "UIChannelsPublishModifyItemViewController.h"
#import "UIChannelsCreateModifyViewController.h"

#define kTableViewReusableKey @"channelItemCellID"
#define kChannelItemsSectionKey @"ChannelItems"
#define dznButtonDisabledColor 0xB2B2B2FF

@interface UIChannelsItemsViewController () <DZNEmptyDataSetDelegate, DZNEmptyDataSetSource, UITableViewDelegate, UITableViewDataSource, ChannelItemCellProtocol>

@property (nonatomic, strong) ChannelsService *channelsService;

@property (nonatomic, strong) FilteredSortedSectionedArray <ChannelItem *> *channelsItems;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic, strong) NSPredicate *globalFilterAllMessages;
@property (nonatomic, strong) NSSortDescriptor *sortSectionAsc;
@property (nonatomic, strong) SectionNameComputationBlock sectionedByName;

// Avoid reloading to search for older messages if we already reach the oldest
@property (nonatomic) BOOL hasOlderMessages;

@property (nonatomic, strong) UIAlertController *actionSheet;

@end

@implementation UIChannelsItemsViewController

-(instancetype) initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];

    if (self) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didLogin:) name:kLoginManagerDidLoginSucceeded object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didLogout:) name:kLoginManagerDidLogoutSucceeded object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didLostConnection:) name:kLoginManagerDidLostConnection object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReconnect:) name:kLoginManagerDidReconnect object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didEndLoadingChannelsItemsFromCache:) name:kChannelsServiceDidEndLoadingChannelsItemsFromCache object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didEndFetchingChannelsItemFromServer:) name:kChannelsServiceDidEndFetchingChannelsItemsFromServer object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReceiveItem:) name:kChannelsServiceDidReceiveItem object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didRetractItem:) name:kChannelsServiceDidRetractItem object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didUpdateItem:) name:kChannelsServiceDidUpdateItem object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didUpdateChannel:) name:kChannelsServiceDidUpdateChannel object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didScrollToTop:) name:@"channelsServiceDidScrollToTop" object:nil];
        
        _channelsService = [ServicesManager sharedInstance].channelsService;
        _channelsItems = [FilteredSortedSectionedArray new];
        
        _globalFilterAllMessages = [NSPredicate predicateWithBlock:^BOOL(ChannelItem *channelItem, NSDictionary<NSString *,id> * bindings) {
            return YES;
        }];
        
        _sortSectionAsc = [NSSortDescriptor sortDescriptorWithKey:@"self" ascending:NO comparator:^NSComparisonResult(NSString* obj1, NSString* obj2) {
            return [obj1 compare:obj2 options:NSCaseInsensitiveSearch];
        }];
        
        _sectionedByName = ^NSString*(ChannelItem *item) {
            return kChannelItemsSectionKey;
        };
        
        // Default filters/sorter/.. values
        _channelsItems.sectionNameFromObjectComputationBlock = _sectionedByName;
        _channelsItems.globalFilteringPredicate = _globalFilterAllMessages;
        _channelsItems.sectionSortDescriptor = _sortSectionAsc;
        
        // __default__ has a special meaning : any other sections not found in the dictionary
        _channelsItems.objectSortDescriptorForSection = @{@"__default__": @[[UITools sortDescriptorByDate]]};
    }
    
    return self;
}

-(void) dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidLoginSucceeded object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidLogoutSucceeded object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidLostConnection object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidReconnect object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kChannelsServiceDidEndLoadingChannelsFromCache object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kChannelsServiceDidEndLoadingChannelsItemsFromCache object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kChannelsServiceDidEndFetchingChannelsFromServer object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kChannelsServiceDidEndFetchingChannelsItemsFromServer object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kChannelsServiceDidReceiveItem object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kChannelsServiceDidRetractItem object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kChannelsServiceDidUpdateItem object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kChannelsServiceDidUpdateChannel object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"channelsServiceDidScrollToTop" object:nil];
    
    @synchronized(_channelsItems) {
        [_channelsItems removeAllObjects];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self updateBarButtonsVisibility];
    
    self.definesPresentationContext = YES;
    self.extendedLayoutIncludesOpaqueBars = YES;
    
    // table view
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 74;
    self.tableView.emptyDataSetSource = self;
    self.tableView.emptyDataSetDelegate = self;
    self.tableView.allowsMultipleSelectionDuringEditing = YES;
    
    self.tableView.dataSource = self;
    
    
    self.tableView.tableFooterView = [UIView new];
    
    self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 50, 0);
    
    for (ChannelItem *item in [_channelsService.channelsItems copy]) {
        [self insertChannelItem:item];
    }
    [_channelsItems reloadData];
    
    _hasOlderMessages = YES;
    [self loadMoreItems:10 atInit:YES];
    
    // Pull to refresh
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(refreshTableView:) forControlEvents:UIControlEventValueChanged];
    _tableView.refreshControl = refreshControl;
}

// Avoid triggering scrollViewDidScroll when loading the controller
- (void)viewDidLayoutSubviews {
    self.tableView.delegate = self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Login/logout notification

-(void) didLogin:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didLogin:notification];
        });
        return;
    }
    
    if ([self isViewLoaded])
        [self.tableView reloadData];
    
    [self.tableView showLoadingFooter];
}

-(void) didLogout:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didLogout:notification];
        });
        return;
    }
    
    @synchronized (_channelsItems) {
        [_channelsItems removeAllObjects];
        [_channelsItems reloadData];
    }
    
    [self.tableView reloadData];
    
    self.tabBarController.selectedIndex = 0;
    [self.navigationController popToRootViewControllerAnimated:NO];
}

-(void) didLostConnection:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didLostConnection:notification];
        });
        return;
    }
    
    if ([self isViewLoaded]){
        [self.tableView reloadData];
    }
}

-(void) didReconnect:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didReconnect:notification];
        });
        return;
    }
        if ([self isViewLoaded])
            [self.tableView reloadData];
    
}

-(void) didEndLoadingChannelsItemsFromCache:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didEndLoadingChannelsItemsFromCache:notification];
        });
        return;
    }
    
    for (ChannelItem *item in [_channelsService.channelsItems copy]) {
        [self insertChannelItem:item];
    }
    [_channelsItems reloadData];
    
    if ([self isViewLoaded])
        [self.tableView reloadData];
    
}

-(void) didEndFetchingChannelsItemFromServer:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didEndFetchingChannelsItemFromServer:notification];
        });
        return;
    }
    
    [_channelsItems removeAllObjects];
    
    for (ChannelItem *item in [_channelsService.channelsItems copy]) {
        [self insertChannelItem:item];
    }
    [_channelsItems reloadData];
    
    if ([self.tableView isLoadingFooterShowing])
        [self.tableView hideLoadingFooter];
    
    if ([self isViewLoaded])
        [self.tableView reloadData];
    
    // If the items are refreshed (for example if you subscribe to see images), we need to load more images if they are too old
    if (_selectedChannel)
        [self loadMoreItems:10 atInit:YES];
}

-(void) didReceiveItem:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didReceiveItem:notification];
        });
        return;
    }
    
    if (notification.object && [notification.object isKindOfClass:[ChannelItem class]]) {
        ChannelItem *item = (ChannelItem *)notification.object;
        
        if ([self insertChannelItem:item]) {
            [_channelsItems reloadData];
            
            if ([self isViewLoaded]) {
                if ([_channelsItems count] > 1) {
                    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
                    [_tableView insertRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationTop];
                } else {
                    [_tableView reloadData];
                }
            }
        }
    }
}

-(void) didRetractItem:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didRetractItem:notification];
        });
        return;
    }
    
    if (notification.object && [notification.object isKindOfClass:[ChannelItem class]]) {
        ChannelItem *item = (ChannelItem *)notification.object;
        
        // Store the row index before removing
        NSInteger rowIndex = -1;
        NSArray *items = [_channelsItems objectsInSection:kChannelItemsSectionKey];
        for (int i = 0; i < [items count] && rowIndex == -1; i++) {
            if ([((ChannelItem *)[items objectAtIndex:i]).itemId isEqualToString:item.itemId])
                rowIndex = i;
        }
        
        if ([self removeChannelItem:item]) {
            [_channelsItems reloadData];
            
            if ([self isViewLoaded] && rowIndex != -1) {
                if ([_channelsItems count] == 0) {
                    [_tableView reloadData];
                } else {
                    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:rowIndex inSection:0];
                    [_tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
                }
            }
        }
    }
}

-(void) didUpdateItem:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didUpdateItem:notification];
        });
        return;
    }
    
    NSDictionary *userInfo = notification.object;
    NSArray<NSString *> *changedKeys = [userInfo objectForKey:kChangedKeys];
    
    if (!changedKeys || [changedKeys count] == 0)
        return;
    
    // In these cases, we need to reload the height of the cell
    if (![changedKeys containsObject:@"images"] && ![changedKeys containsObject:@"video"] && ![changedKeys containsObject:@"message"] && ![changedKeys containsObject:@"attachments"])
        return;
    
    ChannelItem *channelItem = [userInfo objectForKey:kChannelItemKey];
    
    if (channelItem) {
        NSInteger rowIndex = -1;
        NSArray *items = [_channelsItems objectsInSection:kChannelItemsSectionKey];
        for (int i = 0; i < [items count] && rowIndex == -1; i++) {
            if ([((ChannelItem *)[items objectAtIndex:i]).itemId isEqualToString:channelItem.itemId])
                rowIndex = i;
        }
        
        [_channelsItems reloadData];
        
        if ([self isViewLoaded] && rowIndex != -1) {
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:rowIndex inSection:0];
            [_tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        }
    }
}

-(void) didScrollToTop:(NSNotification *) notification {
    if (notification.object && [notification.object isKindOfClass:[NSString class]]) {
        NSString *controllerCurrentlyDisplayed = (NSString *)notification.object;
        
        if ([controllerCurrentlyDisplayed isEqualToString:@"UIChannelsItemsViewController"] && [_channelsItems.objects count] > 0)
            [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
}

-(BOOL) insertChannelItem:(ChannelItem *) channelItem {
    if (_selectedChannel && ![_selectedChannel.id isEqualToString:channelItem.channelId])
        return NO;
    
    if (![_channelsItems containsObject:channelItem]) {
        [_channelsItems addObject:channelItem];
        return YES;
    } else {
        return NO;
    }
}

-(BOOL) removeChannelItem:(ChannelItem *) channelItem {
    if ([_channelsItems containsObject:channelItem]) {
        [_channelsItems removeObject:channelItem];
        return YES;
    } else {
        return NO;
    }
}

#pragma mark - Update channel notification

-(void) didUpdateChannel:(NSNotification *) notification {
    if (notification.object && [notification.object isKindOfClass:[NSDictionary class]]) {
        NSArray *changedKeys = [((NSDictionary *)notification.object) objectForKey:@"changedKeys"];
        if ([changedKeys count] > 0) {
            if ([changedKeys containsObject:@"name"]) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self setTitle:_selectedChannel.name];
                });
            }
            
            if ([changedKeys containsObject:@"type"]) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self updateBarButtonsVisibility];
                });
            }
        }
    }
}

#pragma mark - Segues

-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"showChannelItemsSegueID"]) {
        UIViewController *destViewController = [segue destinationViewController];
        
        if ([destViewController isKindOfClass:[UIChannelsItemsViewController class]] && [sender isKindOfClass:[UIChannelItemTableViewCell class]]) {
            UIChannelsItemsViewController *itemsViewController = (UIChannelsItemsViewController *)destViewController;
            UIChannelItemTableViewCell *cell = (UIChannelItemTableViewCell *)sender;
            itemsViewController.selectedChannel = [_channelsService getChannel:cell.channelItem.channelId];
        }
    } else if ([[segue identifier] isEqualToString:@"publishModifyChannelItemSegueID"]) {
        UIViewController *destViewController = [segue destinationViewController];
        
        if ([destViewController isKindOfClass:[UIChannelsPublishModifyItemViewController class]]) {
            UIChannelsPublishModifyItemViewController *controller = (UIChannelsPublishModifyItemViewController *)destViewController;
            controller.channelService = _channelsService;
            
            if ([sender isKindOfClass:[Channel class]]) // Publishing in the channel
                controller.channel = sender;
            else if ([sender isKindOfClass:[ChannelItem class]]) // Modifying the channelItem
                controller.channelItem = sender;
        }
    } else if ([[segue identifier] isEqualToString:@"modifyChannelSegueID"]) {
        UIViewController *destViewController = [segue destinationViewController];
        
        if ([destViewController isKindOfClass:[UIChannelsCreateModifyViewController class]] && [sender isKindOfClass:[Channel class]]) {
            UIChannelsCreateModifyViewController *controller = (UIChannelsCreateModifyViewController *)destViewController;
            controller.modifyingChannel = sender;
        }
    }
}

-(BOOL) shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender {
    if ([identifier isEqualToString:@"showChannelItemsSegueID"] && _selectedChannel)
        return NO;
    
    return YES;
}

#pragma mark - UIBarButtonItem clicked

-(void) createNewPostClicked {
    [self performSegueWithIdentifier:@"publishModifyChannelItemSegueID" sender:_selectedChannel];
}

-(void) moreMenuClicked {
    if (_actionSheet) {
        [_actionSheet dismissViewControllerAnimated:NO completion:nil];
        _actionSheet = nil;
    }
    
    _actionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    // Set the attributedTitle in bold
    NSAttributedString *attrString = [[NSAttributedString alloc] initWithString:_selectedChannel.name attributes:@{NSFontAttributeName: [UIFont boldSystemFontOfSize:30.0f]}];
    [_actionSheet setValue:attrString forKey:@"_attributedTitle"];
    
    // Modify channel
    UIAlertAction *modifyChannel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Modify this channel", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self performSegueWithIdentifier:@"modifyChannelSegueID" sender:_selectedChannel];
    }];
    [_actionSheet addAction:modifyChannel];
    
    // Delete channel with confirmation
    UIAlertAction *deleteChannel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Delete this channel", nil) style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        if (_actionSheet) {
            [_actionSheet dismissViewControllerAnimated:NO completion:nil];
            _actionSheet = nil;
        }
        
        _actionSheet = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Delete this channel", nil) message:NSLocalizedString(@"Are you sure you want to delete this channel?", nil) preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *confirmDelete = [UIAlertAction actionWithTitle:NSLocalizedString(@"Delete", nil) style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
            [_channelsService deleteChannel:_selectedChannel.id completionHandler:^(NSError *error) {
                if (!error) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self.navigationController popViewControllerAnimated:YES];
                    });
                }
            }];
        }];
        [_actionSheet addAction:confirmDelete];
        
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style:UIAlertActionStyleCancel handler:nil];
        [_actionSheet addAction:cancel];
        
        // show the menu.
        [_actionSheet.view setTintColor:[UITools defaultTintColor]];
        [self presentViewController:_actionSheet animated:YES completion:nil];
    }];
    [_actionSheet addAction:deleteChannel];
    
    // Cancel action
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style:UIAlertActionStyleCancel handler:nil];
    [_actionSheet addAction:cancel];
    
    // show the menu.
    [_actionSheet.view setTintColor:[UITools defaultTintColor]];
    [self presentViewController:_actionSheet animated:YES completion:nil];
}

-(void) updateBarButtonsVisibility {
    if (_selectedChannel) {
        [self setTitle:_selectedChannel.name];
        
        if (_selectedChannel.userType == ChannelUserTypeOwner) {
            // As a owner, the user can create a new post and modify/delete the channel
            UIBarButtonItem *buttonCreateNewPost = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Add"] style:UIBarButtonItemStyleDone target:self action:@selector(createNewPostClicked)];
            UIBarButtonItem *buttonMoreMenu = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"menu_other_vertical"] style:UIBarButtonItemStyleDone target:self action:@selector(moreMenuClicked)];
            self.navigationItem.rightBarButtonItems = @[buttonMoreMenu, buttonCreateNewPost];
        } else if (_selectedChannel.userType == ChannelUserTypePublisher) {
            // As a publisher, the user can create a new post
            self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Add"] style:UIBarButtonItemStyleDone target:self action:@selector(createNewPostClicked)];
        } else {
            self.navigationItem.rightBarButtonItems = nil;
        }
    }
}

#pragma mark - DZNEmptyDataSet
- (UIImage *)imageForEmptyDataSet:(UIScrollView *)scrollView {
    return [UIImage imageNamed:@"ChannelsIcon"];
}

- (UIColor *)imageTintColorForEmptyDataSet:(UIScrollView *)scrollView {
    return [UITools defaultTintColor];
}

- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView {
    return nil;
}

- (NSAttributedString *)descriptionForEmptyDataSet:(UIScrollView *)scrollView {
    NSString *text = NSLocalizedString(@"Share information with channels. Invite users and broadcast messages.", nil);
    if (_selectedChannel)
        text = NSLocalizedString(@"No post in this channel yet", nil);
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont fontWithName:[UITools defaultFontName] size:16.0f], NSForegroundColorAttributeName: [UIColor darkGrayColor]};
    
    return [[NSAttributedString alloc] initWithString:text attributes:attributes];
}

- (NSAttributedString *)buttonTitleForEmptyDataSet:(UIScrollView *)scrollView forState:(UIControlState)state {
    if (_selectedChannel && (_selectedChannel.userType == ChannelUserTypeOwner || _selectedChannel.userType == ChannelUserTypePublisher)) {
        NSDictionary *attributes = @{NSFontAttributeName: [UIFont fontWithName:[UITools defaultFontName] size:16.0f], NSForegroundColorAttributeName : [UIColor whiteColor]};
        return [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Add a post", nil) attributes:attributes];
    }
    
    return nil;
}

-(UIImage *) buttonBackgroundImageForEmptyDataSet:(UIScrollView *)scrollView forState:(UIControlState)state {
    // Change button color for guest user
    if ([ServicesManager sharedInstance].myUser.isGuest) {
        return [UITools imageWithColor:[UITools colorFromHexa:dznButtonDisabledColor] size:CGSizeMake(1, 40)];
    } else {
        return [UITools imageWithColor:[UITools defaultTintColor] size:CGSizeMake(1, 40)];
    }
}

- (UIColor *)backgroundColorForEmptyDataSet:(UIScrollView *)scrollView {
    return [UIColor groupTableViewBackgroundColor];
}

- (CGFloat)verticalOffsetForEmptyDataSet:(UIScrollView *)scrollView {
    CGFloat offset = CGRectGetHeight([UIApplication sharedApplication].statusBarFrame);
    offset += CGRectGetHeight(self.navigationController.navigationBar.frame);
    if (!_selectedChannel)
        offset -= 60;
    return -offset;
}

- (CGFloat)spaceHeightForEmptyDataSet:(UIScrollView *)scrollView {
    return 0.0f;
}

- (BOOL)emptyDataSetShouldDisplay:(UIScrollView *)scrollView {
    return ![self.tableView isLoadingFooterShowing];
}

- (BOOL)emptyDataSetShouldAllowTouch:(UIScrollView *)scrollView {
    // Disable interaction if user is guest
    if ([ServicesManager sharedInstance].myUser.isGuest)
        return NO;
    else
        return  YES;
}

- (BOOL)emptyDataSetShouldAllowScroll:(UIScrollView *)scrollView {
    return NO;
}

- (void)emptyDataSet:(UIScrollView *)scrollView didTapButton:(UIButton *)button {
    [self createNewPostClicked];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (_channelsItems && [[_channelsItems sections] count] != 0)
        return [[_channelsItems sections] count];
    
    return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (_channelsItems)
        return [_channelsItems count];
    
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UIChannelItemTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kTableViewReusableKey forIndexPath:indexPath];

    NSString *key = [[_channelsItems sections] objectAtIndex:indexPath.section];
    ChannelItem *channelItem = [[_channelsItems objectsInSection:key] objectAtIndex:indexPath.row];
    
    // This code reloads a row at a certain place
//    ChannelItemImage *itemImage = (ChannelItemImage *)channelItem.images.firstObject;
//    if (itemImage && !itemImage.image && ([channelItem.images count] == 1 || [channelItem.images count] == 3 || [channelItem.images count] == 4)) {
//        [[ServicesManager sharedInstance].channelsService loadItemImageWith:itemImage completionBlock:^(UIImage *image, NSString *rainbowId) {
//
//            if (image && rainbowId) {
//                dispatch_async(dispatch_get_main_queue(), ^{
//                    UIChannelItemTableViewCell *updateCell = (id)[_tableView cellForRowAtIndexPath:indexPath];
//                    if (updateCell) {
//                        [_tableView beginUpdates];
//                        [_tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
//                        [_tableView endUpdates];
//                    }
//                });
//            }
//        }];
//    }
    
    
    cell.channelsService = _channelsService;
    cell.delegate = self;
    if (_selectedChannel)
        cell.selectedChannel = _selectedChannel;
    cell.channelItem = channelItem;

    return cell;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (_hasOlderMessages) {
        BOOL contentLarger = scrollView.contentSize.height > scrollView.frame.size.height;
        CGFloat viewableHeight = contentLarger ? scrollView.frame.size.height : scrollView.contentSize.height;
        BOOL atBottom = scrollView.contentOffset.y >= scrollView.contentSize.height - viewableHeight + 30;
        
        if (atBottom)
            [self loadMoreItems:10 atInit:NO];
    }
    
    // If the first visible cell is enough scrolled, acknowledge it
    UIChannelItemTableViewCell *firstVisibleCell = [_tableView visibleCells].firstObject;
    if (firstVisibleCell.channelItem.isNew) {
        CGRect rectOfCellInSuperview = [_tableView convertRect:firstVisibleCell.frame toView:_tableView.superview];

        // The superview when we open the channel seems to not be the same.. so remove the difference
        CGFloat differenceWhenOpenInChannel = _selectedChannel ? 87.f : 0.f;

        // The rectOfCellInSuperview.origin.y is the position of the top of the cell displayed on the screen. If it less than -60 (< -60), it means there is 60 px "above" (out of) the screen
        if (rectOfCellInSuperview.origin.y - differenceWhenOpenInChannel < -60)
            [_channelsService markItemAsRead:firstVisibleCell.channelItem];
    }
}

-(void) loadMoreItems:(int) count atInit:(BOOL) atInit {
    NSArray *objects = [_channelsItems objectsInSection:kChannelItemsSectionKey];
    
    if (atInit && [objects count] >= count)
        return;

    if (![self.tableView isLoadingFooterShowing]) {
        OTCLog(@"loadMoreItems with count = %@ (atInit = %@)", @(count), atInit ? @"YES": @"NO");
        
        [self.tableView showLoadingFooter];
        
        // Get the date of the last channel item
        NSDate *beforeDate = nil;
        if ([objects count] > 0) {
            ChannelItem *lastItem = (ChannelItem *)[objects objectAtIndex:[objects count] - 1];
            beforeDate = lastItem.date;
        }
        
        // If it is at init, just fetch the correct number to have the 'count' latest items
        if (atInit)
            count = count - (int)[objects count];
        
        
        void (^endRequestBlock)(NSArray *, NSError *) = ^void(NSArray *items, NSError *error) {
            [NSThread sleepForTimeInterval:1]; // This is purely for better UX
            
            // Hide the loading footer in any case
            [self.tableView hideLoadingFooter];
            
            if (!error && [items count] > 0) {
                int initialIndex = (int)_channelsItems.count;
                // Insert the items fetched
                BOOL somethingToUpdate = NO;
                for (ChannelItem *item in items) {
                    if ([self insertChannelItem:item])
                        somethingToUpdate = YES;
                }
                
                if (somethingToUpdate) {
                    [_channelsItems reloadData];
                    
                    // Reload the view
                    if ([self isViewLoaded]) {
                        if (initialIndex == 0) {
                            dispatch_async(dispatch_get_main_queue(), ^{
                                [_tableView reloadData];
                            });
                        } else {
                            // Create the list of indexPaths to insert
                            int finalIndex = (int)_channelsItems.count;
                            __block NSMutableArray *indexPaths = [NSMutableArray new];
                            for (int i = initialIndex; i < finalIndex; i++) {
                                [indexPaths addObject:[NSIndexPath indexPathForRow:i inSection:0]];
                            }
                            
                            // Insert these rows
                            dispatch_async(dispatch_get_main_queue(), ^{
                                [_tableView insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationBottom];
                            });
                        }
                    }
                } else {
                    _hasOlderMessages = NO;
                }
            } else {
                // This is only for DZN to be reloaded
                if ([items count] == 0) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [_tableView reloadData];
                    });
                }
                _hasOlderMessages = NO;
            }
        };

        if (_selectedChannel) {
            [_channelsService fetchItemsForChannel:_selectedChannel beforeDate:beforeDate number:count completionHandler:^(NSArray<ChannelItem *> *items, NSError *error) {
                endRequestBlock(items, error);
            }];
        } else {
            [_channelsService fetchLatestItemsListBeforeDate:beforeDate number:count completionHandler:^(NSArray<ChannelItem *> *items, NSError *error) {
                endRequestBlock(items, error);
            }];
        }
    }
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSString *key = [[_channelsItems sections] objectAtIndex:indexPath.section];
    ChannelItem *channelItem = [[_channelsItems objectsInSection:key] objectAtIndex:indexPath.row];

    if (channelItem.isNew)
        [_channelsService markItemAsRead:channelItem];
}

#pragma mark - Refresh control

-(void) refreshTableView:(UIRefreshControl *) refreshControl {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [NSThread sleepForTimeInterval:1];
        
        [_channelsService refreshLatestItemsListWithCompletionBlock:^(NSError *error) {
            if (!error)
                _hasOlderMessages = YES;
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [refreshControl endRefreshing];
            });
        }];
    });
}

#pragma mark - ChannelItemCellProtocol

- (void) publishMessageToChannelClicked:(Channel *)channel {
    [self performSegueWithIdentifier:@"publishModifyChannelItemSegueID" sender:channel];
}

- (void)modifyChannelItemClicked:(ChannelItem *)channelItem {
    [self performSegueWithIdentifier:@"publishModifyChannelItemSegueID" sender:channelItem];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
