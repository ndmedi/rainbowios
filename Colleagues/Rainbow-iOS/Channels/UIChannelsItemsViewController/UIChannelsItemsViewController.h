//
//  UIChannelsItemsViewController.h
//  Rainbow-iOS
//
//  Created by Le Trong Nghia Huynh on 04/02/2019.
//  Copyright © 2019 ALE International. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Rainbow/ChannelsService.h>

@interface UIChannelsItemsViewController : UIViewController

@property (nonatomic, strong) Channel *selectedChannel;

@end
