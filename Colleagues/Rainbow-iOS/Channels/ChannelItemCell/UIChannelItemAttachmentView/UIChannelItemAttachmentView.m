//
//  UIChannelItemAttachmentView.m
//  Rainbow-iOS
//
//  Created by Le Trong Nghia Huynh on 27/03/2019.
//  Copyright © 2019 ALE International. All rights reserved.
//

#import "UIChannelItemAttachmentView.h"
#import "UITools.h"
#import <Rainbow/NSString+FileSize.h>
#import "File+DefaultImage.h"
#import <Rainbow/ServicesManager.h>
#import "UIPreviewQuickLookController.h"

int const CHANNEL_ITEM_ATTACHMENT_HEIGHT = 35;

@interface UIChannelItemAttachmentView ()

@property (strong, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UIImageView *filetypeIcon;
@property (weak, nonatomic) IBOutlet UIImageView *rightIcon;
@property (weak, nonatomic) IBOutlet UILabel *filenameLabel;
@property (weak, nonatomic) IBOutlet UILabel *sizeLabel;

@property (strong, nonatomic) File *file;

@end

@implementation UIChannelItemAttachmentView

-(instancetype) initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    
    if (self) {
        [self customInit];
    }
    
    return self;
}

-(instancetype) initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    if (self) {
        [self customInit];
    }
    
    return self;
}

-(void) customInit {
    [[NSBundle mainBundle] loadNibNamed:@"UIChannelItemAttachmentView" owner:self options:nil];
    [self addSubview:self.contentView];
    self.contentView.frame = self.bounds;
    
    [UITools applyCustomFontTo:_filenameLabel];
    [UITools applyCustomFontTo:_sizeLabel];
    [_filenameLabel setTextColor:[UITools defaultTintColor]];
    [_filetypeIcon setTintColor:[UIColor blackColor]];
    [_rightIcon setTintColor:[UIColor blackColor]];
    _rightIcon.hidden = YES;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didUpdateFile:) name:kFileSharingServiceDidUpdateFile object:nil];
    
    self.layer.cornerRadius = 5;
    self.layer.masksToBounds = YES;
    
    [self setHeightConstraint];
}

-(void) setFile:(File *) file {
    if (_file != file) {
        _file = nil;
        _file = file;
    }
    
    _filenameLabel.text = _file.fileName;
    _sizeLabel.text = [NSString stringWithFormat:@"(%@)", [NSString formatFileSize:_file.size]];
    _filetypeIcon.image = _file.icon;
    [_filetypeIcon setTintColor:_file.tintColor];
}

#pragma mark - Setup view

-(void) setHeightConstraint {
    NSLayoutConstraint *constraint = [NSLayoutConstraint constraintWithItem:self
                                                                  attribute:NSLayoutAttributeHeight
                                                                  relatedBy:NSLayoutRelationEqual
                                                                     toItem:nil
                                                                  attribute:NSLayoutAttributeNotAnAttribute
                                                                 multiplier:1
                                                                   constant:35];
    [self addConstraint:constraint];
}

#pragma mark - Configure the correct mode

- (void) configureViewingMode {
    _rightIcon.hidden = NO;
    
    // Configure tap to open
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapDetected:)];
    singleTap.numberOfTapsRequired = 1;
    [self setUserInteractionEnabled:YES];
    [self addGestureRecognizer:singleTap];
}

-(void) configureEditingMode {
    _rightIcon.hidden = NO;
    _rightIcon.image = [UIImage imageNamed:@"ClearIcon"];
    
    // Configure tap on right icon to delete the attachment
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapDetectedToRemoveAttachment:)];
    singleTap.numberOfTapsRequired = 1;
    [_rightIcon setUserInteractionEnabled:YES];
    [_rightIcon addGestureRecognizer:singleTap];
}

#pragma mark - Tap gesture recognizer

-(void) tapDetected:(UITapGestureRecognizer *) recognizer {
    if (_file.data) {
        [self openPreviewControllerForAttachment:_file];
    } else {
        MBProgressHUD *downloadHud = [self showHUDWithMessage:NSLocalizedString(@"Downloading file ...",nil) mode:MBProgressHUDModeIndeterminate dismissCompletionBlock:nil];
        [downloadHud show:YES];
        
        [[ServicesManager sharedInstance].fileSharingService downloadDataForFile:_file withCompletionHandler:^(File *aFile, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [downloadHud hide:YES];
                if(!error){
                    [self openPreviewControllerForAttachment:aFile];
                } else {
                    MBProgressHUD *errorHud = [self showHUDWithMessage:NSLocalizedString(@"Error", nil) mode:MBProgressHUDModeCustomView dismissCompletionBlock:nil];
                    [errorHud showAnimated:YES whileExecutingBlock:^{
                        [NSThread sleepForTimeInterval:3];
                    } completionBlock:^{
                        
                    }];
                }
            });
        }];
    }
}

-(void) tapDetectedToRemoveAttachment:(UITapGestureRecognizer *) recognizer {
    if (_delegate)
        [_delegate removeAttachment:_file andView:self];
}

-(void) openPreviewControllerForAttachment:(File *) attachment {
    // Instanciate a QLPreview​Controller
    MBProgressHUD *openingHud = [self showHUDWithMessage:NSLocalizedString(@"Opening file ...",nil) mode:MBProgressHUDModeIndeterminate dismissCompletionBlock:nil];
    [openingHud show:YES];
    UIPreviewQuickLookController *previewController = [UIPreviewQuickLookController new];
    previewController.attachment = attachment;
    
    UIViewController *activeViewController = [[[UIApplication sharedApplication] keyWindow] rootViewController];
    [activeViewController presentViewController:previewController animated:YES completion:^{
        [openingHud hide:YES];
    }];
}

-(MBProgressHUD *) showHUDWithMessage:(NSString *) message mode:(MBProgressHUDMode) mode dismissCompletionBlock:(void (^)()) dismissCompletion {
    UIViewController *activeViewController = [[[UIApplication sharedApplication] keyWindow] rootViewController];
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:activeViewController.view animated:YES];
    hud.mode = mode;
    hud.removeFromSuperViewOnHide = YES;
    hud.userInteractionEnabled = NO;
    hud.labelText = message;
    [hud setLabelFont:[UIFont fontWithName:[UITools defaultFontName] size:14.0f]];
    if(mode == MBProgressHUDModeCustomView){
        hud.mode = MBProgressHUDModeCustomView;
        hud.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Checkmark"]];
        [hud showAnimated:YES whileExecutingBlock:^{sleep(1);} completionBlock:dismissCompletion];
    }
    return hud;
}

#pragma mark - File notif


// When the file descriptor is not in cache, we will receive update from this notification
-(void) didUpdateFile:(NSNotification *) notification {
    File *file = (File *) notification.object;
    if ([_file.url isEqual:file.url]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self setFile:file];
        });
    }
}

@end
