//
//  UIChannelItemAttachmentView.h
//  Rainbow-iOS
//
//  Created by Le Trong Nghia Huynh on 27/03/2019.
//  Copyright © 2019 ALE International. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Rainbow/File.h>
#import "ChannelItemAttachmentProtocol.h"

extern int const CHANNEL_ITEM_ATTACHMENT_HEIGHT;

@interface UIChannelItemAttachmentView : UIView

-(void) setFile:(File *) file;
- (void) configureViewingMode;
- (void) configureEditingMode;

@property (weak) id<ChannelItemAttachmentProtocol> delegate;

@end
