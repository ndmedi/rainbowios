//
//  ChannelImagePreviewProtocol.h
//  Rainbow-iOS
//
//  Created by Le Trong Nghia Huynh on 15/04/2019.
//  Copyright © 2019 ALE International. All rights reserved.
//

#import <Rainbow/ChannelsService.h>

@protocol ChannelImagePreviewProtocol <NSObject>

-(void) removeImagePreview:(id) object reloadUI:(BOOL) reloadUI;

@end
