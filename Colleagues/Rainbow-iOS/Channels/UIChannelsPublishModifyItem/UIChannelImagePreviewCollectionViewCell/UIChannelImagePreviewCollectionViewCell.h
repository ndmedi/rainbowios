//
//  UIChannelImagePreviewCollectionViewCell.h
//  Rainbow-iOS
//
//  Created by Le Trong Nghia Huynh on 15/04/2019.
//  Copyright © 2019 ALE International. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ChannelImagePreviewProtocol.h"

@interface UIChannelImagePreviewCollectionViewCell : UICollectionViewCell

@property (nonatomic, strong) id object;
@property (weak, nonatomic) IBOutlet UIImageView *previewImageView;
@property (weak, nonatomic) IBOutlet UIImageView *removeImageImageView;
@property (weak, nonatomic) IBOutlet UIView *videoRoundedImageView;
@property (weak) id<ChannelImagePreviewProtocol> delegate;

@end
