//
//  UIChannelsPublishModifyItemViewController.h
//  Rainbow-iOS
//
//  Created by Le Trong Nghia Huynh on 08/04/2019.
//  Copyright © 2019 ALE International. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Rainbow/ChannelsService.h>

@interface UIChannelsPublishModifyItemViewController : UIViewController

@property (nonatomic, strong) ChannelsService *channelService;

/**
 *  If the channel object is not nil, it means the user is publising a new post
 */
@property (nonatomic, strong) Channel *channel;

/**
 *  If the channelItem object is not nil, it means the user is modifying a post
 */
@property (nonatomic, strong) ChannelItem *channelItem;

@end
