//
//  UIChannelsAddMembersViewController.h
//  Rainbow-iOS
//
//  Created by Le Trong Nghia Huynh on 14/05/2019.
//  Copyright © 2019 ALE International. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIGenericListOfObjectWithTokenViewController.h"

@interface UIChannelsAddMembersViewController : UIGenericListOfObjectWithTokenViewController

@property (nonatomic, strong) NSString *channelCategoryString;
@property (nonatomic, strong) UIImage *avatar;
@property (nonatomic, strong) NSString *channelNameString;
@property (nonatomic, strong) NSString *channelDescriptionString;

@property (nonatomic) ChannelMode channelMode;

@end
