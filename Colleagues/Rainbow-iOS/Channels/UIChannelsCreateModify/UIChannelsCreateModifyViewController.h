//
//  UIChannelsCreateModifyViewController.h
//  Rainbow-iOS
//
//  Created by Le Trong Nghia Huynh on 07/05/2019.
//  Copyright © 2019 ALE International. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XLForm/XLForm.h>
#import <Rainbow/ChannelsService.h>

@interface UIChannelsCreateModifyViewController : XLFormViewController

@property (nonatomic, strong) NSString *channelCategoryString;
@property (nonatomic, strong) UIImage *avatar;
@property (nonatomic, strong) NSString *channelNameString;
@property (nonatomic, strong) NSString *channelDescriptionString;

@property (nonatomic, assign) int indexPage;

@property (nonatomic, strong) Channel *modifyingChannel;

@end
