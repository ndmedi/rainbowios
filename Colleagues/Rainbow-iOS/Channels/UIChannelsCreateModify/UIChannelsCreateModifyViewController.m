//
//  UIChannelsCreateModifyViewController.m
//  Rainbow-iOS
//
//  Created by Le Trong Nghia Huynh on 07/05/2019.
//  Copyright © 2019 ALE International. All rights reserved.
//

#import "UIChannelsCreateModifyViewController.h"
#import "UITools.h"
#import "LargePhotoCell.h"
#import "Channel+Rainbow.h"
#import "UIChannelsAddMembersViewController.h"

#define kName @"name"
#define kDescription @"description"
#define kAvatar @"avatar"
#define kCategory @"category"

#define kAudienceCompany @"company"
#define kAudienceAll @"all"
#define kConfidentialityPrivate @"private"
#define kConfidentialityPublic @"public"
#define kConfidentialityClosed @"closed"
#define kPeopleInviteSomeMembers @"people_invite_some_members"
#define kPeopleInviteAllMembers @"people_invite_all_members"

@interface NSString (SpaceReplace)
-(NSString *) replaceSpaceWithNonBrSpace;
-(NSString *) replaceNonBrSpaceWithSpace;
@end

@implementation NSString (SpaceReplace)
-(NSString *) replaceSpaceWithNonBrSpace {
    return [self stringByReplacingOccurrencesOfString:@" " withString:@"\u00a0"];
}
-(NSString *) replaceNonBrSpaceWithSpace {
    return [self stringByReplacingOccurrencesOfString:@"\u00a0" withString:@" "];
}
@end

@interface UIChannelsCreateModifyViewController ()

@property (nonatomic, strong) XLFormRowDescriptor *photo;
@property (nonatomic, strong) XLFormRowDescriptor *category;
@property (nonatomic, strong) XLFormRowDescriptor *name;
@property (nonatomic, strong) XLFormRowDescriptor *channeldescription;

@property (nonatomic, strong) XLFormSectionDescriptor *sectionAudience;
@property (nonatomic, strong) XLFormSectionDescriptor *sectionConfidentiality;
@property (nonatomic, strong) XLFormSectionDescriptor *sectionPeopleToSubscribe;


@end

@implementation UIChannelsCreateModifyViewController

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        _indexPage = 0;
        self.hidesBottomBarWhenPushed = YES;
    }
    return self;
}

- (void)dealloc {
    _photo = nil;
    _category = nil;
    _name = nil;
    _channeldescription = nil;
}

- (BOOL)shouldAutorotate {
    return NO;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return UIInterfaceOrientationPortrait;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    XLFormDescriptor *form = [XLFormDescriptor formDescriptor];
    form.endEditingTableViewOnScroll = YES;
    
    XLFormSectionDescriptor *fakeSectionLabel = [XLFormSectionDescriptor formSection];
    [form addFormSection:fakeSectionLabel];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(_modifyingChannel ? @"Ok" : @"Continue", nil) style:UIBarButtonItemStylePlain target:self action:@selector(continueButtonClicked)];
    
    if (_indexPage == 0) {
        self.title = _modifyingChannel ? NSLocalizedString(@"Modify this channel", nil) : NSLocalizedString(@"Create a channel", nil);
        self.navigationItem.rightBarButtonItem.enabled = NO;
        
        // Add our Custom LargePhotoCell Row to XLForm
        [[XLFormViewController cellClassesForRowDescriptorTypes] setObject:[LargePhotoCell class] forKey:kLargePhotoCellType];
        
        /*
         *  Fake section for a label
         */
        fakeSectionLabel.footerTitle = NSLocalizedString(@"A channel is a place where you can broadcast information about a particular subject. Users can subscribe or be invited to this channel.", nil);
        
        /*
         *  Section for the category + photo
         */
        XLFormSectionDescriptor *sectionCategoryPhoto = [XLFormSectionDescriptor formSection];
        
        // Category row
        _category = [self createRowType:XLFormRowDescriptorTypeSelectorPickerViewInline withTitle:NSLocalizedString(@"Select a category", nil) withDefaultValue:nil];
        _category.tag = kCategory;
        // Create the options
        NSMutableArray *selectorOptions = [NSMutableArray new];
        // Default value
        XLFormOptionsObject *objectDefault = [XLFormOptionsObject formOptionsObjectWithValue:@"globalnews" displayText:NSLocalizedString(@"globalnews", nil)];
        [selectorOptions addObject:objectDefault];
        // All other values
        [selectorOptions addObject:[XLFormOptionsObject formOptionsObjectWithValue:@"design" displayText:NSLocalizedString(@"design", nil)]];
        [selectorOptions addObject:[XLFormOptionsObject formOptionsObjectWithValue:@"innovation" displayText:NSLocalizedString(@"innovation", nil)]];
        [selectorOptions addObject:[XLFormOptionsObject formOptionsObjectWithValue:@"social" displayText:NSLocalizedString(@"social", nil)]];
        [selectorOptions addObject:[XLFormOptionsObject formOptionsObjectWithValue:@"science" displayText:NSLocalizedString(@"science", nil)]];
        [selectorOptions addObject:[XLFormOptionsObject formOptionsObjectWithValue:@"culture" displayText:NSLocalizedString(@"culture", nil)]];
        [selectorOptions addObject:[XLFormOptionsObject formOptionsObjectWithValue:@"business" displayText:NSLocalizedString(@"business", nil)]];
        [selectorOptions addObject:[XLFormOptionsObject formOptionsObjectWithValue:@"hitech" displayText:NSLocalizedString(@"hitech", nil)]];
        [selectorOptions addObject:[XLFormOptionsObject formOptionsObjectWithValue:@"marketing" displayText:NSLocalizedString(@"marketing", nil)]];
        [selectorOptions addObject:[XLFormOptionsObject formOptionsObjectWithValue:@"travel" displayText:NSLocalizedString(@"travel", nil)]];
        _category.selectorOptions = selectorOptions;
        _category.value = objectDefault;
        [sectionCategoryPhoto addFormRow:_category];
        
        // Photo row
        _photo = [self createRowType:kLargePhotoCellType withTitle:NSLocalizedString(@"Change avatar", nil) withDefaultValue:[Channel imageForCategory:ChannelCategoryGlobalNews]];
        _photo.tag = kAvatar;
        [_photo.cellConfig setObject:[UIColor whiteColor] forKey:@"imageView.tintColor"];
        [_photo.cellConfig setObject:[Channel colorForCategory:ChannelCategoryGlobalNews] forKey:@"imageView.backgroundColor"];
        [sectionCategoryPhoto addFormRow:_photo];
        
        /*
         *  Section for the name + description
         */
        XLFormSectionDescriptor *sectionNameDescription = [XLFormSectionDescriptor formSection];
        sectionNameDescription.title = NSLocalizedString(@"Name and description", nil);
        
        // Name row
        _name = [self createRowType:XLFormRowDescriptorTypeText withTitle:nil withDefaultValue:nil];
        _name.tag = kName;
        [_name.cellConfigAtConfigure setObject:NSLocalizedString(@"Give a name", nil) forKey:@"textField.placeholder"];
        [_name setRequired:YES];
        [sectionNameDescription addFormRow:_name];
        
        // Description row
        _channeldescription = [self createRowType:XLFormRowDescriptorTypeTextView withTitle:nil withDefaultValue:nil];
        _channeldescription.tag = kDescription;
        [_channeldescription.cellConfigAtConfigure setObject:NSLocalizedString(@"Describe the channel in a few words (optional)", nil) forKey:@"textView.placeholder"];
        [_channeldescription.cellConfigAtConfigure setObject:[UIFont fontWithName:[UITools defaultFontName] size:15] forKey:@"textView.font"];
        [sectionNameDescription addFormRow:_channeldescription];
        
        [form addFormSection:sectionCategoryPhoto];
        [form addFormSection:sectionNameDescription];
        
        if (_modifyingChannel) {
            // Set correct category
            [_category.selectorOptions enumerateObjectsUsingBlock:^(XLFormOptionsObject *obj, NSUInteger idx, BOOL * _Nonnull stop) {
                if ([obj.valueData isEqualToString:_modifyingChannel.category]) {
                    _category.value = obj;
                    *stop = YES;
                }
            }];
            
            if (_modifyingChannel.photoData) {
                _photo.value = [UIImage imageWithData:_modifyingChannel.photoData];
            } else {
                ChannelCategory category = [Channel channelCategoryFromString:_modifyingChannel.category];
                _photo.value = [Channel imageForCategory:category];
                [_photo.cellConfig setObject:[Channel colorForCategory:category] forKey:@"imageView.backgroundColor"];
            }
            
            _name.value = _modifyingChannel.name;
            _channeldescription.value = _modifyingChannel.channelDescription;
            
            // Add the audience part as readonly
            _sectionAudience = [XLFormSectionDescriptor formSection];
            _sectionAudience.title = NSLocalizedString(@"Audience", nil);
            [form addFormSection:_sectionAudience];
            
            NSString *audienceTitle = nil;
            if (_modifyingChannel.mode == ChannelModeAllPublic || _modifyingChannel.mode == ChannelModeAllPrivate) {
                audienceTitle = NSLocalizedString(@"Public", nil);
                _sectionAudience.footerTitle = NSLocalizedString(@"To all Rainbow users (members of my company and other companies)", nil);
            } else {
                audienceTitle = NSLocalizedString(@"My company", nil);
                _sectionAudience.footerTitle = NSLocalizedString(@"To members of my company only", nil);
            }
            
            XLFormRowDescriptor *audienceRow = [self createRowType:XLFormRowDescriptorTypeBooleanCheck withTitle:audienceTitle withDefaultValue:@NO];
            audienceRow.disabled = @YES;
            [_sectionAudience addFormRow:audienceRow];
            
            // Add the confidentiality part as readonly
            _sectionConfidentiality = [XLFormSectionDescriptor formSection];
            _sectionConfidentiality.title = NSLocalizedString(@"Confidentiality", nil);
            [form addFormSection:_sectionConfidentiality];
            
            NSString *confidentialityTitle = nil;
            if (_modifyingChannel.mode == ChannelModeAllPublic) {
                confidentialityTitle = NSLocalizedString(@"Open channel", nil);
                _sectionConfidentiality.footerTitle = NSLocalizedString(@"Rainbow users can view the channel and subscribe. You can also notify some users and invite them to subscribe.", nil);
            } else if (_modifyingChannel.mode == ChannelModeCompanyPublic) {
                confidentialityTitle = NSLocalizedString(@"Open channel", nil);
                _sectionConfidentiality.footerTitle = NSLocalizedString(@"Any member of your company can view the channel and subscribe. You can also notify some members and invite them to subscribe.", nil);
            } else if (_modifyingChannel.mode == ChannelModeCompanyClosed) {
                confidentialityTitle = NSLocalizedString(@"Restricted channel", nil);
                _sectionConfidentiality.footerTitle = NSLocalizedString(@"People you will choose will automatically be subscribed to this channel. They will not bbe able to unsubscribe.", nil);
            } else { // Private
                confidentialityTitle = NSLocalizedString(@"Private channel", nil);
                _sectionConfidentiality.footerTitle = NSLocalizedString(@"Channel can only be joined through an invitation. Only invited people can consult it, they can unsubscribe at any time.", nil);
            }
            
            XLFormRowDescriptor *confidentialityRow = [self createRowType:XLFormRowDescriptorTypeBooleanCheck withTitle:confidentialityTitle withDefaultValue:@NO];
            confidentialityRow.disabled = @YES;
            [_sectionConfidentiality addFormRow:confidentialityRow];
        }
    } else if (_indexPage == 1) {
        self.title = NSLocalizedString(@"Configure the channel", nil);
        
        /*
         *  Fake section for a label
         */
        fakeSectionLabel.footerTitle = NSLocalizedString(@"Indicate whom will use the channel and decide which confidentiality you want to apply.", nil);
        
        /*
         *  Section for "Who is your channel for?"
         */
        _sectionAudience = [XLFormSectionDescriptor formSection];
        _sectionAudience.title = NSLocalizedString(@"Who is your channel for?", nil);
        _sectionAudience.footerTitle = NSLocalizedString(@"To members of my company only", nil);
        [form addFormSection:_sectionAudience];
        
        // Company channel option
        XLFormRowDescriptor *audienceCompany = [self createRowType:XLFormRowDescriptorTypeBooleanCheck withTitle:NSLocalizedString(@"My company", nil) withDefaultValue:@YES];
        audienceCompany.tag = kAudienceCompany;
        [_sectionAudience addFormRow:audienceCompany];
        
        // Public channel option
        if ([ServicesManager sharedInstance].myUser.isAllowedToCreatePublicChannel) {
            XLFormRowDescriptor *audienceAll = [self createRowType:XLFormRowDescriptorTypeBooleanCheck withTitle:NSLocalizedString(@"All Rainbow users", nil) withDefaultValue:@NO];
            audienceAll.tag = kAudienceAll;
            [_sectionAudience addFormRow:audienceAll];
        } else {
            audienceCompany.disabled = @YES; // If we can only create a company kind, we disable it to avoid animation
        }
        
        [self makeRadioSection:_sectionAudience];

        
        /*
         *  Section for "Select the confidentiality"
         */
        _sectionConfidentiality = [XLFormSectionDescriptor formSection];
        _sectionConfidentiality.title = NSLocalizedString(@"Select the confidentiality", nil);
        _sectionConfidentiality.footerTitle = NSLocalizedString(@"Channel can only be joined through an invitation. Only invited people can consult it, they can unsubscribe at any time.", nil);
        [form addFormSection:_sectionConfidentiality];
        
        // Private channel option
        XLFormRowDescriptor *privateChannelRow = [self createRowType:XLFormRowDescriptorTypeBooleanCheck withTitle:NSLocalizedString(@"Private channel", nil) withDefaultValue:@YES];
        privateChannelRow.tag = kConfidentialityPrivate;
        [_sectionConfidentiality addFormRow:privateChannelRow];
        
        // Open channel option
        XLFormRowDescriptor *publicChannelRow = [self createRowType:XLFormRowDescriptorTypeBooleanCheck withTitle:NSLocalizedString(@"Open channel", nil) withDefaultValue:@NO];
        publicChannelRow.tag = kConfidentialityPublic;
        [_sectionConfidentiality addFormRow:publicChannelRow];
        
        // Closed (or restricted) channel option
        if ([ServicesManager sharedInstance].myUser.isAllowedToCreateClosedChannel) {
            XLFormRowDescriptor *closedChannelRow = [self createRowType:XLFormRowDescriptorTypeBooleanCheck withTitle:NSLocalizedString(@"Restricted channel", nil) withDefaultValue:@NO];
            closedChannelRow.tag = kConfidentialityClosed;
            [_sectionConfidentiality addFormRow:closedChannelRow];
        }
        
        [self makeRadioSection:_sectionConfidentiality];
        
        if ([ServicesManager sharedInstance].myUser.isAllowedToInviteAllCompanyMembersInChannel) {
            /*
             *  Section for "Choose the people to subscribe"
             */
            _sectionPeopleToSubscribe = [XLFormSectionDescriptor formSection];
            _sectionPeopleToSubscribe.title = NSLocalizedString(@"Choose the people to subscribe", nil);
            _sectionPeopleToSubscribe.hidden = @YES;
            [form addFormSection:_sectionPeopleToSubscribe];
            
            // Some members option
            XLFormRowDescriptor *someMembers = [self createRowType:XLFormRowDescriptorTypeBooleanCheck withTitle:NSLocalizedString(@"Some members in my company", nil) withDefaultValue:@YES];
            someMembers.tag = kPeopleInviteSomeMembers;
            [_sectionPeopleToSubscribe addFormRow:someMembers];
            
            // All members option
            XLFormRowDescriptor *allMembers = [self createRowType:XLFormRowDescriptorTypeBooleanCheck withTitle:NSLocalizedString(@"All the members of my company", nil) withDefaultValue:@NO];
            allMembers.tag = kPeopleInviteAllMembers;
            [_sectionPeopleToSubscribe addFormRow:allMembers];
            
            [self makeRadioSection:_sectionPeopleToSubscribe];
        }
    }
    
    self.form = form;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    UIBarButtonItem *item = [[UIBarButtonItem alloc] init];
    item.title = @"";
    self.navigationItem.backBarButtonItem = item;
}

- (void)formRowDescriptorValueHasChanged:(XLFormRowDescriptor *)formRow oldValue:(id)oldValue newValue:(id)newValue {
    [super formRowDescriptorValueHasChanged:formRow oldValue:oldValue newValue:newValue];
    
    if ([formRow.tag isEqualToString:kCategory]) {
        XLFormRowDescriptor *row = [self.form formRowWithTag:kAvatar];
        
        if ([[row valueForKey:@"cell"] isKindOfClass:[LargePhotoCell class]]) {
            LargePhotoCell *cell = [row valueForKey:@"cell"];
            if (!cell.hasChosenImage && !_modifyingChannel.photoData) {
                ChannelCategory category = [Channel channelCategoryFromString:[newValue valueData]];
                [row.cellConfig setObject:[Channel colorForCategory:category] forKey:@"imageView.backgroundColor"];
                [cell chooseImage:[Channel imageForCategory:category]];
                cell.hasChosenImage = NO;
                [self reloadFormRow:row];
            }
        }
        [self updateDoneButton];
    } else if ([formRow.tag isEqualToString:kAvatar] || [formRow.tag isEqualToString:kDescription]) {
        [self updateDoneButton];
    } else if ([formRow.value isEqual:@YES] && formRow.sectionDescriptor == _sectionAudience) {
        
        XLFormRowDescriptor *confidentialityClosedRow = [self.form formRowWithTag:kConfidentialityClosed];
        if ([formRow.tag isEqualToString:kAudienceCompany]) {
            _sectionAudience.footerTitle = NSLocalizedString(@"To members of my company only", nil);
            confidentialityClosedRow.hidden = @NO;
        } else if ([formRow.tag isEqualToString:kAudienceAll]) {
            _sectionAudience.footerTitle = NSLocalizedString(@"To all Rainbow users (members of my company and other companies)", nil);
            if ([confidentialityClosedRow.value isEqual:@YES])
                ((XLFormRowDescriptor *)_sectionConfidentiality.formRows.firstObject).value = @YES;
            confidentialityClosedRow.hidden = @YES;
        }
        
        [UIView setAnimationsEnabled:NO];
        
        XLFormRowDescriptor *confidentialityRowChecked = [self getCheckedRow:_sectionConfidentiality];
        if ([confidentialityRowChecked.tag isEqualToString:kConfidentialityPublic]) {
            if ([formRow.tag isEqualToString:kAudienceAll])
                _sectionConfidentiality.footerTitle = NSLocalizedString(@"Rainbow users can view the channel and subscribe. You can also notify some users and invite them to subscribe.", nil);
            else
                _sectionConfidentiality.footerTitle = NSLocalizedString(@"Any member of your company can view the channel and subscribe. You can also notify some members and invite them to subscribe.", nil);
            
            // Reload the footer of the "Confidentiality" section
            if ([self.tableView numberOfSections] >= 2)
                [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:2] withRowAnimation:UITableViewRowAnimationAutomatic];
        }

        // Reload the footer of the "Confidentiality" section
        if ([self.tableView numberOfSections] >= 1)
            [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:1] withRowAnimation:UITableViewRowAnimationAutomatic];
        [UIView setAnimationsEnabled:YES];
        
    } else if ([formRow.value isEqual:@YES] && formRow.sectionDescriptor == _sectionConfidentiality) {
        
        if ([formRow.tag isEqualToString:kConfidentialityPrivate]) {
            _sectionConfidentiality.footerTitle = NSLocalizedString(@"Channel can only be joined through an invitation. Only invited people can consult it, they can unsubscribe at any time.", nil);
        } else if ([formRow.tag isEqualToString:kConfidentialityPublic]) {
            XLFormRowDescriptor *audienceRowChecked = [self getCheckedRow:_sectionAudience];
            if ([audienceRowChecked.tag isEqualToString:kAudienceAll])
                _sectionConfidentiality.footerTitle = NSLocalizedString(@"Rainbow users can view the channel and subscribe. You can also notify some users and invite them to subscribe.", nil);
            else
                _sectionConfidentiality.footerTitle = NSLocalizedString(@"Any member of your company can view the channel and subscribe. You can also notify some members and invite them to subscribe.", nil);
        } else if ([formRow.tag isEqualToString:kConfidentialityClosed]) {
            _sectionConfidentiality.footerTitle = NSLocalizedString(@"People you will choose will automatically be subscribed to this channel. They will not bbe able to unsubscribe.", nil);
        }
        
        if (_sectionPeopleToSubscribe)
            ((XLFormRowDescriptor *)_sectionPeopleToSubscribe.formRows.firstObject).value = @YES;
        
        [UIView setAnimationsEnabled:NO];
        
        // Reload the footer of the "Confidentiality" section
        if ([self.tableView numberOfSections] >= 2)
            [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:2] withRowAnimation:UITableViewRowAnimationAutomatic];
        
        // Show or hide the last section
        _sectionPeopleToSubscribe.hidden = [formRow.tag isEqualToString:kConfidentialityClosed] ? @NO : @YES;
        
        [UIView setAnimationsEnabled:YES];
        
    } else if ([formRow.value isEqual:@YES] && formRow.sectionDescriptor == _sectionPeopleToSubscribe) {
        if ([formRow.tag isEqualToString:kPeopleInviteAllMembers])
            self.navigationItem.rightBarButtonItem.title = NSLocalizedString(@"Create", nil);
        else
            self.navigationItem.rightBarButtonItem.title = NSLocalizedString(@"Continue", nil);
    } else if ([formRow.tag isEqualToString:kName]) {
        [self updateDoneButton];
    }
}

-(void) makeRadioSection:(XLFormSectionDescriptor *) section {
    __weak typeof(self) weakSelf = self;
    
    for (XLFormRowDescriptor *row in section.formRows) {
        row.onChangeBlock = ^(id  _Nullable oldValue, id  _Nullable newValue, XLFormRowDescriptor * _Nonnull rowDescriptor) {
            if ([newValue isEqual:@YES]) {
                // The case is now checked by the user so we must uncheck all others
                for (XLFormRowDescriptor *r in rowDescriptor.sectionDescriptor.formRows) {
                    if (r != rowDescriptor) {
                        r.value = @NO;
                        [weakSelf reloadFormRow:r];
                    }
                }
            } else if ([oldValue isEqual:@YES]) {
                // The case was checked but now unchecked
                // Try to know if there is another checked case
                __block BOOL someCheckedCase = NO;
                [rowDescriptor.sectionDescriptor.formRows enumerateObjectsUsingBlock:^(XLFormRowDescriptor *obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    if ([obj.value isEqual:@YES]) {
                        someCheckedCase = YES;
                        *stop = YES;
                    }
                }];
                
                // There is no other checked case so the user just clicks on the same case. Recheck it.
                if (!someCheckedCase) {
                    rowDescriptor.value = @YES;
                    [weakSelf reloadFormRow:rowDescriptor];
                }
            }
        };
    }
}

-(void) updateDoneButton {
    self.navigationItem.rightBarButtonItem.enabled = [_name.value length] > 3;
}

-(XLFormRowDescriptor *) getCheckedRow:(XLFormSectionDescriptor *) section {
    __block XLFormRowDescriptor *row = nil;
    [section.formRows enumerateObjectsUsingBlock:^(XLFormRowDescriptor * obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj.value isEqual:@YES]) {
            row = obj;
            *stop = YES;
        }
    }];
    return row;
}

- (XLFormRowDescriptor *) createRowType:(NSString *) type withTitle:(NSString *) title withDefaultValue:(id) defaultValue{
    
    XLFormRowDescriptor *row = [XLFormRowDescriptor formRowDescriptorWithTag:title rowType:type title:NSLocalizedString(title, nil)];
    if (defaultValue) {
        if ([defaultValue isKindOfClass:[NSString class]]) {
            // Replace <spaces> by <non-breaking spaces>
            // This fix an issue because we have right aligned textfields and spaces are not
            // displayed if they are the last character of the string
            row.value = [defaultValue replaceSpaceWithNonBrSpace];
        } else {
            row.value = defaultValue;
        }
    }
    
    // Apply generic customization.
    [row.cellConfig setObject:[UIFont fontWithName:[UITools defaultFontName] size:15] forKey:@"textLabel.font"];
    [row.cellConfig setObject:[UIColor lightGrayColor] forKey:@"textLabel.textColor"];
    [row.cellConfig setObject:[UITools defaultTintColor] forKey:@"self.tintColor"];
    
    if ([[XLFormViewController cellClassesForRowDescriptorTypes] objectForKey:type] == [XLFormTextFieldCell class]) {
        [row.cellConfig setObject:[UIFont fontWithName:[UITools defaultFontName] size:15] forKey:@"textField.font"];
        [row.cellConfig setObject:[NSNumber numberWithFloat:0.4] forKey:XLFormTextFieldLengthPercentage];
    }
    
    if ([type isEqualToString:XLFormRowDescriptorTypeSelectorPickerView]) {
        [row.cellConfig setObject:[UIFont fontWithName:[UITools defaultFontName] size:15] forKey:@"detailTextLabel.font"];
        [row.cellConfig setObject:[UIColor blackColor] forKey:@"detailTextLabel.textColor"];
        [row.cellConfig setObject:[UITools defaultTintColor] forKey:@"self.tintColor"];
    }
    
    if ([[XLFormViewController cellClassesForRowDescriptorTypes] objectForKey:type] == [XLFormTextViewCell class]) {
        [row.cellConfig setObject:[UIFont fontWithName:[UITools defaultFontName] size:15] forKey:@"textView.font"];
        [row.cellConfig setObject:[UIFont fontWithName:[UITools defaultFontName] size:15] forKey:@"textView.placeHolderLabel.font"];
    }
    
    if ([[XLFormViewController cellClassesForRowDescriptorTypes] objectForKey:type] == [LargePhotoCell class]) {
        [row.cellConfigAtConfigure setObject:[NSNumber numberWithBool:YES] forKey:@"squaredImage"];
    }
    
    return row;
}

- (void)tableView:(UITableView *)tableView willDisplayFooterView:(UIView *)view forSection:(NSInteger)section {
    UITableViewHeaderFooterView *footer = (UITableViewHeaderFooterView *)view;
    footer.textLabel.font = [UIFont fontWithName:[UITools defaultFontName] size:13];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    UIViewController *destViewController = [segue destinationViewController];
    
    if ([[segue identifier] isEqualToString:@"createModifyChannelStep2SegueID"]) {
        if ([destViewController isKindOfClass:[UIChannelsCreateModifyViewController class]]) {
            UIChannelsCreateModifyViewController *createModifyStep2ViewController = (UIChannelsCreateModifyViewController *)destViewController;
            createModifyStep2ViewController.indexPage = 1;
            createModifyStep2ViewController.channelNameString = _name.value;
            createModifyStep2ViewController.channelDescriptionString = _channeldescription.value;
            createModifyStep2ViewController.channelCategoryString = [_category.value valueData];
            if ([[_photo valueForKey:@"cell"] isKindOfClass:[LargePhotoCell class]]) {
                LargePhotoCell *cell = [_photo valueForKey:@"cell"];
                if (cell.hasChosenImage)
                    createModifyStep2ViewController.avatar = cell.imageView.image;
            }
        }
    } else if ([[segue identifier] isEqualToString:@"channelsAddMembersSegueID"]) {
        XLFormRowDescriptor *audienceRowChecked = [self getCheckedRow:_sectionAudience];
        XLFormRowDescriptor *confidentialityRowChecked = [self getCheckedRow:_sectionConfidentiality];
        ChannelMode mode = [Channel channelModeFromString:[NSString stringWithFormat:@"%@_%@", audienceRowChecked.tag, confidentialityRowChecked.tag]];
        
        UIChannelsAddMembersViewController *addMembersViewController = (UIChannelsAddMembersViewController *)destViewController;
        addMembersViewController.channelCategoryString = _channelCategoryString;
        addMembersViewController.avatar = _avatar;
        addMembersViewController.channelNameString = _channelNameString;
        addMembersViewController.channelDescriptionString = _channelDescriptionString;
        addMembersViewController.channelMode = mode;
    }
}

#pragma mark - Continue button clicked

-(void) continueButtonClicked {
    if (_indexPage == 0) {
        [self.tableView endEditing:YES];
        if (_modifyingChannel) {
            NSString *updatedName = nil;
            if (![_name.value isEqualToString:_modifyingChannel.name])
                updatedName = _name.value;
            
            NSString *updatedDescription = nil;
            if (![_channeldescription.value isEqualToString:_modifyingChannel.channelDescription])
                updatedDescription = _channeldescription.value ? _channeldescription.value : @"";
            
            NSString *updatedCategory = nil;
            if (![[_category.value valueData] isEqualToString:_modifyingChannel.category])
                updatedCategory = [_category.value valueData];
            
            if (updatedName || updatedDescription || updatedCategory)
                [[ServicesManager sharedInstance].channelsService updateChannel:_modifyingChannel.id name:updatedName description:updatedDescription category:updatedCategory completionHandler:nil];
            
            if ([[_photo valueForKey:@"cell"] isKindOfClass:[LargePhotoCell class]]) {
                LargePhotoCell *cell = [_photo valueForKey:@"cell"];
                if (cell.hasChosenImage)
                    [[ServicesManager sharedInstance].channelsService updateChannel:_modifyingChannel.id avatar:cell.imageView.image completionHandler:nil];
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.navigationController popViewControllerAnimated:YES];
            });
        } else {
            [self performSegueWithIdentifier:@"createModifyChannelStep2SegueID" sender:nil];
        }
    } else if (_indexPage == 1) {
        if ([[self getCheckedRow:_sectionPeopleToSubscribe].tag isEqualToString:kPeopleInviteAllMembers]) {
            // Create a closed channel and invite all members of the company in it
            [[ServicesManager sharedInstance].channelsService createClosedChannel:_channelNameString description:_channelDescriptionString category:_channelCategoryString maxItems:50 autoprov:YES completionHandler:^(Channel *channel, NSError *error) {
                if (!error) {
                    if (_avatar)
                        [[ServicesManager sharedInstance].channelsService updateChannel:channel.id avatar:_avatar completionHandler:nil];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        NSInteger indexViewControllerToPop = [self.navigationController.viewControllers count] - 3;
                        if (indexViewControllerToPop >= 0)
                            [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:indexViewControllerToPop] animated:YES];
                    });
                } else {
                    NSString *message = @"";
                    if (error.code == 409000)
                        message = [NSString stringWithFormat:NSLocalizedString(@"The channel \"%@\" already exists. Please try again with another name.", nil), _channelNameString];
                    else
                        message = [NSString stringWithFormat:NSLocalizedString(@"The channel \"%@\" cannot be created.", nil), _channelNameString];
                    [UITools showErrorPopupWithTitle:NSLocalizedString(@"An error has occurred", nil) message:message inViewController:self];
                }
            }];
        } else {
            // Open the add members view controller
            [self performSegueWithIdentifier:@"channelsAddMembersSegueID" sender:nil];
        }
    }
}


@end
