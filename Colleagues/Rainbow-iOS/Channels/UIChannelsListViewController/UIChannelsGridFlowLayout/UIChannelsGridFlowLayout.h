//
//  UIChannelsGridFlowLayout.h
//  Rainbow-iOS
//
//  Created by Le Trong Nghia Huynh on 05/02/2019.
//  Copyright © 2019 ALE International. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIChannelsGridFlowLayout : UICollectionViewFlowLayout

@property (nonatomic, assign) int padding;
@property (nonatomic) UICollectionViewScrollDirection collectionViewScrollDirection;

-(instancetype) initWithPadding:(int) padding;
-(instancetype) initWithPadding:(int) padding scrollDirection:(UICollectionViewScrollDirection) scrollDirection;

@end
