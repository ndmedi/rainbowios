//
//  UIChannelsListViewController.h
//  Rainbow-iOS
//
//  Created by Le Trong Nghia Huynh on 05/02/2019.
//  Copyright © 2019 ALE International. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Rainbow/ChannelsService.h>
#import "Channel+Rainbow.h"

@interface UIChannelsListViewController : UIViewController

-(void) loadChannelsForCategory:(ChannelCategory) category;
-(void) loadMostFollowedChannels;
-(void) loadMostRecentChannels;

@end
