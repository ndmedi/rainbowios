//
//  UIChannelsListViewController.m
//  Rainbow-iOS
//
//  Created by Le Trong Nghia Huynh on 05/02/2019.
//  Copyright © 2019 ALE International. All rights reserved.
//

#import "UIChannelsListViewController.h"
#import "UIChannelsGridFlowLayout.h"
#import <DZNEmptyDataSet/DZNEmptyDataSet.h>
#import "FilteredSortedSectionedArray.h"
#import <Rainbow/ChannelsService.h>
#import "UITools.h"
#import <Rainbow/ServicesManager.h>
#import "UIChannelCollectionViewCell.h"
#import "UIChannelHeaderViewCell.h"
#import "UIChannelsItemsViewController.h"
#import "ChannelCellProtocol.h"
#import "UIChannelsCreateModifyViewController.h"

#define kCollectionViewChannelReusableKey @"channelCellID"
#define kCollectionViewChannelInvitationReusableKey @"channelInvitationCellID"
#define kChannelHeaderCellID @"channelHeaderCellID"

#define kChannelsInvitationsSection @"ChannelsInvitations"
#define kChannelsSection @"Channels"

#define kModeNormal 0
#define kModeByCategoryChannels 1
#define kModeMostFollowedChannels 2
#define kModeMostRecentsChannels 3

@interface UIChannelsListViewController () <DZNEmptyDataSetDelegate, DZNEmptyDataSetSource, UICollectionViewDelegate, UICollectionViewDataSource, ChannelCellProtocol>

@property (nonatomic, strong) ChannelsService *channelsService;

@property (nonatomic, strong) IBOutlet UICollectionView *collectionView;
@property (nonatomic, strong) UIChannelsGridFlowLayout *gridFlowLayout;
@property (nonatomic, strong) FilteredSortedSectionedArray <Channel *> *channels;

@property (nonatomic, strong) SectionNameComputationBlock sectionedByName;
@property (nonatomic, strong) NSSortDescriptor *sortSectionAsc;
@property (nonatomic) int mode;

@end

@implementation UIChannelsListViewController

-(instancetype) initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    
    if (self) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didLogin:) name:kLoginManagerDidLoginSucceeded object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didLogout:) name:kLoginManagerDidLogoutSucceeded object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didLostConnection:) name:kLoginManagerDidLostConnection object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReconnect:) name:kLoginManagerDidReconnect object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didAddChannel:) name:kChannelsServiceDidAddChannel object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didRemoveChannel:) name:kChannelsServiceDidRemoveChannel object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didUpdateChannel:) name:kChannelsServiceDidUpdateChannel object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didScrollToTop:) name:@"channelsServiceDidScrollToTop" object:nil];
        
        _channelsService = [ServicesManager sharedInstance].channelsService;
        _channels = [FilteredSortedSectionedArray new];
        _mode = kModeNormal;
        
        _sectionedByName = ^NSString*(Channel *channel) {
            if (channel.invited)
                return kChannelsInvitationsSection;
            
            return kChannelsSection;
        };
        
        _sortSectionAsc = [NSSortDescriptor sortDescriptorWithKey:@"self" ascending:NO comparator:^NSComparisonResult(NSString* obj1, NSString* obj2) {
            return [obj1 compare:obj2 options:NSCaseInsensitiveSearch];
        }];
        
        NSPredicate *filter = [NSPredicate predicateWithBlock:^BOOL(Channel *channel, NSDictionary<NSString *,id> * bindings) {
            return YES;
        }];

        _channels.sectionNameFromObjectComputationBlock = _sectionedByName;
        _channels.globalFilteringPredicate = filter;
        _channels.sectionSortDescriptor = _sortSectionAsc;
        
        
        _channels.objectSortDescriptorForSection = @{@"__default__": @[[UITools sortDescriptorByString:@"name"]]};

        // The padding will automatically resize the channel object
        _gridFlowLayout = [[UIChannelsGridFlowLayout alloc] initWithPadding:5];
    }
    
    return self;
}

-(void) dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidLoginSucceeded object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidLogoutSucceeded object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidLostConnection object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidReconnect object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kChannelsServiceDidAddChannel object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kChannelsServiceDidRemoveChannel object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kChannelsServiceDidUpdateChannel object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"channelsServiceDidScrollToTop" object:nil];
    
    @synchronized (_channels) {
        [_channels removeAllObjects];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.collectionView.emptyDataSetSource = self;
    self.collectionView.emptyDataSetDelegate = self;
    self.collectionView.allowsMultipleSelection = YES;
    self.collectionView.dataSource = self;
    self.collectionView.delegate = self;
    self.collectionView.collectionViewLayout = _gridFlowLayout;
    self.collectionView.contentInset = UIEdgeInsetsMake(_gridFlowLayout.padding, _gridFlowLayout.padding, 50 + _gridFlowLayout.padding, _gridFlowLayout.padding);
    [self.collectionView registerNib:[UINib nibWithNibName:@"UIChannelCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:kCollectionViewChannelReusableKey];
    
    if (_mode == kModeNormal) {
        for (Channel *channel in [_channelsService.channels copy]) {
            [self insertChannel:channel];
        }
        [_channels reloadData];
    }
    
    // Pull to refresh
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(refreshCollectionView:) forControlEvents:UIControlEventValueChanged];
    _collectionView.refreshControl = refreshControl;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) loadChannelsForCategory:(ChannelCategory) category {
    self.title = [self getLabelCategory:category];
    _mode = kModeByCategoryChannels;
    
    [_channelsService fetchChannels:50 forCategory:[Channel stringFromChannelCategory:category] ignoreSubscribed:NO completionHandler:^(NSArray<Channel *> *channels, NSError *error) {
        if (!error && channels) {
            _channels.objectSortDescriptorForSection = @{@"__default__": @[[UITools sortDescriptorByInt:@"subscribersCount" ascending:NO]]};
            for (Channel *channel in channels) {
                [self insertChannel:channel];
            }
            [_channels reloadData];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [_collectionView reloadData];
            });
        }
    }];
}

-(void) loadMostFollowedChannels {
    self.title = NSLocalizedString(@"Most followed channels", nil);
    _mode = kModeMostFollowedChannels;
    
    [_channelsService fetchMostFollowedChannels:50 ignoreSubscribed:YES completionHandler:^(NSArray<Channel *> *channels, NSError *error) {
        if (!error && channels) {
            _channels.objectSortDescriptorForSection = @{@"__default__": @[[UITools sortDescriptorByInt:@"subscribersCount" ascending:NO]]};
            for (Channel *channel in channels) {
                [self insertChannel:channel];
            }
            [_channels reloadData];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [_collectionView reloadData];
            });
        }
    }];
}

-(void) loadMostRecentChannels {
    self.title = NSLocalizedString(@"Most recent channels", nil);
    _mode = kModeMostRecentsChannels;
    
    [_channelsService fetchMostRecentChannels:50 ignoreSubscribed:YES completionHandler:^(NSArray<Channel *> *channels, NSError *error) {
        if (!error && channels) {
            _channels.objectSortDescriptorForSection = @{@"__default__": @[[UITools sortDescriptorByCreationDate]]};
            for (Channel *channel in channels) {
                [self insertChannel:channel];
            }
            [_channels reloadData];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [_collectionView reloadData];
            });
        }
    }];
}

// To be refactored...
-(NSString *) getLabelCategory:(ChannelCategory) category {
    NSString *categoryStr = nil;
    
    switch (category) {
        case ChannelCategoryGlobalNews:
            categoryStr = NSLocalizedString(@"globalnews", nil);
            break;
        case ChannelCategoryDesign:
            categoryStr = NSLocalizedString(@"design", nil);
            break;
        case ChannelCategoryInnovation:
            categoryStr = NSLocalizedString(@"innovation", nil);
            break;
        case ChannelCategorySocialMedia:
            categoryStr = NSLocalizedString(@"social", nil);
            break;
        case ChannelCategoryScience:
            categoryStr = NSLocalizedString(@"science", nil);
            break;
        case ChannelCategoryCulture:
            categoryStr = NSLocalizedString(@"culture", nil);
            break;
        case ChannelCategoryBusiness:
            categoryStr = NSLocalizedString(@"business", nil);
            break;
        case ChannelCategoryHitech:
            categoryStr = NSLocalizedString(@"hitech", nil);
            break;
        case ChannelCategoryMarketing:
            categoryStr = NSLocalizedString(@"marketing", nil);
            break;
        case ChannelCategoryTravel:
            categoryStr = NSLocalizedString(@"travel", nil);
            break;
    }
    
    return categoryStr;
}

#pragma mark - Segues

-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([[segue identifier] isEqualToString:@"showChannelItemsFromChannelSegueID"]){
        UIViewController *destViewController = [segue destinationViewController];
        
        if ([destViewController isKindOfClass:[UIChannelsItemsViewController class]] && [sender isKindOfClass:[UIChannelCollectionViewCell class]]) {
            UIChannelsItemsViewController *itemsViewController = (UIChannelsItemsViewController *)destViewController;
            UIChannelCollectionViewCell *cell = (UIChannelCollectionViewCell *)sender;
            itemsViewController.selectedChannel = cell.channel;
        }
    } else if ([[segue identifier] isEqualToString:@"createModifyChannelSegueID"]) {
        UIViewController *destViewController = [segue destinationViewController];
        
        if ([destViewController isKindOfClass:[UIChannelsCreateModifyViewController class]] && [sender isKindOfClass:[Channel class]]) {
            UIChannelsCreateModifyViewController *controller = (UIChannelsCreateModifyViewController *)destViewController;
            controller.modifyingChannel = sender;
        }
    }
}

#pragma mark - Login/logout notification

-(void) didLogin:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didLogin:notification];
        });
        return;
    }
    
    if ([self isViewLoaded])
        [self.collectionView reloadData];
}

-(void) didLogout:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didLogout:notification];
        });
        return;
    }
    
    @synchronized (_channels) {
        [_channels removeAllObjects];
    }
    
    [self.collectionView reloadData];
    
    self.tabBarController.selectedIndex = 0;
    [self.navigationController popToRootViewControllerAnimated:NO];
}

-(void) didLostConnection:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didLostConnection:notification];
        });
        return;
    }
    
    if ([self isViewLoaded])
        [self.collectionView reloadData];
}

-(void) didReconnect:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didReconnect:notification];
        });
        return;
    }
    if ([self isViewLoaded])
        [self.collectionView reloadData];
    
}

-(void) didScrollToTop:(NSNotification *) notification {
    if (notification.object && [notification.object isKindOfClass:[NSString class]]) {
        NSString *controllerCurrentlyDisplayed = (NSString *)notification.object;
        
        if ([controllerCurrentlyDisplayed isEqualToString:@"UIChannelsListViewController"])
            [self.collectionView setContentOffset:CGPointMake(0.0f, -self.collectionView.contentInset.top) animated:YES];
    }
}

-(BOOL) insertChannel:(Channel *) channel {
    Channel *chan = [self getChannelById:channel.id];
    
    // We found the same channel but not the same INSTANCE (because we were not subscribed before)
    // Remove it and add the new instance
    if (chan && chan != channel)
        [_channels removeObject:chan];
    
    if (![_channels containsObject:channel]) {
        [_channels addObject:channel];
        return YES;
    } else {
        return NO;
    }
}

-(BOOL) removeChannel:(Channel *) channel {
    if ([_channels containsObject:channel]) {
        [_channels removeObject:channel];
        return YES;
    } else {
        return NO;
    }
}

-(Channel *) getChannelById:(NSString *) channelId {
    for (Channel *channel in _channels) {
        if ([channel.id isEqualToString:channelId])
            return channel;
    }
    
    return nil;
}

#pragma mark - Notifications channels management

-(void) didAddChannel:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didAddChannel:notification];
        });
        return;
    }
    
    if (!notification.object)
        return;
    
    Channel *channel = (Channel *)[notification.object objectForKey:kChannelKey];
    
    if (channel && [self insertChannel:channel]) {
        [_channels reloadData];
        if ([self isViewLoaded])
            [self.collectionView reloadData];
    }
}

-(void) didRemoveChannel:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didRemoveChannel:notification];
        });
        return;
    }
    
    if (!notification.object)
        return;
    
    Channel *channel = (Channel *)[notification.object objectForKey:kChannelKey];
    
    if (channel) {
        if ((_mode != kModeNormal && [_channels containsObject:channel]) || [self removeChannel:channel]) {
            [_channels reloadData];
            if ([self isViewLoaded])
                [self.collectionView reloadData];
        }
    }
}

-(void) didUpdateChannel:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didUpdateChannel:notification];
        });
        return;
    }
    
    if (!notification.object)
        return;
    
    Channel *channel = (Channel *)[notification.object objectForKey:kChannelKey];
    NSArray *changedKeys = (NSArray *)[notification.object objectForKey:kChangedKeys];
    if (channel && [changedKeys count] > 0) {
        [_channels reloadData];
        if ([self isViewLoaded])
            [self.collectionView reloadData];
    }
}

#pragma mark - Collection view delegates

- (NSInteger) numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    if (_channels && [_channels sections])
        return [[_channels sections] count];
    
    return 0;
}

- (NSInteger) collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (_channels) {
        NSString *key = [[_channels sections] objectAtIndex:section];
        return [[_channels objectsInSection:key] count];
    }
    
    return 0;
}

- (UICollectionViewCell *) collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(nonnull NSIndexPath *)indexPath {
    UIChannelCollectionViewCell *cell; // = [collectionView dequeueReusableCellWithReuseIdentifier:kCollectionViewChannelReusableKey forIndexPath:indexPath];
    NSString *key = [[_channels sections] objectAtIndex:indexPath.section];
    Channel *channel = [[_channels objectsInSection:key] objectAtIndex:indexPath.row];
    if (channel.invited) {
        cell = [collectionView dequeueReusableCellWithReuseIdentifier:kCollectionViewChannelInvitationReusableKey forIndexPath:indexPath];
    } else {
        cell = [collectionView dequeueReusableCellWithReuseIdentifier:kCollectionViewChannelReusableKey forIndexPath:indexPath];
    }
    
    cell.discoverMode = _mode != kModeNormal;
    cell.delegate = self;
    cell.channel = channel;
    
    return cell;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    UIChannelHeaderViewCell *cell = nil;
    
    if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
        cell = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:kChannelHeaderCellID forIndexPath:indexPath];
        
        NSString *key = [[_channels sections] objectAtIndex:indexPath.section];
        Channel *channel = [[_channels objectsInSection:key] objectAtIndex:indexPath.row];
        if (channel.invited)
            cell.sectionLabel.text = NSLocalizedString(@"Invitations", nil);
        else
            cell.sectionLabel.text = NSLocalizedString(@"My channels", nil);
    }
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    NSString *key = [[_channels sections] objectAtIndex:indexPath.section];
    Channel *channel = [[_channels objectsInSection:key] objectAtIndex:indexPath.row];

    if (channel) {
        UIChannelsItemsViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"channelsItemsViewControllerID"];
        viewController.selectedChannel = channel;
        [self.navigationController pushViewController:viewController animated:YES];
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section {
    if ([[_channels sections] count] == 1 && [[[_channels sections] firstObject] isEqualToString:kChannelsSection])
        return CGSizeMake(0, 0);
    
    return CGSizeMake(CGRectGetWidth(collectionView.frame), 50);
}

#pragma mark - Refresh control

-(void) refreshCollectionView:(UIRefreshControl *) refreshControl {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [NSThread sleepForTimeInterval:1];

        [_channelsService refreshChannelsListWithCompletionBlock:^(NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [refreshControl endRefreshing];
            });
        }];
    });
}

#pragma mark - DZNEmptyDataSet
- (UIImage *)imageForEmptyDataSet:(UIScrollView *)scrollView {
    return [UIImage imageNamed:@"ChannelsIcon"];
}

- (UIColor *)imageTintColorForEmptyDataSet:(UIScrollView *)scrollView {
    return [UITools defaultTintColor];
}

- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView {
    return nil;
}

- (NSAttributedString *)descriptionForEmptyDataSet:(UIScrollView *)scrollView {
    NSString *text = NSLocalizedString(@"Share information with channels. Invite users and broadcast messages.", nil);
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont fontWithName:[UITools defaultFontName] size:16.0f], NSForegroundColorAttributeName: [UIColor darkGrayColor]};
    
    return [[NSAttributedString alloc] initWithString:text attributes:attributes];
}

- (NSAttributedString *)buttonTitleForEmptyDataSet:(UIScrollView *)scrollView forState:(UIControlState)state {
    return nil;
    
    //    NSDictionary *attributes = @{NSFontAttributeName: [UIFont fontWithName:[UITools defaultFontName] size:16.0f], NSForegroundColorAttributeName : [UIColor whiteColor]};
    //
    //    return [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Create a bubble", nil) attributes:attributes];
}

-(UIImage *) buttonBackgroundImageForEmptyDataSet:(UIScrollView *)scrollView forState:(UIControlState)state {
    return nil;
}

- (UIColor *)backgroundColorForEmptyDataSet:(UIScrollView *)scrollView {
    return [UITools defaultBackgroundColor];
}

- (CGFloat)verticalOffsetForEmptyDataSet:(UIScrollView *)scrollView {
    CGFloat offset = CGRectGetHeight([UIApplication sharedApplication].statusBarFrame);
    offset += CGRectGetHeight(self.navigationController.navigationBar.frame);
    offset -= 60;
    return -offset;
}

- (CGFloat)spaceHeightForEmptyDataSet:(UIScrollView *)scrollView {
    return 0.0f;
}

- (BOOL)emptyDataSetShouldDisplay:(UIScrollView *)scrollView {
    return YES;
}

- (BOOL)emptyDataSetShouldAllowTouch:(UIScrollView *)scrollView {
    // Disable interaction if user is guest
    if ([ServicesManager sharedInstance].myUser.isGuest)
        return NO;
    else
        return  YES;
}

- (BOOL)emptyDataSetShouldAllowScroll:(UIScrollView *)scrollView {
    return NO;
}

- (void)emptyDataSet:(UIScrollView *)scrollView didTapButton:(UIButton *)button {
    // [self didTapOnAddButton:nil];
}

#pragma mark - ChannelCellProtocol

- (void)modifyChannelClicked:(Channel *)channel {
    [self performSegueWithIdentifier:@"createModifyChannelSegueID" sender:channel];
}

@end
