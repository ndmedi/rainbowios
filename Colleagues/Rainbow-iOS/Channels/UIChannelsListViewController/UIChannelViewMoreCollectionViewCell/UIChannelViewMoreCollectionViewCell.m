//
//  UIChannelViewMoreCollectionViewCell.m
//  Rainbow-iOS
//
//  Created by Le Trong Nghia Huynh on 01/03/2019.
//  Copyright © 2019 ALE International. All rights reserved.
//

#import "UIChannelViewMoreCollectionViewCell.h"
#import "UITools.h"

@implementation UIChannelViewMoreCollectionViewCell

-(void) awakeFromNib {
    [super awakeFromNib];
    
    _arrowRightImageView.tintColor = [UITools defaultTintColor];
    _viewMoreLabel.text = NSLocalizedString(@"View more", nil);
    self.backgroundColor = [UITools colorFromHexa:0xE6E6E6FF];
}

@end
