//
//  UIChannelCollectionViewCell.m
//  Rainbow-iOS
//
//  Created by Le Trong Nghia Huynh on 05/02/2019.
//  Copyright © 2019 ALE International. All rights reserved.
//

#import "UIChannelCollectionViewCell.h"
#import "UITools.h"
#import <Rainbow/ServicesManager.h>
#import "Channel+Rainbow.h"

@interface UIChannelCollectionViewCell ()

@property (weak, nonatomic) IBOutlet UIImageView *avatar;
@property (weak, nonatomic) IBOutlet UILabel *subscribersCountLabel;
@property (weak, nonatomic) IBOutlet UIImageView *actionIcon;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UIStackView *followedStackviewLabel;
@property (weak, nonatomic) IBOutlet UIImageView *doneIcon;
@property (weak, nonatomic) IBOutlet UILabel *followedLabel;
@property (weak, nonatomic) IBOutlet UIImageView *lockIcon;

@property (nonatomic, strong) UIAlertController *actionSheet;

@end

@implementation UIChannelCollectionViewCell

-(void) awakeFromNib {
    [super awakeFromNib];
    
    _discoverMode = NO;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didUpdateChannel:) name:kChannelsServiceDidUpdateChannel object:nil];
    
    [UITools applyThinCustomFontTo:_subscribersCountLabel];
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTapping:)];
    [_actionIcon setUserInteractionEnabled:YES];
    [singleTap setNumberOfTapsRequired:1];
    
    [_actionIcon addGestureRecognizer:singleTap];
    
    // Add gray border around the cell
    self.contentView.layer.borderWidth = 0.5;
    self.contentView.layer.borderColor = [[UITools colorFromHexa:0xBFBFBFFF] CGColor];
    self.contentView.layer.masksToBounds = NO;
    
    [self resetGUI];
    
    [self setTintColor:[UITools defaultTintColor]];
    
    [_doneIcon setTintColor:[UITools colorFromHexa:0x00CC00FF]];
    [UITools applyCustomFontTo:_followedLabel andSize:13];
    [_followedLabel setText:NSLocalizedString(@"Followed", nil)];
}

-(void) singleTapping:(UIGestureRecognizer *) recognizer {
    UIViewController *activeViewController = [[[UIApplication sharedApplication] keyWindow] rootViewController];

    if (_actionSheet) {
        [_actionSheet dismissViewControllerAnimated:NO completion:nil];
        _actionSheet = nil;
    }
    
    _actionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    // Set the attributedTitle in bold
    NSAttributedString *attrString = [[NSAttributedString alloc] initWithString:_channel.name attributes:@{NSFontAttributeName: [UIFont boldSystemFontOfSize:30.0f]}];
    [_actionSheet setValue:attrString forKey:@"_attributedTitle"];
    
    if ([_channel.creatorId isEqualToString:[ServicesManager sharedInstance].myUser.contact.rainbowID]) {
        // Modify channel button
        UIAlertAction *modifyChannel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Modify this channel", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [_delegate modifyChannelClicked:_channel];
        }];
        [_actionSheet addAction:modifyChannel];
        
        // Remove channel button
        UIAlertAction* removeChannel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Delete this channel", nil) style:UIAlertActionStyleDestructive handler:^(UIAlertAction * action) {
            if (_actionSheet) {
                [_actionSheet dismissViewControllerAnimated:NO completion:nil];
                _actionSheet = nil;
            }
            
            _actionSheet = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Delete this channel", nil) message:NSLocalizedString(@"Are you sure you want to delete this channel?", nil) preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *confirmRemove = [UIAlertAction actionWithTitle:NSLocalizedString(@"Delete", nil) style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
                [[ServicesManager sharedInstance].channelsService deleteChannel:_channel.id completionHandler:nil];
            }];
            [_actionSheet addAction:confirmRemove];
            
            UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style:UIAlertActionStyleCancel handler:nil];
            [_actionSheet addAction:cancel];
            
            // show the menu.
            [_actionSheet.view setTintColor:[UITools defaultTintColor]];
            [activeViewController presentViewController:_actionSheet animated:YES completion:nil];
        }];
        [_actionSheet addAction:removeChannel];
    } else if (!_channel.subscribed) {
        // Follow button
        UIAlertAction* followChannel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Follow", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
            [[ServicesManager sharedInstance].channelsService subscribeToChannel:_channel completionHandler:nil];
        }];
        [_actionSheet addAction:followChannel];
    } else if (_channel.mode != ChannelModeCompanyClosed) {
        // Unfollow button
        UIAlertAction* unfollowChannel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Unfollow", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
            [[ServicesManager sharedInstance].channelsService unsubscribeToChannel:_channel completionHandler:nil];
        }];
        [_actionSheet addAction:unfollowChannel];
    }
    
    // Cancel action
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style:UIAlertActionStyleCancel handler:nil];
    [_actionSheet addAction:cancel];
    
    // show the menu.
    [_actionSheet.view setTintColor:[UITools defaultTintColor]];
    [activeViewController presentViewController:_actionSheet animated:YES completion:nil];
}

-(void) prepareForReuse {
    [super prepareForReuse];
    [self resetGUI];
}

-(void) resetGUI {
    // Avatar
    _avatar.image = nil;
    [_avatar setTintColor:nil];
    [_avatar setBackgroundColor:nil];
    [_actionIcon setHidden:YES];
    [_followedStackviewLabel setHidden:YES];
    [_lockIcon setHidden:YES];
}

-(void) setChannel:(Channel *) channel {
    if (_channel != channel) {
        _channel = nil;
        _channel = channel;
    }
    
    if (_channel.photoData) {
        _avatar.image = [UIImage imageWithData:_channel.photoData];
        _avatar.contentMode = UIViewContentModeScaleAspectFill;
    } else {
        _avatar.image = [Channel imageForCategory:_channel.categoryID];
        _avatar.contentMode = UIViewContentModeCenter;
        [_avatar setBackgroundColor:[Channel colorForCategory:_channel.categoryID]];
        [_avatar setTintColor:[UIColor whiteColor]];
    }
    
    if (!channel.invited)
        [_subscribersCountLabel setText:[NSString stringWithFormat:@"%d", _channel.subscribersCount]];
    
    [_name setText:_channel.name];
    
    if (channel.mode == ChannelModeCompanyClosed)
        _lockIcon.hidden = NO;
    
    if (_discoverMode && channel.subscribed) {
        [_followedStackviewLabel setHidden:NO];
    } else {
        // Display the 3 dots if:
        // * the user is the owner
        // * the user is not subscribed
        // * the user is subscribed but this is not a closed company (impossible to unsubscribe)
        _actionIcon.hidden = !([_channel.creatorId isEqualToString:[ServicesManager sharedInstance].myUser.contact.rainbowID] ||
                                !_channel.subscribed ||
                                _channel.mode != ChannelModeCompanyClosed);
    }
}

-(void) didUpdateChannel:(NSNotification *) notification {
    NSDictionary *userInfo = notification.object;
    Channel *channel = [userInfo objectForKey:@"channel"];
    NSArray<NSString *> *changedKeys = [userInfo objectForKey:@"channelChangedAttributes"];
    BOOL needUpdate = NO;
    if ([channel.id isEqualToString:_channel.id]) {
        if([changedKeys containsObject:@"photoData"])
            needUpdate = YES;
        if ([changedKeys containsObject:@"lastAvatarUpdateDate"])
            needUpdate = YES;
    }
    
    if(needUpdate){
        if(![[NSThread currentThread] isEqual:[NSThread mainThread]]) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self setNeedsLayout];
            });
            return;
        } else {
            [self setNeedsLayout];
        }
    }
}

- (IBAction)acceptInvitationButtonClicked:(id)sender {
    [[ServicesManager sharedInstance].channelsService acceptInvitation:_channel];
}

- (IBAction)declineInvitationButtonClicked:(id)sender {
    [[ServicesManager sharedInstance].channelsService declineInvitation:_channel];
}



@end
