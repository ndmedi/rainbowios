//
//  UIChannelCollectionViewCell.h
//  Rainbow-iOS
//
//  Created by Le Trong Nghia Huynh on 05/02/2019.
//  Copyright © 2019 ALE International. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Rainbow/ChannelsService.h>
#import "ChannelCellProtocol.h"

@interface UIChannelCollectionViewCell : UICollectionViewCell

@property (nonatomic, strong) Channel *channel;
@property (nonatomic) BOOL discoverMode;

@property (weak) id<ChannelCellProtocol> delegate;

@end
