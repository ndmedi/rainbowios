//
//  ChannelCellProtocol.h
//  Rainbow-iOS
//
//  Created by Le Trong Nghia Huynh on 20/05/2019.
//  Copyright © 2019 ALE International. All rights reserved.
//

#import <Rainbow/ChannelsService.h>

@protocol ChannelCellProtocol <NSObject>

/**
 *  Perfomed when the user clicks on the Modify option menu on a channel
 */
-(void) modifyChannelClicked:(Channel *) channel;

@end
