//
//  ChannelItemCellProtocol.h
//  Rainbow-iOS
//
//  Created by Le Trong Nghia Huynh on 08/04/2019.
//  Copyright © 2019 ALE International. All rights reserved.
//

#import <Rainbow/ChannelsService.h>

@protocol ChannelItemCellProtocol <NSObject>

/**
 *  Perfomed when the user clicks on the Create option menu on a channel item
 */
-(void) publishMessageToChannelClicked:(Channel *) channel;

/**
 *  Perfomed when the user clicks on the Modify option menu on a channel item
 */
-(void) modifyChannelItemClicked:(ChannelItem *) channelItem;

@end
