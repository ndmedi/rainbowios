/*
 * Rainbow
 *
 * Copyright (c) 2018, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "PublishersTableViewController.h"
#import "PublishersTableViewCell.h"
#import "UITools.h"
#import "UIScrollView+APParallaxHeader.h"
#import <Rainbow/Rainbow.h>

NSString *const kPublishersDidSelected = @"didSelectPublisher";

@interface PublishersTableViewController ()
@property (strong, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UILabel *headerLabel;
@end

@implementation PublishersTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.rowHeight = 60;
    [UITools applyCustomFontTo:_headerLabel];
    _headerLabel.text = NSLocalizedString(@"Who is sharing video", nil);
    _headerView.clipsToBounds = NO;
    [self.tableView addParallaxWithView:_headerView andHeight:_headerView.frame.size.height andMinHeight:_headerView.frame.size.height andShadow:YES];
    self.tableView.parallaxView.layer.zPosition = 1;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didUpdateConference:) name:kConferencesManagerDidUpdateConference object:nil];
}

-(void) dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kConferencesManagerDidUpdateConference object:nil];
}

-(void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    if(_dimView)
        [_dimView removeFromSuperview];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)didTapCloseButton:(UIButton *)sender {
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}

-(void) setRoom:(Room *)room {
    _room = room;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _room.conference.publishers.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    PublishersTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:PublishersCellReuseId forIndexPath:indexPath];
    cell.publisher = [_room.conference.publishers objectAtIndex:indexPath.row];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    ConferencePublisher *publisher = [_room.conference.publishers objectAtIndex:indexPath.row];
    if(!publisher.subscribed)
        [[ServicesManager sharedInstance].conferencesManagerService subscribeToVideoSharedByConferencePublisher:publisher inConference:_room.conference];
    else
        [[ServicesManager sharedInstance].conferencesManagerService unsubscribeToVideoSharedBy:publisher inConference:_room.conference];
    [self didTapCloseButton:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:kPublishersDidSelected object:publisher];
}

-(void) didUpdateConference:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didUpdateConference:notification];
        });
        return;
    }
    
    NSDictionary *userInfo = (NSDictionary *)notification.object;
    NSArray<NSString *> *changedKeys = [userInfo objectForKey:kConferenceChangedAttributesKey];
    Conference *aConference = (Conference *)[userInfo objectForKey:kConferenceKey];
    
    if(![aConference isEqual:_room.conference]){
        return;
    }
    
    if([changedKeys containsObject:@"publishers"]){
        if(aConference.publishers.count == 0){
            [self didTapCloseButton:nil];
        } else
            [self.tableView reloadData];
    }
    
}


@end
