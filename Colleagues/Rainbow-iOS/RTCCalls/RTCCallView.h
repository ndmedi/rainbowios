/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import <UIKit/UIKit.h>

#import <WebRTC/WebRTC.h>
#import <Rainbow/RTCService.h>

@class RTCCallView;
@protocol RTCCallViewDelegate <NSObject>

// YES if video is enabled, NO if audio only
- (BOOL) isVideoEnabled;

- (RTCCall *)rtcCall;

// Called when the user answer an incoming call
- (void) rtcCallViewDidAnswer:(RTCCallView *)view;

// Called when the camera switch button is pressed.
- (void) rtcCallViewDidSwitchCamera:(RTCCallView *)view;

// Called when the hangup button is pressed.
- (void) rtcCallViewDidHangup:(RTCCallView *)view;

// Called when the stats button is pressed.
- (void) rtcCallViewDidStats:(RTCCallView *)view;

@end


@interface RTCCallView : UIView

- (instancetype)initWithFrame:(CGRect)frame withDelegate:(id<RTCCallViewDelegate>)delegate with:(Contact *)contact features:(RTCCallFeatureFlags)features;

@property(nonatomic, strong, readonly) RTCCameraPreviewView *localVideoView;
@property(nonatomic, strong, readonly) RTCEAGLVideoView *remoteVideoView;

-(void) answerCall;

@end
