/*
 * Rainbow
 *
 * Copyright (c) 2017, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "Room+Extensions.h"

@implementation Room (Extensions)

-(BOOL) canJoin {
    
    if(self.myStatusInRoom == ParticipantStatusUnsubscribed) {
        [self standardCannotJoinLogWithCause:@"the bubble is archived"];
        return NO;
    }
    
    if([ServicesManager sharedInstance].conferencesManagerService.hasJoinedConference){
        // We couldn't join more than one conference at a time
        [self standardCannotJoinLogWithCause:@"the user is already in a conference"];
        return NO;
    }
    
    if (self.conference.mediaType == ConferenceMediaTypePSTN) {
        if (self.conference.type == ConferenceTypeInstant) {
            if (!self.conference.endpoint) {
                [self standardCannotJoinLogWithCause:@"the conference is a personal meeting with no endpoint"];
                return NO;
            }
            
            return YES;
        } else if (self.conference.type == ConferenceTypeScheduled) {
            if (!self.conference.endpoint) {
                [self standardCannotJoinLogWithCause:@"the scheduled meeting is ended (no endpoint)"];
                return NO;
            }
            
            if ([self.conference.end isEarlierThanDate:[NSDate date]]) {
                [self standardCannotJoinLogWithCause:@"the scheduled meeting is passed"];
                return NO;
            }
            
            return YES;
        } else {
            [self standardCannotJoinLogWithCause:@".... who knows??"];
            return NO; // Should never happen but who can know
        }
    } else { // If no conference or mediatype unknown, consider as webrtc
        if (![ServicesManager sharedInstance].myUser.isAllowedToParticipateInWebRTCMobile) {
            [self standardCannotJoinLogWithCause:@"the user is not allowed to participate"];
            return NO;
        }
        
        if (!self.isAdmin) {
            if (!self.conference.endpoint) {
                [self standardCannotJoinLogWithCause:@"the user is not organizer and there is no endpoint"];
                return NO;
            }
        } else {
            // If the number of participant
            if (self.authorizedParticipantsCount > [ServicesManager sharedInstance].myUser.maxNumberOfWebRTCParticipantsPerRoom) {
                [self standardCannotJoinLogWithCause:[NSString stringWithFormat:@"the number of participants (%@) is higher than the max number allowed (%@).", @(self.authorizedParticipantsCount), @([ServicesManager sharedInstance].myUser.maxNumberOfWebRTCParticipantsPerRoom)]];
                return NO;
            }
            
            if (!self.conference.endpoint && ![ServicesManager sharedInstance].myUser.isAllowedToUseWebRTCTelephonyConference) {
                [self standardCannotJoinLogWithCause:@"the user is not allowed to start a conference"];
                return NO;
            }
        }
        
        return YES;
    }
}

-(void) standardCannotJoinLogWithCause:(NSString *) cause {
    NSString *mediaType = @"webrtc";
    if (self.conference.mediaType == ConferenceMediaTypePSTN)
        mediaType = @"pstnAudio";
    OTCLog(@"[canJoin][%@] Cannot join (roomId %@ roomName %@) because %@.", mediaType, self.rainbowID, [Tools anonymizeString:self.displayName], cause);
}

@end
