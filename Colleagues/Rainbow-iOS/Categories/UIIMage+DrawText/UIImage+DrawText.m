/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "UIImage+DrawText.h"

@implementation UIImage (DrawText)
-(UIImage *)drawText:(NSString*)text textColor:(UIColor *) textColor textFont:(UIFont *) textFont tintColor:(UIColor *) tintColor {
    
    // Compute rect to draw the text inside
    CGSize imageSize = self.size;
    NSDictionary *attr = @{NSForegroundColorAttributeName: textColor, NSFontAttributeName: textFont};
    CGSize textSize = [text sizeWithAttributes:attr];
    CGRect textRect = CGRectMake((imageSize.width - textSize.width)/2, (imageSize.height - textSize.height)/2-2, textSize.width, textSize.height);
    
    // Create the image
    UIGraphicsBeginImageContextWithOptions(imageSize, NO, 0.0f);
    [tintColor set];
    [self drawInRect:CGRectMake(0, 0, imageSize.width, imageSize.height)];
    [text drawInRect:CGRectIntegral(textRect) withAttributes:attr];
    UIImage *resultImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return resultImage;
}
@end
