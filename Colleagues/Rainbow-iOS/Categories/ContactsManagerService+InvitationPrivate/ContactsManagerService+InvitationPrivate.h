/*
 * Rainbow
 *
 * Copyright (c) 2017, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import <Rainbow/Rainbow.h>

@interface Invitation (InvintingUserId)
@property (nonatomic, readonly) NSString *invitingUserId;
@end

typedef void (^ContactsManagerCreateContactWithRainbowID)(Contact * contact);

@interface ContactsManagerService (InvitationPrivate)
-(Invitation *) createOrUpdateInvitation:(InvitationDirection) direction fromDictionary:(NSDictionary *) invitationDict;
-(void) getOrCreateRainbowContactAsynchronouslyWithRainbowID:(NSString *) rainbowID withCompletionHandler:(ContactsManagerCreateContactWithRainbowID) completionHandler;
@end
