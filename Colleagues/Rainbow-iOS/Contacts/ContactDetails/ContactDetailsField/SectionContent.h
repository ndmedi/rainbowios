/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import <Foundation/Foundation.h>

// The different content-cells type allowed
typedef NS_ENUM(NSInteger, SectionContentType) {
    SectionContentTypeJobCompany,
    SectionContentTypePhoneNumber,
    SectionContentTypeEmailAddress,
    SectionContentTypePostalAddress,
    SectionContentTypeWebsite,
    SectionContentTypeStartConversation,
    SectionContentTypeAddInRoster,
    SectionContentTypeInvite,
    SectionContentTypeGroups,
    SectionContentTypeWebRTCAudioCall,
    SectionContentTypeWebRTCVideoCall,
    SectionContentTypeChangePassword,
    SectionContentTypeDeleteMySelf,
    SectionContentTypeServicePlan
};

/**
 * This defines the content of a cell.
 */
@interface SectionContent : NSObject
-(instancetype) initWithContent:(NSObject*) content andType:(SectionContentType) type;
@property (nonatomic) SectionContentType type;
@property (nonatomic) NSObject *content;
@end

