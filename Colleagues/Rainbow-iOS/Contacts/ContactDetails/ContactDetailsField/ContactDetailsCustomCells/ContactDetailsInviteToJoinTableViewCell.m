/*
 * Rainbow
 *
 * Copyright (c) 2017, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "ContactDetailsInviteToJoinTableViewCell.h"
#import <Rainbow/ServicesManager.h>
#import "UITools.h"

@interface ContactDetailsInviteToJoinTableViewCell ()
@property (weak, nonatomic) IBOutlet UIButton *inviteToJoinButton;

@end

@implementation ContactDetailsInviteToJoinTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    _inviteToJoinButton.tintColor = [UITools defaultTintColor];
    [UITools applyCustomFontTo:_inviteToJoinButton.titleLabel];
    [_inviteToJoinButton setTitleColor:[UITools defaultTintColor] forState:UIControlStateNormal];
    [_inviteToJoinButton setTitle:NSLocalizedString(@"Invite to join Rainbow", nil) forState:UIControlStateNormal];
}

-(CGFloat) computedHeight {
    // Hide cell for guest user
    if ([ServicesManager sharedInstance].myUser.isGuest){
        return 0.0f;
    }else
        return 60.0f;
}

@end
