/*
 * Rainbow
 *
 * Copyright (c) 2017, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "ContactRainbowActionsCell.h"
#import "UITools.h"

@interface ContactRainbowActionsCell ()

@end

@implementation ContactRainbowActionsCell

- (void)awakeFromNib {
    [super awakeFromNib];
    _chatButton.tintColor = [UITools defaultTintColor];
    _audioCallButton.tintColor = [UITools defaultTintColor];
    _videoCallButton.tintColor = [UITools defaultTintColor];
    
    _chatButton.layer.cornerRadius= _chatButton.frame.size.width/2.0;
    _chatButton.layer.borderWidth = 1.5f;
    _chatButton.layer.borderColor = [UITools defaultTintColor].CGColor;
    UIImage *chat = [UITools imageWithImage:[UIImage imageNamed:@"chat"] scaledToSize:CGSizeMake(20, 20)];
    [_chatButton setImage:[chat imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    
    _audioCallButton.layer.cornerRadius= _audioCallButton.frame.size.width/2.0;
    _audioCallButton.layer.borderWidth = 1.5f;
    _audioCallButton.layer.borderColor = [UITools defaultTintColor].CGColor;
    UIImage *audio = [UITools imageWithImage:[UIImage imageNamed:@"Phone"] scaledToSize:CGSizeMake(20, 20)];
    [_audioCallButton setImage:[audio imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];

    _videoCallButton.layer.cornerRadius= _videoCallButton.frame.size.width/2.0;
    _videoCallButton.layer.borderWidth = 1.5f;
    _videoCallButton.layer.borderColor = [UITools defaultTintColor].CGColor;
    UIImage *video = [UITools imageWithImage:[UIImage imageNamed:@"video"] scaledToSize:CGSizeMake(20, 20)];
    [_videoCallButton setImage:[video imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
}

-(CGFloat) computedHeight {
    return 70;
}

-(void) setContact:(Contact *)contact {
    _contact = contact;
    
    //
    _audioCallButton.enabled = [_contact canCallInWebRTC];
    _audioCallButton.layer.borderColor = (_audioCallButton.enabled) ? [UITools defaultTintColor].CGColor : [UIColor lightGrayColor].CGColor;
    _audioCallButton.tintColor = (_audioCallButton.enabled) ? [UITools defaultTintColor] : [UIColor grayColor];
    
    _videoCallButton.enabled = [_contact canCallInWebRTCVideo];
    _videoCallButton.layer.borderColor = (_audioCallButton.enabled) ? [UITools defaultTintColor].CGColor : [UIColor lightGrayColor].CGColor;
    _videoCallButton.tintColor = (_audioCallButton.enabled) ? [UITools defaultTintColor] : [UIColor grayColor];
}

-(void) setCellButtonTapHandler:(ActionButtonTapHandler)cellButtonTapHandler {
    _cellButtonTapHandler = cellButtonTapHandler;
}

- (IBAction)didTapButton:(UIButton *)sender {
    if( _cellButtonTapHandler ) {
        if([sender isEqual:_chatButton] ) {
            _cellButtonTapHandler(ActionButtonTypeChat);
        } else if( [sender isEqual:_audioCallButton]) {
            _cellButtonTapHandler(ActionButtonTypeAudioCall);
        } else if( [sender isEqual:_videoCallButton]) {
            _cellButtonTapHandler(ActionButtonTypeVideoCall);
        }
    }
}

@end
