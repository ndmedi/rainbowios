/*
 * Rainbow
 *
 * Copyright (c) 2017, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import <UIKit/UIKit.h>
#import "Contact+Extensions.h"

typedef enum {
    ActionButtonTypeChat,
    ActionButtonTypeAudioCall,
    ActionButtonTypeVideoCall
} ActionButtonType;

#define kContactRainbowActionsCell @"contactRainbowActionsCell"

typedef void(^ActionButtonTapHandler)(ActionButtonType type);

@interface ContactRainbowActionsCell : UITableViewCell
@property (nonatomic, strong) Contact *contact;
@property (nonatomic, strong) ActionButtonTapHandler cellButtonTapHandler;
@property (weak, nonatomic) IBOutlet UIButton *chatButton;
@property (weak, nonatomic) IBOutlet UIButton *audioCallButton;
@property (weak, nonatomic) IBOutlet UIButton *videoCallButton;
-(CGFloat) computedHeight;
@end
