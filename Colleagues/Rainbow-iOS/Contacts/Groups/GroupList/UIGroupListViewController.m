/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "UIGroupListViewController.h"
#import "UITools.h"
#import "UIGroupsTableViewCell.h"
#import <Rainbow/GroupsService.h>
#import "OrderedOptionalSectionedContent.h"
#import "BGTableViewRowActionWithImage.h"
#import "GoogleAnalytics.h"
#import "UIGroupDetailsAddMembersTableViewCell.h"

#define kCreateGroupSection @"Create a list"
#define kCurrentGroupsSection @"Current lists"
#define kAvailableGroupsSection @"Available lists"

@interface UIGroupListViewController ()
@property (nonatomic, strong) OrderedOptionalSectionedContent<Group*> *groupList;
@property (nonatomic, strong) UIAlertAction *saveAction;
@end

@implementation UIGroupListViewController

-(void) awakeFromNib {
    [super awakeFromNib];
    [self sortUI];
    self.searchOnServer = NO;
    
    _groupList = [[OrderedOptionalSectionedContent alloc] initWithSections:@[kCreateGroupSection,kCurrentGroupsSection, kAvailableGroupsSection]];
    
    Group *createGroup = [[Group alloc] init];
    [createGroup setValue:NSLocalizedString(kCreateGroupSection, nil) forKey:@"name"];
    [_groupList addObject:createGroup toSection:kCreateGroupSection];
    
    [self.servicesManager.groupsService.groups enumerateObjectsUsingBlock:^(Group * group, NSUInteger idx, BOOL * stop) {
        [_groupList addObject:group toSection:kAvailableGroupsSection];
    }];
    NSArray<Group *> *content = [_groupList sectionForKey:kAvailableGroupsSection];
    [self.objects addObjectsFromArray:content];
}

-(void) dealloc {
    _saveAction = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kGroupsServiceDidAddGroup object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kGroupsServiceDidUpdateGroup object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kGroupsServiceDidRemoveGroup object:nil];
}

-(void) viewDidLoad {
    [super viewDidLoad];
    self.title = NSLocalizedString(@"Add to list", nil);
    self.tokenInputView.fieldName = @"";

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didAddGroup:) name:kGroupsServiceDidAddGroup object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didUpdateGroup:) name:kGroupsServiceDidUpdateGroup object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didRemoveGroup:) name:kGroupsServiceDidRemoveGroup object:nil];
}

-(void) setGroups:(NSArray<Group *> *)groups {
    _groups = groups;
    
    [_groups enumerateObjectsUsingBlock:^(Group * group, NSUInteger idx, BOOL * stop) {
        [_groupList addObject:group toSection:kCurrentGroupsSection];
        
        if([_groupList containsObject:group inSection:kAvailableGroupsSection]){
            [_groupList removeObject:group fromSection:kAvailableGroupsSection];
        }
    }];
    
    [self.objects removeAllObjects];
    NSArray<Group *> *content = [_groupList sectionForKey:kAvailableGroupsSection];
    [self.objects addObjectsFromArray:content];
    
    [self sortUI];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if(self.isTyping)
        return 1;
    return [_groupList allNotEmptySections].count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(self.isTyping){
        return [self.filteredObjects count];
    } else {
        NSString *key = [[_groupList allNotEmptySections] objectAtIndex:section];
        return [[_groupList sectionForKey:key] count];
    }
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    NSString *key = [[_groupList allNotEmptySections] objectAtIndex:section];
    if([key isEqualToString:kCreateGroupSection])
        return @"";
    return NSLocalizedString([[_groupList allNotEmptySections] objectAtIndex:section], nil);
}

- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section {
    UITableViewHeaderFooterView *tableViewHeaderFooterView = (UITableViewHeaderFooterView *)view;
    tableViewHeaderFooterView.textLabel.font = [UIFont fontWithName:[UITools boldFontName] size:14.0f];
    tableViewHeaderFooterView.textLabel.textColor = [UITools defaultTintColor];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(self.isTyping){
        UIGroupsTableViewCell *cell = (UIGroupsTableViewCell*)[tableView dequeueReusableCellWithIdentifier:kGroupsCellTableViewReusableKey forIndexPath:indexPath];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.group = [self.filteredObjects objectAtIndex:indexPath.row];
        return cell;
    } else {
        UITableViewCell *cell = nil;
        
        NSString *key = [[_groupList allNotEmptySections] objectAtIndex:indexPath.section];
        
        if([key isEqualToString:kCreateGroupSection]) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"groupDetails"];
            cell.textLabel.text = NSLocalizedString(@"Create a list", nil);
            cell.imageView.image = [UIImage imageNamed:@"Add"];
            cell.imageView.tintColor = [UITools defaultTintColor];
            cell.selectionStyle = UITableViewCellSelectionStyleDefault;
            cell.contentView.backgroundColor = [UITools defaultBackgroundColor];
            cell.backgroundColor = [UITools defaultBackgroundColor];

        } else {
            cell = (UIGroupsTableViewCell*)[tableView dequeueReusableCellWithIdentifier:kGroupsCellTableViewReusableKey forIndexPath:indexPath];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            NSArray<Group *> *content = [_groupList sectionForKey:key];
            ((UIGroupsTableViewCell*)cell).group = [content objectAtIndex:indexPath.row];
        }
        
        return cell;
    }
}

-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *key = [[_groupList allNotEmptySections] objectAtIndex:indexPath.section];
    
    if([key isEqualToString:kCurrentGroupsSection])
        return YES;
    return NO;
}

-(NSArray<UITableViewRowAction *> *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *key = [[_groupList allNotEmptySections] objectAtIndex:indexPath.section];
    NSArray<Group *> *content = [_groupList sectionForKey:key];
    Group * selectedGroup = [content objectAtIndex:indexPath.row];
    
    BGTableViewRowActionWithImage *action = [BGTableViewRowActionWithImage rowActionWithStyle:UITableViewRowActionStyleDestructive title:NSLocalizedString(@"Remove", nil) backgroundColor:[UITools redColor] image:[UIImage imageNamed:@"RemoveAttendees"] forCellHeight:kRainbowGenericTableViewCellHeight+10 handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0), ^{
            [[ServicesManager sharedInstance].groupsService removeContact:_contact fromGroup:selectedGroup];
            [[GoogleAnalytics sharedInstance] dispatchEventWithCategory:@"group" action:@"remove member" label:nil value:nil];
        });
    }];
    return @[action];
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [super tableView:tableView didSelectRowAtIndexPath:indexPath];
    if(self.isTyping){
        Group *group = [self.filteredObjects objectAtIndex:indexPath.row];
        [_groupList removeObject:group fromSection:kAvailableGroupsSection];
        CLToken *token = [[CLToken alloc] initWithDisplayText:group.name context:group];
        [self.tokenInputView addToken:token];
        [tableView reloadData];
        return;
    }
    
    NSString *key = [[_groupList allNotEmptySections] objectAtIndex:indexPath.section];
    if( [key isEqualToString:kCurrentGroupsSection] )
        return;
    
    if( [key isEqualToString:kCreateGroupSection]  ) {
        [self onAccessoryContactAddButtonTapped:nil];
        return;
    }
    
    else {
        Group *selectedGroup = nil;
        if([key isEqualToString:kAvailableGroupsSection]){
            NSArray<Group *> *content = [_groupList sectionForKey:key];
            selectedGroup = [content objectAtIndex:indexPath.row];
        }
        CLToken *token = [[CLToken alloc] initWithDisplayText:selectedGroup.name context:selectedGroup];
        [self.tokenInputView addToken:token];
        
        if([key isEqualToString:kAvailableGroupsSection]){
            [_groupList removeObject:selectedGroup fromSection:kAvailableGroupsSection];
        }
    }
    
    [tableView reloadData];
}

-(NSPredicate *) predicateForText:(NSString *) text {
    return [NSPredicate predicateWithFormat:@"name contains[cd] %@", text];
}

-(NSString *) textForEmptyView {
    return NSLocalizedString(@"No list found",nil);
}

-(void) sortUI {
    NSArray <NSString *> *keys = [_groupList allNotEmptySections];
    
    for (NSString *aKey  in keys) {
        NSMutableArray<Group *> *content = [_groupList sectionForKey:aKey];
        [content sortUsingDescriptors:@[[UITools sortDescriptorForGroupByName]]];
    }
}

-(BOOL) validateUIContent {
    BOOL isValid = YES;
    if(self.tokenInputView.allTokens.count == 0)
        return NO;
    
    return isValid;
}

- (void)onAccessoryContactAddButtonTapped:(id)sender {
    
    UIAlertController *newNameAlert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Enter new list name", nil) message:nil preferredStyle:UIAlertControllerStyleAlert];
    [newNameAlert addTextFieldWithConfigurationHandler:^(UITextField * textField) {
        [UITools applyCustomFontToTextField:textField];
        [textField addTarget:self action:@selector(listNameTextFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    }];
    
    _saveAction = nil;
    _saveAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        UITextField *field = [newNameAlert.textFields firstObject];
        [field removeTarget:nil action:nil forControlEvents:UIControlEventEditingChanged];
        NSString *newName = field.text;
        NSLog(@"NEW NAME %@",newName);
        
        MBProgressHUD *hud = [self showHUDWithMessage:NSLocalizedString(@"Creating new list ...", nil) mode:MBProgressHUDModeIndeterminate dismissCompletionBlock:nil];
        [hud showAnimated:YES whileExecutingBlock:^{
            [[ServicesManager sharedInstance].groupsService createGroupWithName:newName andComment:nil];
        } completionBlock:^{
            [self showHUDWithMessage:NSLocalizedString(@"List created",nil) mode:MBProgressHUDModeCustomView dismissCompletionBlock:nil];
            [[GoogleAnalytics sharedInstance] dispatchEventWithCategory:@"group" action:@"create" label:nil value:nil];
        }];
    }];
    
    _saveAction.enabled = NO;
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction * action) {
        UITextField *field = [newNameAlert.textFields firstObject];
        [field removeTarget:nil action:nil forControlEvents:UIControlEventEditingChanged];
    }];
    
    [newNameAlert addAction:_saveAction];
    [newNameAlert addAction:cancel];
    [newNameAlert.view setTintColor:[UITools defaultTintColor]];
    [self presentViewController:newNameAlert animated:YES completion:nil];
}

- (void)listNameTextFieldDidChange:(UITextField *)textField {
    _saveAction.enabled = (textField.text.length >= 3);
}

-(void) didAddGroup:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didAddGroup:notification];
        });
        return;
    }
    
    Group *group = (Group *)notification.object;
    if(![_groupList containsObject:group inSection:kAvailableGroupsSection]){
        [_groupList addObject:group toSection:kAvailableGroupsSection];
        [self sortUI];
        [self.tableView reloadData];
    }
}

-(void) didUpdateGroup:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didUpdateGroup:notification];
        });
        return;
    }
    
    Group *group = (Group *)notification.object;
    if([group.users containsObject:_contact]){
        // This group contain our contact so adjust the UI
        if(![_groupList containsObject:group inSection:kCurrentGroupsSection]){
            [_groupList addObject:group toSection:kCurrentGroupsSection];
        }
        if([_groupList containsObject:group inSection:kAvailableGroupsSection]){
            [_groupList removeObject:group fromSection:kAvailableGroupsSection];
        }
    } else {
        // We must also check if the current contact was in this group
        if([_groupList containsObject:group inSection:kCurrentGroupsSection]){
            [_groupList removeObject:group fromSection:kCurrentGroupsSection];
        }
        if(![_groupList containsObject:group inSection:kAvailableGroupsSection]){
            [_groupList addObject:group toSection:kAvailableGroupsSection];
        }
    }
    
    [self.objects removeAllObjects];
    NSArray<Group *> *content = [_groupList sectionForKey:kAvailableGroupsSection];
    [self.objects addObjectsFromArray:content];
    
    [self sortUI];
    [self.tableView reloadData];
}

-(void) didRemoveGroup:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didRemoveGroup:notification];
        });
        return;
    }
    
    Group *group = (Group *)notification.object;
    if(![_groupList containsObject:group inSection:kAvailableGroupsSection]){
        [_groupList removeObject:group fromSection:kAvailableGroupsSection];
    }
    if(![_groupList containsObject:group inSection:kCurrentGroupsSection]){
        [_groupList removeObject:group fromSection:kCurrentGroupsSection];
    }
    
    [self sortUI];
    [self.tableView reloadData];
}

- (IBAction)doneButtonClicked:(UIBarButtonItem *)sender {
    [super doneButtonClicked:sender];
    [self addContact:_contact toGroups:self.tokenInputView.allTokens];
}

-(void) addContact:(Contact *) contact toGroups:(NSArray<CLToken *> *) groups{
    MBProgressHUD *hud = [self showHUDWithMessage:NSLocalizedString(@"Adding to groups ...",nil) mode:MBProgressHUDModeIndeterminate dismissCompletionBlock:nil];
    
    [hud showAnimated:YES whileExecutingBlock:^{
        [groups enumerateObjectsUsingBlock:^(CLToken * aToken, NSUInteger idx, BOOL * stop) {
            [[ServicesManager sharedInstance].groupsService addContact:contact inGroup:(Group *)aToken.context];
        }];
    } completionBlock:^{
        [self showHUDWithMessage:NSLocalizedString(@"Done",nil) mode:MBProgressHUDModeCustomView dismissCompletionBlock:^{
            [self.navigationController dismissViewControllerAnimated:YES completion:nil];
        }];
        [[GoogleAnalytics sharedInstance] dispatchEventWithCategory:@"group" action:@"add member" label:nil value:nil];
    }];
}

- (void)tokenInputView:(CLTokenInputView *)view didRemoveToken:(CLToken *)token {
    [_groupList addObject:(Group*)token.context toSection:kAvailableGroupsSection];
    [super tokenInputView:view didRemoveToken:token];
}

@end
