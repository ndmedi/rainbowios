/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "UIContactsTableViewCell.h"

#import "UITools.h"
#import "UIImageView+Letters.h"
#import <Rainbow/EmailAddress.h>
#import "Contact+Extensions.h"
#import <QuartzCore/QuartzCore.h>
#import <Rainbow/ServicesManager.h>
#import "UIContactsTableViewCell+Internal.h"

static int contactNameCenterConstraintDefaultValue = -15;

@implementation UIContactsTableViewCell

-(void) awakeFromNib {
    [super awakeFromNib];
    _avatar.asCircle = YES;
    [UITools applyCustomFontTo:_contactName];
    [UITools applyCustomFontTo:_companyName];
    [_vcardButton setTintColor:[UITools defaultTintColor]];
    [_vcardButton setTitleColor:[UITools defaultTintColor] forState:UIControlStateNormal];
    [_vcardButton setTitleColor:[UITools defaultTintColor] forState:UIControlStateHighlighted];
    [UITools applyCustomFontTo:_vcardButton.titleLabel];
    
    [self setTintColor:[UITools defaultTintColor]];
    self.contentView.backgroundColor = [UITools defaultBackgroundColor];
    self.backgroundColor = [UITools defaultBackgroundColor];
    _vcardButton.contentEdgeInsets = UIEdgeInsetsMake(0, -20, 0, -20);
}

-(void) prepareForReuse {
    [super prepareForReuse];
    _companyName.text = nil;
    _companyName.hidden = NO;
    _contactNameCenterConstraint.constant = contactNameCenterConstraintDefaultValue;
    [_vcardButton setImage:[UIImage imageNamed:@"Vcard"] forState:UIControlStateNormal];
    [_vcardButton setImage:[UIImage imageNamed:@"Vcard"] forState:UIControlStateHighlighted];
    [_vcardButton setTitle:nil forState:UIControlStateNormal];
    [_vcardButton setTitle:nil forState:UIControlStateHighlighted];
}

-(void) dealloc {
    if(_contact){
        [_contact removeObserver:self forKeyPath:kContactLastNameKey context:nil];
        [_contact removeObserver:self forKeyPath:kContactFirstNameKey context:nil];
        _contact = nil;
    }
}

-(void) setContact:(Contact *)contact {
    if(_contact != contact){
        [_contact removeObserver:self forKeyPath:kContactLastNameKey context:nil];
        [_contact removeObserver:self forKeyPath:kContactFirstNameKey context:nil];
        _contact = nil;
        _contact = contact;
        [_contact addObserver:self forKeyPath:kContactLastNameKey options:NSKeyValueObservingOptionInitial|NSKeyValueObservingOptionNew context:nil];
        [_contact addObserver:self forKeyPath:kContactFirstNameKey options:NSKeyValueObservingOptionInitial|NSKeyValueObservingOptionNew context:nil];
    }
    
    _avatar.peer = _contact;
    _contactName.text = _contact.displayName;
    _companyName.text = contact.companyName;
    
    [self adjustLayout];
    if(!_contact.canChatWith && ([_contact.emailAddresses count] || [_contact.phoneNumbers count])) {
        [_vcardButton setImage:nil forState:UIControlStateNormal];
        [_vcardButton setImage:nil forState:UIControlStateHighlighted];
        if (contact.sentInvitation && contact.sentInvitation.status == InvitationStatusPending) {
            [_vcardButton setTitle:NSLocalizedString(@"Pending", nil) forState:UIControlStateNormal];
        } else if(contact.sentInvitation && contact.sentInvitation.status == InvitationStatusFailed) {
            [_vcardButton setTitle:NSLocalizedString(@"Failed", nil) forState:UIControlStateNormal];
        } else {
            [_vcardButton setTitle:NSLocalizedString(@"Invite", nil) forState:UIControlStateNormal];
        }
    } else {
        [_vcardButton setImage:[UIImage imageNamed:@"Vcard"] forState:UIControlStateNormal];
        [_vcardButton setImage:[UIImage imageNamed:@"Vcard"] forState:UIControlStateHighlighted];
        [_vcardButton setTitle:nil forState:UIControlStateNormal];
        [_vcardButton setTitle:nil forState:UIControlStateHighlighted];
    }
}

-(void) adjustLayout {
    if(_contact.displayName.length > 0){
        if(_contact.companyName.length > 0 && ![_contact.companyName isEqualToString:_contact.displayName]){
            _contactNameCenterConstraint.constant = contactNameCenterConstraintDefaultValue;
            _companyName.hidden = NO;
        } else {
            _contactNameCenterConstraint.constant = 0;
            _companyName.hidden = YES;
        }
    }
}

-(void) observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context {
    if([keyPath isEqualToString:kContactLastNameKey] || [keyPath isEqualToString:kContactFirstNameKey]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            _contactName.text = _contact.displayName;
            [self adjustLayout];
        });
    }
}

-(void) setSelected:(BOOL)selected animated:(BOOL)animated{
    [super setSelected:selected animated:animated];
    [_avatar setSelected:selected animated:animated];
}

-(void) setHighlighted:(BOOL)highlighted animated:(BOOL)animated {
    [super setHighlighted:highlighted animated:animated];
    [_avatar setHighlighted:highlighted animated:animated];
}

@end
