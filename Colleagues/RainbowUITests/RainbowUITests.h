/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import <XCTest/XCTest.h>
#import <RainbowUnitTestFakeServer/RainbowUnitTestFakeServer.h>

#define FAKE_SERVER_HOST @"localhost"
#define FAKE_SERVER_PORT 6670


@interface XCUIElement (PrivateAPI)
-(unsigned long long) traits;
-(BOOL) isDisabled;
-(BOOL) isEnabled;
-(BOOL) isHeader;
@end

@implementation XCUIElement (PrivateAPI)
-(BOOL) isDisabled {
    return YES;
}
-(BOOL) isEnabled {
    return ![self isDisabled];
}
-(BOOL) isHeader {
    return self.traits & UIAccessibilityTraitHeader;
}
@end

@interface XCUIApplication(FromBundleID)
-(void) tapElementAndWaitForKeyboardToAppear:(XCUIElement *) element;
@end


/*
 * Our super class for all Rainbow UI Tests
 */
@interface RainbowUITests : XCTestCase

@property (nonatomic, strong) XCUIApplication *app;

-(Class) server;

-(void) addLocalContactFirstname:(NSString *) first lastname:(NSString *) last email:(NSString *) email;

-(void) linkLocalContactName:(NSString *) firstlast with:(NSString *) withfirstlast;

-(void) removeLocalContacts;

-(void) launchApplication;

-(void) launchApplicationWithoutAutoLogin;

-(void) putAppToBackground;

-(void) putAppToForeground;

-(void) startServer;

-(void) stopServer;

@end

