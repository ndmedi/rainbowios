# Rainbow iOS Change Log

## [1.58.1 build 451] - [2019-06-21]

### Fixed
* CRRAINB-5148 : Missing chat messages when opening a conversation
* CRRAINB-6139 : Zendesk#25334 // Inconsistency between calling a busy party or a wrong number from iOS in terms of error message
* CRRAINB-6651 : No notification when Rainbow in background
* CRRAINB-6725 : App crash upon searching for participant name in bubbles and list
* CRRAINB-6744 : Zendesk#26236 // Constant crash on iOS
* CRRAINB-4188 : Zendesk#21282 // Sync of message doesn't occur
* CRRAINB-4276 : Messages not visible in the iOS
* CRRAINB-4415 : Zendesk#21803 // No IMs synchronization between Desktop and iOS 
* CRRAINB-4511 : User's messages not displayed
* CRRAINB-4515 : Zendesk#21691 // Voicemail call not working
* CRRAINB-4969 : Zendesk#22836 // Bad synchronization of IMs
* CRRAINB-6576 : Conference control buttons do not appear directly when user start conference
* CRRAINB-6578 : Organizer and attendee are both muted when user mute a specific attendee
* CRRAINB-6625 : Received messages appear as outgoing messages when  you switch account/users
* CRRAINB-6626 : Zendesk#26038 // Error message "You need to be connected to your network to make calls" while in 4G
* CRRAINB-6629 : Conversation not updated directly  when user share file from share extension
* CRRAINB-6109 : Zendesk#25149 // Contacts from pbx phonebook not displayed
* CRRAINB-6363 : Sent .ppt files  have  "?" icon /thumbnail
* CRRAINB-6628 : Wrong last sent message in conversation list when image failed to sent
* CRRAINB-6695 : Zendesk#26156 // Paper clip icon grayed
* CRRAINB-6505 : Bad UI during screen rotation
* CRRAINB-6582 : Change error message when user login with invalid email address
* CRRAINB-6645 : Change invalid code error to "Invalid or expired code" (same as web behavior)
* CRRAINB-6693 : I see only the first favoris for 2/3 seconds
* RBIOS-92 : [RainbowTV] The text when there is no equipment is not translated
* RBIOS-93 : [RainbowTV] Translation of the string "Rainbow TVs"
* RBIOS-94 : [RainbowTV] Hide the "Add:" part if there is no RainbowTV to add
* RBIOS-95 : [RainbowTV] The font used when there is no Rainbow TV is not correct
* RBIOS-96 : [RainbowTV] Remove actions for Rainbow TV users
* [RainbowTV] Do not display "nil" if the Rainbow TV does not have location or locationDetail information

### Restriction
* Messages status delivered to user and read by user are not displayed for bubbles conversations
* Push : Notifications are not removed from iPhone screen when message is read from another device
* Push : No notifications of invitation to join network
* Entries in Rainbow menu displayed in contact integration can display entries twice

### Develop notes
* The encoding for some URL parameters has been added back to create the NSURL object
* The urlEncode method now replaces the space by %20 instead of +
* Location : display response from server if error + manage error 403630 and 403000 to display correct popup
* Push : custom body for location, GIF and file

## [1.58.0 build 450] - [2019-06-14]

### Added
* RBIOS-85 : Do not send "join room" in case of stream resumed (successfully)
* RBIOS-83 : [UX/UI] Add a Rainbow TV in a bubble
* RBIOS-80 : [UX/UI] Increase audio conference capacity up to 30
* RBIOS-78 : [UX/UI] Suppress the capability to schedule meetings
* RBIOS-77 : [Robustness] Limit favorites using flag FAVORITE_CONVERSATIONS_COUNT
* RBIOS-74 : [Robustness] Encode REST GET requests
* RBIOS-70 : [Performance] Add type=im when sending receipt
* [Channels] The audience and the confidentiality are now displayed in readonly when modifying a channel
* [Channels] A restricted channel is now marked with a locker in the "My channels" tab

### Fixed
* CRRAINB-5952 : Numbering rules should not apply on Rainbow 17 digits numbers
* CRRAINB-6526 : Bad management of the popup indication after sharing the location
* CRRAINB-6220 : Disable publish button in channel when network is off
* CRRAINB-6363 : Sent .ppt files have "?" icon as thumbnail
* CRRAINB-6428 : Bad UI for buttons location on iPhone X
* CRRAINB-6477 : Can't modify post in a channel by adding files from iCloud
* CRRAINB-6480 : Remove post options (Delete, Modify) for a demoted publisher role as web client 
* CRRAINB-6484 : No results when user search for a "word + space"
* CRRAINB-6566 : No avatar nor name displayed when receiving IM from contact who never contacts me before
* CRRAINB-6587 : Conference appears active when user clicks call button in an inactive bubble
* CRRAINB-6609 : Search result returns already added contact while creating a channel
* CRRAINB-6398 : Different UI between iPhone X and iPhone 8
* CRRAINB-6482 : User should see same words when he wants to leave a bubble
* CRRAINB-6490 : Remove/disable "Send conversation by email" option if there is no previous conversation
* CRRAINB-6567 : After promote the user to publisher role the modification isn't dynamically done
* CRRAINB-6597 : Search result returns already added contact while creating a list of contacts
* The click on a GIF doesn't work anymore
* The popup for inactive bubbles was not always displayed (especially when we load the rooms from the cache)
* Crash on start (wrong parsing of image in a channel item)

### Modified
* [WebRTC] Change RTCPeerConnection settings (RTCPeerConnectionFactory and RTCConfiguration) to be closer to other clients settings

### Restriction
* Messages status delivered to user and read by user are not displayed for bubbles conversations
* Push : Notifications are not removed from iPhone screen when message is read from another device
* Push : No notifications of invitation to join network
* Entries in Rainbow menu displayed in contact integration can display entries twice

## [1.57.2 build 449] - [2019-06-13]

### Added
* RBIOS-75 : [UX/UI] Put the button "share my localization" behind a feature flag

### Removed
* [Geoloc] The permission to have always the location has been removed (only "App active" is required)

### Fixed
* CRRAINB-6548 : Crash of the application after every start
* The app cannot be used on a AiO if the conference portal is not reachable
* [Channels] The confirmation popup when deleting a post has been updated

### Developer notes
* The profile SHARE_CURRENT_LOCATION is now also checked inside of the SDK

### Restriction
* Messages status delivered to user and read by user are not displayed for bubbles conversations
* Push : Notifications are not removed from iPhone screen when message is read from another device
* Push : No notifications of invitation to join network
* Entries in Rainbow menu displayed in contact integration can display entries twice

## [1.57.1 build 448] - [2019-05-29]

### Added
* RBIOS-20 : RQ1483 - As a user, I want to change the order of favorites using drag and drop in order to organize my list of favorites
* RBIOS-21 : RQ1483 - As a user, I want to drag and drop a favorite out of my list of favorites in order to remove it from that list
* RBIOS-8 : RQ1507 - As a user, I want to share my location with someone by taping on the user action button “share my location” , authorizing the app to access it and by sending a message to the conversation in order for my recipient to * receive the coordinates
* RBIOS-62 : As a user, I want to see a red bullet over a favorite item in order to inform me that I have unread message in the associated conversation
* RBIOS-53 : [XMPP/REST] Generate and send localization preview #DimensionData
* RBIOS-54 : [UX/UI] Click on map preview and open map application #DimensionData
* CRRAINB-6420 : [Geoloc] Restrict the feature for some profiles only

### Fixed
* CRRAINB-6443 : [PGI] Crash when clicking on "Call me" tab
* CRRAINB-6419 : [Geoloc] The body of the message is "Location" instead of the filename
* CRRAINB-6493 : [Geoloc] It is not possible to create a path when opened in Google Maps

### Modified
* CRRAINB-6421 : [Geoloc] The viewer is added from another request than /notify
* CRRAINB-6423 : [Geoloc] Push the Maps view controller instead of presenting
* CRRAINB-6436 : [Geoloc] The 2 buttons "Share" and "Zoom" are too bold
* CRRAINB-6437 : [Geoloc] Put a spinner instead of the default preview image
* CRRAINB-6439 : [Geoloc] Enhance header view of the Maps view controller
* CRRAINB-6440 : [Geoloc] Change the value of the zoom in the Maps view controller
* CRRAINB-6476 : [Geoloc] Enhance the code
* [Geoloc] Strings have been updated + Add in french
* [Channels] Some strings have been updated
* [Channels] Display button to create a channel in 3 tabs
* [Channels] The actions Modify and Delete channel have been moved to a "More" button
* [Channels] The icon for followed channel in discovering tab is now darker
* [Channels] The border of a channel is now darker (from #E6E6E6 to #BFBFBF)
* [Channels] The message "No post in this channel yet" is not displayed anymore during the loading

### Restriction
* Messages status delivered to user and read by user are not displayed for bubbles conversations
* Push : Notifications are not removed from iPhone screen when message is read from another device
* Push : No notifications of invitation to join network
* Entries in Rainbow menu displayed in contact integration can display entries twice

## [1.57.0 build 446] - [2019-05-23]

### Added
* RQRAINB-1491 : Create, modify and suppress channels

### Changed
* RQRAINB-1236 : Update of WebRTC stack

### Fixed
* CRRAINB-4882 : Wrong push notifications are reveived upon starting/end conference
* CRRAINB-6129 : Bubble is not deleted correctly from conversations list
* CRRAINB-6326 : Remove message options for deleted file currently its crashing the app
* CRRAINB-6383 : Wrong end of conference message push
* CRRAINB-6389 : User can't scroll screen while adding participants in bubble

### Developer notes
* First step of RQRAINB-1507 : Share your location while in a conversation (not finished)
* CRRAINB-6174 : Start web conference from other device and check notification on Android (iOS side ok, now waiting for server side)
* [Channels] The background color of the items screen is now gray (default table view color)
* [Contacts] The sort by firstname was not working in the Contacts tab

### Restriction
* Messages status delivered to user and read by user are not displayed for bubbles conversations
* Push : Notifications are not removed from iPhone screen when message is read from another device
* Push : No notifications of invitation to join network
* Entries in Rainbow menu displayed in contact integration can display entries twice

## [1.56.4 build 447] - [2019-05-29]

### Fixed
* CRRAINB-6483 : XMPP server crash on resume

### Restriction
* Messages status delivered to user and read by user are not displayed for bubbles conversations
* Push : Notifications are not removed from iPhone screen when message is read from another device
* Push : No notifications of invitation to join network
* Entries in Rainbow menu displayed in contact integration can display entries twice

## [1.56.3 build 445] - [2019-05-23]

### Changed
* CRRAINB-6400 : Stuck in busy audio state (Avoid sending CSI active element on resume)

### Restriction
* Messages status delivered to user and read by user are not displayed for bubbles conversations
* Push : Notifications are not removed from iPhone screen when message is read from another device
* Push : No notifications of invitation to join network
* Entries in Rainbow menu displayed in contact integration can display entries twice

## [1.56.2 build 444] - [2019-05-22]

### Fixed
* CRRAINB-6005 : User received double push notification for last message
* CRRAINB-6305 : [SSO] The feature isn't working (connection done on the wrong user)
* CRRAINB-6336 : Zendesk#25578 // Reproducible crash if contact is searched in Channels tab
* CRRAINB-6335 : IM not sent
* CRRAINB-6341 : Zendesk#25572 // Link to Terms of service / Privacy Statement not working if Legal & Copyright page viewed before
* CRRAINB-6346 : "Error while removing the file" popup appears everytime user tries to delete a file
* CRRAINB-6364 : Zendesk#25608 // Crash of iOS after log in
* CRRAINB-6327 : Read and Received ack dates are null

### Restriction
* Messages status delivered to user and read by user are not displayed for bubbles conversations
* Push : Notifications are not removed from iPhone screen when message is read from another device
* Push : No notifications of invitation to join network
* Entries in Rainbow menu displayed in contact integration can display entries twice

## [1.56.1 build 443] - [2019-05-16]

### Fixed
* CRRAINB-5997 : IM received and sent with wrong timestamp (1.55.2.437 iOS)
* CRRAINB-6162 : Messages appear as null when its a reply to an image from web
* CRRAINB-6163 : App is stuck if call is suspended while in connecting mode
* CRRAINB-6181 : Zendesk#25348 // Message does not show up at the right position
* CRRAINB-6200 : impossible to create a bubble after restart when the application was killed (by swipping)
* CRRAINB-6243 : New published posts does not appear at top of News list
* CRRAINB-6244 : Post with rainbow sharing attachment is published empty
* CRRAINB-6247 : Remove message options for deleted messages/files
* CRRAINB-6221 : Bad display for attached image in conversation
* CRRAINB-6222 : Adjust height of reply to an image
* CRRAINB-6256 : Modified channel post appears twice
* CRRAINB-6257 : Remove the empty channel screen that appears when user refresh channel posts
* CRRAINB-6260 : Channel post is not removed directly

### Restriction
* Messages status delivered to user and read by user are not displayed for bubbles conversations
* Push : Notifications are not removed from iPhone screen when message is read from another device
* Push : No notifications of invitation to join network
* Entries in Rainbow menu displayed in contact integration can display entries twice

### Developer notes
* [Channels] The failed images are now managed (timeout + handler)

## [1.56.0 build 442] - [2019-05-09]

### Added
* RQRAINB-1238 : Favorite conversations
* RQRAINB-1307 : Optimize startup phase
* RQRAINB-1406 : Dial in/dial out screens of the audio conference service shall be reviewed
* RQRAINB-1405 : Improvements of function Delete last message
* RQRAINB-1407 : Add new posts in channels
* RQRAINB-1408 : Edit/suppress a post in a channel
* RQRAINB-1377 : Discover channels - Have a visual indicator when a channel is already followed
* RQRAINB-1312 : Access to YouTube videos shall be reviewed in channels
* RQRAINB-1315 : CPaaS: Split the Getting Started into two guides
* [Channels] An animated zoom has been added when the user clicks on an image

#### Fixed
* CRRAINB-6062 : Zendesk#25171 // Message without double check when later messages do have them using iOS 1.55.6.441
* CRRAINB-5723 : Unable to unmute during SFU conference (1.54.1.431 iOS)
* CRRAINB-5886 : Remove unfollow option in restricted channels
* CRRAINB-5981 : I still have conversations that remain "hooked"
* CRRAINB-6001 : Wrong new message badge counter 
* CRRAINB-6014 : Unsubscribe option offered for a restricted channel
* CRRAINB-6017 : Click Channel button goes up only per page
* CRRAINB-5386 : Channel members number is not updated in mobile  
* CRRAINB-5501 : "Loading images" blocks UI, user can't click any other screen/icon
* CRRAINB-5995 : Remove "Add to my network" from invited contact conversation screen
* CRRAINB-6023 : Sent message appears twice 
* CRRAINB-6128 : Bubble call appears active after user disconnect from iPhone power button
* CRRAINB-6150 : Zendesk#25343 // The order of conversation is wrong using the 1.55.6.441
* CRRAINB-5792 : Extension number not displayed on iOS
* CRRAINB-5851 : Zendesk#24692 // The Personal mobile numbers and the Work phone, are placed different way in different contacts
* CRRAINB-5866 : 9 channels are displayed instead of 10 in "Most followed" Channels section
* CRRAINB-6002 : Message badge counter should be cleared if opened while network is off
* CRRAINB-6120 : User should be able to create two bubbles with same name, same as Web current behavior

### Restriction
* Messages status delivered to user and read by user are not displayed for bubbles conversations
* Push : Notifications are not removed from iPhone screen when message is read from another device
* Push : No notifications of invitation to join network
* Entries in Rainbow menu displayed in contact integration can display entries twice

### Developer notes
* Anonymize room name when fetching
* Add limit of 20 stars when anonymizing string
* [Channels] The "News" tab only displays items posted less than one month ago

## [1.55.6 build 441] - [2019-04-29]

### Fixed
* CRRAINB-5956 : Not able to access content of ongoing bubble

### Changed
* Revert of CRRAINB-6016 : Wrong message ordering of messages when network is off
* Revert of CRRAINB-5999 : Double message on iOS
* Revert of Last message extraction from Conversation for faster message display

### Restriction
* Messages status delivered to user and read by user are not displayed for bubbles conversations
* Push : Notifications are not removed from iPhone screen when message is read from another device
* Push : No notifications of invitation to join network
* Entries in Rainbow menu displayed in contact integration can display entries twice

## [1.55.5 build 440] - [2019-04-25]

### Fixed
* CRRAINB-6027 : Sent messages have no timestamp

### Restriction
* Messages status delivered to user and read by user are not displayed for bubbles conversations
* Push : Notifications are not removed from iPhone screen when message is read from another device
* Push : No notifications of invitation to join network
* Entries in Rainbow menu displayed in contact integration can display entries twice

## [1.55.4 build 439] - [2019-04-24]

### Fixed
* CRRAINB-6016 : Wrong message ordering of messages when network is off
* [Crash] - Opening a new conversation with contact causes a crash

### Restriction
* Messages status delivered to user and read by user are not displayed for bubbles conversations
* Push : Notifications are not removed from iPhone screen when message is read from another device
* Push : No notifications of invitation to join network
* Entries in Rainbow menu displayed in contact integration can display entries twice

## [1.55.3 build 438] - [2019-04-23]

### Fixed
* CRRAINB-5775 : iOS: names in logs
* CRRAINB-5987 : Zendesk#24946 // IOS Device freeze during an Audio/Video conference (Bubble)
* CRRAINB-4699 : Rainbow 1.50.2.415 iOS freeze and crash
* CRRAINB-5864 : Freeze and crash of iOS
* CRRAINB-5871 : Messages looks sent from my iOS and not seen on my web discussion
* CRRAINB-5877 : After opening the bubble (without IM sending), this one is reactivated
* CRRAINB-5765 : Zendesk#24533 // Rainbow iOS freezes during a video call
* CRRAINB-5999 : Double message on iOS
* CRRAINB-3661 : Message status  change from pending to send even if network is still off
* CRRAINB-4514 : Wrong background color for received document
* CRRAINB-5658 : Bad display for some pdf files

### Restriction
* Messages status delivered to user and read by user are not displayed for bubbles conversations
* Push : Notifications are not removed from iPhone screen when message is read from another device
* Push : No notifications of invitation to join network
* Entries in Rainbow menu displayed in contact integration can display entries twice

## [1.55.2 build 437] - [2019-04-16]

### Changed
* Revert of CRRAINB-5559 : Feminization of Rainbow labels for FR and EN languages
* Improved last message extraction from Conversation for faster message display

### Fixed
* CRRAINB-5861 : Presence : after logout ( in dnd mode) and log in it's not possible to change it

### Restriction
* Messages status delivered to user and read by user are not displayed for bubbles conversations
* Push : Notifications are not removed from iPhone screen when message is read from another device
* Push : No notifications of invitation to join network
* Entries in Rainbow menu displayed in contact integration can display entries twice

## [1.55.1 build 436] - [2019-04-09]

### Added
* RQRAINB-1239 : Display bubbles with no activity the last past 60 days
* RQRAINB-1308 : Redirect VoIP calls while in DND mode
* RQRAINB-1235 : Refactoring of SFU/PGi source code
* RQRAINB-1306 : GetConversations API on stream resume

### Changed
* Rename "Rooms" section to "Bubbles" and "My meetings" to "Meetings"

### Fixed
* [PGI] At the end of a conference, the connected user is sometimes seen as 'Offline'
* CRRAINB-5438 : Lost messages content when user opens message notifications
* CRRAINB-5455 : PGI : the conversation screen is missing
* CRRAINB-5654 : Share picklist is empty when user share from extension
* CRRAINB-5691 : Meeting invitation notification redirects to bubbles instead of meetings screen
* CRRAINB-5722 : Ongoing connection
* CRRAINB-5723 : Unable to unmute during SFU conference
* CRRAINB-5707 : Last message appears empty in conversation list view if its  a deleted message
* CRRAINB-5801 : Crash - [UIContactsTableViewController insertContact:]
* CRRAINB-5818 : Crash - [RTCService didReceiveProposeMsg:withMedias:from:resource:]
* CRRAINB-5819 : Crash - [FilteredSortedSectionedArray objectsInSection:]
* CRRAINB-3791 : Zendesk#19940 // Inconsistent space at end of messages
* CRRAINB-5679 : User should not be able to reply to conference start and end event message

### Developer notes
* If a conference-info is received during the resume stream, the message has 2 children (delay) and in this case, the isConferenceManagementMessage returns NO
* Bad management of invited guests (email is not displayed + crash if removed)
* Rework method anonymizeXMPPString to anonymizeMessageElementWithString (use element instead of regex)
* Anonymize push IM (body / title / last-message-body / first-last-name / room-name)
* The separators for meetings have been reworked
* The badge on the Rainbow icon is now updated the same way as we do in the application (not always reset to 0)

### Restriction
* Messages status delivered to user and read by user are not displayed for bubbles conversations
* Push : Notifications are not removed from iPhone screen when message is read from another device
* Push : No notifications of invitation to join network
* Entries in Rainbow menu displayed in contact integration can display entries twice

## [1.55.0 build 434] - [2019-04-02]

### Added
* RQRAINB-1311 : Channels - Ack. of notifications shall be enhanced
* RQRAINB-1310 : Channels - Display attached files in posts
* RQRAINB-1237 : Channels - Explore
* The number of pending meeting invitations has been added in the badge of the user avatar

### Fixed
* CRRAINB-5655 : Wrong display for forwarded Message 
* CRRAINB-5592 : Channels - color tags are misinterpreted in posts
* CRRAINB-5593 : Channels - some <br> tags are still misinterpreted
* CRRAINB-5687 : Wrong error is displayed when starting personal conference
* CRRAINB-5707 : last message appears empty in conversation list view if its  a deleted message
* CRRAINB-5590 : Channels - Separation of posts is not clear enough
* CRRAINB-5591 : Channels - size of category icons is too small
* [Channels] When the user has no latest items, he does not have the empty screen information
* The first time a room avatar is set is never taken into account

### Developer notes
* [PGI] Restrict personal meeting to room with endpoint
* Enable stanza filtering flag on resume

### Restriction
* Messages status delivered to user and read by user are not displayed for bubbles conversations
* Push : Notifications are not removed from iPhone screen when message is read from another device
* Push : No notifications of invitation to join network
* Entries in Rainbow menu displayed in contact integration can display entries twice

## [1.54.4 build 435] - [2019-04-08]

### Fixed
* CRRAINB-5782 : Zendesk 24565 // Connection Issue from Android application

### Restriction
* Messages status delivered to user and read by user are not displayed for bubbles conversations
* Push : Notifications are not removed from iPhone screen when message is read from another device
* Push : No notifications of invitation to join network
* Entries in Rainbow menu displayed in contact integration can display entries twice

## [1.54.3 build 433] - [2019-04-02]

### Fixed
* CRRAINB-5347 : Zendesk#23636 // Main issue to register on iOS
* CRRAINB-5640 : Logs are received empty (no attachment)

### Restriction
* Messages status delivered to user and read by user are not displayed for bubbles conversations
* Push : Notifications are not removed from iPhone screen when message is read from another device
* Push : No notifications of invitation to join network
* Entries in Rainbow menu displayed in contact integration can display entries twice

## [1.54.2 build 432] - [2019-03-27]

### Added
* RQRAINB-1227 : Do not create conversations on client side
* RQRAINB-1187 : Delete/modify the last chat message
* RQRAINB-1188 : Transfer/Copy/Share a chat message
* RQRAINB-1189 : Reply to a chat message

### Fixed
* CRRAINB-5678 : App crash when user reply to message 
* CRRAINB-4618 : Zendesk#22142 // Company search result not easy to read and reach the last company
* CRRAINB-5656 : Deleted messages from andorid appears as empty edited message on ios
* CRRAINB-5657 : Login on .net leads to infinite "connecting" loader
* CRRAINB-5659 : PDF thumbnail appears empty when user send a file from icloud drive

### Restriction
* Messages status delivered to user and read by user are not displayed for bubbles conversations
* Push : Notifications are not removed from iPhone screen when message is read from another device
* Push : No notifications of invitation to join network
* Entries in Rainbow menu displayed in contact integration can display entries twice

## [1.54.1 build 431] - [2019-03-22]

### Added
* [PGI] An explicit pop-up error is now showed when we try to join a scheduled meeting more than 10 minutes before

### Changed
* [PGI] Re-enable meetings for users under "Entreprise conference" licence

### Fixed
* [PGI] Users under "Enterprise Conference" were never able to create a meeting
* CRRAINB-5393 : Crash of the application after refresh of the meeting (PGI)
* CRRAINB-5444 : PGI: the dial out is proposed with the licence "entreprise conference"
* CRRAINB-5454 : PGI: the personal meeting's name takes the name of another user 
* CRRAINB-5560 : Zendesk#24074 // PDF file preview not seen on iOS
* CRRAINB-5111 : Pictures are not displayed
* CRRAINB-5390 : Modified text message from iOS should appear as one message on android
* CRRAINB-5612 : Zendesk#24218 // Rainbow APP Crash in iOS
* CRRAINB-5618 : Zendesk#24229 // Rainbow closes itself for incoming WebRTC call
* CRRAINB-4797 : PGI: joining the conference 10 minutes before the beginning of the conference isn't possible
* CRRAINB-5058 : Zendesk#22999 // Received pictures not displayed on iOS
* CRRAINB-5153 : Download/Saving/Viewing large files - videos bad behaviour
* CRRAINB-5528 : Image name appear on image when shared from gallery
* CRRAINB-5625 : Last message apppears empty in conversation list view if its a photo
* CRRAINB-5639 : User can't start personal conference on iOS 
* CRRAINB-5642 : You are not member of this bubble... message appears when I open personal meeting

### Developer notes
* [PGI] An ended scheduled meeting was still joinable after a conferenceRemove event
* [PGI] Add method to find the active personal meeting room
* [PGI] Logs have been added during the startAndJoinConference process
* [PGI] Stop detaching the conference at the end of a personal meeting (it will be done during archiving process)
* [SFU/PGI] Refactoring of the method isMyConference (sometimes we got a wrong information)
* Avoid the creation of default room avatar in the main thread

### Restriction
* Messages status delivered to user and read by user are not displayed for bubbles conversations
* Push : Notifications are not removed from iPhone screen when message is read from another device
* Push : No notifications of invitation to join network
* Entries in Rainbow menu displayed in contact integration can display entries twice

## [1.54.0 build 430] - [2019-03-18]

### Fixed
* CRRAINB-3377 : MP: the communication isn't released on Android on transfer
* CRRAINB-5051 : PGI: after selection of the personal conference, only the share button is displayed
* CRRAINB-5387 : PGI: impossible to open my personal meeting
* CRRAINB-5402 : PGI: after key press on the meeting's phone number, nothing appens
* CRRAINB-5452 : PGI: crash of the application after using the sharing on the web
* CRRAINB-4952 : Zendesk#22802 // User in participant list during a Conference SFU while not even member of the bubble
* CRRAINB-5095 : PGI: after releasing the scheduled meeting, this one is seen as an SFU
* CRRAINB-5519 : No join button on bubble
* CRRAINB-3779 : MP: bad management of the display after a transfer
* CRRAINB-5110 : Click on "Ouvrir" on personal PGI meeting doesn't have any action
* CRRAINB-5495 : Zendesk#23931 // Mobile number not well displayed
* CRRAINB-5527 : App crash when loading channels 
* CRRAINB-5528 : Image name appear on image when shared from gallery
* CRRAINB-5562 : Zendesk#23947 // Overlapping text on iPhone SE 4 inch display
* CRRAINB-5563 : User appears in conversation list as a new conversation after activating a conference bubble
* CRRAINB-5494 : Zendesk#23937 // Missing german translation for "People and conversations"
* CRRAINB-5189 : Zendesk#23293 // Translation errors in german version
* CRRAINB-5558 : User Avatar is not loaded even when network connection is back

### Developer notes
* First step of start-up optimization (Cache contacts vs channels)

### Restriction
* Messages status delivered to user and read by user are not displayed for bubbles conversations
* Push : Notifications are not removed from iPhone screen when message is read from another device
* Push : No notifications of invitation to join network
* Entries in Rainbow menu displayed in contact integration can display entries twice

## [1.53.2 build 429] - [2019-03-07]

### Developer notes
* Impossible to submit the application without iPad support when it has already been activated. Disabling telephony capability in info.plist :
The key UIRequiredDeviceCapabilities in the Info.plist may not contain values that would prevent this application from running on devices that were supported by previous versions.

## [1.53.2 build 428] - [2019-03-07]

### Changed
* Remove meetings menu for users under "Entreprise conference" licence 

### Fixed
* CRRAINB-5302 : Zendesk#23541 // Button “Join the conference” didn’t change the context after conference starting using iOS app
* CRRAINB-5304 : Zendesk#23543 // Bubbles SFU 20 : TDD4Q Weekly Performance call - Not able to join SFU from iOS
* CRRAINB-5499 : App crash when user has a lot of channels 
* CRRAINB-5475 : Reply from notif on locked screen does not work on ios
* CRRAINB-5510 : Links in channel posts are not clickable

### Developer notes
* [PGI] The personal meeting is not archived anymore from iOS client
* [PGI] Fix wrong comparison --> a scheduled meeting was considered as ended if we passed the START date (should be the END)

### Restriction
* Messages status delivered to user and read by user are not displayed for bubbles conversations
* Push : Notifications are not removed from iPhone screen when message is read from another device
* Push : No notifications of invitation to join network
* Entries in Rainbow menu displayed in contact integration can display entries twice

## [1.53.1 build 427] - [2019-03-05]

### Added
* Channels - The items of a channel are now fetched each time you open a channel

### Changed
* Channels - The border color of any image in a channel message is now darker
* Channels - The font of the messages has been uniformized
* Rooms and meetings - Change alert notification behaviour for pending invitations to avoid multiple alerts

### Fixed
* The size of the menu entry "Meetings" is now the same as the other entries
* CRRAINB-5348 : iOS: Same picture preview appears in all conversations
* CRRAINB-5383 : App crash when user from web updates channel name
* CRRAINB-5126 : User can't share video files/pdf/links files through rainbow app
* CRRAINB-5161 : PBX: the call log is missing  on ios
* CRRAINB-5281 : Zendesk#23385 // no voip call menu on IOS
* CRRAINB-5300 : Zendesk#23232 // No telephony option (WebRTC GW) on iPhone
* CRRAINB-5317 : Zendesk#23566 // no VOIP on iOS
* CRRAINB-5351 : Zendesk#23679 // Forwarding a file on ios using the "Sharing" function with the Rainbow app does not work
* CRRAINB-5116 : Some phone config cannot be checked
* CRRAINB-5153 : Download/Saving/Viewing large files - videos bad behaviour
* CRRAINB-5154 : File should be downloaded once 
* CRRAINB-5331 : Zendesk#23619 // Several issues with iOS SDK
* CRRAINB-5350 : App crash upon sending slow motion type video 
* CRRAINB-5384 : User cannot see channel posts
* CRRAINB-5390 : Modified text message from ios should appear as one message on android
* CRRAINB-5446 : Freeze + crash on iOS
* CRRAINB-5120 : Sharing multiple files from gallery is not launching the app 
* CRRAINB-5123 : Sharing multiple files from gallery to multiple users is not successful
* CRRAINB-5423 : Unsent message disappear if user navigate from conversation screen

### Restriction
* Messages status delivered to user and read by user are not displayed for bubbles conversations
* Push : Notifications are not removed from iPhone screen when message is read from another device
* Push : No notifications of invitation to join network
* Entries in Rainbow menu displayed in contact integration can display entries twice

## [1.53.0 build 426] - [2019-02-22]

### Added
* RQRAINB-1162 : Audio resilience in case of P2P video call in degraded network - MOS calculation
* RQRAINB-1166 : Display channels
* RQRAINB-1167 : Channels - notifications
* RQRAINB-1215 : Adjustable ringtone + vibration within WebRTC mode (vs. one ringtone, no vibration mode)
* RQRAINB-1216 : Dial pad for starting PBX voice calls

### Fixed
* CRRAINB-5191 : Crash after starting the application
* CRRAINB-5299 : No notification when someone sends a message
* CRRAINB-5302 : Zendesk#23541 // Button "Join the conference" didn't change the context after conference starting using iOS app
* CRRAINB-5304 : Zendesk#23543 // Bubbles SFU 20 : TDD4Q Weekly Performance call - Not able to join SFU from iOS
* CRRAINB-5069 : PGI - In the meetings tab, personal conference informations are missing
* CRRAINB-5135 : Impossible to join a conference
* CRRAINB-5224 : MP - Endless ringing on the iphone after taking the call on the web
* CRRAINB-5312 : Zendesk#23558 // Loudspeaker issue in CallKit
* CRRAINB-4818 : Order of messages point
* CRRAINB-5102 : White screen upon clicking on notification after 12 hrs app sleep 
* CRRAINB-5101 : User can't scroll to the last conversation in conversation list
* CRRAINB-5202 : Zendesk#23325 // Wrong time stamp in home page
* CRRAINB-5205 : Zendesk#23334 // Team DD4Q performance call meeting issue (no call button on mobiles)
* The GIFs were not playable anymore

### Restriction
* Channels : for the moment, only the 100 latest messages are fetched from the server
* Messages status delivered to user and read by user are not displayed for bubbles conversations
* Push : Notifications are not removed from iPhone screen when message is read from another device
* Push : No notifications of invitation to join network
* Entries in Rainbow menu displayed in contact integration can display entries twice

## [1.52.2 build 425] - [2019-02-08]

### Fixed
* Fix issue with sending too many presences to rooms

### Restriction
* Messages status delivered to user and read by user are not displayed for bubbles conversations
* Push : Notifications are not removed from iPhone screen when message is read from another device
* Push : No notifications of invitation to join network
* Entries in Rainbow menu displayed in contact integration can display entries twice

## [1.52.1 build 424] - [2019-02-04]

### Fixed
* CRRAINB-4621 : App crash when user sends a large video
* CRRAINB-5048 : PGI: after joining the personal conference, the call isn't launched  (only a sandglass is displayed without time out)
* CRRAINB-3883 : Zendesk#20498 // Message sent from ios not displayed on Web
* CRRAINB-4640 : Zendesk#22181 // Calls to Rainbow WebRTC Gateway show an incorrect number on the mobile
* CRRAINB-4755 : iOS: some information/menu item are missing in profile page when app is started
* CRRAINB-5054 : Multiple Error Alert appear to user when files fails to download
* CRRAINB-5057 : PGI: the invitation to an personal conference isn't displayed 
* CRRAINB-5060 : User can't share a file from gallery  through rainbow app 
* CRRAINB-5068 : PGI: crash of the application after sharing the  meeting information
* CRRAINB-5082 : PGI: after an invitation, the personal  meeting  isn't displayed
* CRRAINB-4947 : Bad experience when dealing width large files download
* CRRAINB-5065 : Zendesk#23014 // Impossible to send a link on iOS
* CRRAINB-5083 : PGI: readability (formatting) of conference information after sharing by email needs to be improved

### Developer notes
* Stop fetch connected user in didLogin method (because we already do it once)
* Stop fetch connected user in populateVcardForContactsJids method (because medium mode)
* Remove all codes for "Add mecanism to avoid lots of UI updates after a resume (only one update at the end)" implemented in 1.44_389 (to avoid false logs)
* On a call to a number or voicemail without nomadic mode, a "blink" appeared before the popup to call the number appears
* Remove receipts request according XEP-0353

### Restriction
* Messages status delivered to user and read by user are not displayed for bubbles conversations
* Push : Notifications are not removed from iPhone screen when message is read from another device
* Push : No notifications of invitation to join network
* Entries in Rainbow menu displayed in contact integration can display entries twice

## [1.52.0 build 423] - [2019-01-29]

### Added
* RQRAINB-1134 : Any Enterprise user can start a conference
* RQRAINB-1132 : Share and join personal bridge and new UI

### Fixed
* CRRAINB-4818 : iOS / Order of messages point 1.51.0.417
* CRRAINB-4884 : Contact Us button is not clickeable in Companies profiles
* CRRAINB-4999 : Zendesk#22915 // No possibility to call when typing a phone number on search bar on iOS
* CRRAINB-4796 : PGI:  after releasing the communication , the participant isn't  disconnected
* CRRAINB-4761 : After scheduled conference creation, this one is seen as instant meeting
* CRRAINB-3524 : Zendesk#19139 // Miss audio conference dial in button
* CRRAINB-5002 : Zendesk#22910 // Unable to start conference when organizer and not owner on iOS
* The list of contacts when we add someone in a bubble, meeting or group could contain wrong entries (inviting, guests, ...) 

### Restriction
* Messages status delivered to user and read by user are not displayed for bubbles conversations
* Push : Notifications are not removed from iPhone screen when message is read from another device
* Push : No notifications of invitation to join network
* Entries in Rainbow menu displayed in contact integration can display entries twice

### Developer notes
* RQRAINB-1194 : compile with Xcode 10 (First build with Xcode 10 new build system)

## [1.51.5 build 422] - [2019-01-25]

### Fixed
* CRRAINB-4948 : The mute/unmute issue in a bubble is still there

### Restriction
* Messages status delivered to user and read by user are not displayed for bubbles conversations
* Push : Notifications are not removed from iPhone screen when message is read from another device
* Push : No notifications of invitation to join network
* Entries in Rainbow menu displayed in contact integration can display entries twice

## [1.51.4 build 421] - [2019-01-24]

### Fixed
* Stop send ping to server
* The notification to join a bubble took more than 20 seconds to be displayed

### Restriction
* Messages status delivered to user and read by user are not displayed for bubbles conversations
* Push : Notifications are not removed from iPhone screen when message is read from another device
* Push : No notifications of invitation to join network
* Entries in Rainbow menu displayed in contact integration can display entries twice

## [1.51.3 build 420] - [2019-01-23]

### Fixed
* CRRAINB-4744 : Impossible to make a call (with media pillar) after an incoming webrtc 
* CRRAINB-4685 : Zendesk #22136 // Interface blocked on a WebRTC Gateway call
* (PGI) It was not possible to join a conference directly after having created it
* (PGI) It was not possible to join a conference from a number in the clipboard
* (PGI) At the end of a scheduled meeting, the conference was sometimes ended (put in history)

### Restriction
* Messages status delivered to user and read by user are not displayed for bubbles conversations
* Push : Notifications are not removed from iPhone screen when message is read from another device
* Push : No notifications of invitation to join network
* Entries in Rainbow menu displayed in contact integration can display entries twice

## [1.51.2 build 419] - [2019-01-18]

### Fixed
* CRRAINB-4828 : Filtering search by "company" returns wrong results
* CRRAINB-4858 : The search text is launched without timeout (in the bubble)
* When searching something in a conversation, the message text input has been hidden.

### Restriction
* Messages status delivered to user and read by user are not displayed for bubbles conversations
* Push : Notifications are not removed from iPhone screen when message is read from another device
* Push : No notifications of invitation to join network
* Entries in Rainbow menu displayed in contact integration can display entries twice

## [1.51.1 build 418] - [2019-01-15]

### Fixed
* CRRAINB-4810 : Crash of the application after displaying the result of the text search
* CRRAINB-4824 : Join button in SFU does not appear if user joins a SFU bubble and conference is already started
* CRRAINB-4826 : Join button in SFU does not appear if user click on conference notification
* CRRAINB-4828 : Filtering search by "company" returns wrong results
* CRRAINB-4833 : User can't navigate in search results
* CRRAINB-4751 : Zendesk#22412 // Inconsistent message display after image deletion
* CRRAINB-4700 : Factoring the call log after reinstall

### Restriction
* Messages status delivered to user and read by user are not displayed for bubbles conversations
* Push : Notifications are not removed from iPhone screen when message is read from another device
* Push : No notifications of invitation to join network
* Entries in Rainbow menu displayed in contact integration can display entries twice

## [1.51.0 build 417] - [2019-01-11]

### Fixed
* CRRAINB-4132 : Invitations are not cancelled when cancelled from ios
* CRRAINB-4481 : No synchronization of new IMs between Web and iOS
* CRRAINB-4554 : "Remove all messages" option does not delete all messages 
* CRRAINB-4605 : User should not be able to call users out of his network
* CRRAINB-4606 : Newly registered account received a wrong caller ID call
* CRRAINB-4633 : Disable call and video button in contact card if user is busy in conference or call
* CRRAINB-4704 : Crash - [ConversationsManagerService deleteFailedMessage:to:] 
* CRRAINB-4705 : Crash - [XMPPMessageArchiveManagementCoreDataStorage fetchMessagesForJID:xmmpStream:maxSize:offset:] 
* CRRAINB-4706 : Crash - [ContactsManagerService inviteContactByRainbowID:] 
* CRRAINB-4707 : Crash - [RTCService removeVideoMediaFromCall:] 
* CRRAINB-4708 : Crash - [CLTokenView updateLabelAttributedText] 
* CRRAINB-4743 : Zendesk #22386 // Most of the messages are not displayed in the conversation with a contact
* CRRAINB-4750 : SFU - Error message after trying to join the conference
* CRRAINB-4775 : PGI - Impossible to join the conference (the user is seen as connected)
* CRRAINB-4532 : [PayTren] Attachement disappears from chat converstion if user navigate to other screen 
* CRRAINB-4709 : Zendesk#22327 // Text disappears with picture
* CRRAINB-4737 : Zendesk#22369 // Empty line at the top of the home page
* CRRAINB-4763 : Zendesk#22446 // Application stays in background : doesn't come to foreground
* CRRAINB-4797 : PGI - Joining the conference 10 minutes before the beginning of the conference isn't possible

### Restriction
* Messages status delivered to user and read by user are not displayed for bubbles conversations
* Push : Notifications are not removed from iPhone screen when message is read from another device
* Push : No notifications of invitation to join network
* Entries in Rainbow menu displayed in contact integration can display entries twice

### Developer notes
* Fix crash : https://rink.hockeyapp.net/manage/apps/522894/app_versions/141/crash_reasons/254068621 (removeVideoMediaFromCall)
* Fix crash : https://rink.hockeyapp.net/manage/apps/522894/app_versions/141/crash_reasons/253750256 (deleteFailedMessage)
* Fix crash : https://rink.hockeyapp.net/manage/apps/522894/app_versions/141/crash_reasons/254132932 (inviteContactByRainbowID)
* Fix crash : https://rink.hockeyapp.net/manage/apps/522894/app_versions/141/crash_reasons/254299576 (XMPPMessageArchiveManagementCoreDataStorage fetchMessagesForJID:xmmpStream:maxSize:offset:)
* Fix crash : https://rink.hockeyapp.net/manage/apps/522894/app_versions/141/crash_reasons/256107134 (crash at startup)
* Remove armv7s support


## [1.50.3 build 416] - [2018-12-21]

### Fixed
* CRRAINB-4638 : Cannot join a SFU conference
* CRRAINB-4134 : Zendesk#21135 // WebRTC Gateway call cannot be answered when the phone is locked
* CRRAINB-4520 : WebRTC call : "Call failed" after answering
* CRRAINB-4621 : App crash when user sends a large video
* CRRAINB-4641 : Zendesk #22174 // Gateway : no audio of incoming call after a sleeping mode period
* CRRAINB-4643 : Crash of the application after sending a file
* CRRAINB-3833 : Zendesk#20144 // Incoming Call not ringing immediately
* CRRAINB-4006 : Zendesk#20403 // No audio on the iPhone, when the terminal is idle
* CRRAINB-4106 : No audio when incoming call taken from iOS Callkit for REX-only
* CRRAINB-4443 : Conversation thread disappears when user scroll (silence/alert) options
* The initial value of the random colors setting was always NO even if the toggle was on YES.

### Restriction
* Messages status delivered to user and read by user are not displayed for bubbles conversations
* Push : Notifications are not removed from iPhone screen when message is read from another device
* Push : No notifications of invitation to join network
* Entries in Rainbow menu displayed in contact integration can display entries twice

### Developer notes
* The list of ice servers was sometimes empty because it was cleared before the request to get them
* The startup of the application has been optimized
* On a VoIP push received, the application was started in full login (instead of resume mode)

## [1.50.2 build 415] - [2018-12-14]

### Modified
* The personal meeting feature, partially introduced in the version 1.50.1, has been disabled and postponed to a further version.
* The call logs list has been enhanced.
* An invitation sent to a number by SMS has now the same avatar as Android and Web.

### Removed
* The vcard icon right to a sent invitation has been removed.

### Fixed
* Fix a case where we were not able to join a conference.

### Restriction
* Messages status delivered to user and read by user are not displayed for bubbles conversations
* Push : Notifications are not removed from iPhone screen when message is read from another device
* Push : No notifications of invitation to join network
* Entries in Rainbow menu displayed in contact integration can display entries twice

### Developer notes
* All personal meetings are now hidden (list of conversations, bubbles and meetings)
* In the call logs list, it was not possible to call a number not attached to a Rainbow user
* In the call logs list, entries with only the number were not grouped in a same entry

## [1.50.1 build 414] - [2018-12-12]

### Modified
* In the calllog, the entry concerning the voicemail is now labelled with "Voicemail"

### Fixed
* CRRAINB-4489 : Multiple PGi Conference conversation when started in desktop
* CRRAINB-4436 : Wrong flow when user clicks on text message notification
* CRRAINB-4521 : (MP) Impossible to retrieve the communication after a cellular incoming call
* CRRAINB-4531 : [PayTren] Empty attachement name
* CRRAINB-4533 : [PayTren] Attachements appear as null upon app launch 
* CRRAINB-4534 : Crash - [ContactsManagerService loadInvitationsFromCache] 
* CRRAINB-4535 : Crash - [File fileTypeForMimeType:] 
* CRRAINB-4536 : Crash - [FileSharingService uploadFile:forConversation:completionHandler:]
* CRRAINB-4554 : "Remove all messages" option does not always delete all messages
* CRRAINB-4555 : Videos are not saved to "camera roll" album
* CRRAINB-4574 : Change the text from french to english in (Promote ownership)
* CRRAINB-4575 : Bubble avatar is not updated when user transfers ownership
* CRRAINB-4334 : No active talker information displayed
* CRRAINB-4445 : Zendesk #21851 // (MP) Ringtone doesn't stop after picked up the call
* CRRAINB-4473 : Zendesk#21916 // Missing German translations 
* CRRAINB-4144 : Zendesk#21151 // Missing German translations
* In the calllog, the entries with only a phone number were not displayed
* The last message in the conversations list is now correctly set for an attachment when opening the application
* For a pending invitation sent by SMS, display the phone number instead of nothing
* For a pending invitation sent to an email, display this email address instead of nothing
* Some bubble was named as a meeting in the "Details screen" and in the "More menu"
* The number of turn servers for WebRTC calls could be much more higher than expected
* Sometimes the XMPP websocket is closed even when we have a call in progress

### Restriction
* Messages status delivered to user and read by user are not displayed for bubbles conversations
* Push : Notifications are not removed from iPhone screen when message is read from another device
* Push : No notifications of invitation to join network
* Entries in Rainbow menu displayed in contact integration can display entries twice

### Developer notes
* When fetching the conversations, remove from cache deleted ones

## [1.50.0 build 413] - [2018-11-30]

### Added
* RQRAINB-1149 : Display animated GIFs
* RQRAINB-1141 : Color setting for one-to-one conversations
* RQRAINB-1132 : Share and join personal bridge and new UI
* RQRAINB-1099 : Transfer bubble ownership

### Modified
* In a conversation menu, some labels of actions have been rewritten.
* In a conversation menu, the color of "Remove all messages" and "Remove" entries are now red (destructive actions).

### Fixed
* CRRAINB-3524 : Zendesk#19139 // Miss audio conference dial in button
* CRRAINB-4031 : Failed to send invitation to a specific user (unfinalized guest)
* CRRAINB-4048 : App crash when guest user opens app from invitation  email for first time
* CRRAINB-4298 : [Security] All clients: obfuscate messages in markdown content in logs
* CRRAINB-4307 : Some bubbles are not displayed in "My bubbles" tab
* CRRAINB-3554 : Sharing video in conference is not working properly 
* CRRAINB-3923 : Unwanted PGi personal bridge in "My bubbles" tab
* CRRAINB-4045 : Wrong behavior when guest user in bubble launch from invite email
* CRRAINB-4047 : Guest user credentials are saved in application
* CRRAINB-4186 : (MP) After canceling a call, the application is blocked
* CRRAINB-4190 : Zendesk#21275 // Unknown caller is presenting an incoming call
* CRRAINB-4195 : Zendesk#21303 // No avatar and name for an incoming call on call logs
* CRRAINB-4203 : (MP) The caller display name is "Inconnu" for some calls
* CRRAINB-4275 : Conference bridge disappears from "My meetings"
* CRRAINB-4358 : Attached media disapears if there is no connection 
* CRRAINB-3927 : Zendesk#20554 // Active conversation no longer displayed on web but audio is still sent
* CRRAINB-3988 : Buttons not shown when trying to forward a file while in a conversation
* CRRAINB-4054 : Guest joining does not arrive in the bubble
* CRRAINB-4056 : Bubble list is partially hidden for guest
* CRRAINB-4058 : Guest sees other guests twice
* CRRAINB-4270 : Zendesk#21219 // Possibility to share a file without previously downloading it : creation of an empty email
* CRRAINB-4284 : Logs are getting too large : more than 20 MB
* CRRAINB-4287 : Avatar of bbubbles does not expand
* CRRAINB-4339 : Wrong title of conversation
* CRRAINB-4373 : After app wake up new conversation does not show up
* CRRAINB-4406 : Zendesk#21806 // IM received does not appear on the conversation
* CRRAINB-4288 : Avatar zoom of user Chester Sun does not work

### Restriction
* Messages status delivered to user and read by user are not displayed for bubbles conversations
* Push : Notifications are not removed from iPhone screen when message is read from another device
* Push : No notifications of invitation to join network
* Entries in Rainbow menu displayed in contact integration can display entries twice

### Developer notes
* Fix crash : https://rink.hockeyapp.net/manage/apps/522894/app_versions/138/crash_reasons/250485963 (mute unmute button)
* Fix crash : https://rink.hockeyapp.net/manage/apps/522894/app_versions/138/crash_reasons/250340708 (update contact field with nil object)
* Fix crash : https://rink.hockeyapp.net/manage/apps/522894/app_versions/134/crash_reasons/245134301 (avoid this case, crash is due to guest bad behavior)
* Big first step refactoring of the channels

## [1.49.1 build 412] - [2018-11-20]

### Removed
* It is not possible anymore to edit an archived room

### Fixed
* CRRAINB-4207 : Crash when user received multiple notifications
* CRRAINB-4011 : An organizer can add participants to an archived bubble
* CRRAINB-4216 : Zendesk#21376 // Wrong timestamps
* CRRAINB-4218 : After an invitation to join another company, the onboarding isn't displayed
* CRRAINB-4234 : When user skips the onboarding, it's skipped for all users who login on same device
* The URL received in markdown with accents were not displayed as a hyperlink
* The labels "Remove" and "Delete" were sometimes not used correctly

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Push : Notification are not removed from iPhone screen when message is read from another device
* Push : No notifications of invitation to join network
* Entries in Rainbow menu displayed in contact integration can display entries twice

## [1.49.0 build 411] - [2018-11-12]

### Added
* RQRAINB-1101 : Understand markdown tags
* RQRAINB-1093 : Onboarding - add contacts in my network
* RQRAINB-1095 : Sort bubbles by name or creation date
* RQRAINB-1096 : Delete or leave a bubble in one click instead of archiving the bubble first
* RQRAINB-1097 : Remove and archive multiple bubbles at once
* RQRAINB-1098 : Multiple administrators of a bubble

### Changed
* The avatar size of an entry in a list has been reduced.
* The button to add a participant in a bubble has been moved in the toolbar.
* A popup of confirmation has been added when you remove an organizer from a bubble.
* It is now possible to create a bubble without any participant.

### Fixed
* CRRAINB-3874 : Zendesk#20397 // No pop-up in incoming WebRTC Gateway call 
* CRRAINB-4114 : Rainbow crashed at opening
* CRRAINB-3960 : Zendesk#20040 // Profile switches to busy on presentation mode 
* CRRAINB-3965 : Endless spinner when trying to create account with invalid email address
* CRRAINB-4002 : "Connection in progress" banner although network is ok
* CRRAINB-4011 : An organizer can add participants to an archived bubble
* CRRAINB-4032 : "Invitation is sent" toast message even when network is off
* CRRAINB-4040 : Zendesk#20870 // Bubble totally disappeared when adding users
* CRRAINB-4061 : Non finalized guest is given the possibility to delete its account via bubble participant list
* CRRAINB-4070 : Unfinalized guest should not have acces to his profile
* CRRAINB-4151 : Zendesk#21158 // Sporadically no Chats in conversation tab after iPhone hibernate
* CRRAINB-4156 : Zendesk#21182 // No audio on incoming calls
* CRRAINB-4195 : Zendesk#21303 // no avatar and name for an incoming call on call logs
* CRRAINB-3972 : (French OS) No links to the terms and conditions & privacy policy when creating an account 
* CRRAINB-4007 : Zendesk#20647 // WebRTC Gateway : iOS issue on Calls in CHINA
* CRRAINB-4092 : Bubble pops up on the iOS home page after member add
* CRRAINB-4111 : Zendesk#21035 // Clicking on contact in contact list is not effective
* CRRAINB-4148 : Bubble owner cannot remove organizers
* CRRAINB-4193 : Zendesk#20115 // Empty conversations after crash of the application
* CRRAINB-3966 : Align empty missed call wording with other wordings
* CRRAINB-4131 : No camera transmission in lock screen
* In the bubble details screen, display "Member" if there is only one member instead of "Members (1)"

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Push : Notification are not removed from iPhone screen when message is read from another device
* Push : No notifications of invitation to join network
* Entries in Rainbow menu displayed in contact integration can display entries twice

### Developer notes
* Remove useless loop to build the GroupedMessage on only one message
* Fix crash : https://rink.hockeyapp.net/manage/apps/522894/app_versions/136/crash_reasons/247400898 (key cannot be nil)
* Fix crash : https://rink.hockeyapp.net/manage/apps/522894/app_versions/136/crash_reasons/247218127 (unrecognized selector NSNull)
* Fix crash : https://rink.hockeyapp.net/manage/apps/522894/app_versions/136/crash_reasons/246472115 (out of bounds)
* Fix crash : https://rink.hockeyapp.net/manage/apps/522894/app_versions/136/crash_reasons/247401430 (object cannot be nil)
* Fix crash : https://rink.hockeyapp.net/manage/apps/522894/app_versions/136/crash_reasons/247453065 (_moreMenuButton is nil)
* Change the "other menu" horizontal by a vertical (in conversation screen + list of rooms)
* Move the icon "Vcard" more to the right
* Resize the avatar of the UIRainbowGenericTableViewCell from 57,5 to 45
* Manage the space between the mainLabel (ex: display name) and the subLabel (ex: last message) when there is no midLabel

## [1.48.4_410] - [2018-10-26]

### Changed
* All the analytics (Firebase and Google) have been disabled in this version.

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Push : Notification are not removed from iPhone screen when message is read from another device
* Push : No notifications of invitation to join network
* Entries in Rainbow menu displayed in contact integration can display entries twice

## [1.48.3_409] - [2018-10-25]

### Fixed
* CRRAINB-4015 : Zendesk#20807 // Rainbow Logs containing personal chat conversations in clear text
* CRRAINB-3920 : Double entry in Conversations tab
* CRRAINB-3973 : Invitations sent are no longer displayed

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Push : Notification are not removed from iPhone screen when message is read from another device
* Push : No notifications of invitation to join network
* Entries in Rainbow menu displayed in contact integration can display entries twice

### Developer notes
* Logs about messages were not totally anonymized (content, subject and sometimes the body)

## [1.48.2_408] - [2018-10-23]

### Fixed
* CRRAINB-3854 : User should not be able to receive/make call when he is in busy status
* CRRAINB-3925 : Bubble removed appears in my bubbles list
* CRRAINB-3961 : Conversations tab is empty when app is started after app installation
* CRRAINB-3965 : Endless spinner when trying to create account with invalid email address
* CRRAINB-4001 : "No connection" banner although network is fully ok
* CRRAINB-1878 : Cannot see the screen sharing anymore
* CRRAINB-3924 : Unwanted bubbles in my archives
* CRRAINB-3957 : Bad display during self-enrolment
* CRRAINB-3994 : Links in SMS invitation are not clickable
* CRRAINB-3958 : Minor display improvement for self-enrolment* 
* The invitations by email (sent) were not displayed anymore (wrong filter).

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Push : Notification are not removed from iPhone screen when message is read from another device
* Push : No notifications of invitation to join network
* Entries in Rainbow menu displayed in contact integration can display entries twice

### Developer notes
* Attempt to fix crashes in RecentsConversationsTableViewController for out of bound
* Fix crash : https://rink.hockeyapp.net/manage/apps/522894/app_versions/129/crash_reasons/243562291 (out of bounds in Contact+Extensions)
* Fix crash : https://rink.hockeyapp.net/manage/apps/522894/app_versions/129/crash_reasons/242890718 (out of bounds in RTCService)
* Fix crash : https://rink.hockeyapp.net/manage/apps/522894/app_versions/129/crash_reasons/242875223 (was mutated in RTCService)

## [1.48.1_407] - [2018-10-17]

### Changed
* A mecanism to avoid multiple refreshs was added in version 1.44_389. We remove it temporarily as this is not satisfying and will rework on it later.

### Fixed
* CRRAINB-3884 : Rainbow crashed at opening
* CRRAINB-3773 : Incoming call notification with wrong name
* CRRAINB-3854 : User should not be able to receive/make call when he is in busy status
* CRRAINB-3895 : The button hold is missing
* CRRAINB-3915 : The dtmf button is missing in communication (cellular)
* CRRAINB-3858 : Zendesk#20374 // Conversations in disorder when opening the application
* CRRAINB-3896 : (MediaPillar) After key press on the keyboard (dtmf), the tone indication is missing
* CRRAINB-3768 : Zendesk #19814 // Display of mute and speaker button on IOS for PBX call whereas shouldn't be displayed
* CRRAINB-3888 : Zendesk#20514 // Crash after receiving a push notification
* Fix crash : https://rink.hockeyapp.net/manage/apps/522894/app_versions/131/crash_reasons/243627763

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Push : Notification are not removed from iPhone screen when message is read from another device
* Push : No notifications of invitation to join network
* Entries in Rainbow menu displayed in contact integration can display entries twice

### Developer notes
* Cancel the mechanism to "freeze" the UI during the resume process (need to enhance later)
* Fix issue with our own presence in the VCard (myContact was not in our roster anymore)
* Fix issue for a login from scratch --> no conversations displayed

## [1.48_406] - [2018-10-15]

### Fixed
* CRRAINB-3578 : Login several regressions
* CRRAINB-3660 : Messages disappear when sent in no network connection mode
* CRRAINB-3719 : App crashes each time I want to remove a file from the Rainbow personal storage
* CRRAINB-2830 : Zendesk#16980 // Can not join PSTN conference from my iOS : no numbers displayed
* CRRAINB-3228 : Impossible to invite a guest (with email) from the bubble 
* CRRAINB-3405 : Media Pillar/WebRTC Gateway: iOS client of caller rings on outgoing call made from the Web client
* CRRAINB-3540 : Cannot start a conference on a bubble (no button)
* CRRAINB-3704 : Zendesk#19648 // Keep receiving IM notifications in a bubble I left
* CRRAINB-3707 : No audio once answered an incoming call
* CRRAINB-3729 : Bad management of the screen: the entries scroll behind the banner
* CRRAINB-3737 : No Local notification when user puts app in background
* CRRAINB-3751 : MediaPillar - in communication, the name's resolution isn't done
* CRRAINB-3761 : Wrong incoming call notification then app freeze
* CRRAINB-3771 : After starting the application, my network list is empty
* CRRAINB-3807 : Other people do not see messages I sent after accepting a bubble invitation
* CRRAINB-3825 : Join conference button appears in wrong bubble
* CRRAINB-3826 : Video call does not transfer the camera of the recepient to caller
* CRRAINB-3831 : Zendesk 20126 // Impossible to dial to numbers beginning with * or # if connected to webrtcgw
* CRRAINB-3861 : Conference is muted when user starts it
* CRRAINB-2191 : When a guest joins a bubble, his name does not appear
* CRRAINB-2614 : Weird notification of canceled meetings
* CRRAINB-3243 : Video notification does not direct user to conversation thread
* CRRAINB-3263 : Put a default presence indicator when starting the app
* CRRAINB-3524 : Zendesk#19139 // Miss audio conference dial in button
* CRRAINB-3525 : Zendesk #19138 // When opening the "received files" tab, the file preview does not show
* CRRAINB-3553 : User has to perform extra step when he sends media type in conference 
* CRRAINB-3555 : Zendesk #19237 // Cannot join the public users
* CRRAINB-3626 : Chat message received whilst previous message seems not sent
* CRRAINB-3662 : Bubble avatar does not remove user photo after leaving the bubble
* CRRAINB-3699 : MediaPillar - second call is failed after few seconds
* CRRAINB-3803 : Muted and unmuted didn't work
* CRRAINB-3241 : The notification bar appears above the "add to my network" bar
* CRRAINB-3496 : [Enhancement] User should not be able to forward any files type to archived bubble
* CRRAINB-3762 : Bubble shall not appear in Conversations tab if someone leaves
* CRRAINB-3860 : Close button in profile picture screen is not working on iPhone X
* The presentation mode displayed for a contact on iOS is now aligned on the Web client (icon + label)

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Push : Notification are not removed from iPhone screen when message is read from another device
* Push : No notifications of invitation to join network
* Entries in Rainbow menu displayed in contact integration can display entries twice

### Developer notes
* Known issue : the list of conversations is empty after a login when the app is installed from scratch
* Known issue : the user own presence is not displayed anymore (company name instead)
* Add "sharing" translation for the banner
* Presence issue : ignore resource with no resource
* Presence issue : ignore presence message with stamp older than the current

## [1.47.3_405] - [2018-10-08]

### Fixed
* CRRAINB-3751 : (Mediapillar) In communication, the name's resolution isn't done
* CRRAINB-3787 : After sending files from android, they are not received on iphone
* CRRAINB-2622 : Zendesk#16319 // Received pictures are not displayed on IOS
* Fix crash during application startup when loading conversations from cache

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Push : Notification are not removed from iPhone screen when message is read from another device
* Push : No notifications of invitation to join network
* Entries in Rainbow menu displayed in contact integration can display entries twice

## [1.47.2.404] - [2018-10-04]

### Fixed
* CRRAINB-3751 : (Mediapillar) In communication, the name's resolution isn't done
* Fix crash: https://rink.hockeyapp.net/manage/apps/522894/app_versions/123/crash_reasons/238500722

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Push : Notification are not removed from iPhone screen when message is read from another device
* Push : No notifications of invitation to join network
* Entries in Rainbow menu displayed in contact integration can display entries twice

### Developer notes
* Load rooms which are not in conversations list in a low priority thread

## [1.47.1.403] - [2018-10-02]

### Fixed
* CRRAINB-3645 : Missing message in conversation
* CRRAINB-3746 : The communication is active on iPhone when the noe set releases the call
* CRRAINB-3239 : Problems with the avatars in the bubbles
* CRRAINB-3512 : No switch to the Rainbow conversation screen when receiving PBX calls
* CRRAINB-3513 : No push notification on incoming PBX call when Rainbow app is not started
* CRRAINB-3621 : Conference is still active after user terminates app

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Push : Notification are not removed from iPhone screen when message is read from another device
* Push : No notifications of invitation to join network
* Entries in Rainbow menu displayed in contact integration can display entries twice

### Developer notes
* The rooms displayed in the conversations list are loaded in priority to display all conversations in one time
* Send the first presence directly when authenticated (as before) but now, disable "allowRosterlessOperation" to handle presences after having all roster
* Add hidden button to dump the presences (resources) of all roster storage

## [1.47.402] - [2018-09-25]

### Added
* The settings interface has been updated (new labels, copyright added)

### Fixed
* CRRAINB-3556 : User first and last name appears as null if it contains special character                
* CRRAINB-3691 : iOS: cannot chat with/call a contact of my company that is not in network                
* CRRAINB-3663 : create bubble button is always disbaled after  first login                 
* CRRAINB-3631 : Zendesk#19453 // "MediaPillars" or "mp_xxxxxx" as name of caller
* CRRAINB-3576 : Contacts list not entirely displayed (1.46.399 iOS)                
* CRRAINB-3511 : iOS: no switch to the Rainbow conversation screen when making PBX calls                
* CRRAINB-3490 : Zendesk#18778 // Issue to pass a call with iOS                
* CRRAINB-3446 : Zendesk #18920 // Deleting a File, has deleted the associated message on desktop - but not on iOS
* CRRAINB-3403 : Rainbow/oxe:  the application's  conversation screen display is missing                 
* CRRAINB-3329 : impossible to share a screenshot with  Rainbow' user (search function)                
* CRRAINB-3326 : Media Pillar: the mobile is ringing without incoming call screen                
* CRRAINB-3302 : Zendesk#18479 // Cannot change status from away to online
* CRRAINB-3293 : MP:the conversation screen disappears after an second incoming call                
* CRRAINB-3262 : Zendesk#18408 // Loss of call, when you switch from WIFI to 4G                
* CRRAINB-3116 : Zendesk#17970 //  WebRTC Gateway : caller ID is showed as PBX ID (long number instead of name of the contact)                
* CRRAINB-1941 : no more audio after an cellular incoming call released by remote                
* CRRAINB-3665 : User can still join a conference in an archived bubble                 
* CRRAINB-3621 : Conference is still active after user terminates app                
* CRRAINB-3457 : Conversation list shows  multiple conferences to be active at same time                 
* CRRAINB-3305 : Zendesk#18510 // Add to my Network asked whereas contat already in user's network            
* CRRAINB-2485 : Zendesk#16023 // Wrong display of the caller name in the conversation overview  
* CRRAINB-2526 : Cannot see the avatar of a user without having added him to his contacts
* Fix for https://rink.hockeyapp.net/manage/apps/522894/app_versions/126/crashes/20360211413

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Push : Notification are not removed from iPhone screen when message is read from another device
* Push : No notifications of invitation to join network
* Entries in Rainbow menu displayed in contact integration can display entries twice

### Developer notes
* Fix search bug since we don't have anymore the email address of people that are not in our company
* With fix for CRRAINB-2485, this fix pb around the "You" in the last message (isOutgoing always NO from cache)
* Add condition UI side to not add *** *** contact (appears when we ask to display contacts list too early)
* Stop setting a user as "in roster" too early, but now it's done after rainbow server request for roster vcard (need isTerminated): avoid *** *** contact
* Add condition (no prefix tel_) before setting a user as "isInRoster" in XMPPService (avoid user not in roster but displayed in roster)
* Bunch updates of external dependencies (sometimes wrong)

## [1.46.401] - [2018-09-14]

### Added
* Add Japanese and Korean localization languages and update translations

### Fixed
* CRRAINB-3574 : Message notification did not load bubble thread
* CRRAINB-3459 : Login takes too long
* CRRAINB-3552 : Error while trying to join Conference "error message"
* CRRAINB-3557 : Disable conference button after user clicks on it  once
* CRRAINB-3348 : While trying to connect to an SFU I get an error message

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Push : Notification are not removed from iPhone screen when message is read from another device
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble
* Entries in Rainbow menu displayed in contact integration can display entries twice

### Developer notes
* Google analytics for Firebase less verbose
* Update join conference request to avoid saving the request in cache when the request fails
* Update conference info when load room details
* Disable join conference button and display activity indicator when user click the join conference button

## [1.46.400] - [2018-09-12]

### Added
* New API to remove someone from your network

### Fixed
* CRRAINB-2383 : Zendesk#15720 // login button totally hidden by the keyboard (missing in previous build)
* CRRAINB-3139 : Change activation code error message (missing in previous build)
* CRRAINB-3140 : Change flow - User can't correct the wrong verification code (missing in previous build)

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Push : Notification are not removed from iPhone screen when message is read from another device
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble
* Entries in Rainbow menu displayed in contact integration can display entries twice

## [1.46.399] - [2018-09-07]

### Added
* WebRTC update from google (GoogleWebRTC-1.1.22973) for DTMF support
* Google Analytics for Firebase for IM events
* Add Voicemail feature
* RQRAINB-983 : New API - transfer a file you have received

### Fixed
* CRRAINB-3328 : Bad display after sending a pdf file

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Push : Notification are not removed from iPhone screen when message is read from another device
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble
* Entries in Rainbow menu displayed in contact integration can display entries twice

## [1.45.2.398] - [2018-09-03]
### Fixed
* CRRAINB-3095 : Zendesk#17915 // Wrong order of IM

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Push : Notification are not removed from iPhone screen when message is read from another device
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble
* Entries in Rainbow menu displayed in contact integration can display entries twice

## [1.45.1.397] - [2018-08-31]
### Fixed
* CRRAINB-3301 : MP - the mute button isn't working
* CRRAINB-3381 : MP - when the web takes the call, the communication is  also displayed  on iphone
* CRRAINB-3051 : PGi - wrong UI when user has no phone number in his profile
* CRRAINB-3071 : PGi - no phone numbers in Dial in tab
* CRRAINB-3110 : Zendesk#17978 // WebRTC conference in Bubble still active on iOS whereas conf stopped one hour before

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Push : Notification are not removed from iPhone screen when message is read from another device
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble
* Entries in Rainbow menu displayed in contact integration can display entries twice

## [1.45.396] - [2018-08-28]
### Fixed
* CRRAINB-3468 : iOS: PGi meetings with hang up icon whilst these meetings are not started
* CRRAINB-3453 : "Uploading file(s)" pop-up remains displayed when sharing a picture from native OS feature
* CRRAINB-3354 : Lost connection with 1.45.392 iOS application
* CRRAINB-3455 : Wrong timestamp on a conversation when clicking on it and coming back to the homepage (1.45.395 iOS)
* Fix issue with conversation empty last message
* Fix bad conversation order behaviors

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Push : Notification are not removed from iPhone screen when message is read from another device
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble
* Entries in Rainbow menu displayed in contact integration can display entries twice

## [1.45.395] - [2018-08-24]
### Fixed
* CRRAINB-3432 : "Unknown" contacts are appearing while wanting to add participants to a bubble/meeting (1.45.394 iOS)
* CRRAINB-3101 : Zendesk#17946 // Bubble avatar build up at app launch

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Push : Notification are not removed from iPhone screen when message is read from another device
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble
* Entries in Rainbow menu displayed in contact integration can display entries twice

## [1.45.394] - [2018-08-22]
### Added
* CRRAINB-3202 : the tone notification is missing after starting the sfu conference

### Fixed
* Fix overlapping button and title in WebRTC video calling interface
* CRRAINB-2781 : Zendesk#16862 // crash of iOS app 1.41.371 - EXC_BAD_ACCESS (SIGSEGV), KERN_INVALID_ADDRESS at 0x00000000108c1730
* CRRAINB-3331 : App crash when saving a media file while access on device photos is disabled 
* CRRAINB-3371 : crash of the application after "my rainbow sharing" selection
* CRRAINB-3095 : Zendesk#17915 // Wrong order of IM (iOS)
* CRRAINB-3144 : Message counter is not cleared if messages are opened from different session
* CRRAINB-3101 : Zendesk#17946 // Bubble avatar build up at app launch
* Fix the trying to forever join bubbles even after server has returned a error

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Push : Notification are not removed from iPhone screen when message is read from another device
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble
* Entries in Rainbow menu displayed in contact integration can display entries twice

## [1.45.393] - [2018-08-14]
### Fixed
* CRRAINB-3338: User can initiate two video conferences at two different bubbles and at same time
* CRRAINB-3307: [MP] the conversation screen display is missing (mobile in sleeping mode)
* CRRAINB-3308: [MP] the communication is without audio (the mobile was in sleeping mode)
* CRRAINB-3344: Unknown user (*** ***) using Rainbow 1.45.392 iOS
* The first presence was not always sent at login --> fixed (appId was not sent in that case)

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Push : Notification are not removed from iPhone screen when message is read from another device
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble
* Entries in Rainbow menu displayed in contact integration can display entries twice

## [1.45.392] - [2018-08-09]
### Fixed
* CRRAINB-3300: Crash of the application  after searching a user (we previously sent a pdf)
* CRRAINB-3047: iOS: bad behaviors when trying to download a big file
* CRRAINB-3248: Failure to view a video file
* CRRAINB-3242: Video sent in chat appears as text not media type
* CRRAINB-3233: No button to join a meeting on iOS 1.44.389
* CRRAINB-3334: Anonymized users are not filtered in Contacts tab
* CRRAINB-3305: Zendesk#18510 // Add to my Network asked whereas contat already in user's network
* CRRAINB-3216: Not connected error message on 1.44.388
* Fix popup which asks for update (now retrieves every 12 hours from server instead of always from cache)
* Fix no connection issue
* Fix on the rework conferences
* Fix crash when comparing contact with conversation object
* Fix crash for synchronization
* Work on the Push call Mediapillar

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Push : Notification are not removed from iPhone screen when message is read from another device
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble
* Entries in Rainbow menu displayed in contact integration can display entries twice

## [1.45.391] - [2018-08-01]
### Added
* RQRAINB-1012 : Preview of PDF files

### Fixed
* CRRAINB-2855 Rainbow iOS app crashes and call gets failed when initiating call from rainbow web client
* CRRAINB-2740 Zendesk#16767 // Weird double entries in conversation feed iOS
* CRRAINB-2830 Zendesk#16980 // Can not join PSTN conf√©rence from my iOS : no numbers diplayed (also on 1.42.375 iOS)
* CRRAINB-3206 VoIP setting shall not be displayed if user is not able to do VoIP calls
* CRRAINB-3214 Weird double entries in conversation feed iOS (1.44.388)
* CRRAINB-3204 Deactivate "create a bubble" button when network is off
* Fix HockeyApp crash https://rink.hockeyapp.net/manage/apps/522894/app_versions/106/crash_reasons/229041246
* Fix HockeyApp crash https://rink.hockeyapp.net/manage/apps/522894/app_versions/107/crash_reasons/230511545 
* Fix HockeyApp crash https://rink.hockeyapp.net/manage/apps/522894/app_versions/107/crash_reasons/230386977
* Fix HockeyApp crash https://rink.hockeyapp.net/manage/apps/522894/app_versions/107/crash_reasons/229558910
* Fix HockeyApp crash https://rink.hockeyapp.net/manage/apps/522894/app_versions/107/crash_reasons/230503664
* Fix HockeyApp crash https://rink.hockeyapp.net/manage/apps/522894/app_versions/106/crash_reasons/229022978
* Fix HockeyApp crash https://rink.hockeyapp.net/manage/apps/522894/app_versions/107/crash_reasons/230355454
* Fix HockeyApp crash https://rink.hockeyapp.net/manage/apps/522894/app_versions/108/crash_reasons/230077298
* Fix HockeyApp crash https://rink.hockeyapp.net/manage/apps/522894/app_versions/107/crash_reasons/230271837
* Load presences for contact in conversations in priority
* Load room cache asap so conversation wall room avatar appears in a single shot
* Add tools to anonymize Push Request and user information body
* Rework WebRTC conferences for the latest API
* Enhancement 3PCC call with webRTC gateway (no push notif in background)

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Push : Notification are not removed from iPhone screen when message is read from another device
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble
* Entries in Rainbow menu displayed in contact integration can display entries twice

## [1.44.1.390] - [2018-08-02]
### Fixed
* Fix issue with updates presence coming from resume mode
* Fix popup which asks for update (cache issue from library used to check new version)

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Push : Notification are not removed from iPhone screen when message is read from another device
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble
* Entries in Rainbow menu displayed in contact integration can display entries twice

## [1.44.389] - [2018-07-26]
### Fixed
* CRRAINB-3030: Bubble notification issue using Rainbow 1.43.1.383 iOS application
* CRRAINB-3032: Zendesk#17706 // Make the dial-in PGi efficient, showing the home country of the user first
* CRRAINB-3041: Meeting not displayed
* CRRAINB-3047: Bad behaviors when trying to download a big file
* CRRAINB-3170: Avatar not displayed at application start
* CRRAINB-3180: Contact list is empty 
* Conversations double entries issue
* When a privilege (user, moderator, owner) was changed for a room participant his status was set to unknown and privilege was not updated
* Remove message from saved cache when sent succeed
* Fixed inconsistency for nomadic status between user settings screen and telephony settings
* Add mecanism to avoid lots of UI updates after a resume (only one update at the end)

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Push : Notification are not removed from iPhone screen when message is read from another device
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble
* Entries in Rainbow menu displayed in contact integration can display entries twice

## [1.44.388] - [2018-07-19]
### Fixed
* The button to schedule a conference is working again
* CRRAINB-2835 : Presence displays away of all my network
* CRRAINB-2531 : Wrong contacts status (almost all the contacts are "away for several seconds") on Rainbow 1.41.366 iOS application
* CRRAINB-3001 Zendesk #17631 // 2 bubbles of the same user 1.42.377 iOS
* CRRAINB-3112 Zendesk#17988 // ALE user not connected to another ALE user, "add to my network"
* CRRAINB-3113 Zendesk#17977 // Push notif of IM in WebRTC bubble / IM not reported in the list 
* CRRAINB-2740 Zendesk#16767 // Weird double entries in conversation feed iOS
* CRRAINB-2848 IM disappears while back and forth a bubble
* CRRAINB-2861 Rainbow 1.42.376 iOS application // Persistent red banner
* CRRAINB-2939 Rainbow 1.43.378 // iOS don't show the full conversation done on a PC 
* CRRAINB-2941 OXO Rainbow] Nomadic - No telephony menu in the iOS application
* CRRAINB-3072 Zendesk#17835 // IM appearing on conversation list but NOT in the conversation window on iOS
* CRRAINB-3117 Zendesk#17984 // Missing IM in a WebRTC Bubble on iOS / Had to kill and relaunch to see it all
* CRRAINB-2562 Zendesk#16260 // Rainbow is taking a very long time to refresh (iOS)
* CRRAINB-3077 Zendesk#17856 // Empty bubble on iOS

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Push : Notification are not removed from iPhone screen when message is read from another device
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble
* Entries in Rainbow menu displayed in contact integration can display entries twice

## [1.43.3.387] - [2018-07-18]
### Fixed
* Fix HockeyApp crash https://rink.hockeyapp.net/manage/apps/522894/app_versions/106/crash_reasons/228758613

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Push : Notification are not removed from iphone screen when message is read from another device
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble
* Entries in Rainbow menu displayed in contact integration can display entries twice

## [1.43.2.386] - [2018-07-13]
### Fixed
* CRRAINB-3052 : iOS: no access to the user's phone numbers when trying to call a contact
* Show the application update popup every 3 hours instead of once a day
* Change the bubble avatar during loading (from the grey man to a 3 parts bubble avatar)
* CRRAINB-2021 : Zendesk #14579: inconsistency on archived bubbles display (iOS versus Desktop)
* CRRAINB-2610 : A bubble created by the user doesn't appear in "My bubbles" but only in the conversation home page
* CRRAINB-2417 Zendesk#15642 // Crash on IOS
* CRRAINB-2984 Zendesk #17577 // Message received twice with 24h of difference
* CRRAINB-2993 Zendesk#17606 // Call ghosts
* CRRAINB-2999 MP: sometimes the button to make a call is missing  (tab IM)
* CRRAINB-3051 PGi - wrong UI when user has no phone number in his profile
* CRRAINB-2686 Impossible to set available  presence

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Push : Notification are not removed from iphone screen when message is read from another device
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble
* Entries in Rainbow menu displayed in contact integration can display entries twice


## [1.43.2.385] - [2018-07-09]
### Fixed
* Improved message loading with poor network conditions
* Fix mute button display

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Push : Notification are not removed from iphone screen when message is read from another device
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble
* Entries in Rainbow menu displayed in contact integration can display entries twice

## [1.43.1.384] - [2018-07-06]
### Fixed
* Reduce space under text in messages bubbles
* Save messages in cache when receive remote notifications
* Integration with new API to reset unread messages count for conversation
* Fix issue with loading messages inside conversation
* Fix bugs and crashes

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Push : Notification are not removed from iphone screen when message is read from another device
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble
* Entries in Rainbow menu displayed in contact integration can display entries twice

## [1.43.1.383] - [2018-07-04]
### Fixed
* CRRAINB-2930 Zendesk#17268 // Rainbow on iOS very slow leading to several crashes
* CRRAINB-2933 Zendesk#17308 // Rainbow is totally unusable on my Iphone
* CRRAINB-2955 Zendesk#17478 // Clicking on Emily makes the application crash
* CRRAINB-2989 iOS: crash when starting the app,reviewed
* CRRAINB-3020 Zendesk#16976 // Cannot enter and participate to a bubble whereas still member of the bubble
* CRRAINB-3022 Zendesk#17654 // Bubble displayed whereas no member of this bubble

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Push : Notification are not removed from iphone screen when message is read from another device
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble
* Entries in Rainbow menu displayed in contact integration can display entries twice

## [1.43.1.382] - [2018-07-03]
### Fixed
* Remove clear old logs from app finish launching
* Disable messages grouping
* Fix issue when loading messages and the conversation is synchronized
* Enable push notifications service extension
* Save messages in cache when receive remote notifications
* Integration with new API to reset unread messages count for conversation
* Fix issue with loading messages inside conversation
* Fix crashes

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Push : Notification are not removed from iphone screen when message is read from another device
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble
* Entries in Rainbow menu displayed in contact integration can display entries twice

## [1.43.1.381] - [2018-06-29]
### Fixed
* CRRAINB-2938 Rainbow oxe :crash of the application after an outgoing call
* CRRAINB-2944 "Zendesk #17181 // IM are not sent by iOS ""quick answer"" function"
* CRRAINB-2529 Lost of messages when switching from portrait mode to landscape mode
* Remove presence logs when we retrieve contacts from cache (and then all presences)
* Fix crashes

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Push : Notification are not removed from iphone screen when message is read from another device
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble
* Entries in Rainbow menu displayed in contact integration can display entries twice

## [1.43.1.380] - [2018-06-27]
### Fixed
* Acknowledge every 3 messages from server

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Push : Notification are not removed from iphone screen when message is read from another device
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble
* Entries in Rainbow menu displayed in contact integration can display entries twice

## [1.43.1.379] - [2018-06-25]
### Fixed
* CRRAINB-2680 Zendesk#16619 // Position of the notification icon
* CRRAINB-2468 The "Hang up" button doesn't work during a webRTC conference on Rainbow
* CRRAINB-1384 Bad offline message while in a conversation
* CRRAINB-2782 File sharing doesn't work
* Fix Save message when receive remote notifications (enable notifications extension)

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Push : Notification are not removed from iphone screen when message is read from another device
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble
* Entries in Rainbow menu displayed in contact integration can display entries twice


## [1.43.378] - [2018-06-22]
### Added
* RQRAINB-949 GDPR compliance : suppress user account
* RQRAINB-895 PBX telephony: search for PBX contacts
* RQRAINB-927 3PCC on WebRTC Gateway (iOS): make call (cancel call), clear call
* RQRAINB-928 3PCC on WebRTC Gateway (iOS): take call
* RQRAINB-952 3PCC on WebRTC GW (iOS): ignore incoming WebRTC call while already engaged in a call

### Fixed
* CRRAINB-2782 File sharing doesn't work
* CRRAINB-2832 Weird blinking of black screens when installing from scratch
* CRRAINB-2925 Zendesk#17250 // iPhone client crashes during setup after installation (related to Survey BOT)
* Fix HockeyApp crash // https://rink.hockeyapp.net/manage/apps/522894/app_versions/92/crash_reasons/224082654
* Fix HockeyApp crash // https://rink.hockeyapp.net/manage/apps/522894/app_versions/92/crash_reasons/224436549

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Push : Notification are not removed from iphone screen when message is read from another device
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble
* Entries in Rainbow menu displayed in contact integration can display entries twice

## [1.42.377] - [2018-06-15]
### Fixed
* CRRAINB-2692 Zendesk #16667 // When clicking twice on the conversation button, the list does not scroll to the top. On line is still hidden
* CRRAINB-2858 [OXO Rainbow] Intercation problems between iOS and desktop applications
* CRRAINB-2757 Zendesk#16813 // Contacts and Bubble names are erroneous
* CRRAINB-2873 Telephony menu item in profile menu is no longer available,reviewed
* CRRAINB-2715 Zendesk#16730 // Error while trying to download a file
* CRRAINB-2740 Zendesk#16767 // Weird double entries in conversation feed iOS
* CRRAINB-2824 Zendesk#16711 // Cannot create bubble, the + button does not work
* CRRAINB-2834 Unknown using Rainbow 1.42.376

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Push : Notification are not removed from iphone screen when message is read from another device
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble
* Entries in Rainbow menu displayed in contact integration can display entries twice


## [1.42.376] - [2018-06-11]
### Fixed
* CRRAINB-2774 Empty network with latest release : Rainbow 1.42.374
* CRRAINB-2811 iOS: global counter of new notifications in Conversations tab is not ok
* CRRAINB-2781    Zendesk#16862 // crash of iOS app 1.41.371 - EXC_BAD_ACCESS (SIGSEGV),  KERN_INVALID_ADDRESS at 0x00000000108c1730
* CRRAINB-2663    Some messages are not grouped into the same dialogue even when same time stamp (Rainbow 1.41.370 iOS)
* Disable notification service extension that cause freeze of the application
* Enable remote notification support (Require server evolution to be fully functional)

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Push : Notification are not removed from iphone screen when message is read from another device
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble
* Entries in Rainbow menu displayed in contact integration can display entries twice

## [1.42.375] - [2018-06-07]

### Fixed
* CRRAINB-2361 Zendesk#15655 // German Translation on iOS
* CRRAINB-2379 Zendesk#15723 // Missed Portuguese Translation on iOS
* CRRAINB-2575 Zendesk#16008 // Missing IM on a conversation. A back and forth is necessary
* Avoid duplicating ressources with app extensions
* Fix 3D Touch action in conversation home page

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Push : Notification are not removed from iphone screen when message is read from another device
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble
* Entries in Rainbow menu displayed in contact integration can display entries twice

## [1.42.374] - [2018-06-05]

### Fixed
* CRRAINB-2417    Zendesk#15642 // Crash on IOS
* CRRAINB-2699    Zendesk#16686 // Permanent remission of iOS notifications using 1.41.1.372 iOS application
* CRRAINB-2399    iOS: "Add to my network" action still available even if user is in my network
* CRRAINB-2677    iOS: red panel indicating that there is no network whilst network is available
* CRRAINB-2726    iOS: can no longer search for public contacts
* CRRAINB-2485    Zendesk#16023 // Wrong display of the caller name in the conversation overview (IOS)
* CRRAINB-2730    iOS: Nomadic icon still indicates "ringing" even if mobile will not ring
* CRRAINB-2631    Deleted recent numbers reappear (Rainbow 1.41.370 iOS)
* CRRAINB-2731    iOS: setting "Ring my mobile phone" is not updated when routing is changed from another device

### Added
*  When the user accepts or declines a room invitation, manage the states of the buttons to know which button is clicked

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Push : Notification are not removed from iphone screen when message is read from another device
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble
* Entries in Rainbow menu displayed in contact integration can display entries twice

## [1.41.2.373] - [2018-05-30]
### Fixed
* CRRAINB-2697 : Can no longer communicate with public users 

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Push : Notification are not removed from iphone screen when message is read from another device
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble
* Entries in Rainbow menu displayed in contact integration can display entries twice

## [1.41.1.371] - [2018-05-28]
### Fixed
* Fix white banner layout issue in conversation

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Push : Notification are not removed from iphone screen when message is read from another device
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble
* Entries in Rainbow menu displayed in contact integration can display entries twice

## [1.41.371] - [2018-05-25]
### Fixed
* CRRAINB-2576 : Cannot chat with the bot "Robby"
* Web update of the avatar has taken 11 min to be refreshed on the iOS client
* Cannot scroll up the conversation homepage which is not completely displayed (Rainbow 1.41.370 iOS)
* Telephony menu is not displayed sometimes.

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Push : Notification are not removed from iphone screen when message is read from another device
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble
* Entries in Rainbow menu displayed in contact integration can display entries twice

## [1.41.370] - [2018-05-22]
### Fixed
* CRRAINB-1461 : iOS: suppress "no conversation" screen at startup
* CRRAINB-1454 : No conversation created
* CRRAINB-2547 : The app crashed while trying to write a message
* CRRAINB-2560 : No network connection even if the smartphone is connected to internet
* CRRAINB-2567 : IM reception delay: IM is notified on iPhone but when the app is opened the IM is not showed
* CRRAINB-2585 : iOS app automatically does a logout
* CRRAINB-2521 : Empty number entries in call-log
* CRRAINB-2523 : User shall not be able to display a contact card for calls with no identity

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Push : Notification are not removed from iphone screen when message is read from another device
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble
* Entries in Rainbow menu displayed in contact integration can display entries twice

## [1.41.368] - [2018-05-14]
### Added
* Add push service extension to handle received push and saved them into cache (server evolution needed)

### Fixed
* Zendesk#16170 // Connection not established on WebRTC call (Beta 1.41.366 iOS)
* CRRAINB-2130 : When I add a picture to a text in an IM. Part of the text is erased.
* CRRAINB-2391 : iOS: no action when accepting a bubble invitation
* CRRAINB-2392 : Zendesk#15750 // No new messages although notifications displayed on IOS (Beta 1.40.364)
* CRRAINB-2415 : Zendesk#15767 // Rainbow is frozen in “Call in progress”. The audio call I made was not answered.
* CRRAINB-2493 : Zendesk#16056 // Back in a bubble brings back to global conversation and not list of bubbles
* CRRAINB-2524 : iOS: filtering of Microsoft Office files does not work
* CRRAINB-2525 : iOS: bad ordering when filtering files by size

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Push : Notification are not removed from iphone screen when message is read from another device
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble
* Entries in Rainbow menu displayed in contact integration can display entries twice

## [1.41.366] - 2018-05-04
### Added
* Minimal deployment target is now iOS 10
* Fetch ICE server using new geolocalized API
* RQRAINB-81: Refactoring of file download/upload (native OS share feature)
* RQRAINB-837 : iOS: Call forwarding
* RQRAINB-840 : iOS: Redesign of file sharing interface when sharing from any app
* RQRAINB-820 : iOS: UI enhancements of My Rainbow sharing

### Fixed
* Fix freeze when hangup a call
* Don't count answered call logs in conversation unread messages
* CRRAINB-2439 : Crash when a plus sign is typed on the search field
* Fix strange case where connection to xmpp says it connected but it's not the case
* CRRAINB-2233 : The license is not fully displayed
* CRRAINB-2467    Phone button to join a webRTC conference useless on Rainbow 1.40.365 iOS application
* CRRAINB-2300    iOS: still unknown users in bubbles
* CRRAINB-2301    iOS: still unknown users in Contacts tab
* CRRAINB-2368    Zendesk#15634 // "No network connection" message even with Wifi on IOS
* CRRAINB-2449    Zendesk#15889 // No access to a bubble on iOS (1.40.364)
* CRRAINB-2463    Zendesk#15938 // Can not find contact when creating a bubble
* CRRAINB-2474    iOS: Archiving a bubble is NOK (production version) - regression
* CRRAINB-2381    Zendesk#16045 // A bubble has disappeared - I had too reinstall the iOS app
* CRRAINB-2478    Zendesk#15981 // unread notification "1" after a successful call
* CRRAINB-2144    Wording when stopping to share a file
* Fix pb when adding a contact into roster that is not marked as inRoster

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Push : Notification are not removed from iphone screen when message is read from another device
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble
* Entries in Rainbow menu displayed in contact integration can display entries twice

## [1.40.365] - 2018-04-24
### Added 
* Display a list of invited guests in room details
* Rework app extension

### Fixed
* CRRAINB-2065 : crash of the application after video suppression
* CRRAINB-2289 : Today, 3 iOS crash // Rainbow 1.39.362
* CRRAINB-2347 : Zendesk#15584 // Multiparty Conference with Audio and screen share - Crashed IOS
* CRRAINB-2442 : impossible to display the version's name (not possible to send the log file)
* CRRAINB-999 : the selection of the contact isn't done ( sharing  through safari by using the application rainbow)
* CRRAINB-2190 : Guest participants not seen by bubble creator on iOS
* CRRAINB-1965 : iOS: no Delete menu in bubble
* CRRAINB-2144 : Wording when stopping to share a file
* Zendesk#15641 // No name on PSTN calls received on iOS (only number)

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Push : Notification are not removed from iphone screen when message is read from another device
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble
* Entries in Rainbow menu displayed in contact integration can display entries twice

## [1.40.364] - 2018-04-13
### Added 
* iOS : Audio conferenciung PGI - redirection of email invitation
* iOS : Nomadic mode for OXO users
* iOS : Finalize Guest invitation mode
* iOS : Statistics of deployed versions
* iOS : Audio conference PGI : manage only one instant meeting

### Fixed
* Notifications received but IM not visible in conversation tab on iOS
* the communication isn't released on the iphone.
* When I add a picture to a text in an IM. Part of the text is erased.
* Can not find a bubble I am a member of (Beta iOS)
* Zendesk#15648 // Conversation list is not updated on IOS
* after an search in the azure directory, the contact is twice displayed
* iOS - Wrong information when searching for a contact that is not in your company
* iOS: Quit & Archive actions do no longer appear in the menu
* Display of company to be fixed
* Web update of the avatar has taken 11 min to be refreshed on the iOS client
* Zendesk#14754 // Audio switch from headset to speaker when changing from audio to video call on iOS
* Cannot remove a archived bubble
* Zendesk#14352 // Display issue when saving a phone number received by IM on IOS
* iOS: no Delete menu in bubble
* the calls in  secret identity mode are grouped  (call log)
* Fix pastboard content management

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Push : Notification are not removed from iphone screen when message is read from another device
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble
* Entries in Rainbow menu displayed in contact integration can display entries twice

## [1.39.1.363] - 2018-04-13
### Fixed
* Use the Janus Jid as is for GeoLoc SFU

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Push : Notification are not removed from iphone screen when message is read from another device
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble
* Entries in Rainbow menu displayed in contact integration can display entries twice

## [1.39.362] - 2018-03-29
### Fixed
* Fix crash when entering in My rainbow sharing
* Filtering button displayed in wrong screen (Received files)
* Save sort and filter state in My Rainbow Sharing

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Push : Notification are not removed from iphone screen when message is read from another device
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble
* Entries in Rainbow menu displayed in contact integration can display entries twice


## [1.39.361] - 2018-03-28
### Added
* iOS: Enhanced security for changing user password
* XMPP resource name contain '_sdk' when the SDK is used in CPaaS context
* At login time send 'sdk_ios' for X-Rainbow-Client http header in CPaaS context
* Allow guest to finalize his account
* Add support meeting email invitations
* Simplify onBoarding screens
* Save in cache needed informations for a quick application launch
* WebRTC conversations statistics
* Allow filtering and sort in My Rainbow sharing view
* Add share and suppress file actions

### Fixed
* iOS: no presence anymore in Conversations tab
* Please don't send again the notifications while opening the iOS app
* iOS: message indicating that User is no longer in the bubble whilst he's continuing to chat in the bubble
* Bad behavior of iOS when password is changed via Web
* Notifications received for a conversation despite being disabled
* iOS: latest message received is badly placed in the Conversations tab
* Zendesk#13077 // I have the text from a contact says to add this person to my network, when already are in my network.
* iOS: Resume when swiping the app
* iOS: Share and suppress files
* iOS: Enhanced security for changing user password
* Bubble avatar is not the same at app launch and after opening the bubble
* iOS: conversation statistics
* Wording when stopping to share a file
* Propose to remove a bubble which not belong to me

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Push : Notification are not removed from iphone screen when message is read from another device
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble
* Entries in Rainbow menu displayed in contact integration can display entries twice

## [1.38.360] - 2018-03-19
### Fixed
* Fix problem with guest invitation that could not be accepted after first installation
* Propose to remove a bubble which not belong to me

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Push : Notification are not removed from iphone screen when message is read from another device
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble
* Entries in Rainbow menu displayed in contact integration can display entries twice


## [1.38.359] - 2018-03-14
### Fixed
* iOS - No calllog entry for PBX calls answered directly on the phone
* iOS not updated live on licence change
* Web update of the avatar has taken 11 min to be refreshed on the iOS client
* Remove authorization header for avatar download
* Fix some crash

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Push : Notification are not removed from iphone screen when message is read from another device
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble
* Entries in Rainbow menu displayed in contact integration can display entries twice

## [1.38.358] - 2018-03-09
### Added
* iOS: Guests in bubbles/video conferences - redirect email invitations

### Fixed
* Audio sound not played when a recording is started
* Impossible to add Christophe Muninger to the bubble named « Workspace » from the iPhone app
* iOS: invitation of a contact accepted but contact does not appear in my network
* black screen after using the recording function
* Search in desktop does not give priority on my own conversation and bubbles
* Zendesk#14328 // On iphone 8 model A1905 in German language it is not possible to scroll the info page
* iOS: bubble notification but no conversation displayed at the top
* No video when escalading call with android device

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Push : Notification are not removed from iphone screen when message is read from another device
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble
* Entries in Rainbow menu displayed in contact integration can display entries twice


## [1.38.357] - 2018-03-05
### Fixed
* Fix crash at startup due too call log without callee in unknown conditions.

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Push : Notification are not removed from iphone screen when message is read from another device
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble
* Entries in Rainbow menu displayed in contact integration can display entries twice

## [1.38.356] - 2018-03-02
### Added
* Data consumption (requires new api on server side)
* Video conference : manage reconnections
* Video conference : Video and screen sharing
* Video conference (iOS): bubble compliance with Web/Desktop apps
* iOS: Log of PBX calls
* Multiple administrators of a bubble: display the new owner
* Suppress join/leave notifications from bubbles
* Refactoring authentication applicative
* iOS: Suppress join/leave notifications from bubbles

### Fixed
* Join conference through dial-in misses passcode numbering
* The conference meeting shows previous participants
* Zendesk#7929 // IM not synchronized on iPhone (answered to IM via Dictation feature iWatch)
* Not possible to change the name of my bubble
* Meeting creation fails
* Join instant meeting fails
* Zendesk#13192 Multiple time audio conference dial in numbers : Rainbow 1.34.341 iOS application logs for user dany.jenneve@al-enterprise.com
* Zendesk#13366 // Unknown contacts using Rainbow 1.35.342 iOS application  (requires new api on server side)
* iOS: very high data consumption
* Zendesk#13730 // An image sent via IM is not displayed on iOs
* Please don't send again the notifications while opening the iOS app
* The keyboard is partly covering the "connexion" button
* Application sharing - webrtc - green top bar is not clickable on iPhone X
* the file preview on IOS isn't working
* iOS: no file displayed when opening a conversation
* Zendesk#14511 // In a Bubble, conversation timestamps do not appear
* Zendesk#14597 // Wrong conversation order on iPhone with 1.37
* Strange icon for a file

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Push : Notification are not removed from iphone screen when message is read from another device
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble
* Entries in Rainbow menu displayed in contact integration can display entries twice

## [1.37.355] - 2018-02-21
### Fixed
* Join conference through dial-in misses passcode numbering
* iOS: remove all messages from a conversation crashes the app
* sometime the escalation in video isn't working
* Fix crashes on search and in file sharing

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Push : Notification are not removed from iphone screen when message is read from another device
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble
* Entries in Rainbow menu displayed in contact integration can display entries twice

## [1.37.354] - 2018-02-13
### Fixed
* Remove presence icon on my avatar displayed on top left of the application
* Zendesk#12746 // Textbox disappears when using Emoji screen on iPhone X
* iOS: calendar info
* iOS: entries are duplicated in bubbles
* Zendesk#14240 // Unknown users in a WebRTC conference
* Fix crash when scrolling in my rainbow sharing
* Fix problem with message date when loading conversation from server
* Messages was no more grouped
* Only owner of a bubble can archive and delete it
* Fix problem with P2P video + sharing
* Display SMS composer when inviting someone by phone number

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Push : Notification are not removed from iphone screen when message is read from another device
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble
* Entries in Rainbow menu displayed in contact integration can display entries twice


## [1.37.353] - 2018-02-09
### Added
* File sharing update and download by chunk
* File sharing preview automatic download
* P2P conversations : Show video and desktop sharing
* Invite a contact by email address or phone number
* Add API to define custom data in bubble (CPAAS)
* WebRTC 63 update (from google directly)
* Add API for channels (CPAAS)
* Add translations
* Display participant status and owner of the bubble in the detail of a bubble
* Add quit/archive action in conversation menu and swipe action

### Fixed
* Meeting creations are still possible after the right has been removed
* Can not create meeting right after being given the conversation rigth
* File transfer from iPhone photo library has HEIC extension
* Zendesk#13386 // Contacts listed first name - last name on iOS
* error 409 after sending a second invitation
* after supprssion of a bubble's participant, the update isn't done on iOS
* First-name order does not respect the setting of the iPhone
* The application freezes when I try to add a photo to the Bubble
* Zendesk#14111 // Cannot possible to insert a file in a Bubble
* iOS: wrong ordering of messages
* Zendesk#12284 // A bubble transformed into a meeting
* Fix crashes

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Push : Notification are not removed from iphone screen when message is read from another device
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble
* Entries in Rainbow menu displayed in contact integration can display entries twice

## [1.36.352] - 2018-02-02
### Fixed
* Freeze of the application
* Crash while messages are loaded

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Push : Notification are not removed from iphone screen when message is read from another device
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble
* Entries in Rainbow menu displayed in contact integration can display entries twice


## [1.36.351] - 2018-01-30
### Fixed
* Zendesk#13770 // Georgia does not appear among the selected countries in the iOs
* Zendesk#13862 // In a contact has appeared messages that in principle does not belong to that contact.
* Zendesk#13746 // I cannot open a contact conversation
* Fix sort order in room details

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Push : Notification are not removed from iphone screen when message is read from another device
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble
* Entries in Rainbow menu displayed in contact integration can display entries twice

## [1.36.350] - 2018-01-25
### Fixed
* Zendesk#12190 // Avatar not updated automatically
* Zendesk#11944 // IM different order on a Bubble between iOS & Android version
* Zendesk#11400 // No bubble avatar on Rainbow 1.31.319 iOS App
* Same bubble does not have the same pictures during the day
* iOS - Crash when removing a contact from my network

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Push : Notification are not removed from iphone screen when message is read from another device
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble
* Entries in Rainbow menu displayed in contact integration can display entries twice

## [1.36.349] - 2018-01-23
### Added
* Invite local contact at once
* Display the number of attendees in a bubble/meeting
* Manage multiple administrators of a bubble (Need an evolution on server side to work correctly)

### Fixed
* Search result contact is displayed twice
* iOS - Network connection is unstable under medium wifi reception
* File transfer from iPhone photo library has HEIC extension
* Zendesk#13689 // Can not see picture on iOS
* Zendesk#13680 // 1.35 crashing all the time when switching through contact tabs
* Zendesk#13685 // emoticons hiding text bar on iOS

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Push : Notification are not removed from iphone screen when message is read from another device
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble
* Entries in Rainbow menu displayed in contact integration can display entries twice

## [1.35.348] - 2018-01-19
### Fixed
* Fix crashes
* Resend message was not ordered correclty
* File download can failed in specific condition
* No date on outgoing messages cause a wrong order in the conversation
* Hide no network banner when switching network
* Application sucked few seconds when sending a message
* iOS application freeze when switching 4G to wifi
* iOS application freeze with too many contacts
* Bot connex now answer after the send message

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Push : Notification are not removed from iphone screen when message is read from another device
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble
* Entries in Rainbow menu displayed in contact integration can display entries twice

## [1.35.347] - 2018-01-17
### Fixed
* Cannot change the presence
* Some messages don't have date displayed
* Missing meeting tab at first installation
* My rainbow sharing not sorted correclty
* Last message not updated correctly when the server refuse a message that has been sent
* Conversation not created when we got a message from an unknown user
* Empty my lists view

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Push : Notification are not removed from iphone screen when message is read from another device
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble
* Entries in Rainbow menu displayed in contact integration can display entries twice

## [1.35.1.346] - 2018-01-15
### Fixed
* Crash of application at startup.
* Change method use to calculate message timestamp
* Update websocket connector (Socket rocket)

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Push : Notification are not removed from iphone screen when message is read from another device
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble
* Entries in Rainbow menu displayed in contact integration can display entries twice

## [1.35.345] - 2018-01-12
### Fixed
* Layout issue seen by Apple

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Push : Notification are not removed from iphone screen when message is read from another device
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble
* Entries in Rainbow menu displayed in contact integration can display entries twice

## [1.35.343] - 2018-01-08
### Fixed
* Outgoing calls are replayed in loop
* Missing section separator in my network view
* Impossible to take the call on android mobile

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Push : Notification are not removed from iphone screen when message is read from another device
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble
* Entries in Rainbow menu displayed in contact integration can display entries twice

## [1.35.342] - 2018-01-05
### Added
* iOS local contact optimizations (step 2)

### Fixed
* Many crash fixed
* Zendesk#12894 // There is twice the same bubble
* iOS - Label to be changed when there are no missed calls
* Zendesk#3609 // Impossible to connect with iOS using 1.15
* offline mode : after recover the data link, the message isn't sended
* iOS - After an account creation, the app opens the name screen at start up
* Zendesk#13164 // Once starting the app, it takes about 45sec to see my conversations
* Zendesk#13190 // iPhone App crashes very often
* crash after each startup with an account (not a problem with android)
* Zendesk#13265 // Since upgrade to iOS 1.34.341 red banner "No network connection"
* Zendesk#13267 // App crash while sending a file
* Zendesk#13268 // Rainbow Solution appears as Essential instead of Enterprise Demo

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Push : Notification are not removed from iphone screen when message is read from another device
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble
* Entries in Rainbow menu displayed in contact integration can display entries twice


## [1.34.341] - 2017-12-15
### Fixed
* bad management of the nativ contact: it's possible to call an user without account
* Remove "Connecting" orange banner and "Connected" green banner

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Push : Notification are not removed from iphone screen when message is read from another device
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble
* Entries in Rainbow menu displayed in contact integration can display entries twice

## [1.34.340] - 2017-12-13
### Fixed
* Application blocked in "Connecting" when network is poor.
* Can't call in WebRTC right after retrieving network reachability.
* Connection delay when answering a call while the iPhone is locked.
* Could not start the conference
* While joining conference, the meeting screen is stucked in connecting phase
* iOS - Label to be changed when there are no missed calls
* bad management of the native contact: it's possible to call an user without account

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Push : Notification are not removed from iphone screen when message is read from another device
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble
* Entries in Rainbow menu displayed in contact integration can display entries twice

## [1.34.339] - 2017-12-08
### Changed
* Application blocked on startup
* Sometimes webrtc connection could failed because there is no ICE Servers

### Fixed
* Zendesk#12880 // App blocked displaying 'network processing'
* Zendesk#12879 // IM: it displays 'Add to my network' even though the person is already among my contact
* Zendesk#12746 // Textbox disappears when using Emoji screen on iPhone X
* Zendesk#12708 // Rainbow on iPhone X not really convenient typing a message
* iOS: some conversations are displayed twice (or even more) in conversation tab

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Push : Notification are not removed from iphone screen when message is read from another device
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble

## [1.34.337] - 2017-12-01
### Added
* CallKit integration in native contact information screen (audio, video, chat)
* Offline mode
* Refactoring of contacts, use new Apple api

### Fixed
* While joining conference, the meeting screen is stucked in connecting phase
* Zendesk#12720 // crash of the app during access bubble details
* With no sim, impossible to reconnect in wifi
* Zendesk#12703 // incoherent misspelling
* Zendesk#12628 // Ring of incoming call doesn't stop on iOS when answering on desktop app
* iOS counter still present on conversation even if messages read on Desktop app
* Zendesk#12452 // Click on Recent does not lead to the top of the page
* Zendesk#12451 // Wrong display when launching the app on IOS after a kill
* Zendesk#12177 // Not able to establish a WebRTC call between iPhone 5 with iOs 10
* Presence text is wrong
* Zendesk#11491 // iOS client show wrong offline timestamp if user is monitored by CSTA
* Zendesk#11399 // iOS setting for surname/firstname not taken into account by Rainbow
* Company page presentation is not good
* Company logo shape not taken into account
* Zendesk#10771 // Bad display when going into landscape mode during call on iOS.
* iOS: avoid displaying unwanted presence states when starting the app
* Moussa dans le train (suite) - Pas de message en ouvrant une conversation alors que j'ai une notif
* Zendesk#9251 // "Connection in progress" on iOS app.
* iOS - suppress unwanted information during the starting phase
* Bug Moussa Zaghdoud dans le train (mode offline inopérant en fait)
* File sharing event dates not taken into account in conversion screen
* the communication isn't released on the iphone.
* Bad display after joining a company on iOS

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Push : Notification are not removed from iphone screen when message is read from another device
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble

## [1.33.1.336] - 2017-11-29
### Fixed
* when android release the communication , the session is active on ios

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Push : Notification are not removed from iphone screen when message is read from another device
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble

## [1.33.1.334] - 2017-11-28
### Fixed
* Recompile webrtc in release mode
* Zendesk#12685 // REGRESSION 1.33: iOS app - no presence status after logoff/login

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Push : Notification are not removed from iphone screen when message is read from another device
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble

## [1.33.333] - 2017-11-22
### Fixed
* Don't re-emit messages when changing user
* Workaround to avoid sending in loop message with only a file

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Push : Notification are not removed from iphone screen when message is read from another device
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble

## [1.33.332] - 2017-11-20
### Added
* Add new translations

### Fixed
* Zendesk#12490 // Rainbow app crash on iOS (1.32.325)
* Zendesk#12453 // In recent tab the icon is above the time stamp for unknown users

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Push : Notification are not removed from iphone screen when message is read from another device
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble

## [1.33.331] - 2017-11-15
### Fixed
* Bad counter value when re-opening the application
* Wrong conversation order
* Fix some crash

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Push : Notification are not removed from iphone screen when message is read from another device
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble

## [1.33.330] - 2017-11-10
### Added
* Optimizations, bug fixing (incl. “Quonex” issue)

### Fixed
* Zendesk# 12162 // Uncomplete Bubble lists on Iphone 1.32.325

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Push : Notification are not removed from iphone screen when message is read from another device
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble

## [1.32.325] - 2017-10-31
### Fixed
* No way to create a meeting
* Create meeting fails with 403 error
* Zendesk#11902 // Opening the App on IOS (1.31.319) : screen blocked on the conversation page
* Zendesk#11822 // screen issue when creating an account on iPhone

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Push : Notification are not removed from iphone screen when message is read from another device
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble


## [1.32.324] - 2017-10-30
### Fixed
* Some french translations are missing in the join meeting screen
* Crash when joining immediate conference
* No way to join my instant meeting
* Meetings appear 3 times in the My Meetings list

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Push : Notification are not removed from iphone screen when message is read from another device
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble


## [1.32.323] - 2017-10-24
### Fixed
* Zendesk#11670 // Bad display of a conversation on iOS 11.0.3
* Call history: some calla are tagged as Unknown

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Push : Notification are not removed from iphone screen when message is read from another device
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble

## [1.32.321] - 2017-10-20
### Added
* PGI dial-out mobile added
* WebRTC v61 with TLS 1.2 support
* Share audio & video file with Rainbow and with extension
* Build for iOS 11, some improvement on UI linked to iOS 11

### Fixed
* Zendesk#11584 // On iOS app when opening a shared picture from a conversation, I cannot see the action buttons.
* No meeting update event sent to participant until they accept the invitation - iOS part
* validate creation produces no result (No error message)
* Zendesk#10986 // Poor order management of conversations on iOS
* Not possible to change the name of my bubble

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Push : Notification are not removed from iphone screen when message is read from another device
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble

## [1.31.319] - 2017-10-09
### Fixed
* Crash on iOS 11
* Zendesk#10198 // Some dialog areas including pictures seem very big and empty.
* The indication of unread message in the iOS tabs "pastille" are not centered
* Zendesk#11060 // I can not change the picture of my avatar from my library
* Bubble create middle screen button does not work
* Cannot enter subject bubble
* Cannot enter meeting name nor meeting type (iPhone 5S)
* Crash when joining conference
* When creating a buble from a conversation, initial participant is not added in the bubble
* Bad tab display in Recents page

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Push : Notification are not removed from iphone screen when message is read from another device
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble

## [1.31.318] - 2017-09-29
### Added
* Create bubbles and define a new avatar
* Create meetings
* Custom avatar in bubbles/meetings
* Display active calls in conversations tab (only webrtc call)

### Fixed
* Zendesk#10837 // Bad display of "Récents" tab on iOS 1.30.316
* Zendesk#10799 // When I got into a conversation, I see the spinner downloading the conversation forever.
* iPhone: bad labels when add participants in 1:1 conversations
* Zendesk#10304 // Sometimes the buttons "tous/manqués" doesn't appear in call logs on iOS.
* Create a bubble with a name length of 1 char
* iOS device: Unwanted conversation in Conversations panel
* cannot customize bubble avatar
* Zendesk#5083 // Same contact displayed several times on my conversation list

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Push : Notification are not removed from iphone screen when message is read from another device
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble

## [1.30.317] - 2017-09-11
### Added
* Import translations

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Push : Notification are not removed from iphone screen when message is read from another device
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble

## [1.30.316] - 2017-09-18
### Fixed
* Crash when create a bubble with lot of participants
* Zendesk#10632 // if download is slow, it can be stop by using (previous screen)
* iOS: bad French label for calendar info
* Zendesk#10278 // iOs App in background, once launched it crashed
* Blank audio communication when aswering call on iOS 11 GM
* No search bar displayed in iOS 11 GM

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Push : Notification are not removed from iphone screen when message is read from another device
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble

## [1.30.315] - 2017-09-11
### Fixed
* Looking for someone crash the app

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Push : Notification are not removed from iphone screen when message is read from another device
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble

## [1.30.312] - 2017-09-08
### Added
* Schduled conference (step 1)
* Search in Microsoft Active directory
* Resynchronize conversation when a new participant is added in a bubble 
* Custom avatar for bubble

### Fixed
* Reuse JWT token on refocus of the application
* Don't download all avatar all the time
* Zendesk#10376 // Unexpected calendar presence on a bubble on ios app.
* Zendesk#10245 // IM marked as read (blue) on iOS while they were only received.
* Behavior of clients differs when PGI conference is being established
* Zendesk#9868 // After the update of new version on iOS, some participants of a bubble are not identified, they appear with the "?" avatar.
* Duplicated results of company search on iOS

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Push : Notification are not removed from iphone screen when message is read from another device
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble

## [1.29.309] - 2017-08-31
### Fixed
* Automatic Reply status is erased by meetings on iOS
* "Appointment until" in case of full-day meeting
* Automatic Reply icon is not shown in conversation list or contact list

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Push : Notification are not removed from iphone screen when message is read from another device
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble

## [1.29.308] - 2017-08-29 
### Fixed
* Automatic Reply message should be accessible
* Time should not be displayed for end time later than today
* Zendesk#10090 // Interface display issue on iOS: a gray banner appears at the top of the conversations.
* Join PGI conference icon remains visible even if conference is established (iOS)
* PGI active talker information not translated in French (iOS)
* Changes in current meeting are not updated realtime in iOS
* "No appointment today" is not shown in vCard (iOS)
* Translation errors seen by Christophe

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Push : Notification are not removed from iphone screen when message is read from another device
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble

## [1.29.307] - 2017-08-04
### Added
* Invitation by SMS (Requires server 39)
* Help center in about menu
* Licence in profile
* Out of office calendar presence
* CDN: download avatar using the CDN
* Add option to save a file to in Rainbow Sharing from app extension 

### Fixed
* Create bubble fails
* Zendesk#7082 // When bubble user limit is reached, decline message on iOS device is missing
* Black screen when leaving PGI conference
* Active speaker not available when returning in conference screen

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Push : Notification are not removed from iphone screen when message is read from another device
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble

## [1.28.306] - 2017-07-27
### Fixed
* Pop up info for file transfer not in german language
* Zendesk#9495 // A push notification from a bubble does not open the bubble conversation, but the individual conversation.
* Zendesk#9442 // The link "open a conversation " into a contact card from the "recent call" is not working.
* iOS - Does not see contact details for a contact part of my company
* Wrong used quota information if user lost Enterprise licence while above 1 GB
* "Cannot send message" when sending a file over the quota
* the selection of the contact isn't done ( sharing through safari by using the application rainbow)
* device consistency - call log time
* iPhone mobile: active call stays when incoming audio call is declined
* Message failed to establish call and audio call killed
* iPhone mobile: No sound notification on incoming audio call
* Rainbow iPhone mobile application crash
* Fix regression with contact invited not displayed
* Fix regression with sort of messages

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Push : Notification are not removed from iphone screen when message is read from another device
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble

## [1.28.302] - 2017-07-21
### Added
* Add popup to inform the user that there is an update available on store
* Add webrtc statistics in google analytics and in logs
* Add copyright, term of services and privacy data in about view
* Add new translations
* Display Office 365 presence (Busy only)
* First step of Audio conferencing (Instant conf)


### Fixed
* Fix some issues in lists UI

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Push : Notification are not removed from iphone screen when message is read from another device
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble

## [1.27.301] - 2017-07-05
### Fixed
* Call log screen bad graphical rendering
* Empty conversation when opened
* Missing text when opening a conversation
* Received message with attachment lower than 1Mo was not downloaded automatically
* Slow search for companies on iOS
* Zendesk#6484 // Rainbow app crash
* Remove presence banner in message view for connected/connected on mobile presence

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Push : Notification are not removed from iphone screen when message is read from another device
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble

## [1.27.299] - 2017-07-03
### Fixed
* Add missing icon in splashscreen
* Removing "Connected" checkmark on login
* Fix wording in call logs
* Add confirmation popup on delete all in call logs
* Fix bug when deleting call log (local conversation cache was deleted too)
* Fix crash when opening a contact details after searching in call log tab
* Translation errors
* Missing duration in call log when displayed in conversation

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Push : Notification are not removed from iphone screen when message is read from another device
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble

## [1.27.296] - 2017-06-30
### Fixed
* Crash at startup if the calllogs don't have caller or callee

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Push : Notification are not removed from iphone screen when message is read from another device
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble

## [1.27.295] - 2017-06-30
### Added
* New splash screen
* Display avatar in fullscreen
* Colorize one-to-one tchat bubble
* Rainbow contact informations have priority on local contacts info
* Display textual presence
* Display good presence when in WebRTC call
* Allow calling in WebRTC people not in our roster
* Add support of WebRTC video call
* Add support of Desktop sharing with audio only
* Add call log history

### Fixed
* Remove R logo when presence is unknown
* Display webrtc call log last message correctly
* the video escalation between 2 ios isn't working
* Call still active when the user disconnects
* I'm continuously switching to the offline presence state when I'm using the iOS app
* Application blockage in call when going to info user sceen
* Loss of video call buttons
* after making a video call, the ring back tone is missing
* Earpiece active instead of loudspeaker at the beginning of a video call
* application frozen after several key presses on the video button.
* the video is missing on the iphone after deactivation and activation (video) on the android
* impossible to change the user 's photo
* presence/mutidevice - Away presence is kept when a call is done or received and user is connected on WEB ckient and mobile client
* Zendesk#6343 // Wrong notification on iOS call
* Avatar management on iOS: display inconsistency between rainbow vs. local pictures

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Push : Notification are not removed from iphone screen when message is read from another device
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble

## [1.26.291] - 2017-06-15
### Fixed
* crash of the application when the remote set (android) activate the video
* after roaming (in communication) from wlan( cloud-services) to 3g, the connection is always in progress

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Push : Notification are not removed from iphone screen when message is read from another device
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble

## [1.26.288] - 2017-06-15
### Fixed
* Zendesk#7471 // I have received error message when I tried to update my personal information
* Notifications in tab bar while there are no notifications in conversations
* Zendesk#7436 // IM do not merge properly on iOs
* Zendesk#7382 // Presence issue on iOs device
* Empty list creation
* Not possible to change the name of my bubble
* contacts - contact list should be more or equals to minimum 3 characters
* Create a bubble with a name that already exists is posssible
* Cannot add bubble participant
* Contact appears multiple times
* The app displays the contact as a supposed Rainbow user
* Zendesk#6778 // iOS wrong presence status when iPhone app is called from background
* Zendesk#6475//IM sent from iOS notification on lock screen are not delivered
* iOS - Request to join multiple companies
* Bad display after joining a company on iOS
* All Cancel/OK logic should be cancel on Left and OK on Right
* iOS push mobile: two vigration on IM reception when app is closed

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Push : Notification are not removed from iphone screen when message is read from another device
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble

## [1.26.287] - 2017-06-09
### Added
* Rework on webrtc call screen
* Improve offline mode, keep message and send them on next application focus.

### Fixed
* Zendesk#7564 // 28 push notifications and 40 notifications inside the app
* Notification issue: different indicators on Mobile & PC
* Zendesk#7373 // call issues Mobile Ios
* Zendesk#7268 // Notification bellow remains even though the message was read on desktop
* Last presence seen after removing a contact from my network
* Zendesk#6743 // pabx extension lost in company admin menu if iOS device update the work phone number

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Push : Notification are not removed from iphone screen when message is read from another device
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble

## [1.25.285] - 2017-05-24
### Fixed
* crash of the application after an incoming call (3G mode)
* Picture taken at first registration on iOS is not used as avatar
* On a newly created account, the counter for Contacts on iOS is at 1 and can't be acknowledged
* Unread counter not decreased when accepting an join-my-company invitation

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Push : Notification are not removed from iphone screen when message is read from another device
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble

## [1.25.283] - 2017-05-23
### Fixed
* Leaving a bubble as an administrator is possible	
* Create a bubble with a name length of 1 char
* Crash when enrollment when starting a new conversation	
* Business/Enterprise profiling: Step 2 (Restrict/allow feature for Business/Essential/Enterprise service Plan			
* Picture taken at first registration on iOS is not used as avatar
* First attempt to invite a contact on iOS fails
* On a newly created account, the counter for Contacts on iOS is at 1 and can't be acknowledged
* Picture taken from camera works poorly on iOS client
* Unread counter not decreased when accepting an join-my-company invitation
* iOS user not able to move to online status
* Button "invite" has no effect in the search result page on
* Impossible to click on resent/cancel for an invitation

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Push : Notification are not removed from iphone screen when message is read from another device
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble

## [1.25.279] - 2017-05-19
### Fixed
* Don't upload automatically crash report via Hockey App (ask to the user)

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Push : Notification are not removed from iphone screen when message is read from another device
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble

## [1.25.278] - 2017-05-19
### Added
* Add ringback tone and hangup tone
* Display a notification for webrtc missed call
* Add change password
* Refactoring on search API and get user details client side
* Support token of 6 digits
* Add HockeyApp SDK for automatic crash report
* Business/Enterprise profiling: Step 2 (Restrict/allow feature for Business/Essential/Enterprise service Plan

### Fixed
* Zendesk#6926 // Presence setting failure
* Zendesk#6841 // Unable to pick up a call on Rainbow iOS.
* Presence status is not correct
* Tick to indicate presence is not updated
* Zendesk#6769 // The caller doesn't hear anything on iOS when he makes a call
* the communication isn't released on the iphone.
* Zendesk#6727 // WebRTC call issue in mulit-device case (iOS and Desktop)
* Zendesk#6388 // Missed a call on iOS because no Push Notification
* Zendesk#6359 // Audio Call fail between iOS & PC
* Behavior to be improved on iOS when authorizations were refused
* After logout of a newly created account, the login form is not displayed on iOS
* Opening a conversation on iOS does not display the message the first time

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Push : Notification are not removed from iphone screen when message is read from another device
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble

## [1.24.277] - 2017-05-12
### Fixed
* Delete last file in My rainbow share: crash
* Zendesk#6381 // French correction

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Push : Notification are not removed from iphone screen when message is read from another device
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble

## [1.24.276] - 2017-05-05
### Added
* Track usage with Google analytics
* Add Rainbow in share options of iOS application
* Remove MyInfos tab and rework the top bar.

### Fixed
* WebRTC on iOS is KO if an incoming call is taken on Desktop
* Add menu when click on call button in message view

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Push : Notification are not removed from iphone screen when message is read from another device
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble

## [1.23.274] - 2017-04-20
### Added
* Serveur de fichiers centralisé step 2.1

### Fixed
* Display of images overlap with date of messages
* Crash when opening file from My Rainbow Share tab

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Push : Notification are not removed from iphone screen when message is read from another device
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble

## [1.23.273] - 2017-04-13
### Fixed
* Typo error in french translation
* Emily avatar in My info view is not a circle

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Push : Notification are not removed from iphone screen when message is read from another device
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble

## [1.23.272] - 2017-04-13
### Added
* Add emily in my infos page
* Add My files in my infos page

### Changed
* Hide label name in attachment view for images
* Increase size of images in conversation view
* Rename My files to My Rainbow share

### Fixed
* Enrollment pages overlaps iOS data on the top of the screen
* iphone voip: after several communications , the icon make a voip call is missing

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Push : Notification are not removed from iphone screen when message is read from another device
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble

## [1.23.271] - 2017-04-12
### Changed
* Retrieve correctly maximum number of participant in bubbles
* Retrieve correctly file sharing consumption and quota

### Fixed
* Zendesk#6178 // Status gets blocked to "Busy" after WebRTC on iOS.
* OS client Emily : it's possible to call Emily (RTC audio call) but nothing happens
* Fix issue with ressource name that still contains old application version number

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Push : Notification are not removed from iphone screen when message is read from another device
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble

## [1.23.270] - 2017-04-07
### Added
* File sharing support (step 1)
* Push VoIP, receive calls while application is in background

### Changed
* Invert My network and My list
* Refocus webrtc call screen when application return in foreground

### Fixed
* android voip: crash of the application after an outgoing audio call
* Zendesk#5943 // Settings message mixed french and english
* iOS client Emily : Emily is able to create groups
* Declined join-team-invitation displayed on iOS until next reconnection
* Outgoing call log not displayed in iOS conversation in live
* As a user I want to see invitation with status 'failed'
* CRRAINB-261 Crash of the iOS Rainbow app when clicking the Contacts tab
* Don't increment counter for offline messages
* Fix crash on iPad iOS 9.3
* Speaker button state and mute button state was not persisted when refocusing the webrtc call screen
* Long names was truncated in bubbles
* Organize filter view was not working correctly when there was a webrtc call
* Fix red background color for companies with transparent avatar

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Push : Notification are not removed from iphone screen when message is read from another device
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble

## [1.22.1.269] - 2017-03-28
### Fixed
* deactivating the access to the microphone blocks the iOS app

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Push : Notification are not removed from iphone screen when message is read from another device
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble
* Push : No notifications for incomming VoIP calls

## [1.22.268] - 2017-03-21
### Fixed
* Avatar must be draw as circle except for companies
* New message sound not be played when presence is do not distrub or busy (only when application have focus)
* iOS client search banner : iOS client crashes when i select a company

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Push : Notification are not removed from iphone screen when message is read from another device
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble
* Push : No notifications for incomming VoIP calls

## [1.22.267] - 2017-03-20
### Added 
* Page compagnie/team - gestion de la demande d'accès à une compagnie - banner compagnie

### Fixed
* Zendesk#5411 // Profile information modifications from the iOS app are not working.
* ToS and Privacy policy links no more present in enrolment page in French

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Push : Notification are not removed from iphone screen when message is read from another device
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble
* Push : No notifications for incomming VoIP calls

## [1.22.266] - 2017-03-13
### Added
* Offer WebRTC audio facility on mobiles
* Log des appels webRTC (conversation) - finalisation
* Handle user profile at client side

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Some invitation are not removed when declined in multi-device configuration
* Push : Notification are not removed from iphone screen when message is read from another device
* Push : Counter on application badge are not coherent
* Push : No notifications of messages receive on bubbles
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble

## [1.21.264] - 2017-03-07
### Fixed
* Fix layout issues found during Apple validation

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Some invitation are not removed when declined in multi-device configuration
* Push : Notification are not removed from iphone screen when message is read from another device
* Push : Counter on application badge are not coherent
* Push : No notifications of messages receive on bubbles
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble

## [1.21.263] - 2017-03-03
### Fixed
* iOS app crash on bringing app in foreground

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Some invitation are not removed when declined in multi-device configuration
* Push : Notification are not removed from iphone screen when message is read from another device
* Push : Counter on application badge are not coherent
* Push : No notifications of messages receive on bubbles
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble

## [1.21.262] - 2017-02-27
### Added
* Add 3 languages: Norwegian, Finnish, Swedish languages support

### Fixed
* iOS Crash when opening a conversation with an unknown user
* Visibility invitations on iOS hidden by presence filter in contacts / my network page

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Some invitation are not removed when declined in multi-device configuration
* Push : Notification are not removed from iphone screen when message is read from another device
* Push : Counter on application badge are not coherent
* Push : No notifications of messages receive on bubbles
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble

## [1.21.261] - 2017-02-23
### Fixed
* Application crash when searching someone
* No network connection view stays presented after first installation
* Impossible to load previous messages in bubbles

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Some invitation are not removed when declined in multi-device configuration
* Push : Notification are not removed from iphone screen when message is read from another device
* Push : Counter on application badge are not coherent
* Push : No notifications of messages receive on bubbles
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble

## [1.21.260] - 2017-02-23
### Fixed
* Undefined error when trying to resend a join-company-request that is too young
* Error in "join company" page on iOS
* Please remove bubble's name on events in iOS conversation list
* iOS app crash on opening it after IM reception
* Zendesk#5255 // IOS 9.3 not connecting on Rainbow with v.1.20.258
* Zendesk#5040 // Connection issue with latest IOS 10.2.1 
* iOS client : logout fails when ios was locked
* Join rainbow or Join Team iOS page not fully localized in Brazilian
* Invitations list discrepancy between web and iOS
* Invite banner in conversation on iOS not refreshed in live
* Phone vibrates on incoming IM even if access to Notifications is not allowed
* Visibility invitations on iOS hidden by presence filter in contacts / my network page
* Text overlap in password setting page during invitation enrolment on iOS
* Avatars not updated on iOS after modification on web/desk
* Phone number should  be displayed with international format 
* iOS contacts: refuse to access to phone contacts leads to dislpay ghosts users

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Some invitation are not removed when declined in multi-device configuration
* Push : Notification are not removed from iphone screen when message is read from another device
* Push : Counter on application badge are not coherent
* Push : No notifications of messages receive on bubbles
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble

## [1.21.259] - 2017-02-17
### Added
* Page compagnie/team - gestion de la demande d'accès à une compagnie
* Log des appels webRTC (conversation) - finalisation
* Gestion de l'Admin pour qu'un user rejoingne une compagnie
* Call log informations in iOS conversations not formatted like other clients
* Push mobile on iOS: unread message counter on badge app incremented randomly (Requires new server version to be fully functionnal)
* Escalade conversation vers bulle
* Allow user to change bubble name
* Auto load messages by scrolling

### Fixed
* Reception IM de user qui ne sont pas dans la liste des conversations
* Don't increase conversation unread count for messages received in push
* Don't create conversation for bubble each time we start the application

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Some invitation are not removed when declined in multi-device configuration
* Push : Notification are not removed from iphone screen when message is read from another device
* Push : Counter on application badge are not coherent
* Push : No notifications of messages receive on bubbles
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble

## [1.20.258] - 2017-02-08
### Fixed
* iOS client : logout touch area is reduced when ios was locked
* Say #agenda to Emily and the iOS App crashes

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Some invitation are not removed when declined in multi-device configuration
* Push : Notification are not removed from iphone screen when message is read from another device
* Push : Counter on application badge are not coherent
* Push : No notifications of messages receive on bubbles
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble

## [1.20.257] - 2017-02-06
### Fixed
* iOS invitation acknowledgment button: switch them to have the validation on the right
* Zendesk#2912 // Full names are not all written in the same way.
* Each time iOS app is brought in foreground the "you're disconnected" banner appears
* Zendesk#4815 // Rainbow crash with IPHONE 6S on 4G connection
* Resend of an invitation younger than 1h accepted by iOS
* Zendesk#3780 // Rainbow app crash using iOS

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Some invitation are not removed when declined in multi-device configuration
* Push : Notification are not removed from iphone screen when message is read from another device
* Push : Counter on application badge are not coherent
* Push : No notifications of messages receive on bubbles
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble

## [1.20.256] - 2017-02-02
### Fixed
* Seen as not connected on iOS with mobile data off but wifi on
* Each time iOS app is brought in foreground the "you're disconnected" banner appears
* iOS: indication of who "is typing" not always presented in a bubble
* Zendesk#4992 // Once notifications access has been refused during app installation it is impossible to grant the access in the settings
* Weirp bip-bip when getting into the app
* Resend of an invitation younger than 1h accepted by iOS
* Big rooms tend to slow down infra
* Zendesk#3795 // Missing avatars on the first connection

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Some invitation are not removed when declined in multi-device configuration
* Push : Notification are not removed from iphone screen when message is read from another device
* Push : Counter on application badge are not coherent
* Push : No notifications of messages receive on bubbles
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble

## [1.19.1.255] - 2017-02-02
### Fixed
* Zendesk #5077: when pressing the contacts button on IOS the application crashes

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Some invitation are not removed when declined in multi-device configuration
* Push : Notification are not removed from iphone screen when message is read from another device
* Push : Counter on application badge are not coherent
* Push : No notifications of messages receive on bubbles
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble

## [1.20.254] - 2017-01-27
### Fixed
* iOS push is broken, iOS device doesn't register anymore

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Some invitation are not removed when declined in multi-device configuration
* Push : Notification are not removed from iphone screen when message is read from another device
* Push : Counter on application badge are not coherent
* Push : No notifications of messages receive on bubbles
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble


## [1.20.253] - 2017-01-27
### Added
* Mobile reliability: push mobile hardening
* Add 4 languages: Chinese simplified, Dutch, Portuguese, Portuguese Brazilian
* Privacy issue: better control of the display of personal data
* Gestion des invitations / ajout dans le roster
* Evolutions bulles (arrivée/départ, historique par participant)
* 3D touch shortcuts

### Fixed 
* Missing avatars when starting the app
* Dates of IM are not the send dates
* Problem with whitespaces in iOS App

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Some invitation are not removed when declined in multi-device configuration
* Push : Notification are not removed from iphone screen when message is read from another device
* Push : Counter on application badge are not coherent
* Push : No notifications of messages receive on bubbles
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble

## [1.19.252] - 2017-01-12
### Fixed
* Onboarding: error during account creation on invitation sent by a team admin - IOS PART
* "You're disconnected" banner is empty on iOS
* Message lost in conversation on iOS
* Message timestamp into conversations and bubble
* iOS wizard fields icon missing when no value in
* IM under writing lost when switching back from another conversation
* Wrong timestamp information
* Logout should remove password

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Some invitation are not removed when declined in multi-device configuration
* Push : Notification are not removed from iphone screen when message is read from another device
* Push : Counter on application badge are not coherent
* Push : No notifications of messages receive on bubbles
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble

## [1.18.3.251] - 2017-01-10
### Fixed
* Too many invitations displayed under iOS and using "Resend" adds one in the list
* iOS app crashes during onbaording when app is already installed
* Bad login page after iOS onboarding

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Some invitation are not removed when declined in multi-device configuration
* Push : Notification are not removed from iphone screen when message is read from another device
* Push : Counter on application badge are not coherent
* Push : No notifications of messages receive on bubbles
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble

## [1.18.3.250] - 2017-01-09
### Fixed
* Fixing null cases in onBoarding from email

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Some invitation are not removed when declined in multi-device configuration
* Push : Notification are not removed from iphone screen when message is read from another device
* Push : Counter on application badge are not coherent
* Push : No notifications of messages receive on bubbles
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble

## [1.18.3.249] - 2017-01-09
### Fixed
* Crash during onBoarding from email

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Some invitation are not removed when declined in multi-device configuration
* Push : Notification are not removed from iphone screen when message is read from another device
* Push : Counter on application badge are not coherent
* Push : No notifications of messages receive on bubbles
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble

## [1.18.3.248] - 2017-01-09
### Fixed
* iOS onboarding: invitation ID not transmitted to app,new user needs to "create an account"
* Wording and icons for invitation
* Bad label in iOS for sending an invitation again: "Recall" instead of "Resend"

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Some invitation are not removed when declined in multi-device configuration
* Push : Notification are not removed from iphone screen when message is read from another device
* Push : Counter on application badge are not coherent
* Push : No notifications of messages receive on bubbles
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble


## [1.19.247] - 2017-01-06

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Some invitation are not removed when declined in multi-device configuration
* Push : Notification are not removed from iphone screen when message is read from another device
* Push : Counter on application badge are not coherent
* Push : No notifications of messages receive on bubbles
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble


## [1.18.2.246] - 2017-01-04
### Fixed
* iOS client search : search users in pending , application crash

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Some invitation are not removed when declined in multi-device configuration
* Push : Notification are not removed from iphone screen when message is read from another device
* Push : Counter on application badge are not coherent
* Push : No notifications of messages receive on bubbles
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble

## [1.18.2.245] - 2017-01-04
### Fixed
* iOS client search: Search fails application is closed

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Some invitation are not removed when declined in multi-device configuration
* Push : Notification are not removed from iphone screen when message is read from another device
* Push : Counter on application badge are not coherent
* Push : No notifications of messages receive on bubbles
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble

## [1.18.1.243] - 2016-12-20
### Fixed
* Reverting correction of iOS timestamping inconsistencies on IMs

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Some invitation are not removed when declined in multi-device configuration
* Push : Notification are not removed from iphone screen when message is read from another device
* Push : Counter on application badge are not coherent
* Push : No notifications of messages receive on bubbles
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble

## [1.18.1.242] - 2016-12-20
### Fixed
* Contacts on iOS presented in a bad order (now filtering on both first and last names)
* On Enrollment page the texts are overlapped

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Some invitation are not removed when declined in multi-device configuration
* Push : Notification are not removed from iphone screen when message is read from another device
* Push : Counter on application badge are not coherent
* Push : No notifications of messages receive on bubbles
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble

## [1.18.1.241] - 2016-12-20
### Fixed
* Conversation to email fails on iOS
* Opening contact page with 2 pending invitations make the iOS app crash
* iOS timestamping inconsistencies on IMs
* Missing timestamps in conversations
* Contacts on iOS presented in a bad order
* Contacts appears twice in search result
* Icons in text fields on login page blinks when typing

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Some invitation are not removed when declined in multi-device configuration
* Push : Notification are not removed from iphone screen when message is read from another device
* Push : Counter on application badge are not coherent
* Push : No notifications of messages receive on bubbles
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble

## [1.18.239] - 2016-12-16
### Fixed
* Fix crash when opening a conversation

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Some invitation are not removed when declined in multi-device configuration
* Push : Notification are not removed from iphone screen when message is read from another device
* Push : Counter on application badge are not coherent
* Push : No notifications of messages receive on bubbles
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble

## [1.18.238] - 2016-12-16
### Fixed
* iOS notification: pressing on a notif opens the wrong conversation
* Bad conversation opened on iOS on IM reception
* Push mobile on iOS: unread counter not reset
* Push mobile on iOS: two vibration on IM reception

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Some invitation are not removed when declined in multi-device configuration
* Push : Notification are not removed from iphone screen when message is read from another device
* Push : Counter on application badge are not coherent
* Push : No notifications of messages receive on bubbles
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble

## [1.18.237] - 2016-12-14
### Added
* Search engine improvements
* Contacts, Lists and invitations

### Fixed
* iOS client group : I can add myself in my group
* iOS client call log : Missed call , decline , answered call are not displayed on iOS
* Zendesk#2342 // Rainbow drains my battery on my iPhone

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Some invitation are not removed when declined in multi-device configuration
* Push : Notification are not removed from iphone screen when message is read from another device
* Push : Counter on application badge are not coherent
* Push : No notifications of messages receive on bubbles
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble

## [1.18.236] - 2016-12-08
### Added
* Conversation view has been rework to follow marketing request
* Message view has been reworkd to follow marketing request
* Add search button in left corner of conversation and bubble screen
* Add send conversation by email
* Add cache of conversations and bubbles for offline mode
* Enable Apple push, only peer to peer notification are presented

### Fixed
* Remove "remove message" feature in case of bubble
* List creation doesn't work anymore on iOS
* iOS client tries to reconnect even with bad password causing a locked account
* French translation of "password"
* The word "activation" is not entirely written on iOS
* Message with password reset features
* iOS Contact filtering setting kept at logout but lost at app closing
* Acknowledgement ticks get blue even if the message has not been read by distant

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Some invitation are not removed when declined in multi-device configuration
* Push : Notification are not removed from iphone screen when message is read from another device
* Push : Counter on application badge are not coherent
* Push : No notifications of messages receive on bubbles
* Push : No notifications of invitation to join network
* Push : No notifications of invitation to join a bubble

## [1.17.233] - 2016-11-22
### Fixed
* iOS app doesn't refresh the list of pending invitation
* Trying to open a vCard crash the app

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Some invitation are not removed when declined in multi-device configuration

## [1.17.232] - 2016-11-21
### Fixed
* Fix display problem in Bubble details when scrolling
* Display number of pending invitation counter on Contact tab
* Integrate correct translation for english
* Change size of top banner in conversation view

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Some invitation are not removed when declined in multi-device configuration

## [1.17.231] - 2016-11-18
### Added
* User can now change is country from user profile editing view

### Fixed
* Checkmark are back in My info view
* Select a list was not working
* Presence in My Info view was not refreshed correctly
* Message sent to Emily are now marked as read correctly
* Fix disconnection problem when the server send us an 503 error

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Some invitation are not removed when declined in multi-device configuration

## [1.17.230] - 2016-11-15
### Added
* Invitation to join my network are now presented at top of contact view
* Onboarding add country list
* New translations
* Presence is now persisted when user logout
* Display error message when login with a too old application

### Fixed
* Fix slow typing issue
* Fix Mute/Unmute problems
* Improvement of presence
* Improvement of contacts list

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations
* Some invitation are not removed when declined in multi-device configuration

## [1.16.1.229] - 2016-11-02
### Fixed
* Contact access problem again

## [1.16.1.227] - 2016-11-02
### Fixed
* Crash when entering in conversation
* Contact access problem

## [1.16.224] - 2016-10-28
### Fixed
* Replace all groups by lists
* Personal information like contact names, IM exchanges must be hidden in the logs
* Mobile iOS client : Not possible to delete an archived bubble

## [1.16.223] - 2016-10-27
### Added
* Allow edition of groups from contact vcard

### Fixed
* Avatar is missing with the icon "typing"
* User is able to see all informations about another user in a different company (without visibility)
* Rainbow asks me access to my local contacts to create a group
* Zendesk#3556 // User typing" status doesn't show up in desktop client when other user is using Iphone
* No specific error in case of to many many failed login attempts
* Personal information like contact names, IM exchanges must be hidden in the logs
* Zendesk#3519 // iOS Connection impossible using 4G network
* IM counter on Rainbow icon not always the good one (not systematic)
* Back an forth on the conversation changes the order of the IMs
* Application data should be removed with the app

## [1.16.222] - 2016-10-21
### Added
* Silence mode for conversations
* Term of service added after creating a user, or loggin with an not initialized user
* Delete all messages for bubbles
* Unread message for bubbles are now displayed
* Groups management
* Sort and filter contacts
* Click on phone number on web client, make using the mobile

### Fixed
* A user should not see himself in the participant list of the bubble

### Restriction
* Messages status delivered to user and read by user are not display for bubbles conversations

## [1.15.221] - 2016-10-10
### Fixed
* Presence is shown as offline while I'm really online

## [1.15.220] - 2016-10-07
### Fixed
* Online contact filter modifications
* Bad content of contact / my network view after refusing to see "all"
* Bubble tab is not displayed
* Crash of application
* Lose of last message when using Chinese keyboard
* Result of search is not consistent
* Application data should be removed with the app
* I send a message to a College, but message was send to my self now I see my self under chats
* iOS APP crash during add several users to a bubble


### Restriction
* Unread messages for bubbles are not displayed
* Remove all messages for bubbles is not available
* Messages status delivered to user and read by user are not display for bubbles conversations

## [1.15.219] - 2016-09-30
### Added
* Bubbles archive and Bubble delete mechanisms
* Display delivery date and read date for messages
* Add manual away presence

### Fixed
* Crash of application with linked contact
* Avatar for Emily
* Messages ergonomy improvement
* Notification was not displayed in some cases
* Bubble multi devices improvements

### Restriction
* Unread messages for bubbles are not displayed
* Remove all messages for bubbles is not available
* Messages status delivered to user and read by user are not display for bubbles conversations

## [1.14.218] - 2016-09-19
###Fixed
* Bubble invitation counter not removed on iOS in multidevice config	
* Mobile client iOS : mix french english
* Mobile client iOS bubble: Key create a bubble not active
* MUC on iOS: delete room not offered to room's owner
* Bubble avatar are identical between clients
* Fix crash when contact change is status from online->invisible

### Restriction
* Unread messages for bubbles are not displayed
* Remove all messages for bubbles is not available
* Messages status delivered to user and read by user are not display for bubbles conversations

## [1.14.217] - 2016-09-16
### Fixed
* MUC on iOS: delete room not offered to room's owner
* Mobile client iOS: remove the messages in a room is not possible
* iOS: Bad contact list displayed according to group selected
* vcard data modification on iOS when out of coverage is possible
* iOS App crashes when reconnecting after "out of coverage" period
* IM sent while being out of any network is not presented to recipient
* Translation error and misspelling
* Mobile client iOS: sometime the connection fails
* Job title is not updated on iPhone
* Not possible to change the avatar through the iOS

### Restriction
* Unread messages for bubbles are not displayed
* Remove all messages for bubbles is not available
* Messages status delivered to user and read by user are not display for bubbles conversations

## [1.14.216] - 2016-09-09
### Fixed
* Download avatars from server when it's required
* Fix crash on network re-connexion

## [1.14.215] - 2016-09-09
### Added
* Add support of Multi user chat in multi device configuration
* Implementation of fast reconnection XEP-198
* Improvement of cache mechanism to increase the application startup
* Improvement of UI when application lost network
* Filter contacts by Online status

### Fixed
* Crash at startup due to local contacts access
* Mobile presence is not correctly displayed

## [1.13.214] - 2016-08-29
### Fixed
* Contact tab disappears after network lost

## [1.13.213] - 2016-08-26
### Added
* Add support of Multi user chat
* New emily

### Fixed
* Remove messages is not synchro in multi device configuration

### Restriction
* Mutli user chat is not working in multi device scenario

## [1.12.212] - 2016-08-08
### Fixed
* Freeze of application due to too many thread created

## [1.12.211] - 2016-08-05
### Fixed
* Fix some reported crash
* Fix some user interface issues
* Improve messages cache

## [1.11.210] - 2016-07-12
### Fixed
* The conversation screen is blocked and the application crashed

## [1.11.209] - 2016-07-12
### Added
* Click on INVIT should be validated with a pop up

### Fixed
* Application crashed sometimes when clicking 'load earlier messages'
* Contact page should not overlap the abcdef... column
* Slightly improve the speed of display the message view

## [1.11.208] - 2016-07-08
### Fixed
* Problem with avatar cache after installing last version

## [1.11.207] - 2016-07-08
### Added
* Chinese translations added 
* Display date of last messages in conversation view
* Move unread message indicator
* Implement new vCard API (Phone numbers are not yet transmitted to server)
* Refresh automatically vCard
* Implement lost password feature
* Take iPhone contact sort order and contact display order into account in Rainbow
* Add cache of vCard
* Implement delete all messages in message view
* Add cache of messages and synchronisation mechanism

### Fixed
* Display correctly PBX telephony presence
* Fix some translation error in french (Some new messages are only display in French and in English)
* Rework login screen
* Remove personal information from logs file
* Change some messages and images in on boarding pages
* Do not merge message bubble and missed call or file transfer bubble
* Change avatar cache key
* Fix some graphical display problem depending of device screen size

## [1.10.2.206] - 2016-06-29
### Fixed
* Decrease application counters when reading messages are read from another device.

## [1.10.205] - 2016-06-27
### Fixed
* Fix error popup at first installation

## [1.10.204] - 2016-06-24
### Fixed
* Fix crash on logout

## [1.10.203] - 2016-06-24
### Fixed
* Change online mobile icon and mechanism
* Harmonize message ackittement
* Fix crash
* Fix reconnection issues

## [1.10.202] - 2016-06-21
### Added
* Add a cache mechanism for avatar
* Add chinese translations
* Display correclty missed call and file transfer in conversation view 

### Fixed
* Fix reconnection issues again

## [1.10.201] - 2016-06-20
### Fixed
* Fix reconnection issues
* Fix conversation selection and wrong update after sending messages
* Some minor GUI evolutions

## [1.10.200] - 2016-06-17
### Added
* Load conversation messages from a cache
* Rework vcard display
* Add new icons from graphic library
* Implement delivered and read mechanisms

### Fixed
* Fix connection issue for user with long email address
* Finnish on boarding on mobile

## [1.04.195] - 2016-06-09
### Fixed
* Open the good conversation when selecting a contact

## [1.04.194] - 2016-06-07
### Fixed
* Avoid creating multiple conversations with the same person in case of offline messages
* Open the good conversation when selecting a contact
* Contact details action was not working correctly

## [1.04.193] - 2016-06-07
### Fixed
* Fix crash at startup due to wrong type of object in lastMessageDate

## [1.04.192] - 2016-06-07
### Fixed
* Keep avatar in cache to avoid too many data consumption
* Fix crash at startup
* Avoid creating multiple conversations with the same person (bis)

## [1.04.191] - 2016-06-03
### Fixed
* Avoid creating multiple conversations with the same person 

## [1.04.190] - 2016-06-03
### Added
* New conversations api implemented
* New avatar api implemented
* Add telephony presence state
* Add new translations

### Fixed
* Display good date and time for offline messages
* Fix disconnection problems
* Fix application freeze problems
* Fix connection issues

## [1.03.180] - 2016-05-20
### Added
* Allow sending debug logs from login screen
* New presence algorithm implemented
* Set RainbowID in vcard
* Differentiate busy state and Do not distrub state

### Fixed
* Fix problem when connecting to ALE CORP wifi
* Allow removing info from vcard
* Fix problem that can consume batterie
* Fix problem with name displayed when local informations are available
* Fix problem of conversation with myself after a webrtc call from web

## [1.02.167] - 2016-04-29
### Fixed
* Reconnection issue while application is in background
* Disable contact synchronisation
* Change label in contacts access popup

## [1.6.2] 1.0.0 Build 165 - 2016-04-25
### Added
* Legals infos

### Fixed
* Application very slow when contact access os allowed
* Fix crash when tchatting with someone, or when logout
* Detection of rainbow user was not working correctly 
* Avoid blocking main thread with contact synchronization
* Fix crash
* Stabilize application in background mode
* Display conversation imediatelly after login don't wait for last message

## [1.6.1] 1.0.0 Build 161 - 2016-04-15
### Added
* User can now edit is own vCard
* Detect local contact update, and upload updated contact to server
* Automatically accept subscription request if users are in the same company 
* Add Unit test environement
* Converged presence

### Fixed
* Display good last activity date for away contact
* Don't display Rainbow logo on online contact
* Improve login screen for iPhone 4S screen size


## [1.5.0] 1.0.0 Build 22 - 2016-04-01
### Added
* Send invitation to local contact
* Add support fo acknownledged messages (XEP 184)
* Search if local contact are Rainbow users
* Add Popup information about contact acces

### Fixed
* Use the "real" date for offline messages
* Fix emoticon supported

## [1.4.0] 1.0.0 Build 21 - 2016-03-18
### Added
* en-US and fr-FR translations
* Support of conversation with user not in our colleagues list
* Upload of iPhone local contacts on server

### Fixed
* Improve reconnection on network lost
* Last message in conversation not displayed
* Improve emoji support
